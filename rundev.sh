#!/usr/bin/env bash
#yarn run webpack:build
#./mvnw -Pdev,no-liquibase -DskipTests=true package
pkill -9 java
file=/home/ec2-user/amass_marketplace/target/datamarketplace-0.0.1-SNAPSHOT.war
if [ -f $file ] ; then 
    cd /home/ec2-user/amass_marketplace
	java -Xmx6G -Dschedule=true -DprocessEmails=true -jar $file >> out.log &
else
	java -Xmx6G -Dschedule=true -DprocessEmails=true -jar target/datamarketplace-0.0.1-SNAPSHOT.war --spring.profiles.active=dev,no-liquibase,swagger >> /home/ec2-user/amass_marketplace/out.log &	
fi
