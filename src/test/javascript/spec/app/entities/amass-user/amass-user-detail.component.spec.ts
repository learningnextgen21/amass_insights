
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AmassUserDetailComponent } from '../../../../../../main/webapp/app/entities/amass-user/amass-user-detail.component';
import { AmassUserService } from '../../../../../../main/webapp/app/entities/amass-user/amass-user.service';
import { AmassUser } from '../../../../../../main/webapp/app/entities/amass-user/amass-user.model';

describe('Component Tests', () => {

    describe('AmassUser Management Detail Component', () => {
        let comp: AmassUserDetailComponent;
        let fixture: ComponentFixture<AmassUserDetailComponent>;
        let service: AmassUserService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [AmassUserDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AmassUserService,
                    JhiEventManager
                ]
            }).overrideTemplate(AmassUserDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AmassUserDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AmassUserService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new AmassUser(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.amassUser).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
