
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DataArticleDetailComponent } from '../../../../../../main/webapp/app/entities/data-article/data-article-detail.component';
import { DataArticleService } from '../../../../../../main/webapp/app/entities/data-article/data-article.service';
import { DataArticle } from '../../../../../../main/webapp/app/entities/data-article/data-article.model';

describe('Component Tests', () => {

    describe('DataArticle Management Detail Component', () => {
        let comp: DataArticleDetailComponent;
        let fixture: ComponentFixture<DataArticleDetailComponent>;
        let service: DataArticleService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [DataArticleDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DataArticleService,
                    JhiEventManager
                ]
            }).overrideTemplate(DataArticleDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DataArticleDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DataArticleService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new DataArticle(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.dataArticle).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
