
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DataProviderTypeDetailComponent } from '../../../../../../main/webapp/app/entities/data-provider-type/data-provider-type-detail.component';
import { DataProviderTypeService } from '../../../../../../main/webapp/app/entities/data-provider-type/data-provider-type.service';
import { DataProviderType } from '../../../../../../main/webapp/app/entities/data-provider-type/data-provider-type.model';

describe('Component Tests', () => {

    describe('DataProviderType Management Detail Component', () => {
        let comp: DataProviderTypeDetailComponent;
        let fixture: ComponentFixture<DataProviderTypeDetailComponent>;
        let service: DataProviderTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [DataProviderTypeDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DataProviderTypeService,
                    JhiEventManager
                ]
            }).overrideTemplate(DataProviderTypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DataProviderTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DataProviderTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new DataProviderType(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.dataProviderType).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
