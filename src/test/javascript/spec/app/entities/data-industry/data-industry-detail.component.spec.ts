
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DataIndustryDetailComponent } from '../../../../../../main/webapp/app/entities/data-industry/data-industry-detail.component';
import { DataIndustryService } from '../../../../../../main/webapp/app/entities/data-industry/data-industry.service';
import { DataIndustry } from '../../../../../../main/webapp/app/entities/data-industry/data-industry.model';

describe('Component Tests', () => {

    describe('DataIndustry Management Detail Component', () => {
        let comp: DataIndustryDetailComponent;
        let fixture: ComponentFixture<DataIndustryDetailComponent>;
        let service: DataIndustryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [DataIndustryDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DataIndustryService,
                    JhiEventManager
                ]
            }).overrideTemplate(DataIndustryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DataIndustryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DataIndustryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new DataIndustry(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.dataIndustry).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
