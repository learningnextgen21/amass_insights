
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AdminRoleDetailComponent } from '../../../../../../main/webapp/app/entities/admin-role/admin-role-detail.component';
import { AdminRoleService } from '../../../../../../main/webapp/app/entities/admin-role/admin-role.service';
import { AdminRole } from '../../../../../../main/webapp/app/entities/admin-role/admin-role.model';

describe('Component Tests', () => {

    describe('AdminRole Management Detail Component', () => {
        let comp: AdminRoleDetailComponent;
        let fixture: ComponentFixture<AdminRoleDetailComponent>;
        let service: AdminRoleService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [AdminRoleDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AdminRoleService,
                    JhiEventManager
                ]
            }).overrideTemplate(AdminRoleDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AdminRoleDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AdminRoleService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new AdminRole(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.adminRole).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
