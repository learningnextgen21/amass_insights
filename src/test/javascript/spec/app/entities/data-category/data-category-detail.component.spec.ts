
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DataCategoryDetailComponent } from '../../../../../../main/webapp/app/entities/data-category/data-category-detail.component';
import { DataCategoryService } from '../../../../../../main/webapp/app/entities/data-category/data-category.service';
import { DataCategory } from '../../../../../../main/webapp/app/entities/data-category/data-category.model';

describe('Component Tests', () => {

    describe('DataCategory Management Detail Component', () => {
        let comp: DataCategoryDetailComponent;
        let fixture: ComponentFixture<DataCategoryDetailComponent>;
        let service: DataCategoryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [DataCategoryDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DataCategoryService,
                    JhiEventManager
                ]
            }).overrideTemplate(DataCategoryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DataCategoryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DataCategoryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new DataCategory(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.dataCategory).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
