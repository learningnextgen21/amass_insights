
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AmassUserProviderDetailComponent } from '../../../../../../main/webapp/app/entities/amass-user-provider/amass-user-provider-detail.component';
import { AmassUserProviderService } from '../../../../../../main/webapp/app/entities/amass-user-provider/amass-user-provider.service';
import { AmassUserProvider } from '../../../../../../main/webapp/app/entities/amass-user-provider/amass-user-provider.model';

describe('Component Tests', () => {

    describe('AmassUserProvider Management Detail Component', () => {
        let comp: AmassUserProviderDetailComponent;
        let fixture: ComponentFixture<AmassUserProviderDetailComponent>;
        let service: AmassUserProviderService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [AmassUserProviderDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AmassUserProviderService,
                    JhiEventManager
                ]
            }).overrideTemplate(AmassUserProviderDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AmassUserProviderDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AmassUserProviderService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new AmassUserProvider(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.amassUserProvider).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
