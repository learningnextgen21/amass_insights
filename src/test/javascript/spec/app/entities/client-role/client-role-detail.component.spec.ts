
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ClientRoleDetailComponent } from '../../../../../../main/webapp/app/entities/client-role/client-role-detail.component';
import { ClientRoleService } from '../../../../../../main/webapp/app/entities/client-role/client-role.service';
import { ClientRole } from '../../../../../../main/webapp/app/entities/client-role/client-role.model';

describe('Component Tests', () => {

    describe('ClientRole Management Detail Component', () => {
        let comp: ClientRoleDetailComponent;
        let fixture: ComponentFixture<ClientRoleDetailComponent>;
        let service: ClientRoleService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [ClientRoleDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ClientRoleService,
                    JhiEventManager
                ]
            }).overrideTemplate(ClientRoleDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientRoleDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientRoleService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new ClientRole(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.clientRole).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
