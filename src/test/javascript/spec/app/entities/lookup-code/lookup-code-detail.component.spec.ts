
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { LookupCodeDetailComponent } from '../../../../../../main/webapp/app/entities/lookup-code/lookup-code-detail.component';
import { LookupCodeService } from '../../../../../../main/webapp/app/entities/lookup-code/lookup-code.service';
import { LookupCode } from '../../../../../../main/webapp/app/entities/lookup-code/lookup-code.model';

describe('Component Tests', () => {

    describe('LookupCode Management Detail Component', () => {
        let comp: LookupCodeDetailComponent;
        let fixture: ComponentFixture<LookupCodeDetailComponent>;
        let service: LookupCodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [LookupCodeDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    LookupCodeService,
                    JhiEventManager
                ]
            }).overrideTemplate(LookupCodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LookupCodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LookupCodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new LookupCode(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.lookupCode).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
