
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AmassUserNotificationDetailComponent } from '../../../../../../main/webapp/app/entities/amass-user-notification/amass-user-notification-detail.component';
import { AmassUserNotificationService } from '../../../../../../main/webapp/app/entities/amass-user-notification/amass-user-notification.service';
import { AmassUserNotification } from '../../../../../../main/webapp/app/entities/amass-user-notification/amass-user-notification.model';

describe('Component Tests', () => {

    describe('AmassUserNotification Management Detail Component', () => {
        let comp: AmassUserNotificationDetailComponent;
        let fixture: ComponentFixture<AmassUserNotificationDetailComponent>;
        let service: AmassUserNotificationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [AmassUserNotificationDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AmassUserNotificationService,
                    JhiEventManager
                ]
            }).overrideTemplate(AmassUserNotificationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AmassUserNotificationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AmassUserNotificationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new AmassUserNotification(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.amassUserNotification).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
