
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DataOrganizationDetailComponent } from '../../../../../../main/webapp/app/entities/data-organization/data-organization-detail.component';
import { DataOrganizationService } from '../../../../../../main/webapp/app/entities/data-organization/data-organization.service';
import { DataOrganization } from '../../../../../../main/webapp/app/entities/data-organization/data-organization.model';

describe('Component Tests', () => {

    describe('DataOrganization Management Detail Component', () => {
        let comp: DataOrganizationDetailComponent;
        let fixture: ComponentFixture<DataOrganizationDetailComponent>;
        let service: DataOrganizationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [DataOrganizationDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DataOrganizationService,
                    JhiEventManager
                ]
            }).overrideTemplate(DataOrganizationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DataOrganizationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DataOrganizationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new DataOrganization(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.dataOrganization).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
