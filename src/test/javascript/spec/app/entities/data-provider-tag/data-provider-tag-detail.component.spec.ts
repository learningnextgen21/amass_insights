
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DataProviderTagDetailComponent } from '../../../../../../main/webapp/app/entities/data-provider-tag/data-provider-tag-detail.component';
import { DataProviderTagService } from '../../../../../../main/webapp/app/entities/data-provider-tag/data-provider-tag.service';
import { DataProviderTag } from '../../../../../../main/webapp/app/entities/data-provider-tag/data-provider-tag.model';

describe('Component Tests', () => {

    describe('DataProviderTag Management Detail Component', () => {
        let comp: DataProviderTagDetailComponent;
        let fixture: ComponentFixture<DataProviderTagDetailComponent>;
        let service: DataProviderTagService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [DataProviderTagDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DataProviderTagService,
                    JhiEventManager
                ]
            }).overrideTemplate(DataProviderTagDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DataProviderTagDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DataProviderTagService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new DataProviderTag(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.dataProviderTag).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
