
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DataProviderDetailComponent } from '../../../../../../main/webapp/app/entities/data-provider/data-provider-detail.component';
import { DataProviderService } from '../../../../../../main/webapp/app/entities/data-provider/data-provider.service';
import { DataProvider } from '../../../../../../main/webapp/app/entities/data-provider/data-provider.model';

describe('Component Tests', () => {

    describe('DataProvider Management Detail Component', () => {
        let comp: DataProviderDetailComponent;
        let fixture: ComponentFixture<DataProviderDetailComponent>;
        let service: DataProviderService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [DataProviderDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DataProviderService,
                    JhiEventManager
                ]
            }).overrideTemplate(DataProviderDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DataProviderDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DataProviderService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new DataProvider(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.dataProvider).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
