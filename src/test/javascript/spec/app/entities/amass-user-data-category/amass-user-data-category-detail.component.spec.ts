
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AmassUserDataCategoryDetailComponent } from '../../../../../../main/webapp/app/entities/amass-user-data-category/amass-user-data-category-detail.component';
import { AmassUserDataCategoryService } from '../../../../../../main/webapp/app/entities/amass-user-data-category/amass-user-data-category.service';
import { AmassUserDataCategory } from '../../../../../../main/webapp/app/entities/amass-user-data-category/amass-user-data-category.model';

describe('Component Tests', () => {

    describe('AmassUserDataCategory Management Detail Component', () => {
        let comp: AmassUserDataCategoryDetailComponent;
        let fixture: ComponentFixture<AmassUserDataCategoryDetailComponent>;
        let service: AmassUserDataCategoryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [AmassUserDataCategoryDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AmassUserDataCategoryService,
                    JhiEventManager
                ]
            }).overrideTemplate(AmassUserDataCategoryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AmassUserDataCategoryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AmassUserDataCategoryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new AmassUserDataCategory(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.amassUserDataCategory).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
