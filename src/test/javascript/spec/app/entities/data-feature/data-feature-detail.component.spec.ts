
import {of as observableOf,  Observable } from 'rxjs';
/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DatamarketplaceTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DataFeatureDetailComponent } from '../../../../../../main/webapp/app/entities/data-feature/data-feature-detail.component';
import { DataFeatureService } from '../../../../../../main/webapp/app/entities/data-feature/data-feature.service';
import { DataFeature } from '../../../../../../main/webapp/app/entities/data-feature/data-feature.model';

describe('Component Tests', () => {

    describe('DataFeature Management Detail Component', () => {
        let comp: DataFeatureDetailComponent;
        let fixture: ComponentFixture<DataFeatureDetailComponent>;
        let service: DataFeatureService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DatamarketplaceTestModule],
                declarations: [DataFeatureDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DataFeatureService,
                    JhiEventManager
                ]
            }).overrideTemplate(DataFeatureDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DataFeatureDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DataFeatureService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(observableOf(new DataFeature(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.dataFeature).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
