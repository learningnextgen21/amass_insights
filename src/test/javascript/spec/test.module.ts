import { NgModule } from '@angular/core';
// import { MockBackend } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from './helpers/mock-language.service';

@NgModule({
    providers: [
       // MockBackend,
       // BaseRequestOptions,
        {
            provide: JhiLanguageService,
            useClass: MockLanguageService
        },
        {
    provide: HttpClient,
    useValue: undefined
},
         //   deps: [MockBackend, BaseRequestOptions]
       // }
    ]
})
export class DatamarketplaceTestModule {}
