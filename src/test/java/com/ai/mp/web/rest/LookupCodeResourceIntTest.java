package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;
import com.ai.mp.domain.LookupCode;
import com.ai.mp.repository.LookupCodeRepository;
import com.ai.mp.repository.search.LookupCodeSearchRepository;
import com.ai.mp.service.UserService;
import com.ai.mp.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LookupCodeResource REST controller.
 *
 * @see LookupCodeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class LookupCodeResourceIntTest {

    private static final String DEFAULT_LOOKUPMODULE = "AAAAAAAAAA";
    private static final String UPDATED_LOOKUPMODULE = "BBBBBBBBBB";

    private static final String DEFAULT_LOOKUPCODE = "AAAAAAAAAA";
    private static final String UPDATED_LOOKUPCODE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_SORTORDER = 11;
    private static final Integer UPDATED_SORTORDER = 10;

    private static final Integer DEFAULT_LASTUPDATEDBY = 11;
    private static final Integer UPDATED_LASTUPDATEDBY = 10;

    private static final Date DEFAULT_CREATEDON = new Date();
    private static final Date UPDATED_CREATEDON = new Date();

    private static final Date DEFAULT_LASTUPDATE = new Date();
    private static final Date UPDATED_LASTUPDATE = new Date();

    private static final Integer DEFAULT_CREATEDBY = 11;
    private static final Integer UPDATED_CREATEDBY = 10;

    @Autowired
    private LookupCodeRepository lookupCodeRepository;

    @Autowired
    private LookupCodeSearchRepository lookupCodeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLookupCodeMockMvc;

    private LookupCode lookupCode;

    @Autowired
    private UserService userService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LookupCodeResource lookupCodeResource = new LookupCodeResource(lookupCodeRepository, lookupCodeSearchRepository,userService);
        this.restLookupCodeMockMvc = MockMvcBuilders.standaloneSetup(lookupCodeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LookupCode createEntity(EntityManager em) {
        LookupCode lookupCode = new LookupCode()
            .lookupmodule(DEFAULT_LOOKUPMODULE)
            .lookupcode(DEFAULT_LOOKUPCODE)
            .description(DEFAULT_DESCRIPTION)
            .sortorder(DEFAULT_SORTORDER)
            .lastupdatedby(DEFAULT_LASTUPDATEDBY)
            .createdon(DEFAULT_CREATEDON)
            .lastupdate(DEFAULT_LASTUPDATE)
            .createdby(DEFAULT_CREATEDBY);
        return lookupCode;
    }

    @Before
    public void initTest() {
        lookupCodeSearchRepository.deleteAll();
        lookupCode = createEntity(em);
    }

    @Test
    @Transactional
    public void createLookupCode() throws Exception {
        int databaseSizeBeforeCreate = lookupCodeRepository.findAll().size();

        // Create the LookupCode
        restLookupCodeMockMvc.perform(post("/api/lookup-codes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lookupCode)))
            .andExpect(status().isCreated());

        // Validate the LookupCode in the database
        List<LookupCode> lookupCodeList = lookupCodeRepository.findAll();
        assertThat(lookupCodeList).hasSize(databaseSizeBeforeCreate + 1);
        LookupCode testLookupCode = lookupCodeList.get(lookupCodeList.size() - 1);
        assertThat(testLookupCode.getLookupmodule()).isEqualTo(DEFAULT_LOOKUPMODULE);
        assertThat(testLookupCode.getLookupcode()).isEqualTo(DEFAULT_LOOKUPCODE);
        assertThat(testLookupCode.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testLookupCode.getSortorder()).isEqualTo(DEFAULT_SORTORDER);
        assertThat(testLookupCode.getLastupdatedby()).isEqualTo(DEFAULT_LASTUPDATEDBY);
        assertThat(testLookupCode.getCreatedon()).isEqualTo(DEFAULT_CREATEDON);
        assertThat(testLookupCode.getLastupdate()).isEqualTo(DEFAULT_LASTUPDATE);
        assertThat(testLookupCode.getCreatedby()).isEqualTo(DEFAULT_CREATEDBY);

        // Validate the LookupCode in Elasticsearch
        LookupCode lookupCodeEs = lookupCodeSearchRepository.findOne(testLookupCode.getId());
        assertThat(lookupCodeEs).isEqualToComparingFieldByField(testLookupCode);
    }

    @Test
    @Transactional
    public void createLookupCodeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = lookupCodeRepository.findAll().size();

        // Create the LookupCode with an existing ID
        lookupCode.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLookupCodeMockMvc.perform(post("/api/lookup-codes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lookupCode)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<LookupCode> lookupCodeList = lookupCodeRepository.findAll();
        assertThat(lookupCodeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkLookupmoduleIsRequired() throws Exception {
        int databaseSizeBeforeTest = lookupCodeRepository.findAll().size();
        // set the field null
        lookupCode.setLookupmodule(null);

        // Create the LookupCode, which fails.

        restLookupCodeMockMvc.perform(post("/api/lookup-codes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lookupCode)))
            .andExpect(status().isBadRequest());

        List<LookupCode> lookupCodeList = lookupCodeRepository.findAll();
        assertThat(lookupCodeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedonIsRequired() throws Exception {
        int databaseSizeBeforeTest = lookupCodeRepository.findAll().size();
        // set the field null
        lookupCode.setCreatedon(null);

        // Create the LookupCode, which fails.

        restLookupCodeMockMvc.perform(post("/api/lookup-codes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lookupCode)))
            .andExpect(status().isBadRequest());

        List<LookupCode> lookupCodeList = lookupCodeRepository.findAll();
        assertThat(lookupCodeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLookupCodes() throws Exception {
        // Initialize the database
        lookupCodeRepository.saveAndFlush(lookupCode);

        // Get all the lookupCodeList
        restLookupCodeMockMvc.perform(get("/api/lookup-codes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lookupCode.getId().intValue())))
            .andExpect(jsonPath("$.[*].lookupmodule").value(hasItem(DEFAULT_LOOKUPMODULE.toString())))
            .andExpect(jsonPath("$.[*].lookupcode").value(hasItem(DEFAULT_LOOKUPCODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].sortorder").value(hasItem(DEFAULT_SORTORDER)))
            .andExpect(jsonPath("$.[*].lastupdatedby").value(hasItem(DEFAULT_LASTUPDATEDBY)))
            .andExpect(jsonPath("$.[*].createdon").value(hasItem(DEFAULT_CREATEDON.toString())))
            .andExpect(jsonPath("$.[*].lastupdate").value(hasItem(DEFAULT_LASTUPDATE.toString())))
            .andExpect(jsonPath("$.[*].createdby").value(hasItem(DEFAULT_CREATEDBY)));
    }

    @Test
    @Transactional
    public void getLookupCode() throws Exception {
        // Initialize the database
        lookupCodeRepository.saveAndFlush(lookupCode);

        // Get the lookupCode
        restLookupCodeMockMvc.perform(get("/api/lookup-codes/{id}", lookupCode.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(lookupCode.getId().intValue()))
            .andExpect(jsonPath("$.lookupmodule").value(DEFAULT_LOOKUPMODULE.toString()))
            .andExpect(jsonPath("$.lookupcode").value(DEFAULT_LOOKUPCODE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.sortorder").value(DEFAULT_SORTORDER))
            .andExpect(jsonPath("$.lastupdatedby").value(DEFAULT_LASTUPDATEDBY))
            .andExpect(jsonPath("$.createdon").value(DEFAULT_CREATEDON.toString()))
            .andExpect(jsonPath("$.lastupdate").value(DEFAULT_LASTUPDATE.toString()))
            .andExpect(jsonPath("$.createdby").value(DEFAULT_CREATEDBY));
    }

    @Test
    @Transactional
    public void getNonExistingLookupCode() throws Exception {
        // Get the lookupCode
        restLookupCodeMockMvc.perform(get("/api/lookup-codes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLookupCode() throws Exception {
        // Initialize the database
        lookupCodeRepository.saveAndFlush(lookupCode);
        lookupCodeSearchRepository.save(lookupCode);
        int databaseSizeBeforeUpdate = lookupCodeRepository.findAll().size();

        // Update the lookupCode
        LookupCode updatedLookupCode = lookupCodeRepository.findOne(lookupCode.getId());
        updatedLookupCode
            .lookupmodule(UPDATED_LOOKUPMODULE)
            .lookupcode(UPDATED_LOOKUPCODE)
            .description(UPDATED_DESCRIPTION)
            .sortorder(UPDATED_SORTORDER)
            .lastupdatedby(UPDATED_LASTUPDATEDBY)
            .createdon(UPDATED_CREATEDON)
            .lastupdate(UPDATED_LASTUPDATE)
            .createdby(UPDATED_CREATEDBY);

        restLookupCodeMockMvc.perform(put("/api/lookup-codes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLookupCode)))
            .andExpect(status().isOk());

        // Validate the LookupCode in the database
        List<LookupCode> lookupCodeList = lookupCodeRepository.findAll();
        assertThat(lookupCodeList).hasSize(databaseSizeBeforeUpdate);
        LookupCode testLookupCode = lookupCodeList.get(lookupCodeList.size() - 1);
        assertThat(testLookupCode.getLookupmodule()).isEqualTo(UPDATED_LOOKUPMODULE);
        assertThat(testLookupCode.getLookupcode()).isEqualTo(UPDATED_LOOKUPCODE);
        assertThat(testLookupCode.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testLookupCode.getSortorder()).isEqualTo(UPDATED_SORTORDER);
        assertThat(testLookupCode.getLastupdatedby()).isEqualTo(UPDATED_LASTUPDATEDBY);
        assertThat(testLookupCode.getCreatedon()).isEqualTo(UPDATED_CREATEDON);
        assertThat(testLookupCode.getLastupdate()).isEqualTo(UPDATED_LASTUPDATE);
        assertThat(testLookupCode.getCreatedby()).isEqualTo(UPDATED_CREATEDBY);

        // Validate the LookupCode in Elasticsearch
        LookupCode lookupCodeEs = lookupCodeSearchRepository.findOne(testLookupCode.getId());
        assertThat(lookupCodeEs).isEqualToComparingFieldByField(testLookupCode);
    }

    @Test
    @Transactional
    public void updateNonExistingLookupCode() throws Exception {
        int databaseSizeBeforeUpdate = lookupCodeRepository.findAll().size();

        // Create the LookupCode

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLookupCodeMockMvc.perform(put("/api/lookup-codes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(lookupCode)))
            .andExpect(status().isCreated());

        // Validate the LookupCode in the database
        List<LookupCode> lookupCodeList = lookupCodeRepository.findAll();
        assertThat(lookupCodeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteLookupCode() throws Exception {
        // Initialize the database
        lookupCodeRepository.saveAndFlush(lookupCode);
        lookupCodeSearchRepository.save(lookupCode);
        int databaseSizeBeforeDelete = lookupCodeRepository.findAll().size();

        // Get the lookupCode
        restLookupCodeMockMvc.perform(delete("/api/lookup-codes/{id}", lookupCode.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean lookupCodeExistsInEs = lookupCodeSearchRepository.exists(lookupCode.getId());
        assertThat(lookupCodeExistsInEs).isFalse();

        // Validate the database is empty
        List<LookupCode> lookupCodeList = lookupCodeRepository.findAll();
        assertThat(lookupCodeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchLookupCode() throws Exception {
        // Initialize the database
        lookupCodeRepository.saveAndFlush(lookupCode);
        lookupCodeSearchRepository.save(lookupCode);

        // Search the lookupCode
        restLookupCodeMockMvc.perform(get("/api/_search/lookup-codes?query=id:" + lookupCode.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(lookupCode.getId().intValue())))
            .andExpect(jsonPath("$.[*].lookupmodule").value(hasItem(DEFAULT_LOOKUPMODULE.toString())))
            .andExpect(jsonPath("$.[*].lookupcode").value(hasItem(DEFAULT_LOOKUPCODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].sortorder").value(hasItem(DEFAULT_SORTORDER)))
            .andExpect(jsonPath("$.[*].lastupdatedby").value(hasItem(DEFAULT_LASTUPDATEDBY)))
            .andExpect(jsonPath("$.[*].createdon").value(hasItem(DEFAULT_CREATEDON.toString())))
            .andExpect(jsonPath("$.[*].lastupdate").value(hasItem(DEFAULT_LASTUPDATE.toString())))
            .andExpect(jsonPath("$.[*].createdby").value(hasItem(DEFAULT_CREATEDBY)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LookupCode.class);
        LookupCode lookupCode1 = new LookupCode();
        lookupCode1.setId(1L);
        LookupCode lookupCode2 = new LookupCode();
        lookupCode2.setId(lookupCode1.getId());
        assertThat(lookupCode1).isEqualTo(lookupCode2);
        lookupCode2.setId(2L);
        assertThat(lookupCode1).isNotEqualTo(lookupCode2);
        lookupCode1.setId(null);
        assertThat(lookupCode1).isNotEqualTo(lookupCode2);
    }
}
