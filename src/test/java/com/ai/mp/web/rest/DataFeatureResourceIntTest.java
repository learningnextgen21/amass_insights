package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;

import com.ai.mp.domain.DataFeature;
import com.ai.mp.repository.DataFeatureRepository;
import com.ai.mp.repository.search.DataFeatureSearchRepository;
import com.ai.mp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DataFeatureResource REST controller.
 *
 * @see DataFeatureResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class DataFeatureResourceIntTest {

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_DATA_FEATURE = "AAAAAAAAAA";
    private static final String UPDATED_DATA_FEATURE = "BBBBBBBBBB";

    private static final String DEFAULT_EXPLANATION = "AAAAAAAAAA";
    private static final String UPDATED_EXPLANATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_INACTIVE = 2;
    private static final Integer UPDATED_INACTIVE = 1;

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private DataFeatureRepository dataFeatureRepository;

    @Autowired
    private DataFeatureSearchRepository dataFeatureSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDataFeatureMockMvc;

    private DataFeature dataFeature;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DataFeatureResource dataFeatureResource = new DataFeatureResource(dataFeatureRepository, dataFeatureSearchRepository);
        this.restDataFeatureMockMvc = MockMvcBuilders.standaloneSetup(dataFeatureResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataFeature createEntity(EntityManager em) {
        DataFeature dataFeature = new DataFeature()
            .recordID(DEFAULT_RECORD_ID)
            .dataFeature(DEFAULT_DATA_FEATURE)
            .explanation(DEFAULT_EXPLANATION)
            .inactive(DEFAULT_INACTIVE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return dataFeature;
    }

    @Before
    public void initTest() {
        dataFeatureSearchRepository.deleteAll();
        dataFeature = createEntity(em);
    }

    @Test
    @Transactional
    public void createDataFeature() throws Exception {
        int databaseSizeBeforeCreate = dataFeatureRepository.findAll().size();

        // Create the DataFeature
        restDataFeatureMockMvc.perform(post("/api/data-features")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataFeature)))
            .andExpect(status().isCreated());

        // Validate the DataFeature in the database
        List<DataFeature> dataFeatureList = dataFeatureRepository.findAll();
        assertThat(dataFeatureList).hasSize(databaseSizeBeforeCreate + 1);
        DataFeature testDataFeature = dataFeatureList.get(dataFeatureList.size() - 1);
        assertThat(testDataFeature.getRecordID()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testDataFeature.getDataFeature()).isEqualTo(DEFAULT_DATA_FEATURE);
        assertThat(testDataFeature.getExplanation()).isEqualTo(DEFAULT_EXPLANATION);
        assertThat(testDataFeature.getInactive()).isEqualTo(DEFAULT_INACTIVE);
        assertThat(testDataFeature.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDataFeature.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);

        // Validate the DataFeature in Elasticsearch
        DataFeature dataFeatureEs = dataFeatureSearchRepository.findOne(testDataFeature.getId());
        assertThat(dataFeatureEs).isEqualToComparingFieldByField(testDataFeature);
    }

    @Test
    @Transactional
    public void createDataFeatureWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dataFeatureRepository.findAll().size();

        // Create the DataFeature with an existing ID
        dataFeature.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataFeatureMockMvc.perform(post("/api/data-features")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataFeature)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DataFeature> dataFeatureList = dataFeatureRepository.findAll();
        assertThat(dataFeatureList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDataFeatureIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataFeatureRepository.findAll().size();
        // set the field null
        dataFeature.setDataFeature(null);

        // Create the DataFeature, which fails.

        restDataFeatureMockMvc.perform(post("/api/data-features")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataFeature)))
            .andExpect(status().isBadRequest());

        List<DataFeature> dataFeatureList = dataFeatureRepository.findAll();
        assertThat(dataFeatureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInactiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataFeatureRepository.findAll().size();
        // set the field null
        dataFeature.setInactive(null);

        // Create the DataFeature, which fails.

        restDataFeatureMockMvc.perform(post("/api/data-features")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataFeature)))
            .andExpect(status().isBadRequest());

        List<DataFeature> dataFeatureList = dataFeatureRepository.findAll();
        assertThat(dataFeatureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataFeatureRepository.findAll().size();
        // set the field null
        dataFeature.setCreatedDate(null);

        // Create the DataFeature, which fails.

        restDataFeatureMockMvc.perform(post("/api/data-features")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataFeature)))
            .andExpect(status().isBadRequest());

        List<DataFeature> dataFeatureList = dataFeatureRepository.findAll();
        assertThat(dataFeatureList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDataFeatures() throws Exception {
        // Initialize the database
        dataFeatureRepository.saveAndFlush(dataFeature);

        // Get all the dataFeatureList
        restDataFeatureMockMvc.perform(get("/api/data-features?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataFeature.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].dataFeature").value(hasItem(DEFAULT_DATA_FEATURE.toString())))
            .andExpect(jsonPath("$.[*].explanation").value(hasItem(DEFAULT_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getDataFeature() throws Exception {
        // Initialize the database
        dataFeatureRepository.saveAndFlush(dataFeature);

        // Get the dataFeature
        restDataFeatureMockMvc.perform(get("/api/data-features/{id}", dataFeature.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dataFeature.getId().intValue()))
            .andExpect(jsonPath("$.recordID").value(DEFAULT_RECORD_ID.toString()))
            .andExpect(jsonPath("$.dataFeature").value(DEFAULT_DATA_FEATURE.toString()))
            .andExpect(jsonPath("$.explanation").value(DEFAULT_EXPLANATION.toString()))
            .andExpect(jsonPath("$.inactive").value(DEFAULT_INACTIVE))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDataFeature() throws Exception {
        // Get the dataFeature
        restDataFeatureMockMvc.perform(get("/api/data-features/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDataFeature() throws Exception {
        // Initialize the database
        dataFeatureRepository.saveAndFlush(dataFeature);
        dataFeatureSearchRepository.save(dataFeature);
        int databaseSizeBeforeUpdate = dataFeatureRepository.findAll().size();

        // Update the dataFeature
        DataFeature updatedDataFeature = dataFeatureRepository.findOne(dataFeature.getId());
        updatedDataFeature
            .recordID(UPDATED_RECORD_ID)
            .dataFeature(UPDATED_DATA_FEATURE)
            .explanation(UPDATED_EXPLANATION)
            .inactive(UPDATED_INACTIVE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restDataFeatureMockMvc.perform(put("/api/data-features")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDataFeature)))
            .andExpect(status().isOk());

        // Validate the DataFeature in the database
        List<DataFeature> dataFeatureList = dataFeatureRepository.findAll();
        assertThat(dataFeatureList).hasSize(databaseSizeBeforeUpdate);
        DataFeature testDataFeature = dataFeatureList.get(dataFeatureList.size() - 1);
        assertThat(testDataFeature.getRecordID()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testDataFeature.getDataFeature()).isEqualTo(UPDATED_DATA_FEATURE);
        assertThat(testDataFeature.getExplanation()).isEqualTo(UPDATED_EXPLANATION);
        assertThat(testDataFeature.getInactive()).isEqualTo(UPDATED_INACTIVE);
        assertThat(testDataFeature.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDataFeature.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);

        // Validate the DataFeature in Elasticsearch
        DataFeature dataFeatureEs = dataFeatureSearchRepository.findOne(testDataFeature.getId());
        assertThat(dataFeatureEs).isEqualToComparingFieldByField(testDataFeature);
    }

    @Test
    @Transactional
    public void updateNonExistingDataFeature() throws Exception {
        int databaseSizeBeforeUpdate = dataFeatureRepository.findAll().size();

        // Create the DataFeature

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDataFeatureMockMvc.perform(put("/api/data-features")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataFeature)))
            .andExpect(status().isCreated());

        // Validate the DataFeature in the database
        List<DataFeature> dataFeatureList = dataFeatureRepository.findAll();
        assertThat(dataFeatureList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDataFeature() throws Exception {
        // Initialize the database
        dataFeatureRepository.saveAndFlush(dataFeature);
        dataFeatureSearchRepository.save(dataFeature);
        int databaseSizeBeforeDelete = dataFeatureRepository.findAll().size();

        // Get the dataFeature
        restDataFeatureMockMvc.perform(delete("/api/data-features/{id}", dataFeature.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dataFeatureExistsInEs = dataFeatureSearchRepository.exists(dataFeature.getId());
        assertThat(dataFeatureExistsInEs).isFalse();

        // Validate the database is empty
        List<DataFeature> dataFeatureList = dataFeatureRepository.findAll();
        assertThat(dataFeatureList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDataFeature() throws Exception {
        // Initialize the database
        dataFeatureRepository.saveAndFlush(dataFeature);
        dataFeatureSearchRepository.save(dataFeature);

        // Search the dataFeature
        restDataFeatureMockMvc.perform(get("/api/_search/data-features?query=id:" + dataFeature.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataFeature.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].dataFeature").value(hasItem(DEFAULT_DATA_FEATURE.toString())))
            .andExpect(jsonPath("$.[*].explanation").value(hasItem(DEFAULT_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataFeature.class);
        DataFeature dataFeature1 = new DataFeature();
        dataFeature1.setId(1L);
        DataFeature dataFeature2 = new DataFeature();
        dataFeature2.setId(dataFeature1.getId());
        assertThat(dataFeature1).isEqualTo(dataFeature2);
        dataFeature2.setId(2L);
        assertThat(dataFeature1).isNotEqualTo(dataFeature2);
        dataFeature1.setId(null);
        assertThat(dataFeature1).isNotEqualTo(dataFeature2);
    }
}
