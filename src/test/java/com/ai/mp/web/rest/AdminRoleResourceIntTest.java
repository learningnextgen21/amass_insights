package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;

import com.ai.mp.domain.AdminRole;
import com.ai.mp.repository.AdminRoleRepository;
import com.ai.mp.repository.search.AdminRoleSearchRepository;
import com.ai.mp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AdminRoleResource REST controller.
 *
 * @see AdminRoleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class AdminRoleResourceIntTest {

    private static final Integer DEFAULT_USER_ID = 11;
    private static final Integer UPDATED_USER_ID = 10;

    private static final String DEFAULT_ADMIN_ROLE = "AAAAAAAAAA";
    private static final String UPDATED_ADMIN_ROLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private AdminRoleRepository adminRoleRepository;

    @Autowired
    private AdminRoleSearchRepository adminRoleSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAdminRoleMockMvc;

    private AdminRole adminRole;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdminRoleResource adminRoleResource = new AdminRoleResource(adminRoleRepository, adminRoleSearchRepository);
        this.restAdminRoleMockMvc = MockMvcBuilders.standaloneSetup(adminRoleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdminRole createEntity(EntityManager em) {
        AdminRole adminRole = new AdminRole()
            .userID(DEFAULT_USER_ID)
            .adminRole(DEFAULT_ADMIN_ROLE)
            .description(DEFAULT_DESCRIPTION);
        return adminRole;
    }

    @Before
    public void initTest() {
        adminRoleSearchRepository.deleteAll();
        adminRole = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdminRole() throws Exception {
        int databaseSizeBeforeCreate = adminRoleRepository.findAll().size();

        // Create the AdminRole
        restAdminRoleMockMvc.perform(post("/api/admin-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adminRole)))
            .andExpect(status().isCreated());

        // Validate the AdminRole in the database
        List<AdminRole> adminRoleList = adminRoleRepository.findAll();
        assertThat(adminRoleList).hasSize(databaseSizeBeforeCreate + 1);
        AdminRole testAdminRole = adminRoleList.get(adminRoleList.size() - 1);
        assertThat(testAdminRole.getUserID()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testAdminRole.getAdminRole()).isEqualTo(DEFAULT_ADMIN_ROLE);
        assertThat(testAdminRole.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the AdminRole in Elasticsearch
        AdminRole adminRoleEs = adminRoleSearchRepository.findOne(testAdminRole.getId());
        assertThat(adminRoleEs).isEqualToComparingFieldByField(testAdminRole);
    }

    @Test
    @Transactional
    public void createAdminRoleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = adminRoleRepository.findAll().size();

        // Create the AdminRole with an existing ID
        adminRole.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdminRoleMockMvc.perform(post("/api/admin-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adminRole)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<AdminRole> adminRoleList = adminRoleRepository.findAll();
        assertThat(adminRoleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUserIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = adminRoleRepository.findAll().size();
        // set the field null
        adminRole.setUserID(null);

        // Create the AdminRole, which fails.

        restAdminRoleMockMvc.perform(post("/api/admin-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adminRole)))
            .andExpect(status().isBadRequest());

        List<AdminRole> adminRoleList = adminRoleRepository.findAll();
        assertThat(adminRoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAdminRoleIsRequired() throws Exception {
        int databaseSizeBeforeTest = adminRoleRepository.findAll().size();
        // set the field null
        adminRole.setAdminRole(null);

        // Create the AdminRole, which fails.

        restAdminRoleMockMvc.perform(post("/api/admin-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adminRole)))
            .andExpect(status().isBadRequest());

        List<AdminRole> adminRoleList = adminRoleRepository.findAll();
        assertThat(adminRoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = adminRoleRepository.findAll().size();
        // set the field null
        adminRole.setDescription(null);

        // Create the AdminRole, which fails.

        restAdminRoleMockMvc.perform(post("/api/admin-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adminRole)))
            .andExpect(status().isBadRequest());

        List<AdminRole> adminRoleList = adminRoleRepository.findAll();
        assertThat(adminRoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAdminRoles() throws Exception {
        // Initialize the database
        adminRoleRepository.saveAndFlush(adminRole);

        // Get all the adminRoleList
        restAdminRoleMockMvc.perform(get("/api/admin-roles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(adminRole.getId().intValue())))
            .andExpect(jsonPath("$.[*].userID").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].adminRole").value(hasItem(DEFAULT_ADMIN_ROLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getAdminRole() throws Exception {
        // Initialize the database
        adminRoleRepository.saveAndFlush(adminRole);

        // Get the adminRole
        restAdminRoleMockMvc.perform(get("/api/admin-roles/{id}", adminRole.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(adminRole.getId().intValue()))
            .andExpect(jsonPath("$.userID").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.adminRole").value(DEFAULT_ADMIN_ROLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAdminRole() throws Exception {
        // Get the adminRole
        restAdminRoleMockMvc.perform(get("/api/admin-roles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdminRole() throws Exception {
        // Initialize the database
        adminRoleRepository.saveAndFlush(adminRole);
        adminRoleSearchRepository.save(adminRole);
        int databaseSizeBeforeUpdate = adminRoleRepository.findAll().size();

        // Update the adminRole
        AdminRole updatedAdminRole = adminRoleRepository.findOne(adminRole.getId());
        updatedAdminRole
            .userID(UPDATED_USER_ID)
            .adminRole(UPDATED_ADMIN_ROLE)
            .description(UPDATED_DESCRIPTION);

        restAdminRoleMockMvc.perform(put("/api/admin-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAdminRole)))
            .andExpect(status().isOk());

        // Validate the AdminRole in the database
        List<AdminRole> adminRoleList = adminRoleRepository.findAll();
        assertThat(adminRoleList).hasSize(databaseSizeBeforeUpdate);
        AdminRole testAdminRole = adminRoleList.get(adminRoleList.size() - 1);
        assertThat(testAdminRole.getUserID()).isEqualTo(UPDATED_USER_ID);
        assertThat(testAdminRole.getAdminRole()).isEqualTo(UPDATED_ADMIN_ROLE);
        assertThat(testAdminRole.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the AdminRole in Elasticsearch
        AdminRole adminRoleEs = adminRoleSearchRepository.findOne(testAdminRole.getId());
        assertThat(adminRoleEs).isEqualToComparingFieldByField(testAdminRole);
    }

    @Test
    @Transactional
    public void updateNonExistingAdminRole() throws Exception {
        int databaseSizeBeforeUpdate = adminRoleRepository.findAll().size();

        // Create the AdminRole

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAdminRoleMockMvc.perform(put("/api/admin-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(adminRole)))
            .andExpect(status().isCreated());

        // Validate the AdminRole in the database
        List<AdminRole> adminRoleList = adminRoleRepository.findAll();
        assertThat(adminRoleList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAdminRole() throws Exception {
        // Initialize the database
        adminRoleRepository.saveAndFlush(adminRole);
        adminRoleSearchRepository.save(adminRole);
        int databaseSizeBeforeDelete = adminRoleRepository.findAll().size();

        // Get the adminRole
        restAdminRoleMockMvc.perform(delete("/api/admin-roles/{id}", adminRole.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean adminRoleExistsInEs = adminRoleSearchRepository.exists(adminRole.getId());
        assertThat(adminRoleExistsInEs).isFalse();

        // Validate the database is empty
        List<AdminRole> adminRoleList = adminRoleRepository.findAll();
        assertThat(adminRoleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAdminRole() throws Exception {
        // Initialize the database
        adminRoleRepository.saveAndFlush(adminRole);
        adminRoleSearchRepository.save(adminRole);

        // Search the adminRole
        restAdminRoleMockMvc.perform(get("/api/_search/admin-roles?query=id:" + adminRole.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(adminRole.getId().intValue())))
            .andExpect(jsonPath("$.[*].userID").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].adminRole").value(hasItem(DEFAULT_ADMIN_ROLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdminRole.class);
        AdminRole adminRole1 = new AdminRole();
        adminRole1.setId(1L);
        AdminRole adminRole2 = new AdminRole();
        adminRole2.setId(adminRole1.getId());
        assertThat(adminRole1).isEqualTo(adminRole2);
        adminRole2.setId(2L);
        assertThat(adminRole1).isNotEqualTo(adminRole2);
        adminRole1.setId(null);
        assertThat(adminRole1).isNotEqualTo(adminRole2);
    }
}
