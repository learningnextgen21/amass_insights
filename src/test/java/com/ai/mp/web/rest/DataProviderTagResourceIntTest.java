package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;

import com.ai.mp.batch.DBUtils;
import com.ai.mp.domain.DataProviderTag;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.repository.DataProviderTagRepository;
import com.ai.mp.repository.search.DataProviderSearchRepository;
import com.ai.mp.repository.search.DataProviderTagSearchRepository;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.service.UserService;
import com.ai.mp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DataProviderTagResource REST controller.
 *
 * @see DataProviderTagResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class DataProviderTagResourceIntTest {

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_PROVIDER_TAG = "AAAAAAAAAA";
    private static final String UPDATED_PROVIDER_TAG = "BBBBBBBBBB";

    private static final String DEFAULT_EXPLANATION = "AAAAAAAAAA";
    private static final String UPDATED_EXPLANATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_INACTIVE = 2;
    private static final Integer UPDATED_INACTIVE = 1;

    private static final boolean DEFAULT_TAG_INACTIVE = false;

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private UserService userService;

    @Autowired
    private DBUtils dbutils;

    @Autowired
    private ElasticsearchIndexService elasticsearchIndexService;

    @Autowired
    private DataProviderTagRepository dataProviderTagRepository;



    @Autowired
    private final DataProviderRepository dataProviderRepository;

    @Autowired
    private final DataProviderSearchRepository dataProviderSearchRepository;


    @Autowired
    private DataProviderTagSearchRepository dataProviderTagSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDataProviderTagMockMvc;

    private DataProviderTag dataProviderTag;

    public DataProviderTagResourceIntTest(DataProviderRepository dataProviderRepository, DataProviderSearchRepository dataProviderSearchRepository) {
        this.dataProviderRepository = dataProviderRepository;
        this.dataProviderSearchRepository = dataProviderSearchRepository;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DataProviderTagResource dataProviderTagResource = new DataProviderTagResource(userService, dataProviderTagRepository, elasticsearchIndexService,dataProviderSearchRepository,dataProviderRepository, dataProviderTagSearchRepository,dbutils);
        this.restDataProviderTagMockMvc = MockMvcBuilders.standaloneSetup(dataProviderTagResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataProviderTag createEntity(EntityManager em) {
        DataProviderTag dataProviderTag = new DataProviderTag()
            .recordID(DEFAULT_RECORD_ID)
            .providerTag(DEFAULT_PROVIDER_TAG)
            .explanation(DEFAULT_EXPLANATION)
           // .inactive(DEFAULT_TAG_INACTIVE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return dataProviderTag;
    }

    @Before
    public void initTest() {
        dataProviderTagSearchRepository.deleteAll();
        dataProviderTag = createEntity(em);
    }

    @Test
    @Transactional
    public void createDataProviderTag() throws Exception {
        int databaseSizeBeforeCreate = dataProviderTagRepository.findAll().size();

        // Create the DataProviderTag
        restDataProviderTagMockMvc.perform(post("/api/data-provider-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProviderTag)))
            .andExpect(status().isCreated());

        // Validate the DataProviderTag in the database
        List<DataProviderTag> dataProviderTagList = dataProviderTagRepository.findAll();
        assertThat(dataProviderTagList).hasSize(databaseSizeBeforeCreate + 1);
        DataProviderTag testDataProviderTag = dataProviderTagList.get(dataProviderTagList.size() - 1);
        assertThat(testDataProviderTag.getRecordID()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testDataProviderTag.getProviderTag()).isEqualTo(DEFAULT_PROVIDER_TAG);
        assertThat(testDataProviderTag.getExplanation()).isEqualTo(DEFAULT_EXPLANATION);
        assertThat(testDataProviderTag.getInactive()).isEqualTo(DEFAULT_INACTIVE);
        assertThat(testDataProviderTag.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDataProviderTag.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);

        // Validate the DataProviderTag in Elasticsearch
        DataProviderTag dataProviderTagEs = dataProviderTagSearchRepository.findOne(testDataProviderTag.getId());
        assertThat(dataProviderTagEs).isEqualToComparingFieldByField(testDataProviderTag);
    }

    @Test
    @Transactional
    public void createDataProviderTagWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dataProviderTagRepository.findAll().size();

        // Create the DataProviderTag with an existing ID
        dataProviderTag.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataProviderTagMockMvc.perform(post("/api/data-provider-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProviderTag)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DataProviderTag> dataProviderTagList = dataProviderTagRepository.findAll();
        assertThat(dataProviderTagList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkProviderTagIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderTagRepository.findAll().size();
        // set the field null
        dataProviderTag.setProviderTag(null);

        // Create the DataProviderTag, which fails.

        restDataProviderTagMockMvc.perform(post("/api/data-provider-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProviderTag)))
            .andExpect(status().isBadRequest());

        List<DataProviderTag> dataProviderTagList = dataProviderTagRepository.findAll();
        assertThat(dataProviderTagList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderTagRepository.findAll().size();
        // set the field null
        dataProviderTag.setCreatedDate(null);

        // Create the DataProviderTag, which fails.

        restDataProviderTagMockMvc.perform(post("/api/data-provider-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProviderTag)))
            .andExpect(status().isBadRequest());

        List<DataProviderTag> dataProviderTagList = dataProviderTagRepository.findAll();
        assertThat(dataProviderTagList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDataProviderTags() throws Exception {
        // Initialize the database
        dataProviderTagRepository.saveAndFlush(dataProviderTag);

        // Get all the dataProviderTagList
        restDataProviderTagMockMvc.perform(get("/api/data-provider-tags?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataProviderTag.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].providerTag").value(hasItem(DEFAULT_PROVIDER_TAG.toString())))
            .andExpect(jsonPath("$.[*].explanation").value(hasItem(DEFAULT_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getDataProviderTag() throws Exception {
        // Initialize the database
        dataProviderTagRepository.saveAndFlush(dataProviderTag);

        // Get the dataProviderTag
        restDataProviderTagMockMvc.perform(get("/api/data-provider-tags/{id}", dataProviderTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dataProviderTag.getId().intValue()))
            .andExpect(jsonPath("$.recordID").value(DEFAULT_RECORD_ID.toString()))
            .andExpect(jsonPath("$.providerTag").value(DEFAULT_PROVIDER_TAG.toString()))
            .andExpect(jsonPath("$.explanation").value(DEFAULT_EXPLANATION.toString()))
            .andExpect(jsonPath("$.inactive").value(DEFAULT_INACTIVE))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDataProviderTag() throws Exception {
        // Get the dataProviderTag
        restDataProviderTagMockMvc.perform(get("/api/data-provider-tags/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDataProviderTag() throws Exception {
        // Initialize the database
        dataProviderTagRepository.saveAndFlush(dataProviderTag);
        dataProviderTagSearchRepository.save(dataProviderTag);
        int databaseSizeBeforeUpdate = dataProviderTagRepository.findAll().size();

        // Update the dataProviderTag
        DataProviderTag updatedDataProviderTag = dataProviderTagRepository.findOne(dataProviderTag.getId());
        updatedDataProviderTag
            .recordID(UPDATED_RECORD_ID)
            .providerTag(UPDATED_PROVIDER_TAG)
            .explanation(UPDATED_EXPLANATION)
            //.inactive(DEFAULT_TAG_INACTIVE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restDataProviderTagMockMvc.perform(put("/api/data-provider-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDataProviderTag)))
            .andExpect(status().isOk());

        // Validate the DataProviderTag in the database
        List<DataProviderTag> dataProviderTagList = dataProviderTagRepository.findAll();
        assertThat(dataProviderTagList).hasSize(databaseSizeBeforeUpdate);
        DataProviderTag testDataProviderTag = dataProviderTagList.get(dataProviderTagList.size() - 1);
        assertThat(testDataProviderTag.getRecordID()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testDataProviderTag.getProviderTag()).isEqualTo(UPDATED_PROVIDER_TAG);
        assertThat(testDataProviderTag.getExplanation()).isEqualTo(UPDATED_EXPLANATION);
        assertThat(testDataProviderTag.getInactive()).isEqualTo(UPDATED_INACTIVE);
        assertThat(testDataProviderTag.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDataProviderTag.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);

        // Validate the DataProviderTag in Elasticsearch
        DataProviderTag dataProviderTagEs = dataProviderTagSearchRepository.findOne(testDataProviderTag.getId());
        assertThat(dataProviderTagEs).isEqualToComparingFieldByField(testDataProviderTag);
    }

    @Test
    @Transactional
    public void updateNonExistingDataProviderTag() throws Exception {
        int databaseSizeBeforeUpdate = dataProviderTagRepository.findAll().size();

        // Create the DataProviderTag

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDataProviderTagMockMvc.perform(put("/api/data-provider-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProviderTag)))
            .andExpect(status().isCreated());

        // Validate the DataProviderTag in the database
        List<DataProviderTag> dataProviderTagList = dataProviderTagRepository.findAll();
        assertThat(dataProviderTagList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDataProviderTag() throws Exception {
        // Initialize the database
        dataProviderTagRepository.saveAndFlush(dataProviderTag);
        dataProviderTagSearchRepository.save(dataProviderTag);
        int databaseSizeBeforeDelete = dataProviderTagRepository.findAll().size();

        // Get the dataProviderTag
        restDataProviderTagMockMvc.perform(delete("/api/data-provider-tags/{id}", dataProviderTag.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dataProviderTagExistsInEs = dataProviderTagSearchRepository.exists(dataProviderTag.getId());
        assertThat(dataProviderTagExistsInEs).isFalse();

        // Validate the database is empty
        List<DataProviderTag> dataProviderTagList = dataProviderTagRepository.findAll();
        assertThat(dataProviderTagList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDataProviderTag() throws Exception {
        // Initialize the database
        dataProviderTagRepository.saveAndFlush(dataProviderTag);
        dataProviderTagSearchRepository.save(dataProviderTag);

        // Search the dataProviderTag
        restDataProviderTagMockMvc.perform(get("/api/_search/data-provider-tags?query=id:" + dataProviderTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataProviderTag.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].providerTag").value(hasItem(DEFAULT_PROVIDER_TAG.toString())))
            .andExpect(jsonPath("$.[*].explanation").value(hasItem(DEFAULT_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataProviderTag.class);
        DataProviderTag dataProviderTag1 = new DataProviderTag();
        dataProviderTag1.setId(1L);
        DataProviderTag dataProviderTag2 = new DataProviderTag();
        dataProviderTag2.setId(dataProviderTag1.getId());
        assertThat(dataProviderTag1).isEqualTo(dataProviderTag2);
        dataProviderTag2.setId(2L);
        assertThat(dataProviderTag1).isNotEqualTo(dataProviderTag2);
        dataProviderTag1.setId(null);
        assertThat(dataProviderTag1).isNotEqualTo(dataProviderTag2);
    }
}
