package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;
import com.ai.mp.domain.UserProvider;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.repository.UserProviderRepository;
import com.ai.mp.repository.search.UserProviderSearchRepository;
import com.ai.mp.service.UserProviderService;
import com.ai.mp.service.UserService;
import com.ai.mp.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import com.ai.mp.repository.DataProviderRepository;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AmassUserProviderResource REST controller.
 *
 * @see UserProviderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class AmassUserProviderResourceIntTest {

    private static final Long DEFAULT_USER_ID = 11L;
    private static final Long UPDATED_USER_ID = 10L;

    private static final Long DEFAULT_PROVIDER_ID = 11L;
    private static final Long UPDATED_PROVIDER_ID = 10L;

    private static final Integer DEFAULT_GET_NOTIFICATIONS = 2;
    private static final Integer UPDATED_GET_NOTIFICATIONS = 1;

    private static final Integer DEFAULT_NOTIFICATION_TYPE = 11;
    private static final Integer UPDATED_NOTIFICATION_TYPE = 10;

    private static final Integer DEFAULT_INTEREST_TYPE = 11;
    private static final Integer UPDATED_INTEREST_TYPE = 10;

    private static final boolean DEFAULT_INACTIVE = false;
    private static final boolean UPDATED_INACTIVE = true;

    private static final LocalDateTime DEFAULT_CREATED_DATE = LocalDateTime.now();
    private static final LocalDateTime UPDATED_CREATED_DATE = LocalDateTime.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private UserProviderRepository amassUserProviderRepository;

    @Autowired
    private  UserService userService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;
    @Autowired
    private UserProviderSearchRepository amassUserProviderSearchRepository;
    
    @Autowired
    private EntityManager em;

    private MockMvc restAmassUserProviderMockMvc;

    private UserProvider amassUserProvider;

    private  UserProviderRepository userProviderRepository;
    private  UserProviderService userProviderService;
    private UserProviderSearchRepository userProviderSearchRepository;
    private DataProviderRepository dataProviderRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserProviderResource amassUserProviderResource = new UserProviderResource(dataProviderRepository, userProviderRepository,  userProviderService,  userProviderSearchRepository,userService);
        this.restAmassUserProviderMockMvc = MockMvcBuilders.standaloneSetup(amassUserProviderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserProvider createEntity(EntityManager em) {
        UserProvider amassUserProvider = new UserProvider()
            .userID(DEFAULT_USER_ID)
            .providerID(DEFAULT_PROVIDER_ID)
            .getNotifications(DEFAULT_GET_NOTIFICATIONS)
            .notificationType(DEFAULT_NOTIFICATION_TYPE)
            .interestType(DEFAULT_INTEREST_TYPE)
            .inactive(DEFAULT_INACTIVE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return amassUserProvider;
    }

    @Before
    public void initTest() {
        amassUserProviderSearchRepository.deleteAll();
        amassUserProvider = createEntity(em);
    }

    @Test
    @Transactional
    public void createAmassUserProvider() throws Exception {
        int databaseSizeBeforeCreate = amassUserProviderRepository.findAll().size();

        // Create the AmassUserProvider
        restAmassUserProviderMockMvc.perform(post("/api/amass-user-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserProvider)))
            .andExpect(status().isCreated());

        // Validate the AmassUserProvider in the database
        List<UserProvider> amassUserProviderList = amassUserProviderRepository.findAll();
        assertThat(amassUserProviderList).hasSize(databaseSizeBeforeCreate + 1);
        UserProvider testAmassUserProvider = amassUserProviderList.get(amassUserProviderList.size() - 1);
        assertThat(testAmassUserProvider.getUserID()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testAmassUserProvider.getProviderID()).isEqualTo(DEFAULT_PROVIDER_ID);
        assertThat(testAmassUserProvider.getGetNotifications()).isEqualTo(DEFAULT_GET_NOTIFICATIONS);
        assertThat(testAmassUserProvider.getNotificationType()).isEqualTo(DEFAULT_NOTIFICATION_TYPE);
        assertThat(testAmassUserProvider.getInterestType()).isEqualTo(DEFAULT_INTEREST_TYPE);
        assertThat(testAmassUserProvider.getInactive()).isEqualTo(DEFAULT_INACTIVE);
        assertThat(testAmassUserProvider.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testAmassUserProvider.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);

        // Validate the AmassUserProvider in Elasticsearch
        UserProvider amassUserProviderEs = amassUserProviderSearchRepository.findOne(testAmassUserProvider.getId());
        assertThat(amassUserProviderEs).isEqualToComparingFieldByField(testAmassUserProvider);
    }

    @Test
    @Transactional
    public void createAmassUserProviderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = amassUserProviderRepository.findAll().size();

        // Create the AmassUserProvider with an existing ID
        amassUserProvider.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAmassUserProviderMockMvc.perform(post("/api/amass-user-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserProvider)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<UserProvider> amassUserProviderList = amassUserProviderRepository.findAll();
        assertThat(amassUserProviderList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUserIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserProviderRepository.findAll().size();
        // set the field null
        amassUserProvider.setUserID(null);

        // Create the AmassUserProvider, which fails.

        restAmassUserProviderMockMvc.perform(post("/api/amass-user-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserProvider)))
            .andExpect(status().isBadRequest());

        List<UserProvider> amassUserProviderList = amassUserProviderRepository.findAll();
        assertThat(amassUserProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProviderIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserProviderRepository.findAll().size();
        // set the field null
        amassUserProvider.setProviderID(null);

        // Create the AmassUserProvider, which fails.

        restAmassUserProviderMockMvc.perform(post("/api/amass-user-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserProvider)))
            .andExpect(status().isBadRequest());

        List<UserProvider> amassUserProviderList = amassUserProviderRepository.findAll();
        assertThat(amassUserProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGetNotificationsIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserProviderRepository.findAll().size();
        // set the field null
        amassUserProvider.setGetNotifications(null);

        // Create the AmassUserProvider, which fails.

        restAmassUserProviderMockMvc.perform(post("/api/amass-user-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserProvider)))
            .andExpect(status().isBadRequest());

        List<UserProvider> amassUserProviderList = amassUserProviderRepository.findAll();
        assertThat(amassUserProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNotificationTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserProviderRepository.findAll().size();
        // set the field null
        amassUserProvider.setNotificationType(null);

        // Create the AmassUserProvider, which fails.

        restAmassUserProviderMockMvc.perform(post("/api/amass-user-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserProvider)))
            .andExpect(status().isBadRequest());

        List<UserProvider> amassUserProviderList = amassUserProviderRepository.findAll();
        assertThat(amassUserProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInterestTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserProviderRepository.findAll().size();
        // set the field null
        amassUserProvider.setInterestType(null);

        // Create the AmassUserProvider, which fails.

        restAmassUserProviderMockMvc.perform(post("/api/amass-user-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserProvider)))
            .andExpect(status().isBadRequest());

        List<UserProvider> amassUserProviderList = amassUserProviderRepository.findAll();
        assertThat(amassUserProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInactiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserProviderRepository.findAll().size();
        // set the field null
        amassUserProvider.setInactive(false);

        // Create the AmassUserProvider, which fails.

        restAmassUserProviderMockMvc.perform(post("/api/amass-user-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserProvider)))
            .andExpect(status().isBadRequest());

        List<UserProvider> amassUserProviderList = amassUserProviderRepository.findAll();
        assertThat(amassUserProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserProviderRepository.findAll().size();
        // set the field null
        amassUserProvider.setCreatedDate(null);

        // Create the AmassUserProvider, which fails.

        restAmassUserProviderMockMvc.perform(post("/api/amass-user-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserProvider)))
            .andExpect(status().isBadRequest());

        List<UserProvider> amassUserProviderList = amassUserProviderRepository.findAll();
        assertThat(amassUserProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAmassUserProviders() throws Exception {
        // Initialize the database
        amassUserProviderRepository.saveAndFlush(amassUserProvider);

        // Get all the amassUserProviderList
        restAmassUserProviderMockMvc.perform(get("/api/amass-user-providers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(amassUserProvider.getId().intValue())))
            .andExpect(jsonPath("$.[*].userID").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].providerID").value(hasItem(DEFAULT_PROVIDER_ID)))
            .andExpect(jsonPath("$.[*].getNotifications").value(hasItem(DEFAULT_GET_NOTIFICATIONS)))
            .andExpect(jsonPath("$.[*].notificationType").value(hasItem(DEFAULT_NOTIFICATION_TYPE)))
            .andExpect(jsonPath("$.[*].interestType").value(hasItem(DEFAULT_INTEREST_TYPE)))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getAmassUserProvider() throws Exception {
        // Initialize the database
        amassUserProviderRepository.saveAndFlush(amassUserProvider);

        // Get the amassUserProvider
        restAmassUserProviderMockMvc.perform(get("/api/amass-user-providers/{id}", amassUserProvider.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(amassUserProvider.getId().intValue()))
            .andExpect(jsonPath("$.userID").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.providerID").value(DEFAULT_PROVIDER_ID))
            .andExpect(jsonPath("$.getNotifications").value(DEFAULT_GET_NOTIFICATIONS))
            .andExpect(jsonPath("$.notificationType").value(DEFAULT_NOTIFICATION_TYPE))
            .andExpect(jsonPath("$.interestType").value(DEFAULT_INTEREST_TYPE))
            .andExpect(jsonPath("$.inactive").value(DEFAULT_INACTIVE))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAmassUserProvider() throws Exception {
        // Get the amassUserProvider
        restAmassUserProviderMockMvc.perform(get("/api/amass-user-providers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAmassUserProvider() throws Exception {
        // Initialize the database
        amassUserProviderRepository.saveAndFlush(amassUserProvider);
        amassUserProviderSearchRepository.save(amassUserProvider);
        int databaseSizeBeforeUpdate = amassUserProviderRepository.findAll().size();

        // Update the amassUserProvider
        UserProvider updatedAmassUserProvider = amassUserProviderRepository.findOne(amassUserProvider.getId());
        updatedAmassUserProvider
            .userID(UPDATED_USER_ID)
            .providerID(UPDATED_PROVIDER_ID)
            .getNotifications(UPDATED_GET_NOTIFICATIONS)
            .notificationType(UPDATED_NOTIFICATION_TYPE)
            .interestType(UPDATED_INTEREST_TYPE)
            .inactive(UPDATED_INACTIVE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restAmassUserProviderMockMvc.perform(put("/api/amass-user-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAmassUserProvider)))
            .andExpect(status().isOk());

        // Validate the AmassUserProvider in the database
        List<UserProvider> amassUserProviderList = amassUserProviderRepository.findAll();
        assertThat(amassUserProviderList).hasSize(databaseSizeBeforeUpdate);
        UserProvider testAmassUserProvider = amassUserProviderList.get(amassUserProviderList.size() - 1);
        assertThat(testAmassUserProvider.getUserID()).isEqualTo(UPDATED_USER_ID);
        assertThat(testAmassUserProvider.getProviderID()).isEqualTo(UPDATED_PROVIDER_ID);
        assertThat(testAmassUserProvider.getGetNotifications()).isEqualTo(UPDATED_GET_NOTIFICATIONS);
        assertThat(testAmassUserProvider.getNotificationType()).isEqualTo(UPDATED_NOTIFICATION_TYPE);
        assertThat(testAmassUserProvider.getInterestType()).isEqualTo(UPDATED_INTEREST_TYPE);
        assertThat(testAmassUserProvider.getInactive()).isEqualTo(UPDATED_INACTIVE);
        assertThat(testAmassUserProvider.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testAmassUserProvider.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);

        // Validate the AmassUserProvider in Elasticsearch
        UserProvider amassUserProviderEs = amassUserProviderSearchRepository.findOne(testAmassUserProvider.getId());
        assertThat(amassUserProviderEs).isEqualToComparingFieldByField(testAmassUserProvider);
    }

    @Test
    @Transactional
    public void updateNonExistingAmassUserProvider() throws Exception {
        int databaseSizeBeforeUpdate = amassUserProviderRepository.findAll().size();

        // Create the AmassUserProvider

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAmassUserProviderMockMvc.perform(put("/api/amass-user-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserProvider)))
            .andExpect(status().isCreated());

        // Validate the AmassUserProvider in the database
        List<UserProvider> amassUserProviderList = amassUserProviderRepository.findAll();
        assertThat(amassUserProviderList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAmassUserProvider() throws Exception {
        // Initialize the database
        amassUserProviderRepository.saveAndFlush(amassUserProvider);
        amassUserProviderSearchRepository.save(amassUserProvider);
        int databaseSizeBeforeDelete = amassUserProviderRepository.findAll().size();

        // Get the amassUserProvider
        restAmassUserProviderMockMvc.perform(delete("/api/amass-user-providers/{id}", amassUserProvider.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean amassUserProviderExistsInEs = amassUserProviderSearchRepository.exists(amassUserProvider.getId());
        assertThat(amassUserProviderExistsInEs).isFalse();

        // Validate the database is empty
        List<UserProvider> amassUserProviderList = amassUserProviderRepository.findAll();
        assertThat(amassUserProviderList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAmassUserProvider() throws Exception {
        // Initialize the database
        amassUserProviderRepository.saveAndFlush(amassUserProvider);
        amassUserProviderSearchRepository.save(amassUserProvider);

        // Search the amassUserProvider
        restAmassUserProviderMockMvc.perform(get("/api/_search/amass-user-providers?query=id:" + amassUserProvider.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(amassUserProvider.getId().intValue())))
            .andExpect(jsonPath("$.[*].userID").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].providerID").value(hasItem(DEFAULT_PROVIDER_ID)))
            .andExpect(jsonPath("$.[*].getNotifications").value(hasItem(DEFAULT_GET_NOTIFICATIONS)))
            .andExpect(jsonPath("$.[*].notificationType").value(hasItem(DEFAULT_NOTIFICATION_TYPE)))
            .andExpect(jsonPath("$.[*].interestType").value(hasItem(DEFAULT_INTEREST_TYPE)))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserProvider.class);
        UserProvider amassUserProvider1 = new UserProvider();
        amassUserProvider1.setId(1L);
        UserProvider amassUserProvider2 = new UserProvider();
        amassUserProvider2.setId(amassUserProvider1.getId());
        assertThat(amassUserProvider1).isEqualTo(amassUserProvider2);
        amassUserProvider2.setId(2L);
        assertThat(amassUserProvider1).isNotEqualTo(amassUserProvider2);
        amassUserProvider1.setId(null);
        assertThat(amassUserProvider1).isNotEqualTo(amassUserProvider2);
    }
}
