package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;

import com.ai.mp.domain.DataCategory;
import com.ai.mp.repository.CategoryLogoRepository;
import com.ai.mp.repository.DataCategoryRepository;
import com.ai.mp.repository.search.DataCategorySearchRepository;
import com.ai.mp.service.UserService;
import com.ai.mp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DataCategoryResource REST controller.
 *
 * @see DataCategoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class DataCategoryResourceIntTest {

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_DATACATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_DATACATEGORY = "BBBBBBBBBB";

    private static final String DEFAULT_EXPLANATION = "AAAAAAAAAA";
    private static final String UPDATED_EXPLANATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_INACTIVE = 2;
    private static final Integer UPDATED_INACTIVE = 1;

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private DataCategoryRepository dataCategoryRepository;

    @Autowired
    private DataCategorySearchRepository dataCategorySearchRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CategoryLogoRepository categoryLogoRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDataCategoryMockMvc;

    private DataCategory dataCategory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DataCategoryResource dataCategoryResource = new DataCategoryResource(dataCategoryRepository, dataCategorySearchRepository, userService,categoryLogoRepository);
        this.restDataCategoryMockMvc = MockMvcBuilders.standaloneSetup(dataCategoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataCategory createEntity(EntityManager em) {
        DataCategory dataCategory = new DataCategory()
            .recordID(DEFAULT_RECORD_ID)
            .datacategory(DEFAULT_DATACATEGORY)
            .explanation(DEFAULT_EXPLANATION)
            .inactive(DEFAULT_INACTIVE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return dataCategory;
    }

    @Before
    public void initTest() {
        dataCategorySearchRepository.deleteAll();
        dataCategory = createEntity(em);
    }

    @Test
    @Transactional
    public void createDataCategory() throws Exception {
        int databaseSizeBeforeCreate = dataCategoryRepository.findAll().size();

        // Create the DataCategory
        restDataCategoryMockMvc.perform(post("/api/data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataCategory)))
            .andExpect(status().isCreated());

        // Validate the DataCategory in the database
        List<DataCategory> dataCategoryList = dataCategoryRepository.findAll();
        assertThat(dataCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        DataCategory testDataCategory = dataCategoryList.get(dataCategoryList.size() - 1);
        assertThat(testDataCategory.getRecordID()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testDataCategory.getDatacategory()).isEqualTo(DEFAULT_DATACATEGORY);
        assertThat(testDataCategory.getExplanation()).isEqualTo(DEFAULT_EXPLANATION);
        assertThat(testDataCategory.getInactive()).isEqualTo(DEFAULT_INACTIVE);
        assertThat(testDataCategory.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDataCategory.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);

        // Validate the DataCategory in Elasticsearch
        DataCategory dataCategoryEs = dataCategorySearchRepository.findOne(testDataCategory.getId());
        assertThat(dataCategoryEs).isEqualToComparingFieldByField(testDataCategory);
    }

    @Test
    @Transactional
    public void createDataCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dataCategoryRepository.findAll().size();

        // Create the DataCategory with an existing ID
        dataCategory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataCategoryMockMvc.perform(post("/api/data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataCategory)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DataCategory> dataCategoryList = dataCategoryRepository.findAll();
        assertThat(dataCategoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDatacategoryIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataCategoryRepository.findAll().size();
        // set the field null
        dataCategory.setDatacategory(null);

        // Create the DataCategory, which fails.

        restDataCategoryMockMvc.perform(post("/api/data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataCategory)))
            .andExpect(status().isBadRequest());

        List<DataCategory> dataCategoryList = dataCategoryRepository.findAll();
        assertThat(dataCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInactiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataCategoryRepository.findAll().size();
        // set the field null
        dataCategory.setInactive(null);

        // Create the DataCategory, which fails.

        restDataCategoryMockMvc.perform(post("/api/data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataCategory)))
            .andExpect(status().isBadRequest());

        List<DataCategory> dataCategoryList = dataCategoryRepository.findAll();
        assertThat(dataCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataCategoryRepository.findAll().size();
        // set the field null
        dataCategory.setCreatedDate(null);

        // Create the DataCategory, which fails.

        restDataCategoryMockMvc.perform(post("/api/data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataCategory)))
            .andExpect(status().isBadRequest());

        List<DataCategory> dataCategoryList = dataCategoryRepository.findAll();
        assertThat(dataCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDataCategories() throws Exception {
        // Initialize the database
        dataCategoryRepository.saveAndFlush(dataCategory);

        // Get all the dataCategoryList
        restDataCategoryMockMvc.perform(get("/api/data-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].datacategory").value(hasItem(DEFAULT_DATACATEGORY.toString())))
            .andExpect(jsonPath("$.[*].explanation").value(hasItem(DEFAULT_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getDataCategory() throws Exception {
        // Initialize the database
        dataCategoryRepository.saveAndFlush(dataCategory);

        // Get the dataCategory
        restDataCategoryMockMvc.perform(get("/api/data-categories/{id}", dataCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dataCategory.getId().intValue()))
            .andExpect(jsonPath("$.recordID").value(DEFAULT_RECORD_ID.toString()))
            .andExpect(jsonPath("$.datacategory").value(DEFAULT_DATACATEGORY.toString()))
            .andExpect(jsonPath("$.explanation").value(DEFAULT_EXPLANATION.toString()))
            .andExpect(jsonPath("$.inactive").value(DEFAULT_INACTIVE))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDataCategory() throws Exception {
        // Get the dataCategory
        restDataCategoryMockMvc.perform(get("/api/data-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDataCategory() throws Exception {
        // Initialize the database
        dataCategoryRepository.saveAndFlush(dataCategory);
        dataCategorySearchRepository.save(dataCategory);
        int databaseSizeBeforeUpdate = dataCategoryRepository.findAll().size();

        // Update the dataCategory
        DataCategory updatedDataCategory = dataCategoryRepository.findOne(dataCategory.getId());
        updatedDataCategory
            .recordID(UPDATED_RECORD_ID)
            .datacategory(UPDATED_DATACATEGORY)
            .explanation(UPDATED_EXPLANATION)
            .inactive(UPDATED_INACTIVE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restDataCategoryMockMvc.perform(put("/api/data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDataCategory)))
            .andExpect(status().isOk());

        // Validate the DataCategory in the database
        List<DataCategory> dataCategoryList = dataCategoryRepository.findAll();
        assertThat(dataCategoryList).hasSize(databaseSizeBeforeUpdate);
        DataCategory testDataCategory = dataCategoryList.get(dataCategoryList.size() - 1);
        assertThat(testDataCategory.getRecordID()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testDataCategory.getDatacategory()).isEqualTo(UPDATED_DATACATEGORY);
        assertThat(testDataCategory.getExplanation()).isEqualTo(UPDATED_EXPLANATION);
        assertThat(testDataCategory.getInactive()).isEqualTo(UPDATED_INACTIVE);
        assertThat(testDataCategory.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDataCategory.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);

        // Validate the DataCategory in Elasticsearch
        DataCategory dataCategoryEs = dataCategorySearchRepository.findOne(testDataCategory.getId());
        assertThat(dataCategoryEs).isEqualToComparingFieldByField(testDataCategory);
    }

    @Test
    @Transactional
    public void updateNonExistingDataCategory() throws Exception {
        int databaseSizeBeforeUpdate = dataCategoryRepository.findAll().size();

        // Create the DataCategory

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDataCategoryMockMvc.perform(put("/api/data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataCategory)))
            .andExpect(status().isCreated());

        // Validate the DataCategory in the database
        List<DataCategory> dataCategoryList = dataCategoryRepository.findAll();
        assertThat(dataCategoryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDataCategory() throws Exception {
        // Initialize the database
        dataCategoryRepository.saveAndFlush(dataCategory);
        dataCategorySearchRepository.save(dataCategory);
        int databaseSizeBeforeDelete = dataCategoryRepository.findAll().size();

        // Get the dataCategory
        restDataCategoryMockMvc.perform(delete("/api/data-categories/{id}", dataCategory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dataCategoryExistsInEs = dataCategorySearchRepository.exists(dataCategory.getId());
        assertThat(dataCategoryExistsInEs).isFalse();

        // Validate the database is empty
        List<DataCategory> dataCategoryList = dataCategoryRepository.findAll();
        assertThat(dataCategoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDataCategory() throws Exception {
        // Initialize the database
        dataCategoryRepository.saveAndFlush(dataCategory);
        dataCategorySearchRepository.save(dataCategory);

        // Search the dataCategory
        restDataCategoryMockMvc.perform(get("/api/_search/data-categories?query=id:" + dataCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].datacategory").value(hasItem(DEFAULT_DATACATEGORY.toString())))
            .andExpect(jsonPath("$.[*].explanation").value(hasItem(DEFAULT_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataCategory.class);
        DataCategory dataCategory1 = new DataCategory();
        dataCategory1.setId(1L);
        DataCategory dataCategory2 = new DataCategory();
        dataCategory2.setId(dataCategory1.getId());
        assertThat(dataCategory1).isEqualTo(dataCategory2);
        dataCategory2.setId(2L);
        assertThat(dataCategory1).isNotEqualTo(dataCategory2);
        dataCategory1.setId(null);
        assertThat(dataCategory1).isNotEqualTo(dataCategory2);
    }
}
