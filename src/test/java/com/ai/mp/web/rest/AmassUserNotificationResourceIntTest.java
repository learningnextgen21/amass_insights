package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;

import com.ai.mp.domain.UserNotification;
import com.ai.mp.repository.UserNotificationRepository;
import com.ai.mp.repository.search.UserNotificationSearchRepository;
import com.ai.mp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AmassUserNotificationResource REST controller.
 *
 * @see UserNotificationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class AmassUserNotificationResourceIntTest {

    private static final Integer DEFAULT_NOTIFICATION_ID = 11;
    private static final Integer UPDATED_NOTIFICATION_ID = 10;

    private static final Integer DEFAULT_USER_ID = 11;
    private static final Integer UPDATED_USER_ID = 10;

    private static final Integer DEFAULT_USER_PROVIDER_ID = 11;
    private static final Integer UPDATED_USER_PROVIDER_ID = 10;

    private static final Integer DEFAULT_USER_DATA_CATEGORY_ID = 11;
    private static final Integer UPDATED_USER_DATA_CATEGORY_ID = 10;

    private static final Integer DEFAULT_USER_PRODUCT_ID = 11;
    private static final Integer UPDATED_USER_PRODUCT_ID = 10;

    private static final Integer DEFAULT_USER_COMMENT_ID = 11;
    private static final Integer UPDATED_USER_COMMENT_ID = 10;

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_NOTIFICATION_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_NOTIFICATION_TYPE = "BBBBBBBBBB";

    @Autowired
    private UserNotificationRepository amassUserNotificationRepository;

    @Autowired
    private UserNotificationSearchRepository amassUserNotificationSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAmassUserNotificationMockMvc;

    private UserNotification amassUserNotification;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserNotificationResource amassUserNotificationResource = new UserNotificationResource(amassUserNotificationRepository, amassUserNotificationSearchRepository);
        this.restAmassUserNotificationMockMvc = MockMvcBuilders.standaloneSetup(amassUserNotificationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserNotification createEntity(EntityManager em) {
        UserNotification amassUserNotification = new UserNotification()
            .notificationID(DEFAULT_NOTIFICATION_ID)
            .userID(DEFAULT_USER_ID)
            .userProviderID(DEFAULT_USER_PROVIDER_ID)
            .userDataCategoryID(DEFAULT_USER_DATA_CATEGORY_ID)
            .userProductID(DEFAULT_USER_PRODUCT_ID)
            .userCommentID(DEFAULT_USER_COMMENT_ID)
            .createdDate(DEFAULT_CREATED_DATE)
            .status(DEFAULT_STATUS)
            .notificationType(DEFAULT_NOTIFICATION_TYPE);
        return amassUserNotification;
    }

    @Before
    public void initTest() {
        amassUserNotificationSearchRepository.deleteAll();
        amassUserNotification = createEntity(em);
    }

    @Test
    @Transactional
    public void createAmassUserNotification() throws Exception {
        int databaseSizeBeforeCreate = amassUserNotificationRepository.findAll().size();

        // Create the AmassUserNotification
        restAmassUserNotificationMockMvc.perform(post("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserNotification)))
            .andExpect(status().isCreated());

        // Validate the AmassUserNotification in the database
        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeCreate + 1);
        UserNotification testAmassUserNotification = amassUserNotificationList.get(amassUserNotificationList.size() - 1);
        assertThat(testAmassUserNotification.getNotificationID()).isEqualTo(DEFAULT_NOTIFICATION_ID);
        assertThat(testAmassUserNotification.getUserID()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testAmassUserNotification.getUserProviderID()).isEqualTo(DEFAULT_USER_PROVIDER_ID);
        assertThat(testAmassUserNotification.getUserDataCategoryID()).isEqualTo(DEFAULT_USER_DATA_CATEGORY_ID);
        assertThat(testAmassUserNotification.getUserProductID()).isEqualTo(DEFAULT_USER_PRODUCT_ID);
        assertThat(testAmassUserNotification.getUserCommentID()).isEqualTo(DEFAULT_USER_COMMENT_ID);
        assertThat(testAmassUserNotification.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testAmassUserNotification.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testAmassUserNotification.getNotificationType()).isEqualTo(DEFAULT_NOTIFICATION_TYPE);

        // Validate the AmassUserNotification in Elasticsearch
        UserNotification amassUserNotificationEs = amassUserNotificationSearchRepository.findOne(testAmassUserNotification.getId());
        assertThat(amassUserNotificationEs).isEqualToComparingFieldByField(testAmassUserNotification);
    }

    @Test
    @Transactional
    public void createAmassUserNotificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = amassUserNotificationRepository.findAll().size();

        // Create the AmassUserNotification with an existing ID
        amassUserNotification.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAmassUserNotificationMockMvc.perform(post("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserNotification)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNotificationIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserNotificationRepository.findAll().size();
        // set the field null
        amassUserNotification.setNotificationID(null);

        // Create the AmassUserNotification, which fails.

        restAmassUserNotificationMockMvc.perform(post("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserNotification)))
            .andExpect(status().isBadRequest());

        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUserIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserNotificationRepository.findAll().size();
        // set the field null
        amassUserNotification.setUserID(null);

        // Create the AmassUserNotification, which fails.

        restAmassUserNotificationMockMvc.perform(post("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserNotification)))
            .andExpect(status().isBadRequest());

        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUserProviderIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserNotificationRepository.findAll().size();
        // set the field null
        amassUserNotification.setUserProviderID(null);

        // Create the AmassUserNotification, which fails.

        restAmassUserNotificationMockMvc.perform(post("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserNotification)))
            .andExpect(status().isBadRequest());

        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUserDataCategoryIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserNotificationRepository.findAll().size();
        // set the field null
        amassUserNotification.setUserDataCategoryID(null);

        // Create the AmassUserNotification, which fails.

        restAmassUserNotificationMockMvc.perform(post("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserNotification)))
            .andExpect(status().isBadRequest());

        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUserProductIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserNotificationRepository.findAll().size();
        // set the field null
        amassUserNotification.setUserProductID(null);

        // Create the AmassUserNotification, which fails.

        restAmassUserNotificationMockMvc.perform(post("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserNotification)))
            .andExpect(status().isBadRequest());

        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUserCommentIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserNotificationRepository.findAll().size();
        // set the field null
        amassUserNotification.setUserCommentID(null);

        // Create the AmassUserNotification, which fails.

        restAmassUserNotificationMockMvc.perform(post("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserNotification)))
            .andExpect(status().isBadRequest());

        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserNotificationRepository.findAll().size();
        // set the field null
        amassUserNotification.setCreatedDate(null);

        // Create the AmassUserNotification, which fails.

        restAmassUserNotificationMockMvc.perform(post("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserNotification)))
            .andExpect(status().isBadRequest());

        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserNotificationRepository.findAll().size();
        // set the field null
        amassUserNotification.setStatus(null);

        // Create the AmassUserNotification, which fails.

        restAmassUserNotificationMockMvc.perform(post("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserNotification)))
            .andExpect(status().isBadRequest());

        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNotificationTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserNotificationRepository.findAll().size();
        // set the field null
        amassUserNotification.setNotificationType(null);

        // Create the AmassUserNotification, which fails.

        restAmassUserNotificationMockMvc.perform(post("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserNotification)))
            .andExpect(status().isBadRequest());

        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAmassUserNotifications() throws Exception {
        // Initialize the database
        amassUserNotificationRepository.saveAndFlush(amassUserNotification);

        // Get all the amassUserNotificationList
        restAmassUserNotificationMockMvc.perform(get("/api/amass-user-notifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(amassUserNotification.getId().intValue())))
            .andExpect(jsonPath("$.[*].notificationID").value(hasItem(DEFAULT_NOTIFICATION_ID)))
            .andExpect(jsonPath("$.[*].userID").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].userProviderID").value(hasItem(DEFAULT_USER_PROVIDER_ID)))
            .andExpect(jsonPath("$.[*].userDataCategoryID").value(hasItem(DEFAULT_USER_DATA_CATEGORY_ID)))
            .andExpect(jsonPath("$.[*].userProductID").value(hasItem(DEFAULT_USER_PRODUCT_ID)))
            .andExpect(jsonPath("$.[*].userCommentID").value(hasItem(DEFAULT_USER_COMMENT_ID)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].notificationType").value(hasItem(DEFAULT_NOTIFICATION_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getAmassUserNotification() throws Exception {
        // Initialize the database
        amassUserNotificationRepository.saveAndFlush(amassUserNotification);

        // Get the amassUserNotification
        restAmassUserNotificationMockMvc.perform(get("/api/amass-user-notifications/{id}", amassUserNotification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(amassUserNotification.getId().intValue()))
            .andExpect(jsonPath("$.notificationID").value(DEFAULT_NOTIFICATION_ID))
            .andExpect(jsonPath("$.userID").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.userProviderID").value(DEFAULT_USER_PROVIDER_ID))
            .andExpect(jsonPath("$.userDataCategoryID").value(DEFAULT_USER_DATA_CATEGORY_ID))
            .andExpect(jsonPath("$.userProductID").value(DEFAULT_USER_PRODUCT_ID))
            .andExpect(jsonPath("$.userCommentID").value(DEFAULT_USER_COMMENT_ID))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.notificationType").value(DEFAULT_NOTIFICATION_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAmassUserNotification() throws Exception {
        // Get the amassUserNotification
        restAmassUserNotificationMockMvc.perform(get("/api/amass-user-notifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAmassUserNotification() throws Exception {
        // Initialize the database
        amassUserNotificationRepository.saveAndFlush(amassUserNotification);
        amassUserNotificationSearchRepository.save(amassUserNotification);
        int databaseSizeBeforeUpdate = amassUserNotificationRepository.findAll().size();

        // Update the amassUserNotification
        UserNotification updatedAmassUserNotification = amassUserNotificationRepository.findOne(amassUserNotification.getId());
        updatedAmassUserNotification
            .notificationID(UPDATED_NOTIFICATION_ID)
            .userID(UPDATED_USER_ID)
            .userProviderID(UPDATED_USER_PROVIDER_ID)
            .userDataCategoryID(UPDATED_USER_DATA_CATEGORY_ID)
            .userProductID(UPDATED_USER_PRODUCT_ID)
            .userCommentID(UPDATED_USER_COMMENT_ID)
            .createdDate(UPDATED_CREATED_DATE)
            .status(UPDATED_STATUS)
            .notificationType(UPDATED_NOTIFICATION_TYPE);

        restAmassUserNotificationMockMvc.perform(put("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAmassUserNotification)))
            .andExpect(status().isOk());

        // Validate the AmassUserNotification in the database
        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeUpdate);
        UserNotification testAmassUserNotification = amassUserNotificationList.get(amassUserNotificationList.size() - 1);
        assertThat(testAmassUserNotification.getNotificationID()).isEqualTo(UPDATED_NOTIFICATION_ID);
        assertThat(testAmassUserNotification.getUserID()).isEqualTo(UPDATED_USER_ID);
        assertThat(testAmassUserNotification.getUserProviderID()).isEqualTo(UPDATED_USER_PROVIDER_ID);
        assertThat(testAmassUserNotification.getUserDataCategoryID()).isEqualTo(UPDATED_USER_DATA_CATEGORY_ID);
        assertThat(testAmassUserNotification.getUserProductID()).isEqualTo(UPDATED_USER_PRODUCT_ID);
        assertThat(testAmassUserNotification.getUserCommentID()).isEqualTo(UPDATED_USER_COMMENT_ID);
        assertThat(testAmassUserNotification.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testAmassUserNotification.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAmassUserNotification.getNotificationType()).isEqualTo(UPDATED_NOTIFICATION_TYPE);

        // Validate the AmassUserNotification in Elasticsearch
        UserNotification amassUserNotificationEs = amassUserNotificationSearchRepository.findOne(testAmassUserNotification.getId());
        assertThat(amassUserNotificationEs).isEqualToComparingFieldByField(testAmassUserNotification);
    }

    @Test
    @Transactional
    public void updateNonExistingAmassUserNotification() throws Exception {
        int databaseSizeBeforeUpdate = amassUserNotificationRepository.findAll().size();

        // Create the AmassUserNotification

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAmassUserNotificationMockMvc.perform(put("/api/amass-user-notifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserNotification)))
            .andExpect(status().isCreated());

        // Validate the AmassUserNotification in the database
        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAmassUserNotification() throws Exception {
        // Initialize the database
        amassUserNotificationRepository.saveAndFlush(amassUserNotification);
        amassUserNotificationSearchRepository.save(amassUserNotification);
        int databaseSizeBeforeDelete = amassUserNotificationRepository.findAll().size();

        // Get the amassUserNotification
        restAmassUserNotificationMockMvc.perform(delete("/api/amass-user-notifications/{id}", amassUserNotification.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean amassUserNotificationExistsInEs = amassUserNotificationSearchRepository.exists(amassUserNotification.getId());
        assertThat(amassUserNotificationExistsInEs).isFalse();

        // Validate the database is empty
        List<UserNotification> amassUserNotificationList = amassUserNotificationRepository.findAll();
        assertThat(amassUserNotificationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAmassUserNotification() throws Exception {
        // Initialize the database
        amassUserNotificationRepository.saveAndFlush(amassUserNotification);
        amassUserNotificationSearchRepository.save(amassUserNotification);

        // Search the amassUserNotification
        restAmassUserNotificationMockMvc.perform(get("/api/_search/amass-user-notifications?query=id:" + amassUserNotification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(amassUserNotification.getId().intValue())))
            .andExpect(jsonPath("$.[*].notificationID").value(hasItem(DEFAULT_NOTIFICATION_ID)))
            .andExpect(jsonPath("$.[*].userID").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].userProviderID").value(hasItem(DEFAULT_USER_PROVIDER_ID)))
            .andExpect(jsonPath("$.[*].userDataCategoryID").value(hasItem(DEFAULT_USER_DATA_CATEGORY_ID)))
            .andExpect(jsonPath("$.[*].userProductID").value(hasItem(DEFAULT_USER_PRODUCT_ID)))
            .andExpect(jsonPath("$.[*].userCommentID").value(hasItem(DEFAULT_USER_COMMENT_ID)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].notificationType").value(hasItem(DEFAULT_NOTIFICATION_TYPE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserNotification.class);
        UserNotification amassUserNotification1 = new UserNotification();
        amassUserNotification1.setId(1L);
        UserNotification amassUserNotification2 = new UserNotification();
        amassUserNotification2.setId(amassUserNotification1.getId());
        assertThat(amassUserNotification1).isEqualTo(amassUserNotification2);
        amassUserNotification2.setId(2L);
        assertThat(amassUserNotification1).isNotEqualTo(amassUserNotification2);
        amassUserNotification1.setId(null);
        assertThat(amassUserNotification1).isNotEqualTo(amassUserNotification2);
    }
}
