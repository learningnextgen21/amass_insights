package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;

import com.ai.mp.config.ApplicationProperties;
import com.ai.mp.domain.DataArticle;
import com.ai.mp.repository.DataArticleRepository;
import com.ai.mp.service.DataArticleService;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.repository.search.DataArticleSearchRepository;
import com.ai.mp.service.MailService;
import com.ai.mp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DataArticleResource REST controller.
 *
 * @see DataArticleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class DataArticleResourceIntTest {

    private static final String DEFAULT_ARTICLEUNIQUEKEY = "AAAAAAAAAA";
    private static final String UPDATED_ARTICLEUNIQUEKEY = "BBBBBBBBBB";

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final Date DEFAULT_PUBLISHED_DATE = new Date();
    private static final Date UPDATED_PUBLISHED_DATE = new Date();

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT_HTML = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT_HTML = "BBBBBBBBBB";

    private static final String DEFAULT_SUMMARY = "AAAAAAAAAA";
    private static final String UPDATED_SUMMARY = "BBBBBBBBBB";

    private static final String DEFAULT_AUTHOR = "AAAAAAAAAA";
    private static final String UPDATED_AUTHOR = "BBBBBBBBBB";

    private static final String DEFAULT_PUBLISHER = "AAAAAAAAAA";
    private static final String UPDATED_PUBLISHER = "BBBBBBBBBB";

    private static final String DEFAULT_SUMMARY_HTML = "AAAAAAAAAA";
    private static final String UPDATED_SUMMARY_HTML = "BBBBBBBBBB";

    private static final Integer DEFAULT_RELEVANCY = 11;
    private static final Integer UPDATED_RELEVANCY = 10;

    private static final String DEFAULT_PURPOSE = "AAAAAAAAAA";
    private static final String UPDATED_PURPOSE = "BBBBBBBBBB";

    private static final String DEFAULT_PURPOSE_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_PURPOSE_TYPE = "BBBBBBBBBB";

    private static final Date DEFAULT_CREATED_DATE = new Date();
    private static final Date UPDATED_CREATED_DATE = new Date();

    private static final Date DEFAULT_UPDATED_DATE = new Date();
    private static final Date UPDATED_UPDATED_DATE = new Date();

    @Autowired
    private DataArticleRepository dataArticleRepository;

    @Autowired
    private DataArticleService dataArticleService;

    @Autowired
    private DataArticleSearchRepository dataArticleSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDataArticleMockMvc;

    private DataArticle dataArticle;

    private MailService mailService;

    private ElasticsearchIndexService elasticSearchIndexService;

    private ApplicationProperties applicationProperties;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DataArticleResource dataArticleResource = new DataArticleResource(dataArticleService, dataArticleRepository, applicationProperties, mailService,elasticSearchIndexService);
        this.restDataArticleMockMvc = MockMvcBuilders.standaloneSetup(dataArticleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataArticle createEntity(EntityManager em) {
        DataArticle dataArticle = new DataArticle()
            .articleuniquekey(DEFAULT_ARTICLEUNIQUEKEY)
            .recordID(DEFAULT_RECORD_ID)
            .title(DEFAULT_TITLE)
            .url(DEFAULT_URL)
            //.publishedDate(DEFAULT_PUBLISHED_DATE)
            .content(DEFAULT_CONTENT)
            .contentHTML(DEFAULT_CONTENT_HTML)
            .summary(DEFAULT_SUMMARY)
            .author(DEFAULT_AUTHOR)
            .publisher(DEFAULT_PUBLISHER)
            .summaryHTML(DEFAULT_SUMMARY_HTML)
            .relevancy(DEFAULT_RELEVANCY);
//            .purpose(DEFAULT_PURPOSE)
//            .purposeType(DEFAULT_PURPOSE_TYPE)
            //.createdDate(DEFAULT_CREATED_DATE)
           // .updatedDate(DEFAULT_UPDATED_DATE);
        return dataArticle;
    }

    @Before
    public void initTest() {
        dataArticleSearchRepository.deleteAll();
        dataArticle = createEntity(em);
    }

    @Test
    @Transactional
    public void createDataArticle() throws Exception {
        int databaseSizeBeforeCreate = dataArticleRepository.findAll().size();

        // Create the DataArticle
        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isCreated());

        // Validate the DataArticle in the database
        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeCreate + 1);
        DataArticle testDataArticle = dataArticleList.get(dataArticleList.size() - 1);
        assertThat(testDataArticle.getArticleuniquekey()).isEqualTo(DEFAULT_ARTICLEUNIQUEKEY);
        assertThat(testDataArticle.getRecordID()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testDataArticle.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testDataArticle.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testDataArticle.getPublishedDate()).isEqualTo(DEFAULT_PUBLISHED_DATE);
        assertThat(testDataArticle.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testDataArticle.getContentHTML()).isEqualTo(DEFAULT_CONTENT_HTML);
        assertThat(testDataArticle.getSummary()).isEqualTo(DEFAULT_SUMMARY);
        assertThat(testDataArticle.getAuthor()).isEqualTo(DEFAULT_AUTHOR);
        assertThat(testDataArticle.getPublisher()).isEqualTo(DEFAULT_PUBLISHER);
        assertThat(testDataArticle.getSummaryHTML()).isEqualTo(DEFAULT_SUMMARY_HTML);
        assertThat(testDataArticle.getRelevancy()).isEqualTo(DEFAULT_RELEVANCY);
//        assertThat(testDataArticle.getPurpose()).isEqualTo(DEFAULT_PURPOSE);
//        assertThat(testDataArticle.getPurposeType()).isEqualTo(DEFAULT_PURPOSE_TYPE);
        assertThat(testDataArticle.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDataArticle.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);

        // Validate the DataArticle in Elasticsearch
        DataArticle dataArticleEs = dataArticleSearchRepository.findOne(testDataArticle.getId());
        assertThat(dataArticleEs).isEqualToComparingFieldByField(testDataArticle);
    }

    @Test
    @Transactional
    public void createDataArticleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dataArticleRepository.findAll().size();

        // Create the DataArticle with an existing ID
        dataArticle.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkArticleuniquekeyIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
        dataArticle.setArticleuniquekey(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
        dataArticle.setTitle(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUrlIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
        dataArticle.setUrl(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContentIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
        dataArticle.setContent(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContentHTMLIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
        dataArticle.setContentHTML(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSummaryIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
        dataArticle.setSummary(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAuthorIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
        dataArticle.setAuthor(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPublisherIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
        dataArticle.setPublisher(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSummaryHTMLIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
        dataArticle.setSummaryHTML(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRelevancyIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
        dataArticle.setRelevancy(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPurposeIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
//        dataArticle.setPurpose(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPurposeTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
//        dataArticle.setPurposeType(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataArticleRepository.findAll().size();
        // set the field null
        dataArticle.setCreatedDate(null);

        // Create the DataArticle, which fails.

        restDataArticleMockMvc.perform(post("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isBadRequest());

        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDataArticles() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList
        restDataArticleMockMvc.perform(get("/api/data-articles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataArticle.getId().intValue())))
            .andExpect(jsonPath("$.[*].articleuniquekey").value(hasItem(DEFAULT_ARTICLEUNIQUEKEY.toString())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].publishedDate").value(hasItem(DEFAULT_PUBLISHED_DATE.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].contentHTML").value(hasItem(DEFAULT_CONTENT_HTML.toString())))
            .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
            .andExpect(jsonPath("$.[*].author").value(hasItem(DEFAULT_AUTHOR.toString())))
            .andExpect(jsonPath("$.[*].publisher").value(hasItem(DEFAULT_PUBLISHER.toString())))
            .andExpect(jsonPath("$.[*].summaryHTML").value(hasItem(DEFAULT_SUMMARY_HTML.toString())))
            .andExpect(jsonPath("$.[*].relevancy").value(hasItem(DEFAULT_RELEVANCY)))
            .andExpect(jsonPath("$.[*].purpose").value(hasItem(DEFAULT_PURPOSE.toString())))
            .andExpect(jsonPath("$.[*].purposeType").value(hasItem(DEFAULT_PURPOSE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getDataArticle() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get the dataArticle
        restDataArticleMockMvc.perform(get("/api/data-articles/{id}", dataArticle.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dataArticle.getId().intValue()))
            .andExpect(jsonPath("$.articleuniquekey").value(DEFAULT_ARTICLEUNIQUEKEY.toString()))
            .andExpect(jsonPath("$.recordID").value(DEFAULT_RECORD_ID.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.publishedDate").value(DEFAULT_PUBLISHED_DATE.toString()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.contentHTML").value(DEFAULT_CONTENT_HTML.toString()))
            .andExpect(jsonPath("$.summary").value(DEFAULT_SUMMARY.toString()))
            .andExpect(jsonPath("$.author").value(DEFAULT_AUTHOR.toString()))
            .andExpect(jsonPath("$.publisher").value(DEFAULT_PUBLISHER.toString()))
            .andExpect(jsonPath("$.summaryHTML").value(DEFAULT_SUMMARY_HTML.toString()))
            .andExpect(jsonPath("$.relevancy").value(DEFAULT_RELEVANCY))
            .andExpect(jsonPath("$.purpose").value(DEFAULT_PURPOSE.toString()))
            .andExpect(jsonPath("$.purposeType").value(DEFAULT_PURPOSE_TYPE.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllDataArticlesByArticleuniquekeyIsEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where articleuniquekey equals to DEFAULT_ARTICLEUNIQUEKEY
        defaultDataArticleShouldBeFound("articleuniquekey.equals=" + DEFAULT_ARTICLEUNIQUEKEY);

        // Get all the dataArticleList where articleuniquekey equals to UPDATED_ARTICLEUNIQUEKEY
        defaultDataArticleShouldNotBeFound("articleuniquekey.equals=" + UPDATED_ARTICLEUNIQUEKEY);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByArticleuniquekeyIsInShouldWork() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where articleuniquekey in DEFAULT_ARTICLEUNIQUEKEY or UPDATED_ARTICLEUNIQUEKEY
        defaultDataArticleShouldBeFound("articleuniquekey.in=" + DEFAULT_ARTICLEUNIQUEKEY + "," + UPDATED_ARTICLEUNIQUEKEY);

        // Get all the dataArticleList where articleuniquekey equals to UPDATED_ARTICLEUNIQUEKEY
        defaultDataArticleShouldNotBeFound("articleuniquekey.in=" + UPDATED_ARTICLEUNIQUEKEY);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByArticleuniquekeyIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where articleuniquekey is not null
        defaultDataArticleShouldBeFound("articleuniquekey.specified=true");

        // Get all the dataArticleList where articleuniquekey is null
        defaultDataArticleShouldNotBeFound("articleuniquekey.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataArticlesByRecordIDIsEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where recordID equals to DEFAULT_RECORD_ID
        defaultDataArticleShouldBeFound("recordID.equals=" + DEFAULT_RECORD_ID);

        // Get all the dataArticleList where recordID equals to UPDATED_RECORD_ID
        defaultDataArticleShouldNotBeFound("recordID.equals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByRecordIDIsInShouldWork() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where recordID in DEFAULT_RECORD_ID or UPDATED_RECORD_ID
        defaultDataArticleShouldBeFound("recordID.in=" + DEFAULT_RECORD_ID + "," + UPDATED_RECORD_ID);

        // Get all the dataArticleList where recordID equals to UPDATED_RECORD_ID
        defaultDataArticleShouldNotBeFound("recordID.in=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByRecordIDIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where recordID is not null
        defaultDataArticleShouldBeFound("recordID.specified=true");

        // Get all the dataArticleList where recordID is null
        defaultDataArticleShouldNotBeFound("recordID.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataArticlesByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where title equals to DEFAULT_TITLE
        defaultDataArticleShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the dataArticleList where title equals to UPDATED_TITLE
        defaultDataArticleShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultDataArticleShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the dataArticleList where title equals to UPDATED_TITLE
        defaultDataArticleShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where title is not null
        defaultDataArticleShouldBeFound("title.specified=true");

        // Get all the dataArticleList where title is null
        defaultDataArticleShouldNotBeFound("title.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataArticlesByUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where url equals to DEFAULT_URL
        defaultDataArticleShouldBeFound("url.equals=" + DEFAULT_URL);

        // Get all the dataArticleList where url equals to UPDATED_URL
        defaultDataArticleShouldNotBeFound("url.equals=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByUrlIsInShouldWork() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where url in DEFAULT_URL or UPDATED_URL
        defaultDataArticleShouldBeFound("url.in=" + DEFAULT_URL + "," + UPDATED_URL);

        // Get all the dataArticleList where url equals to UPDATED_URL
        defaultDataArticleShouldNotBeFound("url.in=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where url is not null
        defaultDataArticleShouldBeFound("url.specified=true");

        // Get all the dataArticleList where url is null
        defaultDataArticleShouldNotBeFound("url.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPublishedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where publishedDate equals to DEFAULT_PUBLISHED_DATE
        defaultDataArticleShouldBeFound("publishedDate.equals=" + DEFAULT_PUBLISHED_DATE);

        // Get all the dataArticleList where publishedDate equals to UPDATED_PUBLISHED_DATE
        defaultDataArticleShouldNotBeFound("publishedDate.equals=" + UPDATED_PUBLISHED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPublishedDateIsInShouldWork() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where publishedDate in DEFAULT_PUBLISHED_DATE or UPDATED_PUBLISHED_DATE
        defaultDataArticleShouldBeFound("publishedDate.in=" + DEFAULT_PUBLISHED_DATE + "," + UPDATED_PUBLISHED_DATE);

        // Get all the dataArticleList where publishedDate equals to UPDATED_PUBLISHED_DATE
        defaultDataArticleShouldNotBeFound("publishedDate.in=" + UPDATED_PUBLISHED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPublishedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where publishedDate is not null
        defaultDataArticleShouldBeFound("publishedDate.specified=true");

        // Get all the dataArticleList where publishedDate is null
        defaultDataArticleShouldNotBeFound("publishedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPublishedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where publishedDate greater than or equals to DEFAULT_PUBLISHED_DATE
        defaultDataArticleShouldBeFound("publishedDate.greaterOrEqualThan=" + DEFAULT_PUBLISHED_DATE);

        // Get all the dataArticleList where publishedDate greater than or equals to UPDATED_PUBLISHED_DATE
        defaultDataArticleShouldNotBeFound("publishedDate.greaterOrEqualThan=" + UPDATED_PUBLISHED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPublishedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where publishedDate less than or equals to DEFAULT_PUBLISHED_DATE
        defaultDataArticleShouldNotBeFound("publishedDate.lessThan=" + DEFAULT_PUBLISHED_DATE);

        // Get all the dataArticleList where publishedDate less than or equals to UPDATED_PUBLISHED_DATE
        defaultDataArticleShouldBeFound("publishedDate.lessThan=" + UPDATED_PUBLISHED_DATE);
    }


    @Test
    @Transactional
    public void getAllDataArticlesByAuthorIsEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where author equals to DEFAULT_AUTHOR
        defaultDataArticleShouldBeFound("author.equals=" + DEFAULT_AUTHOR);

        // Get all the dataArticleList where author equals to UPDATED_AUTHOR
        defaultDataArticleShouldNotBeFound("author.equals=" + UPDATED_AUTHOR);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByAuthorIsInShouldWork() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where author in DEFAULT_AUTHOR or UPDATED_AUTHOR
        defaultDataArticleShouldBeFound("author.in=" + DEFAULT_AUTHOR + "," + UPDATED_AUTHOR);

        // Get all the dataArticleList where author equals to UPDATED_AUTHOR
        defaultDataArticleShouldNotBeFound("author.in=" + UPDATED_AUTHOR);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByAuthorIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where author is not null
        defaultDataArticleShouldBeFound("author.specified=true");

        // Get all the dataArticleList where author is null
        defaultDataArticleShouldNotBeFound("author.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPublisherIsEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where publisher equals to DEFAULT_PUBLISHER
        defaultDataArticleShouldBeFound("publisher.equals=" + DEFAULT_PUBLISHER);

        // Get all the dataArticleList where publisher equals to UPDATED_PUBLISHER
        defaultDataArticleShouldNotBeFound("publisher.equals=" + UPDATED_PUBLISHER);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPublisherIsInShouldWork() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where publisher in DEFAULT_PUBLISHER or UPDATED_PUBLISHER
        defaultDataArticleShouldBeFound("publisher.in=" + DEFAULT_PUBLISHER + "," + UPDATED_PUBLISHER);

        // Get all the dataArticleList where publisher equals to UPDATED_PUBLISHER
        defaultDataArticleShouldNotBeFound("publisher.in=" + UPDATED_PUBLISHER);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPublisherIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where publisher is not null
        defaultDataArticleShouldBeFound("publisher.specified=true");

        // Get all the dataArticleList where publisher is null
        defaultDataArticleShouldNotBeFound("publisher.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataArticlesByRelevancyIsEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where relevancy equals to DEFAULT_RELEVANCY
        defaultDataArticleShouldBeFound("relevancy.equals=" + DEFAULT_RELEVANCY);

        // Get all the dataArticleList where relevancy equals to UPDATED_RELEVANCY
        defaultDataArticleShouldNotBeFound("relevancy.equals=" + UPDATED_RELEVANCY);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByRelevancyIsInShouldWork() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where relevancy in DEFAULT_RELEVANCY or UPDATED_RELEVANCY
        defaultDataArticleShouldBeFound("relevancy.in=" + DEFAULT_RELEVANCY + "," + UPDATED_RELEVANCY);

        // Get all the dataArticleList where relevancy equals to UPDATED_RELEVANCY
        defaultDataArticleShouldNotBeFound("relevancy.in=" + UPDATED_RELEVANCY);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByRelevancyIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where relevancy is not null
        defaultDataArticleShouldBeFound("relevancy.specified=true");

        // Get all the dataArticleList where relevancy is null
        defaultDataArticleShouldNotBeFound("relevancy.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataArticlesByRelevancyIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where relevancy greater than or equals to DEFAULT_RELEVANCY
        defaultDataArticleShouldBeFound("relevancy.greaterOrEqualThan=" + DEFAULT_RELEVANCY);

        // Get all the dataArticleList where relevancy greater than or equals to (DEFAULT_RELEVANCY + 1)
        defaultDataArticleShouldNotBeFound("relevancy.greaterOrEqualThan=" + (DEFAULT_RELEVANCY + 1));
    }

    @Test
    @Transactional
    public void getAllDataArticlesByRelevancyIsLessThanSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where relevancy less than or equals to DEFAULT_RELEVANCY
        defaultDataArticleShouldNotBeFound("relevancy.lessThan=" + DEFAULT_RELEVANCY);

        // Get all the dataArticleList where relevancy less than or equals to (DEFAULT_RELEVANCY + 1)
        defaultDataArticleShouldBeFound("relevancy.lessThan=" + (DEFAULT_RELEVANCY + 1));
    }


    @Test
    @Transactional
    public void getAllDataArticlesByPurposeIsEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where purpose equals to DEFAULT_PURPOSE
        defaultDataArticleShouldBeFound("purpose.equals=" + DEFAULT_PURPOSE);

        // Get all the dataArticleList where purpose equals to UPDATED_PURPOSE
        defaultDataArticleShouldNotBeFound("purpose.equals=" + UPDATED_PURPOSE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPurposeIsInShouldWork() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where purpose in DEFAULT_PURPOSE or UPDATED_PURPOSE
        defaultDataArticleShouldBeFound("purpose.in=" + DEFAULT_PURPOSE + "," + UPDATED_PURPOSE);

        // Get all the dataArticleList where purpose equals to UPDATED_PURPOSE
        defaultDataArticleShouldNotBeFound("purpose.in=" + UPDATED_PURPOSE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPurposeIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where purpose is not null
        defaultDataArticleShouldBeFound("purpose.specified=true");

        // Get all the dataArticleList where purpose is null
        defaultDataArticleShouldNotBeFound("purpose.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPurposeTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where purposeType equals to DEFAULT_PURPOSE_TYPE
        defaultDataArticleShouldBeFound("purposeType.equals=" + DEFAULT_PURPOSE_TYPE);

        // Get all the dataArticleList where purposeType equals to UPDATED_PURPOSE_TYPE
        defaultDataArticleShouldNotBeFound("purposeType.equals=" + UPDATED_PURPOSE_TYPE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPurposeTypeIsInShouldWork() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where purposeType in DEFAULT_PURPOSE_TYPE or UPDATED_PURPOSE_TYPE
        defaultDataArticleShouldBeFound("purposeType.in=" + DEFAULT_PURPOSE_TYPE + "," + UPDATED_PURPOSE_TYPE);

        // Get all the dataArticleList where purposeType equals to UPDATED_PURPOSE_TYPE
        defaultDataArticleShouldNotBeFound("purposeType.in=" + UPDATED_PURPOSE_TYPE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByPurposeTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where purposeType is not null
        defaultDataArticleShouldBeFound("purposeType.specified=true");

        // Get all the dataArticleList where purposeType is null
        defaultDataArticleShouldNotBeFound("purposeType.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataArticlesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where createdDate equals to DEFAULT_CREATED_DATE
        defaultDataArticleShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the dataArticleList where createdDate equals to UPDATED_CREATED_DATE
        defaultDataArticleShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultDataArticleShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the dataArticleList where createdDate equals to UPDATED_CREATED_DATE
        defaultDataArticleShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where createdDate is not null
        defaultDataArticleShouldBeFound("createdDate.specified=true");

        // Get all the dataArticleList where createdDate is null
        defaultDataArticleShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataArticlesByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where createdDate greater than or equals to DEFAULT_CREATED_DATE
        defaultDataArticleShouldBeFound("createdDate.greaterOrEqualThan=" + DEFAULT_CREATED_DATE);

        // Get all the dataArticleList where createdDate greater than or equals to UPDATED_CREATED_DATE
        defaultDataArticleShouldNotBeFound("createdDate.greaterOrEqualThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where createdDate less than or equals to DEFAULT_CREATED_DATE
        defaultDataArticleShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the dataArticleList where createdDate less than or equals to UPDATED_CREATED_DATE
        defaultDataArticleShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllDataArticlesByUpdatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where updatedDate equals to DEFAULT_UPDATED_DATE
        defaultDataArticleShouldBeFound("updatedDate.equals=" + DEFAULT_UPDATED_DATE);

        // Get all the dataArticleList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultDataArticleShouldNotBeFound("updatedDate.equals=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByUpdatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where updatedDate in DEFAULT_UPDATED_DATE or UPDATED_UPDATED_DATE
        defaultDataArticleShouldBeFound("updatedDate.in=" + DEFAULT_UPDATED_DATE + "," + UPDATED_UPDATED_DATE);

        // Get all the dataArticleList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultDataArticleShouldNotBeFound("updatedDate.in=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataArticlesByUpdatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataArticleRepository.saveAndFlush(dataArticle);

        // Get all the dataArticleList where updatedDate is not null
        defaultDataArticleShouldBeFound("updatedDate.specified=true");

        // Get all the dataArticleList where updatedDate is null
        defaultDataArticleShouldNotBeFound("updatedDate.specified=false");
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultDataArticleShouldBeFound(String filter) throws Exception {
        restDataArticleMockMvc.perform(get("/api/data-articles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataArticle.getId().intValue())))
            .andExpect(jsonPath("$.[*].articleuniquekey").value(hasItem(DEFAULT_ARTICLEUNIQUEKEY.toString())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].publishedDate").value(hasItem(DEFAULT_PUBLISHED_DATE.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].contentHTML").value(hasItem(DEFAULT_CONTENT_HTML.toString())))
            .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
            .andExpect(jsonPath("$.[*].author").value(hasItem(DEFAULT_AUTHOR.toString())))
            .andExpect(jsonPath("$.[*].publisher").value(hasItem(DEFAULT_PUBLISHER.toString())))
            .andExpect(jsonPath("$.[*].summaryHTML").value(hasItem(DEFAULT_SUMMARY_HTML.toString())))
            .andExpect(jsonPath("$.[*].relevancy").value(hasItem(DEFAULT_RELEVANCY)))
            .andExpect(jsonPath("$.[*].purpose").value(hasItem(DEFAULT_PURPOSE.toString())))
            .andExpect(jsonPath("$.[*].purposeType").value(hasItem(DEFAULT_PURPOSE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultDataArticleShouldNotBeFound(String filter) throws Exception {
        restDataArticleMockMvc.perform(get("/api/data-articles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingDataArticle() throws Exception {
        // Get the dataArticle
        restDataArticleMockMvc.perform(get("/api/data-articles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDataArticle() throws Exception {
        // Initialize the database
        dataArticleService.save(dataArticle);

        int databaseSizeBeforeUpdate = dataArticleRepository.findAll().size();

        // Update the dataArticle
        DataArticle updatedDataArticle = dataArticleRepository.findOne(dataArticle.getId());
        updatedDataArticle
            .articleuniquekey(UPDATED_ARTICLEUNIQUEKEY)
            .recordID(UPDATED_RECORD_ID)
            .title(UPDATED_TITLE)
            .url(UPDATED_URL)
            //.publishedDate(UPDATED_PUBLISHED_DATE)
            .content(UPDATED_CONTENT)
            .contentHTML(UPDATED_CONTENT_HTML)
            .summary(UPDATED_SUMMARY)
            .author(UPDATED_AUTHOR)
            .publisher(UPDATED_PUBLISHER)
            .summaryHTML(UPDATED_SUMMARY_HTML)
            .relevancy(UPDATED_RELEVANCY);
//            .purpose(UPDATED_PURPOSE)
//            .purposeType(UPDATED_PURPOSE_TYPE)
            //.createdDate(UPDATED_CREATED_DATE)
            //.updatedDate(UPDATED_UPDATED_DATE);

        restDataArticleMockMvc.perform(put("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDataArticle)))
            .andExpect(status().isOk());

        // Validate the DataArticle in the database
        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeUpdate);
        DataArticle testDataArticle = dataArticleList.get(dataArticleList.size() - 1);
        assertThat(testDataArticle.getArticleuniquekey()).isEqualTo(UPDATED_ARTICLEUNIQUEKEY);
        assertThat(testDataArticle.getRecordID()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testDataArticle.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testDataArticle.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testDataArticle.getPublishedDate()).isEqualTo(UPDATED_PUBLISHED_DATE);
        assertThat(testDataArticle.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testDataArticle.getContentHTML()).isEqualTo(UPDATED_CONTENT_HTML);
        assertThat(testDataArticle.getSummary()).isEqualTo(UPDATED_SUMMARY);
        assertThat(testDataArticle.getAuthor()).isEqualTo(UPDATED_AUTHOR);
        assertThat(testDataArticle.getPublisher()).isEqualTo(UPDATED_PUBLISHER);
        assertThat(testDataArticle.getSummaryHTML()).isEqualTo(UPDATED_SUMMARY_HTML);
        assertThat(testDataArticle.getRelevancy()).isEqualTo(UPDATED_RELEVANCY);
//        assertThat(testDataArticle.getPurpose()).isEqualTo(UPDATED_PURPOSE);
//        assertThat(testDataArticle.getPurposeType()).isEqualTo(UPDATED_PURPOSE_TYPE);
        assertThat(testDataArticle.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDataArticle.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);

        // Validate the DataArticle in Elasticsearch
        DataArticle dataArticleEs = dataArticleSearchRepository.findOne(testDataArticle.getId());
        assertThat(dataArticleEs).isEqualToComparingFieldByField(testDataArticle);
    }

    @Test
    @Transactional
    public void updateNonExistingDataArticle() throws Exception {
        int databaseSizeBeforeUpdate = dataArticleRepository.findAll().size();

        // Create the DataArticle

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDataArticleMockMvc.perform(put("/api/data-articles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataArticle)))
            .andExpect(status().isCreated());

        // Validate the DataArticle in the database
        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDataArticle() throws Exception {
        // Initialize the database
        dataArticleService.save(dataArticle);

        int databaseSizeBeforeDelete = dataArticleRepository.findAll().size();

        // Get the dataArticle
        restDataArticleMockMvc.perform(delete("/api/data-articles/{id}", dataArticle.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dataArticleExistsInEs = dataArticleSearchRepository.exists(dataArticle.getId());
        assertThat(dataArticleExistsInEs).isFalse();

        // Validate the database is empty
        List<DataArticle> dataArticleList = dataArticleRepository.findAll();
        assertThat(dataArticleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDataArticle() throws Exception {
        // Initialize the database
        dataArticleService.save(dataArticle);

        // Search the dataArticle
        restDataArticleMockMvc.perform(get("/api/_search/data-articles?query=id:" + dataArticle.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataArticle.getId().intValue())))
            .andExpect(jsonPath("$.[*].articleuniquekey").value(hasItem(DEFAULT_ARTICLEUNIQUEKEY.toString())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].publishedDate").value(hasItem(DEFAULT_PUBLISHED_DATE.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].contentHTML").value(hasItem(DEFAULT_CONTENT_HTML.toString())))
            .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
            .andExpect(jsonPath("$.[*].author").value(hasItem(DEFAULT_AUTHOR.toString())))
            .andExpect(jsonPath("$.[*].publisher").value(hasItem(DEFAULT_PUBLISHER.toString())))
            .andExpect(jsonPath("$.[*].summaryHTML").value(hasItem(DEFAULT_SUMMARY_HTML.toString())))
            .andExpect(jsonPath("$.[*].relevancy").value(hasItem(DEFAULT_RELEVANCY)))
            .andExpect(jsonPath("$.[*].purpose").value(hasItem(DEFAULT_PURPOSE.toString())))
            .andExpect(jsonPath("$.[*].purposeType").value(hasItem(DEFAULT_PURPOSE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataArticle.class);
        DataArticle dataArticle1 = new DataArticle();
        dataArticle1.setId(1L);
        DataArticle dataArticle2 = new DataArticle();
        dataArticle2.setId(dataArticle1.getId());
        assertThat(dataArticle1).isEqualTo(dataArticle2);
        dataArticle2.setId(2L);
        assertThat(dataArticle1).isNotEqualTo(dataArticle2);
        dataArticle1.setId(null);
        assertThat(dataArticle1).isNotEqualTo(dataArticle2);
    }
}
