package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;

import com.ai.mp.domain.DataProviderType;
import com.ai.mp.repository.DataProviderTypeRepository;
import com.ai.mp.repository.search.DataProviderTypeSearchRepository;
import com.ai.mp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DataProviderTypeResource REST controller.
 *
 * @see DataProviderTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class DataProviderTypeResourceIntTest {

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_EXPLANATION = "AAAAAAAAAA";
    private static final String UPDATED_EXPLANATION = "BBBBBBBBBB";

    private static final String DEFAULT_PROVIDER_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_PROVIDER_TYPE = "BBBBBBBBBB";

    private static final Integer DEFAULT_INACTIVE = 2;
    private static final Integer UPDATED_INACTIVE = 1;

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private DataProviderTypeRepository dataProviderTypeRepository;

    @Autowired
    private DataProviderTypeSearchRepository dataProviderTypeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDataProviderTypeMockMvc;

    private DataProviderType dataProviderType;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DataProviderTypeResource dataProviderTypeResource = new DataProviderTypeResource(dataProviderTypeRepository, dataProviderTypeSearchRepository);
        this.restDataProviderTypeMockMvc = MockMvcBuilders.standaloneSetup(dataProviderTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataProviderType createEntity(EntityManager em) {
        DataProviderType dataProviderType = new DataProviderType()
            .recordID(DEFAULT_RECORD_ID)
            .explanation(DEFAULT_EXPLANATION)
            .providerType(DEFAULT_PROVIDER_TYPE)
            .inactive(DEFAULT_INACTIVE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return dataProviderType;
    }

    @Before
    public void initTest() {
        dataProviderTypeSearchRepository.deleteAll();
        dataProviderType = createEntity(em);
    }

    @Test
    @Transactional
    public void createDataProviderType() throws Exception {
        int databaseSizeBeforeCreate = dataProviderTypeRepository.findAll().size();

        // Create the DataProviderType
        restDataProviderTypeMockMvc.perform(post("/api/data-provider-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProviderType)))
            .andExpect(status().isCreated());

        // Validate the DataProviderType in the database
        List<DataProviderType> dataProviderTypeList = dataProviderTypeRepository.findAll();
        assertThat(dataProviderTypeList).hasSize(databaseSizeBeforeCreate + 1);
        DataProviderType testDataProviderType = dataProviderTypeList.get(dataProviderTypeList.size() - 1);
        assertThat(testDataProviderType.getRecordID()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testDataProviderType.getExplanation()).isEqualTo(DEFAULT_EXPLANATION);
        assertThat(testDataProviderType.getProviderType()).isEqualTo(DEFAULT_PROVIDER_TYPE);
        assertThat(testDataProviderType.getInactive()).isEqualTo(DEFAULT_INACTIVE);
        assertThat(testDataProviderType.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDataProviderType.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);

        // Validate the DataProviderType in Elasticsearch
        DataProviderType dataProviderTypeEs = dataProviderTypeSearchRepository.findOne(testDataProviderType.getId());
        assertThat(dataProviderTypeEs).isEqualToComparingFieldByField(testDataProviderType);
    }

    @Test
    @Transactional
    public void createDataProviderTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dataProviderTypeRepository.findAll().size();

        // Create the DataProviderType with an existing ID
        dataProviderType.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataProviderTypeMockMvc.perform(post("/api/data-provider-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProviderType)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DataProviderType> dataProviderTypeList = dataProviderTypeRepository.findAll();
        assertThat(dataProviderTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkExplanationIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderTypeRepository.findAll().size();
        // set the field null
        dataProviderType.setExplanation(null);

        // Create the DataProviderType, which fails.

        restDataProviderTypeMockMvc.perform(post("/api/data-provider-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProviderType)))
            .andExpect(status().isBadRequest());

        List<DataProviderType> dataProviderTypeList = dataProviderTypeRepository.findAll();
        assertThat(dataProviderTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProviderTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderTypeRepository.findAll().size();
        // set the field null
        dataProviderType.setProviderType(null);

        // Create the DataProviderType, which fails.

        restDataProviderTypeMockMvc.perform(post("/api/data-provider-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProviderType)))
            .andExpect(status().isBadRequest());

        List<DataProviderType> dataProviderTypeList = dataProviderTypeRepository.findAll();
        assertThat(dataProviderTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInactiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderTypeRepository.findAll().size();
        // set the field null
        dataProviderType.setInactive(null);

        // Create the DataProviderType, which fails.

        restDataProviderTypeMockMvc.perform(post("/api/data-provider-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProviderType)))
            .andExpect(status().isBadRequest());

        List<DataProviderType> dataProviderTypeList = dataProviderTypeRepository.findAll();
        assertThat(dataProviderTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderTypeRepository.findAll().size();
        // set the field null
        dataProviderType.setCreatedDate(null);

        // Create the DataProviderType, which fails.

        restDataProviderTypeMockMvc.perform(post("/api/data-provider-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProviderType)))
            .andExpect(status().isBadRequest());

        List<DataProviderType> dataProviderTypeList = dataProviderTypeRepository.findAll();
        assertThat(dataProviderTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDataProviderTypes() throws Exception {
        // Initialize the database
        dataProviderTypeRepository.saveAndFlush(dataProviderType);

        // Get all the dataProviderTypeList
        restDataProviderTypeMockMvc.perform(get("/api/data-provider-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataProviderType.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].explanation").value(hasItem(DEFAULT_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].providerType").value(hasItem(DEFAULT_PROVIDER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getDataProviderType() throws Exception {
        // Initialize the database
        dataProviderTypeRepository.saveAndFlush(dataProviderType);

        // Get the dataProviderType
        restDataProviderTypeMockMvc.perform(get("/api/data-provider-types/{id}", dataProviderType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dataProviderType.getId().intValue()))
            .andExpect(jsonPath("$.recordID").value(DEFAULT_RECORD_ID.toString()))
            .andExpect(jsonPath("$.explanation").value(DEFAULT_EXPLANATION.toString()))
            .andExpect(jsonPath("$.providerType").value(DEFAULT_PROVIDER_TYPE.toString()))
            .andExpect(jsonPath("$.inactive").value(DEFAULT_INACTIVE))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDataProviderType() throws Exception {
        // Get the dataProviderType
        restDataProviderTypeMockMvc.perform(get("/api/data-provider-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDataProviderType() throws Exception {
        // Initialize the database
        dataProviderTypeRepository.saveAndFlush(dataProviderType);
        dataProviderTypeSearchRepository.save(dataProviderType);
        int databaseSizeBeforeUpdate = dataProviderTypeRepository.findAll().size();

        // Update the dataProviderType
        DataProviderType updatedDataProviderType = dataProviderTypeRepository.findOne(dataProviderType.getId());
        updatedDataProviderType
            .recordID(UPDATED_RECORD_ID)
            .explanation(UPDATED_EXPLANATION)
            .providerType(UPDATED_PROVIDER_TYPE)
            .inactive(UPDATED_INACTIVE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restDataProviderTypeMockMvc.perform(put("/api/data-provider-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDataProviderType)))
            .andExpect(status().isOk());

        // Validate the DataProviderType in the database
        List<DataProviderType> dataProviderTypeList = dataProviderTypeRepository.findAll();
        assertThat(dataProviderTypeList).hasSize(databaseSizeBeforeUpdate);
        DataProviderType testDataProviderType = dataProviderTypeList.get(dataProviderTypeList.size() - 1);
        assertThat(testDataProviderType.getRecordID()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testDataProviderType.getExplanation()).isEqualTo(UPDATED_EXPLANATION);
        assertThat(testDataProviderType.getProviderType()).isEqualTo(UPDATED_PROVIDER_TYPE);
        assertThat(testDataProviderType.getInactive()).isEqualTo(UPDATED_INACTIVE);
        assertThat(testDataProviderType.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDataProviderType.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);

        // Validate the DataProviderType in Elasticsearch
        DataProviderType dataProviderTypeEs = dataProviderTypeSearchRepository.findOne(testDataProviderType.getId());
        assertThat(dataProviderTypeEs).isEqualToComparingFieldByField(testDataProviderType);
    }

    @Test
    @Transactional
    public void updateNonExistingDataProviderType() throws Exception {
        int databaseSizeBeforeUpdate = dataProviderTypeRepository.findAll().size();

        // Create the DataProviderType

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDataProviderTypeMockMvc.perform(put("/api/data-provider-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProviderType)))
            .andExpect(status().isCreated());

        // Validate the DataProviderType in the database
        List<DataProviderType> dataProviderTypeList = dataProviderTypeRepository.findAll();
        assertThat(dataProviderTypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDataProviderType() throws Exception {
        // Initialize the database
        dataProviderTypeRepository.saveAndFlush(dataProviderType);
        dataProviderTypeSearchRepository.save(dataProviderType);
        int databaseSizeBeforeDelete = dataProviderTypeRepository.findAll().size();

        // Get the dataProviderType
        restDataProviderTypeMockMvc.perform(delete("/api/data-provider-types/{id}", dataProviderType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dataProviderTypeExistsInEs = dataProviderTypeSearchRepository.exists(dataProviderType.getId());
        assertThat(dataProviderTypeExistsInEs).isFalse();

        // Validate the database is empty
        List<DataProviderType> dataProviderTypeList = dataProviderTypeRepository.findAll();
        assertThat(dataProviderTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDataProviderType() throws Exception {
        // Initialize the database
        dataProviderTypeRepository.saveAndFlush(dataProviderType);
        dataProviderTypeSearchRepository.save(dataProviderType);

        // Search the dataProviderType
        restDataProviderTypeMockMvc.perform(get("/api/_search/data-provider-types?query=id:" + dataProviderType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataProviderType.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].explanation").value(hasItem(DEFAULT_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].providerType").value(hasItem(DEFAULT_PROVIDER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataProviderType.class);
        DataProviderType dataProviderType1 = new DataProviderType();
        dataProviderType1.setId(1L);
        DataProviderType dataProviderType2 = new DataProviderType();
        dataProviderType2.setId(dataProviderType1.getId());
        assertThat(dataProviderType1).isEqualTo(dataProviderType2);
        dataProviderType2.setId(2L);
        assertThat(dataProviderType1).isNotEqualTo(dataProviderType2);
        dataProviderType1.setId(null);
        assertThat(dataProviderType1).isNotEqualTo(dataProviderType2);
    }
}
