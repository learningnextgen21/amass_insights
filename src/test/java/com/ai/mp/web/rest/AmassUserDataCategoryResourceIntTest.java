package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;
import com.ai.mp.batch.DownloadLogo;
import com.ai.mp.domain.UserDataCategory;
import com.ai.mp.repository.UserDataCategoryRepository;
import com.ai.mp.repository.search.UserDataCategorySearchRepository;
import com.ai.mp.service.UserDataCategoryService;
import com.ai.mp.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AmassUserDataCategoryResource REST controller.
 *
 * @see UserDataCategoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class AmassUserDataCategoryResourceIntTest {

    private static final Long DEFAULT_USER_ID = 11L;
    private static final Long UPDATED_USER_ID = 10L;

    private static final Long DEFAULT_DATA_CATEGORY_ID = 11L;
    private static final Long UPDATED_DATA_CATEGORY_ID = 10L;

    private static final Integer DEFAULT_GET_NOTIFICATIONS = 2;
    private static final Integer UPDATED_GET_NOTIFICATIONS = 1;

    private static final String DEFAULT_NOTIFICATION_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_NOTIFICATION_TYPE = "BBBBBBBBBB";

    private static final Integer DEFAULT_INTEREST_TYPE = 1;
    private static final Integer UPDATED_INTEREST_TYPE = 2;

    private static final boolean DEFAULT_INACTIVE = false;
    private static final boolean UPDATED_INACTIVE = true;

    private static final LocalDateTime DEFAULT_CREATED_DATE = LocalDateTime.now();
    private static final LocalDateTime UPDATED_CREATED_DATE = LocalDateTime.now();

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private UserDataCategoryRepository amassUserDataCategoryRepository;

    @Autowired
    private UserDataCategorySearchRepository amassUserDataCategorySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private UserDataCategoryService amassUserDataCategoryService;

    private MockMvc restAmassUserDataCategoryMockMvc;

    private UserDataCategory amassUserDataCategory;

    private DownloadLogo downloadLogo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserDataCategoryResource amassUserDataCategoryResource = new UserDataCategoryResource(amassUserDataCategoryRepository, amassUserDataCategorySearchRepository, amassUserDataCategoryService,downloadLogo);
        this.restAmassUserDataCategoryMockMvc = MockMvcBuilders.standaloneSetup(amassUserDataCategoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserDataCategory createEntity(EntityManager em) {
        UserDataCategory amassUserDataCategory = new UserDataCategory()
            .userID(DEFAULT_USER_ID)
            .dataCategoryID(DEFAULT_DATA_CATEGORY_ID)
            .getNotifications(DEFAULT_GET_NOTIFICATIONS)
            .notificationType(DEFAULT_NOTIFICATION_TYPE)
            .interestType(DEFAULT_INTEREST_TYPE)
            .inactive(DEFAULT_INACTIVE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return amassUserDataCategory;
    }

    @Before
    public void initTest() {
        amassUserDataCategorySearchRepository.deleteAll();
        amassUserDataCategory = createEntity(em);
    }

    @Test
    @Transactional
    public void createAmassUserDataCategory() throws Exception {
        int databaseSizeBeforeCreate = amassUserDataCategoryRepository.findAll().size();

        // Create the AmassUserDataCategory
        restAmassUserDataCategoryMockMvc.perform(post("/api/amass-user-data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserDataCategory)))
            .andExpect(status().isCreated());

        // Validate the AmassUserDataCategory in the database
        List<UserDataCategory> amassUserDataCategoryList = amassUserDataCategoryRepository.findAll();
        assertThat(amassUserDataCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        UserDataCategory testAmassUserDataCategory = amassUserDataCategoryList.get(amassUserDataCategoryList.size() - 1);
        assertThat(testAmassUserDataCategory.getUserID()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testAmassUserDataCategory.getDataCategoryID()).isEqualTo(DEFAULT_DATA_CATEGORY_ID);
        assertThat(testAmassUserDataCategory.getGetNotifications()).isEqualTo(DEFAULT_GET_NOTIFICATIONS);
        assertThat(testAmassUserDataCategory.getNotificationType()).isEqualTo(DEFAULT_NOTIFICATION_TYPE);
        assertThat(testAmassUserDataCategory.getInterestType()).isEqualTo(DEFAULT_INTEREST_TYPE);
        assertThat(testAmassUserDataCategory.getInactive()).isEqualTo(DEFAULT_INACTIVE);
        assertThat(testAmassUserDataCategory.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testAmassUserDataCategory.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);

        // Validate the AmassUserDataCategory in Elasticsearch
        UserDataCategory amassUserDataCategoryEs = amassUserDataCategorySearchRepository.findOne(testAmassUserDataCategory.getId());
        assertThat(amassUserDataCategoryEs).isEqualToComparingFieldByField(testAmassUserDataCategory);
    }

    @Test
    @Transactional
    public void createAmassUserDataCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = amassUserDataCategoryRepository.findAll().size();

        // Create the AmassUserDataCategory with an existing ID
        amassUserDataCategory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAmassUserDataCategoryMockMvc.perform(post("/api/amass-user-data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserDataCategory)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<UserDataCategory> amassUserDataCategoryList = amassUserDataCategoryRepository.findAll();
        assertThat(amassUserDataCategoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUserIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserDataCategoryRepository.findAll().size();
        // set the field null
        amassUserDataCategory.setUserID(null);

        // Create the AmassUserDataCategory, which fails.

        restAmassUserDataCategoryMockMvc.perform(post("/api/amass-user-data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserDataCategory)))
            .andExpect(status().isBadRequest());

        List<UserDataCategory> amassUserDataCategoryList = amassUserDataCategoryRepository.findAll();
        assertThat(amassUserDataCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataCategoryIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserDataCategoryRepository.findAll().size();
        // set the field null
        amassUserDataCategory.setDataCategoryID(null);

        // Create the AmassUserDataCategory, which fails.

        restAmassUserDataCategoryMockMvc.perform(post("/api/amass-user-data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserDataCategory)))
            .andExpect(status().isBadRequest());

        List<UserDataCategory> amassUserDataCategoryList = amassUserDataCategoryRepository.findAll();
        assertThat(amassUserDataCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGetNotificationsIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserDataCategoryRepository.findAll().size();
        // set the field null
        amassUserDataCategory.setGetNotifications(null);

        // Create the AmassUserDataCategory, which fails.

        restAmassUserDataCategoryMockMvc.perform(post("/api/amass-user-data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserDataCategory)))
            .andExpect(status().isBadRequest());

        List<UserDataCategory> amassUserDataCategoryList = amassUserDataCategoryRepository.findAll();
        assertThat(amassUserDataCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNotificationTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserDataCategoryRepository.findAll().size();
        // set the field null
        amassUserDataCategory.setNotificationType(null);

        // Create the AmassUserDataCategory, which fails.

        restAmassUserDataCategoryMockMvc.perform(post("/api/amass-user-data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserDataCategory)))
            .andExpect(status().isBadRequest());

        List<UserDataCategory> amassUserDataCategoryList = amassUserDataCategoryRepository.findAll();
        assertThat(amassUserDataCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInterestTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserDataCategoryRepository.findAll().size();
        // set the field null
        amassUserDataCategory.setInterestType(null);

        // Create the AmassUserDataCategory, which fails.

        restAmassUserDataCategoryMockMvc.perform(post("/api/amass-user-data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserDataCategory)))
            .andExpect(status().isBadRequest());

        List<UserDataCategory> amassUserDataCategoryList = amassUserDataCategoryRepository.findAll();
        assertThat(amassUserDataCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInactiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserDataCategoryRepository.findAll().size();
        // set the field null
        amassUserDataCategory.setInactive(false);

        // Create the AmassUserDataCategory, which fails.

        restAmassUserDataCategoryMockMvc.perform(post("/api/amass-user-data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserDataCategory)))
            .andExpect(status().isBadRequest());

        List<UserDataCategory> amassUserDataCategoryList = amassUserDataCategoryRepository.findAll();
        assertThat(amassUserDataCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = amassUserDataCategoryRepository.findAll().size();
        // set the field null
        amassUserDataCategory.setCreatedDate(null);

        // Create the AmassUserDataCategory, which fails.

        restAmassUserDataCategoryMockMvc.perform(post("/api/amass-user-data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserDataCategory)))
            .andExpect(status().isBadRequest());

        List<UserDataCategory> amassUserDataCategoryList = amassUserDataCategoryRepository.findAll();
        assertThat(amassUserDataCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAmassUserDataCategories() throws Exception {
        // Initialize the database
        amassUserDataCategoryRepository.saveAndFlush(amassUserDataCategory);

        // Get all the amassUserDataCategoryList
        restAmassUserDataCategoryMockMvc.perform(get("/api/amass-user-data-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(amassUserDataCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].userID").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].dataCategoryID").value(hasItem(DEFAULT_DATA_CATEGORY_ID)))
            .andExpect(jsonPath("$.[*].getNotifications").value(hasItem(DEFAULT_GET_NOTIFICATIONS)))
            .andExpect(jsonPath("$.[*].notificationType").value(hasItem(DEFAULT_NOTIFICATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].interestType").value(hasItem(DEFAULT_INTEREST_TYPE.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getAmassUserDataCategory() throws Exception {
        // Initialize the database
        amassUserDataCategoryRepository.saveAndFlush(amassUserDataCategory);

        // Get the amassUserDataCategory
        restAmassUserDataCategoryMockMvc.perform(get("/api/amass-user-data-categories/{id}", amassUserDataCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(amassUserDataCategory.getId().intValue()))
            .andExpect(jsonPath("$.userID").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.dataCategoryID").value(DEFAULT_DATA_CATEGORY_ID))
            .andExpect(jsonPath("$.getNotifications").value(DEFAULT_GET_NOTIFICATIONS))
            .andExpect(jsonPath("$.notificationType").value(DEFAULT_NOTIFICATION_TYPE.toString()))
            .andExpect(jsonPath("$.interestType").value(DEFAULT_INTEREST_TYPE.toString()))
            .andExpect(jsonPath("$.inactive").value(DEFAULT_INACTIVE))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAmassUserDataCategory() throws Exception {
        // Get the amassUserDataCategory
        restAmassUserDataCategoryMockMvc.perform(get("/api/amass-user-data-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAmassUserDataCategory() throws Exception {
        // Initialize the database
        amassUserDataCategoryRepository.saveAndFlush(amassUserDataCategory);
        amassUserDataCategorySearchRepository.save(amassUserDataCategory);
        int databaseSizeBeforeUpdate = amassUserDataCategoryRepository.findAll().size();

        // Update the amassUserDataCategory
        UserDataCategory updatedAmassUserDataCategory = amassUserDataCategoryRepository.findOne(amassUserDataCategory.getId());
        updatedAmassUserDataCategory
            .userID(UPDATED_USER_ID)
            .dataCategoryID(UPDATED_DATA_CATEGORY_ID)
            .getNotifications(UPDATED_GET_NOTIFICATIONS)
            .notificationType(UPDATED_NOTIFICATION_TYPE)
            .interestType(UPDATED_INTEREST_TYPE)
            .inactive(UPDATED_INACTIVE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restAmassUserDataCategoryMockMvc.perform(put("/api/amass-user-data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAmassUserDataCategory)))
            .andExpect(status().isOk());

        // Validate the AmassUserDataCategory in the database
        List<UserDataCategory> amassUserDataCategoryList = amassUserDataCategoryRepository.findAll();
        assertThat(amassUserDataCategoryList).hasSize(databaseSizeBeforeUpdate);
        UserDataCategory testAmassUserDataCategory = amassUserDataCategoryList.get(amassUserDataCategoryList.size() - 1);
        assertThat(testAmassUserDataCategory.getUserID()).isEqualTo(UPDATED_USER_ID);
        assertThat(testAmassUserDataCategory.getDataCategoryID()).isEqualTo(UPDATED_DATA_CATEGORY_ID);
        assertThat(testAmassUserDataCategory.getGetNotifications()).isEqualTo(UPDATED_GET_NOTIFICATIONS);
        assertThat(testAmassUserDataCategory.getNotificationType()).isEqualTo(UPDATED_NOTIFICATION_TYPE);
        assertThat(testAmassUserDataCategory.getInterestType()).isEqualTo(UPDATED_INTEREST_TYPE);
        assertThat(testAmassUserDataCategory.getInactive()).isEqualTo(UPDATED_INACTIVE);
        assertThat(testAmassUserDataCategory.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testAmassUserDataCategory.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);

        // Validate the AmassUserDataCategory in Elasticsearch
        UserDataCategory amassUserDataCategoryEs = amassUserDataCategorySearchRepository.findOne(testAmassUserDataCategory.getId());
        assertThat(amassUserDataCategoryEs).isEqualToComparingFieldByField(testAmassUserDataCategory);
    }

    @Test
    @Transactional
    public void updateNonExistingAmassUserDataCategory() throws Exception {
        int databaseSizeBeforeUpdate = amassUserDataCategoryRepository.findAll().size();

        // Create the AmassUserDataCategory

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAmassUserDataCategoryMockMvc.perform(put("/api/amass-user-data-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(amassUserDataCategory)))
            .andExpect(status().isCreated());

        // Validate the AmassUserDataCategory in the database
        List<UserDataCategory> amassUserDataCategoryList = amassUserDataCategoryRepository.findAll();
        assertThat(amassUserDataCategoryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAmassUserDataCategory() throws Exception {
        // Initialize the database
        amassUserDataCategoryRepository.saveAndFlush(amassUserDataCategory);
        amassUserDataCategorySearchRepository.save(amassUserDataCategory);
        int databaseSizeBeforeDelete = amassUserDataCategoryRepository.findAll().size();

        // Get the amassUserDataCategory
        restAmassUserDataCategoryMockMvc.perform(delete("/api/amass-user-data-categories/{id}", amassUserDataCategory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean amassUserDataCategoryExistsInEs = amassUserDataCategorySearchRepository.exists(amassUserDataCategory.getId());
        assertThat(amassUserDataCategoryExistsInEs).isFalse();

        // Validate the database is empty
        List<UserDataCategory> amassUserDataCategoryList = amassUserDataCategoryRepository.findAll();
        assertThat(amassUserDataCategoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAmassUserDataCategory() throws Exception {
        // Initialize the database
        amassUserDataCategoryRepository.saveAndFlush(amassUserDataCategory);
        amassUserDataCategorySearchRepository.save(amassUserDataCategory);

        // Search the amassUserDataCategory
        restAmassUserDataCategoryMockMvc.perform(get("/api/_search/amass-user-data-categories?query=id:" + amassUserDataCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(amassUserDataCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].userID").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].dataCategoryID").value(hasItem(DEFAULT_DATA_CATEGORY_ID)))
            .andExpect(jsonPath("$.[*].getNotifications").value(hasItem(DEFAULT_GET_NOTIFICATIONS)))
            .andExpect(jsonPath("$.[*].notificationType").value(hasItem(DEFAULT_NOTIFICATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].interestType").value(hasItem(DEFAULT_INTEREST_TYPE.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserDataCategory.class);
        UserDataCategory amassUserDataCategory1 = new UserDataCategory();
        amassUserDataCategory1.setId(1L);
        UserDataCategory amassUserDataCategory2 = new UserDataCategory();
        amassUserDataCategory2.setId(amassUserDataCategory1.getId());
        assertThat(amassUserDataCategory1).isEqualTo(amassUserDataCategory2);
        amassUserDataCategory2.setId(2L);
        assertThat(amassUserDataCategory1).isNotEqualTo(amassUserDataCategory2);
        amassUserDataCategory1.setId(null);
        assertThat(amassUserDataCategory1).isNotEqualTo(amassUserDataCategory2);
    }
}
