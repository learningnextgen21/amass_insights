package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;
import com.ai.mp.batch.DBUtils;
import com.ai.mp.config.ApplicationProperties;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.LookupCode;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.repository.search.DataArticleSearchRepository;
import com.ai.mp.repository.search.DataCategorySearchRepository;
import com.ai.mp.repository.search.DataEventSearchRepository;
import com.ai.mp.repository.search.DataInvestmentMangerSearchRepository;
import com.ai.mp.repository.search.DataProviderSearchRepository;
import com.ai.mp.service.DataOrganizationService;
import com.ai.mp.service.DataProviderService;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.service.UserService;
import com.ai.mp.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DataProviderResource REST controller.
 *
 * @see DataProviderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class DataProviderResourceIntTest {

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_PROVIDER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PROVIDER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_WEBSITE = "AAAAAAAAAA";
    private static final String UPDATED_WEBSITE = "BBBBBBBBBB";

    private static final String DEFAULT_SHORT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_SHORT_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_LONG_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_LONG_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_MAIN_ASSET_CLASS = "AAAAAAAAAA";
    private static final String UPDATED_MAIN_ASSET_CLASS = "BBBBBBBBBB";

    private static final String DEFAULT_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_NOTES = "BBBBBBBBBB";

    private static final Integer DEFAULT_LIVE = 2;
    private static final Integer UPDATED_LIVE = 1;

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_DATA_UPDATE_FREQUENCY = "AAAAAAAAAA";
    private static final String UPDATED_DATA_UPDATE_FREQUENCY = "BBBBBBBBBB";

    private static final String DEFAULT_DATA_UPDATE_FREQUENCY_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_DATA_UPDATE_FREQUENCY_NOTES = "BBBBBBBBBB";

    private static final String DEFAULT_DATE_COLLECTION_BEGAN = "AAAAAAAAAA";
    private static final String UPDATED_DATE_COLLECTION_BEGAN = "BBBBBBBBBB";

    private static final String DEFAULT_DATE_COLLECTION_RANGE_EXPLANATION = "AAAAAAAAAA";
    private static final String UPDATED_DATE_COLLECTION_RANGE_EXPLANATION = "BBBBBBBBBB";

    private static final String DEFAULT_COLLECTION_METHODS_EXPLANATION = "AAAAAAAAAA";
    private static final String UPDATED_COLLECTION_METHODS_EXPLANATION = "BBBBBBBBBB";

    private static final String DEFAULT_POTENTIAL_DRAWBACKS = "AAAAAAAAAA";
    private static final String UPDATED_POTENTIAL_DRAWBACKS = "BBBBBBBBBB";

    private static final String DEFAULT_UNIQUE_VALUE_PROPS = "AAAAAAAAAA";
    private static final String UPDATED_UNIQUE_VALUE_PROPS = "BBBBBBBBBB";

    private static final String DEFAULT_HISTORICAL_DATE_RANGE = "AAAAAAAAAA";
    private static final String UPDATED_HISTORICAL_DATE_RANGE = "BBBBBBBBBB";

    private static final String DEFAULT_KEY_DATA_FIELDS = "AAAAAAAAAA";
    private static final String UPDATED_KEY_DATA_FIELDS = "BBBBBBBBBB";

    private static final String DEFAULT_SUMMARY = "AAAAAAAAAA";
    private static final String UPDATED_SUMMARY = "BBBBBBBBBB";

    private static final String DEFAULT_RESEARCH_METHODS_COMPLETED = "AAAAAAAAAA";
    private static final String UPDATED_RESEARCH_METHODS_COMPLETED = "BBBBBBBBBB";

    private static final String DEFAULT_INVESTORS_SCORE_WILLINGNESS = "AAAAAAAAAA";
    private static final String UPDATED_INVESTORS_SCORE_WILLINGNESS = "BBBBBBBBBB";

    private static final String DEFAULT_INVESTOR_CLIENTS = "AAAAAAAAAA";
    private static final String UPDATED_INVESTOR_CLIENTS = "BBBBBBBBBB";

    private static final String DEFAULT_HISTORICAL_DATE_RANGE_ESTIMATE = "AAAAAAAAAA";
    private static final String UPDATED_HISTORICAL_DATE_RANGE_ESTIMATE = "BBBBBBBBBB";

    private static final String DEFAULT_PUBLIC_COMPANIES_COVERED = "AAAAAAAAAA";
    private static final String UPDATED_PUBLIC_COMPANIES_COVERED = "BBBBBBBBBB";

    private static final String DEFAULT_UNIQUENESS_SCORE = "AAAAAAAAAA";
    private static final String UPDATED_UNIQUENESS_SCORE = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE_SCORE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE_SCORE = "BBBBBBBBBB";

    private static final String DEFAULT_READINESS_SCORE = "AAAAAAAAAA";
    private static final String UPDATED_READINESS_SCORE = "BBBBBBBBBB";

    private static final String DEFAULT_MARKETPLACE_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_MARKETPLACE_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_FREE_TRIAL_AVAILABLE = "AAAAAAAAAA";
    private static final String UPDATED_FREE_TRIAL_AVAILABLE = "BBBBBBBBBB";

    private static final String DEFAULT_PRODUCT_DETAILS = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCT_DETAILS = "BBBBBBBBBB";

    @Autowired
    private DataProviderRepository dataProviderRepository;

    @Autowired
    private DataProviderService dataProviderService;

    @Autowired
    private DataOrganizationService dataOrganizationService;

    @Autowired
    private DataProviderSearchRepository dataProviderSearchRepository;
    @Autowired
    private  DataEventSearchRepository dataEventSearchReopsitory;
    @Autowired
    private  DataArticleSearchRepository dataArticleSearchRepository;
    @Autowired
    private  DataCategorySearchRepository dataCategorySearchRepository;
    
    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDataProviderMockMvc;

    private DataProvider dataProvider;
    @Autowired
    private UserService userService;

    @Autowired
    private ElasticsearchIndexService elasticsearchIndexService;

    @Autowired
    private DBUtils dbutils;

    @Autowired
    private ApplicationProperties applicationProperties;
    @Autowired
    private DataInvestmentMangerSearchRepository dataInvestmentManagerSearchRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DataProviderResource dataProviderResource = new DataProviderResource(userService, dataProviderRepository, elasticsearchIndexService,dataOrganizationService, dataProviderService, dbutils, applicationProperties, dataProviderSearchRepository, dataEventSearchReopsitory, dataArticleSearchRepository, dataCategorySearchRepository,dataInvestmentManagerSearchRepository);
        this.restDataProviderMockMvc = MockMvcBuilders.standaloneSetup(dataProviderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataProvider createEntity(EntityManager em) {
        DataProvider dataProvider = new DataProvider()
            .recordID(DEFAULT_RECORD_ID)
            .providerName(DEFAULT_PROVIDER_NAME)
            .website(DEFAULT_WEBSITE)
            .shortDescription(DEFAULT_SHORT_DESCRIPTION)
            .longDescription(DEFAULT_LONG_DESCRIPTION)
            // .mainAssetClass(DEFAULT_MAIN_ASSET_CLASS)
            .notes(DEFAULT_NOTES)
            //.live(DEFAULT_LIVE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE)
            //.dataUpdateFrequency(new LookupCode (DEFAULT_DATA_UPDATE_FREQUENCY))
            .dataUpdateFrequencyNotes(DEFAULT_DATA_UPDATE_FREQUENCY_NOTES)
            //.dateCollectionBegan(DEFAULT_DATE_COLLECTION_BEGAN)
            .dateCollectionRangeExplanation(DEFAULT_DATE_COLLECTION_RANGE_EXPLANATION)
            .collectionMethodsExplanation(DEFAULT_COLLECTION_METHODS_EXPLANATION)
            .potentialDrawbacks(DEFAULT_POTENTIAL_DRAWBACKS)
            .uniqueValueProps(DEFAULT_UNIQUE_VALUE_PROPS)
         // .historicalDateRange(DEFAULT_HISTORICAL_DATE_RANGE)
            .keyDataFields(DEFAULT_KEY_DATA_FIELDS)
            .summary(DEFAULT_SUMMARY)
          //  .researchMethodsCompleted(DEFAULT_RESEARCH_METHODS_COMPLETED)
            .investorsScoreWillingness(new LookupCode(DEFAULT_INVESTORS_SCORE_WILLINGNESS))
            .investor_clients(DEFAULT_INVESTOR_CLIENTS)
           // .historicalDateRangeEstimate(DEFAULT_HISTORICAL_DATE_RANGE_ESTIMATE)
            .publicCompaniesCovered(DEFAULT_PUBLIC_COMPANIES_COVERED)
            .uniquenessScore(new LookupCode(DEFAULT_UNIQUENESS_SCORE))
            .valueScore(new LookupCode(DEFAULT_VALUE_SCORE))
            .readinessScore(new LookupCode(DEFAULT_READINESS_SCORE))
            //.marketplaceStatus(DEFAULT_MARKETPLACE_STATUS)
           // .freeTrialAvailable(DEFAULT_FREE_TRIAL_AVAILABLE)
            .productDetails(DEFAULT_PRODUCT_DETAILS);
        return dataProvider;
    }

    @Before
    public void initTest() {
        dataProviderSearchRepository.deleteAll();
        dataProvider = createEntity(em);
    }

    @Test
    @Transactional
    public void createDataProvider() throws Exception {
        int databaseSizeBeforeCreate = dataProviderRepository.findAll().size();

        // Create the DataProvider
        restDataProviderMockMvc.perform(post("/api/data-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProvider)))
            .andExpect(status().isCreated());

        // Validate the DataProvider in the database
        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeCreate + 1);
        DataProvider testDataProvider = dataProviderList.get(dataProviderList.size() - 1);
        assertThat(testDataProvider.getRecordID()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testDataProvider.getProviderName()).isEqualTo(DEFAULT_PROVIDER_NAME);
        assertThat(testDataProvider.getWebsite()).isEqualTo(DEFAULT_WEBSITE);
        assertThat(testDataProvider.getShortDescription()).isEqualTo(DEFAULT_SHORT_DESCRIPTION);
        assertThat(testDataProvider.getLongDescription()).isEqualTo(DEFAULT_LONG_DESCRIPTION);
        assertThat(testDataProvider.getMainAssetClass()).isEqualTo(DEFAULT_MAIN_ASSET_CLASS);
        assertThat(testDataProvider.getNotes()).isEqualTo(DEFAULT_NOTES);
       // assertThat(testDataProvider.getLive()).isEqualTo(DEFAULT_LIVE);
        assertThat(testDataProvider.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDataProvider.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testDataProvider.getDataUpdateFrequency()).isEqualTo(DEFAULT_DATA_UPDATE_FREQUENCY);
        assertThat(testDataProvider.getDataUpdateFrequencyNotes()).isEqualTo(DEFAULT_DATA_UPDATE_FREQUENCY_NOTES);
        assertThat(testDataProvider.getDateCollectionBegan()).isEqualTo(DEFAULT_DATE_COLLECTION_BEGAN);
        assertThat(testDataProvider.getDateCollectionRangeExplanation()).isEqualTo(DEFAULT_DATE_COLLECTION_RANGE_EXPLANATION);
        assertThat(testDataProvider.getCollectionMethodsExplanation()).isEqualTo(DEFAULT_COLLECTION_METHODS_EXPLANATION);
        assertThat(testDataProvider.getPotentialDrawbacks()).isEqualTo(DEFAULT_POTENTIAL_DRAWBACKS);
        assertThat(testDataProvider.getUniqueValueProps()).isEqualTo(DEFAULT_UNIQUE_VALUE_PROPS);
     //   assertThat(testDataProvider.getHistoricalDateRange()).isEqualTo(DEFAULT_HISTORICAL_DATE_RANGE);
        assertThat(testDataProvider.getKeyDataFields()).isEqualTo(DEFAULT_KEY_DATA_FIELDS);
        assertThat(testDataProvider.getSummary()).isEqualTo(DEFAULT_SUMMARY);
        assertThat(testDataProvider.getResearchMethodsCompleted()).isEqualTo(DEFAULT_RESEARCH_METHODS_COMPLETED);
        assertThat(testDataProvider.getScoreInvestorsWillingness()).isEqualTo(DEFAULT_INVESTORS_SCORE_WILLINGNESS);
        assertThat(testDataProvider.getInvestor_clients()).isEqualTo(DEFAULT_INVESTOR_CLIENTS);
       // assertThat(testDataProvider.getHistoricalDateRangeEstimate()).isEqualTo(DEFAULT_HISTORICAL_DATE_RANGE_ESTIMATE);
        assertThat(testDataProvider.getPublicCompaniesCovered()).isEqualTo(DEFAULT_PUBLIC_COMPANIES_COVERED);
        assertThat(testDataProvider.getScoreUniqueness()).isEqualTo(DEFAULT_UNIQUENESS_SCORE);
        assertThat(testDataProvider.getScoreValue()).isEqualTo(DEFAULT_VALUE_SCORE);
        assertThat(testDataProvider.getScoreReadiness()).isEqualTo(DEFAULT_READINESS_SCORE);
        assertThat(testDataProvider.getMarketplaceStatus()).isEqualTo(DEFAULT_MARKETPLACE_STATUS);
        assertThat(testDataProvider.getFreeTrialAvailable()).isEqualTo(DEFAULT_FREE_TRIAL_AVAILABLE);
        assertThat(testDataProvider.getProductDetails()).isEqualTo(DEFAULT_PRODUCT_DETAILS);

        // Validate the DataProvider in Elasticsearch
        DataProvider dataProviderEs = dataProviderSearchRepository.findOne(testDataProvider.getId());
        assertThat(dataProviderEs).isEqualToComparingFieldByField(testDataProvider);
    }

    @Test
    @Transactional
    public void createDataProviderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dataProviderRepository.findAll().size();

        // Create the DataProvider with an existing ID
        dataProvider.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataProviderMockMvc.perform(post("/api/data-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProvider)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkProviderNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderRepository.findAll().size();
        // set the field null
        dataProvider.setProviderName(null);

        // Create the DataProvider, which fails.

        restDataProviderMockMvc.perform(post("/api/data-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProvider)))
            .andExpect(status().isBadRequest());

        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkWebsiteIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderRepository.findAll().size();
        // set the field null
        dataProvider.setWebsite(null);

        // Create the DataProvider, which fails.

        restDataProviderMockMvc.perform(post("/api/data-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProvider)))
            .andExpect(status().isBadRequest());

        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkShortDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderRepository.findAll().size();
        // set the field null
        dataProvider.setShortDescription(null);

        // Create the DataProvider, which fails.

        restDataProviderMockMvc.perform(post("/api/data-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProvider)))
            .andExpect(status().isBadRequest());

        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLongDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderRepository.findAll().size();
        // set the field null
        dataProvider.setLongDescription(null);

        // Create the DataProvider, which fails.

        restDataProviderMockMvc.perform(post("/api/data-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProvider)))
            .andExpect(status().isBadRequest());

        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMainAssetClassIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderRepository.findAll().size();
        // set the field null
        dataProvider.setMainAssetClass(null);

        // Create the DataProvider, which fails.

        restDataProviderMockMvc.perform(post("/api/data-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProvider)))
            .andExpect(status().isBadRequest());

        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNotesIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderRepository.findAll().size();
        // set the field null
        dataProvider.setNotes(null);

        // Create the DataProvider, which fails.

        restDataProviderMockMvc.perform(post("/api/data-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProvider)))
            .andExpect(status().isBadRequest());

        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderRepository.findAll().size();
        // set the field null
       // dataProvider.setLive(null);

        // Create the DataProvider, which fails.

        restDataProviderMockMvc.perform(post("/api/data-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProvider)))
            .andExpect(status().isBadRequest());

        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataProviderRepository.findAll().size();
        // set the field null
        dataProvider.setCreatedDate(null);

        // Create the DataProvider, which fails.

        restDataProviderMockMvc.perform(post("/api/data-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProvider)))
            .andExpect(status().isBadRequest());

        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDataProviders() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList
        restDataProviderMockMvc.perform(get("/api/data-providers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataProvider.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].providerName").value(hasItem(DEFAULT_PROVIDER_NAME.toString())))
            .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE.toString())))
            .andExpect(jsonPath("$.[*].shortDescription").value(hasItem(DEFAULT_SHORT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].longDescription").value(hasItem(DEFAULT_LONG_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].mainAssetClass").value(hasItem(DEFAULT_MAIN_ASSET_CLASS.toString())))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES.toString())))
            .andExpect(jsonPath("$.[*].live").value(hasItem(DEFAULT_LIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].dataUpdateFrequency").value(hasItem(DEFAULT_DATA_UPDATE_FREQUENCY.toString())))
            .andExpect(jsonPath("$.[*].dataUpdateFrequencyNotes").value(hasItem(DEFAULT_DATA_UPDATE_FREQUENCY_NOTES.toString())))
            .andExpect(jsonPath("$.[*].dateCollectionBegan").value(hasItem(DEFAULT_DATE_COLLECTION_BEGAN.toString())))
            .andExpect(jsonPath("$.[*].dateCollectionRangeExplanation").value(hasItem(DEFAULT_DATE_COLLECTION_RANGE_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].collectionMethodsExplanation").value(hasItem(DEFAULT_COLLECTION_METHODS_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].potentialDrawbacks").value(hasItem(DEFAULT_POTENTIAL_DRAWBACKS.toString())))
            .andExpect(jsonPath("$.[*].uniqueValueProps").value(hasItem(DEFAULT_UNIQUE_VALUE_PROPS.toString())))
            .andExpect(jsonPath("$.[*].historicalDateRange").value(hasItem(DEFAULT_HISTORICAL_DATE_RANGE.toString())))
            .andExpect(jsonPath("$.[*].keyDataFields").value(hasItem(DEFAULT_KEY_DATA_FIELDS.toString())))
            .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
            .andExpect(jsonPath("$.[*].researchMethodsCompleted").value(hasItem(DEFAULT_RESEARCH_METHODS_COMPLETED.toString())))
            .andExpect(jsonPath("$.[*].investorsScoreWillingness").value(hasItem(DEFAULT_INVESTORS_SCORE_WILLINGNESS.toString())))
            .andExpect(jsonPath("$.[*].investor_clients").value(hasItem(DEFAULT_INVESTOR_CLIENTS.toString())))
            .andExpect(jsonPath("$.[*].historicalDateRangeEstimate").value(hasItem(DEFAULT_HISTORICAL_DATE_RANGE_ESTIMATE.toString())))
            .andExpect(jsonPath("$.[*].publicCompaniesCovered").value(hasItem(DEFAULT_PUBLIC_COMPANIES_COVERED.toString())))
            .andExpect(jsonPath("$.[*].uniquenessScore").value(hasItem(DEFAULT_UNIQUENESS_SCORE.toString())))
            .andExpect(jsonPath("$.[*].valueScore").value(hasItem(DEFAULT_VALUE_SCORE.toString())))
            .andExpect(jsonPath("$.[*].readinessScore").value(hasItem(DEFAULT_READINESS_SCORE.toString())))
            .andExpect(jsonPath("$.[*].marketplaceStatus").value(hasItem(DEFAULT_MARKETPLACE_STATUS.toString())))
            .andExpect(jsonPath("$.[*].freeTrialAvailable").value(hasItem(DEFAULT_FREE_TRIAL_AVAILABLE.toString())))
            .andExpect(jsonPath("$.[*].productDetails").value(hasItem(DEFAULT_PRODUCT_DETAILS.toString())));
    }

    @Test
    @Transactional
    public void getDataProvider() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get the dataProvider
        restDataProviderMockMvc.perform(get("/api/data-providers/{id}", dataProvider.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dataProvider.getId().intValue()))
            .andExpect(jsonPath("$.recordID").value(DEFAULT_RECORD_ID.toString()))
            .andExpect(jsonPath("$.providerName").value(DEFAULT_PROVIDER_NAME.toString()))
            .andExpect(jsonPath("$.website").value(DEFAULT_WEBSITE.toString()))
            .andExpect(jsonPath("$.shortDescription").value(DEFAULT_SHORT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.longDescription").value(DEFAULT_LONG_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.mainAssetClass").value(DEFAULT_MAIN_ASSET_CLASS.toString()))
            .andExpect(jsonPath("$.notes").value(DEFAULT_NOTES.toString()))
            .andExpect(jsonPath("$.live").value(DEFAULT_LIVE))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.dataUpdateFrequency").value(DEFAULT_DATA_UPDATE_FREQUENCY.toString()))
            .andExpect(jsonPath("$.dataUpdateFrequencyNotes").value(DEFAULT_DATA_UPDATE_FREQUENCY_NOTES.toString()))
            .andExpect(jsonPath("$.dateCollectionBegan").value(DEFAULT_DATE_COLLECTION_BEGAN.toString()))
            .andExpect(jsonPath("$.dateCollectionRangeExplanation").value(DEFAULT_DATE_COLLECTION_RANGE_EXPLANATION.toString()))
            .andExpect(jsonPath("$.collectionMethodsExplanation").value(DEFAULT_COLLECTION_METHODS_EXPLANATION.toString()))
            .andExpect(jsonPath("$.potentialDrawbacks").value(DEFAULT_POTENTIAL_DRAWBACKS.toString()))
            .andExpect(jsonPath("$.uniqueValueProps").value(DEFAULT_UNIQUE_VALUE_PROPS.toString()))
            .andExpect(jsonPath("$.historicalDateRange").value(DEFAULT_HISTORICAL_DATE_RANGE.toString()))
            .andExpect(jsonPath("$.keyDataFields").value(DEFAULT_KEY_DATA_FIELDS.toString()))
            .andExpect(jsonPath("$.summary").value(DEFAULT_SUMMARY.toString()))
            .andExpect(jsonPath("$.researchMethodsCompleted").value(DEFAULT_RESEARCH_METHODS_COMPLETED.toString()))
            .andExpect(jsonPath("$.investorsScoreWillingness").value(DEFAULT_INVESTORS_SCORE_WILLINGNESS.toString()))
            .andExpect(jsonPath("$.investor_clients").value(DEFAULT_INVESTOR_CLIENTS.toString()))
            .andExpect(jsonPath("$.historicalDateRangeEstimate").value(DEFAULT_HISTORICAL_DATE_RANGE_ESTIMATE.toString()))
            .andExpect(jsonPath("$.publicCompaniesCovered").value(DEFAULT_PUBLIC_COMPANIES_COVERED.toString()))
            .andExpect(jsonPath("$.uniquenessScore").value(DEFAULT_UNIQUENESS_SCORE.toString()))
            .andExpect(jsonPath("$.valueScore").value(DEFAULT_VALUE_SCORE.toString()))
            .andExpect(jsonPath("$.readinessScore").value(DEFAULT_READINESS_SCORE.toString()))
            .andExpect(jsonPath("$.marketplaceStatus").value(DEFAULT_MARKETPLACE_STATUS.toString()))
            .andExpect(jsonPath("$.freeTrialAvailable").value(DEFAULT_FREE_TRIAL_AVAILABLE.toString()))
            .andExpect(jsonPath("$.productDetails").value(DEFAULT_PRODUCT_DETAILS.toString()));
    }

    @Test
    @Transactional
    public void getAllDataProvidersByRecordIDIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where recordID equals to DEFAULT_RECORD_ID
        defaultDataProviderShouldBeFound("recordID.equals=" + DEFAULT_RECORD_ID);

        // Get all the dataProviderList where recordID equals to UPDATED_RECORD_ID
        defaultDataProviderShouldNotBeFound("recordID.equals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByRecordIDIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where recordID in DEFAULT_RECORD_ID or UPDATED_RECORD_ID
        defaultDataProviderShouldBeFound("recordID.in=" + DEFAULT_RECORD_ID + "," + UPDATED_RECORD_ID);

        // Get all the dataProviderList where recordID equals to UPDATED_RECORD_ID
        defaultDataProviderShouldNotBeFound("recordID.in=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByRecordIDIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where recordID is not null
        defaultDataProviderShouldBeFound("recordID.specified=true");

        // Get all the dataProviderList where recordID is null
        defaultDataProviderShouldNotBeFound("recordID.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByProviderNameIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where providerName equals to DEFAULT_PROVIDER_NAME
        defaultDataProviderShouldBeFound("providerName.equals=" + DEFAULT_PROVIDER_NAME);

        // Get all the dataProviderList where providerName equals to UPDATED_PROVIDER_NAME
        defaultDataProviderShouldNotBeFound("providerName.equals=" + UPDATED_PROVIDER_NAME);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByProviderNameIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where providerName in DEFAULT_PROVIDER_NAME or UPDATED_PROVIDER_NAME
        defaultDataProviderShouldBeFound("providerName.in=" + DEFAULT_PROVIDER_NAME + "," + UPDATED_PROVIDER_NAME);

        // Get all the dataProviderList where providerName equals to UPDATED_PROVIDER_NAME
        defaultDataProviderShouldNotBeFound("providerName.in=" + UPDATED_PROVIDER_NAME);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByProviderNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where providerName is not null
        defaultDataProviderShouldBeFound("providerName.specified=true");

        // Get all the dataProviderList where providerName is null
        defaultDataProviderShouldNotBeFound("providerName.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByWebsiteIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where website equals to DEFAULT_WEBSITE
        defaultDataProviderShouldBeFound("website.equals=" + DEFAULT_WEBSITE);

        // Get all the dataProviderList where website equals to UPDATED_WEBSITE
        defaultDataProviderShouldNotBeFound("website.equals=" + UPDATED_WEBSITE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByWebsiteIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where website in DEFAULT_WEBSITE or UPDATED_WEBSITE
        defaultDataProviderShouldBeFound("website.in=" + DEFAULT_WEBSITE + "," + UPDATED_WEBSITE);

        // Get all the dataProviderList where website equals to UPDATED_WEBSITE
        defaultDataProviderShouldNotBeFound("website.in=" + UPDATED_WEBSITE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByWebsiteIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where website is not null
        defaultDataProviderShouldBeFound("website.specified=true");

        // Get all the dataProviderList where website is null
        defaultDataProviderShouldNotBeFound("website.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByMainAssetClassIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where mainAssetClass equals to DEFAULT_MAIN_ASSET_CLASS
        defaultDataProviderShouldBeFound("mainAssetClass.equals=" + DEFAULT_MAIN_ASSET_CLASS);

        // Get all the dataProviderList where mainAssetClass equals to UPDATED_MAIN_ASSET_CLASS
        defaultDataProviderShouldNotBeFound("mainAssetClass.equals=" + UPDATED_MAIN_ASSET_CLASS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByMainAssetClassIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where mainAssetClass in DEFAULT_MAIN_ASSET_CLASS or UPDATED_MAIN_ASSET_CLASS
        defaultDataProviderShouldBeFound("mainAssetClass.in=" + DEFAULT_MAIN_ASSET_CLASS + "," + UPDATED_MAIN_ASSET_CLASS);

        // Get all the dataProviderList where mainAssetClass equals to UPDATED_MAIN_ASSET_CLASS
        defaultDataProviderShouldNotBeFound("mainAssetClass.in=" + UPDATED_MAIN_ASSET_CLASS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByMainAssetClassIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where mainAssetClass is not null
        defaultDataProviderShouldBeFound("mainAssetClass.specified=true");

        // Get all the dataProviderList where mainAssetClass is null
        defaultDataProviderShouldNotBeFound("mainAssetClass.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByLiveIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where live equals to DEFAULT_LIVE
        defaultDataProviderShouldBeFound("live.equals=" + DEFAULT_LIVE);

        // Get all the dataProviderList where live equals to UPDATED_LIVE
        defaultDataProviderShouldNotBeFound("live.equals=" + UPDATED_LIVE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByLiveIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where live in DEFAULT_LIVE or UPDATED_LIVE
        defaultDataProviderShouldBeFound("live.in=" + DEFAULT_LIVE + "," + UPDATED_LIVE);

        // Get all the dataProviderList where live equals to UPDATED_LIVE
        defaultDataProviderShouldNotBeFound("live.in=" + UPDATED_LIVE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByLiveIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where live is not null
        defaultDataProviderShouldBeFound("live.specified=true");

        // Get all the dataProviderList where live is null
        defaultDataProviderShouldNotBeFound("live.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByLiveIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where live greater than or equals to DEFAULT_LIVE
        defaultDataProviderShouldBeFound("live.greaterOrEqualThan=" + DEFAULT_LIVE);

        // Get all the dataProviderList where live greater than or equals to (DEFAULT_LIVE + 1)
        defaultDataProviderShouldNotBeFound("live.greaterOrEqualThan=" + (DEFAULT_LIVE + 1));
    }

    @Test
    @Transactional
    public void getAllDataProvidersByLiveIsLessThanSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where live less than or equals to DEFAULT_LIVE
        defaultDataProviderShouldNotBeFound("live.lessThan=" + DEFAULT_LIVE);

        // Get all the dataProviderList where live less than or equals to (DEFAULT_LIVE + 1)
        defaultDataProviderShouldBeFound("live.lessThan=" + (DEFAULT_LIVE + 1));
    }


    @Test
    @Transactional
    public void getAllDataProvidersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where createdDate equals to DEFAULT_CREATED_DATE
        defaultDataProviderShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the dataProviderList where createdDate equals to UPDATED_CREATED_DATE
        defaultDataProviderShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultDataProviderShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the dataProviderList where createdDate equals to UPDATED_CREATED_DATE
        defaultDataProviderShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where createdDate is not null
        defaultDataProviderShouldBeFound("createdDate.specified=true");

        // Get all the dataProviderList where createdDate is null
        defaultDataProviderShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where createdDate greater than or equals to DEFAULT_CREATED_DATE
        defaultDataProviderShouldBeFound("createdDate.greaterOrEqualThan=" + DEFAULT_CREATED_DATE);

        // Get all the dataProviderList where createdDate greater than or equals to UPDATED_CREATED_DATE
        defaultDataProviderShouldNotBeFound("createdDate.greaterOrEqualThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where createdDate less than or equals to DEFAULT_CREATED_DATE
        defaultDataProviderShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the dataProviderList where createdDate less than or equals to UPDATED_CREATED_DATE
        defaultDataProviderShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllDataProvidersByUpdatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where updatedDate equals to DEFAULT_UPDATED_DATE
        defaultDataProviderShouldBeFound("updatedDate.equals=" + DEFAULT_UPDATED_DATE);

        // Get all the dataProviderList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultDataProviderShouldNotBeFound("updatedDate.equals=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByUpdatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where updatedDate in DEFAULT_UPDATED_DATE or UPDATED_UPDATED_DATE
        defaultDataProviderShouldBeFound("updatedDate.in=" + DEFAULT_UPDATED_DATE + "," + UPDATED_UPDATED_DATE);

        // Get all the dataProviderList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultDataProviderShouldNotBeFound("updatedDate.in=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByUpdatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where updatedDate is not null
        defaultDataProviderShouldBeFound("updatedDate.specified=true");

        // Get all the dataProviderList where updatedDate is null
        defaultDataProviderShouldNotBeFound("updatedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByDataUpdateFrequencyIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where dataUpdateFrequency equals to DEFAULT_DATA_UPDATE_FREQUENCY
        defaultDataProviderShouldBeFound("dataUpdateFrequency.equals=" + DEFAULT_DATA_UPDATE_FREQUENCY);

        // Get all the dataProviderList where dataUpdateFrequency equals to UPDATED_DATA_UPDATE_FREQUENCY
        defaultDataProviderShouldNotBeFound("dataUpdateFrequency.equals=" + UPDATED_DATA_UPDATE_FREQUENCY);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByDataUpdateFrequencyIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where dataUpdateFrequency in DEFAULT_DATA_UPDATE_FREQUENCY or UPDATED_DATA_UPDATE_FREQUENCY
        defaultDataProviderShouldBeFound("dataUpdateFrequency.in=" + DEFAULT_DATA_UPDATE_FREQUENCY + "," + UPDATED_DATA_UPDATE_FREQUENCY);

        // Get all the dataProviderList where dataUpdateFrequency equals to UPDATED_DATA_UPDATE_FREQUENCY
        defaultDataProviderShouldNotBeFound("dataUpdateFrequency.in=" + UPDATED_DATA_UPDATE_FREQUENCY);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByDataUpdateFrequencyIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where dataUpdateFrequency is not null
        defaultDataProviderShouldBeFound("dataUpdateFrequency.specified=true");

        // Get all the dataProviderList where dataUpdateFrequency is null
        defaultDataProviderShouldNotBeFound("dataUpdateFrequency.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByDataUpdateFrequencyNotesIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where dataUpdateFrequencyNotes equals to DEFAULT_DATA_UPDATE_FREQUENCY_NOTES
        defaultDataProviderShouldBeFound("dataUpdateFrequencyNotes.equals=" + DEFAULT_DATA_UPDATE_FREQUENCY_NOTES);

        // Get all the dataProviderList where dataUpdateFrequencyNotes equals to UPDATED_DATA_UPDATE_FREQUENCY_NOTES
        defaultDataProviderShouldNotBeFound("dataUpdateFrequencyNotes.equals=" + UPDATED_DATA_UPDATE_FREQUENCY_NOTES);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByDataUpdateFrequencyNotesIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where dataUpdateFrequencyNotes in DEFAULT_DATA_UPDATE_FREQUENCY_NOTES or UPDATED_DATA_UPDATE_FREQUENCY_NOTES
        defaultDataProviderShouldBeFound("dataUpdateFrequencyNotes.in=" + DEFAULT_DATA_UPDATE_FREQUENCY_NOTES + "," + UPDATED_DATA_UPDATE_FREQUENCY_NOTES);

        // Get all the dataProviderList where dataUpdateFrequencyNotes equals to UPDATED_DATA_UPDATE_FREQUENCY_NOTES
        defaultDataProviderShouldNotBeFound("dataUpdateFrequencyNotes.in=" + UPDATED_DATA_UPDATE_FREQUENCY_NOTES);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByDataUpdateFrequencyNotesIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where dataUpdateFrequencyNotes is not null
        defaultDataProviderShouldBeFound("dataUpdateFrequencyNotes.specified=true");

        // Get all the dataProviderList where dataUpdateFrequencyNotes is null
        defaultDataProviderShouldNotBeFound("dataUpdateFrequencyNotes.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByDateCollectionBeganIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where dateCollectionBegan equals to DEFAULT_DATE_COLLECTION_BEGAN
        defaultDataProviderShouldBeFound("dateCollectionBegan.equals=" + DEFAULT_DATE_COLLECTION_BEGAN);

        // Get all the dataProviderList where dateCollectionBegan equals to UPDATED_DATE_COLLECTION_BEGAN
        defaultDataProviderShouldNotBeFound("dateCollectionBegan.equals=" + UPDATED_DATE_COLLECTION_BEGAN);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByDateCollectionBeganIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where dateCollectionBegan in DEFAULT_DATE_COLLECTION_BEGAN or UPDATED_DATE_COLLECTION_BEGAN
        defaultDataProviderShouldBeFound("dateCollectionBegan.in=" + DEFAULT_DATE_COLLECTION_BEGAN + "," + UPDATED_DATE_COLLECTION_BEGAN);

        // Get all the dataProviderList where dateCollectionBegan equals to UPDATED_DATE_COLLECTION_BEGAN
        defaultDataProviderShouldNotBeFound("dateCollectionBegan.in=" + UPDATED_DATE_COLLECTION_BEGAN);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByDateCollectionBeganIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where dateCollectionBegan is not null
        defaultDataProviderShouldBeFound("dateCollectionBegan.specified=true");

        // Get all the dataProviderList where dateCollectionBegan is null
        defaultDataProviderShouldNotBeFound("dateCollectionBegan.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByDateCollectionRangeExplanationIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where dateCollectionRangeExplanation equals to DEFAULT_DATE_COLLECTION_RANGE_EXPLANATION
        defaultDataProviderShouldBeFound("dateCollectionRangeExplanation.equals=" + DEFAULT_DATE_COLLECTION_RANGE_EXPLANATION);

        // Get all the dataProviderList where dateCollectionRangeExplanation equals to UPDATED_DATE_COLLECTION_RANGE_EXPLANATION
        defaultDataProviderShouldNotBeFound("dateCollectionRangeExplanation.equals=" + UPDATED_DATE_COLLECTION_RANGE_EXPLANATION);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByDateCollectionRangeExplanationIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where dateCollectionRangeExplanation in DEFAULT_DATE_COLLECTION_RANGE_EXPLANATION or UPDATED_DATE_COLLECTION_RANGE_EXPLANATION
        defaultDataProviderShouldBeFound("dateCollectionRangeExplanation.in=" + DEFAULT_DATE_COLLECTION_RANGE_EXPLANATION + "," + UPDATED_DATE_COLLECTION_RANGE_EXPLANATION);

        // Get all the dataProviderList where dateCollectionRangeExplanation equals to UPDATED_DATE_COLLECTION_RANGE_EXPLANATION
        defaultDataProviderShouldNotBeFound("dateCollectionRangeExplanation.in=" + UPDATED_DATE_COLLECTION_RANGE_EXPLANATION);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByDateCollectionRangeExplanationIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where dateCollectionRangeExplanation is not null
        defaultDataProviderShouldBeFound("dateCollectionRangeExplanation.specified=true");

        // Get all the dataProviderList where dateCollectionRangeExplanation is null
        defaultDataProviderShouldNotBeFound("dateCollectionRangeExplanation.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByPotentialDrawbacksIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where potentialDrawbacks equals to DEFAULT_POTENTIAL_DRAWBACKS
        defaultDataProviderShouldBeFound("potentialDrawbacks.equals=" + DEFAULT_POTENTIAL_DRAWBACKS);

        // Get all the dataProviderList where potentialDrawbacks equals to UPDATED_POTENTIAL_DRAWBACKS
        defaultDataProviderShouldNotBeFound("potentialDrawbacks.equals=" + UPDATED_POTENTIAL_DRAWBACKS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByPotentialDrawbacksIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where potentialDrawbacks in DEFAULT_POTENTIAL_DRAWBACKS or UPDATED_POTENTIAL_DRAWBACKS
        defaultDataProviderShouldBeFound("potentialDrawbacks.in=" + DEFAULT_POTENTIAL_DRAWBACKS + "," + UPDATED_POTENTIAL_DRAWBACKS);

        // Get all the dataProviderList where potentialDrawbacks equals to UPDATED_POTENTIAL_DRAWBACKS
        defaultDataProviderShouldNotBeFound("potentialDrawbacks.in=" + UPDATED_POTENTIAL_DRAWBACKS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByPotentialDrawbacksIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where potentialDrawbacks is not null
        defaultDataProviderShouldBeFound("potentialDrawbacks.specified=true");

        // Get all the dataProviderList where potentialDrawbacks is null
        defaultDataProviderShouldNotBeFound("potentialDrawbacks.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByUniqueValuePropsIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where uniqueValueProps equals to DEFAULT_UNIQUE_VALUE_PROPS
        defaultDataProviderShouldBeFound("uniqueValueProps.equals=" + DEFAULT_UNIQUE_VALUE_PROPS);

        // Get all the dataProviderList where uniqueValueProps equals to UPDATED_UNIQUE_VALUE_PROPS
        defaultDataProviderShouldNotBeFound("uniqueValueProps.equals=" + UPDATED_UNIQUE_VALUE_PROPS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByUniqueValuePropsIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where uniqueValueProps in DEFAULT_UNIQUE_VALUE_PROPS or UPDATED_UNIQUE_VALUE_PROPS
        defaultDataProviderShouldBeFound("uniqueValueProps.in=" + DEFAULT_UNIQUE_VALUE_PROPS + "," + UPDATED_UNIQUE_VALUE_PROPS);

        // Get all the dataProviderList where uniqueValueProps equals to UPDATED_UNIQUE_VALUE_PROPS
        defaultDataProviderShouldNotBeFound("uniqueValueProps.in=" + UPDATED_UNIQUE_VALUE_PROPS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByUniqueValuePropsIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where uniqueValueProps is not null
        defaultDataProviderShouldBeFound("uniqueValueProps.specified=true");

        // Get all the dataProviderList where uniqueValueProps is null
        defaultDataProviderShouldNotBeFound("uniqueValueProps.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByHistoricalDateRangeIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where historicalDateRange equals to DEFAULT_HISTORICAL_DATE_RANGE
        defaultDataProviderShouldBeFound("historicalDateRange.equals=" + DEFAULT_HISTORICAL_DATE_RANGE);

        // Get all the dataProviderList where historicalDateRange equals to UPDATED_HISTORICAL_DATE_RANGE
        defaultDataProviderShouldNotBeFound("historicalDateRange.equals=" + UPDATED_HISTORICAL_DATE_RANGE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByHistoricalDateRangeIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where historicalDateRange in DEFAULT_HISTORICAL_DATE_RANGE or UPDATED_HISTORICAL_DATE_RANGE
        defaultDataProviderShouldBeFound("historicalDateRange.in=" + DEFAULT_HISTORICAL_DATE_RANGE + "," + UPDATED_HISTORICAL_DATE_RANGE);

        // Get all the dataProviderList where historicalDateRange equals to UPDATED_HISTORICAL_DATE_RANGE
        defaultDataProviderShouldNotBeFound("historicalDateRange.in=" + UPDATED_HISTORICAL_DATE_RANGE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByHistoricalDateRangeIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where historicalDateRange is not null
        defaultDataProviderShouldBeFound("historicalDateRange.specified=true");

        // Get all the dataProviderList where historicalDateRange is null
        defaultDataProviderShouldNotBeFound("historicalDateRange.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByKeyDataFieldsIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where keyDataFields equals to DEFAULT_KEY_DATA_FIELDS
        defaultDataProviderShouldBeFound("keyDataFields.equals=" + DEFAULT_KEY_DATA_FIELDS);

        // Get all the dataProviderList where keyDataFields equals to UPDATED_KEY_DATA_FIELDS
        defaultDataProviderShouldNotBeFound("keyDataFields.equals=" + UPDATED_KEY_DATA_FIELDS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByKeyDataFieldsIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where keyDataFields in DEFAULT_KEY_DATA_FIELDS or UPDATED_KEY_DATA_FIELDS
        defaultDataProviderShouldBeFound("keyDataFields.in=" + DEFAULT_KEY_DATA_FIELDS + "," + UPDATED_KEY_DATA_FIELDS);

        // Get all the dataProviderList where keyDataFields equals to UPDATED_KEY_DATA_FIELDS
        defaultDataProviderShouldNotBeFound("keyDataFields.in=" + UPDATED_KEY_DATA_FIELDS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByKeyDataFieldsIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where keyDataFields is not null
        defaultDataProviderShouldBeFound("keyDataFields.specified=true");

        // Get all the dataProviderList where keyDataFields is null
        defaultDataProviderShouldNotBeFound("keyDataFields.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersBySummaryIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where summary equals to DEFAULT_SUMMARY
        defaultDataProviderShouldBeFound("summary.equals=" + DEFAULT_SUMMARY);

        // Get all the dataProviderList where summary equals to UPDATED_SUMMARY
        defaultDataProviderShouldNotBeFound("summary.equals=" + UPDATED_SUMMARY);
    }

    @Test
    @Transactional
    public void getAllDataProvidersBySummaryIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where summary in DEFAULT_SUMMARY or UPDATED_SUMMARY
        defaultDataProviderShouldBeFound("summary.in=" + DEFAULT_SUMMARY + "," + UPDATED_SUMMARY);

        // Get all the dataProviderList where summary equals to UPDATED_SUMMARY
        defaultDataProviderShouldNotBeFound("summary.in=" + UPDATED_SUMMARY);
    }

    @Test
    @Transactional
    public void getAllDataProvidersBySummaryIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where summary is not null
        defaultDataProviderShouldBeFound("summary.specified=true");

        // Get all the dataProviderList where summary is null
        defaultDataProviderShouldNotBeFound("summary.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByResearchMethodsCompletedIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where researchMethodsCompleted equals to DEFAULT_RESEARCH_METHODS_COMPLETED
        defaultDataProviderShouldBeFound("researchMethodsCompleted.equals=" + DEFAULT_RESEARCH_METHODS_COMPLETED);

        // Get all the dataProviderList where researchMethodsCompleted equals to UPDATED_RESEARCH_METHODS_COMPLETED
        defaultDataProviderShouldNotBeFound("researchMethodsCompleted.equals=" + UPDATED_RESEARCH_METHODS_COMPLETED);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByResearchMethodsCompletedIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where researchMethodsCompleted in DEFAULT_RESEARCH_METHODS_COMPLETED or UPDATED_RESEARCH_METHODS_COMPLETED
        defaultDataProviderShouldBeFound("researchMethodsCompleted.in=" + DEFAULT_RESEARCH_METHODS_COMPLETED + "," + UPDATED_RESEARCH_METHODS_COMPLETED);

        // Get all the dataProviderList where researchMethodsCompleted equals to UPDATED_RESEARCH_METHODS_COMPLETED
        defaultDataProviderShouldNotBeFound("researchMethodsCompleted.in=" + UPDATED_RESEARCH_METHODS_COMPLETED);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByResearchMethodsCompletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where researchMethodsCompleted is not null
        defaultDataProviderShouldBeFound("researchMethodsCompleted.specified=true");

        // Get all the dataProviderList where researchMethodsCompleted is null
        defaultDataProviderShouldNotBeFound("researchMethodsCompleted.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByInvestorsScoreWillingnessIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where investorsScoreWillingness equals to DEFAULT_INVESTORS_SCORE_WILLINGNESS
        defaultDataProviderShouldBeFound("investorsScoreWillingness.equals=" + DEFAULT_INVESTORS_SCORE_WILLINGNESS);

        // Get all the dataProviderList where investorsScoreWillingness equals to UPDATED_INVESTORS_SCORE_WILLINGNESS
        defaultDataProviderShouldNotBeFound("investorsScoreWillingness.equals=" + UPDATED_INVESTORS_SCORE_WILLINGNESS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByInvestorsScoreWillingnessIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where investorsScoreWillingness in DEFAULT_INVESTORS_SCORE_WILLINGNESS or UPDATED_INVESTORS_SCORE_WILLINGNESS
        defaultDataProviderShouldBeFound("investorsScoreWillingness.in=" + DEFAULT_INVESTORS_SCORE_WILLINGNESS + "," + UPDATED_INVESTORS_SCORE_WILLINGNESS);

        // Get all the dataProviderList where investorsScoreWillingness equals to UPDATED_INVESTORS_SCORE_WILLINGNESS
        defaultDataProviderShouldNotBeFound("investorsScoreWillingness.in=" + UPDATED_INVESTORS_SCORE_WILLINGNESS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByInvestorsScoreWillingnessIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where investorsScoreWillingness is not null
        defaultDataProviderShouldBeFound("investorsScoreWillingness.specified=true");

        // Get all the dataProviderList where investorsScoreWillingness is null
        defaultDataProviderShouldNotBeFound("investorsScoreWillingness.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByInvestor_clientsIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where investor_clients equals to DEFAULT_INVESTOR_CLIENTS
        defaultDataProviderShouldBeFound("investor_clients.equals=" + DEFAULT_INVESTOR_CLIENTS);

        // Get all the dataProviderList where investor_clients equals to UPDATED_INVESTOR_CLIENTS
        defaultDataProviderShouldNotBeFound("investor_clients.equals=" + UPDATED_INVESTOR_CLIENTS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByInvestor_clientsIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where investor_clients in DEFAULT_INVESTOR_CLIENTS or UPDATED_INVESTOR_CLIENTS
        defaultDataProviderShouldBeFound("investor_clients.in=" + DEFAULT_INVESTOR_CLIENTS + "," + UPDATED_INVESTOR_CLIENTS);

        // Get all the dataProviderList where investor_clients equals to UPDATED_INVESTOR_CLIENTS
        defaultDataProviderShouldNotBeFound("investor_clients.in=" + UPDATED_INVESTOR_CLIENTS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByInvestor_clientsIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where investor_clients is not null
        defaultDataProviderShouldBeFound("investor_clients.specified=true");

        // Get all the dataProviderList where investor_clients is null
        defaultDataProviderShouldNotBeFound("investor_clients.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByHistoricalDateRangeEstimateIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where historicalDateRangeEstimate equals to DEFAULT_HISTORICAL_DATE_RANGE_ESTIMATE
        defaultDataProviderShouldBeFound("historicalDateRangeEstimate.equals=" + DEFAULT_HISTORICAL_DATE_RANGE_ESTIMATE);

        // Get all the dataProviderList where historicalDateRangeEstimate equals to UPDATED_HISTORICAL_DATE_RANGE_ESTIMATE
        defaultDataProviderShouldNotBeFound("historicalDateRangeEstimate.equals=" + UPDATED_HISTORICAL_DATE_RANGE_ESTIMATE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByHistoricalDateRangeEstimateIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where historicalDateRangeEstimate in DEFAULT_HISTORICAL_DATE_RANGE_ESTIMATE or UPDATED_HISTORICAL_DATE_RANGE_ESTIMATE
        defaultDataProviderShouldBeFound("historicalDateRangeEstimate.in=" + DEFAULT_HISTORICAL_DATE_RANGE_ESTIMATE + "," + UPDATED_HISTORICAL_DATE_RANGE_ESTIMATE);

        // Get all the dataProviderList where historicalDateRangeEstimate equals to UPDATED_HISTORICAL_DATE_RANGE_ESTIMATE
        defaultDataProviderShouldNotBeFound("historicalDateRangeEstimate.in=" + UPDATED_HISTORICAL_DATE_RANGE_ESTIMATE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByHistoricalDateRangeEstimateIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where historicalDateRangeEstimate is not null
        defaultDataProviderShouldBeFound("historicalDateRangeEstimate.specified=true");

        // Get all the dataProviderList where historicalDateRangeEstimate is null
        defaultDataProviderShouldNotBeFound("historicalDateRangeEstimate.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByPublicCompaniesCoveredIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where publicCompaniesCovered equals to DEFAULT_PUBLIC_COMPANIES_COVERED
        defaultDataProviderShouldBeFound("publicCompaniesCovered.equals=" + DEFAULT_PUBLIC_COMPANIES_COVERED);

        // Get all the dataProviderList where publicCompaniesCovered equals to UPDATED_PUBLIC_COMPANIES_COVERED
        defaultDataProviderShouldNotBeFound("publicCompaniesCovered.equals=" + UPDATED_PUBLIC_COMPANIES_COVERED);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByPublicCompaniesCoveredIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where publicCompaniesCovered in DEFAULT_PUBLIC_COMPANIES_COVERED or UPDATED_PUBLIC_COMPANIES_COVERED
        defaultDataProviderShouldBeFound("publicCompaniesCovered.in=" + DEFAULT_PUBLIC_COMPANIES_COVERED + "," + UPDATED_PUBLIC_COMPANIES_COVERED);

        // Get all the dataProviderList where publicCompaniesCovered equals to UPDATED_PUBLIC_COMPANIES_COVERED
        defaultDataProviderShouldNotBeFound("publicCompaniesCovered.in=" + UPDATED_PUBLIC_COMPANIES_COVERED);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByPublicCompaniesCoveredIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where publicCompaniesCovered is not null
        defaultDataProviderShouldBeFound("publicCompaniesCovered.specified=true");

        // Get all the dataProviderList where publicCompaniesCovered is null
        defaultDataProviderShouldNotBeFound("publicCompaniesCovered.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByUniquenessScoreIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where uniquenessScore equals to DEFAULT_UNIQUENESS_SCORE
        defaultDataProviderShouldBeFound("uniquenessScore.equals=" + DEFAULT_UNIQUENESS_SCORE);

        // Get all the dataProviderList where uniquenessScore equals to UPDATED_UNIQUENESS_SCORE
        defaultDataProviderShouldNotBeFound("uniquenessScore.equals=" + UPDATED_UNIQUENESS_SCORE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByUniquenessScoreIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where uniquenessScore in DEFAULT_UNIQUENESS_SCORE or UPDATED_UNIQUENESS_SCORE
        defaultDataProviderShouldBeFound("uniquenessScore.in=" + DEFAULT_UNIQUENESS_SCORE + "," + UPDATED_UNIQUENESS_SCORE);

        // Get all the dataProviderList where uniquenessScore equals to UPDATED_UNIQUENESS_SCORE
        defaultDataProviderShouldNotBeFound("uniquenessScore.in=" + UPDATED_UNIQUENESS_SCORE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByUniquenessScoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where uniquenessScore is not null
        defaultDataProviderShouldBeFound("uniquenessScore.specified=true");

        // Get all the dataProviderList where uniquenessScore is null
        defaultDataProviderShouldNotBeFound("uniquenessScore.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByValueScoreIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where valueScore equals to DEFAULT_VALUE_SCORE
        defaultDataProviderShouldBeFound("valueScore.equals=" + DEFAULT_VALUE_SCORE);

        // Get all the dataProviderList where valueScore equals to UPDATED_VALUE_SCORE
        defaultDataProviderShouldNotBeFound("valueScore.equals=" + UPDATED_VALUE_SCORE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByValueScoreIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where valueScore in DEFAULT_VALUE_SCORE or UPDATED_VALUE_SCORE
        defaultDataProviderShouldBeFound("valueScore.in=" + DEFAULT_VALUE_SCORE + "," + UPDATED_VALUE_SCORE);

        // Get all the dataProviderList where valueScore equals to UPDATED_VALUE_SCORE
        defaultDataProviderShouldNotBeFound("valueScore.in=" + UPDATED_VALUE_SCORE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByValueScoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where valueScore is not null
        defaultDataProviderShouldBeFound("valueScore.specified=true");

        // Get all the dataProviderList where valueScore is null
        defaultDataProviderShouldNotBeFound("valueScore.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByReadinessScoreIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where readinessScore equals to DEFAULT_READINESS_SCORE
        defaultDataProviderShouldBeFound("readinessScore.equals=" + DEFAULT_READINESS_SCORE);

        // Get all the dataProviderList where readinessScore equals to UPDATED_READINESS_SCORE
        defaultDataProviderShouldNotBeFound("readinessScore.equals=" + UPDATED_READINESS_SCORE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByReadinessScoreIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where readinessScore in DEFAULT_READINESS_SCORE or UPDATED_READINESS_SCORE
        defaultDataProviderShouldBeFound("readinessScore.in=" + DEFAULT_READINESS_SCORE + "," + UPDATED_READINESS_SCORE);

        // Get all the dataProviderList where readinessScore equals to UPDATED_READINESS_SCORE
        defaultDataProviderShouldNotBeFound("readinessScore.in=" + UPDATED_READINESS_SCORE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByReadinessScoreIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where readinessScore is not null
        defaultDataProviderShouldBeFound("readinessScore.specified=true");

        // Get all the dataProviderList where readinessScore is null
        defaultDataProviderShouldNotBeFound("readinessScore.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByMarketplaceStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where marketplaceStatus equals to DEFAULT_MARKETPLACE_STATUS
        defaultDataProviderShouldBeFound("marketplaceStatus.equals=" + DEFAULT_MARKETPLACE_STATUS);

        // Get all the dataProviderList where marketplaceStatus equals to UPDATED_MARKETPLACE_STATUS
        defaultDataProviderShouldNotBeFound("marketplaceStatus.equals=" + UPDATED_MARKETPLACE_STATUS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByMarketplaceStatusIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where marketplaceStatus in DEFAULT_MARKETPLACE_STATUS or UPDATED_MARKETPLACE_STATUS
        defaultDataProviderShouldBeFound("marketplaceStatus.in=" + DEFAULT_MARKETPLACE_STATUS + "," + UPDATED_MARKETPLACE_STATUS);

        // Get all the dataProviderList where marketplaceStatus equals to UPDATED_MARKETPLACE_STATUS
        defaultDataProviderShouldNotBeFound("marketplaceStatus.in=" + UPDATED_MARKETPLACE_STATUS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByMarketplaceStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where marketplaceStatus is not null
        defaultDataProviderShouldBeFound("marketplaceStatus.specified=true");

        // Get all the dataProviderList where marketplaceStatus is null
        defaultDataProviderShouldNotBeFound("marketplaceStatus.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByFreeTrialAvailableIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where freeTrialAvailable equals to DEFAULT_FREE_TRIAL_AVAILABLE
        defaultDataProviderShouldBeFound("freeTrialAvailable.equals=" + DEFAULT_FREE_TRIAL_AVAILABLE);

        // Get all the dataProviderList where freeTrialAvailable equals to UPDATED_FREE_TRIAL_AVAILABLE
        defaultDataProviderShouldNotBeFound("freeTrialAvailable.equals=" + UPDATED_FREE_TRIAL_AVAILABLE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByFreeTrialAvailableIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where freeTrialAvailable in DEFAULT_FREE_TRIAL_AVAILABLE or UPDATED_FREE_TRIAL_AVAILABLE
        defaultDataProviderShouldBeFound("freeTrialAvailable.in=" + DEFAULT_FREE_TRIAL_AVAILABLE + "," + UPDATED_FREE_TRIAL_AVAILABLE);

        // Get all the dataProviderList where freeTrialAvailable equals to UPDATED_FREE_TRIAL_AVAILABLE
        defaultDataProviderShouldNotBeFound("freeTrialAvailable.in=" + UPDATED_FREE_TRIAL_AVAILABLE);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByFreeTrialAvailableIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where freeTrialAvailable is not null
        defaultDataProviderShouldBeFound("freeTrialAvailable.specified=true");

        // Get all the dataProviderList where freeTrialAvailable is null
        defaultDataProviderShouldNotBeFound("freeTrialAvailable.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataProvidersByProductDetailsIsEqualToSomething() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where productDetails equals to DEFAULT_PRODUCT_DETAILS
        defaultDataProviderShouldBeFound("productDetails.equals=" + DEFAULT_PRODUCT_DETAILS);

        // Get all the dataProviderList where productDetails equals to UPDATED_PRODUCT_DETAILS
        defaultDataProviderShouldNotBeFound("productDetails.equals=" + UPDATED_PRODUCT_DETAILS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByProductDetailsIsInShouldWork() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where productDetails in DEFAULT_PRODUCT_DETAILS or UPDATED_PRODUCT_DETAILS
        defaultDataProviderShouldBeFound("productDetails.in=" + DEFAULT_PRODUCT_DETAILS + "," + UPDATED_PRODUCT_DETAILS);

        // Get all the dataProviderList where productDetails equals to UPDATED_PRODUCT_DETAILS
        defaultDataProviderShouldNotBeFound("productDetails.in=" + UPDATED_PRODUCT_DETAILS);
    }

    @Test
    @Transactional
    public void getAllDataProvidersByProductDetailsIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataProviderRepository.saveAndFlush(dataProvider);

        // Get all the dataProviderList where productDetails is not null
        defaultDataProviderShouldBeFound("productDetails.specified=true");

        // Get all the dataProviderList where productDetails is null
        defaultDataProviderShouldNotBeFound("productDetails.specified=false");
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultDataProviderShouldBeFound(String filter) throws Exception {
        restDataProviderMockMvc.perform(get("/api/data-providers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataProvider.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].providerName").value(hasItem(DEFAULT_PROVIDER_NAME.toString())))
            .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE.toString())))
            .andExpect(jsonPath("$.[*].shortDescription").value(hasItem(DEFAULT_SHORT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].longDescription").value(hasItem(DEFAULT_LONG_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].mainAssetClass").value(hasItem(DEFAULT_MAIN_ASSET_CLASS.toString())))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES.toString())))
            .andExpect(jsonPath("$.[*].live").value(hasItem(DEFAULT_LIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].dataUpdateFrequency").value(hasItem(DEFAULT_DATA_UPDATE_FREQUENCY.toString())))
            .andExpect(jsonPath("$.[*].dataUpdateFrequencyNotes").value(hasItem(DEFAULT_DATA_UPDATE_FREQUENCY_NOTES.toString())))
            .andExpect(jsonPath("$.[*].dateCollectionBegan").value(hasItem(DEFAULT_DATE_COLLECTION_BEGAN.toString())))
            .andExpect(jsonPath("$.[*].dateCollectionRangeExplanation").value(hasItem(DEFAULT_DATE_COLLECTION_RANGE_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].collectionMethodsExplanation").value(hasItem(DEFAULT_COLLECTION_METHODS_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].potentialDrawbacks").value(hasItem(DEFAULT_POTENTIAL_DRAWBACKS.toString())))
            .andExpect(jsonPath("$.[*].uniqueValueProps").value(hasItem(DEFAULT_UNIQUE_VALUE_PROPS.toString())))
            .andExpect(jsonPath("$.[*].historicalDateRange").value(hasItem(DEFAULT_HISTORICAL_DATE_RANGE.toString())))
            .andExpect(jsonPath("$.[*].keyDataFields").value(hasItem(DEFAULT_KEY_DATA_FIELDS.toString())))
            .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
            .andExpect(jsonPath("$.[*].researchMethodsCompleted").value(hasItem(DEFAULT_RESEARCH_METHODS_COMPLETED.toString())))
            .andExpect(jsonPath("$.[*].investorsScoreWillingness").value(hasItem(DEFAULT_INVESTORS_SCORE_WILLINGNESS.toString())))
            .andExpect(jsonPath("$.[*].investor_clients").value(hasItem(DEFAULT_INVESTOR_CLIENTS.toString())))
            .andExpect(jsonPath("$.[*].historicalDateRangeEstimate").value(hasItem(DEFAULT_HISTORICAL_DATE_RANGE_ESTIMATE.toString())))
            .andExpect(jsonPath("$.[*].publicCompaniesCovered").value(hasItem(DEFAULT_PUBLIC_COMPANIES_COVERED.toString())))
            .andExpect(jsonPath("$.[*].uniquenessScore").value(hasItem(DEFAULT_UNIQUENESS_SCORE.toString())))
            .andExpect(jsonPath("$.[*].valueScore").value(hasItem(DEFAULT_VALUE_SCORE.toString())))
            .andExpect(jsonPath("$.[*].readinessScore").value(hasItem(DEFAULT_READINESS_SCORE.toString())))
            .andExpect(jsonPath("$.[*].marketplaceStatus").value(hasItem(DEFAULT_MARKETPLACE_STATUS.toString())))
            .andExpect(jsonPath("$.[*].freeTrialAvailable").value(hasItem(DEFAULT_FREE_TRIAL_AVAILABLE.toString())))
            .andExpect(jsonPath("$.[*].productDetails").value(hasItem(DEFAULT_PRODUCT_DETAILS.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultDataProviderShouldNotBeFound(String filter) throws Exception {
        restDataProviderMockMvc.perform(get("/api/data-providers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingDataProvider() throws Exception {
        // Get the dataProvider
        restDataProviderMockMvc.perform(get("/api/data-providers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDataProvider() throws Exception {
        // Initialize the database
        dataProviderService.save(dataProvider);

        int databaseSizeBeforeUpdate = dataProviderRepository.findAll().size();

        // Update the dataProvider
        DataProvider updatedDataProvider = dataProviderRepository.findOne(dataProvider.getId());
        updatedDataProvider
            .recordID(UPDATED_RECORD_ID)
            .providerName(UPDATED_PROVIDER_NAME)
            .website(UPDATED_WEBSITE)
            .shortDescription(UPDATED_SHORT_DESCRIPTION)
            .longDescription(UPDATED_LONG_DESCRIPTION)
//            .mainAssetClass(UPDATED_MAIN_ASSET_CLASS)
            .notes(UPDATED_NOTES)
            //.live(UPDATED_LIVE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE)
            //.dataUpdateFrequency(new LookupCode(UPDATED_DATA_UPDATE_FREQUENCY))
            .dataUpdateFrequencyNotes(UPDATED_DATA_UPDATE_FREQUENCY_NOTES)
            //.dateCollectionBegan(UPDATED_DATE_COLLECTION_BEGAN)
            .dateCollectionRangeExplanation(UPDATED_DATE_COLLECTION_RANGE_EXPLANATION)
            .collectionMethodsExplanation(UPDATED_COLLECTION_METHODS_EXPLANATION)
            .potentialDrawbacks(UPDATED_POTENTIAL_DRAWBACKS)
            .uniqueValueProps(UPDATED_UNIQUE_VALUE_PROPS)
         //   .historicalDateRange(UPDATED_HISTORICAL_DATE_RANGE)
            .keyDataFields(UPDATED_KEY_DATA_FIELDS)
            .summary(UPDATED_SUMMARY)
           // .researchMethodsCompleted(UPDATED_RESEARCH_METHODS_COMPLETED)
            .investorsScoreWillingness(new LookupCode(UPDATED_INVESTORS_SCORE_WILLINGNESS))
            .investor_clients(UPDATED_INVESTOR_CLIENTS)
           // .historicalDateRangeEstimate(UPDATED_HISTORICAL_DATE_RANGE_ESTIMATE)
            .publicCompaniesCovered(UPDATED_PUBLIC_COMPANIES_COVERED)
            .uniquenessScore(new LookupCode(UPDATED_UNIQUENESS_SCORE))
            .valueScore(new LookupCode(UPDATED_VALUE_SCORE))
            .readinessScore(new LookupCode(UPDATED_READINESS_SCORE))
            //.marketplaceStatus(UPDATED_MARKETPLACE_STATUS)
            //.freeTrialAvailable(UPDATED_FREE_TRIAL_AVAILABLE)
            .productDetails(UPDATED_PRODUCT_DETAILS);

        restDataProviderMockMvc.perform(put("/api/data-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDataProvider)))
            .andExpect(status().isOk());

        // Validate the DataProvider in the database
        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeUpdate);
        DataProvider testDataProvider = dataProviderList.get(dataProviderList.size() - 1);
        assertThat(testDataProvider.getRecordID()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testDataProvider.getProviderName()).isEqualTo(UPDATED_PROVIDER_NAME);
        assertThat(testDataProvider.getWebsite()).isEqualTo(UPDATED_WEBSITE);
        assertThat(testDataProvider.getShortDescription()).isEqualTo(UPDATED_SHORT_DESCRIPTION);
        assertThat(testDataProvider.getLongDescription()).isEqualTo(UPDATED_LONG_DESCRIPTION);
        assertThat(testDataProvider.getMainAssetClass()).isEqualTo(UPDATED_MAIN_ASSET_CLASS);
        assertThat(testDataProvider.getNotes()).isEqualTo(UPDATED_NOTES);
       // assertThat(testDataProvider.getLive()).isEqualTo(UPDATED_LIVE);
        assertThat(testDataProvider.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDataProvider.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testDataProvider.getDataUpdateFrequency()).isEqualTo(UPDATED_DATA_UPDATE_FREQUENCY);
        assertThat(testDataProvider.getDataUpdateFrequencyNotes()).isEqualTo(UPDATED_DATA_UPDATE_FREQUENCY_NOTES);
        assertThat(testDataProvider.getDateCollectionBegan()).isEqualTo(UPDATED_DATE_COLLECTION_BEGAN);
        assertThat(testDataProvider.getDateCollectionRangeExplanation()).isEqualTo(UPDATED_DATE_COLLECTION_RANGE_EXPLANATION);
        assertThat(testDataProvider.getCollectionMethodsExplanation()).isEqualTo(UPDATED_COLLECTION_METHODS_EXPLANATION);
        assertThat(testDataProvider.getPotentialDrawbacks()).isEqualTo(UPDATED_POTENTIAL_DRAWBACKS);
        assertThat(testDataProvider.getUniqueValueProps()).isEqualTo(UPDATED_UNIQUE_VALUE_PROPS);
     //   assertThat(testDataProvider.getHistoricalDateRange()).isEqualTo(UPDATED_HISTORICAL_DATE_RANGE);
        assertThat(testDataProvider.getKeyDataFields()).isEqualTo(UPDATED_KEY_DATA_FIELDS);
        assertThat(testDataProvider.getSummary()).isEqualTo(UPDATED_SUMMARY);
        assertThat(testDataProvider.getResearchMethodsCompleted()).isEqualTo(UPDATED_RESEARCH_METHODS_COMPLETED);
        assertThat(testDataProvider.getScoreInvestorsWillingness()).isEqualTo(UPDATED_INVESTORS_SCORE_WILLINGNESS);
        assertThat(testDataProvider.getInvestor_clients()).isEqualTo(UPDATED_INVESTOR_CLIENTS);
       // assertThat(testDataProvider.getHistoricalDateRangeEstimate()).isEqualTo(UPDATED_HISTORICAL_DATE_RANGE_ESTIMATE);
        assertThat(testDataProvider.getPublicCompaniesCovered()).isEqualTo(UPDATED_PUBLIC_COMPANIES_COVERED);
        assertThat(testDataProvider.getScoreUniqueness()).isEqualTo(UPDATED_UNIQUENESS_SCORE);
        assertThat(testDataProvider.getScoreValue()).isEqualTo(UPDATED_VALUE_SCORE);
        assertThat(testDataProvider.getScoreReadiness()).isEqualTo(UPDATED_READINESS_SCORE);
        assertThat(testDataProvider.getMarketplaceStatus()).isEqualTo(UPDATED_MARKETPLACE_STATUS);
        assertThat(testDataProvider.getFreeTrialAvailable()).isEqualTo(UPDATED_FREE_TRIAL_AVAILABLE);
        assertThat(testDataProvider.getProductDetails()).isEqualTo(UPDATED_PRODUCT_DETAILS);

        // Validate the DataProvider in Elasticsearch
        DataProvider dataProviderEs = dataProviderSearchRepository.findOne(testDataProvider.getId());
        assertThat(dataProviderEs).isEqualToComparingFieldByField(testDataProvider);
    }

    @Test
    @Transactional
    public void updateNonExistingDataProvider() throws Exception {
        int databaseSizeBeforeUpdate = dataProviderRepository.findAll().size();

        // Create the DataProvider

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDataProviderMockMvc.perform(put("/api/data-providers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataProvider)))
            .andExpect(status().isCreated());

        // Validate the DataProvider in the database
        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDataProvider() throws Exception {
        // Initialize the database
        dataProviderService.save(dataProvider);

        int databaseSizeBeforeDelete = dataProviderRepository.findAll().size();

        // Get the dataProvider
        restDataProviderMockMvc.perform(delete("/api/data-providers/{id}", dataProvider.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dataProviderExistsInEs = dataProviderSearchRepository.exists(dataProvider.getId());
        assertThat(dataProviderExistsInEs).isFalse();

        // Validate the database is empty
        List<DataProvider> dataProviderList = dataProviderRepository.findAll();
        assertThat(dataProviderList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDataProvider() throws Exception {
        // Initialize the database
        dataProviderService.save(dataProvider);

        // Search the dataProvider
        restDataProviderMockMvc.perform(get("/api/_search/data-providers?query=id:" + dataProvider.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataProvider.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].providerName").value(hasItem(DEFAULT_PROVIDER_NAME.toString())))
            .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE.toString())))
            .andExpect(jsonPath("$.[*].shortDescription").value(hasItem(DEFAULT_SHORT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].longDescription").value(hasItem(DEFAULT_LONG_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].mainAssetClass").value(hasItem(DEFAULT_MAIN_ASSET_CLASS.toString())))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES.toString())))
            .andExpect(jsonPath("$.[*].live").value(hasItem(DEFAULT_LIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].dataUpdateFrequency").value(hasItem(DEFAULT_DATA_UPDATE_FREQUENCY.toString())))
            .andExpect(jsonPath("$.[*].dataUpdateFrequencyNotes").value(hasItem(DEFAULT_DATA_UPDATE_FREQUENCY_NOTES.toString())))
            .andExpect(jsonPath("$.[*].dateCollectionBegan").value(hasItem(DEFAULT_DATE_COLLECTION_BEGAN.toString())))
            .andExpect(jsonPath("$.[*].dateCollectionRangeExplanation").value(hasItem(DEFAULT_DATE_COLLECTION_RANGE_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].collectionMethodsExplanation").value(hasItem(DEFAULT_COLLECTION_METHODS_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].potentialDrawbacks").value(hasItem(DEFAULT_POTENTIAL_DRAWBACKS.toString())))
            .andExpect(jsonPath("$.[*].uniqueValueProps").value(hasItem(DEFAULT_UNIQUE_VALUE_PROPS.toString())))
            .andExpect(jsonPath("$.[*].historicalDateRange").value(hasItem(DEFAULT_HISTORICAL_DATE_RANGE.toString())))
            .andExpect(jsonPath("$.[*].keyDataFields").value(hasItem(DEFAULT_KEY_DATA_FIELDS.toString())))
            .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
            .andExpect(jsonPath("$.[*].researchMethodsCompleted").value(hasItem(DEFAULT_RESEARCH_METHODS_COMPLETED.toString())))
            .andExpect(jsonPath("$.[*].investorsScoreWillingness").value(hasItem(DEFAULT_INVESTORS_SCORE_WILLINGNESS.toString())))
            .andExpect(jsonPath("$.[*].investor_clients").value(hasItem(DEFAULT_INVESTOR_CLIENTS.toString())))
            .andExpect(jsonPath("$.[*].historicalDateRangeEstimate").value(hasItem(DEFAULT_HISTORICAL_DATE_RANGE_ESTIMATE.toString())))
            .andExpect(jsonPath("$.[*].publicCompaniesCovered").value(hasItem(DEFAULT_PUBLIC_COMPANIES_COVERED.toString())))
            .andExpect(jsonPath("$.[*].uniquenessScore").value(hasItem(DEFAULT_UNIQUENESS_SCORE.toString())))
            .andExpect(jsonPath("$.[*].valueScore").value(hasItem(DEFAULT_VALUE_SCORE.toString())))
            .andExpect(jsonPath("$.[*].readinessScore").value(hasItem(DEFAULT_READINESS_SCORE.toString())))
            .andExpect(jsonPath("$.[*].marketplaceStatus").value(hasItem(DEFAULT_MARKETPLACE_STATUS.toString())))
            .andExpect(jsonPath("$.[*].freeTrialAvailable").value(hasItem(DEFAULT_FREE_TRIAL_AVAILABLE.toString())))
            .andExpect(jsonPath("$.[*].productDetails").value(hasItem(DEFAULT_PRODUCT_DETAILS.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataProvider.class);
        DataProvider dataProvider1 = new DataProvider();
        dataProvider1.setId(1L);
        DataProvider dataProvider2 = new DataProvider();
        dataProvider2.setId(dataProvider1.getId());
        assertThat(dataProvider1).isEqualTo(dataProvider2);
        dataProvider2.setId(2L);
        assertThat(dataProvider1).isNotEqualTo(dataProvider2);
        dataProvider1.setId(null);
        assertThat(dataProvider1).isNotEqualTo(dataProvider2);
    }
}
