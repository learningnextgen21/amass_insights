package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;

import com.ai.mp.domain.DataOrganization;
import com.ai.mp.repository.DataOrganizationRepository;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.service.DataOrganizationService;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.service.UserService;
import com.ai.mp.repository.search.DataOrganizationSearchRepository;
import com.ai.mp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DataOrganizationResource REST controller.
 *
 * @see DataOrganizationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class DataOrganizationResourceIntTest {

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_WEBSITE = "AAAAAAAAAA";
    private static final String UPDATED_WEBSITE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_ZIP_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ZIP_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_FACEBOOK = "AAAAAAAAAA";
    private static final String UPDATED_FACEBOOK = "BBBBBBBBBB";

    private static final String DEFAULT_GITHUB = "AAAAAAAAAA";
    private static final String UPDATED_GITHUB = "BBBBBBBBBB";

    private static final String DEFAULT_TWITTER = "AAAAAAAAAA";
    private static final String UPDATED_TWITTER = "BBBBBBBBBB";

    private static final String DEFAULT_BLOG = "AAAAAAAAAA";
    private static final String UPDATED_BLOG = "BBBBBBBBBB";

    private static final String DEFAULT_BLOG_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_BLOG_STATUS = "BBBBBBBBBB";

    private static final Integer DEFAULT_YEAR_FOUNDED = 11;
    private static final Integer UPDATED_YEAR_FOUNDED = 10;

    private static final String DEFAULT_CEO = "AAAAAAAAAA";
    private static final String UPDATED_CEO = "BBBBBBBBBB";

    private static final String DEFAULT_FINANCIAL_POSITION = "AAAAAAAAAA";
    private static final String UPDATED_FINANCIAL_POSITION = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_BASE = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_BASE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_MNDA = false;
    private static final Boolean UPDATED_MNDA = true;

    private static final String DEFAULT_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_NOTES = "BBBBBBBBBB";

    private static final Integer DEFAULT_INACTIVE = 2;
    private static final Integer UPDATED_INACTIVE = 1;

    private static final Integer DEFAULT_LIVE = 2;
    private static final Integer UPDATED_LIVE = 1;

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    /*private static final Instant DEFAULT_UPDATEDDATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATEDDATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);
*/
    private static final LocalDate DEFAULT_UPDATEDDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate UPDATED_UPDATEDDATE = LocalDate.now(ZoneId.systemDefault());
    @Autowired
    private DataOrganizationRepository dataOrganizationRepository;

    @Autowired
    private DataOrganizationService dataOrganizationService;

    @Autowired
    private DataOrganizationSearchRepository dataOrganizationSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDataOrganizationMockMvc;

    private DataOrganization dataOrganization;
    @Autowired
    private UserService userService;
   @Autowired
   private ElasticsearchIndexService elasticsearchIndexService;
@Autowired
private DataProviderRepository dataProviderRepository;
   
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DataOrganizationResource dataOrganizationResource = new DataOrganizationResource(dataOrganizationService,userService,dataProviderRepository,elasticsearchIndexService);
        this.restDataOrganizationMockMvc = MockMvcBuilders.standaloneSetup(dataOrganizationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataOrganization createEntity(EntityManager em) {
        DataOrganization dataOrganization = new DataOrganization()
            .recordID(DEFAULT_RECORD_ID)
            .name(DEFAULT_NAME)
            //.website(DEFAULT_WEBSITE)
            .description(DEFAULT_DESCRIPTION)
            .city(DEFAULT_CITY)
            .state(DEFAULT_STATE)
            .country(DEFAULT_COUNTRY)
            .zipCode(DEFAULT_ZIP_CODE)
           // .facebook(DEFAULT_FACEBOOK)
          //  .github(DEFAULT_GITHUB)
           // .twitter(DEFAULT_TWITTER)
            .yearFounded(DEFAULT_YEAR_FOUNDED)
            .ceo(DEFAULT_CEO)
           // .financialPosition(DEFAULT_FINANCIAL_POSITION)
            //.clientBase(DEFAULT_CLIENT_BASE)
            //.notes(DEFAULT_NOTES)
           // .inactive(DEFAULT_INACTIVE)
           // .live(DEFAULT_LIVE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updateddate(DEFAULT_UPDATEDDATE);
        return dataOrganization;
    }

    @Before
    public void initTest() {
        dataOrganizationSearchRepository.deleteAll();
        dataOrganization = createEntity(em);
    }

    @Test
    @Transactional
    public void createDataOrganization() throws Exception {
        int databaseSizeBeforeCreate = dataOrganizationRepository.findAll().size();

        // Create the DataOrganization
        restDataOrganizationMockMvc.perform(post("/api/data-organizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataOrganization)))
            .andExpect(status().isCreated());
        // Validate the DataOrganization in the database
        List<DataOrganization> dataOrganizationList = dataOrganizationRepository.findAll();
        assertThat(dataOrganizationList).hasSize(databaseSizeBeforeCreate + 1);
        DataOrganization testDataOrganization = dataOrganizationList.get(dataOrganizationList.size() - 1);
        assertThat(testDataOrganization.getRecordID()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testDataOrganization.getName()).isEqualTo(DEFAULT_NAME);
       // assertThat(testDataOrganization.getWebsite()).isEqualTo(DEFAULT_WEBSITE);
        assertThat(testDataOrganization.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDataOrganization.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testDataOrganization.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testDataOrganization.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testDataOrganization.getZipCode()).isEqualTo(DEFAULT_ZIP_CODE);
       // assertThat(testDataOrganization.getFacebook()).isEqualTo(DEFAULT_FACEBOOK);
       // assertThat(testDataOrganization.getGithub()).isEqualTo(DEFAULT_GITHUB);
       // assertThat(testDataOrganization.getTwitter()).isEqualTo(DEFAULT_TWITTER);
        assertThat(testDataOrganization.getYearFounded()).isEqualTo(DEFAULT_YEAR_FOUNDED);
        assertThat(testDataOrganization.getHeadCount()).isEqualTo(DEFAULT_CEO);
      //  assertThat(testDataOrganization.getFinancialPosition()).isEqualTo(DEFAULT_FINANCIAL_POSITION);
      //  assertThat(testDataOrganization.getClientBase()).isEqualTo(DEFAULT_CLIENT_BASE);
      //  assertThat(testDataOrganization.getNotes()).isEqualTo(DEFAULT_NOTES);
       // assertThat(testDataOrganization.getInactive()).isEqualTo(DEFAULT_INACTIVE);
       // assertThat(testDataOrganization.getLive()).isEqualTo(DEFAULT_LIVE);
        assertThat(testDataOrganization.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDataOrganization.getUpdateddate()).isEqualTo(DEFAULT_UPDATEDDATE);

        // Validate the DataOrganization in Elasticsearch
        DataOrganization dataOrganizationEs = dataOrganizationSearchRepository.findOne(testDataOrganization.getId());
        assertThat(dataOrganizationEs).isEqualToComparingFieldByField(testDataOrganization);
    }

    @Test
    @Transactional
    public void createDataOrganizationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dataOrganizationRepository.findAll().size();

        // Create the DataOrganization with an existing ID
        dataOrganization.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataOrganizationMockMvc.perform(post("/api/data-organizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataOrganization)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DataOrganization> dataOrganizationList = dataOrganizationRepository.findAll();
        assertThat(dataOrganizationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataOrganizationRepository.findAll().size();
        // set the field null
        dataOrganization.setCreatedDate(null);

        // Create the DataOrganization, which fails.

        restDataOrganizationMockMvc.perform(post("/api/data-organizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataOrganization)))
            .andExpect(status().isBadRequest());

        List<DataOrganization> dataOrganizationList = dataOrganizationRepository.findAll();
        assertThat(dataOrganizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDataOrganizations() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList
        restDataOrganizationMockMvc.perform(get("/api/data-organizations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataOrganization.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].zipCode").value(hasItem(DEFAULT_ZIP_CODE.toString())))
            .andExpect(jsonPath("$.[*].facebook").value(hasItem(DEFAULT_FACEBOOK.toString())))
            .andExpect(jsonPath("$.[*].github").value(hasItem(DEFAULT_GITHUB.toString())))
            .andExpect(jsonPath("$.[*].twitter").value(hasItem(DEFAULT_TWITTER.toString())))
            .andExpect(jsonPath("$.[*].blog").value(hasItem(DEFAULT_BLOG.toString())))
            .andExpect(jsonPath("$.[*].blogStatus").value(hasItem(DEFAULT_BLOG_STATUS.toString())))
            .andExpect(jsonPath("$.[*].yearFounded").value(hasItem(DEFAULT_YEAR_FOUNDED)))
            .andExpect(jsonPath("$.[*].ceo").value(hasItem(DEFAULT_CEO.toString())))
            .andExpect(jsonPath("$.[*].financialPosition").value(hasItem(DEFAULT_FINANCIAL_POSITION.toString())))
            .andExpect(jsonPath("$.[*].clientBase").value(hasItem(DEFAULT_CLIENT_BASE.toString())))
            .andExpect(jsonPath("$.[*].mnda").value(hasItem(DEFAULT_MNDA.booleanValue())))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].live").value(hasItem(DEFAULT_LIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateddate").value(hasItem(DEFAULT_UPDATEDDATE.toString())));
    }

    @Test
    @Transactional
    public void getDataOrganization() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get the dataOrganization
        restDataOrganizationMockMvc.perform(get("/api/data-organizations/{id}", dataOrganization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dataOrganization.getId().intValue()))
            .andExpect(jsonPath("$.recordID").value(DEFAULT_RECORD_ID.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.website").value(DEFAULT_WEBSITE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.zipCode").value(DEFAULT_ZIP_CODE.toString()))
            .andExpect(jsonPath("$.facebook").value(DEFAULT_FACEBOOK.toString()))
            .andExpect(jsonPath("$.github").value(DEFAULT_GITHUB.toString()))
            .andExpect(jsonPath("$.twitter").value(DEFAULT_TWITTER.toString()))
            .andExpect(jsonPath("$.blog").value(DEFAULT_BLOG.toString()))
            .andExpect(jsonPath("$.blogStatus").value(DEFAULT_BLOG_STATUS.toString()))
            .andExpect(jsonPath("$.yearFounded").value(DEFAULT_YEAR_FOUNDED))
            .andExpect(jsonPath("$.ceo").value(DEFAULT_CEO.toString()))
            .andExpect(jsonPath("$.financialPosition").value(DEFAULT_FINANCIAL_POSITION.toString()))
            .andExpect(jsonPath("$.clientBase").value(DEFAULT_CLIENT_BASE.toString()))
            .andExpect(jsonPath("$.mnda").value(DEFAULT_MNDA.booleanValue()))
            .andExpect(jsonPath("$.notes").value(DEFAULT_NOTES.toString()))
            .andExpect(jsonPath("$.inactive").value(DEFAULT_INACTIVE))
            .andExpect(jsonPath("$.live").value(DEFAULT_LIVE))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updateddate").value(DEFAULT_UPDATEDDATE.toString()));
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByRecordIDIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where recordID equals to DEFAULT_RECORD_ID
        defaultDataOrganizationShouldBeFound("recordID.equals=" + DEFAULT_RECORD_ID);

        // Get all the dataOrganizationList where recordID equals to UPDATED_RECORD_ID
        defaultDataOrganizationShouldNotBeFound("recordID.equals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByRecordIDIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where recordID in DEFAULT_RECORD_ID or UPDATED_RECORD_ID
        defaultDataOrganizationShouldBeFound("recordID.in=" + DEFAULT_RECORD_ID + "," + UPDATED_RECORD_ID);

        // Get all the dataOrganizationList where recordID equals to UPDATED_RECORD_ID
        defaultDataOrganizationShouldNotBeFound("recordID.in=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByRecordIDIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where recordID is not null
        defaultDataOrganizationShouldBeFound("recordID.specified=true");

        // Get all the dataOrganizationList where recordID is null
        defaultDataOrganizationShouldNotBeFound("recordID.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where name equals to DEFAULT_NAME
        defaultDataOrganizationShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the dataOrganizationList where name equals to UPDATED_NAME
        defaultDataOrganizationShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where name in DEFAULT_NAME or UPDATED_NAME
        defaultDataOrganizationShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the dataOrganizationList where name equals to UPDATED_NAME
        defaultDataOrganizationShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where name is not null
        defaultDataOrganizationShouldBeFound("name.specified=true");

        // Get all the dataOrganizationList where name is null
        defaultDataOrganizationShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByWebsiteIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where website equals to DEFAULT_WEBSITE
        defaultDataOrganizationShouldBeFound("website.equals=" + DEFAULT_WEBSITE);

        // Get all the dataOrganizationList where website equals to UPDATED_WEBSITE
        defaultDataOrganizationShouldNotBeFound("website.equals=" + UPDATED_WEBSITE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByWebsiteIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where website in DEFAULT_WEBSITE or UPDATED_WEBSITE
        defaultDataOrganizationShouldBeFound("website.in=" + DEFAULT_WEBSITE + "," + UPDATED_WEBSITE);

        // Get all the dataOrganizationList where website equals to UPDATED_WEBSITE
        defaultDataOrganizationShouldNotBeFound("website.in=" + UPDATED_WEBSITE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByWebsiteIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where website is not null
        defaultDataOrganizationShouldBeFound("website.specified=true");

        // Get all the dataOrganizationList where website is null
        defaultDataOrganizationShouldNotBeFound("website.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByCityIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where city equals to DEFAULT_CITY
        defaultDataOrganizationShouldBeFound("city.equals=" + DEFAULT_CITY);

        // Get all the dataOrganizationList where city equals to UPDATED_CITY
        defaultDataOrganizationShouldNotBeFound("city.equals=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByCityIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where city in DEFAULT_CITY or UPDATED_CITY
        defaultDataOrganizationShouldBeFound("city.in=" + DEFAULT_CITY + "," + UPDATED_CITY);

        // Get all the dataOrganizationList where city equals to UPDATED_CITY
        defaultDataOrganizationShouldNotBeFound("city.in=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByCityIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where city is not null
        defaultDataOrganizationShouldBeFound("city.specified=true");

        // Get all the dataOrganizationList where city is null
        defaultDataOrganizationShouldNotBeFound("city.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByStateIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where state equals to DEFAULT_STATE
        defaultDataOrganizationShouldBeFound("state.equals=" + DEFAULT_STATE);

        // Get all the dataOrganizationList where state equals to UPDATED_STATE
        defaultDataOrganizationShouldNotBeFound("state.equals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByStateIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where state in DEFAULT_STATE or UPDATED_STATE
        defaultDataOrganizationShouldBeFound("state.in=" + DEFAULT_STATE + "," + UPDATED_STATE);

        // Get all the dataOrganizationList where state equals to UPDATED_STATE
        defaultDataOrganizationShouldNotBeFound("state.in=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByStateIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where state is not null
        defaultDataOrganizationShouldBeFound("state.specified=true");

        // Get all the dataOrganizationList where state is null
        defaultDataOrganizationShouldNotBeFound("state.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByCountryIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where country equals to DEFAULT_COUNTRY
        defaultDataOrganizationShouldBeFound("country.equals=" + DEFAULT_COUNTRY);

        // Get all the dataOrganizationList where country equals to UPDATED_COUNTRY
        defaultDataOrganizationShouldNotBeFound("country.equals=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByCountryIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where country in DEFAULT_COUNTRY or UPDATED_COUNTRY
        defaultDataOrganizationShouldBeFound("country.in=" + DEFAULT_COUNTRY + "," + UPDATED_COUNTRY);

        // Get all the dataOrganizationList where country equals to UPDATED_COUNTRY
        defaultDataOrganizationShouldNotBeFound("country.in=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByCountryIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where country is not null
        defaultDataOrganizationShouldBeFound("country.specified=true");

        // Get all the dataOrganizationList where country is null
        defaultDataOrganizationShouldNotBeFound("country.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByZipCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where zipCode equals to DEFAULT_ZIP_CODE
        defaultDataOrganizationShouldBeFound("zipCode.equals=" + DEFAULT_ZIP_CODE);

        // Get all the dataOrganizationList where zipCode equals to UPDATED_ZIP_CODE
        defaultDataOrganizationShouldNotBeFound("zipCode.equals=" + UPDATED_ZIP_CODE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByZipCodeIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where zipCode in DEFAULT_ZIP_CODE or UPDATED_ZIP_CODE
        defaultDataOrganizationShouldBeFound("zipCode.in=" + DEFAULT_ZIP_CODE + "," + UPDATED_ZIP_CODE);

        // Get all the dataOrganizationList where zipCode equals to UPDATED_ZIP_CODE
        defaultDataOrganizationShouldNotBeFound("zipCode.in=" + UPDATED_ZIP_CODE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByZipCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where zipCode is not null
        defaultDataOrganizationShouldBeFound("zipCode.specified=true");

        // Get all the dataOrganizationList where zipCode is null
        defaultDataOrganizationShouldNotBeFound("zipCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByFacebookIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where facebook equals to DEFAULT_FACEBOOK
        defaultDataOrganizationShouldBeFound("facebook.equals=" + DEFAULT_FACEBOOK);

        // Get all the dataOrganizationList where facebook equals to UPDATED_FACEBOOK
        defaultDataOrganizationShouldNotBeFound("facebook.equals=" + UPDATED_FACEBOOK);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByFacebookIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where facebook in DEFAULT_FACEBOOK or UPDATED_FACEBOOK
        defaultDataOrganizationShouldBeFound("facebook.in=" + DEFAULT_FACEBOOK + "," + UPDATED_FACEBOOK);

        // Get all the dataOrganizationList where facebook equals to UPDATED_FACEBOOK
        defaultDataOrganizationShouldNotBeFound("facebook.in=" + UPDATED_FACEBOOK);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByFacebookIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where facebook is not null
        defaultDataOrganizationShouldBeFound("facebook.specified=true");

        // Get all the dataOrganizationList where facebook is null
        defaultDataOrganizationShouldNotBeFound("facebook.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByGithubIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where github equals to DEFAULT_GITHUB
        defaultDataOrganizationShouldBeFound("github.equals=" + DEFAULT_GITHUB);

        // Get all the dataOrganizationList where github equals to UPDATED_GITHUB
        defaultDataOrganizationShouldNotBeFound("github.equals=" + UPDATED_GITHUB);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByGithubIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where github in DEFAULT_GITHUB or UPDATED_GITHUB
        defaultDataOrganizationShouldBeFound("github.in=" + DEFAULT_GITHUB + "," + UPDATED_GITHUB);

        // Get all the dataOrganizationList where github equals to UPDATED_GITHUB
        defaultDataOrganizationShouldNotBeFound("github.in=" + UPDATED_GITHUB);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByGithubIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where github is not null
        defaultDataOrganizationShouldBeFound("github.specified=true");

        // Get all the dataOrganizationList where github is null
        defaultDataOrganizationShouldNotBeFound("github.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByTwitterIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where twitter equals to DEFAULT_TWITTER
        defaultDataOrganizationShouldBeFound("twitter.equals=" + DEFAULT_TWITTER);

        // Get all the dataOrganizationList where twitter equals to UPDATED_TWITTER
        defaultDataOrganizationShouldNotBeFound("twitter.equals=" + UPDATED_TWITTER);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByTwitterIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where twitter in DEFAULT_TWITTER or UPDATED_TWITTER
        defaultDataOrganizationShouldBeFound("twitter.in=" + DEFAULT_TWITTER + "," + UPDATED_TWITTER);

        // Get all the dataOrganizationList where twitter equals to UPDATED_TWITTER
        defaultDataOrganizationShouldNotBeFound("twitter.in=" + UPDATED_TWITTER);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByTwitterIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where twitter is not null
        defaultDataOrganizationShouldBeFound("twitter.specified=true");

        // Get all the dataOrganizationList where twitter is null
        defaultDataOrganizationShouldNotBeFound("twitter.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByBlogIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where blog equals to DEFAULT_BLOG
        defaultDataOrganizationShouldBeFound("blog.equals=" + DEFAULT_BLOG);

        // Get all the dataOrganizationList where blog equals to UPDATED_BLOG
        defaultDataOrganizationShouldNotBeFound("blog.equals=" + UPDATED_BLOG);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByBlogIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where blog in DEFAULT_BLOG or UPDATED_BLOG
        defaultDataOrganizationShouldBeFound("blog.in=" + DEFAULT_BLOG + "," + UPDATED_BLOG);

        // Get all the dataOrganizationList where blog equals to UPDATED_BLOG
        defaultDataOrganizationShouldNotBeFound("blog.in=" + UPDATED_BLOG);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByBlogIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where blog is not null
        defaultDataOrganizationShouldBeFound("blog.specified=true");

        // Get all the dataOrganizationList where blog is null
        defaultDataOrganizationShouldNotBeFound("blog.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByBlogStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where blogStatus equals to DEFAULT_BLOG_STATUS
        defaultDataOrganizationShouldBeFound("blogStatus.equals=" + DEFAULT_BLOG_STATUS);

        // Get all the dataOrganizationList where blogStatus equals to UPDATED_BLOG_STATUS
        defaultDataOrganizationShouldNotBeFound("blogStatus.equals=" + UPDATED_BLOG_STATUS);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByBlogStatusIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where blogStatus in DEFAULT_BLOG_STATUS or UPDATED_BLOG_STATUS
        defaultDataOrganizationShouldBeFound("blogStatus.in=" + DEFAULT_BLOG_STATUS + "," + UPDATED_BLOG_STATUS);

        // Get all the dataOrganizationList where blogStatus equals to UPDATED_BLOG_STATUS
        defaultDataOrganizationShouldNotBeFound("blogStatus.in=" + UPDATED_BLOG_STATUS);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByBlogStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where blogStatus is not null
        defaultDataOrganizationShouldBeFound("blogStatus.specified=true");

        // Get all the dataOrganizationList where blogStatus is null
        defaultDataOrganizationShouldNotBeFound("blogStatus.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByYearFoundedIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where yearFounded equals to DEFAULT_YEAR_FOUNDED
        defaultDataOrganizationShouldBeFound("yearFounded.equals=" + DEFAULT_YEAR_FOUNDED);

        // Get all the dataOrganizationList where yearFounded equals to UPDATED_YEAR_FOUNDED
        defaultDataOrganizationShouldNotBeFound("yearFounded.equals=" + UPDATED_YEAR_FOUNDED);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByYearFoundedIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where yearFounded in DEFAULT_YEAR_FOUNDED or UPDATED_YEAR_FOUNDED
        defaultDataOrganizationShouldBeFound("yearFounded.in=" + DEFAULT_YEAR_FOUNDED + "," + UPDATED_YEAR_FOUNDED);

        // Get all the dataOrganizationList where yearFounded equals to UPDATED_YEAR_FOUNDED
        defaultDataOrganizationShouldNotBeFound("yearFounded.in=" + UPDATED_YEAR_FOUNDED);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByYearFoundedIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where yearFounded is not null
        defaultDataOrganizationShouldBeFound("yearFounded.specified=true");

        // Get all the dataOrganizationList where yearFounded is null
        defaultDataOrganizationShouldNotBeFound("yearFounded.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByYearFoundedIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where yearFounded greater than or equals to DEFAULT_YEAR_FOUNDED
        defaultDataOrganizationShouldBeFound("yearFounded.greaterOrEqualThan=" + DEFAULT_YEAR_FOUNDED);

        // Get all the dataOrganizationList where yearFounded greater than or equals to (DEFAULT_YEAR_FOUNDED + 1)
        defaultDataOrganizationShouldNotBeFound("yearFounded.greaterOrEqualThan=" + (DEFAULT_YEAR_FOUNDED + 1));
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByYearFoundedIsLessThanSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where yearFounded less than or equals to DEFAULT_YEAR_FOUNDED
        defaultDataOrganizationShouldNotBeFound("yearFounded.lessThan=" + DEFAULT_YEAR_FOUNDED);

        // Get all the dataOrganizationList where yearFounded less than or equals to (DEFAULT_YEAR_FOUNDED + 1)
        defaultDataOrganizationShouldBeFound("yearFounded.lessThan=" + (DEFAULT_YEAR_FOUNDED + 1));
    }


    @Test
    @Transactional
    public void getAllDataOrganizationsByCeoIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where ceo equals to DEFAULT_CEO
        defaultDataOrganizationShouldBeFound("ceo.equals=" + DEFAULT_CEO);

        // Get all the dataOrganizationList where ceo equals to UPDATED_CEO
        defaultDataOrganizationShouldNotBeFound("ceo.equals=" + UPDATED_CEO);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByCeoIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where ceo in DEFAULT_CEO or UPDATED_CEO
        defaultDataOrganizationShouldBeFound("ceo.in=" + DEFAULT_CEO + "," + UPDATED_CEO);

        // Get all the dataOrganizationList where ceo equals to UPDATED_CEO
        defaultDataOrganizationShouldNotBeFound("ceo.in=" + UPDATED_CEO);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByCeoIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where ceo is not null
        defaultDataOrganizationShouldBeFound("ceo.specified=true");

        // Get all the dataOrganizationList where ceo is null
        defaultDataOrganizationShouldNotBeFound("ceo.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByFinancialPositionIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where financialPosition equals to DEFAULT_FINANCIAL_POSITION
        defaultDataOrganizationShouldBeFound("financialPosition.equals=" + DEFAULT_FINANCIAL_POSITION);

        // Get all the dataOrganizationList where financialPosition equals to UPDATED_FINANCIAL_POSITION
        defaultDataOrganizationShouldNotBeFound("financialPosition.equals=" + UPDATED_FINANCIAL_POSITION);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByFinancialPositionIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where financialPosition in DEFAULT_FINANCIAL_POSITION or UPDATED_FINANCIAL_POSITION
        defaultDataOrganizationShouldBeFound("financialPosition.in=" + DEFAULT_FINANCIAL_POSITION + "," + UPDATED_FINANCIAL_POSITION);

        // Get all the dataOrganizationList where financialPosition equals to UPDATED_FINANCIAL_POSITION
        defaultDataOrganizationShouldNotBeFound("financialPosition.in=" + UPDATED_FINANCIAL_POSITION);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByFinancialPositionIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where financialPosition is not null
        defaultDataOrganizationShouldBeFound("financialPosition.specified=true");

        // Get all the dataOrganizationList where financialPosition is null
        defaultDataOrganizationShouldNotBeFound("financialPosition.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByClientBaseIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where clientBase equals to DEFAULT_CLIENT_BASE
        defaultDataOrganizationShouldBeFound("clientBase.equals=" + DEFAULT_CLIENT_BASE);

        // Get all the dataOrganizationList where clientBase equals to UPDATED_CLIENT_BASE
        defaultDataOrganizationShouldNotBeFound("clientBase.equals=" + UPDATED_CLIENT_BASE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByClientBaseIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where clientBase in DEFAULT_CLIENT_BASE or UPDATED_CLIENT_BASE
        defaultDataOrganizationShouldBeFound("clientBase.in=" + DEFAULT_CLIENT_BASE + "," + UPDATED_CLIENT_BASE);

        // Get all the dataOrganizationList where clientBase equals to UPDATED_CLIENT_BASE
        defaultDataOrganizationShouldNotBeFound("clientBase.in=" + UPDATED_CLIENT_BASE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByClientBaseIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where clientBase is not null
        defaultDataOrganizationShouldBeFound("clientBase.specified=true");

        // Get all the dataOrganizationList where clientBase is null
        defaultDataOrganizationShouldNotBeFound("clientBase.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByMndaIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where mnda equals to DEFAULT_MNDA
        defaultDataOrganizationShouldBeFound("mnda.equals=" + DEFAULT_MNDA);

        // Get all the dataOrganizationList where mnda equals to UPDATED_MNDA
        defaultDataOrganizationShouldNotBeFound("mnda.equals=" + UPDATED_MNDA);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByMndaIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where mnda in DEFAULT_MNDA or UPDATED_MNDA
        defaultDataOrganizationShouldBeFound("mnda.in=" + DEFAULT_MNDA + "," + UPDATED_MNDA);

        // Get all the dataOrganizationList where mnda equals to UPDATED_MNDA
        defaultDataOrganizationShouldNotBeFound("mnda.in=" + UPDATED_MNDA);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByMndaIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where mnda is not null
        defaultDataOrganizationShouldBeFound("mnda.specified=true");

        // Get all the dataOrganizationList where mnda is null
        defaultDataOrganizationShouldNotBeFound("mnda.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByInactiveIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where inactive equals to DEFAULT_INACTIVE
        defaultDataOrganizationShouldBeFound("inactive.equals=" + DEFAULT_INACTIVE);

        // Get all the dataOrganizationList where inactive equals to UPDATED_INACTIVE
        defaultDataOrganizationShouldNotBeFound("inactive.equals=" + UPDATED_INACTIVE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByInactiveIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where inactive in DEFAULT_INACTIVE or UPDATED_INACTIVE
        defaultDataOrganizationShouldBeFound("inactive.in=" + DEFAULT_INACTIVE + "," + UPDATED_INACTIVE);

        // Get all the dataOrganizationList where inactive equals to UPDATED_INACTIVE
        defaultDataOrganizationShouldNotBeFound("inactive.in=" + UPDATED_INACTIVE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByInactiveIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where inactive is not null
        defaultDataOrganizationShouldBeFound("inactive.specified=true");

        // Get all the dataOrganizationList where inactive is null
        defaultDataOrganizationShouldNotBeFound("inactive.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByInactiveIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where inactive greater than or equals to DEFAULT_INACTIVE
        defaultDataOrganizationShouldBeFound("inactive.greaterOrEqualThan=" + DEFAULT_INACTIVE);

        // Get all the dataOrganizationList where inactive greater than or equals to (DEFAULT_INACTIVE + 1)
        defaultDataOrganizationShouldNotBeFound("inactive.greaterOrEqualThan=" + (DEFAULT_INACTIVE + 1));
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByInactiveIsLessThanSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where inactive less than or equals to DEFAULT_INACTIVE
        defaultDataOrganizationShouldNotBeFound("inactive.lessThan=" + DEFAULT_INACTIVE);

        // Get all the dataOrganizationList where inactive less than or equals to (DEFAULT_INACTIVE + 1)
        defaultDataOrganizationShouldBeFound("inactive.lessThan=" + (DEFAULT_INACTIVE + 1));
    }


    @Test
    @Transactional
    public void getAllDataOrganizationsByLiveIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where live equals to DEFAULT_LIVE
        defaultDataOrganizationShouldBeFound("live.equals=" + DEFAULT_LIVE);

        // Get all the dataOrganizationList where live equals to UPDATED_LIVE
        defaultDataOrganizationShouldNotBeFound("live.equals=" + UPDATED_LIVE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByLiveIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where live in DEFAULT_LIVE or UPDATED_LIVE
        defaultDataOrganizationShouldBeFound("live.in=" + DEFAULT_LIVE + "," + UPDATED_LIVE);

        // Get all the dataOrganizationList where live equals to UPDATED_LIVE
        defaultDataOrganizationShouldNotBeFound("live.in=" + UPDATED_LIVE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByLiveIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where live is not null
        defaultDataOrganizationShouldBeFound("live.specified=true");

        // Get all the dataOrganizationList where live is null
        defaultDataOrganizationShouldNotBeFound("live.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByLiveIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where live greater than or equals to DEFAULT_LIVE
        defaultDataOrganizationShouldBeFound("live.greaterOrEqualThan=" + DEFAULT_LIVE);

        // Get all the dataOrganizationList where live greater than or equals to (DEFAULT_LIVE + 1)
        defaultDataOrganizationShouldNotBeFound("live.greaterOrEqualThan=" + (DEFAULT_LIVE + 1));
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByLiveIsLessThanSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where live less than or equals to DEFAULT_LIVE
        defaultDataOrganizationShouldNotBeFound("live.lessThan=" + DEFAULT_LIVE);

        // Get all the dataOrganizationList where live less than or equals to (DEFAULT_LIVE + 1)
        defaultDataOrganizationShouldBeFound("live.lessThan=" + (DEFAULT_LIVE + 1));
    }


    @Test
    @Transactional
    public void getAllDataOrganizationsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where createdDate equals to DEFAULT_CREATED_DATE
        defaultDataOrganizationShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the dataOrganizationList where createdDate equals to UPDATED_CREATED_DATE
        defaultDataOrganizationShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultDataOrganizationShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the dataOrganizationList where createdDate equals to UPDATED_CREATED_DATE
        defaultDataOrganizationShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where createdDate is not null
        defaultDataOrganizationShouldBeFound("createdDate.specified=true");

        // Get all the dataOrganizationList where createdDate is null
        defaultDataOrganizationShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where createdDate greater than or equals to DEFAULT_CREATED_DATE
        defaultDataOrganizationShouldBeFound("createdDate.greaterOrEqualThan=" + DEFAULT_CREATED_DATE);

        // Get all the dataOrganizationList where createdDate greater than or equals to UPDATED_CREATED_DATE
        defaultDataOrganizationShouldNotBeFound("createdDate.greaterOrEqualThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where createdDate less than or equals to DEFAULT_CREATED_DATE
        defaultDataOrganizationShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the dataOrganizationList where createdDate less than or equals to UPDATED_CREATED_DATE
        defaultDataOrganizationShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllDataOrganizationsByUpdateddateIsEqualToSomething() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where updateddate equals to DEFAULT_UPDATEDDATE
        defaultDataOrganizationShouldBeFound("updateddate.equals=" + DEFAULT_UPDATEDDATE);

        // Get all the dataOrganizationList where updateddate equals to UPDATED_UPDATEDDATE
        defaultDataOrganizationShouldNotBeFound("updateddate.equals=" + UPDATED_UPDATEDDATE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByUpdateddateIsInShouldWork() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where updateddate in DEFAULT_UPDATEDDATE or UPDATED_UPDATEDDATE
        defaultDataOrganizationShouldBeFound("updateddate.in=" + DEFAULT_UPDATEDDATE + "," + UPDATED_UPDATEDDATE);

        // Get all the dataOrganizationList where updateddate equals to UPDATED_UPDATEDDATE
        defaultDataOrganizationShouldNotBeFound("updateddate.in=" + UPDATED_UPDATEDDATE);
    }

    @Test
    @Transactional
    public void getAllDataOrganizationsByUpdateddateIsNullOrNotNull() throws Exception {
        // Initialize the database
        dataOrganizationRepository.saveAndFlush(dataOrganization);

        // Get all the dataOrganizationList where updateddate is not null
        defaultDataOrganizationShouldBeFound("updateddate.specified=true");

        // Get all the dataOrganizationList where updateddate is null
        defaultDataOrganizationShouldNotBeFound("updateddate.specified=false");
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultDataOrganizationShouldBeFound(String filter) throws Exception {
        restDataOrganizationMockMvc.perform(get("/api/data-organizations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataOrganization.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].zipCode").value(hasItem(DEFAULT_ZIP_CODE.toString())))
            .andExpect(jsonPath("$.[*].facebook").value(hasItem(DEFAULT_FACEBOOK.toString())))
            .andExpect(jsonPath("$.[*].github").value(hasItem(DEFAULT_GITHUB.toString())))
            .andExpect(jsonPath("$.[*].twitter").value(hasItem(DEFAULT_TWITTER.toString())))
            .andExpect(jsonPath("$.[*].blog").value(hasItem(DEFAULT_BLOG.toString())))
            .andExpect(jsonPath("$.[*].blogStatus").value(hasItem(DEFAULT_BLOG_STATUS.toString())))
            .andExpect(jsonPath("$.[*].yearFounded").value(hasItem(DEFAULT_YEAR_FOUNDED)))
            .andExpect(jsonPath("$.[*].ceo").value(hasItem(DEFAULT_CEO.toString())))
            .andExpect(jsonPath("$.[*].financialPosition").value(hasItem(DEFAULT_FINANCIAL_POSITION.toString())))
            .andExpect(jsonPath("$.[*].clientBase").value(hasItem(DEFAULT_CLIENT_BASE.toString())))
            .andExpect(jsonPath("$.[*].mnda").value(hasItem(DEFAULT_MNDA.booleanValue())))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].live").value(hasItem(DEFAULT_LIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateddate").value(hasItem(DEFAULT_UPDATEDDATE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultDataOrganizationShouldNotBeFound(String filter) throws Exception {
        restDataOrganizationMockMvc.perform(get("/api/data-organizations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingDataOrganization() throws Exception {
        // Get the dataOrganization
        restDataOrganizationMockMvc.perform(get("/api/data-organizations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDataOrganization() throws Exception {
        // Initialize the database
        dataOrganizationService.save(dataOrganization);

        int databaseSizeBeforeUpdate = dataOrganizationRepository.findAll().size();

        // Update the dataOrganization
        DataOrganization updatedDataOrganization = dataOrganizationRepository.findOne(dataOrganization.getId());
        updatedDataOrganization
            .recordID(UPDATED_RECORD_ID)
            .name(UPDATED_NAME)
           // .website(UPDATED_WEBSITE)
            .description(UPDATED_DESCRIPTION)
            .city(UPDATED_CITY)
            .state(UPDATED_STATE)
            .country(UPDATED_COUNTRY)
            .zipCode(UPDATED_ZIP_CODE)
           // .facebook(UPDATED_FACEBOOK)
          //  .github(UPDATED_GITHUB)
          //  .twitter(UPDATED_TWITTER)
            .yearFounded(UPDATED_YEAR_FOUNDED)
            .ceo(UPDATED_CEO)
          //  .financialPosition(UPDATED_FINANCIAL_POSITION)
          //  .clientBase(UPDATED_CLIENT_BASE)
           // .notes(UPDATED_NOTES)
         //   .inactive(UPDATED_INACTIVE)
         //   .live(UPDATED_LIVE)
            .createdDate(UPDATED_CREATED_DATE)
            .updateddate(UPDATED_UPDATEDDATE);

        restDataOrganizationMockMvc.perform(put("/api/data-organizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDataOrganization)))
            .andExpect(status().isOk());

        // Validate the DataOrganization in the database
        List<DataOrganization> dataOrganizationList = dataOrganizationRepository.findAll();
        assertThat(dataOrganizationList).hasSize(databaseSizeBeforeUpdate);
        DataOrganization testDataOrganization = dataOrganizationList.get(dataOrganizationList.size() - 1);
        assertThat(testDataOrganization.getRecordID()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testDataOrganization.getName()).isEqualTo(UPDATED_NAME);
        //assertThat(testDataOrganization.getWebsite()).isEqualTo(UPDATED_WEBSITE);
        assertThat(testDataOrganization.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDataOrganization.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testDataOrganization.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testDataOrganization.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testDataOrganization.getZipCode()).isEqualTo(UPDATED_ZIP_CODE);
      //  assertThat(testDataOrganization.getFacebook()).isEqualTo(UPDATED_FACEBOOK);
       // assertThat(testDataOrganization.getGithub()).isEqualTo(UPDATED_GITHUB);
      //  assertThat(testDataOrganization.getTwitter()).isEqualTo(UPDATED_TWITTER);
        assertThat(testDataOrganization.getYearFounded()).isEqualTo(UPDATED_YEAR_FOUNDED);
        assertThat(testDataOrganization.getHeadCount()).isEqualTo(UPDATED_CEO);
      //  assertThat(testDataOrganization.getFinancialPosition()).isEqualTo(UPDATED_FINANCIAL_POSITION);
       // assertThat(testDataOrganization.getClientBase()).isEqualTo(UPDATED_CLIENT_BASE);
       // assertThat(testDataOrganization.getNotes()).isEqualTo(UPDATED_NOTES);
       // assertThat(testDataOrganization.getInactive()).isEqualTo(UPDATED_INACTIVE);
       // assertThat(testDataOrganization.getLive()).isEqualTo(UPDATED_LIVE);
        assertThat(testDataOrganization.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDataOrganization.getUpdateddate()).isEqualTo(UPDATED_UPDATEDDATE);

        // Validate the DataOrganization in Elasticsearch
        DataOrganization dataOrganizationEs = dataOrganizationSearchRepository.findOne(testDataOrganization.getId());
        assertThat(dataOrganizationEs).isEqualToComparingFieldByField(testDataOrganization);
    }

    @Test
    @Transactional
    public void updateNonExistingDataOrganization() throws Exception {
        int databaseSizeBeforeUpdate = dataOrganizationRepository.findAll().size();

        // Create the DataOrganization

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDataOrganizationMockMvc.perform(put("/api/data-organizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataOrganization)))
            .andExpect(status().isCreated());

        // Validate the DataOrganization in the database
        List<DataOrganization> dataOrganizationList = dataOrganizationRepository.findAll();
        assertThat(dataOrganizationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDataOrganization() throws Exception {
        // Initialize the database
        dataOrganizationService.save(dataOrganization);

        int databaseSizeBeforeDelete = dataOrganizationRepository.findAll().size();

        // Get the dataOrganization
        restDataOrganizationMockMvc.perform(delete("/api/data-organizations/{id}", dataOrganization.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dataOrganizationExistsInEs = dataOrganizationSearchRepository.exists(dataOrganization.getId());
        assertThat(dataOrganizationExistsInEs).isFalse();

        // Validate the database is empty
        List<DataOrganization> dataOrganizationList = dataOrganizationRepository.findAll();
        assertThat(dataOrganizationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDataOrganization() throws Exception {
        // Initialize the database
        dataOrganizationService.save(dataOrganization);

        // Search the dataOrganization
        restDataOrganizationMockMvc.perform(get("/api/_search/data-organizations?query=id:" + dataOrganization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataOrganization.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].website").value(hasItem(DEFAULT_WEBSITE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].zipCode").value(hasItem(DEFAULT_ZIP_CODE.toString())))
            .andExpect(jsonPath("$.[*].facebook").value(hasItem(DEFAULT_FACEBOOK.toString())))
            .andExpect(jsonPath("$.[*].github").value(hasItem(DEFAULT_GITHUB.toString())))
            .andExpect(jsonPath("$.[*].twitter").value(hasItem(DEFAULT_TWITTER.toString())))
            .andExpect(jsonPath("$.[*].blog").value(hasItem(DEFAULT_BLOG.toString())))
            .andExpect(jsonPath("$.[*].blogStatus").value(hasItem(DEFAULT_BLOG_STATUS.toString())))
            .andExpect(jsonPath("$.[*].yearFounded").value(hasItem(DEFAULT_YEAR_FOUNDED)))
            .andExpect(jsonPath("$.[*].ceo").value(hasItem(DEFAULT_CEO.toString())))
            .andExpect(jsonPath("$.[*].financialPosition").value(hasItem(DEFAULT_FINANCIAL_POSITION.toString())))
            .andExpect(jsonPath("$.[*].clientBase").value(hasItem(DEFAULT_CLIENT_BASE.toString())))
            .andExpect(jsonPath("$.[*].mnda").value(hasItem(DEFAULT_MNDA.booleanValue())))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].live").value(hasItem(DEFAULT_LIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updateddate").value(hasItem(DEFAULT_UPDATEDDATE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataOrganization.class);
        DataOrganization dataOrganization1 = new DataOrganization();
        dataOrganization1.setId(1L);
        DataOrganization dataOrganization2 = new DataOrganization();
        dataOrganization2.setId(dataOrganization1.getId());
        assertThat(dataOrganization1).isEqualTo(dataOrganization2);
        dataOrganization2.setId(2L);
        assertThat(dataOrganization1).isNotEqualTo(dataOrganization2);
        dataOrganization1.setId(null);
        assertThat(dataOrganization1).isNotEqualTo(dataOrganization2);
    }
}
