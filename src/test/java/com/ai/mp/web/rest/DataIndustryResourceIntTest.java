package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;

import com.ai.mp.domain.DataIndustry;
import com.ai.mp.repository.DataIndustryRepository;
import com.ai.mp.repository.search.DataIndustrySearchRepository;
import com.ai.mp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DataIndustryResource REST controller.
 *
 * @see DataIndustryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class DataIndustryResourceIntTest {

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_DATA_INDUSTRY = "AAAAAAAAAA";
    private static final String UPDATED_DATA_INDUSTRY = "BBBBBBBBBB";

    private static final String DEFAULT_EXPLANATION = "AAAAAAAAAA";
    private static final String UPDATED_EXPLANATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_INACTIVE = 2;
    private static final Integer UPDATED_INACTIVE = 1;

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private DataIndustryRepository dataIndustryRepository;

    @Autowired
    private DataIndustrySearchRepository dataIndustrySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDataIndustryMockMvc;

    private DataIndustry dataIndustry;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DataIndustryResource dataIndustryResource = new DataIndustryResource(dataIndustryRepository, dataIndustrySearchRepository);
        this.restDataIndustryMockMvc = MockMvcBuilders.standaloneSetup(dataIndustryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataIndustry createEntity(EntityManager em) {
        DataIndustry dataIndustry = new DataIndustry()
            .recordID(DEFAULT_RECORD_ID)
            .dataIndustry(DEFAULT_DATA_INDUSTRY)
            .explanation(DEFAULT_EXPLANATION)
            .inactive(DEFAULT_INACTIVE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return dataIndustry;
    }

    @Before
    public void initTest() {
        dataIndustrySearchRepository.deleteAll();
        dataIndustry = createEntity(em);
    }

    @Test
    @Transactional
    public void createDataIndustry() throws Exception {
        int databaseSizeBeforeCreate = dataIndustryRepository.findAll().size();

        // Create the DataIndustry
        restDataIndustryMockMvc.perform(post("/api/data-industries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataIndustry)))
            .andExpect(status().isCreated());

        // Validate the DataIndustry in the database
        List<DataIndustry> dataIndustryList = dataIndustryRepository.findAll();
        assertThat(dataIndustryList).hasSize(databaseSizeBeforeCreate + 1);
        DataIndustry testDataIndustry = dataIndustryList.get(dataIndustryList.size() - 1);
        assertThat(testDataIndustry.getRecordID()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testDataIndustry.getDataIndustry()).isEqualTo(DEFAULT_DATA_INDUSTRY);
        assertThat(testDataIndustry.getExplanation()).isEqualTo(DEFAULT_EXPLANATION);
        assertThat(testDataIndustry.getInactive()).isEqualTo(DEFAULT_INACTIVE);
        assertThat(testDataIndustry.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDataIndustry.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);

        // Validate the DataIndustry in Elasticsearch
        DataIndustry dataIndustryEs = dataIndustrySearchRepository.findOne(testDataIndustry.getId());
        assertThat(dataIndustryEs).isEqualToComparingFieldByField(testDataIndustry);
    }

    @Test
    @Transactional
    public void createDataIndustryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dataIndustryRepository.findAll().size();

        // Create the DataIndustry with an existing ID
        dataIndustry.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataIndustryMockMvc.perform(post("/api/data-industries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataIndustry)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DataIndustry> dataIndustryList = dataIndustryRepository.findAll();
        assertThat(dataIndustryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDataIndustryIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataIndustryRepository.findAll().size();
        // set the field null
        dataIndustry.setDataIndustry(null);

        // Create the DataIndustry, which fails.

        restDataIndustryMockMvc.perform(post("/api/data-industries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataIndustry)))
            .andExpect(status().isBadRequest());

        List<DataIndustry> dataIndustryList = dataIndustryRepository.findAll();
        assertThat(dataIndustryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInactiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataIndustryRepository.findAll().size();
        // set the field null
        dataIndustry.setInactive(null);

        // Create the DataIndustry, which fails.

        restDataIndustryMockMvc.perform(post("/api/data-industries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataIndustry)))
            .andExpect(status().isBadRequest());

        List<DataIndustry> dataIndustryList = dataIndustryRepository.findAll();
        assertThat(dataIndustryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataIndustryRepository.findAll().size();
        // set the field null
        dataIndustry.setCreatedDate(null);

        // Create the DataIndustry, which fails.

        restDataIndustryMockMvc.perform(post("/api/data-industries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataIndustry)))
            .andExpect(status().isBadRequest());

        List<DataIndustry> dataIndustryList = dataIndustryRepository.findAll();
        assertThat(dataIndustryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDataIndustries() throws Exception {
        // Initialize the database
        dataIndustryRepository.saveAndFlush(dataIndustry);

        // Get all the dataIndustryList
        restDataIndustryMockMvc.perform(get("/api/data-industries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataIndustry.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].dataIndustry").value(hasItem(DEFAULT_DATA_INDUSTRY.toString())))
            .andExpect(jsonPath("$.[*].explanation").value(hasItem(DEFAULT_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void getDataIndustry() throws Exception {
        // Initialize the database
        dataIndustryRepository.saveAndFlush(dataIndustry);

        // Get the dataIndustry
        restDataIndustryMockMvc.perform(get("/api/data-industries/{id}", dataIndustry.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dataIndustry.getId().intValue()))
            .andExpect(jsonPath("$.recordID").value(DEFAULT_RECORD_ID.toString()))
            .andExpect(jsonPath("$.dataIndustry").value(DEFAULT_DATA_INDUSTRY.toString()))
            .andExpect(jsonPath("$.explanation").value(DEFAULT_EXPLANATION.toString()))
            .andExpect(jsonPath("$.inactive").value(DEFAULT_INACTIVE))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDataIndustry() throws Exception {
        // Get the dataIndustry
        restDataIndustryMockMvc.perform(get("/api/data-industries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDataIndustry() throws Exception {
        // Initialize the database
        dataIndustryRepository.saveAndFlush(dataIndustry);
        dataIndustrySearchRepository.save(dataIndustry);
        int databaseSizeBeforeUpdate = dataIndustryRepository.findAll().size();

        // Update the dataIndustry
        DataIndustry updatedDataIndustry = dataIndustryRepository.findOne(dataIndustry.getId());
        updatedDataIndustry
            .recordID(UPDATED_RECORD_ID)
            .dataIndustry(UPDATED_DATA_INDUSTRY)
            .explanation(UPDATED_EXPLANATION)
            .inactive(UPDATED_INACTIVE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restDataIndustryMockMvc.perform(put("/api/data-industries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDataIndustry)))
            .andExpect(status().isOk());

        // Validate the DataIndustry in the database
        List<DataIndustry> dataIndustryList = dataIndustryRepository.findAll();
        assertThat(dataIndustryList).hasSize(databaseSizeBeforeUpdate);
        DataIndustry testDataIndustry = dataIndustryList.get(dataIndustryList.size() - 1);
        assertThat(testDataIndustry.getRecordID()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testDataIndustry.getDataIndustry()).isEqualTo(UPDATED_DATA_INDUSTRY);
        assertThat(testDataIndustry.getExplanation()).isEqualTo(UPDATED_EXPLANATION);
        assertThat(testDataIndustry.getInactive()).isEqualTo(UPDATED_INACTIVE);
        assertThat(testDataIndustry.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDataIndustry.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);

        // Validate the DataIndustry in Elasticsearch
        DataIndustry dataIndustryEs = dataIndustrySearchRepository.findOne(testDataIndustry.getId());
        assertThat(dataIndustryEs).isEqualToComparingFieldByField(testDataIndustry);
    }

    @Test
    @Transactional
    public void updateNonExistingDataIndustry() throws Exception {
        int databaseSizeBeforeUpdate = dataIndustryRepository.findAll().size();

        // Create the DataIndustry

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDataIndustryMockMvc.perform(put("/api/data-industries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataIndustry)))
            .andExpect(status().isCreated());

        // Validate the DataIndustry in the database
        List<DataIndustry> dataIndustryList = dataIndustryRepository.findAll();
        assertThat(dataIndustryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDataIndustry() throws Exception {
        // Initialize the database
        dataIndustryRepository.saveAndFlush(dataIndustry);
        dataIndustrySearchRepository.save(dataIndustry);
        int databaseSizeBeforeDelete = dataIndustryRepository.findAll().size();

        // Get the dataIndustry
        restDataIndustryMockMvc.perform(delete("/api/data-industries/{id}", dataIndustry.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dataIndustryExistsInEs = dataIndustrySearchRepository.exists(dataIndustry.getId());
        assertThat(dataIndustryExistsInEs).isFalse();

        // Validate the database is empty
        List<DataIndustry> dataIndustryList = dataIndustryRepository.findAll();
        assertThat(dataIndustryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDataIndustry() throws Exception {
        // Initialize the database
        dataIndustryRepository.saveAndFlush(dataIndustry);
        dataIndustrySearchRepository.save(dataIndustry);

        // Search the dataIndustry
        restDataIndustryMockMvc.perform(get("/api/_search/data-industries?query=id:" + dataIndustry.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataIndustry.getId().intValue())))
            .andExpect(jsonPath("$.[*].recordID").value(hasItem(DEFAULT_RECORD_ID.toString())))
            .andExpect(jsonPath("$.[*].dataIndustry").value(hasItem(DEFAULT_DATA_INDUSTRY.toString())))
            .andExpect(jsonPath("$.[*].explanation").value(hasItem(DEFAULT_EXPLANATION.toString())))
            .andExpect(jsonPath("$.[*].inactive").value(hasItem(DEFAULT_INACTIVE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataIndustry.class);
        DataIndustry dataIndustry1 = new DataIndustry();
        dataIndustry1.setId(1L);
        DataIndustry dataIndustry2 = new DataIndustry();
        dataIndustry2.setId(dataIndustry1.getId());
        assertThat(dataIndustry1).isEqualTo(dataIndustry2);
        dataIndustry2.setId(2L);
        assertThat(dataIndustry1).isNotEqualTo(dataIndustry2);
        dataIndustry1.setId(null);
        assertThat(dataIndustry1).isNotEqualTo(dataIndustry2);
    }
}
