package com.ai.mp.web.rest;

import com.ai.mp.DatamarketplaceApp;

import com.ai.mp.domain.ClientRole;
import com.ai.mp.repository.ClientRoleRepository;
import com.ai.mp.repository.search.ClientRoleSearchRepository;
import com.ai.mp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClientRoleResource REST controller.
 *
 * @see ClientRoleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatamarketplaceApp.class)
public class ClientRoleResourceIntTest {

    private static final Integer DEFAULT_INVESTOR_TYPE = 11;
    private static final Integer UPDATED_INVESTOR_TYPE = 10;

    private static final Integer DEFAULT_ASSETS = 11;
    private static final Integer UPDATED_ASSETS = 10;

    private static final String DEFAULT_ASSET_CLASSES = "AAAAAAAAAA";
    private static final String UPDATED_ASSET_CLASSES = "BBBBBBBBBB";

    private static final String DEFAULT_EQUITIES = "AAAAAAAAAA";
    private static final String UPDATED_EQUITIES = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_PAID = "AAAAAAAAAA";
    private static final String UPDATED_PAID = "BBBBBBBBBB";

    private static final Integer DEFAULT_APPROVAL_STATUS = 11;
    private static final Integer UPDATED_APPROVAL_STATUS = 10;

    private static final Integer DEFAULT_SUBSCRIPTION_TYPE = 11;
    private static final Integer UPDATED_SUBSCRIPTION_TYPE = 10;

    @Autowired
    private ClientRoleRepository clientRoleRepository;

    @Autowired
    private ClientRoleSearchRepository clientRoleSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restClientRoleMockMvc;

    private ClientRole clientRole;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientRoleResource clientRoleResource = new ClientRoleResource(clientRoleRepository, clientRoleSearchRepository);
        this.restClientRoleMockMvc = MockMvcBuilders.standaloneSetup(clientRoleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientRole createEntity(EntityManager em) {
        ClientRole clientRole = new ClientRole()
            .investorType(DEFAULT_INVESTOR_TYPE)
            .assets(DEFAULT_ASSETS)
            .assetClasses(DEFAULT_ASSET_CLASSES)
            .equities(DEFAULT_EQUITIES)
            .description(DEFAULT_DESCRIPTION)
            .paid(DEFAULT_PAID)
            .approvalStatus(DEFAULT_APPROVAL_STATUS)
            .subscriptionType(DEFAULT_SUBSCRIPTION_TYPE);
        return clientRole;
    }

    @Before
    public void initTest() {
        clientRoleSearchRepository.deleteAll();
        clientRole = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientRole() throws Exception {
        int databaseSizeBeforeCreate = clientRoleRepository.findAll().size();

        // Create the ClientRole
        restClientRoleMockMvc.perform(post("/api/client-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientRole)))
            .andExpect(status().isCreated());

        // Validate the ClientRole in the database
        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeCreate + 1);
        ClientRole testClientRole = clientRoleList.get(clientRoleList.size() - 1);
        assertThat(testClientRole.getInvestorType()).isEqualTo(DEFAULT_INVESTOR_TYPE);
        assertThat(testClientRole.getAssets()).isEqualTo(DEFAULT_ASSETS);
        assertThat(testClientRole.getAssetClasses()).isEqualTo(DEFAULT_ASSET_CLASSES);
        assertThat(testClientRole.getEquities()).isEqualTo(DEFAULT_EQUITIES);
        assertThat(testClientRole.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testClientRole.getPaid()).isEqualTo(DEFAULT_PAID);
        assertThat(testClientRole.getApprovalStatus()).isEqualTo(DEFAULT_APPROVAL_STATUS);
        assertThat(testClientRole.getSubscriptionType()).isEqualTo(DEFAULT_SUBSCRIPTION_TYPE);

        // Validate the ClientRole in Elasticsearch
        ClientRole clientRoleEs = clientRoleSearchRepository.findOne(testClientRole.getId());
        assertThat(clientRoleEs).isEqualToComparingFieldByField(testClientRole);
    }

    @Test
    @Transactional
    public void createClientRoleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientRoleRepository.findAll().size();

        // Create the ClientRole with an existing ID
        clientRole.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientRoleMockMvc.perform(post("/api/client-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientRole)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkInvestorTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientRoleRepository.findAll().size();
        // set the field null
        clientRole.setInvestorType(null);

        // Create the ClientRole, which fails.

        restClientRoleMockMvc.perform(post("/api/client-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientRole)))
            .andExpect(status().isBadRequest());

        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAssetsIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientRoleRepository.findAll().size();
        // set the field null
        clientRole.setAssets(null);

        // Create the ClientRole, which fails.

        restClientRoleMockMvc.perform(post("/api/client-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientRole)))
            .andExpect(status().isBadRequest());

        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAssetClassesIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientRoleRepository.findAll().size();
        // set the field null
        clientRole.setAssetClasses(null);

        // Create the ClientRole, which fails.

        restClientRoleMockMvc.perform(post("/api/client-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientRole)))
            .andExpect(status().isBadRequest());

        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEquitiesIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientRoleRepository.findAll().size();
        // set the field null
        clientRole.setEquities(null);

        // Create the ClientRole, which fails.

        restClientRoleMockMvc.perform(post("/api/client-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientRole)))
            .andExpect(status().isBadRequest());

        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientRoleRepository.findAll().size();
        // set the field null
        clientRole.setDescription(null);

        // Create the ClientRole, which fails.

        restClientRoleMockMvc.perform(post("/api/client-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientRole)))
            .andExpect(status().isBadRequest());

        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPaidIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientRoleRepository.findAll().size();
        // set the field null
        clientRole.setPaid(null);

        // Create the ClientRole, which fails.

        restClientRoleMockMvc.perform(post("/api/client-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientRole)))
            .andExpect(status().isBadRequest());

        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkApprovalStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientRoleRepository.findAll().size();
        // set the field null
        clientRole.setApprovalStatus(null);

        // Create the ClientRole, which fails.

        restClientRoleMockMvc.perform(post("/api/client-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientRole)))
            .andExpect(status().isBadRequest());

        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSubscriptionTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientRoleRepository.findAll().size();
        // set the field null
        clientRole.setSubscriptionType(null);

        // Create the ClientRole, which fails.

        restClientRoleMockMvc.perform(post("/api/client-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientRole)))
            .andExpect(status().isBadRequest());

        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientRoles() throws Exception {
        // Initialize the database
        clientRoleRepository.saveAndFlush(clientRole);

        // Get all the clientRoleList
        restClientRoleMockMvc.perform(get("/api/client-roles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientRole.getId().intValue())))
            .andExpect(jsonPath("$.[*].investorType").value(hasItem(DEFAULT_INVESTOR_TYPE)))
            .andExpect(jsonPath("$.[*].assets").value(hasItem(DEFAULT_ASSETS)))
            .andExpect(jsonPath("$.[*].assetClasses").value(hasItem(DEFAULT_ASSET_CLASSES.toString())))
            .andExpect(jsonPath("$.[*].equities").value(hasItem(DEFAULT_EQUITIES.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].paid").value(hasItem(DEFAULT_PAID.toString())))
            .andExpect(jsonPath("$.[*].approvalStatus").value(hasItem(DEFAULT_APPROVAL_STATUS)))
            .andExpect(jsonPath("$.[*].subscriptionType").value(hasItem(DEFAULT_SUBSCRIPTION_TYPE)));
    }

    @Test
    @Transactional
    public void getClientRole() throws Exception {
        // Initialize the database
        clientRoleRepository.saveAndFlush(clientRole);

        // Get the clientRole
        restClientRoleMockMvc.perform(get("/api/client-roles/{id}", clientRole.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientRole.getId().intValue()))
            .andExpect(jsonPath("$.investorType").value(DEFAULT_INVESTOR_TYPE))
            .andExpect(jsonPath("$.assets").value(DEFAULT_ASSETS))
            .andExpect(jsonPath("$.assetClasses").value(DEFAULT_ASSET_CLASSES.toString()))
            .andExpect(jsonPath("$.equities").value(DEFAULT_EQUITIES.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.paid").value(DEFAULT_PAID.toString()))
            .andExpect(jsonPath("$.approvalStatus").value(DEFAULT_APPROVAL_STATUS))
            .andExpect(jsonPath("$.subscriptionType").value(DEFAULT_SUBSCRIPTION_TYPE));
    }

    @Test
    @Transactional
    public void getNonExistingClientRole() throws Exception {
        // Get the clientRole
        restClientRoleMockMvc.perform(get("/api/client-roles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientRole() throws Exception {
        // Initialize the database
        clientRoleRepository.saveAndFlush(clientRole);
        clientRoleSearchRepository.save(clientRole);
        int databaseSizeBeforeUpdate = clientRoleRepository.findAll().size();

        // Update the clientRole
        ClientRole updatedClientRole = clientRoleRepository.findOne(clientRole.getId());
        updatedClientRole
            .investorType(UPDATED_INVESTOR_TYPE)
            .assets(UPDATED_ASSETS)
            .assetClasses(UPDATED_ASSET_CLASSES)
            .equities(UPDATED_EQUITIES)
            .description(UPDATED_DESCRIPTION)
            .paid(UPDATED_PAID)
            .approvalStatus(UPDATED_APPROVAL_STATUS)
            .subscriptionType(UPDATED_SUBSCRIPTION_TYPE);

        restClientRoleMockMvc.perform(put("/api/client-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedClientRole)))
            .andExpect(status().isOk());

        // Validate the ClientRole in the database
        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeUpdate);
        ClientRole testClientRole = clientRoleList.get(clientRoleList.size() - 1);
        assertThat(testClientRole.getInvestorType()).isEqualTo(UPDATED_INVESTOR_TYPE);
        assertThat(testClientRole.getAssets()).isEqualTo(UPDATED_ASSETS);
        assertThat(testClientRole.getAssetClasses()).isEqualTo(UPDATED_ASSET_CLASSES);
        assertThat(testClientRole.getEquities()).isEqualTo(UPDATED_EQUITIES);
        assertThat(testClientRole.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testClientRole.getPaid()).isEqualTo(UPDATED_PAID);
        assertThat(testClientRole.getApprovalStatus()).isEqualTo(UPDATED_APPROVAL_STATUS);
        assertThat(testClientRole.getSubscriptionType()).isEqualTo(UPDATED_SUBSCRIPTION_TYPE);

        // Validate the ClientRole in Elasticsearch
        ClientRole clientRoleEs = clientRoleSearchRepository.findOne(testClientRole.getId());
        assertThat(clientRoleEs).isEqualToComparingFieldByField(testClientRole);
    }

    @Test
    @Transactional
    public void updateNonExistingClientRole() throws Exception {
        int databaseSizeBeforeUpdate = clientRoleRepository.findAll().size();

        // Create the ClientRole

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClientRoleMockMvc.perform(put("/api/client-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientRole)))
            .andExpect(status().isCreated());

        // Validate the ClientRole in the database
        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClientRole() throws Exception {
        // Initialize the database
        clientRoleRepository.saveAndFlush(clientRole);
        clientRoleSearchRepository.save(clientRole);
        int databaseSizeBeforeDelete = clientRoleRepository.findAll().size();

        // Get the clientRole
        restClientRoleMockMvc.perform(delete("/api/client-roles/{id}", clientRole.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean clientRoleExistsInEs = clientRoleSearchRepository.exists(clientRole.getId());
        assertThat(clientRoleExistsInEs).isFalse();

        // Validate the database is empty
        List<ClientRole> clientRoleList = clientRoleRepository.findAll();
        assertThat(clientRoleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchClientRole() throws Exception {
        // Initialize the database
        clientRoleRepository.saveAndFlush(clientRole);
        clientRoleSearchRepository.save(clientRole);

        // Search the clientRole
        restClientRoleMockMvc.perform(get("/api/_search/client-roles?query=id:" + clientRole.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientRole.getId().intValue())))
            .andExpect(jsonPath("$.[*].investorType").value(hasItem(DEFAULT_INVESTOR_TYPE)))
            .andExpect(jsonPath("$.[*].assets").value(hasItem(DEFAULT_ASSETS)))
            .andExpect(jsonPath("$.[*].assetClasses").value(hasItem(DEFAULT_ASSET_CLASSES.toString())))
            .andExpect(jsonPath("$.[*].equities").value(hasItem(DEFAULT_EQUITIES.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].paid").value(hasItem(DEFAULT_PAID.toString())))
            .andExpect(jsonPath("$.[*].approvalStatus").value(hasItem(DEFAULT_APPROVAL_STATUS)))
            .andExpect(jsonPath("$.[*].subscriptionType").value(hasItem(DEFAULT_SUBSCRIPTION_TYPE)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientRole.class);
        ClientRole clientRole1 = new ClientRole();
        clientRole1.setId(1L);
        ClientRole clientRole2 = new ClientRole();
        clientRole2.setId(clientRole1.getId());
        assertThat(clientRole1).isEqualTo(clientRole2);
        clientRole2.setId(2L);
        assertThat(clientRole1).isNotEqualTo(clientRole2);
        clientRole1.setId(null);
        assertThat(clientRole1).isNotEqualTo(clientRole2);
    }
}
