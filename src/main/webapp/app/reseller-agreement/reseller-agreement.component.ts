import { Component, OnInit, Inject } from '@angular/core';
import {WINDOW} from '../layouts/main/window.service';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';
@Component({
	selector: 'jhi-reseller-agreement',
	templateUrl: './reseller-agreement.component.html',
	styleUrls: [
		'reseller-agreement.scss',
		'../../content/scss/amass-form.scss'
	]

})
export class ResellerAgreementComponent implements OnInit {
    constructor(
        @Inject(WINDOW) private window: Window,
        public googleAnalyticsService: GoogleAnalyticsEventsService
	) {
	}

	ngOnInit() {

	}

}
