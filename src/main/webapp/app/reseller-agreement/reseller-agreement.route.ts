import { Route } from '@angular/router';
import { ResellerAgreementComponent } from './reseller-agreement.component';

export const RESELLERAGREEMNT_ROUTE: Route = {
	path: 'reseller-agreement',
	component: ResellerAgreementComponent,
	data: {
		authorities: [],
		pageTitle: 'Reseller Agreement',
		breadcrumb: 'Reseller Agreement',
		type: 'static'
	}
};
