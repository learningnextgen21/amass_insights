import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ResellerAgreementComponent } from './reseller-agreement.component';
import { RESELLERAGREEMNT_ROUTE } from './reseller-agreement.route';


@NgModule({
	imports: [
		RouterModule.forRoot([ RESELLERAGREEMNT_ROUTE ], { useHash: true })
	],
	declarations: [
		ResellerAgreementComponent,
	],
	entryComponents: [
	],
	providers: [

	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceResellerAgreementModule {}
