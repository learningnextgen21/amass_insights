import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Account, LoginModalService, Principal } from '../shared';

import { SigninDialogComponent } from '../dialog/signindialog.component';

import { ApplyAccountComponent } from '../account/apply/apply.component';
import { BlogService } from './blog.service';
import { NgForm } from '@angular/forms';
import { DataArticleService } from '../entities/data-article/data-article.service';
import { DataArticle } from '../entities/data-article/data-article.model';
import { JhiAlertService } from 'ng-jhipster';
import { DatePipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';

@Component({
	selector: 'jhi-blog',
	templateUrl: './blog.component.html',
	styleUrls: [
		'blog.scss',
		'../../content/scss/amass-form.scss'
	]

})
export class BlogComponent implements OnInit {
	account: Account;
	modalRef: NgbModalRef;
	mdDialogRef: MatDialogRef<any>;
	contact: any;
	inquiryTypes = [];
	contactFields: any;
	validationError: any;
	error: any;
	responseMessage: any;
    formValid: boolean;
    subscription: any;
    totalItems: any;
    dataArticles: DataArticle[];
    articleObj: any;
    isAdmin: any;

	constructor(
		private principal: Principal,
		private loginModalService: LoginModalService,
		private eventManager: JhiEventManager,
		public dialog: MatDialog,
        private contactService: BlogService,
        private dataArticleService: DataArticleService,
        private alertService: JhiAlertService,
        private datePipe: DatePipe,
        public _DomSanitizer: DomSanitizer,
		private router: Router,
		public googleAnalyticsService: GoogleAnalyticsEventsService
	) {
    }


    loadAll() {
        let queryBody = {};
        const req = {
            'filter': {
                'query': {
                    'bool': {
                        'must': {
                            'terms': {
                                'authorOrganization.id': [7828]
                            }
                        }
                    }
                }
            }
        };


        if (this.isAuthenticated()) {
            this.subscription = this.dataArticleService.elasticSearchAuthenticated(req).subscribe(
                res => this.onSuccess(res, res.headers),
                res => this.onError(res)
            );
        } else {
            this.subscription = this.dataArticleService.blogUnrestricted(queryBody).subscribe(
                res => this.onSuccess(res, res.headers),
                res => this.onError(res)
            );
        }
		return;
	}

	ngOnInit() {
		this.responseMessage = null;
		this.error = null;
		this.contactFields = {};
		this.formValid = false;
		this.inquiryTypes = ['General Inquiry', 'Help Finding Data', 'Help Selling My Data', 'Press', 'Tech Support'];
		this.principal.identity().then(account => {
			this.account = account;
		});
		this.registerAuthenticationSuccess();
        this.validationError = null;

        this.loadAll();

	}

	registerAuthenticationSuccess() {
		this.eventManager.subscribe('authenticationSuccess', (message) => {
			this.principal.identity().then(account => {
				this.account = account;
			});
		});
	}

	isAuthenticated() {
		return this.principal.isAuthenticated();
	}

	login() {
		this.modalRef = this.loginModalService.open();
	}

	openSigninDialog() {
		this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
			width: '400px'
		});
	}

	openApplyDialog() {
		this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '400px'
		});
    }
    private onSuccess(data, headers) {
		this.totalItems = data['hits'] ? data['hits'].total : 0;
		if (data['hits'] && data['hits'].hits) {
			for (let i = 0; i <= data['hits'].hits.length; i++) {
                this.articleObj = data['hits'].hits[i]._source;
               /*  this.dataArticles.push(this.articleObj);
                console.log(this.dataArticles); */
            }
        }
    }
    private onError(error) {
		this.alertService.error(error.message, null, null);
    }
    read(events) {
		this.router.navigate(['/blog/amassing-insights-in-the-new-world-of-data']);
		this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
}
