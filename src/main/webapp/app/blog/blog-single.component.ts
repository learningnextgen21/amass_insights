import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-blog-single',
    templateUrl: 'blog-single.component.html',
    styleUrls: ['./blog-single.component.css', 'blog.scss',
    '../../content/scss/amass-form.scss']
})
export class BlogSingleComponent implements OnInit {

    constructor() { }

    ngOnInit() {

    }

}
