import { Routes } from '@angular/router';

import { BlogComponent } from './blog.component';
import {BlogSingleComponent} from './blog-single.component';

export const BLOG_ROUTE: Routes = [{
	path: 'blog',
	data: {
		pageTitle: 'home.title',
        breadcrumb: 'Blog',
        type: 'dynamic'
    },
    children: [
        {
          path: 'amassing-insights-in-the-new-world-of-data',
          component: BlogSingleComponent,
          data: {
            pageTitle: 'home.title',
            breadcrumb: 'Amassing Insights in the New World of Data',
            type: 'static'
          }
        }, {
            path: '',
            component: BlogComponent,
            data: {
              pageTitle: 'home.title',
              breadcrumb: '',
              type: 'dynamic'
            }
          }]
}];
