import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RecaptchaModule } from 'ng-recaptcha';

import { DatamarketplaceSharedModule } from '../shared';

import { BLOG_ROUTE, BlogComponent } from './';
import { BlogService } from './blog.service';
import {BlogSingleComponent} from './blog-single.component';

@NgModule({
	imports: [
		DatamarketplaceSharedModule,
		RouterModule.forRoot( BLOG_ROUTE , { useHash: true }),
		RecaptchaModule.forRoot()
	],
	declarations: [
        BlogComponent,
        BlogSingleComponent
	],
	entryComponents: [
	],
	providers: [
		BlogService
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceBlogModule {}
