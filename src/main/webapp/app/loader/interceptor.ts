
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {catchError, tap} from 'rxjs/operators';
import { EventEmitter, Injectable, Component, NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { HTTP_INTERCEPTORS,  HttpClientModule, HttpResponse, HttpErrorResponse, HttpClient, HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';






@Injectable()
export class MyHttpLogInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('processing request', request);

    const customReq = request.clone({
      headers: request.headers.set('app-language', 'it')
    });

    return next
      .handle(customReq).pipe(
      tap((ev: HttpEvent<any>) => {
        if (ev instanceof HttpResponse) {
          console.log('processing response', ev);
        }
      }),
      catchError((response) => {
        if (response instanceof HttpErrorResponse) {
          console.log('Processing http error', response);
        }

        return observableThrowError(response);
      }),);
  }
}
