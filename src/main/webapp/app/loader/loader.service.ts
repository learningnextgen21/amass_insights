
import {finalize} from 'rxjs/operators';
import { EventEmitter, Injectable, Component, NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { HTTP_INTERCEPTORS,  HttpClientModule, HttpClient, HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';






@Injectable()
export class LoadingIndicatorInterceptor implements HttpInterceptor {
  //  loading: any;
  constructor(private loadingIndicatorService: LoadingIndicatorService) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loadingIndicatorService.onStarted(req);
    return next
      .handle(req).pipe(
      finalize(() => this.loadingIndicatorService.onFinished(req)));
  }
}

@Injectable()
export class LoadingIndicatorService {
  onLoadingChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  private requests: HttpRequest<any>[] = [];
  onStarted(req: HttpRequest<any>): void {
    this.requests.push(req);
    this.notify();
  }
  onFinished(req: HttpRequest<any>): void {
    const index = this.requests.indexOf(req);
    if (index !== -1) {
      this.requests.splice(index, 1);
    }
    this.notify();
  }
  private notify(): void {
    this.onLoadingChanged.emit(this.requests.length !== 0);
  }
}
