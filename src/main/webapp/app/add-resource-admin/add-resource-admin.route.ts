import { Route } from '@angular/router';
import { CanDeactivateGuard, UserRouteAccessService } from 'app/shared';
import { AddResourceAdminFormComponent } from './add-resource-admin-form.component';

export const addResourceAdminRoute: Route = {
    path: 'add-resource-admin',
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'Data Resources',
        breadcrumb: 'Resources > Add New > Admin',
        type: 'dynamic'
    },
    component: AddResourceAdminFormComponent,
    canActivate: [UserRouteAccessService],
    canDeactivate: [CanDeactivateGuard]
};
