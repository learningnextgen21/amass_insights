
import {of as observableOf, Subject} from 'rxjs';
import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, Inject, Injectable, Input, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { DomSanitizer } from '@angular/platform-browser';
import { AddResourceAdminService } from './add-resource-admin.service';
import { DataProviderService } from 'app/entities/data-provider/data-provider.service';
import { ContactDialogComponent } from 'app/shared/contactdialog/contact-dialog.component';
import { Principal } from 'app/shared/auth/principal.service';
import { DataSource } from '@angular/cdk/table';
import { GoogleAnalyticsEventsService } from 'app/shared/googleanalytics/google-analytics-events.service';
import { LoaderService } from '../loader/loaders.service';
import { NestedTreeControl, FlatTreeControl } from '@angular/cdk/tree';
import { WINDOW } from 'app/layouts';
import { SigninModalService, AmassFilterService, CSRFService, AuthServerProvider, StateStorageService } from 'app/shared';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig, FormlyTemplateOptions } from '@ngx-formly/core';
import { ConfirmDialogComponent } from 'app/shared/dialogs/confirm-dialog.component';
import { UppyService, UppyComponent } from 'app/uppy';
import { contains } from 'ramda';
let _that = null;
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie';
import { DataOrganization } from '../../app/entities/data-organization/data-organization.model';
import { DataProviderLinkAdminFormComponent } from 'app/entities/data-provider/data-provider-link-admin-form.component';
import { environmentDev } from 'app/environments/environment.dev';

@Component({
	selector: 'jhi-add-resource-admin-form',
	templateUrl: './add-resource-admin-form.component.html',
	styleUrls: [ '../../content/css/material-tab.css']
})
export class AddResourceAdminFormComponent implements OnInit {
    dialogRef: MatDialogRef<ConfirmDialogComponent>;
    //adminDialogRef: MatDialogRef<AddResourceAdminFormComponent>
    resDialogRef: MatDialogRef<any>;
    formSubmitted: boolean;
    dataResourceName: any;
    dataResourceId: any;
    dataResourceModel: any;
    dataResourceRecordId: any;
    dataResourceLinkID: any;
    routerOutletRedirect: string;
    valueChanged: boolean;

    dataOrganization: DataOrganization;
    resourceID: number;
    basicProvider: any;
    submissionMessage: boolean;
    dataName: any;
    providerID: any;
    basicModelID: any;
    resourceRecordID: any;
    linkID: any;
    emptyField: boolean;
    dataLinksList: any[];
    linkMapId: any;
    editLinkMapId: any;
    editRelevantFile: any;
    providerRecordID: any;
    fileId: any;
    fileName: any;
	private subscription: Subscription;
	private eventSubscriber: Subscription;
	isLoading: boolean;
	isAdmin: boolean;
	isPendingUnlock: boolean;
	isPendingExpand: boolean;
	domSanitizer: any;
	authoredArticles: any;
	relevantArticles: any;
	tabIndex: number;
	relevantArticlePage: number;
	relevantArticleLinks: any;
	relevantArticleFrom: number;
	totalRelevantArticles: number;
	authoredArticlePage: number;
	authoredArticleLinks: any;
	authoredArticleFrom: number;
	totalAuthoredArticles: number;
	recordID: string;
	profileTabLabel: any;
    ProfileCategory: any;
    equitesDelete: any;
    relevantOrganizations: any[] = [];
    relevantDataProviders: any[] = [];
    trackLoad: any;
    closePopup: boolean;
    redirect: any;

	progressBarColor = 'primary';
	progressBarMode = 'determinate';
    mdTooltipDelay: number;
    trackname: any;
    tracklink: any;
    trackfile: any;
    trackownerOrganizationCity: any;
    trackownerOrganizationState: any;
    trackownerOrganizationCountry: any;
    trackownerOrganizationYearfound: number;
    trackownerOrganizationHeadcount: any;
    trackownerOrganizationHeadcountNumber: any;
    trackownerOrganizationHeadcountBucket: any;
    tracknumberOfAnalystEmployees: any;
    trackInvestor: any;
    trackNumberOfInvestor: any;
    topicsCount: number = 0;

    dataLinks: any[] = [];
    dataFiles: any[] = [];
    resourceMarketplaceStatus: any[] = [];
    resourceWorkFlows: any[] = [];
    resourceResearchMethods: any[] = [];
    resourceIEIStatus: any[] = [];
    resourcePriority: any[] = [];
    resourceTopics: any[] = [];
    resourcePurposes: any[] = [];
    resourcePurposeType: any[] = [];
    selectedValue: any[] = [];

    basicExpansionPanel: boolean;
    adminExpansionPanel: boolean;
    categorizationsExpansionPanel: boolean;
    relevantEntitiesPanel: boolean;
    panelName:string;
    overAll:string;
    progressSpinner:boolean;

    basicForm = new FormGroup({});
    basicLinkForm = new FormGroup({});
    basicFileForm = new FormGroup({});
    basicResourceForm = new FormGroup({});

    basicModel: any = {};

    basicOptions: FormlyFormOptions = {};
    basicLinkOptions: FormlyFormOptions = {};
    basicFileOptions: FormlyFormOptions = {};
    basicResourceOptions: FormlyFormOptions = {};

	adminForm = new FormGroup({});
	adminModel: any = {};
    adminOptions: FormlyFormOptions = {};

    categorizationsDetailsForm = new FormGroup({});
	categorizationsDetailsModel: any = {};
    categorizationsDetailsOptions: FormlyFormOptions = {};

    relevantEntitiesDetailsForm = new FormGroup({});
	relevantEntitiesDetailsModel: any = {};
    relevantEntitiesDetailsOptions: FormlyFormOptions = {};


	basicLinkFields: FormlyFieldConfig[] = [
		{
			key: 'link',
			type: 'autocomplete',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Link:',
				placeholder: '',
                required: false,
                options: this.dataLinks,
                attributes: {
                    field: 'link',
                    'trackNames': 'Link',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Dropdown - Link',
                    'minLength': '1'
                },
				change: (event: any) => {
                    //console.log('default value....', event);
                    if(event) {
                        this.valueChanged = true;
                    }
					const queryObj = event;
					this.dataLinks = [];
                    this.basicInput(event);
                    if(queryObj.query) {
                        this.filterService.getDataLinks(queryObj.query).subscribe(d => {
                            const listItems =  d ? d['hits']['hits'] : [];
                            //console.log('...search...', listItems);
                            for (let i = 0; i < listItems.length; i++) {
                                //console.log('...search 1...', listItems);
                                this.dataLinks.push({
                                    id: listItems[i]._source.id,
                                    link: listItems[i]._source.link,
                                });
                                //console.log('...search...', this.dataOrganizations);
                            }
                            this.basicLinkFields[0].templateOptions.options = this.dataLinks;
                        });
                    }
				},
				blur: (event: any) => {
					// if (event && event.target && !event['selectedVal']) {
                    //     event.target.value = '';
                    // }
                    if (event && event.target && event.target.value === '') {
                        this.emptyField = true;
                        this.basicModel.link = null;
                        console.log('link blur', event.target.value, this.emptyField);
                    }
                },
                click: (event: any) => {
                    this.linkID = event ? event.id : '';
					if (this.linkID != '' && this.linkID != undefined && this.linkID != null) {
                        this.emptyField = false;
                        this.basicModel = {
                            ...this.basicModel,
                            name: event.link
                        }
					} else {
						this.emptyField = true;
					}
					//console.log(this.organizationButton, '------', this.emptyField);
				}
			},
        },
    ];
    basicFileFields: FormlyFieldConfig[] = [
        {
			key: 'file',
			type: 'autocomplete',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'File:',
				placeholder: '',
                required: false,
                options: this.dataFiles,
                attributes: {
                    field: 'fileName',
                    'trackNames': 'File',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Attachment - File',
                    'logoType': 'Logo',
                    'minLength': '1'
                },
				change: (event: any) => {
                    //console.log('default value....', event);
					const queryObj = event;
					this.dataFiles = [];
                    if(event) {
                        this.valueChanged = true;
                    }
                    if(queryObj.query) {
                        this.filterService.getAllFiles(queryObj.query).subscribe(d => {
                            const listItems =  d ? d['hits']['hits'] : [];
                            //console.log('...search...', listItems);
                            for (let i = 0; i < listItems.length; i++) {
                                //console.log('...search 1...', listItems);
                                this.dataFiles.push({
                                    id: listItems[i]._source.id,
                                    fileName: listItems[i]._source.fileName,
                                });
                                //console.log('...search...', this.dataOrganizations);
                            }
                            this.basicFileFields[0].templateOptions.options = this.dataFiles;
                        });
                    }
				},
				blur: (event: any) => {
					//console.log('Blur event', event);
					// if (event && event.target && !event['selectedVal']) {
					// 	// event.target.value = '';
                    // }
                    if (event && event.target && event.target.value === '') {
                        this.basicModel.file = null;
                    }
                },
                click: (event: any) => {
                    //console.log('click event', event);
                    //console.log('Basic Form', this.basicFileForm);
                    this.basicModel = {
                        ...this.basicModel,
                        name: event.fileName
                    }
				}
			},
        },
        {
			key: '',
			type: 'fileUpload',
			className: 'Width',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: '',
				change: ( event : any) => {
					this.dataProviderService.onResourceFileUpload(this.providerRecordID, event[0]).subscribe(data=>{
                        this.fileName = data.fileName;
                        this.fileId = data.id;
                        if(this.fileName) {
                            this.basicModel = {
                                ...this.basicModel,
                                file: {id: this.fileId, fileName: this.fileName} ,
                                name: this.fileName
                            }
                        }
					});
				}
            },
            hideExpression: 'model.file',
		}
    ];
    basicResourceFields: FormlyFieldConfig[] = [
        {
			key: 'name',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Resource Name:',
				placeholder: '',
                required: true,
                attributes: {
                    'trackNames': 'Resource Name',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Textbox - Name'
                },
				change: (event: any) => {
                    // console.log('Drop', event);
                    this.basicInput;
                    if(event) {
                       // console.log('Input', event);
                        this.valueChanged = true;
                    }
                },
                keypress:(event)=> {
                   // console.log('Event', event);
                    if(event) {
                      this.valueChanged = true;
                     // console.log('Event 1', this.valueChanged);
                    }
                },
			},
		},
	];

    adminFields: FormlyFieldConfig[] = [
        {
			key: 'userPermission',
            type: 'radioButton',
            defaultValue: 'ROLE_SILVER',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'User Permissions:',
				required: true,
                description: 'Who should be able to see this resource?',
                attributes: {
                    'trackNames': 'User Permissions',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Dropdown - User Permissions'
                },
				options: [{
					value: 'ROLE_ADMIN',
					label: 'Admin'
				}, {
					value: 'ROLE_SILVER',
					label: 'Silver'
				}, {
					value: 'ROLE_BRONZE',
					label: 'Bronze'
				}, {
					value: 'ROLE_USER',
					label: 'Registered'
                }],
                change: (event: any) => {
                    // console.log('Drop', event);
                     if(event) {
                         this.valueChanged = true;
                     }
                }
			},
		},
		{
			key: 'marketplaceStatus.id',
            type: 'dropdown',
            className: 'customMediumWidth-dropdown',
            defaultValue: environmentDev.dev ? 2043 : 1985,
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Marketplace Status:',
                description: 'What should an external client understand about the status of our research on this resource?',
				required: true,
                options: this.resourceMarketplaceStatus,
                attributes: {
                    'placeholderText' : '',
                    field: 'label',
                    'labelTooltip': 'The only statuses IEI will be using are ""Minimal"" and ""Complete"". Ignore all other statuses.Explanation of choices:""Minimal"": When you\'ve only done the minimal research""Complete"": When the full research has been completed	',
                    'descriptionTooltip': 'The only statuses IEI will be using are ""Minimal"" and ""Complete"". Ignore all other statuses. Explanation of choices:""Minimal"": When you\'ve only done the minimal research""Complete"": When the full research has been completed',
                    'trackNames': 'Marketplace Status',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Dropdown - Marketplace Status'
                },
                change: (event: any) => {
                   // console.log('Drop', event);
                    if(event) {
                        this.valueChanged = true;
                    }
                }
			},
        },
        {
			key: 'workflows',
            type: 'multiselect',
            //className: 'custom-multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Workflows:',
                description: 'What are the workflows that need to be completed to complete the research on this resource?',
				required: false,
				options: this.resourceWorkFlows,
                attributes: {
                    'placeholderText' : 'An indication of the process that should be taken when reviewing the resource.',
					field: 'desc',
                    'labelTooltip': 'If the resource mentions more than 10 potentially relevant companies, designate it Numerous Companies, then label the resource \'Amass Review\' and move on.If each tracked company in the resource has been categorized as a Data Tool, designate it Data Tool and move on.If a tracked Data Tool is mentioned, you may add the Data Tool workflow.',
                    'descriptionTooltip': 'If the resource mentions more than 10 potentially relevant companies, designate it Numerous Companies, then label the resource \'Amass Review\' and move on.If each tracked company in the resource has been categorized as a Data Tool, designate it Data Tool and move on.If a tracked Data Tool is mentioned, you may add the Data Tool workflow.',
                    'placeholderTooltip': 'If the resource mentions more than 10 potentially relevant companies, designate it Numerous Companies, then label the resource \'Amass Review\' and move on.If each tracked company in the resource has been categorized as a Data Tool, designate it Data Tool and move on.If a tracked Data Tool is mentioned, you may add the Data Tool workflow.',
                    'trackNames': 'Workflows',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Dropdown - Workflow'
                },
                change: (event: any) => {
                    if(event) {
                        this.valueChanged = true;
                    }
                }
            },
        },
		{
			key: 'researchMethodsCompleted',
            type: 'multiselect',
            className: 'custom-multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Research Methods Completed:',
                description: 'What types/methods of researching this resource have been completed?',
				required: true,
				options: this.resourceResearchMethods,
                attributes: {
                    'placeholderText' : '',
					field: 'desc',
                    'labelTooltip': 'Fill these in as you research a resource, and especially after you\'re done researching a resource.',
                    'descriptionTooltip': 'Fill these in as you research a resource, and especially after you\'re done researching a resource.',
                    'trackNames': 'Research Methods Completed',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Dropdown - Research Methods Completed'
                },
                change: (event: any) => {
                    if(event) {
                        this.valueChanged = true;
                    }
                }
            },
        },
        {
			key: 'ieiStatus.id',
            type: 'dropdown',
            className: 'mediumWidthSticky-dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'IEI Status:',
                description: 'What\'s the status of this resource\'s research?',
				required: false,
				options: this.resourceIEIStatus,
                attributes: {
                    'placeholderText' : '',
					field: 'label',
                    'labelTooltip': '',
                    'descriptionTooltip': '',
                    'trackNames': 'IEI Status',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Dropdown - IEI Status'
                },
                change: (event: any) => {
                    // console.log('Drop', event);
                     if(event) {
                         this.valueChanged = true;
                     }
                }
            },
        },
        {
			key: 'priority.id',
            type: 'dropdown',
            className: 'widthSticky-dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Priority:',
                description: '',
				required: false,
				options: this.resourcePriority,
                attributes: {
                    'placeholderText' : 'If filled in, this denotes how a researcher should prioritize their work.',
					field: 'label',
                    'labelTooltip': '',
                    'descriptionTooltip': '',
                    'trackNames': 'Priority',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Dropdown - Priority'
                },
                change: (event: any) => {
                    // console.log('Drop', event);
                     if(event) {
                         this.valueChanged = true;
                     }
                }
            },
		},
    ];
    categorizationsFields: FormlyFieldConfig[] = [
        {
			key: 'topics',
            type: 'multiselect',
            className: 'custom-multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Topics ('+this.topicsCount+'):',
                description: 'What topics does this resource relate to or touch upon?',
				required: false,
				options: this.resourceTopics,
                attributes: {
                    'placeholderText' : '',
					field: 'id',
                    'labelTooltip': '',
                    'descriptionTooltip': '',
                    'trackNames': 'Topics',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Dropdown - Topic'
                },
                change: (event: any) => {
                    if(event) {
                        this.valueChanged = true;
                    }
                }
            },
        },
        {
			key: 'purposes',
            type: 'multiselect',
            className: 'custom-multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Purposes:',
                description: '',
				required: false,
				options: this.resourcePurposes,
                attributes: {
                    'placeholderText' : 'A quick categorization of the focus of the resource.',
					field: 'id',
                    'labelTooltip': 'There may be inaccurate purposes already recorded, these can be taken out.There is no limit on the number of purposes a resource can have.Resources regarding new organizations, events or products should be labelled \'New\'.For Employee Movement resources, do not link all of the former Organizations and Data Providers the employees mentioned in the resource worked at. Only link the most immediate Organizations and Data Providers that the employee left and went to. So if an employee went from Organization A to Organization B but used to work at Organization C, only link Organizations A and B.',
                    'descriptionTooltip': '',
                    'placeholderTooltip': 'There may be inaccurate purposes already recorded, these can be taken out.There is no limit on the number of purposes a resource can have.Resources regarding new organizations, events or products should be labelled New.For Employee Movement resources, do not link all of the former Organizations and Data Providers the employees mentioned in the resource worked at. Only link the most immediate Organizations and Data Providers that the employee left and went to. So if an employee went from Organization A to Organization B but used to work at Organization C, only link Organizations A and B.',
                    'trackNames': 'Purposes',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Dropdown - Purpose'
                },
                change: (event: any) => {
                    if(event) {
                        this.valueChanged = true;
                    }
                }
            },
        },
        {
			key: 'purposeTypes',
            type: 'multiselect',
            className: 'custom-multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Purpose Types:',
                description: '',
				required: false,
				options: this.resourcePurposeType,
                attributes: {
                    'placeholderText' : '',
					field: 'id',
                    'labelTooltip': 'If an organization is only one product, link it as Organization, not Product. Most resources will be either Organization or Product types.',
                    'descriptionTooltip': '',
                    'placeholderTooltip': '',
                    'trackNames': 'Purpose Types',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Dropdown - Purpose Type'
                },
                change: (event: any) => {
                    if(event) {
                        this.valueChanged = true;
                    }
                }
            },
        },
    ];
    relevantEntitiesFields: FormlyFieldConfig[] = [
        {
			key: 'relavantOrganizations',
			type: 'autocomplete',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Relevant Organizations:*',
                type: 'autocomplete',
				//placeholder: '',
                required: false,
                options: this.relevantOrganizations,
                attributes: {
                    field: 'name',
                    'minLength': '1',
                    'multiple': 'true',
                    'placeholderText': 'All of the Organizations mentioned in or relevant to the resource or the content in the resource.',
                    'labelTooltip': 'Any organization mentioned and already in the database should be included.Only link Organizations that the resource really has any relevance towards. (As an example for Organizations not to link, see Purpose notes below).It\'s not necessary to link Organizations that are being mentioned in a way that isn\'t related to them being a data provider, data tool, or potential customer. For example, if the resource is an analysis of other companies, the companies beiung analyzed don\'t need to be linked here.',
                    'placeholderTooltip': 'Any organization mentioned and already in the database should be included.Only link Organizations that the resource really has any relevance towards. (As an example for Organizations not to link, see Purpose notes below).It\'s not necessary to link Organizations that are being mentioned in a way that isn\'t related to them being a data provider, data tool, or potential customer. For example, if the resource is an analysis of other companies, the companies beiung analyzed don\'t need to be linked here.',
                    'trackNames': 'Relevant Organizations',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Dropdown - Relevant Organizations'
                },
				change: (event: any) => {
                    //console.log('default value....', event);
					const queryObj = event;
					this.relevantOrganizations = [];
                    if(event) {
                        this.valueChanged = true;
                    }
                    const selected = this.basicModel['relavantOrganizations'];
                    ///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
                    if(selected && selected.length) {
                        this.selectedValue = selected.map(item => item.id);
                    }
                    if(queryObj.query) {
                        this.filterService.getAllOrganizations(queryObj.query).subscribe(d => {
                            const listItems = d && d['hits'] ? d['hits']['hits'] : [];
                            //console.log('...search...', listItems);
                            for (let i = 0; i < listItems.length; i++) {
                                if (!this.selectedValue.includes(listItems[i]._source.id)) {
                                //console.log('...search...', listItems);
                                    this.relevantOrganizations.push({
                                        id: listItems[i]._source.id,
                                        name: listItems[i]._source.name
                                    });
                                }
                                //console.log('...search...', this.dataOrganizations);
                            }
                            this.relevantEntitiesFields[0].templateOptions.options = this.relevantOrganizations;
                        });
                    }
				},
			},
        },
        {
			key: 'relavantDataProviders',
			type: 'autocomplete',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Relevant Data Providers:',
				placeholder: '',
                required: false,
                attributes: {
                    field : 'providerName',
                    'minLength': '1',
                    'multiple': 'true',
                    'placeholderText': 'All of the Data Providers mentioned in or relevant to the resource or the content in the resource.',
                    'labelTooltip': 'Each Data Provider should first be included as a Relevant Organization. Be sure new Data Providers have an Owner Organization listed and the entries are connected. It\'s not necessary to link Data Providers that are being mentioned in a way that isn\'t related to them being a data provider. For example, if the resource is an analysis of a company\'s retail sales, even if that company is a data provider themselves, since the resource analyzes that company and is not using that company\'s own data, it should not included.',
                    'placeholderTooltip': 'Each Data Provider should first be included as a Relevant Organization. Be sure new Data Providers have an Owner Organization listed and the entries are connected. It\'s not necessary to link Data Providers that are being mentioned in a way that isn\'t related to them being a data provider. For example, if the resource is an analysis of a company\'s retail sales, even if that company is a data provider themselves, since the resource analyzes that company and is not using that company\'s own data, it should not included.',
                    'trackNames': 'Relevant Data Providers',
                    'trackCategory': 'Resource Admin - Input',
                    'trackLabel': 'Resource Admin - Dropdown - Relevant Data Providers'
                },
				change: (event: any) => {
					const queryObj = event;
					this.relevantDataProviders = [];
                    if(event) {
                        this.valueChanged = true;
                    }
                    const selected = this.basicModel['relavantOrganizations'];
                    ///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
                    if(selected && selected.length) {
                        this.selectedValue = selected.map(item => item.id);
                    }
                    if(queryObj.query) {
                        this.filterService.getAllProviders(queryObj.query).subscribe(d => {
                        const listItems = d && d['hits'] ? d['hits']['hits'] : [];
                        for (let i = 0; i < listItems.length; ++i) {
                            const item = listItems[i]._source;
                            if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
                                if (!this.selectedValue.includes(item.id)) {
                                    this.relevantDataProviders.push({
                                        id: item.id,
                                        providerName: item.providerName,
                                        img: item.logo ? item.logo.logo : null
                                    });
                                }
                            }
                        }
                            this.relevantEntitiesFields[1].templateOptions.options = this.relevantDataProviders;
                        });
                    }
				},
			},
		},
    ];

    csrf: string;
    organisationRecordID:any;

	constructor(
        private dataProviderService: DataProviderService,
		private dataResourceService: AddResourceAdminService,
		private filterService: AmassFilterService,
		private route: ActivatedRoute,
		private router: Router,
		public dialog: MatDialog,
        private principal: Principal,
        private authService: AuthServerProvider,
        public _DomSanitizer: DomSanitizer,
        private adminDialogRef: MatDialogRef<AddResourceAdminFormComponent>,
		public googleAnalyticsService: GoogleAnalyticsEventsService,
		private loaderService: LoaderService,
		private signinModalService: SigninModalService,
		@Inject(WINDOW) private window: Window,
		public snackBar: MatSnackBar,
		private uppyService: UppyService,
        private csrfService: CSRFService,
        private el: ElementRef,
        private stateService: StateStorageService,
        private cookieService: CookieService
	) {
        this.routerOutletRedirect = '';
		this.domSanitizer = DomSanitizer;
		this.formSubmitted = false;
	}
	openUppyDialog(events:any){
		//this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        const dialogRef = this.dialog.open(UppyComponent, {
			width: '500px',
			height: '3000px'
		});
		// dialogRef.afterClosed().subscribe(result => {
		// 	console.log('The dialog was closed');
		//   });
	  }
	ngOnInit() {

         this.dataResourceModel = this.basicModel;
         this.constructFormModel(this.basicModel);
            if(this.basicModel) {
                this.dataResourceId = this.basicModel.id;
                this.dataResourceRecordId = this.basicModel.recordID;
                this.dataResourceName = this.basicModel.name;
                this.dataResourceLinkID = this.basicModel.link && this.basicModel.link.id ? this.basicModel.link.id : null;
                if (this.basicModel.link && this.basicModel.link.id) {
                    this.emptyField = true;
                }

                if (this.basicModel.workflows) {
                    const workflows = [];
                    for (const obj in this.basicModel.workflows) {
                        if (obj) {
                            workflows.push({
                                id: this.basicModel.workflows[obj].id,
                                desc: this.basicModel.workflows[obj].lookupcode
                            });
                        }
                    }
                    delete this.basicModel.workflows;
                    this.basicModel.workflows = workflows;
                }

                if (this.basicModel.researchMethodsCompleted) {
                    const researchMethodsCompleted = [];
                    for (const obj in this.basicModel.researchMethodsCompleted) {
                        if (obj) {
                            researchMethodsCompleted.push({
                                id: this.basicModel.researchMethodsCompleted[obj].id,
                                desc: this.basicModel.researchMethodsCompleted[obj].lookupcode
                            });
                        }
                    }
                    delete this.basicModel.researchMethodsCompleted;
                    this.basicModel.researchMethodsCompleted = researchMethodsCompleted;
                }

                if (this.basicModel.topics) {
                    const topics = [];
                    for (const obj in this.basicModel.topics) {
                        if (obj) {
                            topics.push({
                                id: this.basicModel.topics[obj].id,
                                desc: this.basicModel.topics[obj].topicName
                            });
                        }
                    }
                    delete this.basicModel.topics;
                    this.basicModel.topics = topics;
                }

                if (this.basicModel.purposes) {
                    const purposes = [];
                    for (const obj in this.basicModel.purposes) {
                        if (obj) {
                            purposes.push({
                                id: this.basicModel.purposes[obj].id,
                                desc: this.basicModel.purposes[obj].purpose
                            });
                        }
                    }
                    delete this.basicModel.purposes;
                    this.basicModel.purposes = purposes;
                }

                if (this.basicModel.purposeTypes) {
                    const purposeTypes = [];
                    for (const obj in this.basicModel.purposeTypes) {
                        if (obj) {
                            purposeTypes.push({
                                id: this.basicModel.purposeTypes[obj].id,
                                desc: this.basicModel.purposeTypes[obj].purposeType
                            });
                        }
                    }
                    delete this.basicModel.purposeTypes;
                    this.basicModel.purposeTypes = purposeTypes;
                }
            }
        //console.log('Basic links', this.editRelevantFile);
        if(this.basicProvider) {
            this.basicModel= {
                relavantDataProviders : [{id: this.basicProvider, providerName: this.dataName}]
            };
        }

        _that = this;
        this.trackownerOrganizationCity =  0;
        this.trackownerOrganizationState =  0;
        this.trackownerOrganizationCountry = 0;
        this.trackownerOrganizationYearfound = 0;
        this.trackownerOrganizationHeadcount =  0;
        this.trackownerOrganizationHeadcountBucket =  0;
        this.trackownerOrganizationHeadcountNumber =  0;
        this.topicsCount = 0;
        this.csrf = this.csrfService.getCSRF();
        this.filterService.setAuthorisedUrl();
		this.loaderService.display(false);
		this.isLoading = true;
		this.isPendingUnlock = false;
        this.isPendingExpand = false;
        this.valueChanged = false;
        this.tabIndex = 0;
		this.basicExpansionPanel = true;
        this.adminExpansionPanel= false;
        this.categorizationsExpansionPanel= false;
        this.relevantEntitiesPanel= true;
        if (this.dataResourceLinkID) {
            this.emptyField = false;
        } else {
            this.emptyField = true;
        }
		this.route.queryParams.subscribe(params => {
			if (params['pending-unlock']) {
				this.isPendingUnlock = true;
			}

			if (params['pending-expand']) {
				this.isPendingExpand = true;
			}
		});
		this.subscription = this.route.params.subscribe(params => {
			this.recordID = params['recordID'];
            //this.load(params['recordID']);
            this.trackLoad = this.recordID;
        });
		this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
		this.relevantArticlePage = 0;
		this.relevantArticleLinks = {
			last: 0
		};
		this.authoredArticlePage = 0;
		this.authoredArticleLinks = {
			last: 0
		};
        this.mdTooltipDelay = 500;

        // if (this.basicModel.providerPartners) {
        //     this.providerPartnersCount = this.dataProvider.providerPartners.length;
        //     this.relatedProvidersFields[0].templateOptions.label = 'Provider Data Partners ('+this.providerPartnersCount+'):';
        //     const providerPartners = [];
        //     for (const obj in this.dataProvider.providerPartners) {
        //         if (obj) {
        //             const partnerName = !this.dataProvider.providerPartners[obj].locked ? this.dataProvider.providerPartners[obj].providerName : 'Provider Locked';
        //             providerPartners.push({
        //                 id: this.dataProvider.providerPartners[obj].id,
        //                 providerName: partnerName
        //             });
        //         }
        //     }
        //     delete this.dataProvider.providerPartners;
        //     this.dataProvider.providerPartners = providerPartners;
        // }

        this.dataLinks = [];
		this.filterService.getDataLinks().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.dataLinks.push({
                    value: {
                        id: listItems[i]._source.id,
                        link: listItems[i]._source.link
                    },
                    label: listItems[i]._source.link
				});
            }
            this.basicLinkFields[0].templateOptions.options = this.dataLinks;
        });

        this.dataFiles = [];
		this.filterService.getAllFiles().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.dataFiles.push({
                    value: {
                        id: listItems[i]._source.id,
                        fileName: listItems[i]._source.fileName
                    },
                    label: listItems[i]._source.fileName
				});
            }
            this.basicFileFields[0].templateOptions.options = this.dataFiles;
		});

        this.resourceMarketplaceStatus = [];
		this.filterService.getLookupCodes('MARKETPLACE_STATUS').subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			// this.resourceMarketplaceStatus.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.resourceMarketplaceStatus.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
            }
            this.adminFields[1].templateOptions.options = this.resourceMarketplaceStatus;
        });

        this.resourceWorkFlows = [];
		this.filterService.getLookupCodes('WORKFLOW').subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.resourceWorkFlows.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.adminFields[2].templateOptions.options = this.resourceWorkFlows;
        });

        this.resourceResearchMethods = [];
		this.filterService.getLookupCodes('RESEARCH_METHOD').subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.resourceResearchMethods.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.adminFields[3].templateOptions.options = this.resourceResearchMethods;
        });

        this.resourceIEIStatus = [];
		this.filterService.getLookupCodes('IEI_STATUS').subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			// this.resourceIEIStatus.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.resourceIEIStatus.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
            }
            this.adminFields[4].templateOptions.options = this.resourceIEIStatus;
        });

        this.resourcePriority = [];
		this.filterService.getLookupCodes('PRIORITY').subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			// this.resourcePriority.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.resourcePriority.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
            }
            this.adminFields[5].templateOptions.options = this.resourcePriority;
        });

        this.resourceTopics = [];
		this.filterService.getDataTopics().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.resourceTopics.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.topicName
					},
					label: listItems[i]._source.topicName
				});
			}
			this.categorizationsFields[0].templateOptions.options = this.resourceTopics;
        });

        this.resourcePurposes = [];
		this.filterService.getDataResourcePurpose().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.resourcePurposes.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.purpose
					},
					label: listItems[i]._source.purpose
				});
			}
			this.categorizationsFields[1].templateOptions.options = this.resourcePurposes;
        });

        this.resourcePurposeType = [];
		this.filterService.getDataResourcePurposeType().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
                if(listItems[i]._source.purposeType !== '') {
                    this.resourcePurposeType.push({
                        value: {
                            id: listItems[i]._source.id,
                            desc: listItems[i]._source.purposeType
                        },
                        label: listItems[i]._source.purposeType
                    });
                }
			}
			this.categorizationsFields[2].templateOptions.options = this.resourcePurposeType;
        });

        this.relevantOrganizations = [];
        this.filterService.getAllOrganizations().subscribe(d => {
            const listItems = d && d['hits'] ? d['hits']['hits'] : [];
            for (let i = 0; i < listItems.length; i++) {
                this.relevantOrganizations.push({
                    id: listItems[i]._source.id,
                    name: listItems[i]._source.name
                });
            }
            this.relevantEntitiesFields[0].templateOptions.options = this.relevantOrganizations;
        });
    }


    openLinkDialog(events: any) {
		if(events === 'editLink') {
			if(this.linkID && !this.linkMapId && !this.editLinkMapId) {
				this.filterService.findByLinkId(this.linkID).subscribe(linkData => {
					this.loaderService.display(false);
					this.dataLinksList = linkData['hits'] ? linkData['hits'].hits[0]._source : null;
                });
            } else if(this.linkMapId && !this.editLinkMapId && !this.linkID) {
				this.filterService.findByLinkId(this.linkMapId).subscribe(linkData => {
					this.loaderService.display(false);
					this.dataLinksList = linkData['hits'] ? linkData['hits'].hits[0]._source : null;
                });
			} else if(this.editLinkMapId && !this.linkID && !this.linkMapId) {
				this.filterService.findByLinkId(this.editLinkMapId).subscribe(linkData => {
					this.loaderService.display(false);
					this.dataLinksList = linkData['hits'] ? linkData['hits'].hits[0]._source : null;
                });
			} else if(this.dataResourceLinkID) {
				this.filterService.findByLinkId(this.dataResourceLinkID).subscribe(linkData => {
					this.loaderService.display(false);
					this.dataLinksList = linkData['hits'] && linkData['hits'].hits[0] ?  linkData['hits'].hits[0]._source : null;
                });
			}
			setTimeout(() => {
				this.resDialogRef = this.dialog.open(DataProviderLinkAdminFormComponent, {
					width: '1000px',
					disableClose: true
                });
                this.resDialogRef.componentInstance.defaultValue = 'editLink';
                this.resDialogRef.componentInstance.basicModel = this.dataLinksList;
                this.resDialogRef.afterClosed().subscribe(result => {
					if(result) {
                        this.emptyField = false;
                        this.valueChanged = true;
                        // if((this.dataResourceModel.link && this.dataResourceModel.link.link) === this.dataResourceModel.name) {
                        //     this.basicModel = {
                        //         ...this.basicModel,
                        //         link: {id:result.id, link: result.link},
                        //         name: result.link
                        //     }
                        // }
						this.basicModel = {
							...this.basicModel,
                            link: {id:result.id, link: result.link},
                            name: result.link
                        }
                        this.editLinkMapId = result.id;
				    }
				});
			}, 500);
		} else if(events === 'addLink') {
			setTimeout(() => {
				this.resDialogRef = this.dialog.open(DataProviderLinkAdminFormComponent, {
					width: '1000px',
					disableClose: true
                });
                this.resDialogRef.afterClosed().subscribe(result=> {
					if(result) {
						this.emptyField = false;
						this.basicModel = {
							...this.basicModel,
                            link: {id:result.id, link: result.link},
                            name: result.link
                        }
                        this.linkMapId = result.id;
				    }
				});
				//this.orgDialogRef.componentInstance.basicProvider = this.dataProviderId;
			}, 500);
		}
    }

	getLinkImgUrl(desc) {
		if (desc) {
			const fileName = ((desc).toLowerCase()).replace(' ', '_');
			const link = 'content/images/' + fileName  + '.png';
			return link;
		}
	}

	providerProfileTab(event: MatTabChangeEvent) {

	   /*  this.profileTabLabel = 'Provider Profile - Tab ' + event.tab.textLabel;
		this.ProfileCategory = 'Provider Profile ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent(this.ProfileCategory, 'Tab Link Clicked', this.profileTabLabel); */
	}

	providerFollow(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	unlockProviders(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	requestMoreInfo(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	providerPartnerTab(event: MatTabChangeEvent) {
		this.profileTabLabel = 'Provider Profile -  ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent('Partner Provider', 'Internal Link Clicked', this.profileTabLabel);
	}

	dataFieldLink(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	profileWebsite(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	profileArticle(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
	samplesTab(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    addLinks(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

	load(recordID) {
		this.dataResourceService.findByIdForOrganizationForm(recordID).subscribe(dataOrganization => {
			this.loaderService.display(false);
            this.dataOrganization = dataOrganization['hits'].total ? dataOrganization['hits'].hits[0]._source : null;
			this.isLoading = false;
			this.constructFormModel(this.dataOrganization);
            this.basicModel = this.dataOrganization;
            this.providerID = this.dataOrganization ? this.dataOrganization.id : null;
		}, error => {
			if (error.status === 401) {
                this.stateService.storeUrl(null);
                this.router.navigate(['']);
				this.signinModalService.openDialog('Session expired! Please re-login.');
			}
			this.dataOrganization = null;
			this.isLoading = false;
		});
	   // this.loaderService.display(false);
    }

    constructFormModel(resourceTrack) {
        console.log(resourceTrack);
        this.trackname = resourceTrack.name ? resourceTrack.name.length : 0;
    }

	previousState() {
		this.window.history.back();
	}

	// openConfirmationDialog(nextState?: RouterStateSnapshot) {
    //     console.log(nextState);
    //     //const redirect = nextState.url;
    //     //this.dialog.closeAll();
    //     this.dialogRef = this.dialog.open(ConfirmDialogComponent, {
    //         disableClose: false
    //     });
    //     this.dialogRef.componentInstance.confirmMessage = 'You have some unsaved edits in this form.';
    //     this.dialogRef.componentInstance.button = {
    //         leave: true,
    //         confirm: true,
    //         leaveText: 'Leave and Don\'t Save',
    //         confirmText: 'Save and Submit'
    //     };
    //     this.dialogRef.afterClosed().subscribe(result => {
    //         if (result) {
    //             // do confirmation actions
    //             if (result === 'leave') {
    //                 this.dialog.closeAll();
    //             }

    //             if (result === 'save') {
    //                 //console.log('Popup', this.basicProvider, this.action);
    //                 this.submitResourceAdmin(this.basicModel);
    //                 this.dialog.closeAll();
    //             }
    //             this.dialogRef = null;
    //         }
    //     });
	// 	return false;
    // }

    openConfirmationDialog(nextState?: RouterStateSnapshot) {
        if(nextState) {
        this.redirect = nextState.url;
        }
        //this.dialog.closeAll();
        this.dialogRef = this.dialog.open(ConfirmDialogComponent, {
            disableClose: false
        });
        this.dialogRef.componentInstance.confirmMessage = 'You have some unsaved edits in this form.';
        this.dialogRef.componentInstance.button = {
            leave: true,
            confirm: true,
            leaveText: 'Leave and Don\'t Save',
            confirmText: 'Save and Submit'
        };
        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // do confirmation actions
                if(result === 'leave') {
                    if (this.redirect === '/') {
                        this.authService.logout().subscribe();
                        this.formSubmitted = true;
                        this.principal.authenticate(null);
                        this.stateService.storeUrl(null);
                        this.router.navigate(['../../']);
                    } else {
                        this.formSubmitted = true;
                        if(this.redirect) {
                            this.router.navigate([this.redirect]);
                            this.dialog.closeAll();
                        } else if (this.principal.isAuthenticated()) {
                            this.stateService.storeUrl(null);
                            if(this.redirect) {
                                this.router.navigate([this.redirect]);
                                this.dialog.closeAll();
                            }
                            this.dialog.closeAll();
                        } else {
                            this.stateService.storeUrl(null);
                            this.router.navigate(['']);
                            this.dialog.closeAll();
                        }
                    }
                }
                if (result === 'save') {
                    //console.log('Popup', this.basicProvider, this.action);
                    this.submitResourceAdmin(this.basicModel);
                    this.dialog.closeAll();
                }
                this.dialogRef = null;
            }
        });
		return false;
	}

	trackByIndex(index) {
		return index;
    }
    closeSignUpDialog() {
        if(this.valueChanged === false) {
            this.adminDialogRef.close();
        }
        if(this.valueChanged === true) {
            if(this.formSubmitted) {
                this.openConfirmationDialog();
            } else {
                if(this.formSubmitted === true) {
                    this.openConfirmationDialog();
                } else if(this.formSubmitted === false) {
                    this.openConfirmationDialog();
                }
            }
        }
    }

    onClose() {
       this.adminDialogRef.close();
    }

    updateResource(model, panelName, formName, currentPanel, nextPanel, tabIndex, submitMessage, action) {
        delete model['id'];
        delete model['recordID'];
        if(this.dataResourceId) {
            model['id'] = this.dataResourceId;
            model['recordID'] = this.dataResourceRecordId;
        } else {
            model['id'] = this.basicModelID;
            model['recordID'] = this.resourceRecordID;
        }
        this.progressSpinner=true;
        this.dataResourceService.updateResourceAdmin(model, action).subscribe(data => {
            if (data) {
                console.log('either', data, model);
                if (data && model.link === null && model.file === null) {
                    this.openSnackBar('Please fill either Link or File fields', 'Close');
                    this.progressSpinner=false;
                } else if (data && model.link !== null || model.file !== null) {
                    this.progressSpinner=false;
                    this.formSubmitted = true;
                    //this.basicModel = data;
                    this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                    if (action === 'submit') {
                        this.onClose();
                    }
                    this.loaderService.display(false);
                    if (currentPanel && currentPanel !== '') {
                        this[currentPanel] = false;
                        if (nextPanel && nextPanel !== '') {
                            this[nextPanel] = true;
                        }
                        if (tabIndex) {
                            this.tabIndex = tabIndex;
                        }
                    }
                }
            } else {
                this.formSubmitted = false;
                this.progressSpinner=false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        }, error => {
            if (error.status === 401) {
                this.formSubmitted = false;
                this.progressSpinner=false;
                this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
            } else {
                this.progressSpinner=false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        });
    }

    insertResource(model, panelName, formName, currentPanel, nextPanel, tabIndex, submitMessage, action) {
        this.progressSpinner=true;
        this.dataResourceService.addResourceAdmin(model).subscribe(data => {
            if (data) {
                console.log('either insert', data, model);
                if (data && model.link === undefined && model.file === undefined) {
                    this.openSnackBar('Please fill either Link or File fields', 'Close');
                    this.progressSpinner=false;
                } else if (data && model.link !== undefined || model.file !== undefined) {
                    this.progressSpinner=false;
                    this.formSubmitted = true;
                    this.basicModelID = data.id;
                    this.resourceRecordID = data.recordID;
                    this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                    if (action === 'submit') {
                        this.onClose();
                    }
                    this.loaderService.display(false);


                    if (currentPanel && currentPanel !== '') {
                        this[currentPanel] = false;
                        if (nextPanel && nextPanel !== '') {
                            this[nextPanel] = true;
                        }
                        if (tabIndex) {
                            this.tabIndex = tabIndex;
                        }
                    }
                }
            } else {
                this.progressSpinner=false;
                this.formSubmitted = false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        }, error => {
            if (error.status === 401) {
                this.progressSpinner=false;
                this.formSubmitted = false;
                this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
            } else {
                this.progressSpinner=false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        });
    }

	submitResourceAdmin(model, panelName?: string, currentPanel?: string, nextPanel?: string, tabIndex?: number, form?: string, action?: string) {
        //this.panelName = panelName;
       // console.log('panelname',panelName);
        if(panelName !=='Overall Details')
		{
			this.panelName = panelName;
            this.overAll='';
		}
		else
		{
            this.overAll= panelName;
            panelName = '';
		}
       // console.log('over',this.overAll);
        //console.log('panel',this.panelName);
        const formName = panelName ? panelName + '' : 'Form';
        // const submitMessage = `The Resource with the name "${model.name}" has been successfully submitted and has been added to the database immediately.`;
        const filterArr = Array.prototype.filter;
        if (form && this[form].valid) {
            // const autoCompleteFields = this.el.nativeElement.querySelectorAll('#' + form + ' .ui-autocomplete-input');
            // const filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
            //     return node.value === '';
            // });
            // if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
            //     setTimeout(() => {
            //         filteredAutoCompleteFields[0].focus();
            //     }, 1000);
            //     this.openSnackBar('Please fill all the required fields!', 'Close');
            //     this.expandFormPanels();
            // } else {
                if (model && !this.basicModelID && !this.resourceRecordID && !this.dataResourceId) {
                    const insertSubmitMessage = `The Resource with the name "${model.name}" has been successfully submitted and has been added to the database immediately.`;
                    this.insertResource(model, panelName, formName, currentPanel, nextPanel, tabIndex, insertSubmitMessage, action);
                } else {
                    const updateSubmitMessage = this.submissionMessage === true && action === 'submit' ? `The Resource with the name "${model.name}" has been successfully submitted and has been added to the database immediately.` : 'Your edits have been successfully submitted and have been applied to the profile immediately.';
                    this.updateResource(model, panelName, formName, currentPanel, nextPanel, tabIndex, updateSubmitMessage, action);
                }
           // }
        } else {
            let invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
            let autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
            let filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                return node.required && node.value === '';
            });
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else if (invalidElements.length > 0) {
                if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                    invalidElements[0].querySelectorAll('input')[0].focus();
                } else {
                    setTimeout(() => {
                        invalidElements[0].focus();
                    }, 1000);
                }
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else {
                this.tabIndex = this.tabIndex === 0 ? 1 : 0;
                setTimeout(() => {
                    invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
                    autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
                    filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                        return node.required && node.value === '';
                    });
                    if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                        setTimeout(() => {
                            filteredAutoCompleteFields[0].focus();
                        }, 1000);
                        this.openSnackBar('Please fill all the required fields!', 'Close');
                        this.expandFormPanels();
                    } else if (invalidElements.length > 0) {
                        if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                            invalidElements[0].querySelectorAll('input')[0].focus();
                        } else {
                            setTimeout(() => {
                                invalidElements[0].focus();
                            }, 1000);
                        }
                        this.openSnackBar('Please fill all the required fields!', 'Close');
                        this.expandFormPanels();
                    } else {
                        if (model && !this.basicModelID && !this.resourceRecordID && !this.dataResourceId) {
                            const insertSubmitMessage = `The Resource with the name "${model.name}" has been successfully submitted and has been added to the database immediately.`;
                            this.insertResource(model, panelName, formName, currentPanel, nextPanel, tabIndex, insertSubmitMessage, action);
                        } else {
                            const updateSubmitMessage = this.submissionMessage === true && action === 'submit' ? `The Resource with the name "${model.name}" has been successfully submitted and has been added to the database immediately.` : 'Your edits have been successfully submitted and have been applied to the profile immediately.';
                            this.updateResource(model, panelName, formName, currentPanel, nextPanel, tabIndex, updateSubmitMessage, action);
                        }
                    }
                }, 2000);
            }
        }
    }

    expandFormPanels() {
        this.basicExpansionPanel = true;
        this.adminExpansionPanel = true;
        this.categorizationsExpansionPanel= true;
        this.relevantEntitiesPanel= true;
    }

    formSubmit(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    formSave(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    basicInput($event) {
        const inputKey = $event;
        /* delete entry */
        if (inputKey.key === 'name') {
            if (inputKey.model.name.length < _that.trackname) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);

            } else if (inputKey.model.name.length > _that.trackname) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        }
    }

    openSnackBar(message: string, action: string, delay?: number) {
        this.snackBar.open(message, action, {
            duration: delay ? delay : 10000,
            panelClass: ['blue-snackbar']
        });
    }

	submitBasic(model) {
		// console.log(model);
		this.basicExpansionPanel = false;
		this.adminExpansionPanel = true;
	}

	submitAdmin(model) {
		// console.log(model);
		this.adminExpansionPanel = false;
		this.categorizationsExpansionPanel = true;
	}

	submitCategorization(model) {
		// console.log(model);
        this.categorizationsExpansionPanel = false;
        this.tabIndex = 1;
    }

    submitRelatedEntities(model) {
		// console.log(model);
        this.relevantEntitiesPanel = true;
	}


    beforeSessionExpires(event) {
        if (event) {
           // this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }

    onExpireSession(event) {
        if (event) {
            this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
                this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
            });
        }
    }

    @HostListener('document:keyup', [])
    @HostListener('document:click', [])
    @HostListener('document:wheel', [])
    @HostListener('window:scroll', [])
    resetTimer() {
        this.authService.notifyUserAction();
    }

    @HostListener('window:popstate', [])
    onPopState() {
        this.cookieService.put('logoutClicked', '0');

    }

    @HostListener('window:beforeunload', ['$event'])
    preventUser($event) {
        $event.preventDefault();
        return $event.returnValue = "Are you sure you want to exit?";
    }
}
