import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { DataOrganization } from '../entities/data-organization/data-organization.model';
import { ResponseWrapper, createRequestOption } from 'app/shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class AddResourceAdminService {
    public relevantProviders: any;
    public  resourcesObj: any;
    private resourceElasticSearchUrl = 'api/es/organizationsearch';
    private resourceAdminUrl = 'api/resources';
    private updateAllResourceAdminUrl = 'api/resourcesList';

	constructor(private http: HttpClient) { }

    findByIdForOrganizationForm(recordID: string): Observable<DataOrganization> {
		const req = {
			'_source': {
				'exclude': [

				],
				'include': [
                    'name',
                    'file',
                    'link',
                    'userPermission',
                    'workflows',
                    'marketplaceStatus',
                    'researchMethodsCompleted',
                    'ieiStatus',
                    'priority',
                    'topics',
                    'purposes',
                    'purposeTypes',
                    'relavantOrganizations',
                    'relavantDataProviders'
				]
			},
			'filter': {
				'query': {
					'match': {
						'recordID': recordID
					}
				}
			}
		};
		return this.http.post(this.resourceElasticSearchUrl + '?detail=true', req);
	}

    addResourceAdmin(request?: any): Observable<any> {
        const model = request;
        //console.log('ID 1', request.ieiStatus.id);
        if(request && request.ieiStatus && request.ieiStatus.id === null || undefined) {
            model.ieiStatus = null;
            //console.log('ID', request.ieiStatus.id);
        }
        if(request && request.priority && request.priority.id === null || undefined) {
            model.priority = null;
        }
        return this.http.post(this.resourceAdminUrl, model);
    }

    updateResourceAdmin(request?: any, action?: string): Observable<any> {
        const model = request;
        if(request && request.ieiStatus && request.ieiStatus.id === null || undefined) {
            model.ieiStatus = null;
            //console.log('ID', request.ieiStatus.id);
        }
        if(request && request.priority && request.priority.id === null || undefined) {
            model.priority = null;
        }
        if (request && request.ieiStatus && typeof(request.ieiStatus) === 'object' && !request.ieiStatus.id) {
            model.ieiStatus = null;
        }
        if (request && request.priority && typeof(request.priority) === 'object' && !request.priority.id) {
            model.priority = null;
        }
        return this.http.put(this.resourceAdminUrl, model);
    }

    createResourceLink(request?: any): Observable<any> {
        const model = request;
        return this.http.post(this.resourceAdminUrl, model);
    }

    updateResourceLink(request?: any): Observable<any> {
        const model = request;
        return this.http.put(this.resourceAdminUrl, model);
    }

    deleteResourceLink(request?: any, deleteLinkObj?: any, dataProviderId?:any, resources?: any): Observable<any> {
        const model = request;
        for(let i=0; i < resources.length; i++) {
            if(deleteLinkObj === resources[i].id) {
                this.relevantProviders = resources[i].relavantDataProviders;
                for(let i=0; i < this.relevantProviders.length; i++) {
                    if(this.relevantProviders[i].id === dataProviderId) {
                         this.resourcesObj= resources[i];
                      this.relevantProviders.splice(i, 1);
                      this.resourcesObj.relavantDataProviders = this.relevantProviders;
                    }
                }
               // console.log('Delete',this.deleteLinkObj,this.resources[i].id, this.resources[i].relavantDataProviders, this.itemRelevantProviders);
            }
        }
        return this.http.delete(this.resourceAdminUrl + '?providerId='+dataProviderId +'&resourceId='+deleteLinkObj);

    }

    updateAllResources(request?: any, action?: string): Observable<any> {
        const model = request;
        return this.http.post(this.updateAllResourceAdminUrl, model);
    }
}
