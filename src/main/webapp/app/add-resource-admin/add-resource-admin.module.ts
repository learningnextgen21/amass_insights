import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
    DatamarketplaceSharedModule,
    FormlyEditorFieldComponent,
    FormlyAutoCompleteFieldComponent,
    FormlyMultiSelectFieldComponent,
    FormlyDropdownFieldComponent,
    FormlyMaskFieldComponent,
    FormlySliderFieldComponent,
    FormlyRadioButtonFieldComponent,
    FormlyTextAreaFieldComponent
} from '../shared';
import {
    DataProviderService,
    DataProviderPopupService,
    DataProviderComponent,
    DataProviderDetailComponent,
    DataProviderEditComponent,
    DataProviderDialogComponent,
    DataProviderPopupComponent,
    DataProviderDeletePopupComponent,
    DataProviderDeleteDialogComponent,
    dataProviderRoute,
    dataProviderPopupRoute,
    TreeFlatOverviewExampleComponent
} from '../entities/data-provider';
//import {DataProviderAdminFormComponent} from './data-provider-admin-form.component';
import { FormlyModule } from '@ngx-formly/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyHorizontalWrapperComponent } from 'app/entities/data-provider/horizontal-wrapper';
import { UppyModule } from 'app/uppy/uppy.module';
import { UppyService } from 'app/uppy';
import { GenericProfileModule } from 'app/shared/generic-profile';
// import { LoadingIndicatorService, LoadingIndicatorInterceptor} from '../../loader/loader.service';

const ENTITY_STATES = [...dataProviderRoute, ...dataProviderPopupRoute];
// import { ShareButtonModule } from 'ngx-sharebuttons/button';
// import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { MatMenuModule } from '@angular/material/menu';
import { CommentsModule } from '../comments';
import { from } from 'rxjs';
import { FormlyDatePickerFieldComponent } from 'app/shared/custom-form-fields/datepicker-field.component';
import { FormlyFileUploadFieldComponent } from  'app/shared/custom-form-fields/fileupload-form-field.component';
import { AddResourceAdminFormComponent } from './add-resource-admin-form.component';
import { addResourceAdminRoute } from './add-resource-admin.route';
import { AddResourceAdminService } from './add-resource-admin.service';

@NgModule({
    imports: [
        CommentsModule,
        DatamarketplaceSharedModule,
        // HttpClientModule,
        ReactiveFormsModule,
        MatMenuModule,
        RouterModule.forRoot([addResourceAdminRoute], { useHash: true }),
        FormlyModule.forRoot({
            wrappers: [{ name: 'form-field-horizontal', component: FormlyHorizontalWrapperComponent  }],
            validationMessages: [
                { name: 'required', message: 'This field is required' },
            ],
            types: [{
                name: 'inputEditor', component: FormlyEditorFieldComponent
            }, {
                name: 'autocomplete', component: FormlyAutoCompleteFieldComponent
            }, {
                name: 'multiselect', component: FormlyMultiSelectFieldComponent
            }, {
                name: 'dropdown', component: FormlyDropdownFieldComponent
            }, {
                name: 'mask', component: FormlyMaskFieldComponent
            }, {
                name: 'slider', component: FormlySliderFieldComponent
            }, {
                name: 'radioButton', component: FormlyRadioButtonFieldComponent
            }, {
                name: 'textarea', component: FormlyTextAreaFieldComponent
            }, {
                name: 'datepicker', component: FormlyDatePickerFieldComponent
            }, {
                name: 'fileUpload', component: FormlyFileUploadFieldComponent
            }]
        }),
        FormlyBootstrapModule,
        UppyModule,
    ],
    declarations: [
        AddResourceAdminFormComponent
    ],
    entryComponents: [
        
    ],
    providers: [
        AddResourceAdminService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceAddResourceAdminFormModule {}
