import { BaseEntity } from '../shared';
import { DataProvider } from '../entities/data-provider/data-provider.model';
import { DataOrganization } from '../entities/data-organization';

export class DataResource implements BaseEntity {
    constructor(
		public id?: number,
        public recordID?: string,
        public name?: any,
        public topics?: any,
        public purposeTypes?: any,
        public purposes?: any,
        public purpose?: any,
        public description?: string,
        public link?: any,
        public userPermission?: any,
        public locked?: any,
        public file?: any,
        public fileName?: any,
        public createdTimeFormula?: any,
        public dataLinksCategory?: DataResource[],
        public relavantOrganizations?: DataOrganization[],
        public relavantDataProviders?: DataProvider[],
        public linkCategory?: any,
        public linkTitle?: any
    ) {

    }
}
