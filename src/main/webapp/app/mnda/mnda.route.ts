import { Route } from '@angular/router';
import { MndaComponent } from './mnda.component';
import {UserRouteAccessService } from 'app/shared';

export const MNDA_ROUTE: Route = {
	path: 'mndatrial',
	component: MndaComponent,
	data: {
		authorities: ['ROLE_PROVIDER'],
		pageTitle: 'Mutual Non-Disclosure & Trial Agreement',
		breadcrumb: 'MNDA & Trial',
		type: 'static'
	},
	canActivate: [UserRouteAccessService]
};
