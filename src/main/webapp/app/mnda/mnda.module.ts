import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MNDA_ROUTE } from './mnda.route';
import { MndaComponent } from './mnda.component';


@NgModule({
	imports: [
		RouterModule.forRoot([ MNDA_ROUTE ], { useHash: true })
	],
	declarations: [
		MndaComponent,
	],
	entryComponents: [
	],
	providers: [

	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceMndaModule {}
