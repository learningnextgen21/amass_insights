import { Component, OnInit, Inject } from '@angular/core';
import {WINDOW} from '../layouts/main/window.service';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';
@Component({
	selector: 'jhi-mndatrial',
	templateUrl: './mnda.component.html',
	styleUrls: [
		'mnda.scss',
		'../../content/scss/amass-form.scss'
	]

})
export class MndaComponent implements OnInit {
    constructor(
        @Inject(WINDOW) private window: Window,
        public googleAnalyticsService: GoogleAnalyticsEventsService
	) {
	}

	ngOnInit() {

	}

}
