import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DatamarketplaceSharedModule } from '../shared';
import { HOWWORK_ROUTE} from './howworks.route';
import { HowWorksComponent } from './howworks.component';

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot([ HOWWORK_ROUTE ], { useHash: true })
    ],
    declarations: [ HowWorksComponent ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap:    [ HowWorksComponent ]
})
export class DataMarketplaceWorksModule { }
