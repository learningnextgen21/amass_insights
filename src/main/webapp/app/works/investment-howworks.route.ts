import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { InvestmentHowWorksComponent } from './investment-howworks.component';

export const INVESTMENT_ROUTE: Route = {
	path: 'how-it-works-investors',
	component: InvestmentHowWorksComponent,
	data: {
		authorities: ['ROLE_INVESTOR'],
		pageTitle: 'home.title',
		breadcrumb: 'How It Works',
		type: 'static'
	}
};
