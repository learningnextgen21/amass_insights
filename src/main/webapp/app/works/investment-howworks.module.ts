import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { RouterModule } from '@angular/router';
import { DatamarketplaceSharedModule } from '../shared';
import { INVESTMENT_ROUTE} from './investment-howworks.route';
import { InvestmentHowWorksComponent } from './investment-howworks.component';

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot([ INVESTMENT_ROUTE ], { useHash: true })
    ],
    declarations: [ InvestmentHowWorksComponent ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap:    [ InvestmentHowWorksComponent ]
})
export class DataMarketplaceInvestmentWorksModule { }
