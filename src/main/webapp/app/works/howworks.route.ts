import { Route } from '@angular/router';

import { CanDeactivateGuard, UserRouteAccessService } from 'app/shared';
import { HowWorksComponent } from './howworks.component';

export const HOWWORK_ROUTE: Route = {
	path: 'how-it-works-providers',
	component: HowWorksComponent,
	data: {
		authorities: ['ROLE_PROVIDER'],
		pageTitle: 'home.title',
		breadcrumb: 'How It Works',
		type: 'static'
	},
	canActivate: [UserRouteAccessService]

};
