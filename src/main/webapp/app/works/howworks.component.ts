import { Component, OnInit, Inject } from '@angular/core';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';
import {WINDOW} from '../layouts/main/window.service';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApplyAccountComponent } from '../account/apply/apply.component';
import { ActivatedRoute, Router } from '@angular/router';
import {Principal} from '../shared';
import { AmassSettingService } from '../account/settings/settings.service';
import { AuthServerProvider,StateStorageService,SigninModalService } from '../shared';
@Component({
    selector: 'jhi-about',
    templateUrl: 'howworks.component.html',
    styleUrls: [
        './howworks.scss'
    ]
})
export class HowWorksComponent implements OnInit {
    modalRef: NgbModalRef;
    mdDialogRef: MatDialogRef<any>;
    isAdmin: boolean;
    isProvider: boolean = false;
    providerId: any;
    pageName = 'list';

    constructor(
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        @Inject(WINDOW) private window: Window,
        public dialog: MatDialog,
        private router: Router,
		private principal: Principal,
        private amassSettingService: AmassSettingService,
        private stateService: StateStorageService,
        private authService: AuthServerProvider,
		private signinModalService: SigninModalService,
        public route: ActivatedRoute
        ) {

        }
    ngOnInit() {
        this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN', 'ROLE_USER']);
        this.isProvider = this.principal.hasAnyAuthorityDirect(['ROLE_PROVIDER', 'ROLE_USER']);
        this.amassSettingService.setAuthorisedUrl().subscribe(data => {
            this.providerId = data[0].providerRecordID;
            console.log(this.providerId);
        });
    }
    contactUs(events) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    signup(events: any) {
        // console.log(events);
        if (!this.isAdmin && !this.isAuthenticated()) {
            this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            });
            this.mdDialogRef.componentInstance.companyName = 'ROLE_PROVIDER';
            this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        }
    }
    providers(events) {
        // console.log(events);
        if (this.isAuthenticated()) {
            this.router.navigate(['/providers']);
        }
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    providerProfile(events) {
        // if (this.isAdmin && this.providerId) {
        //     this.router.navigate(['/providers', this.providerId]);
        // }
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    navigateRelatedPage() {
        if (this.isAuthenticated() && this.providerId && this.isProvider) {
            this.router.navigate(['/providers', this.providerId, 'edit']);
        }
    }
    communityRelatedPage() {
        if (this.isAuthenticated() && this.isProvider) {
            this.router.navigate(['/investors']);
        }
    }
    isAuthenticated() {
		return this.principal.isAuthenticated();
	}
    onExpireSession(event) {
		//console.log('eventcheck',event);
        if (event) {
			//console.log('eventcheck',event);
           // this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
				setTimeout(() => {
				this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
			}, 2000);
			});
        }
    }
	  beforeSessionExpires(event) {
        if (event) {
           // this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }
}
