import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { AmassUser } from './amass-user.model';
import { AmassUserService } from './amass-user.service';

@Injectable()
export class AmassUserPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private amassUserService: AmassUserService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.amassUserService.find(id).subscribe((amassUser) => {
                    if (amassUser.createdDate) {
                        amassUser.createdDate = {
                            year: amassUser.createdDate.getFullYear(),
                            month: amassUser.createdDate.getMonth() + 1,
                            day: amassUser.createdDate.getDate()
                        };
                    }
                    amassUser.updatedDate = this.datePipe
                        .transform(amassUser.updatedDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.amassUserModalRef(component, amassUser);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.amassUserModalRef(component, new AmassUser());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    amassUserModalRef(component: Component, amassUser: AmassUser): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.amassUser = amassUser;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
