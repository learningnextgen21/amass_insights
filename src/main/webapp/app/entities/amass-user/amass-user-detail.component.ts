import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { AmassUser } from './amass-user.model';
import { AmassUserService } from './amass-user.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-amass-user-detail',
    templateUrl: './amass-user-detail.component.html'
})
export class AmassUserDetailComponent implements OnInit, OnDestroy {

    amassUser: AmassUser;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private amassUserService: AmassUserService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAmassUsers();
    }

    load(id) {
        this.amassUserService.find(id).subscribe((amassUser) => {
            this.amassUser = amassUser;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAmassUsers() {
        this.eventSubscriber = this.eventManager.subscribe(
            'amassUserListModification',
            (response) => this.load(this.amassUser.id)
        );
    }
}
