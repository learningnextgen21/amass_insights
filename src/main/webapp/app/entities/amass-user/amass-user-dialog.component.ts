import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { AmassUser } from './amass-user.model';
import { AmassUserPopupService } from './amass-user-popup.service';
import { AmassUserService } from './amass-user.service';

@Component({
    selector: 'jhi-amass-user-dialog',
    templateUrl: './amass-user-dialog.component.html'
})
export class AmassUserDialogComponent implements OnInit {

    amassUser: AmassUser;
    isSaving: boolean;
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private amassUserService: AmassUserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.amassUser.id !== undefined) {
            this.subscribeToSaveResponse(
                this.amassUserService.update(this.amassUser));
        } else {
            this.subscribeToSaveResponse(
                this.amassUserService.create(this.amassUser));
        }
    }

    private subscribeToSaveResponse(result: Observable<AmassUser>) {
        result.subscribe((res: AmassUser) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: AmassUser) {
        this.eventManager.broadcast({ name: 'amassUserListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-amass-user-popup',
    template: ''
})
export class AmassUserPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private amassUserPopupService: AmassUserPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.amassUserPopupService
                    .open(AmassUserDialogComponent as Component, params['id']);
            } else {
                this.amassUserPopupService
                    .open(AmassUserDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
