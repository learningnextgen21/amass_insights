import { BaseEntity } from './../../shared';

export class AmassUser implements BaseEntity {
    constructor(
        public id?: number,
        public accountName?: string,
        public fullName?: string,
        public email?: string,
        public password?: string,
        public phone?: string,
        public securityQuestions?: any,
        public optedIn?: number,
        public company?: string,
        public website?: string,
        public location?: string,
        public userType?: number,
        public inactive?: number,
        public createdDate?: any,
        public updatedDate?: any,
    ) {
    }
}
