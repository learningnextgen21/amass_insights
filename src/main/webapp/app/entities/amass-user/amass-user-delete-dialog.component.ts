import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AmassUser } from './amass-user.model';
import { AmassUserPopupService } from './amass-user-popup.service';
import { AmassUserService } from './amass-user.service';

@Component({
    selector: 'jhi-amass-user-delete-dialog',
    templateUrl: './amass-user-delete-dialog.component.html'
})
export class AmassUserDeleteDialogComponent {

    amassUser: AmassUser;

    constructor(
        private amassUserService: AmassUserService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.amassUserService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'amassUserListModification',
                content: 'Deleted an amassUser'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-amass-user-delete-popup',
    template: ''
})
export class AmassUserDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private amassUserPopupService: AmassUserPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.amassUserPopupService
                .open(AmassUserDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
