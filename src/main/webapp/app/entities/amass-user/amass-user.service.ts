
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { AmassUser } from './amass-user.model';
import { createRequestOption } from '../../shared';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AmassUserService {

    private resourceUrl = 'api/amass-users';
    private resourceSearchUrl = 'api/_search/amass-users';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(amassUser: AmassUser): Observable<AmassUser> {
        const copy = this.convert(amassUser);
        return this.http.post(this.resourceUrl, copy);
    }

    update(amassUser: AmassUser): Observable<AmassUser> {
        const copy = this.convert(amassUser);
        return this.http.put(this.resourceUrl, copy);
    }

    find(id: number): Observable<AmassUser> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, {params: options});
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, {params: options});
    }

    private convert(amassUser: AmassUser): AmassUser {
        const copy: AmassUser = Object.assign({}, amassUser);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(amassUser.createdDate);

        copy.updatedDate = this.dateUtils.toDate(amassUser.updatedDate);
        return copy;
    }
}
