import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AmassUserComponent } from './amass-user.component';
import { AmassUserDetailComponent } from './amass-user-detail.component';
import { AmassUserPopupComponent } from './amass-user-dialog.component';
import { AmassUserDeletePopupComponent } from './amass-user-delete-dialog.component';

export const amassUserRoute: Routes = [
    {
        path: 'amass-user',
        component: AmassUserComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.amassUser.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'amass-user/:id',
        component: AmassUserDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.amassUser.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const amassUserPopupRoute: Routes = [
    {
        path: 'amass-user-new',
        component: AmassUserPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.amassUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'amass-user/:id/edit',
        component: AmassUserPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.amassUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'amass-user/:id/delete',
        component: AmassUserDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.amassUser.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
