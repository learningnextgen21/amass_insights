import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    AmassUserService,
    AmassUserPopupService,
    AmassUserComponent,
    AmassUserDetailComponent,
    AmassUserDialogComponent,
    AmassUserPopupComponent,
    AmassUserDeletePopupComponent,
    AmassUserDeleteDialogComponent,
    amassUserRoute,
    amassUserPopupRoute,
} from './';

const ENTITY_STATES = [
    ...amassUserRoute,
    ...amassUserPopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AmassUserComponent,
        AmassUserDetailComponent,
        AmassUserDialogComponent,
        AmassUserDeleteDialogComponent,
        AmassUserPopupComponent,
        AmassUserDeletePopupComponent,
    ],
    entryComponents: [
        AmassUserComponent,
        AmassUserDialogComponent,
        AmassUserPopupComponent,
        AmassUserDeleteDialogComponent,
        AmassUserDeletePopupComponent,
    ],
    providers: [
        AmassUserService,
        AmassUserPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceAmassUserModule {}
