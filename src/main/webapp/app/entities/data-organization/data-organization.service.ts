

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { DataOrganization } from './data-organization.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class DataOrganizationService {

    private resourceUrl = 'api/data-organizations';
    private resourceSearchUrl = 'api/_search/data-organizations';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(dataOrganization: DataOrganization): Observable<DataOrganization> {
        const copy = this.convert(dataOrganization);
        return this.http.post(this.resourceUrl, copy);
    }

    update(dataOrganization: DataOrganization): Observable<DataOrganization> {
        const copy = this.convert(dataOrganization);
        return this.http.put(this.resourceUrl, copy);
    }

    find(id: number): Observable<DataOrganization> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, {params: options});
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, {params: options, observe: 'response'});
    }

    private convert(dataOrganization: DataOrganization): DataOrganization {
        const copy: DataOrganization = Object.assign({}, dataOrganization);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(dataOrganization.createdDate);

        copy.updateddate = this.dateUtils.toDate(dataOrganization.updateddate);
        return copy;
    }
}
