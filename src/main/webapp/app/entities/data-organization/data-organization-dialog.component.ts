import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { DataOrganization } from './data-organization.model';
import { DataOrganizationPopupService } from './data-organization-popup.service';
import { DataOrganizationService } from './data-organization.service';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-data-organization-dialog',
    templateUrl: './data-organization-dialog.component.html'
})
export class DataOrganizationDialogComponent implements OnInit {

    dataOrganization: DataOrganization;
    isSaving: boolean;

    dataorganizations: DataOrganization[];
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private dataOrganizationService: DataOrganizationService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.dataOrganizationService.query()
            .subscribe((res) => { this.dataorganizations = res; }, (res) => this.onError(res));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dataOrganization.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dataOrganizationService.update(this.dataOrganization));
        } else {
            this.subscribeToSaveResponse(
                this.dataOrganizationService.create(this.dataOrganization));
        }
    }

    private subscribeToSaveResponse(result: Observable<DataOrganization>) {
        result.subscribe((res: DataOrganization) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DataOrganization) {
        this.eventManager.broadcast({ name: 'dataOrganizationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackDataOrganizationById(index: number, item: DataOrganization) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-data-organization-popup',
    template: ''
})
export class DataOrganizationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataOrganizationPopupService: DataOrganizationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dataOrganizationPopupService
                    .open(DataOrganizationDialogComponent as Component, params['id']);
            } else {
                this.dataOrganizationPopupService
                    .open(DataOrganizationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
