import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    DataOrganizationService,
    DataOrganizationPopupService,
    DataOrganizationComponent,
    DataOrganizationDetailComponent,
    DataOrganizationDialogComponent,
    DataOrganizationPopupComponent,
    DataOrganizationDeletePopupComponent,
    DataOrganizationDeleteDialogComponent,
    dataOrganizationRoute,
    dataOrganizationPopupRoute,
} from './';

const ENTITY_STATES = [
    ...dataOrganizationRoute,
    ...dataOrganizationPopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DataOrganizationComponent,
        DataOrganizationDetailComponent,
        DataOrganizationDialogComponent,
        DataOrganizationDeleteDialogComponent,
        DataOrganizationPopupComponent,
        DataOrganizationDeletePopupComponent,
    ],
    entryComponents: [
        DataOrganizationComponent,
        DataOrganizationDialogComponent,
        DataOrganizationPopupComponent,
        DataOrganizationDeleteDialogComponent,
        DataOrganizationDeletePopupComponent,
    ],
    providers: [
        DataOrganizationService,
        DataOrganizationPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceDataOrganizationModule {}
