import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DataOrganization } from './data-organization.model';
import { DataOrganizationPopupService } from './data-organization-popup.service';
import { DataOrganizationService } from './data-organization.service';

@Component({
    selector: 'jhi-data-organization-delete-dialog',
    templateUrl: './data-organization-delete-dialog.component.html'
})
export class DataOrganizationDeleteDialogComponent {

    dataOrganization: DataOrganization;

    constructor(
        private dataOrganizationService: DataOrganizationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dataOrganizationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dataOrganizationListModification',
                content: 'Deleted an dataOrganization'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-data-organization-delete-popup',
    template: ''
})
export class DataOrganizationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataOrganizationPopupService: DataOrganizationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dataOrganizationPopupService
                .open(DataOrganizationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
