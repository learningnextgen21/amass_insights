import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DataOrganization } from './data-organization.model';
import { DataOrganizationService } from './data-organization.service';

@Injectable()
export class DataOrganizationPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private dataOrganizationService: DataOrganizationService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dataOrganizationService.find(id).subscribe((dataOrganization) => {
                    if (dataOrganization.createdDate) {
                        dataOrganization.createdDate = {
                            year: dataOrganization.createdDate.getFullYear(),
                            month: dataOrganization.createdDate.getMonth() + 1,
                            day: dataOrganization.createdDate.getDate()
                        };
                    }
                    dataOrganization.updateddate = this.datePipe
                        .transform(dataOrganization.updateddate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.dataOrganizationModalRef(component, dataOrganization);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dataOrganizationModalRef(component, new DataOrganization());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dataOrganizationModalRef(component: Component, dataOrganization: DataOrganization): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dataOrganization = dataOrganization;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
