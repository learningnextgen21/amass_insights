import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { DataOrganization } from './data-organization.model';
import { DataOrganizationService } from './data-organization.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-data-organization-detail',
    templateUrl: './data-organization-detail.component.html'
})
export class DataOrganizationDetailComponent implements OnInit, OnDestroy {

    dataOrganization: DataOrganization;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private dataOrganizationService: DataOrganizationService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDataOrganizations();
    }

    load(id) {
        this.dataOrganizationService.find(id).subscribe((dataOrganization) => {
            this.dataOrganization = dataOrganization;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDataOrganizations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dataOrganizationListModification',
            (response) => this.load(this.dataOrganization.id)
        );
    }
}
