import { BaseEntity } from './../../shared';

export class DataOrganization implements BaseEntity {
    constructor(
        public id?: number,
        public recordID?: string,
        public name?: string,
        public website?: string,
        public description?: any,
        public city?: string,
        public state?: string,
        public country?: string,
        public zipCode?: string,
        public facebook?: string,
        public github?: string,
        public twitter?: string,
        public blog?: string,
        public blogStatus?: string,
        public yearFounded?: number,
        public headCount?: string,
        public ceo?: string,
        public financialPosition?: string,
        public clientBase?: string,
        public mnda?: boolean,
        public notes?: any,
        public inactive?: number,
        public live?: number,
        public createdDate?: any,
        public updateddate?: any,
        public parentOrganization?: BaseEntity,
        public dataArticles?: BaseEntity[],
        public owners?: BaseEntity[],
        public dataOrganizations?: BaseEntity[],
        public yearExit?: any,
        public headcountBucket?: any,
        public headcountNumber?: any,
        public ownerOrganization?: any,
        public providerName?: any

    ) {
        this.mnda = false;
    }
}
