import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DataOrganizationComponent } from './data-organization.component';
import { DataOrganizationDetailComponent } from './data-organization-detail.component';
import { DataOrganizationPopupComponent } from './data-organization-dialog.component';
import { DataOrganizationDeletePopupComponent } from './data-organization-delete-dialog.component';

export const dataOrganizationRoute: Routes = [
    {
        path: 'data-organization',
        component: DataOrganizationComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataOrganization.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'data-organization/:id',
        component: DataOrganizationDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataOrganization.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dataOrganizationPopupRoute: Routes = [
    {
        path: 'data-organization-new',
        component: DataOrganizationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataOrganization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-organization/:id/edit',
        component: DataOrganizationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataOrganization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-organization/:id/delete',
        component: DataOrganizationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataOrganization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
