import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { LookupCode } from './lookup-code.model';
import { LookupCodePopupService } from './lookup-code-popup.service';
import { LookupCodeService } from './lookup-code.service';
import { DataProvider, DataProviderService } from '../data-provider';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-lookup-code-dialog',
    templateUrl: './lookup-code-dialog.component.html'
})
export class LookupCodeDialogComponent implements OnInit {

    lookupCode: LookupCode;
    isSaving: boolean;

    dataproviders: DataProvider[];

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private lookupCodeService: LookupCodeService,
        private dataProviderService: DataProviderService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.dataProviderService.query()
            .subscribe((res) => { this.dataproviders = res.body; }, (res) => this.onError(res));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.lookupCode.id !== undefined) {
            this.subscribeToSaveResponse(
                this.lookupCodeService.update(this.lookupCode));
        } else {
            this.subscribeToSaveResponse(
                this.lookupCodeService.create(this.lookupCode));
        }
    }

    private subscribeToSaveResponse(result: Observable<LookupCode>) {
        result.subscribe((res: LookupCode) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: LookupCode) {
        this.eventManager.broadcast({ name: 'lookupCodeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackDataProviderById(index: number, item: DataProvider) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-lookup-code-popup',
    template: ''
})
export class LookupCodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private lookupCodePopupService: LookupCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.lookupCodePopupService
                    .open(LookupCodeDialogComponent as Component, params['id']);
            } else {
                this.lookupCodePopupService
                    .open(LookupCodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
