import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { LookupCode } from './lookup-code.model';
import { LookupCodeService } from './lookup-code.service';

@Injectable()
export class LookupCodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private lookupCodeService: LookupCodeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.lookupCodeService.find(id).subscribe((lookupCode) => {
                    lookupCode.createdon = this.datePipe
                        .transform(lookupCode.createdon, 'yyyy-MM-ddTHH:mm:ss');
                    lookupCode.lastupdate = this.datePipe
                        .transform(lookupCode.lastupdate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.lookupCodeModalRef(component, lookupCode);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.lookupCodeModalRef(component, new LookupCode());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    lookupCodeModalRef(component: Component, lookupCode: LookupCode): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.lookupCode = lookupCode;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
