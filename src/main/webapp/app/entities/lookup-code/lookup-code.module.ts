import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    LookupCodeService,
    LookupCodePopupService,
    LookupCodeComponent,
    LookupCodeDetailComponent,
    LookupCodeDialogComponent,
    LookupCodePopupComponent,
    LookupCodeDeletePopupComponent,
    LookupCodeDeleteDialogComponent,
    lookupCodeRoute,
    lookupCodePopupRoute,
} from './';

const ENTITY_STATES = [
    ...lookupCodeRoute,
    ...lookupCodePopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        LookupCodeComponent,
        LookupCodeDetailComponent,
        LookupCodeDialogComponent,
        LookupCodeDeleteDialogComponent,
        LookupCodePopupComponent,
        LookupCodeDeletePopupComponent,
    ],
    entryComponents: [
        LookupCodeComponent,
        LookupCodeDialogComponent,
        LookupCodePopupComponent,
        LookupCodeDeleteDialogComponent,
        LookupCodeDeletePopupComponent,
    ],
    providers: [
        LookupCodeService,
        LookupCodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceLookupCodeModule {}
