import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LookupCode } from './lookup-code.model';
import { LookupCodePopupService } from './lookup-code-popup.service';
import { LookupCodeService } from './lookup-code.service';

@Component({
    selector: 'jhi-lookup-code-delete-dialog',
    templateUrl: './lookup-code-delete-dialog.component.html'
})
export class LookupCodeDeleteDialogComponent {

    lookupCode: LookupCode;

    constructor(
        private lookupCodeService: LookupCodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.lookupCodeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'lookupCodeListModification',
                content: 'Deleted an lookupCode'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-lookup-code-delete-popup',
    template: ''
})
export class LookupCodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private lookupCodePopupService: LookupCodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.lookupCodePopupService
                .open(LookupCodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
