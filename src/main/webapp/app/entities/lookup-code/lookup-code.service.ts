import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { LookupCode } from './lookup-code.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class LookupCodeService {

    private resourceUrl = 'api/lookup-codes';
    private resourceSearchUrl = 'api/_search/lookup-codes';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(lookupCode: LookupCode): Observable<any> {
        const copy = this.convert(lookupCode);
        return this.http.post(this.resourceUrl, copy, { observe: 'response' });
    }

    update(lookupCode: LookupCode): Observable<any> {
        const copy = this.convert(lookupCode);
        return this.http.put(this.resourceUrl, copy, { observe: 'response' });
    }

    find(id: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    private convert(lookupCode: LookupCode): LookupCode {
        const copy: LookupCode = Object.assign({}, lookupCode);

        copy.createdon = this.dateUtils.toDate(lookupCode.createdon);

        copy.lastupdate = this.dateUtils.toDate(lookupCode.lastupdate);
        return copy;
    }
}
