import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { LookupCode } from './lookup-code.model';
import { LookupCodeService } from './lookup-code.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-lookup-code-detail',
    templateUrl: './lookup-code-detail.component.html'
})
export class LookupCodeDetailComponent implements OnInit, OnDestroy {

    lookupCode: LookupCode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private lookupCodeService: LookupCodeService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLookupCodes();
    }

    load(id) {
        this.lookupCodeService.find(id).subscribe((lookupCode) => {
            this.lookupCode = lookupCode;
        });
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLookupCodes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'lookupCodeListModification',
            (response) => this.load(this.lookupCode.id)
        );
    }
}
