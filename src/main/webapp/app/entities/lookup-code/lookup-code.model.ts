import { BaseEntity } from './../../shared';

export class LookupCode implements BaseEntity {
    constructor(
        public id?: number,
        public lookupmodule?: string,
        public lookupcode?: string,
        public description?: any,
        public sortorder?: number,
        public lastupdatedby?: number,
        public createdon?: any,
        public lastupdate?: any,
        public createdby?: number,
        public providerGeographicalFoci?: BaseEntity[],
        public providerAssetClasses?: BaseEntity[],
        public providerInvestorTypes?: BaseEntity[],
        public providerSecurityTypes?: BaseEntity[],
        public providerDeliveryMethods?: BaseEntity[],
        public providerDeliveryFormats?: BaseEntity[],
        public providerDeliveryFrequencies?: BaseEntity[],
        public providerPricingModels?: BaseEntity[],
        public providerSectors?: BaseEntity[],
    ) {
    }
}
