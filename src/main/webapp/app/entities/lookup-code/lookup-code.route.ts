import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { LookupCodeComponent } from './lookup-code.component';
import { LookupCodeDetailComponent } from './lookup-code-detail.component';
import { LookupCodePopupComponent } from './lookup-code-dialog.component';
import { LookupCodeDeletePopupComponent } from './lookup-code-delete-dialog.component';

export const lookupCodeRoute: Routes = [
    {
        path: 'lookup-code',
        component: LookupCodeComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.lookupCode.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'lookup-code/:id',
        component: LookupCodeDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.lookupCode.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const lookupCodePopupRoute: Routes = [
    {
        path: 'lookup-code-new',
        component: LookupCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.lookupCode.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'lookup-code/:id/edit',
        component: LookupCodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.lookupCode.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'lookup-code/:id/delete',
        component: LookupCodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.lookupCode.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
