import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { DataFeature } from './data-feature.model';
import { DataFeaturePopupService } from './data-feature-popup.service';
import { DataFeatureService } from './data-feature.service';
import { DataCategory, DataCategoryService } from '../data-category';
import { DataProvider, DataProviderService } from '../data-provider';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-data-feature-dialog',
    templateUrl: './data-feature-dialog.component.html'
})
export class DataFeatureDialogComponent implements OnInit {

    dataFeature: DataFeature;
    isSaving: boolean;

    datacategories: DataCategory[];

    dataproviders: DataProvider[];
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private dataFeatureService: DataFeatureService,
        private dataCategoryService: DataCategoryService,
        private dataProviderService: DataProviderService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.dataCategoryService.query()
            .subscribe((res) => { this.datacategories = res; }, (res) => this.onError(res));
        this.dataProviderService.query()
            .subscribe((res) => { this.dataproviders = res.body; }, (res) => this.onError(res));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dataFeature.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dataFeatureService.update(this.dataFeature));
        } else {
            this.subscribeToSaveResponse(
                this.dataFeatureService.create(this.dataFeature));
        }
    }

    private subscribeToSaveResponse(result: Observable<DataFeature>) {
        result.subscribe((res: DataFeature) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DataFeature) {
        this.eventManager.broadcast({ name: 'dataFeatureListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackDataCategoryById(index: number, item: DataCategory) {
        return item.id;
    }

    trackDataProviderById(index: number, item: DataProvider) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-data-feature-popup',
    template: ''
})
export class DataFeaturePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataFeaturePopupService: DataFeaturePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dataFeaturePopupService
                    .open(DataFeatureDialogComponent as Component, params['id']);
            } else {
                this.dataFeaturePopupService
                    .open(DataFeatureDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
