import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DataFeature } from './data-feature.model';
import { DataFeatureService } from './data-feature.service';

@Injectable()
export class DataFeaturePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private dataFeatureService: DataFeatureService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dataFeatureService.find(id).subscribe((dataFeature) => {
                    if (dataFeature.createdDate) {
                        dataFeature.createdDate = {
                            year: dataFeature.createdDate.getFullYear(),
                            month: dataFeature.createdDate.getMonth() + 1,
                            day: dataFeature.createdDate.getDate()
                        };
                    }
                    dataFeature.updatedDate = this.datePipe
                        .transform(dataFeature.updatedDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.dataFeatureModalRef(component, dataFeature);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dataFeatureModalRef(component, new DataFeature());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dataFeatureModalRef(component: Component, dataFeature: DataFeature): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dataFeature = dataFeature;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
