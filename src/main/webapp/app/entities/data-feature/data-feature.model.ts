import { BaseEntity } from './../../shared';

export class DataFeature implements BaseEntity {
    constructor(
        public id?: number,
        public recordID?: string,
        public dataFeature?: string,
        public explanation?: any,
        public inactive?: number,
        public createdDate?: any,
        public updatedDate?: any,
        public categories?: BaseEntity[],
        public featureProviders?: BaseEntity[],
    ) {
    }
}
