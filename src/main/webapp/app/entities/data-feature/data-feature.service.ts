

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { DataFeature } from './data-feature.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataFeatureService {

    private resourceUrl = 'api/data-features';
    private resourceSearchUrl = 'api/_search/data-features';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(dataFeature: DataFeature): Observable<DataFeature> {
        const copy = this.convert(dataFeature);
        return this.http.post(this.resourceUrl, copy);
    }

    update(dataFeature: DataFeature): Observable<DataFeature> {
        const copy = this.convert(dataFeature);
        return this.http.put(this.resourceUrl, copy);
    }

    find(id: number): Observable<DataFeature> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, {params: options});
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, {params: options});
    }

    private convert(dataFeature: DataFeature): DataFeature {
        const copy: DataFeature = Object.assign({}, dataFeature);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(dataFeature.createdDate);

        copy.updatedDate = this.dateUtils.toDate(dataFeature.updatedDate);
        return copy;
    }
}
