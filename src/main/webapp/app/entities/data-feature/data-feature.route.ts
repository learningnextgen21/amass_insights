import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DataFeatureComponent } from './data-feature.component';
import { DataFeatureDetailComponent } from './data-feature-detail.component';
import { DataFeaturePopupComponent } from './data-feature-dialog.component';
import { DataFeatureDeletePopupComponent } from './data-feature-delete-dialog.component';

export const dataFeatureRoute: Routes = [
    {
        path: 'data-feature',
        component: DataFeatureComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataFeature.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'data-feature/:id',
        component: DataFeatureDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataFeature.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dataFeaturePopupRoute: Routes = [
    {
        path: 'data-feature-new',
        component: DataFeaturePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataFeature.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-feature/:id/edit',
        component: DataFeaturePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataFeature.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-feature/:id/delete',
        component: DataFeatureDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataFeature.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
