import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DataFeature } from './data-feature.model';
import { DataFeaturePopupService } from './data-feature-popup.service';
import { DataFeatureService } from './data-feature.service';

@Component({
    selector: 'jhi-data-feature-delete-dialog',
    templateUrl: './data-feature-delete-dialog.component.html'
})
export class DataFeatureDeleteDialogComponent {

    dataFeature: DataFeature;

    constructor(
        private dataFeatureService: DataFeatureService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dataFeatureService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dataFeatureListModification',
                content: 'Deleted an dataFeature'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-data-feature-delete-popup',
    template: ''
})
export class DataFeatureDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataFeaturePopupService: DataFeaturePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dataFeaturePopupService
                .open(DataFeatureDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
