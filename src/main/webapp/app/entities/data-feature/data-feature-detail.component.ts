import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { DataFeature } from './data-feature.model';
import { DataFeatureService } from './data-feature.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-data-feature-detail',
    templateUrl: './data-feature-detail.component.html'
})
export class DataFeatureDetailComponent implements OnInit, OnDestroy {

    dataFeature: DataFeature;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private dataFeatureService: DataFeatureService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDataFeatures();
    }

    load(id) {
        this.dataFeatureService.find(id).subscribe((dataFeature) => {
            this.dataFeature = dataFeature;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDataFeatures() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dataFeatureListModification',
            (response) => this.load(this.dataFeature.id)
        );
    }
}
