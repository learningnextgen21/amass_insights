import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    DataFeatureService,
    DataFeaturePopupService,
    DataFeatureComponent,
    DataFeatureDetailComponent,
    DataFeatureDialogComponent,
    DataFeaturePopupComponent,
    DataFeatureDeletePopupComponent,
    DataFeatureDeleteDialogComponent,
    dataFeatureRoute,
    dataFeaturePopupRoute,
} from './';

const ENTITY_STATES = [
    ...dataFeatureRoute,
    ...dataFeaturePopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DataFeatureComponent,
        DataFeatureDetailComponent,
        DataFeatureDialogComponent,
        DataFeatureDeleteDialogComponent,
        DataFeaturePopupComponent,
        DataFeatureDeletePopupComponent,
    ],
    entryComponents: [
        DataFeatureComponent,
        DataFeatureDialogComponent,
        DataFeaturePopupComponent,
        DataFeatureDeleteDialogComponent,
        DataFeatureDeletePopupComponent,
    ],
    providers: [
        DataFeatureService,
        DataFeaturePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceDataFeatureModule {}
