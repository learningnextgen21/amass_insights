import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DataProvider } from './data-provider.model';
import { DataProviderPopupService } from './data-provider-popup.service';
import { DataProviderService } from './data-provider.service';

@Component({
    selector: 'jhi-data-provider-delete-dialog',
    templateUrl: './data-provider-delete-dialog.component.html'
})
export class DataProviderDeleteDialogComponent {

    dataProvider: DataProvider;

    constructor(
        private dataProviderService: DataProviderService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dataProviderService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dataProviderListModification',
                content: 'Deleted an dataProvider'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-data-provider-delete-popup',
    template: ''
})
export class DataProviderDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataProviderPopupService: DataProviderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dataProviderPopupService
                .open(DataProviderDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
