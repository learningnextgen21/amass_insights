
import {forkJoin, of as observableOf, Subject} from 'rxjs';
import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, Inject, Injectable, Input, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';
import { ActivatedRoute, Router,Scroll, RouterStateSnapshot,Event,NavigationEnd } from '@angular/router';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { DomSanitizer } from '@angular/platform-browser';
import { DataProvider } from './data-provider.model';
import { DataProviderService } from './data-provider.service';
import { ContactDialogComponent } from '../../shared/contactdialog/contact-dialog.component';
import { Principal } from '../../shared/auth/principal.service';
import { DataSource } from '@angular/cdk/table';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { LoaderService } from '../../loader/loaders.service';
import { NestedTreeControl, FlatTreeControl } from '@angular/cdk/tree';
import { WINDOW } from 'app/layouts';
import { SigninModalService, AmassFilterService, CSRFService, AuthServerProvider, StateStorageService, AccountService } from 'app/shared';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig, FormlyTemplateOptions } from '@ngx-formly/core';
import { ConfirmDialogComponent } from 'app/shared/dialogs/confirm-dialog.component';
import { UppyService, UppyComponent } from 'app/uppy';
import { contains } from 'ramda';
let _that = null;
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie';
import { DataOrganizationAdminFormComponent } from './data-organization-admin-form.component';
import { AddResourceAdminFormComponent } from '../../add-resource-admin/add-resource-admin-form.component';
import { DataResource } from 'app/add-resource-admin/add-resource-admin-form.model';
import { AddResourceAdminService } from 'app/add-resource-admin/add-resource-admin.service';
import { filter } from 'rxjs/operators';
import {  ViewportScroller } from '@angular/common';
import { SigninDialogComponent } from 'app/dialog/signindialog.component';
import { environmentDev } from 'app/environments/environment.dev';

@Component({
	selector: 'jhi-data-provider-admin-form',
	templateUrl: './data-provider-admin-form.component.html',
	styleUrls: [ '../../../content/css/material-tab.css']
})
export class DataProviderAdminFormComponent implements OnInit, AfterViewInit {
	dialogRef: MatDialogRef<ConfirmDialogComponent>;
	orgDialogRef: MatDialogRef<any>;
	editOrgDialogRef: MatDialogRef<any>;
	addOrgDialogRef: MatDialogRef<any>;
	resourceDialogRef: MatDialogRef<AddResourceAdminFormComponent>;
    formSubmitted: boolean = false;
    routerOutletRedirect: string = '';

    dataProvider: DataProvider = {};
	providerID: number = 0;
	dataProviderId: number = 0;
	providerRecordID: string = '';
	dataProviderName: string = '';
    dataProviderPrivacy: string = '';
//	relavantLinks: any;
	resourceObj: any;
	providerLogo: string = '';
	disableTable: boolean = false;
	resources: DataResource[] = [];
//	relevantProviders: DataProvider[] = [];
	itemRelevantProviders: DataResource[] = [];
	itemObject: {};
	deleteLinkObj: {};
	itemID: number = 0;
	resourceLink: DataResource[] = [];
	dataResources: any[] = [];
//	addedLinks: any;
	addRelevantLinks: DataResource[] = [];
	dataRelevantLinks: DataResource[] = [];
	dataRelevantFiles: DataResource[] = [];
	selectedValue: any[] = [];
	private subscription: Subscription;
	private eventSubscriber: Subscription;
	isLoading: boolean = false;
	isAdmin: boolean = false;
	isPendingUnlock: boolean = false;
	isPendingExpand: boolean = false;
	emptyField: boolean = false;
    forceFormSubmit: boolean = false;
    editedDataByUser: {};
    approvedProviderData: any = {};
    approvedData: DataProvider = {};
//	domSanitizer: any = false;
//	authoredArticles: any;
//	relevantArticles: any;
	tabIndex: number = 0;
	relevantArticlePage: number = 0;
	relevantArticleLinks: {};
//	relevantArticleFrom: number;
//	totalRelevantArticles: number;
	authoredArticlePage: number = 0;
	authoredArticleLinks: {};
//	authoredArticleFrom: number;
//	totalAuthoredArticles: number;
	recordID: string = '';
	organizationID: any;
	orgID: any;
	id: string = '';
	linkID: any;
	profileTabLabel: string = '';
//    ProfileCategory: any;
    trackname: any;
    trackwebsite: any;
    trackownerOrganizationCity: any;
    trackownerOrganizationState: any;
    trackownerOrganizationCountry: any;
    trackownerOrganizationYearfound: number;
    trackownerOrganizationHeadcount: any;
    tracknumberOfAnalystEmployees: any;
    trackInvestor: any;
    trackNumberOfInvestor: any;
    trackSummary: any;
    trackShortDescription: any;
    trackLongDescription: any;
    trackMainDataCategory: any;
    trackDataCollection: any;
    trackNumberofInvestorClientDetails: any;
    trackpublicCompaniesCovered: any;
    trackpublicEquitesCovered: any;
    trackcollectionMethodsExplanation: any;
    tracksampleOrPanelSize: any
    trackDateCollectionRangeExplanation: any;
    trackkeyData: any;
    trackOutOfSampleData: any;
    trackProductDetails: any;
    trackSampleOrPanelBiasis: any
    trackDiscussionCleanlines: any;
    trackDeliveryFrequencyNotes: any;
    trackDataUpdateFrequencyNotes: any;
    trackTrialDuration: any;
    trackTrialPricingDetails: any;
    trackDataLicenseTypeDetails: any;
    trackMaximumYearlyPrice: any;
    trackMinimumYearlyPrice: any;
    trackPricingDetails: any;
    trackUseCasesOrQuestionsAddressed: any;
    trackDiscussionsAnalytics: any;
    trackPotentialDrawbacks: any;
    trackUniqueValueProps: any;
    trackLegalComplianceIssues: any;
    trackDataRoadmap: any;
    trackDataSourcesDetails: any;
    trackNotes: any;
    trackCompetitiveDifferentiators: any;
    trackYearFirstDataProductLaunched:any
    trackdiscussionCleanliness: any;
    trackLagTime: any;

	progressBarColor = 'primary';
	progressBarMode = 'determinate';
	mdTooltipDelay: number;

	dataProviderTypes: any[] = [];
	pTypesArr: any[] = [];
	mainDataIndustriesArr: any[] = [];
	dataIndustries: any[] = [];
	mainDataCategories: any[] = [];
	dataCategories: any[] = [];
    emailAddress: any[] = [];
    emailAddressCount: number = 0;
    users: any[] = [];
    usersCount: number = 0;
    dataFeatures: any[] = [];
	dataOrganizations: any[] = [];
	dataSources: any[] = [];
	geographicalFocus: any[] = [];
	providerDataLicenseType: any[] = [];
	providerPaymentMethodsOffered: any[] = [];
	providerPrivacy: any[] = [];
	providerMarketplaceStatus: any[] = [];
	providerResearchMethods: any[] = [];
	providerIEIStatus: any[] = [];
	providerPriority: any[] = [];
	providerNumberOfDataSources: any[] = [];
	providerTags: any[] = [];
	providerPartners: any[] = [];
	providerDistributionPartner: any[] = [];
	providerConsumerPartner: any[] = [];
	providerCompetitors: any[] = [];
	providerPartnersCount: number = 0;
    distributionPartnersCount: number = 0;
    consumerPartnersCount: number = 0;
    competitorsCount: number = 0;
	relevantSectors: any[] = [];
	deliveryMethods: any[] = [];
	deliveryFormats: any[] = [];
	deliveryFrequencies: any[] = [];
	dataUpdateFrequency: any[] = [];
	mdDialogRef: MatDialogRef<any>;

    // commented to use only local variable.

    // providerAccessOffered: any[] = [];
	// providerDataProductTypes: any[] = [];
	// providerDataTypes: any[] = [];
	// providerDataLanguagesAvailable: any[] = [];
	// providerDataGapsReasons: any[] = [];
	// providerDatasetSize: any[] = [];
	// providerDatasetNumberOfRows: any[] = [];
	// providerOutlierReasons: any[] = [];
	// providerBiases: any[] = [];
	// providerTrialAgreementType: any[] = [];
    // relevantSecurityTypes: any[] = [];
    // providerUserPermission: any[];
    // providerpublicEquitiesCoveredRange: any[] = [];
    // providerOrganizationType: any[] = [];
	// providerManagerType: any[] = [];
	// providerStrategies: any[] = [];
	// providerResearchStyles: any[] = [];
	// providerInvestingTimeFrame: any[] = [];
	// providerEquitiesMarketCap: any[] = [];
	// providerEquitiesStyle: any[] = [];
	// providerIdentifiersAvailable: any[] = [];
	// providerPointInTimeAccuracy: any[] = [];
	// providerIncludesOutliers: any[] = [];
	// providerDataGaps: any[] = [];
	// providerDuplicatesCleaned: any[] = [];
	// providerNormalized: any[] = [];
	// providerSubscriptionModel: any[] = [];
	// providerPricingModel: any[] = [];
	// providerSubscriptionTimePeriod: any[] = [];
	// providerMonthlyPriceRange: any[] = [];
    // providerInvestorClientBucket: any[] = [];
	// providerTrialPricingModel: any[] = [];
    // providerWillingness: any[] = [];
    // providerUniquness: any[] = [];
    // providerReadiness: any[] = [];
	// providerOverall: any[] = [];
	// providerSecRegulated: any[] = [];
    // providerValue: any[] = [];
	// providerRelevantInvestors: any[] = [];
	// providerMainAssetClass: any[] = [];
	// providerRelevantAssetClass: any[] = [];

	basicExpansionPanel: boolean = false;
	companyExpansionPanel: boolean = false;
	adminExpansionPanel: boolean = false;
	descriptionsExpansionPanel: boolean = false;
	categorizationsExpansionPanel: boolean = false;
	investigationExpansionPanel: boolean = false;
	relatedProvidersExpansionPanel: boolean = false;
	dataDetailsExpansionPanel: boolean = false;
	dataQualityExpansionPanel: boolean = false;
	dataDeliveryExpansionPanel: boolean = false;
	triallingExpansionPanel: boolean = false;
	licensingExpansionPanel: boolean = false;
	pricingExpansionPanel: boolean = false;
    discussionExpansionPanel: boolean = false;
    organizationList: {};
    organizationButton: boolean = false;
    resourceProvidersExpansionPanel: boolean = false;
    editLinkBtn: boolean = false;
	panelName: string = '';
	overAll: string = '';
	progressSpinner:boolean = false;
    approveSpinner: boolean = false;
    pendingEdits: boolean = false;
    // approveChangesBtn: boolean = false;
	basicForm = new FormGroup({});
	basicModel: any = {};
	providerFormModel: any = {};
	basicOptions: FormlyFormOptions = {};

	companyDetailsForm = new FormGroup({});
	companyDetailsModel: any = {};
	companyDetailsOptions: FormlyFormOptions = {};

	numberOfAnalystEmployeesForm = new FormGroup({});
	numberOfAnalystEmployeesModel: any = {};
	numberOfAnalystEmployeesOptions: FormlyFormOptions = {};

	adminForm = new FormGroup({});
	adminModel: any = {};
	adminOptions: FormlyFormOptions = {};

	descriptionsForm = new FormGroup({});
	descriptionsModel: any = {};
	descriptionsOptions: FormlyFormOptions = {};

	categorizationsForm = new FormGroup({});
	categorizationsModel: any = {};
	categorizationsOptions: FormlyFormOptions = {};

	investigationForm = new FormGroup({});
	investigationOptions: FormlyFormOptions = {};

	relatedProvidersForm = new FormGroup({});
	relatedProvidersOptions: FormlyFormOptions = {};

	dataDetailsForm = new FormGroup({});
    dataDetailsOptions: FormlyFormOptions = {};

    dataQualityForm = new FormGroup({});
	dataQualityOptions: FormlyFormOptions = {};

	dataDeliveryForm = new FormGroup({});
	dataDeliveryOptions: FormlyFormOptions = {};

	triallingForm = new FormGroup({});
	triallingOptions: FormlyFormOptions = {};

	licensingForm = new FormGroup({});
    licensingOptions: FormlyFormOptions = {};

    pricingForm = new FormGroup({});
    pricingModel: any = {};
	pricingOptions: FormlyFormOptions = {};

	discussionForm = new FormGroup({});
    discussionOptions: FormlyFormOptions = {};

	resourceProviderForm = new FormGroup({});
	resourceModel: any = {}
	resourcrProviderOption: FormlyFormOptions = {};

	relevantLinksForm = new FormGroup({});
    relevantLinksOption: FormlyFormOptions = {};

	basicFields: FormlyFieldConfig[] = [
        {
            key: 'id',
            type: 'input',
            className: 'col-md-3',
            hideExpression: true,
            templateOptions: {
                label: 'ID',
                fieldName:'id'
            }
        },
		{
			key: 'providerName',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Provider Name',
				placeholder: '',
				description: 'What is the name of the brand which acts as a data provider? This is often the name of the company.',
				required: true,
				attributes: {
					'placeholderText' : 'Name of the data provider.',
					'labelTooltip': 'Usually found in the logo on the website. This should be the name of the brand or division of the organization that collects/provides/analyzes data. It is not always the same name as the organization (but it often is).',
                    'descriptionTooltip': 'Usually found in the logo on the website. This should be the name of the brand or division of the organization that collects/provides/analyzes data. It is not always the same name as the organization (but it often is).',
                    'placeholderTooltip': 'Usually found in the logo on the website. This should be the name of the brand or division of the organization that collects/provides/analyzes data. It is not always the same name as the organization (but it often is).',
                    'trackNames': 'Provider Name',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input'
				},
                keypress: (event) => {
				},
                maxLength : 256,
				change: this.basicInput
			},
		},
        // {
        //     key: 'providerName',
        //     type: 'input',
        //     wrappers: ['form-field-horizontal'],
        //     templateOptions: {
        //         label: 'Edited Provider Name',
        //         readonly: false
        //     }
        // },
        {
            key: 'providerNameEdited',
            type: 'fieldset',
            // hideExpression: this.approvedProviderData.providerName !== null ? false : true,
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'website',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Provider Website',
				type: 'input',
				placeholder: '',
				//description: 'What is the link to the home webpage of your brand that acts as a data provider?',
				required: true,
				attributes: {
					'placeholderText' : 'Link to the website of the data provider or the division within the company/organization that provides/analyzes data.',
					'labelTooltip': '- Use the home page of the data provider.- If the data provider is a brand or division of a larger company, make sure the right link is added.- This should be a clean link without ""http://"" or ""https://"". And if the link contains variables after a ""?"" with ""utm"" in them, erase everything after the ""?""- e.g. ""http://www.google.com/?utm_source=abc&query=123"" should be changed to ""www.google.com""- Make sure the link is the most relevant one you can find for that data provider. Sometimes that means that the home page of the company is not the right link if their main business is not providing/analyzing data. For example, ShareThis (www.sharethis.com) is a social widget (technology plugin), but it also has an ""intelligence"" section of the website that is specifically regarding the data they are collecting/providing/analyzing. Therefore, www.sharethis.com/intelligence is the most relevant link.',
                    'descriptionTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                    'placeholderTooltip': '- Use the home page of the data provider.- If the data provider is a brand or division of a larger company, make sure the right link is added.- This should be a clean link without ""http://"" or ""https://"". And if the link contains variables after a ""?"" with ""utm"" in them, erase everything after the ""?""- e.g. ""http://www.google.com/?utm_source=abc&query=123"" should be changed to ""www.google.com""- Make sure the link is the most relevant one you can find for that data provider. Sometimes that means that the home page of the company is not the right link if their main business is not providing/analyzing data. For example, ShareThis (www.sharethis.com) is a social widget (technology plugin), but it also has an ""intelligence"" section of the website that is specifically regarding the data they are collecting/providing/analyzing. Therefore, www.sharethis.com/intelligence is the most relevant link.',
                    'trackNames': 'Provider Website',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input'
				},
                keypress: (event) => {
				},
				maxLength : 512,
				change: this.basicInput
            },
            validation: {
                messages: {
                    required: 'Website is required'
                }
            }
		},
        {
            key: 'websiteEdited',
            type: 'fieldset',
            // hideExpression: this.approvedProviderData.website !== null ? false : true,
            templateOptions: {
                label: ''
            }
        },
		{
			key: '',
			type: 'fileUpload',
			className: 'customWidth',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Provider Logo',
				attributes: {
					'logoType' : 'Logo',
					'accept': 'image/*',
					'role': 'admin',
					'trackNames': 'Provider Logo',
                    'trackCategory': 'Provider Profile Form - File',
                    'trackLabel': 'Provider Profile Form - File - Provider Logo'
				},
				change: ( event : any) => {
					if(event && event[0].name) {
						this.basicInput(event);
					}
					this.dataProviderService.onUpload(this.recordID, event[0]).subscribe(data=> {
						//console.log('Logo', data);
						if (data.logo) {
							this.basicModel = {
								...this.basicModel,
								logo: {logo: data.logo}
							}
							this.dataProviderService.sendChangeSignal(data.logo);
						}
						//console.log('Basic Model', this.basicModel);
					});
				}
			}
		}
	];

	companyDetailsFields: FormlyFieldConfig[] = [
        {
			key: 'ownerOrganization',
			type: 'autocomplete',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Owner Organization:',
                type: 'autocomplete',
				required: true,
				options: this.dataOrganizations,
				placeholder: 'Organization (company/subsidiary) that owns this data provider.',
                attributes: {
					field: 'name',
					'minLength': '1',
					'placeholderText': 'Organization (company/subsidiary) that owns this data provider.',
                    'labelTooltip': 'Often this is just one link to an organization of the same name.',
                    'placeholderTooltip':'Often this is just one link to an organization of the same name.'
				},
				change: (event: any) => {
					const queryObj = event;
					this.dataOrganizations = [];
					if(queryObj.query) {
						this.filterService.getAllOrganizations(queryObj.query).subscribe(d => {
							const listItems = d && d['hits'] ? d['hits']['hits'] : [];
							//console.log('...search...', listItems);
							for (let i = 0; i < listItems.length; i++) {
								//console.log('...search...', listItems);
								this.dataOrganizations.push({
									id: listItems[i]._source.id,
									name: listItems[i]._source.name,
								});
								//console.log('...search...', this.dataOrganizations);
							}
							this.companyDetailsFields[0].templateOptions.options = this.dataOrganizations;
						});
					}
				},
				blur: (event: any) => {
					//console.log('Blur event', event);
					if (event && event.target && event.target.value === '') {
						//event.target.value = '';
						this.emptyField = true;
					}
				},
				click: (event: any) => {
					//console.log('click event', event);
					this.orgID = event ? event.id : '';
					if (this.orgID != '' && this.orgID != undefined && this.orgID != null) {
						this.emptyField = false;
					} else {
						this.emptyField = true;
					}
					//console.log(this.organizationButton, '------', this.emptyField);
				}
			},
		},
	]
        /*{
			key: 'ownerOrganization.city',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'City:',
                type: 'input',
                required: true,
                description: 'City of the headquarters of the owner organization.',
                attributes: {
                    'trackNames': 'City',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input'
                },
				change: this.basicInput
			},
		},
		{
			key: 'ownerOrganization.state',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'State:',
				type: 'input',
                description: 'State of the headquarters of the owner organization.',
                attributes: {
                    'trackNames': 'State',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input'
                },
				change: this.basicInput
			},
		},
        {
			key: 'ownerOrganization.country',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Country:',
                type: 'input',
                required: true,
                description: 'Country of the headquarters of the organization.',
                attributes: {
                    'trackNames': 'Country',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input'
                },
				change: this.basicInput
			},
		},
		{
			key: 'ownerOrganization.zipCode',
			type: 'mask',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Zip Code:',
				description: 'Zip code of the headquarters of the owner organization.',
				attributes: {
                    'mask': '99999',
                    'trackNames': 'Zip Code',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input'
				}
			},
		},
		{
			key: 'ownerOrganization.yearFounded',
			type: 'mask',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Year Founded:',
                description: 'Year that the owner organization was founded.',
                attributes: {
                    'mask': '9999',
                    'trackNames': 'Year Founded',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input'
                },
                maxLength: 4,
                change: this.basicInput
			},
		},
		{
			key: 'ownerOrganization.headCount',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Headcount Details:',
                description: 'Details regarding the number of people that work at the organization.',
                attributes: {
                    'trackNames': 'Headcount Details',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input'
                },
				change: this.basicInput
			},
		},*/
		numberOfAnalystEmployeesFields: FormlyFieldConfig[] = [
		{
			key: 'numberOfAnalystEmployees',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Number of Analyst Employees:',
				type: 'number',
                description: 'How many employees does the data provider have focused on improving the value of their data (such as collecting, cleaning, or analyzing it)?',
                attributes: {
                    'trackNames': 'Number of Analyst Employees',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input'
                },
                keypress: (event) => {
				},
				change: this.basicInput
			},
		},
        {
            key: 'numberOfAnalystEmployeesEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
        {
            key: 'emailAddress',
            type: 'autocomplete',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label:'Email Addresses('+this.emailAddressCount+'):',
                description: 'Please input important email addresses, such as support, CEO or main POC.',
                placeholder: '',
                options: this.emailAddress,
                attributes: {
                    field: 'emailAddress',
                    'minLength': '1',
                    'placeholderText' : 'Email addresses belonging to the data provider (such as employee email addresses or generic provider-wide email addresses).',
                    'labelTooltip': '',
                    'multiple': 'true',
                    // 'template': 'true',
                    // 'dropdown': 'true',
                    'placeholderTooltip': ''
                },
                change: (event: any) => {
                    const queryObj = event;
					this.emailAddress = [];
					const selected = this.basicModel['emailAddress'];
					///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
					if(selected && selected.length) {
						this.selectedValue = selected.map(item => item.id);
					}
					if(queryObj.query) {
						this.filterService.getAllEmailAddres(queryObj.query).subscribe(d => {
						const listItems = d && d['hits'] ? d['hits']['hits'] : [];
                        console.log(listItems);
						for (let i = 0; i < listItems.length; ++i) {
							const item = listItems[i]._source;
                            console.log(item);
							// if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
								if (!this.selectedValue.includes(item.id)) {
									this.emailAddress.push({
										id: item.id,
										emailAddress: item.emailAddress,
									});
								}
							// }
						}
                            console.log(this.emailAddress);
                            setTimeout(() => {
                                this.numberOfAnalystEmployeesFields[2].templateOptions.options = this.emailAddress;
                            }, 2500);
						});
					} else {
                        this.filterService.getAllEmailAddres().subscribe(d => {
                            const listItems = d && d['hits'] ? d['hits']['hits'] : [];
                            console.log(listItems);
                            for (let i = 0; i < listItems.length; ++i) {
                                const item = listItems[i]._source;
                                console.log(item);
                                // if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
                                    if (!this.selectedValue.includes(item.id)) {
                                        this.emailAddress.push({
                                            id: item.id,
                                            emailAddress: item.emailAddress,
                                        });
                                    }
                                // }
                            }
                            console.log(this.emailAddress);
                            setTimeout(() => {
                                this.numberOfAnalystEmployeesFields[2].templateOptions.options = this.emailAddress;
                            }, 2500);
                        });
                    }
                },
                blur: (event: any) => {
                    if (event && event.target && !event['selectedVal']) {
                        event.target.value = '';
                    }
                }
            },
        },
        {
            key: 'user',
            type: 'autocomplete',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Users('+this.usersCount+'):',
                placeholder: '',
                options: this.users,
                attributes: {
                    field: 'user',
                    'minLength': '1',
                    'multiple': 'true',
                    // 'template': 'true',
                    // 'dropdown': 'true',
                },
                change: (event: any) => {
                    const queryObj = event;
					this.users = [];
					const selected = this.basicModel['user'];
					///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
					if(selected && selected.length) {
						this.selectedValue = selected.map(item => item.id);
					}
					if(queryObj.query) {
						this.filterService.getAllUsers(queryObj.query).subscribe(d => {
						const listItems = d && d['hits'] ? d['hits']['hits'] : [];
                        console.log(listItems);
						for (let i = 0; i < listItems.length; ++i) {
							const item = listItems[i]._source;
                            console.log(item);
							// if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
								if (!this.selectedValue.includes(item.id)) {
									this.users.push({
										id: item.id,
										user: item.email
									});
								}
							// }
						}
                            console.log(this.users);
                            setTimeout(() => {
                                this.numberOfAnalystEmployeesFields[3].templateOptions.options = this.users;
                            }, 2500);
						});
					} else {
                        this.filterService.getAllUsers().subscribe(d => {
                            const listItems = d && d['hits'] ? d['hits']['hits'] : [];
                            console.log(listItems);
                            for (let i = 0; i < listItems.length; ++i) {
                                const item = listItems[i]._source;
                                console.log(item);
                                // if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
                                    if (!this.selectedValue.includes(item.id)) {
                                        this.users.push({
                                            id: item.id,
                                            user: item.email
                                        });
                                    }
                                // }
                            }
                            console.log(this.users);
                            setTimeout(() => {
                                this.numberOfAnalystEmployeesFields[3].templateOptions.options = this.users;
                            }, 2500);
                        });
                    }
                },
                blur: (event: any) => {
                    if (event && event.target && !event['selectedVal']) {
                        event.target.value = '';
                    }
                }
            },
        },
	];

	descriptionsFields: FormlyFieldConfig[] = [
		{
			key: 'summary',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Summary:',
				description: '',
				change: this.basicInput,
				attributes:{
                    'labelTooltip': 'this should be specifically geared towards the "alternative data in investing" use cases',
					'placeholderText': 'Summary of the data provider\'s offerings in approximately one paragraph.',
					'placeholderTooltip': 'this should be specifically geared towards the "alternative data in investing" use cases',
                    'trackNames': 'Summary',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input',
                    'maximumLength' : 1024
				},
				// maxLength : 1024,
			},
		},
        {
            key: 'summaryEdited',
            type: 'fieldset',
            // hideExpression: this.approvedProviderData.summary !== null ? false : true,
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'shortDescription',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Short Description:',
				minRows: 2,
				maxRows: 100,
				required: true,
				placeholder: '',
                description: '',
                change: this.basicInput,
                attributes: {
					'placeholderText' : 'Short one sentence (or less) description of the specific types of data or information that the data provider collects/provides/analyzes.',
					'labelTooltip': 'Do not include the name of the provider. This should be a brief (1 sentence or less) summary of the specific data/analytics/products that the provider offers that are relevant to investment research or analysis. This is not just a general description of the company and everything they do.If there are several datasets or analytics, describe each briefly, or group them together.Also, it needs to be specific enough to be understood in such a brief statement, so not using any of their ""marketing jargon"".Example: if the company does consulting for the healthcare sector but also provides healthcare analytics to hospitals, this description should focus on the analytics not the consulting.This needs to be informative on the data component of the business and re-phrased in order to be more subjective. Example: The short description you originally wrote just describes who ChannelAdvisor helps, not how they are helping them and what the company is actually doing. And for ChannelAdvisor, since it doesn\'t directly operate in the finance industry, we don\'t really care who they are helping, but more about the core of the data they are collecting (in the hopes that data or analysis is also useful in our use case).',
                    'placeholderTooltip': 'Do not include the name of the provider.This should be a brief (1 sentence or less) summary of the specific data/analytics/products that the provider offers that are relevant to investment research or analysis. This is not just a general description of the company and everything they do.If there are several datasets or analytics, describe each briefly, or group them together.Also, it needs to be specific enough to be understood in such a brief statement, so not using any of their ""marketing jargon"".Example: if the company does consulting for the healthcare sector but also provides healthcare analytics to hospitals, this description should focus on the analytics not the consulting.This needs to be informative on the data component of the business and re-phrased in order to be more subjective. Example: The short description you originally wrote just describes who ChannelAdvisor helps, not how they are helping them and what the company is actually doing. And for ChannelAdvisor, since it doesn\t directly operate in the finance industry, we don\'t really care who they are helping, but more about the core of the data they are collecting (in the hopes that data or analysis is also useful in our use case).',
                    'trackNames': 'Short Description',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input',
                    'maximumLength' : 400
				},
				// maxLength : 400,
            },
		},
        {
            key: 'shortDescriptionEdited',
            type: 'fieldset',
            // hideExpression: this.approvedProviderData.shortDescription !== null ? false : true,
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'longDescription',
			type: 'inputEditor',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Long Description:',
                required: true,
				placeholder: '',
                description: '',
                change: this.basicInput,
                attributes:{
					'placeholderText' : 'Longer description of the data provider, its datasets, and wider business. Usually 1-3 paragraphs in length.',
                    'labelTooltip': 'Quickest way to fill this in is by copying straight from the data provider\'s website or other resource (suggestions listed below). This should be a more general description of the data provider\'s businesses and operations. But it should be also specific enough in nature to have a pretty thorough understanding of what the data provider does.Eliminate paragraphs that do not give any information about what the company does, or a sentence explaining the founder and office locations. Do not go line by line cleaning up the description. The description does not have to explicitly include all products and services, but it should reference all the capabilities provided.It\'s sometimes difficult to capture this precisely from the homepage - try looking through the ""About"" section of the website.Other good sources to find the core functions of a data provider are: Crunchbase Angellist Wikipedia LinkedIn Do not include information that appears elsewhere in this database, such as date founded, location, CEO, or the like.',
                    'placeholderTooltip': 'Quickest way to fill this in is by copying straight from the data provider\'s website or other resource (suggestions listed below). This should be a more general description of the data provider\'s businesses and operations. But it should be also specific enough in nature to have a pretty thorough understanding of what the data provider does.Eliminate paragraphs that do not give any information about what the company does, or a sentence explaining the founder and office locations. Do not go line by line cleaning up the description. The description does not have to explicitly include all products and services, but it should reference all the capabilities provided.It\'s sometimes difficult to capture this precisely from the homepage - try looking through the ""About"" section of the website.Other good sources to find the core functions of a data provider are: Crunchbase Angellist Wikipedia LinkedIn Do not include information that appears elsewhere in this database, such as date founded, location, CEO, or the like.',
                    'trackNames': 'Long Description',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input'
				},
			},
		},
        {
            key: 'longDescriptionEdited',
            type: 'fieldset',
            // hideExpression: this.approvedProviderData.longDescription !== null ? false : true,
            templateOptions: {
                label: ''
            }
        },
	];

	categorizationsFields: FormlyFieldConfig[] = [];

	investigationFields: FormlyFieldConfig[] = [];

	relatedProvidersFields: FormlyFieldConfig[] = [
		{
			key: 'providerPartners',
			type: 'autocomplete',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Provider Data Partners ('+this.providerPartnersCount+'):',
				description: '',
				options: this.providerPartners,
				attributes:{
					field : 'providerName',
					'multiple': 'true',
					'minLength': '1',
					'placeholderText': 'Data providers that this data provider receives data from.',
					'placeholderTooltip': 'Do not proactively search for partners. Only add if you come across the fact that a company has a partner.The data providers referenced in this field supply data to the data provider being researched in this row.',
					'labelTooltip': 'Do not proactively search for partners. Only add if you come across the fact that a company has a partner.The data providers referenced in this field supply data to the data provider being researched in this row.'
				},
				change: (event: any) => {
					const queryObj = event;
					this.providerPartners = [];
					const selected = this.basicModel['providerPartners'];
					///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
					if(selected && selected.length) {
						this.selectedValue = selected.map(item => item.id);
					}
					if(queryObj.query) {
						this.filterService.getAllProviders(queryObj.query).subscribe(d => {
						const listItems = d && d['hits'] ? d['hits']['hits'] : [];
						for (let i = 0; i < listItems.length; ++i) {
							const item = listItems[i]._source;
							if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
								if (!this.selectedValue.includes(item.id)) {
									this.providerPartners.push({
										id: item.id,
										providerName: item.providerName,
										img: item.logo ? item.logo.logo : null
									});
								}
							}
						}
							this.relatedProvidersFields[0].templateOptions.options = this.providerPartners;
						});
					}
				},
				blur: (event: any) => {
					if (event && event.target && !event['selectedVal']) {
						event.target.value = '';
					}
				}
	        },
		},
        {
            key: 'editProviderDataPartnerEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'distributionPartners',
			type: 'autocomplete',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Distribution Data Partners ('+this.distributionPartnersCount+'):',
                description: '',
				options: this.providerDistributionPartner,
				attributes:{
					field : 'providerName',
					'multiple': 'true',
					'minLength': '1',
					'placeholderText': 'Data providers that have partnered with this this provider in order to distribute their data.',
					'labelTooltip': 'Do not proactively search for partners. Only add if you come across the fact that a company has a partner.The data providers referenced in this field are partnered (or have some type of relationship) with the data provider being researched in this row in order to distribute their data to third parties.',
					'placeholderTooltip' : 'Do not proactively search for partners. Only add if you come across the fact that a company has a partner. The data providers referenced in this field are partnered (or have some type of relationship) with the data provider being researched in this row in order to distribute their data to third parties.'
				},
				change: (event: any) => {
					const queryObj = event;
					this.providerDistributionPartner = [];

					const selected = this.basicModel['distributionPartners'];
					///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
					if(selected && selected.length) {
						this.selectedValue = selected.map(item => item.id);
					}
                    if(queryObj.query) {
						this.filterService.getAllProviders(queryObj.query).subscribe(d => {
						const listItems = d && d['hits'] ? d['hits']['hits'] : [];
						for (let i = 0; i < listItems.length; ++i) {
							const item = listItems[i]._source;
							if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
								if (!this.selectedValue.includes(item.id)) {
									this.providerDistributionPartner.push({
										id: item.id,
										providerName: item.providerName,
										img: item.logo ? item.logo.logo : null
									});
							    }
							}
						}
							this.relatedProvidersFields[1].templateOptions.options = this.providerDistributionPartner;
						});
				    }
				},
				blur: (event: any) => {
					if (event && event.target && !event['selectedVal']) {
						event.target.value = '';
					}
				}
			},
		},
        {
            key: 'editDistributionDataPartnerEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'consumerPartners',
			type: 'autocomplete',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Consumer Data Partners ('+this.consumerPartnersCount+'):',
                description: '',
				options: this.providerConsumerPartner,
				attributes:{
					field : 'providerName',
					'multiple': 'true',
					'minLength': '1',
					'placeholderText': 'Data providers that receive data from this data provider and do not distribute, but use internally.',
					'labelTooltip': 'Do not proactively search for partners. Only add if you come across the fact that a company has a partner.The data providers referenced in this field purchase or receive (whether through a partnership or another type of relationship) with the data provider being researched in this row in order to use the data internally for research or to write qualitative/investment reports but not to distribute externally.',
					'placeholderTooltip' : 'Do not proactively search for partners. Only add if you come across the fact that a company has a partner. The data providers referenced in this field purchase or receive (whether through a partnership or another type of relationship) with the data provider being researched in this row in order to use the data internally for research or to write qualitative/investment reports but not to distribute externally.'
				},
				change: (event: any) => {
					const queryObj = event;
					this.providerConsumerPartner = [];
                    const selected = this.basicModel['consumerPartners'];
					//console.log('Selected Providers', selected);
					///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
					if(selected && selected.length) {
						this.selectedValue = selected.map(item => item.id);
					}
					//console.log('Query...', _that.selectedValue);
				    if(queryObj.query) {
						this.filterService.getAllProviders(queryObj.query).subscribe(d => {
							const listItems = d && d['hits'] ? d['hits']['hits'] : [];
							for (let i = 0; i < listItems.length; ++i) {
								const item = listItems[i]._source;
								if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
									if (!this.selectedValue.includes(item.id)) {
										this.providerConsumerPartner.push({
											id: item.id,
											providerName: item.providerName,
											img: item.logo ? item.logo.logo : null
										});
									}
								}
							}
							this.relatedProvidersFields[2].templateOptions.options = this.providerConsumerPartner;
						});
				    }
				},
				blur: (event: any) => {
					if (event && event.target && !event['selectedVal']) {
						event.target.value = '';
					}
				}
			},
		},
        {
            key: 'editConsumerDataPartnerEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'competitors',
			type: 'autocomplete',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Competitors ('+this.competitorsCount+'):',
                description: 'Who does the provider consider their closest competitors?',
				options: this.providerCompetitors,
				attributes:{
					field : 'providerName',
					'multiple': 'true',
					'minLength': '1',
					'placeholderText' : ''
				},
				change: (event: any) => {
					const queryObj = event;
					this.providerCompetitors = [];

					const selected = this.basicModel['competitors'];
					///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
					if(selected && selected.length) {
						this.selectedValue = selected.map(item => item.id);
					}
					if(queryObj.query) {
						this.filterService.getAllProviders(queryObj.query).subscribe(d => {
						const listItems = d && d['hits'] ? d['hits']['hits'] : [];
						for (let i = 0; i < listItems.length; ++i) {
							const item = listItems[i]._source;
							if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
								if (!this.selectedValue.includes(item.id)) {
									this.providerCompetitors.push({
										id: item.id,
										providerName: item.providerName,
										img: item.logo ? item.logo.logo : null
									});
								}
							}
						}
							this.relatedProvidersFields[3].templateOptions.options = this.providerCompetitors;
						});
					}
				},
				blur: (event: any) => {
					if (event && event.target && !event['selectedVal']) {
						event.target.value = '';
					}
				}
			},
		},
        {
            key: 'editCompetitorProviderEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'competitiveDifferentiators',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Competitive Differentiators:',
                description: 'What sets the data provider\'s data offerings apart from their competition?',
                attributes: {
                    'trackNames': 'Competitive Differentiators',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input'
				},
                change: this.basicInput
			},
		},
        {
            key: 'competitiveDifferentiatorsEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
	];

	dataDetailsFields: FormlyFieldConfig[] = [];
    /*---------------------------------- newly added section for admin------------------------------- */
    dataQualityFields: FormlyFieldConfig[] = [];

	dataDeliveryFields: FormlyFieldConfig[] = [
		{
			key: 'deliveryMethods',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Delivery Methods:',
				description: 'Using what methods does this provider typically deliver their data products?',
				options: this.deliveryMethods,
				attributes: {
					field: 'desc'
				}
			},
		},
        {
            key: 'deliveryMethodsEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'deliveryFormats',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Delivery Formats:',
				description: 'In what formats does this provider typically deliver their data products?',
				options: this.deliveryFormats,
				attributes: {
					field: 'desc'
				}
			},
        },
        {
            key: 'deliveryFormatsEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
        /* -------------------------newly added dropdowm----------------- */
		{
			key: 'deliveryFrequencies',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Delivery Frequency:',
				description: 'How often does this provider typically deliver their data or analytics?',
                options: this.deliveryFrequencies
			},
		},
        {
            key: 'deliveryFrequenciesEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'lagTime',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Lag Time:',
				description: 'What is the lag time between when an event or action occurs and the data for that event or action is available to the data consumer?',
                placeholder: '',
				change: this.basicInput,
				attributes:{
                    'placeholderText' : 'What is the lag time between when an event or action occurs and the data for that event or action is available to the data consumer?',
                    'trackNames': 'Lag Time',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input',
                    'maximumLength': 50
				},
				// maxLength : 50,
			},
		},
        {
            key: 'lagTimeEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'deliveryFrequencyNotes',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Delivery Frequency Notes:',
				minRows: 2,
				maxRows: 100,
                description: 'Please provide any additional details regarding how often the provider is able to deliver their data offerings.',
                attributes: {
                    'trackNames': 'Delivery Frequency Notes',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input',
                    'maximumLength': 255
				},
				// maxLength : 255,
                change: this.basicInput
			},
		},
        {
            key: 'deliveryFrequencyNotesEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'dataUpdateFrequency',
			type: 'dropdown',
			className: 'widthSticky-dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data Update Frequency:',
				description: 'How often does this provider typically update their data products?',
				options: this.dataUpdateFrequency,
				attributes: {
					field: 'label',
				},
			},
		},
        {
            key: 'dataUpdateFrequencyEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'dataUpdateFrequencyNotes',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data Update Frequency Notes:',
				minRows: 2,
				maxRows: 100,
                placeholder: '',
				change: this.basicInput,
				attributes:{
                    'placeholderText' : 'Details about the frequency that this provider updates their data products',
                    'trackNames': 'Provider Name',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input',
                    'maximumLength': 512
				},
				// maxLength : 512,
			},
		},
        {
            key: 'dataUpdateFrequencyNotesEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
	];

	triallingFields: FormlyFieldConfig[] = [];

	licensingFields: FormlyFieldConfig[] = [
		{
			key: 'dataLicenseType.id',
			type: 'dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data License Type:',
				description: 'What type of data license is offered by this provider for their data?',
				options: this.providerDataLicenseType,
				attributes: {
					field: 'label'
				}
			},
		},
        {
            key: 'dataLicenseTypeEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'dataLicenseTypeDetails',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data License Type Details:',
				minRows: 2,
                maxRows: 100,
                placeholder: '',
				change: this.basicInput,
				attributes:{
                    'placeholderText' : 'Any relevant additional details regarding the provider\'s data licenses',
                    'trackNames': 'Data License Type Details',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input',
                    'maximumLength': 10000
				},
				// maxLength : 10000,

			},
		},
        {
            key: 'dataLicenseTypeDetailsEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
		{
			key: 'paymentMethodsOffered',
			type: 'multiselect',
			className: 'custom-multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Payment Methods Offered:',
				description: 'What payment methods does the data provider offer to their customers?',
				options: this.providerPaymentMethodsOffered,
				attributes: {
					'lookupmodule' : 'PAYMENT_METHOD'
				}
			},
		},
        {
            key: 'paymentMethodsOfferedEdited',
            type: 'fieldset',
            templateOptions: {
                label: ''
            }
        },
    ];

    /* ----------------------------- newly added admin form fields ---------------------------- */

    pricingFields: FormlyFieldConfig[] = [];
    adminFields: FormlyFieldConfig[] = [
		{
			key: 'permission.name',
			type: 'radioButton',
			defaultValue: 'ROLE_SILVER',
			/* className: 'customSmallWidth-dropdown', */
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'User Permissions:',
				required: true,
				description: 'Who should be able to see this data profile?',
				options: [
					{
						value: 'ROLE_USER',
						label: 'Unregistered'
					},
					{
						value: 'ROLE_USER',
						label: 'Registered'
					},
					{
						value: 'ROLE_BRONZE',
						label: 'Bronze'
					},
					{
						value: 'ROLE_SILVER',
						label: 'Silver'
					},
					{
						value: 'ROLE_GOLD',
						label: 'Gold'
					},
					{
						value: 'ROLE_PLATINUM',
						label: 'Platinum'
					},
					{
					value: 'ROLE_ADMIN',
					label: 'Admin'
				}
			],
			},
		},
		{
			key: 'privacy.id',
			type: 'radioButton',
			defaultValue: 582,
			/* className: 'customSmallWidth-dropdown', */
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Privacy:',
				required: true,
				description: 'Who should be able to see this data profile?',
				options: this.providerPrivacy,
				attributes: {
					field: 'label',
				},
			},
		},
		{
			key: 'marketplaceStatus.id',
			type: 'dropdown',
			className: 'customMediumWidth-dropdown',
			defaultValue: environmentDev.dev ? 2043 : 1985,
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Marketplace Status:',
				required: true,
				description: 'What should an external client understand about the status of our research on this provider?',
                options: this.providerMarketplaceStatus,
                attributes: {
                    field: 'label',
                    'labelTooltip': 'The only statuses IEI will be using are ""Minimal"" and ""Complete"". Ignore all other statuses.Explanation of choices:""Minimal"": When you\'ve only done the minimal research""Complete"": When the full research has been completed',
                    'descriptionTooltip': 'The only statuses IEI will be using are ""Minimal"" and ""Complete"". Ignore all other statuses.Explanation of choices:""Minimal"": When you\'ve only done the minimal research""Complete"": When the full research has been completed'
                }
			},
		},
		{
			key: 'researchMethods',
			type: 'multiselect',
			// className: 'custom-multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Research Methods Completed:',
				description: 'What types/methods of researching a provider have been completed?',
				required: true,
				change: this.basicInput,
				options: this.providerResearchMethods,
                attributes: {
					field: 'desc',
                    'labelTooltip': 'Fill these in as you research a provider, and especially after you\'re done researching a provider.Explanation of choices:""Web Research - Minimal"": used when only the minimum research has been completed for a provider, normally when adding a new provider that was discovered in an article. This should always be activated if an IEI researcher goes through the full research process for a provider.""Web Research - Provider\'s Website"": used when the provider\'s website has been completely looked through to fill in the assigned fields. This should always be activated if an IEI researcher goes through the full research process for a provider""Web Research - Google Search"": used when a google search was done on the provider and other sites besides the provider\'s website, LinkedIn and Crunchbase were consulted with""Web Research - LinkedIn"": used when any information is gathered from the provider\'s LinkedIn page""Web Research - Crunchbase"": used when any information is gathered from the provider\'s Crunchbase page""Web Research - Angelist"": used when any information is gathered from the provider\'s Angelist page""Web Research - Wikipedia"": used when any information is gathered from the provider\'s Wikipedia page""Partners - Data"": used when all of the provider\'s data partners (provider, distribution, and consumer) have been gathered from the provider\'s website. This should not be checked off only some of the partners were gathered and not all, such as when there were too many on the page to be done efficiently.""Audit - IEI Quality Control"": used when a Quality Control employee within IEI has reviewed the information collected by an IEI researcher',
					'descriptionTooltip': 'Fill these in as you research a provider, and especially after you\'re done researching a provider.Explanation of choices:""Web Research - Minimal"": used when only the minimum research has been completed for a provider, normally when adding a new provider that was discovered in an article. This should always be activated if an IEI researcher goes through the full research process for a provider.""Web Research - Provider\'s Website"": used when the provider\'s website has been completely looked through to fill in the assigned fields. This should always be activated if an IEI researcher goes through the full research process for a provider""Web Research - Google Search"": used when a google search was done on the provider and other sites besides the provider\'s website, LinkedIn and Crunchbase were consulted with""Web Research - LinkedIn"": used when any information is gathered from the provider\'s LinkedIn page""Web Research - Crunchbase"": used when any information is gathered from the provider\'s Crunchbase page""Web Research - Angelist"": used when any information is gathered from the provider\'s Angelist page""Web Research - Wikipedia"": used when any information is gathered from the provider\'s Wikipedia page""Partners - Data"": used when all of the provider\'s data partners (provider, distribution, and consumer) have been gathered from the provider\'s website. This should not be checked off only some of the partners were gathered and not all, such as when there were too many on the page to be done efficiently.""Audit - IEI Quality Control"": used when a Quality Control employee within IEI has reviewed the information collected by an IEI researcher',
                }
            },
		},
		{
			key: 'ieiStatus.id',
			type: 'dropdown',
			className: 'mediumWidthSticky-dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'IEI Status:',
				description: 'What\'s the status of this provider\'s research?',
				required: false,
				options: this.providerIEIStatus,
                attributes: {
					field: 'label',
                    'labelTooltip': 'Explanation of choices:""Needs IEI QA"": the IEI researcher is unsure about the quality of his/her work and wants IEI\'s QA manager to review it""Needs Amass QA"": the IEI researcher and the IEI QA manager is unsure about the quality of his/her work and wants an Amass employee to review it""Done"": IEI is confident about their work and have finished all of the steps in the research process',
					'descriptionTooltip': 'Explanation of choices:""Needs IEI QA"": the IEI researcher is unsure about the quality of his/her work and wants IEI\'s QA manager to review it""Needs Amass QA"": the IEI researcher and the IEI QA manager is unsure about the quality of his/her work and wants an Amass employee to review it""Done"": IEI is confident about their work and have finished all of the steps in the research process',
                }
            },
		},
		{
			key: 'priority',
			type: 'dropdown',
			className: 'smallWidthSticky-dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Priority:',
				description: 'If filled in, this denotes how a researcher should prioritize their work.',
				required: false,
				options: this.providerPriority,
                attributes: {
					field: 'label'
                }
            },
		},
	];
	discussionFields: FormlyFieldConfig[] = [];
    dataResourceFieldsFile: FormlyFieldConfig[] = [
       /* {
			key: '',
			type: '',
			className: 'customWidth',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Relevant Files:',
				description: 'Upload any documents that contain further details about this data provider\'s offerings, such as marketing literature, data dictionaries, data snapshots, data samples, or anything else you think would help a potential data consumer to understand the data provider\'s products.',
				required: false,
                attributes: {
					field: 'id',
                    'labelTooltip': 'Fill these in as you research a provider, and especially after you\'re done researching a provider.Explanation of choices:""Web Research - Minimal"": used when only the minimum research has been completed for a provider, normally when adding a new provider that was discovered in an article. This should always be activated if an IEI researcher goes through the full research process for a provider.""Web Research - Provider\'s Website"": used when the provider\'s website has been completely looked through to fill in the assigned fields. This should always be activated if an IEI researcher goes through the full research process for a provider""Web Research - Google Search"": used when a google search was done on the provider and other sites besides the provider\'s website, LinkedIn and Crunchbase were consulted with""Web Research - LinkedIn"": used when any information is gathered from the provider\'s LinkedIn page""Web Research - Crunchbase"": used when any information is gathered from the provider\'s Crunchbase page""Web Research - Angelist"": used when any information is gathered from the provider\'s Angelist page""Web Research - Wikipedia"": used when any information is gathered from the provider\'s Wikipedia page""Partners - Data"": used when all of the provider\'s data partners (provider, distribution, and consumer) have been gathered from the provider\'s website. This should not be checked off only some of the partners were gathered and not all, such as when there were too many on the page to be done efficiently.""Audit - IEI Quality Control"": used when a Quality Control employee within IEI has reviewed the information collected by an IEI researcher',
					'descriptionTooltip': 'Fill these in as you research a provider, and especially after you\'re done researching a provider.Explanation of choices:""Web Research - Minimal"": used when only the minimum research has been completed for a provider, normally when adding a new provider that was discovered in an article. This should always be activated if an IEI researcher goes through the full research process for a provider.""Web Research - Provider\'s Website"": used when the provider\'s website has been completely looked through to fill in the assigned fields. This should always be activated if an IEI researcher goes through the full research process for a provider""Web Research - Google Search"": used when a google search was done on the provider and other sites besides the provider\'s website, LinkedIn and Crunchbase were consulted with""Web Research - LinkedIn"": used when any information is gathered from the provider\'s LinkedIn page""Web Research - Crunchbase"": used when any information is gathered from the provider\'s Crunchbase page""Web Research - Angelist"": used when any information is gathered from the provider\'s Angelist page""Web Research - Wikipedia"": used when any information is gathered from the provider\'s Wikipedia page""Partners - Data"": used when all of the provider\'s data partners (provider, distribution, and consumer) have been gathered from the provider\'s website. This should not be checked off only some of the partners were gathered and not all, such as when there were too many on the page to be done efficiently.""Audit - IEI Quality Control"": used when a Quality Control employee within IEI has reviewed the information collected by an IEI researcher',
                },
				change: ( event : any) => {
					console.log('File event', event);
					this.dataProviderService.onRelevantFileUpload(this.recordID, event).subscribe(data => {
						console.log('Data', data);
					});
				}
			}
		} */
    ];
    dataResourceFieldsLink: FormlyFieldConfig[] = [
       /*  {
             key: 'link',
             type: 'autocomplete',
             className: 'disableSelectedValue',
             wrappers: ['form-field-horizontal'],
             templateOptions: {
                 label: 'Relevant Links:',
                 required: false,
                 options: this.dataLinks,
                 attributes: {
                     field: 'link',
					 'multiple': 'true',
					 'minLength': '1',
                     'labelTooltip': 'Only add a link if the information within the link has not already been added to the Data Provider Research, such as when there are numerous partners listed and it would take a lot of time to add them all. If there is an affiliate program, what is the website where the affiliate program is described or laid out? If the price of products is posted, what is the website where it\'s posted? (Only makes sense to fill this in if the prices are posted centrally onto one site.) If the site should be scraped for partners, what website has the listing of the partners? (Only makes sense to fill this in if the partners are listed centrally on one (or a couple) site.) If the site should be scraped for customers, what website had the listing of the customers? (Only makes sense to fill this in if the customers are listed centrally on one (or a couple) site.',
                     'descriptionTooltip': 'Fill these in as you research a provider, and especially after you\'re done researching a provider.Explanation of choices:""Web Research - Minimal"": used when only the minimum research has been completed for a provider, normally when adding a new provider that was discovered in an article. This should always be activated if an IEI researcher goes through the full research process for a provider.""Web Research - Provider\'s Website"": used when the provider\'s website has been completely looked through to fill in the assigned fields. This should always be activated if an IEI researcher goes through the full research process for a provider""Web Research - Google Search"": used when a google search was done on the provider and other sites besides the provider\'s website, LinkedIn and Crunchbase were consulted with""Web Research - LinkedIn"": used when any information is gathered from the provider\'s LinkedIn page""Web Research - Crunchbase"": used when any information is gathered from the provider\'s Crunchbase page""Web Research - Angelist"": used when any information is gathered from the provider\'s Angelist page""Web Research - Wikipedia"": used when any information is gathered from the provider\'s Wikipedia page""Partners - Data"": used when all of the provider\'s data partners (provider, distribution, and consumer) have been gathered from the provider\'s website. This should not be checked off only some of the partners were gathered and not all, such as when there were too many on the page to be done efficiently.""Audit - IEI Quality Control"": used when a Quality Control employee within IEI has reviewed the information collected by an IEI researcher',
                     'placeholderText' : 'A list of the links that are relevant to gathering more information on this data provider.',
                     'placeholderTooltip' : 'Only add a link if the information within the link has not already been added to the Data Provider Research, such as when there are numerous partners listed and it would take a lot of time to add them all. If there is an affiliate program, what is the website where the affiliate program is described or laid out? If the price of products is posted, what is the website where it\'s posted? (Only makes sense to fill this in if the prices are posted centrally onto one site.) If the site should be scraped for partners, what website has the listing of the partners? (Only makes sense to fill this in if the partners are listed centrally on one (or a couple) site.) If the site should be scraped for customers, what website had the listing of the customers? (Only makes sense to fill this in if the customers are listed centrally on one (or a couple) site.'
                 },
                 change: (event: any) => {
                    // console.log('default value....', event);
                     const queryObj = event;
                     this.dataLinks = [];

                     this.filterService.getDataLinks(queryObj.query).subscribe(d => {
                         const listItems =  d ? d['hits']['hits'] : [];
                        // console.log('...search...', listItems);
                         for (let i = 0; i < listItems.length; i++) {
                            // console.log('...search 1...', listItems);
                             this.dataLinks.push({
                                 id: listItems[i]._source.id,
                                 link: listItems[i]._source.link,
                             });
                             //console.log('...search...', this.dataOrganizations);
                         }
                         this.dataResourceFieldsLink[0].templateOptions.options = this.dataLinks;
                     });
                 },
                 blur: (event: any) => {
                     //console.log('Blur event', event);
                     if (event && event.target && !event['selectedVal']) {
                         event.target.value = '';
                     }
				 },
				 click: (event: any) => {
					console.log('click event', event);
					if(event) {
						this.disableTable = true;
					}
					this.addRelevantLinks.push(event);
                    this.linkID = event ? event.id : '';
					//console.log(this.organizationButton, '------', this.emptyField);
				}
            },
         }, */
     ];

	csrf: string;
	// uppyEvent = new Subject<[string, any, any, any]>();
	// uppyPlugins = [
	// 	['Dashboard', {
	// 		target: '.DashboardContainer',
	// 		replaceTargetContent: true,
	// 		inline: true,
	// 		restrictions: {
	// 			maxFileSize: 200000000,
	// 			maxNumberOfFiles: 10,
	// 			minNumberOfFiles: 1,
	// 			allowedFileTypes: ['image/*', 'video/*']
	// 		}
	// 	}],
	// 	['XHRUpload', {
	// 		endpoint: '/api/ur/multiple-file-upload?providerId=' + this.recordID,
	// 		formData: true,
	// 		bundle: true,
	// 		fieldName: 'fileUploads'
    //     }]/* ,
	// 	['GoogleDrive', {
	// 		target: 'Dashboard',
	// 		serverUrl: 'https://companion.uppy.io'
	// 	}],
	// 	['Dropbox', { target: 'Dashboard', serverUrl: 'https://companion.uppy.io' }] */
	// ];
	// onDestroy$ = new Subject<void>();

	constructor(
		private dataProviderService: DataProviderService,
		private resourceAdminService: AddResourceAdminService,
		private filterService: AmassFilterService,
		private route: ActivatedRoute,
		private router: Router,
		public dialog: MatDialog,
        private principal: Principal,
        private authService: AuthServerProvider,
		public _DomSanitizer: DomSanitizer,
		public googleAnalyticsService: GoogleAnalyticsEventsService,
		private loaderService: LoaderService,
		private signinModalService: SigninModalService,
		@Inject(WINDOW) private window: Window,
		public snackBar: MatSnackBar,
		private uppyService: UppyService,
        private csrfService: CSRFService,
        private el: ElementRef,
        private stateService: StateStorageService,
		private cookieService: CookieService,
        private account: AccountService,
		private viewportScroller: ViewportScroller
	) {
        this.routerOutletRedirect = '';
	//	this.domSanitizer = DomSanitizer;
		this.formSubmitted = false;
		// this.uppyEvent
		// 	.takeUntil(this.onDestroy$)
		// 	.filter(([ev]) => contains(ev, ['file-added']))
		// 	.subscribe(
		// 		([ev, data1, data2, data3]) => {
		// 			/* console.log("Received '" + ev + "' event from instance 1", 'Upload complete');
		// 			console.log(data1);
		// 			console.log(data2);
		// 			console.log(data3);
		// 			console.log(ev); */
		// 		},
		// 		err => console.log(err),
		// 		() => console.log('done')
		// 	);

	}
	openUppyDialog(events:any){
		//this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        const dialogRef = this.dialog.open(UppyComponent, {
			width: '500px',
			height: '3000px'
		});
		// dialogRef.afterClosed().subscribe(result => {
		// 	console.log('The dialog was closed');
		//   });
	}
	ngOnInit() {
		/* this.router.events
		.pipe(filter((routerEvent: Event) => routerEvent instanceof NavigationEnd))
		.subscribe(() => window.scrollTo(0, 0)); */

		_that = this;
        this.csrf = this.csrfService.getCSRF();
        this.filterService.setAuthorisedUrl();
		this.loaderService.display(true);
		this.isLoading = true;
		this.isPendingUnlock = false;
		this.isPendingExpand = false;
		this.disableTable = false;
		this.tabIndex = 0;
		this.basicExpansionPanel = true;
		this.companyExpansionPanel = false;
		this.adminExpansionPanel= false;
		this.descriptionsExpansionPanel = false;
		this.categorizationsExpansionPanel = false;
		this.investigationExpansionPanel = false;
		this.relatedProvidersExpansionPanel = true;
		this.dataDetailsExpansionPanel = false;
		this.dataQualityExpansionPanel = false;
		this.dataDeliveryExpansionPanel = false;
		this.triallingExpansionPanel = false;
		this.licensingExpansionPanel = false;
        this.pricingExpansionPanel = false;
        this.resourceProvidersExpansionPanel = true;
		this.route.queryParams.subscribe(params => {
			if (params['pending-unlock']) {
				this.isPendingUnlock = true;
			}

			if (params['pending-expand']) {
				this.isPendingExpand = true;
			}
		});
		this.providerPartnersCount = 0;
        this.distributionPartnersCount = 0;
        this.consumerPartnersCount = 0;
        this.competitorsCount = 0;
		this.subscription = this.route.params.subscribe(params => {
			this.recordID = params['recordID'];
            this.account.getProviderEdit(params['recordID']).subscribe(data => {
				if (data && !data.recordUpdated) {
					this.pendingEdits = false;
				} else {
					this.pendingEdits = true;
                    this.loadEditedData();
                    this.dataProviderService.recordUpdated$.next(true);
				}
			}, error => {

			});
			//this.load(params['recordID']);
        });

        // this.uppyPlugins = [
        //     ['Dashboard', {
        //         target: '.DashboardContainer',
        //         replaceTargetContent: true,
        //         inline: true,
        //         restrictions: {
        //             maxFileSize: 200000000,
        //             maxNumberOfFiles: 10,
        //             minNumberOfFiles: 1,
        //             allowedFileTypes: ['image/*', 'video/*']
        //         }
        //     }],
        //     ['XHRUpload', {
        //         endpoint: '/api/ur/multiple-file-upload?providerId=' + this.recordID,
        //         formData: true,
        //         bundle: true,
        //         fieldName: 'fileUploads'
        //     }]/* ,
        //     ['GoogleDrive', {
        //         target: 'Dashboard',
        //         serverUrl: 'https://companion.uppy.io'
        //     }],
        //     ['Dropbox', { target: 'Dashboard', serverUrl: 'https://companion.uppy.io' }] */
        // ];
		this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
		this.relevantArticlePage = 0;
		this.relevantArticleLinks = {
			last: 0
		};
		this.authoredArticlePage = 0;
		this.authoredArticleLinks = {
			last: 0
		};
		this.mdTooltipDelay = 500;
        this.principal.inactivityFormSubmit$.subscribe(result => {
            console.log('Result Admin Form 1', result);
            if (result === true) {
                this.forceFormSubmit = true;
                this.formSubmitted = true;
                this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
            }
        });
		// this.providerTags = [];

        // this.filterService.getDataProviderPublicTagUrl().subscribe(data => {
        //     const listItems = data;
		// 	for (let i = 0; i < listItems.length; i++) {
        //         const item = listItems[i];
        //             this.providerTags.push({
        //                 value: {
        //                     id: listItems[i].id,
        //                     //desc: listItems[i].providerTag,
        //                     //locked: listItems[i].locked ? listItems[i].locked : false
        //                 },
        //                 label: listItems[i].providerTag
        //             });
		// 	}
		// 	//console.log('tags',this.providerTags);
		// });
		this.mainDataIndustriesArr = [];
		this.filterService.getMainDataIndustry().subscribe(data => {
			const listItems = (data['aggregations'] && data['aggregations'].dataindustries) ? data['aggregations'].dataindustries.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i].mainDataIndustry.hits.hits[0]._source.mainDataIndustry;

				this.mainDataIndustriesArr.push({
					id: listItems[i].key,
					dataIndustry: item.dataIndustry
				});
			}
		});

		// this.dataIndustries = [];
		// this.filterService.getRelatedDataIndustry().subscribe(data => {
		// 	const listItems = data ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		if (listItems[i]._source.dataIndustry !== '' && listItems[i]._source.dataIndustry !== 'DATA_INDUSTRY_NULL') {
		// 			this.dataIndustries.push({
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.dataIndustry
		// 			});
		// 		}
		// 	}
		// });

		this.mainDataCategories = [];
		this.filterService.getAllDataCategories().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i]._source;
				if (item.datacategory !== '' && item.datacategory !== 'DATA_CATEGORY_NULL') {
					this.mainDataCategories.push({
						id: item.id,
						datacategory: item.datacategory,
						img: item.logo ? item.logo.logo : null
					});
				}
			}
		});

		// this.dataCategories = [];
		// this.filterService.getAllDataCategories().subscribe(data => {
		// 	const listItems = data ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		if (listItems[i]._source.datacategory !== '' && listItems[i]._source.datacategory !== 'DATA_CATEGORY_NULL') {
		// 			this.dataCategories.push({
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.datacategory,
		// 				locked: listItems[i]._source.locked,
		// 				img: listItems[i]._source.logo ? listItems[i]._source.logo.logo : null
		// 			});
		// 		}
		// 	}
		// });

		this.dataFeatures = [];
		this.filterService.getDataFeatures().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.dataFeatures.push({
					id: listItems[i]._source.id,
					desc: listItems[i]._source.dataFeature
				});
			}
        });
        // this.dataLinks = [];
		// this.filterService.getDataLinks().subscribe(data => {
		// 	const listItems = data ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.dataLinks.push({
		// 			id: listItems[i]._source.id,
		// 			link: listItems[i]._source.link
        //         });
        //         //console.log('...datalinks...', this.dataLinks);
        //     }
        //     this.dataResourceFieldsLink[0].templateOptions.options = this.dataLinks;
		// });

		// this.pTypesArr = [];
		// this.dataProviderService.getProvidersTypes().subscribe((data: any[]) => {
		// 	console.log('Provider Types', data);
		// 	const providerTypes = data;
		// 	// const pTypesArr = [];
		// 	providerTypes.map((val, index) => {
		// 		console.log(val);
		// 		this.pTypesArr.push({
		// 			value: val.id,
		// 			label: val.providerType
		// 		});
		// 	});
		//     this.categorizationsFields[1].templateOptions.options = this.pTypesArr;
		// });

		// this.dataSources = [];
		// this.filterService.getDataSource().subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.dataSources.push({
		// 			value: {
		// 				id: listItems[i]._id,
		// 				//desc: listItems[i]._source.dataSource,
		// 				//locked: listItems[i]._source.locked ? listItems[i]._source.locked : false
		// 			},
		// 			label: listItems[i]._source.dataSource
		// 		});
		// 	}
		// //	this.categorizationsFields[7].templateOptions.options = this.dataSources;
		// });

		this.providerNumberOfDataSources = [];
		this.filterService.getLookupCodes('DATA_SOURCES_RANGE').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// this.providerNumberOfDataSources.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerNumberOfDataSources.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			}
		//	this.categorizationsFields[8].templateOptions.options = this.providerNumberOfDataSources;
		});

		this.geographicalFocus = [];
		this.filterService.getGeographicFocus().subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.geographicalFocus.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
        //   this.categorizationsFields[10].templateOptions.options = this.geographicalFocus;
		});

		// this.providerAccessOffered = [];
		// this.filterService.getLookupCodes('PROVIDER_ACCESS').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerAccessOffered.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}
		// 	this.dataDetailsFields[1].templateOptions.options = this.providerAccessOffered;
		// });

		// this.providerDataProductTypes = [];
		// this.filterService.getLookupCodes('DATA_PRODUCT_TYPE').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerDataProductTypes.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}
		// 	this.dataDetailsFields[2].templateOptions.options = this.providerDataProductTypes;
		// });

		// this.providerDataTypes = [];
		// this.filterService.getLookupCodes('DATA_TYPE').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerDataTypes.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}
		// 	this.dataDetailsFields[3].templateOptions.options = this.providerDataTypes;
		// });

		// this.providerDataLanguagesAvailable = [];
		// this.filterService.getLookupCodes('LANGUAGE').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerDataLanguagesAvailable.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}
		// 	this.dataDetailsFields[4].templateOptions.options = this.providerDataLanguagesAvailable;

		// 	if (this.dataDetailsFields[4] && this.dataDetailsFields[4].model) {
		// 		if (Array.isArray(this.dataDetailsFields[4].model['languagesAvailable']) || this.dataDetailsFields[4].model['languagesAvailable']  === undefined) {
		// 			this.dataDetailsFields[4].model['languagesAvailable'] = [];
		// 			this.dataDetailsFields[4].model['languagesAvailable'].push( this.providerDataLanguagesAvailable[0].value);
		// 		} else {
		// 			this.dataDetailsFields[4].model['languagesAvailable']
		// 		}
		// 	}
		// });

		// this.providerDatasetSize = [];
		// this.filterService.getLookupCodes('DATA_SIZE_RANGE').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	/*for (let i = 0; i < listItems.length; i++) {
		// 		this.providerDatasetSize.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}*/
		// 	const datasetSize = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	datasetSize.map((val, index) => {
        //         //console.log('......pointer mapping', val);
		// 		this.providerDatasetSize.push({
		// 			value: val._source.description,
		// 			label: val._source.description
        //         });
		// 	});
		// 	this.dataDetailsFields[5].templateOptions.options = this.providerDatasetSize;
		// });

		// this.providerDatasetNumberOfRows = [];
		// this.filterService.getLookupCodes('ROWS_RANGE').subscribe(data => {
		// 	/*const listItems = data ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerDatasetNumberOfRows.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}*/
		// 	const datasetNumberOfRows = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	datasetNumberOfRows.map((val, index) => {
        //         //console.log('......pointer mapping', val);
		// 		this.providerDatasetNumberOfRows.push({
		// 			value: val._source.description,
		// 			label: val._source.description
        //         });
		// 	});
		// 	this.dataDetailsFields[6].templateOptions.options = this.providerDatasetNumberOfRows;
		// });

		// this.providerPointInTimeAccuracy = [];
		// this.filterService.getLookupCodes('YES_OR_NO_SOFT').subscribe(data => {
		// 	//const listItems = data ? data['hits']['hits'] : [];
		// 	/*for (let i = 0; i < listItems.length; i++) {
		// 		this.providerPointInTimeAccuracy.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}*/
		// 	const pointInTimeAccuracy = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	// const providerPointInTime = [];
		// 	pointInTimeAccuracy.map((val, index) => {
        //        // console.log('......pointer mapping', val);
		// 		this.providerPointInTimeAccuracy.push({
		// 			value: val._source.id,
		// 			label: val._source.description
        //         });
		// 	});
		// 	this.dataQualityFields[0].templateOptions.options = this.providerPointInTimeAccuracy;
		// });

		// this.providerDataGaps = [];
		// this.filterService.getLookupCodes('YES_NO_UNKNOWN').subscribe(data => {
		// 	// const listItems = data ? data['hits']['hits'] : [];
		// 	/*for (let i = 0; i < listItems.length; i++) {
		// 		this.providerDataGaps.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}*/
		// 	const dataGaps = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	// const providerPointInTime = [];
		// 	dataGaps.map((val, index) => {
        //       //  console.log('......DataGaps mapping', val);
		// 		this.providerDataGaps.push({
		// 			value: val._source.id,
		// 			label: val._source.description
        //         });
		// 	});
		// 	this.dataQualityFields[1].templateOptions.options = this.providerDataGaps;
		// });

		// this.providerDataGapsReasons = [];
		// this.filterService.getLookupCodes('DATA_GAPS_REASON').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerDataGapsReasons.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}
		// 	this.dataQualityFields[2].templateOptions.options = this.providerDataGapsReasons;
		// });

		// this.providerDuplicatesCleaned = [];
		// this.filterService.getLookupCodes('YES_NO_UNKNOWN').subscribe(data => {
		// 	/*const listItems = data ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerDuplicatesCleaned.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}*/
		// 	const duplicatesCleaned = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	duplicatesCleaned.map((val, index) => {
        //         //console.log('......pointer mapping', val);
		// 		this.providerDuplicatesCleaned.push({
		// 			value: val._source.id,
		// 			label: val._source.description
        //         });
		// 	});
		// 	this.dataQualityFields[3].templateOptions.options = this.providerDuplicatesCleaned;
		// });

		// this.providerIncludesOutliers = [];
		// this.filterService.getLookupCodes('YES_NO_UNKNOWN').subscribe(data => {
		// 	/*const listItems = data ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerIncludesOutliers.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}*/
		// 	const includesOutliers = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	includesOutliers.map((val, index) => {
        //         //console.log('......pointer mapping', val);
		// 		this.providerIncludesOutliers.push({
		// 			value: val._source.id,
		// 			label: val._source.description
        //         });
		// 	});
		// 	this.dataQualityFields[4].templateOptions.options = this.providerIncludesOutliers;
		// });

		// this.providerOutlierReasons = [];
		// this.filterService.getLookupCodes('OUTLIER_REASON').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerOutlierReasons.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}
		// 	this.dataQualityFields[5].templateOptions.options = this.providerOutlierReasons;
		// });

		// this.providerBiases = [];
		// this.filterService.getLookupCodes('BIASES').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerBiases.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}
		// 	this.dataQualityFields[6].templateOptions.options = this.providerBiases;
		// });

		// this.providerNormalized = [];
		// this.filterService.getLookupCodes('YES_NO_UNKNOWN').subscribe(data => {
		// 	/*const listItems = data ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerNormalized.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}*/
		// 	const normalized = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	normalized.map((val, index) => {
        //         //console.log('......pointer mapping', val);
		// 		this.providerNormalized.push({
		// 			value: val._source.id,
		// 			label: val._source.description
        //         });
		// 	});
		// 	this.dataQualityFields[8].templateOptions.options = this.providerNormalized;
		// });

		// this.providerTrialPricingModel = [];
		// this.filterService.getLookupCodes('PRICING_MODEL').subscribe(data => {
		// 	/*const listItems = data ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerTrialPricingModel.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}*/
		// 	const trialPricingModel = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	trialPricingModel.map((val, index) => {
        //         //console.log('......pointer mapping', val);
		// 		this.providerTrialPricingModel.push({
		// 			value: val._source.id,
		// 			label: val._source.description
        //         });
		// 	});
		// 	this.triallingFields[1].templateOptions.options = this.providerTrialPricingModel;
		// });

		// this.providerTrialAgreementType = [];
		// this.filterService.getLookupCodes('AGREEMENT_TYPE').subscribe(data => {
		// 	/*const listItems = data ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerTrialAgreementType.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}*/
		// 	const trialAgreementType = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	trialAgreementType.map((val, index) => {
        //         //console.log('......pointer mapping', val);
		// 		this.providerTrialAgreementType.push({
		// 			value: val._source.id,
		// 			label: val._source.description
        //         });
		// 	});
		// 	this.triallingFields[3].templateOptions.options = this.providerTrialAgreementType;
		// });

		this.providerDataLicenseType = [];
		this.filterService.getLookupCodes('LICENSE_TYPE').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// this.providerDataLicenseType.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerDataLicenseType.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			}
			this.licensingFields[0].templateOptions.options = this.providerDataLicenseType;
		});

		this.providerPaymentMethodsOffered = [];
		this.filterService.getLookupCodes('PAYMENT_METHOD').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerPaymentMethodsOffered.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.licensingFields[4].templateOptions.options = this.providerPaymentMethodsOffered;
		});

		this.providerPrivacy = [];
		this.filterService.getLookupCodes('PRIVACY').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			console.log('..privacy...', listItems);
			// this.providerPrivacy.push({label : 'Select', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerPrivacy.push({
				/* 	value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					}, */
					label: listItems[i]._source.lookupcode,
					value: listItems[i]._source.id
				});
			}
			this.adminFields[1].templateOptions.options = this.providerPrivacy;
		});

		this.providerMarketplaceStatus = [];
		this.filterService.getLookupCodes('MARKETPLACE_STATUS').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// this.providerMarketplaceStatus.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerMarketplaceStatus.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
            }
            this.adminFields[2].templateOptions.options = this.providerMarketplaceStatus;
		});

		this.providerResearchMethods = [];
		this.filterService.getLookupCodes('RESEARCH_METHOD').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerResearchMethods.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			//console.log('researchmethodscalled',this.providerResearchMethods);
			this.adminFields[3].templateOptions.options = this.providerResearchMethods;
		});

		this.providerIEIStatus = [];
		this.filterService.getLookupCodes('IEI_STATUS').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// this.providerIEIStatus.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerIEIStatus.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
            }
            this.adminFields[4].templateOptions.options = this.providerIEIStatus;
		});

		this.providerPriority = [];
		this.filterService.getLookupCodes('PRIORITY').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// this.providerPriority.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerPriority.push({
					value: listItems[i]._source.lookupcode,
					label: listItems[i]._source.lookupcode
				});
            }
            this.adminFields[5].templateOptions.options = this.providerPriority;
		});

		// this.providerSubscriptionModel = [];
		// this.filterService.getLookupCodes('SUBSCRIPTION_MODEL').subscribe(data => {
		// 	/*const listItems = data ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerSubscriptionModel.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}*/
		// 	const subscriptionModel = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	subscriptionModel.map((val, index) => {
        //         console.log('......pointer mapping', val);
		// 		this.providerSubscriptionModel.push({
		// 			value: val._source.id,
		// 			label: val._source.description
        //         });
		// 	});
        //     console.log('ProviderSubscriptionModel', this.providerSubscriptionModel);
		// 	this.pricingFields[1].templateOptions.options = this.providerSubscriptionModel;
		// });

		// this.providerSubscriptionTimePeriod = [];
		// this.filterService.getLookupCodes('TIME_PERIOD').subscribe(data => {
		// 	/*const listItems = data ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerSubscriptionTimePeriod.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}*/
		// 	const subscriptionTimePeriod = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	subscriptionTimePeriod.map((val, index) => {
        //         //console.log('......pointer mapping', val);
		// 		this.providerSubscriptionTimePeriod.push({
		// 			value: val._source.id,
		// 			label: val._source.description
        //         });
		// 	});
		// 	this.pricingFields[2].templateOptions.options = this.providerSubscriptionTimePeriod;
		// });

		// this.providerMonthlyPriceRange = [];
		// this.filterService.getLookupCodes('MONTHLY_PRICE_RANGE').subscribe(data => {
		// 	/*const listItems = data ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerMonthlyPriceRange.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}*/
		// 	const monthlyPriceRange = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	monthlyPriceRange.map((val, index) => {
        //         //console.log('......pointer mapping', val);
		// 		this.providerMonthlyPriceRange.push({
		// 			value: val._source.id,
		// 			label: val._source.description
        //         });
		// 	});
		// 	this.pricingFields[3].templateOptions.options = this.providerMonthlyPriceRange;
		// });
		// this.providerPricingModel = [];
		// this.filterService.getLookupCodes('PRICING_MODEL').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerPricingModel.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}
		// 	/* const pricingModel = data ? data['hits']['hits'] : [];
		// 	pricingModel.map((val, index) => {
        //         //console.log('......pointer mapping', val);
		// 		this.providerPricingModel.push({
		// 			value: val._source.id,
		// 			label: val._source.description
        //         });
		// 	}); */
		// 	this.pricingFields[0].templateOptions.options = this.providerPricingModel;
		// });
		// this.providerIdentifiersAvailable = [];
		// this.filterService.getLookupCodes('IDENTIFIERS').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerIdentifiersAvailable.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}
		// 	this.investigationFields[24].templateOptions.options = this.providerIdentifiersAvailable;
		// });

		// this.providerWillingness = [];
		// this.filterService.getWillingnessToProvideToInvestors().subscribe(data => {
        //     const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	this.providerWillingness.push({label : 'Select', value: null });
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		if (listItems[i]._source.description !== '') {
		// 			this.providerWillingness.push({
		// 				value: listItems[i]._source.id,
		// 				label: listItems[i]._source.description
		// 			});
		// 		}
        //     }
		// 	this.investigationFields[0].templateOptions.options = this.providerWillingness;
        // });
        // this.providerUniquness = [];
		// this.filterService.getUniqueness().subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	this.providerUniquness.push({label : 'Select', value: null });
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		if (listItems[i]._source.description !== '') {
		// 			this.providerUniquness.push({
		// 				value: listItems[i]._source.id,
		// 				label: listItems[i]._source.description
		// 			});
		// 		}
		// 	}
		// 	this.investigationFields[1].templateOptions.options = this.providerUniquness;
        // });
        // this.providerReadiness = [];
		// this.filterService.getReadiness().subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	this.providerReadiness.push({label : 'Select', value: null });
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		if (listItems[i]._source.description !== '') {
		// 			this.providerReadiness.push({
		// 				value: listItems[i]._source.id,
		// 				label: listItems[i]._source.description
		// 			});
		// 		}
		// 	}
		// 	this.investigationFields[3].templateOptions.options = this.providerReadiness;
        // });
        // this.providerValue = [];
		// this.filterService.getValue().subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	this.providerValue.push({label : 'Select', value: null });
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		if (listItems[i]._source.description !== '') {
		// 			this.providerValue.push({
		// 				value: listItems[i]._source.id,
		// 				label: listItems[i]._source.description
		// 			});
		// 		}
		// 	}
		// 	this.investigationFields[2].templateOptions.options = this.providerValue;
        // });
        // this.providerOverall = [];
		// this.filterService.getOverall().subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	this.providerOverall.push({label : 'Select', value: null });
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		if (listItems[i]._source.description !== '') {
		// 			this.providerOverall.push({
		// 				value: listItems[i]._source.id,
		// 				label: listItems[i]._source.description
		// 			});
		// 		}
		// 	}
		// 	this.investigationFields[4].templateOptions.options = this.providerOverall;
		// });
		// this.providerSecRegulated = [];
		// this.filterService.getLookupCodes('YES_OR_NO').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	this.providerSecRegulated.push({label : 'Select', value: null });
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerSecRegulated.push({
		// 			// value: {
		// 			// 	id: listItems[i]._source.id,
		// 			// 	desc: listItems[i]._source.lookupcode
		// 			// },
		// 			value: listItems[i]._source.id,
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}
		// 	this.investigationFields[5].templateOptions.options = this.providerSecRegulated;
		// });
		// this.providerRelevantInvestors = [];
		// this.filterService.getInvestorType().subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerRelevantInvestors.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				description: listItems[i]._source.lookupcode
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}

		// 	this.investigationFields[11].templateOptions.options = this.providerRelevantInvestors;
		// });
		// // this.providerMainAssetClass = [];
		// // this.filterService.getMainAssetClass().subscribe(data => {
		// // 	const listItems = (data['aggregations'] && data['aggregations'].mainAssetclass
		// // ) ? data['aggregations'].mainAssetclass.buckets : [];
		// // this.providerMainAssetClass.push({label : 'Select', value: null });
		// // 	for (let i = 0; i < listItems.length; ++i) {
		// // 		const item = listItems[i].mainAssetClass.hits.hits[0]._source.mainAssetClass;
		// // 		this.providerMainAssetClass.push({
		// // 			value: listItems[i].key,
		// // 			label: item.description
		// // 		});
		// // 	}
		// // 	this.investigationFields[17].templateOptions.options = this.providerMainAssetClass;
		// // });

		// this.providerMainAssetClass = [];
		// this.filterService.getLookupCodes('ASSET_CLASS').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	this.providerMainAssetClass.push({label : 'Select', value: null });
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerMainAssetClass.push({
		// 			value: listItems[i]._source.description,
		// 			label: listItems[i]._source.description
		// 		});
		// 	}
		// 	this.investigationFields[17].templateOptions.options = this.providerMainAssetClass;
		// });

		// this.providerRelevantAssetClass = [];
		// this.filterService.getRelatedAssetClass().subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	//console.log('List Items',listItems);
		// 	//listItems.shift();
		// 	//console.log('List Items 1',listItems);
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		//console.log('4',this.providerRelevantAssetClass[0]);
		// 		//this.providerRelevantAssetClass.shift();
		// 		if(listItems[i]._source.description !== 'DEFAULT_NULL') {
		// 			this.providerRelevantAssetClass.push({
		// 				value: {
		// 					id: listItems[i]._source.id,
		// 					desc: listItems[i]._source.description
		// 				},
		// 				label: listItems[i]._source.description
		// 			});
		// 		}
		// 	}
		// 	this.investigationFields[18].templateOptions.options = this.providerRelevantAssetClass;
		// });

		// this.relevantSecurityTypes = [];
		// this.filterService.getLookupCodes('RLVNT_SECURITY_TYPE').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.relevantSecurityTypes.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				//desc: listItems[i]._source.lookupcode,
		// 				//locked: listItems[i]._source.locked ? listItems[i]._source : false
		// 			},
		// 			label: listItems[i]._source.lookupcode
		// 		});
		// 	}
		// 	this.investigationFields[19].templateOptions.options = this.relevantSecurityTypes;
		// });

		// this.relevantSectors = [];
		// this.filterService.getLookupCodes('RLVNT_SECTOR').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.relevantSectors.push({
		// 			value: {
		// 				id: listItems[i]._source.id,
		// 				//desc: listItems[i]._source.description,
		// 				//locked: listItems[i]._source.locked ? listItems[i]._source : false
		// 			},
		// 			label: listItems[i]._source.description
		// 		});
		// 	}
		// 	this.investigationFields[20].templateOptions.options = this.relevantSectors;
		// });
        // this.providerpublicEquitiesCoveredRange = [];
		// this.filterService.getLookupCodes('COVERAGE_RANGE').subscribe(data => {
		// 	const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	for (let i = 0; i < listItems.length; i++) {
		// 		this.providerpublicEquitiesCoveredRange.push({
		// 		/* 	value: {
		// 				id: listItems[i]._source.id,
		// 				desc: listItems[i]._source.lookupcode
		// 			}, */
		// 			label: listItems[i]._source.lookupcode,
		// 			value: listItems[i]._source.description
		// 		});
		// 	}
		// 	this.investigationFields[21].templateOptions.options = this.providerpublicEquitiesCoveredRange;
		// });

        this.deliveryMethods = [];
		this.filterService.getDeliveryMethods().subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.deliveryMethods.push({
					value: {
						id: listItems[i]._source.id,
					    desc: listItems[i]._source.lookupcode,
					//	locked: listItems[i]._source.locked ? listItems[i]._source.locked : false
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDeliveryFields[0].templateOptions.options = this.deliveryMethods;
		});

		this.deliveryFormats = [];
		this.filterService.getDeliveryFormat().subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.deliveryFormats.push({
					value: {
						id: listItems[i]._source.id,
					 	desc: listItems[i]._source.lookupcode,
					//	locked: listItems[i]._source.locked ? listItems[i]._source.locked : false
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDeliveryFields[2].templateOptions.options = this.deliveryFormats;
		});

		this.deliveryFrequencies = [];
		this.filterService.deliveryFrequency().subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.deliveryFrequencies.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDeliveryFields[4].templateOptions.options = this.deliveryFrequencies;
		});

		this.dataUpdateFrequency = [];
		this.filterService.getLookupCodes('DATA_UPDATE_FREQUENCY').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// this.dataUpdateFrequency.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.dataUpdateFrequency.push({
					value: listItems[i]._source.lookupcode,
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDeliveryFields[10].templateOptions.options = this.dataUpdateFrequency;
		});

		this.dataProviderService.getProvidersTypes().subscribe((data: any[]) => {
			const providerTypes = data;
			const pTypesArr = [];
			providerTypes.map((val, index) => {
				pTypesArr.push({
					value: val.id,
					label: val.providerType
				});
			});
			this.dataProviderTypes = pTypesArr;
			this.filterService.getDataProviderPublicTagUrl().subscribe(data => {
				const listItems = data;
				for (let i = 0; i < listItems.length; i++) {
					const item = listItems[i];
					if (item.privacy === 'Public' && item.type === 'Tag') {
						this.providerTags.push({
							value: {
								id: listItems[i].id,
								desc: listItems[i].providerTag,
								//locked: listItems[i].locked ? listItems[i].locked : false
							},
							label: listItems[i].providerTag
						});
					}
				}

                this.dataSources = [];
				this.filterService.getDataSource().subscribe((data: any[]) => {
					const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
					for (let i = 0; i < listItems.length; i++) {
						this.dataSources.push({
							value: {
								id: listItems[i]._source.id,
								desc: listItems[i]._source.dataSource,
								//locked: listItems[i]._source.locked ? listItems[i]._source.locked : false
							},
							label: listItems[i]._source.dataSource
						});
					}
				//	this.categorizationsFields[7].templateOptions.options = this.dataSources;

				this.categorizationsFields = [
					{
						key: 'inactive',
						type: 'checkbox',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Inactive:',
							description: 'Does this data provider still operate?',
							attributes:{
                                'multiple': 'false',
								'labelTooltip':'See Confluence Page titled "How to Handle Inactive/Acquired/Merged Companies"',
								'descriptionTooltip': 'See Confluence Page titled "How to Handle Inactive/Acquired/Merged Companies"'
							},
                            change: (event) => {
                                // console.log(event, event['modelKey']);
                                this.basicModel.inactive = event['modelKey'];
                                //console.log(this.basicModel.inactive, this.basicModel);
                            }
						},
					},
					{
						key: 'providerType.id',
						type: 'radioButton',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Provider Type:',
							description: 'How does the provider handle data and what does the provider do with it? Is the provider creating their own analytics, strictly acting as a pass-through data provider, or just collecting data? In other words, at what step in the data monetization process does the provider sit?',
							required: true,
							options: pTypesArr,
							attributes: {
								'labelTooltip': 'Definitions of the types of data providers:""Aggregator"": a company that collects data from multiple different sources or internally, but is not in the habit of providing it to external partners/customers for reasons aside from their core business (if that\'s outside of being a data provider).""Provider"": a company that is primarily in the business of collecting data and providing that data in a fairly raw format to partners. So basically, a company that does not do much analysis on the data that they are selling, such as a data marketplace or financial data provider.""Analyzer"": a company that is already in the business of analyzing the data they collect internally or from 3rd parties and provided that analyzed information to other parties. So basically, a company that is in the business of analyzing and reformatting the data they collect and sell, such as an Investment Research company or a company that sells reports or data visualizations.""Investor"": a company that uses specific data to make their investments.Most companies are providers and analyzers.',
								'descriptionTooltip': 'Definitions of the types of data providers:""Aggregator"": a company that collects data from multiple different sources or internally, but is not in the habit of providing it to external partners/customers for reasons aside from their core business (if that\'s outside of being a data provider).""Provider"": a company that is primarily in the business of collecting data and providing that data in a fairly raw format to partners. So basically, a company that does not do much analysis on the data that they are selling, such as a data marketplace or financial data provider.""Analyzer"": a company that is already in the business of analyzing the data they collect internally or from 3rd parties and provided that analyzed information to other parties. So basically, a company that is in the business of analyzing and reformatting the data they collect and sell, such as an Investment Research company or a company that sells reports or data visualizations.""Investor"": a company that uses specific data to make their investments.Most companies are providers and analyzers.'
							}
						},
					},
                    {
                        key: 'providerTypeEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
					/*------------------- newwly added dropdown - previously autocomplete------------------------- */
					{
						key: 'mainDataIndustry',
						type: 'autocomplete',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Main Data Industry:',
							type: 'autocomplete',
							placeholder: '',
							// description: 'What is the primary industry you provide data to?',
							required: true,
							attributes: {
								field: 'dataIndustry',
								'dropdown': 'true',
								'minLength': '1',
								'placeholderText' : 'Primary industry that the data provider operates in and provides data to.',
								'labelTooltip': 'See "Data Industries" table for a definition of each data industry.Some providers will belong in more than one industry, e.g. marketing and retail - choose the primary industry that it provides data to, and add the primary and all other industries to the Industries column.If the company provides data for use in investing, Investment Management is the correct industry.In the past, this column was used a different type of categorization, not an industry, so some of the industries may need to be completely changed or moved to a different industry. If you think this is the case, contact Jordan [Administrator].',
								'placeholderTooltip': 'See "Data Industries" table for a definition of each data industry.Some providers will belong in more than one industry, e.g. marketing and retail - choose the primary industry that it provides data to, and add the primary and all other industries to the Industries column.If the company provides data for use in investing, Investment Management is the correct industry.In the past, this column was used a different type of categorization, not an industry, so some of the industries may need to be completely changed or moved to a different industry. If you think this is the case, contact Jordan [Administrator].'
							},
							options: this.mainDataIndustriesArr,
							change: (event: any) => {
								const queryObj = event;
								this.mainDataIndustriesArr = [];

								this.filterService.getMainDataIndustry(queryObj.query).subscribe(d => {
									const listArray = [];
									const listItems = (d['aggregations'] && d['aggregations'].dataindustries) ? d['aggregations'].dataindustries.buckets : [];
									for (let i = 0; i < listItems.length; ++i) {
										const item = listItems[i].mainDataIndustry.hits.hits[0]._source.mainDataIndustry;
										if (item.dataIndustry !== 'DATA_INDUSTRY_NULL') {
											listArray.push({
												id: listItems[i].key,
												dataIndustry: item.dataIndustry
											});
										}
									}
									this.mainDataIndustriesArr = [...listArray];
									setTimeout(() => {
										this.categorizationsFields[3].templateOptions.options = this.mainDataIndustriesArr;
									}, 2000);
								});
							},
							blur: (event: any) => {
								if (event && event.target && !event['selectedVal']) {
									event.target.value = '';
								}
							}
						},
					},
                    {
                        key: 'mainDataIndustryEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
					{
						key: 'industries',
						type: 'autocomplete',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Data Industries:',
							placeholder: '',
							description: '',
							options: this.dataIndustries,
							required: true,
							attributes: {
								field: 'desc',
								'minLength': '1',
								'placeholderText' : 'All the industries that the provider operates in and provides data to.',
								'labelTooltip': 'See "Data Industries" table for a definition of each data industry.Always make sure the Main Industry is included in here.Be sure to add ""Investment Management"" if the company provides data for use in investing.Use the company website for this. Often the website will have a list of the sectors or industries that they serve. These should all be recorded.If the webpage does not clearly tell you which industries are served, just add the main data industry and move on',
								'placeholderTooltip': 'See "Data Industries" table for a definition of each data industry.Always make sure the Main Industry is included in here. Be sure to add ""Investment Management"" if the company provides data for use in investing.Use the company website for this. Often the website will have a list of the sectors or industries that they serve. These should all be recorded. If the webpage does not clearly tell you which industries are served, just add the main data industry and move on.',
								'multiple': 'true',
								'dropdown': 'true'
							},
							change: (event: any) => {
								const queryObj = event;
								this.dataIndustries = [];

								const selected = this.basicModel['industries'];
								if(selected && selected.length) {
									this.selectedValue = selected.map(item => parseInt(item.id, 10));
								}
								if(queryObj.query)
								{
									this.filterService.getRelatedDataIndustry(queryObj.query).subscribe(d => {
										const listItems = d ? d['hits']['hits'] : [];
										for (let i = 0; i < listItems.length; i++) {
											const item = listItems[i]._source;
											if (listItems[i]._source.dataIndustry !== '' && listItems[i]._source.dataIndustry !== 'DATA_INDUSTRY_NULL') {
												if (!this.selectedValue.includes(item.id)) {
													this.dataIndustries.push({
														id: item.id,
														desc: item.dataIndustry
													});
												}
											}
										}
										//console.log(this.dataIndustries);
										setTimeout(() => {
											this.categorizationsFields[5].templateOptions.options = this.dataIndustries;
										}, 2500);
									});
							}
							else
							{
								this.filterService.getIndustriesForFilter().subscribe(data => {
								//	const listItems = d ? d['hits']['hits'] : [];
									const listItems = (data['aggregations'] && data['aggregations'].industries) ? data['aggregations'].industries.buckets : [];

									for (let i = 0; i < listItems.length; ++i) {
										const item = listItems[i];
										const industryId = parseInt(item.key, 10);
										const industryList = item.industries.hits.hits[0]._source.industries;
										const filteredIndustryDescription = industryList.filter( (items) => items.id === item.key);
										if (item.industries.hits.hits[0]._source.industries !== '' && item.industries.hits.hits[0]._source.industries !== 'DATA_INDUSTRY_NULL') {
											if (!this.selectedValue.includes(industryId)) {
												this.dataIndustries.push({
													id: industryId,
													desc: filteredIndustryDescription && filteredIndustryDescription[0] && filteredIndustryDescription[0].desc ? filteredIndustryDescription[0].desc : ''
												});
											}
										}
									}
									//console.log(this.dataIndustries);
									setTimeout(() => {
										this.categorizationsFields[5].templateOptions.options = this.dataIndustries;
									}, 2500);
								});
							}
							},
							blur: (event: any) => {
								if (event && event.target && !event['selectedVal']) {
									event.target.value = '';
								}
							}
						},
					},
                    {
                        key: 'industriesEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
					{
						key: 'mainDataCategory',
						type: 'autocomplete',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Main Data Category:',
							placeholder: '',
							description: '',
							required: true,
							options: this.mainDataCategories,
							attributes: {
								field: 'datacategory',
								'minLength': '1',
								'placeholderText' : 'The main type of data or information that the data provider collects/provides/analyzes.',
								'labelTooltip': 'See "Data Categories" table for a definition of each data category. This is the most important categorization to get correct. If you are unsure of what category to choose, please check with Jordan. The provider has usually built a core business within one particular type of data or business model before expanding to other things. Some providers will belong in more than one category - choose the primary data category, and add the primary and all other industries to the Data Categories column. Data Categories are meant to be an overarching classification that can encompass several Data Features and are meant to generally describe what the data is or can be used to understand more deeply.In the past, this column was used slightly differently, and so sometimes the Data Category is better suited as an Industry or Data Feature and the category should be more specific than the industry but less specific than the data feature. If you think this is the case, contact Jordan Hauer.',
								'placeholderTooltip': 'See " Data Categories" table for a definition of each data category. This is the most important categorization to get correct. If you are unsure of what category to choose, please check with Jordan. The provider has usually built a core business within one particular type of data or business model before expanding to other things. Some providers will belong in more than one category - choose the primary data category, and add the primary and all other industries to the Data Categories column. Data Categories are meant to be an overarching classification that can encompass several Data Features and are meant to generally describe what the data is or can be used to understand more deeply. In the past, this column was used slightly differently, and so sometimes the Data Category is better suited as an Industry or Data Feature and the category should be more specific than the industry but less specific than the data feature. If you think this is the case, contact Jordan Hauer.',
								'dropdown': 'true'
							},
							change: (event: any) => {
								const queryObj = event;
								this.mainDataCategories = [];
								if(queryObj.query)
								{
								this.filterService.getAllDataCategories(queryObj.query).subscribe(d => {
									const listItems = d && d['hits'] ? d['hits']['hits'] : [];
									for (let i = 0; i < listItems.length; ++i) {
										const item = listItems[i]._source;
										if (listItems[i]._source.datacategory !== '' && listItems[i]._source.datacategory !== 'DATA_CATEGORY_NULL') {
											this.mainDataCategories.push({
												id: item.id,
												datacategory: item.datacategory,
												img: item.logo ? item.logo.logo : null
											});
										}
									}
									setTimeout(() => {
										this.categorizationsFields[7].templateOptions.options = this.mainDataCategories;
									}, 2500);
								});
							}
							else
							{
								this.filterService.getAllDataCategorie().subscribe(d => {
									const listArray = [];
									const listItems = (d['aggregations'] && d['aggregations'].datacategories) ? d['aggregations'].datacategories.buckets : [];
									for (let i = 0; i < listItems.length; ++i) {
										const item = listItems[i].mainDataCategory.hits.hits[0]._source.mainDataCategory;
										if (item.datacategories !== 'DATA_CATEGORY_NULL') {
											listArray.push({
												id: listItems[i].key,
												datacategory: item.datacategory
											});
										}
									}
									this.mainDataCategories = [...listArray];
									setTimeout(() => {
										this.categorizationsFields[7].templateOptions.options = this.mainDataCategories;
									}, 2500);
								});
							}
							},
							blur: (event: any) => {
								if (event && event.target && !event['selectedVal']) {
									event.target.value = '';
								}
							}
						},
					},
                    {
                        key: 'mainDataCategoryEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
					{
						key: 'categories',
						type: 'autocomplete',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Data Categories:',
							placeholder: '',
							options: this.dataCategories,
							required: true,
							attributes: {
								field: 'desc',
								'minLength': '1',
								'placeholderText' : 'All the data categories this data provider collects/provides/analyzes.',
								'labelTooltip': 'See "Data Categories" table for a definition of each data category. Always make sure the Main Data Category is included in here. Try to get up to 7 most appropriate data categories for each company. Some companies may have less than this and some other may have many more. In such unusual cases, seek assistance.',
								'multiple': 'true',
								'template': 'true',
								'dropdown': 'true',
								'placeholderTooltip': 'See "Data Categories" table for a definition of each data category. Always make sure the Main Data Category is included in here. Try to get up to 7 most appropriate data categories for each company. Some companies may have less than this and some other may have many more. In such unusual cases, seek assistance.'
							},
							change: (event: any) => {
								const queryObj = event;
								this.dataCategories = [];

								const selected = this.basicModel['categories'];
								///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
								if(selected && selected.length) {
									this.selectedValue = selected.map(item => parseInt(item.id, 10));
								}
								if(queryObj.query)
								{
								this.filterService.getAllDataCategories(queryObj.query).subscribe(d => {
								const listItems = d ? d['hits']['hits'] : [];
								for (let i = 0; i < listItems.length; i++) {
									const item = listItems[i]._source;
									if (listItems[i]._source.datacategory !== '' && listItems[i]._source.datacategory !== 'DATA_CATEGORY_NULL') {
										if (!this.selectedValue.includes(item.id)) {
											this.dataCategories.push({
												id: item.id,
												desc: item.datacategory,
												img: item.logo ? item.logo.logo : null
											});
										}
									}
								}
								setTimeout(() => {
									this.categorizationsFields[9].templateOptions.options = this.dataCategories;
									}, 2500);
								});
							}
							else
							{
								this.filterService.getCategoriesForFilter().subscribe(data => {
									//const listItems = d ? d['hits']['hits'] : [];
									const listItems = (data['aggregations'] && data['aggregations'].categories) ? data['aggregations'].categories.buckets : [];
									for (let i = 0; i < listItems.length; i++) {
										//const item = listItems[i]._source;
										const category = listItems[i];
										const categoryId = parseInt(category.key, 10);
										const categoryList = category.categories.hits.hits[0]._source.categories;
										const filteredCategoryDescription = categoryList.filter( (item) => item.id === category.key);
										if (category.categories.hits.hits[0]._source.categories !== 'DEFAULT_NULL') {
											if (!this.selectedValue.includes(categoryId)) {
												this.dataCategories.push({
													id: categoryId,
													desc: filteredCategoryDescription && filteredCategoryDescription[0] && filteredCategoryDescription[0].desc ? filteredCategoryDescription[0].desc : '',
													img: categoryList.logo ? categoryList.logo.logo : null
												});
											}
										}
									}
									setTimeout(() => {
										this.categorizationsFields[9].templateOptions.options = this.dataCategories;
									}, 2500);
								});
							}
							},
							blur: (event: any) => {
								if (event && event.target && !event['selectedVal']) {
									event.target.value = '';
								}
							}
						},
					},
                    {
                        key: 'categoriesEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
					{
						key: 'features',
						type: 'autocomplete',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Data Features:',
							description: 'What is the specific data or information being collected or organized by this provider?',
							placeholder: '',
							options: this.dataFeatures,
							attributes: {
								field: 'desc',
								'minLength': '1',
								'placeholderText' : '',
								'labelTooltip': 'See "Data Features" table for a definition of each data feature. Use similar companies as reference for what to fill in here. Use the links between Data Features and Data Categories to help determine which Data Features to consider. The company (within providers and analyzers) often describes many of the data features provided. In short, this field is a more detailed version of a data category. If you think of a table or spreadsheet or any structured data (data with cleanly organized columns and rows), a data feature usually fits into one or a combination of only a few columns. Example: "Store Location" would be one or a few fields describing the address of a store (address, city, state, zip, etc.). "Product Ratings" is likely one columns with the number of stars a consumer rates a product, but it could also be a count of the number of ratings each product has. The important thing is that the data provider possesses information about product ratings.',
								'multiple': 'true',
								'dropdown': 'true',
								'descriptionTooltip': 'See "Data Features" table for a definition of each data feature.Use similar companies as reference for what to fill in here.Use the links between Data Features and Data Categories to help determine which Data Features to consider.The company (within providers and analyzers) often describes many of the data features provided.In short, this field is a more detailed version of a data category.If you think of a table or spreadsheet or any structured data (data with cleanly organized columns and rows), a data feature usually fits into one or a combination of only a few columns.Example: ""Store Location"" would be one or a few fields describing the address of a store (address, city, state, zip, etc.). ""Product Ratings"" is likely one columns with the number of stars a consumer rates a product, but it could also be a count of the number of ratings each product has. The important thing is that the data provider possesses information about product ratings.',
								'placeholderTooltip': 'See "Data Features"   table for a definition of each data feature. Use similar companies as reference for what to fill in here. Use the links between Data Features and Data Categories to help determine which Data Features to consider. The company (within providers and analyzers) often describes many of the data features provided. In short, this field is a more detailed version of a data category. If you think of a table or spreadsheet or any structured data (data with cleanly organized columns and rows), a data feature usually fits into one or a combination of only a few columns. Example: ""Store Location"" would be one or a few fields describing the address of a store (address, city, state, zip, etc.). ""Product Ratings"" is likely one columns with the number of stars a consumer rates a product, but it could also be a count of the number of ratings each product has. The important thing is that the data provider possesses information about product ratings.'
							},
							change: (event: any) => {
								const queryObj = event;
								this.dataFeatures = [];
								const selected = this.basicModel['features'];
								///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
								if(selected && selected.length) {
									this.selectedValue = selected.map(item => item.id);
								}
								if(queryObj.query) {
									this.filterService.getDataFeatures(queryObj.query).subscribe(d => {
										const listArray = [];
										const listItems = d ? d['hits']['hits'] : [];
										this.dataFeatures = listItems;
										for (let i = 0; i < listItems.length; i++) {
											const item = listItems[i]._source;
											if (!this.selectedValue.includes(item.id)) {
												listArray.push({
													id: item.id,
													dataFeature: item.dataFeature,
													desc: item.dataFeature
												});
											}
										}
										this.dataFeatures = [...listArray];
										setTimeout(() => {
											this.categorizationsFields[11].templateOptions.options = this.dataFeatures;
										}, 2500);
									});
								}
								else
								{
								this.filterService.getFeaturesForFilter().subscribe(data => {
									//const listArray = [];
									//const listItems = d ? d['hits']['hits'] : [];
									const listItems = (data['aggregations'] && data['aggregations'].features) ? data['aggregations'].features.buckets : [];
									//this.dataFeatures = listItems;
									for (let i = 0; i < listItems.length; i++) {
										const feature = listItems[i];
										const featureId = parseInt(feature.key, 10);
										const featureList = feature.features.hits.hits[0]._source.features;
										const filteredFeatureDescription = featureList.filter( (item) => item.id === feature.key);
										//const item = listItems[i]._source;
										if (!this.selectedValue.includes(featureId)) {
											this.dataFeatures.push({
												id: featureId,
												dataFeature: featureList.dataFeature,
												desc: filteredFeatureDescription && filteredFeatureDescription[0] && filteredFeatureDescription[0].desc ? filteredFeatureDescription[0].desc : '',
											});
										}
									}
									//this.dataFeatures = [...listArray];
									setTimeout(() => {
										this.categorizationsFields[11].templateOptions.options = this.dataFeatures;
									}, 2500);
								});
							}
							},
							blur: (event: any) => {
								if (event && event.target && !event['selectedVal']) {
									event.target.value = '';
								}
							}
						},
					},
                    {
                        key: 'featuresEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
					{
						key: 'sources',
						type: 'multiselect',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Data Sources:',
							placeholder: '',
							description: 'What is the original source of the data being produced by this data provider?',
							options: this.dataSources,
							attributes: {
								field: 'desc',
								'placeholderText' : 'The original (physical) source of the provider\'s data, not to be confused with external Provider Partners (the companies the provider receives data from).',
					            'labelTooltip': 'See "Data Sources" table for a definition of each data source. Fill this in if the data source is explicitly stated on the company\'s website or through another source. If the source is not clear, leave blank. This is meant to be a more generic indicator of where the data came from.',
					            'descriptionTooltip': 'See "Data Sources" table for a definition of each data source. Fill this in if the data source is explicitly stated on the company\'s website or through another source. If the source is not clear, leave blank. This is meant to be a more generic indicator of where the data came from.',
					            'placeholderTooltip': 'See "Data Sources" table for a definition of each data source. Fill this in if the data source is explicitly stated on the company\'s website or through another source. If the source is not clear, leave blank. This is meant to be a more generic indicator of where the data came from.'
							}
						},
					},
                    {
                        key: 'sourcesEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
					{
						key: 'numberOfDataSources.id',
						type: 'dropdown',
						className: 'customSmallWidth-dropdown',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Approximate Number of Data Sources:',
							description: 'From approximately how many unique sources does the provider receive/gather data from?',
							options: this.providerNumberOfDataSources,
							attributes: {
								field: 'label',
							}
						},
					},
                    {
                        key: 'numberOfDataSourcesEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
					{
						key: 'dataSourcesDetails',
						type: 'textarea',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Data Sources Details:',
							description: 'Please provide more details on exactly where and how the data provider acquired their data.',
							attributes: {
								field: 'desc',
								'trackNames': 'Data Sources Details',
								'trackCategory': 'Provider Profile Admin - Textbox',
								'trackLabel': 'Provider Profile Admin - Input',
                                'maximumLength' : 255
							},
							// maxLength : 255,
							change: this.basicInput
						},
					},
                    {
                        key: 'dataSourcesDetailsEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
					{
						key: 'geographicalFoci',
						type: 'multiselect',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Geographical Focus:',
							placeholder: '',
							required: true,
							options: this.geographicalFocus,
							attributes: {
								'placeholderText' : 'The geographies that the data provider focuses on collecting/providing/analyzing data about.',
								'labelTooltip': 'If a data provider covers numerous countries across continents, then it is multinational. If a data provider only covers one continent, certain countries or a specific region, then select each one. This is not the location of the company itself.',
								'placeholderTooltip': 'If a data provider covers numerous countries across continents, then it is multinational. If a data provider only covers one continent, certain countries or a specific region, then select each one. This is not the location of the company itself.',
								'lookupmodule':'GEOGRAPHICAL_FOCUS'
							}
					    },
					    validation: {
					        messages: {
					            required: 'Geographical Focus is required'
					        }
					    }
					},
                    {
                        key: 'geographicalFociEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
					{
						key: 'providerTags',
						type: 'multiselect',
						className: 'custom-multiselect',
					    wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Tags:',
							options: this.providerTags,
							attributes: {
								field: 'desc',
								'placeholderText' : 'Special circumstances for a data provider.',
					            'labelTooltip': 'See "Data Provider Tags" table for a definition of each tag. Particular attributes of the provider or the provider\'s data that are differentiators would be added as new Tags which wouldn\'t fit into any of the other categorizations.',
					            'placeholderTooltip': 'See "Data Provider Tags" table for a definition of each tag. Particular attributes of the provider or the provider\'s data that are differentiators would be added as new Tags which wouldn\'t fit into any of the other categorizations.'
							}
						},
					},
                    {
                        key: 'providerTagsEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
				];
			});
			if(this.providerTags)
			{
				this.load(this.recordID);
		}
		});

			this.companyDetailsModel = this.dataProvider;
		});

        // INVESTIGATIONPANELFIELDS

        const investigationScoreWillingness = [];
        const investigationScoreUniqueness = [];
        const investigationScoreValue = [];
        const investigationScoreReadiness = [];
        const investigationScoreOverall = [];
        const investigationSecRegulated = [];
        const investigationInvestorClientBucket = [];
        const investigationOrganizationType = [];
        const investigationManagerType = [];
        const investigationRelevantInvestors = [];
        const investigationStrategies = [];
        const investigationResearchStyles = [];
        const investigationInvestingTimeFrame = [];
        const investigationEquitiesMarketCap = [];
        const investigationEquitiesStyle = [];
        const investigationMainAssetClass = [];
        const investigationRelevantAssetClass = [];
        const investigationSecurityTypes = [];
        const investigationRelevantSectors = [];
        const investigationpublicEquitiesCoveredRange = [];
        const investigationIdentifiersAvailable = [];

		this.filterService.getWillingnessToProvideToInvestors().subscribe(data => {
            const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// investigationScoreWillingness.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.description !== '') {
					investigationScoreWillingness.push({
						value: listItems[i]._source.id,
						label: listItems[i]._source.description
					});
				}
            }
        });

		this.filterService.getUniqueness().subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// investigationScoreUniqueness.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.description !== '') {
					investigationScoreUniqueness.push({
						value: listItems[i]._source.id,
						label: listItems[i]._source.description
					});
				}
			}
        });

		this.filterService.getValue().subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// investigationScoreValue.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.description !== '') {
					investigationScoreValue.push({
						value: listItems[i]._source.id,
						label: listItems[i]._source.description
					});
				}
			}
        });

		this.filterService.getReadiness().subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// investigationScoreReadiness.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.description !== '') {
					investigationScoreReadiness.push({
						value: listItems[i]._source.id,
						label: listItems[i]._source.description
					});
				}
			}
        });

		this.filterService.getOverall().subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// investigationScoreOverall.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.description !== '') {
					investigationScoreOverall.push({
						value: listItems[i]._source.id,
						label: listItems[i]._source.description
					});
				}
			}
		});

		this.filterService.getLookupCodes('YES_OR_NO').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// investigationSecRegulated.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				investigationSecRegulated.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('CLIENT_RANGE').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// investigationInvestorClientBucket.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				investigationInvestorClientBucket.push({
					value: listItems[i]._source.lookupcode,
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('ORGANIZATION_TAG').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				investigationOrganizationType.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('INVESTMENT_MANAGER_TYPE').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				investigationManagerType.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getInvestorType().subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				investigationRelevantInvestors.push({
					value: {
						id: listItems[i]._source.id,
						description: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('INVESTMENT_MANAGER_STRATEGY').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				investigationStrategies.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('INVESTMENT_MANAGER_RESEARCH_STYLE').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				investigationResearchStyles.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('INVESTING_TIME_FRAME').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				investigationInvestingTimeFrame.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('EQUITY_MARKET_CAP').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				investigationEquitiesMarketCap.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('EQUITY_STYLE').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				investigationEquitiesStyle.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		// this.providerMainAssetClass = [];
		// this.filterService.getMainAssetClass().subscribe(data => {
		// 	const listItems = (data['aggregations'] && data['aggregations'].mainAssetclass
		// ) ? data['aggregations'].mainAssetclass.buckets : [];
		// this.providerMainAssetClass.push({label : 'Select', value: null });
		// 	for (let i = 0; i < listItems.length; ++i) {
		// 		const item = listItems[i].mainAssetClass.hits.hits[0]._source.mainAssetClass;
		// 		this.providerMainAssetClass.push({
		// 			value: listItems[i].key,
		// 			label: item.description
		// 		});
		// 	}
		// 	this.investigationFields[17].templateOptions.options = this.providerMainAssetClass;
		// });

		this.filterService.getLookupCodes('ASSET_CLASS').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// investigationMainAssetClass.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				investigationMainAssetClass.push({
					value: listItems[i]._source.lookupcode,
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getRelatedAssetClass().subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			//console.log('List Items',listItems);
			//listItems.shift();
			//console.log('List Items 1',listItems);
			for (let i = 0; i < listItems.length; i++) {
				//console.log('4',this.providerRelevantAssetClass[0]);
				//this.providerRelevantAssetClass.shift();
				if(listItems[i]._source.description !== 'DEFAULT_NULL') {
					investigationRelevantAssetClass.push({
						value: {
							id: listItems[i]._source.id,
							desc: listItems[i]._source.description
						},
						label: listItems[i]._source.description
					});
				}
			}
		});

		this.filterService.getLookupCodes('RLVNT_SECURITY_TYPE').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				investigationSecurityTypes.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode,
						//locked: listItems[i]._source.locked ? listItems[i]._source : false
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('RLVNT_SECTOR').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				investigationRelevantSectors.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.description,
						//locked: listItems[i]._source.locked ? listItems[i]._source : false
					},
					label: listItems[i]._source.description
				});
			}
		});

		this.filterService.getLookupCodes('COVERAGE_RANGE').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
            console.log('Range', listItems);
			for (let i = 0; i < listItems.length; i++) {
				investigationpublicEquitiesCoveredRange.push({
				/* 	value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					}, */
					label: listItems[i]._source.description,
					value: listItems[i]._source.description
				});
			}
		});

        this.filterService.getLookupCodes('IDENTIFIERS').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				investigationIdentifiersAvailable.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}

            this.investigationFields = [
                {
                    key: 'scoreInvestorsWillingness.id',
                    type: 'dropdown',
                    className: 'customWidth-dropdown',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Willingness to Provide to Asset Managers Score:',
                        description: 'How willing is the data provider to sell their data or analytics products to the institutional investment industry (e.g. hedge funds)?',
                        required: true,
                        options: investigationScoreWillingness,
                        attributes: {
                            field: 'label'
                        }
                    },
                },
                {
                    key: 'scoreInvestorsWillingnessEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'scoreUniqueness.id',
                    type: 'dropdown',
                    className: 'customWidth-dropdown',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Uniqueness Score:',
                        placeholder: '',
                        required: false,
                        options: investigationScoreUniqueness,
                        attributes: {
                            field: 'label',
                            'placeholderText' : 'An Amass-created subjective score of how unique this provider\'s data or analytics is compared to traditional investment research techniques.'
                        }
                    },
                },
                {
                    key: 'scoreValue.id',
                    type: 'dropdown',
                    className: 'customWidth-dropdown',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Value Score:',
                        placeholder: '',
                        required: false,
                        options: investigationScoreValue,
                        attributes: {
                            field: 'label',
                            'placeholderText' : 'An Amass-created subjective score of how valuable this provider\'s data or analytics is compared to traditional investment research techniques.'
                        }
                    },
                },
                {
                    key: 'scoreReadiness.id',
                    type: 'dropdown',
                    className: 'customLargeWidth-dropdown',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Readiness Score:',
                        placeholder: '',
                        required: false,
                        options: investigationScoreReadiness,
                        attributes: {
                            field: 'label',
                            'placeholderText' : 'An Amass-created subjective score of how ready this provider\'s data or analytics is to be deployed within investment research.'
                        }
                    },
                },
                {
                    key: 'scoreOverall.id',
                    type: 'dropdown',
                    className: 'customSmallWidth-dropdown',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Overall Score:',
                        placeholder: '',
                        required: false,
                        options: investigationScoreOverall,
                        attributes: {
                            field: 'label',
                            'placeholderText' : 'An Amass-created subjective score of the overall importance of integrating this provider\'s data or analytics into an investment research process.'
                        }
                    },
                },
                {
                    key: 'secRegulated.id',
                    type: 'dropdown',
                    className: 'customSmallWidth-dropdown',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'SEC-Regulated:',
                        description: 'Is the data provider registered with the SEC?',
                        required: false,
                        options: investigationSecRegulated,
                        attributes: {
                            field: 'label'
                        }
                    },
                },
                {
                    key: 'secRegulatedEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'investorClientBucket',
                    type: 'dropdown',
                    className: 'customSmallWidth-dropdown',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Approximate Number of Investor Clients:',
                        description: 'Approximately how many clients in the asset management industry does the data provider have?',
                        required: false,
                        options: investigationInvestorClientBucket,
                        attributes: {
                            field: 'label',
                            'labelTooltip': 'Usually the provider will not offer up this information, especially if they sell to the investment management industry, but may just give you a sense for how many.',
                            'descriptionTooltip': 'Usually the provider will not offer up this information, especially if they sell to the investment management industry, but may just give you a sense for how many.'
                        }
                    },
                },
                {
                    key: 'investorClientBucketEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'investorClientsCount',
                    type: 'input',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Number of Investor Clients:',
                        type: 'number',
                        description: 'How many clients in the asset management industry does the data provider have?',
                        required: false,
                        change: this.basicInput,
                        attributes: {
                            field: 'label',
                            'labelTooltip': 'The precise number of clients the provider has in the investment management industry. Usually the provider will not offer up this information, especially if they sell to the investment management industry, but may just give you a sense for how many.',
                            'descriptionTooltip': 'The precise number of clients the provider has in the investment management industry. Usually the provider will not offer up this information, especially if they sell to the investment management industry, but may just give you a sense for how many.',
                            'trackNames': 'Number of Investor Clients',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input'
                        },
                        keypress: (event) => {
                        },
                        maxLength : 10000000
                    }
                },
                {
                    key: 'investorClientsCountEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'investor_clients',
                    type: 'input',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Number of Investor Clients Details:',
                        description: 'How many clients in the investment management industry does this provider have?',
                        change: this.basicInput,
                        attributes: {
                            'labelTooltip': 'Usually the provider will not offer up this information, especially if they sell to the investment management industry, but may just give you a sense for how many.',
                            'descriptionTooltip': 'Usually the provider will not offer up this information, especially if they sell to the investment management industry, but may just give you a sense for how many.',
                            'trackNames': 'Number of Investor Clients Details',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input'
                        },
                        keypress: (event) => {
                        },
                        maxLength : 512,
                    },
                },
                {
                    key: 'investor_clientsEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'organizationType',
                    type: 'multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Target Organization Type:',
                        description: 'What types of companies/organizations is this provider focusing on selling their data offerings to?',
                        change: this.basicInput,
                        options: investigationOrganizationType,
                        attributes: {
                            field: 'desc',
                            'labelTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'descriptionTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'lookupmodule': 'ORGANIZATION_TAG'
                        }
                    },
                },
                {
                    key: 'organizationTypeEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'managerType',
                    type: 'multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Target Investment Manager Type:',
                        description: 'What types of investors will this data provider\'s offerings be useful for?',
                        change: this.basicInput,
                        options: investigationManagerType,
                        attributes: {
                            'labelTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'descriptionTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'lookupmodule': 'INVESTMENT_MANAGER_TYPE'
                        }
                    },
                },
                {
                    key: 'managerTypeEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'investorTypes',
                    type: 'multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Relevant Investor Type:',
                        placeholder: '',
                        description: 'What types of investors will this data provider\'s offerings be useful for?',
                        options: investigationRelevantInvestors,
                        attributes: {
                            'placeholderText' : 'The types of investors the provider\'s data would be useful for or would have the ability to use.',
                            'labelTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'descriptionTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'placeholderTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'lookupmodule': 'RLVNT_INVESTOR_TYPE'

                        }
                    },
                },
                {
                    key: 'investorTypesEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'strategies',
                    type: 'multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Target Investment Managers\' Strategies:',
                        description: 'Which investor strategies are this provider\'s offerings most applicable to or useful for?',
                        options: investigationStrategies,
                        change: this.basicInput,
                        attributes: {
                            'labelTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'descriptionTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'lookupmodule': 'INVESTMENT_MANAGER_STRATEGY'
                        }
                    },
                },
                {
                    key: 'strategiesEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'researchStyles',
                    type: 'multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Target Investment Managers\' Research Styles:',
                        description: 'Which investor research styles are this provider\'s offerings most applicable or useful for?',
                        options: investigationResearchStyles,
                        change: this.basicInput,
                        attributes: {
                            'labelTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'descriptionTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'lookupmodule': 'INVESTMENT_MANAGER_RESEARCH_STYLE'
                        }
                    },
                },
                {
                    key: 'researchStylesEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'investingTimeFrame',
                    type: 'multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Target Investment Managers\' Investing Time Frame:',
                        description: 'Which investing time frames are this provider\'s offerings most applicable or useful for?',
                        options: investigationInvestingTimeFrame,
                        change: this.basicInput,
                        attributes: {
                            'labelTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'descriptionTooltip': 'If applicable, use the provider\'s previous client types to fill this in.'
                        }
                    },
                },
                {
                    key: 'investingTimeFrameEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'equitiesMarketCap',
                    type: 'multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Target Investment Managers\' Public Equities Market Cap:',
                        description: 'What size of public companies are this provider\'s offerings most applicable or useful for researching/understanding?',
                        options: investigationEquitiesMarketCap,
                        change: this.basicInput,
                        attributes: {
                            'labelTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'descriptionTooltip': 'If applicable, use the provider\'s previous client types to fill this in.'
                        }
                    },
                },
                {
                    key: 'equitiesMarketCapEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'equitiesStyle',
                    type: 'multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Target Investment Managers\' Public Equities Style:',
                        description: 'What type of public companies are this provider\'s offerings most applicable or useful for researching/understanding?',
                        options: investigationEquitiesStyle,
                        change: this.basicInput,
                        attributes: {
                            'labelTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                            'descriptionTooltip': 'If applicable, use the provider\'s previous client types to fill this in.'
                        }
                    },
                },
                {
                    key: 'equitiesStyleEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'mainAssetClass',
                    type: 'dropdown',
                    className: 'customWidth-dropdown',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Main Asset Class:',
                        required: true,
                        placeholder: '',
                        description: 'Which security asset class is the provider\'s data offerings the most useful to research?',
                        options: investigationMainAssetClass,
                        attributes: {
                            field: 'label',
                                'placeholderText' : 'Sometimes the data provider will belong in more than one asset class - choose the primary asset class, and add all other asset classes in the following question.',
                                'labelTooltip': 'For some Providers this will be explicitly stated. Bonds are part of the Fixed Income class, stocks are Public Equities, Foreign Exchange is Currencies. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.',
                                'descriptionTooltip': 'For some Providers this will be explicitly stated. Bonds are part of the Fixed Income class, stocks are Public Equities, Foreign Exchange is Currencies. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.',
                                'placeholderTooltip': 'For some Providers this will be explicitly stated. Bonds are part of the Fixed Income class, stocks are Public Equities, Foreign Exchange is Currencies. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.'
                        }
                    },
                },
                {
                    key: 'mainAssetClassEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'assetClasses',
                    type: 'multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Relevant Asset Classes:',
                        required: true,
                        placeholder: '',
                        description: 'What are all the asset classes this provider\'s data offerings can be used to research?',
                        options: investigationRelevantAssetClass,
                        attributes: {
                            field: 'desc',
                            'placeholderText' : 'All of the financial security asset classes that the provider\'s data can be used for or has any relevance towards.',
                            'labelTooltip': 'At minimum, the Main Asset Class should be listed here. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.',
                            'descriptionTooltip': 'At minimum, the Main Asset Class should be listed here. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.',
                            'placeholderTooltip': 'At minimum, the Main Asset Class should be listed here. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.'
                        }
                    },
                },
                {
                    key: 'assetClassesEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'securityTypes',
                    type: 'multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Relevant Security Types:',
                        required: true,
                        placeholder: '',
                        description: 'What are all the security types this provider\'s data offerings can be used to research?',
                        options: investigationSecurityTypes,
                        attributes: {
							 field:'desc',
                            'placeholderText' : 'All of the financial security types the provider\'s data can be used for or has any relevance towards.',
                            'labelTooltip': 'Only add the obvious choices here. If the website talks about their data\'s use for specific securities add them. Look through the list of Security types. These are specific names, and will often be referred to directly.',
                            'descriptionTooltip': 'Only add the obvious choices here. If the website talks about their data\'s use for specific securities add them. Look through the list of Security types. These are specific names, and will often be referred to directly.',
                            'placeholderTooltip': 'Only add the obvious choices here. If the website talks about their data\'s use for specific securities add them. Look through the list of Security types. These are specific names, and will often be referred to directly.',
                            'lookupmodule': 'RLVNT_SECURITY_TYPE'
                        }
                    },
                },
                {
                    key: 'securityTypesEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'relevantSectors',
                    type: 'multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Relevant Sectors:',
                        required: true,
                        placeholder: '',
                        description: 'What sectors are the companies/securities in that this data could be used for?',
                        options: investigationRelevantSectors,
                        attributes: {
                            field: 'desc',
                            'placeholderText' : 'All of the sectors the provider\'s data could be used for or has any relevance towards.'
                        }
                    },
                },
                {
                    key: 'relevantSectorsEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'publicEquitiesCoveredRange',
                    type: 'radioButton',
                    /* defaultValue: 582, */
                    /* className: 'customSmallWidth-dropdown', */
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Approximate Number of Public Equities Covered:',
                        description: 'What is the range of the number of public equities the data providers data offerings be used to research?',
                        options: investigationpublicEquitiesCoveredRange,
                        attributes: {
                            field: 'label',
                        },
                    },
                },
                {
                    key: 'publicEquitiesCoveredRangeEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'publicEquitiesCoveredCount',
                    type: 'input',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Number of Public Equities Covered:',
                        type: 'number',
                        required: false,
                        description: 'Precisely, how many public equities can the data provider\'s data offerings be used to research?',
                        attributes: {
                            'trackNames': 'Number of Public Equities Covered',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input'
                        },
                        keypress: (event) => {
                        },
                        change: this.basicInput,
                        maxLength : 10000000,
                    },
                },
                {
                    key: 'publicEquitiesCoveredCountEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'publicCompaniesCovered',
                    type: 'textarea',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Public Companies Covered Details:',
                        minRows: 2,
                        maxRows: 100,
                        placeholder: '',
                        description: 'What companies/tickers can this data be used for?',
                        change: this.basicInput,
                        attributes:{
                            'placeholderText' : 'The public companies/tickers the provider\'s data can be used for or are relevant towards.',
                            'trackNames': 'Public Companies Covered Details',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input',
                            'maximumLength' : 256
                        },
                        // maxLength : 256,

                    },
                },
                {
                    key: 'publicCompaniesCoveredEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'identifiersAvailable',
                    type: 'multiselect',
                    className: 'custom-multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Identifiers Available:',
                        required: false,
                        description: 'What types of public company identifiers are available within the provider\'s data offerings?',
                        placeholder: '',
                        options: investigationIdentifiersAvailable,
                        attributes: {
                            'placeholderText' : '"None" indicates the provider has not standardized the public company identifiers in their dataset.'
                        }
                    },
                },
                {
                    key: 'identifiersAvailableEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
            ];
		});

        // DATADETAILSPANELFIELDS

        const dataDetailsAccessOffered = [];
        const dataDetailsDataProductTypes = [];
        const dataDetailsDataTypes = [];
        const dataDetailsDataLanguagesAvailable = [];
        const dataDetailsDatasetSize = [];
        const dataDetailsDatasetNumberOfRows = [];

		this.filterService.getLookupCodes('PROVIDER_ACCESS').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				dataDetailsAccessOffered.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('DATA_PRODUCT_TYPE').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				dataDetailsDataProductTypes.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('DATA_TYPE').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				dataDetailsDataTypes.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('LANGUAGE').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				dataDetailsDataLanguagesAvailable.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}

			if (this.dataDetailsFields[4] && this.dataDetailsFields[4].model) {
				if (Array.isArray(this.dataDetailsFields[4].model['languagesAvailable']) || this.dataDetailsFields[4].model['languagesAvailable']  === undefined) {
					this.dataDetailsFields[4].model['languagesAvailable'] = [];
					this.dataDetailsFields[4].model['languagesAvailable'].push(dataDetailsDataLanguagesAvailable[0].value);
				} else {
					this.dataDetailsFields[4].model['languagesAvailable']
				}
			}
		});

		this.filterService.getLookupCodes('DATA_SIZE_RANGE').subscribe(data => {
			const datasetSize = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			datasetSize.map((val, index) => {
                //console.log('......pointer mapping', val);
				dataDetailsDatasetSize.push({
					value: val._source.description,
					label: val._source.description
                });
			});
		});

		this.filterService.getLookupCodes('ROWS_RANGE').subscribe(data => {
			const datasetNumberOfRows = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			datasetNumberOfRows.map((val, index) => {
                //console.log('......pointer mapping', val);
				dataDetailsDatasetNumberOfRows.push({
					value: val._source.description,
					label: val._source.description
                });
			});

            // setTimeout(() => {
                this.dataDetailsFields = [
                    {
                        key: 'yearFirstDataProductLaunched',
                        type: 'mask',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Year First Data Product Launched:',
                            description: 'In what year did the provider launch their first data product to external clients?',
                            change: this.basicInput,
                            attributes: {
                                'mask': '9999',
                                'trackNames': 'Year First Data Product Launched',
                                'trackCategory': 'Provider Profile Admin - Textbox',
                                'trackLabel': 'Provider Profile Admin - Input'
                            },
                            keypress:(event)=> {

                            },
                        },
                    },
                    {
                        key: 'yearFirstDataProductLaunchedEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'accessOffered',
                        type: 'multiselect',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Access Offered:',
                            description: 'What level of access and visibility into the provider\'s research/analysis process do they offer to clients',
                            options: dataDetailsAccessOffered,
                            attributes: {
                                'lookupmodule': 'PROVIDER_ACCESS'
                            }
                        },
                    },
                    {
                        key: 'accessOfferedEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'dataProductTypes',
                        type: 'multiselect',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Data Product Types:',
                            description: 'What best describes the types of data products this provider offers to their clients? In what general format does the provider deliver information to their clients?',
                            options: dataDetailsDataProductTypes,
                            attributes: {
                                'lookupmodule': 'DATA_PRODUCT_TYPE'
                            }
                        },
                    },
                    {
                        key: 'dataProductTypesEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'dataTypes',
                        type: 'multiselect',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Data Types:',
                            description: 'What best describes the types of datasets the provider offers to their clients? How mature are their datasets?',
                            options: dataDetailsDataTypes,
                            attributes: {
                                'lookupmodule': 'DATA_TYPE'
                            }
                        },
                    },
                    {
                        key: 'dataTypesEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'languagesAvailable',
                        type: 'multiselect',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Data Languages Available:',
                            required: true,
                            description: 'In what languages is this provider\'s data products offered in?',
                            options: dataDetailsDataLanguagesAvailable,
                            attributes: {
                                'lookupmodule': 'LANGUAGE'
                            }
                        },
                    },
                    {
                        key: 'languagesAvailableEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'datasetSize',
                        type: 'radioButton',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Dataset Size:',
                            required: false,
                            description: 'Approximately how much storage is needed for the data provider\'s entire dataset?',
                            options: dataDetailsDatasetSize
                        },
                    },
                    {
                        key: 'datasetSizeEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'datasetNumberOfRows',
                        type: 'radioButton',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Dataset # of Rows:',
                            required: false,
                            description: 'Approximately how many rows are in the data provider\'s dataset?',
                            options: dataDetailsDatasetNumberOfRows
                        },
                    },
                    {
                        key: 'datasetNumberOfRowsEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'collectionMethodsExplanation',
                        type: 'textarea',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Collection Methods Explanation:',
                            minRows: 2,
                            maxRows: 100,
                            placeholder: '',
                            description: 'Provide an overview of how the provider collects their data. Has there been any changes to their collection methods over time? If so, does this affect the ability to compare between time periods?',
                            change: this.basicInput,
                            attributes:{
                                'placeholderText' : 'Details about how and where this provider gets their raw data',
                                'trackNames': 'Collection Methods Explanation',
                                'trackCategory': 'Provider Profile Admin - Textbox',
                                'trackLabel': 'Provider Profile Admin - Input',
                                'maximumLength': 1000
                            },
                            // maxLength : 1000,

                        },
                    },
                    {
                        key: 'collectionMethodsExplanationEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'sampleOrPanelSize',
                        type: 'textarea',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Sample or Panel Size:',
                            minRows: 2,
                            maxRows: 100,
                            placeholder: '',
                            change: this.basicInput,
                            attributes: {
                                'placeholderText' : 'Details about the sample or panel size of this provider\'s raw data',
                                'trackNames': 'Sample or Panel Size',
                                'trackCategory': 'Provider Profile Admin - Textbox',
                                'trackLabel': 'Provider Profile Admin - Input',
                                'maximumLength': 512
                            },
                            // maxLength : 512,
                        },
                    },
                    {
                        key: 'sampleOrPanelSizeEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'dateCollectionBegan',
                        type: 'datepicker',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Date Collection Began:',
                            description: 'What is the approximate earliest date of consistent data within your offerings?',
                            change: this.basicInput,
                            attributes: {
                                'labelTooltip': 'the date format is US style: MM/DD/YYYY (month then day then year)',
                                'descriptionTooltip': 'the date format is US style: MM/DD/YYYY (month then day then year)'
                            }
                        },
                    },
                    {
                        key: 'dateCollectionBeganEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'dateCollectionRangeExplanation',
                        type: 'textarea',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Date Collection Range Explanation:',
                            minRows: 2,
                            maxRows: 100,
                            placeholder: '',
                            change: this.basicInput,
                            attributes: {
                                'placeholderText' : 'Details about how much historical data this provider has',
                                'trackNames': 'Date Collection Range Explanation',
                                'trackCategory': 'Provider Profile Admin - Textbox',
                                'trackLabel': 'Provider Profile Admin - Input',
                                'maximumLength': 256
                            },
                            // maxLength : 256,
                        },
                    },
                    {
                        key: 'dateCollectionRangeExplanationEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'keyDataFields',
                        type: 'inputEditor',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Key Data Fields:',
                            attributes: {
                                'placeholderText' : 'The most important data fields within the provider\'s dataset',
                                'trackNames': 'Key Data Fields',
                                'trackCategory': 'Provider Profile Admin - Textbox',
                                'trackLabel': 'Provider Profile Admin - Input',
                                'maximumLength' : 512,
                            },
                            change: this.basicInput
                        },
                    },
                    {
                        key: 'keyDataFieldsEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'outOfSampleData',
                        type: 'textarea',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Out-of-Sample Data:',
                            minRows: 2,
                            maxRows: 100,
                            description: 'If applicable, when did the data provider\'s data become out-of-sample (or when did people first begin trading on it)?',
                            attributes: {
                                'trackNames': 'Out-of-Sample Data',
                                'trackCategory': 'Provider Profile Admin - Textbox',
                                'trackLabel': 'Provider Profile Admin - Input',
                                'maximumLength': 255
                            },
                            // maxLength : 255,
                            change: this.basicInput
                        },
                    },
                    {
                        key: 'outOfSampleDataEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                    {
                        key: 'productDetails',
                        type: 'inputEditor',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Product Details:',
                            attributes:{
                                'placeholderText' : 'Further details regarding the provider\'s data products, such as pricing or details that don\'t fit into any other field.',
                                'trackNames': 'Product Details',
                                'trackCategory': 'Provider Profile Admin - Textbox',
                                'trackLabel': 'Provider Profile Admin - Input',
                                'maximumLength': 3000
                            },
                            //maxLength : 6,
                            change: this.basicInput
                        },
                    },
                    {
                        key: 'productDetailsEdited',
                        type: 'fieldset',
                        templateOptions: {
                            label: ''
                        }
                    },
                ];
            // }, 20000);
		});

        // DATAQUALITYPANELFIELDS

        const dataQualityPointInTimeAccuracy = [];
        const dataQualityDataGaps = [];
        const dataQualityDataGapsReasons = [];
        const dataQualityDuplicatesCleaned = [];
        const dataQualityIncludesOutliers = [];
        const dataQualityOutlierReasons = [];
        const dataQualityBiases = [];
        const dataQualityNormalized = [];

		this.filterService.getLookupCodes('YES_OR_NO_SOFT').subscribe(data => {
			const pointInTimeAccuracy = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// const providerPointInTime = [];
			pointInTimeAccuracy.map((val, index) => {
               // console.log('......pointer mapping', val);
               dataQualityPointInTimeAccuracy.push({
					value: val._source.id,
					label: val._source.description
                });
			});
		});

		this.filterService.getLookupCodes('YES_NO_UNKNOWN').subscribe(data => {
			const dataGaps = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			// const providerPointInTime = [];
			dataGaps.map((val, index) => {
              //  console.log('......DataGaps mapping', val);
                dataQualityDataGaps.push({
					value: val._source.id,
					label: val._source.description
                });
			});
		});

		this.filterService.getLookupCodes('DATA_GAPS_REASON').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				dataQualityDataGapsReasons.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('YES_NO_UNKNOWN').subscribe(data => {
			const duplicatesCleaned = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			duplicatesCleaned.map((val, index) => {
                //console.log('......pointer mapping', val);
				dataQualityDuplicatesCleaned.push({
					value: val._source.id,
					label: val._source.description
                });
			});
		});

		this.filterService.getLookupCodes('YES_NO_UNKNOWN').subscribe(data => {
			const includesOutliers = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			includesOutliers.map((val, index) => {
                //console.log('......pointer mapping', val);
				dataQualityIncludesOutliers.push({
					value: val._source.id,
					label: val._source.description
                });
			});
		});

		this.filterService.getLookupCodes('OUTLIER_REASON').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				dataQualityOutlierReasons.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('BIASES').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				dataQualityBiases.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.filterService.getLookupCodes('YES_NO_UNKNOWN').subscribe(data => {
			const normalized = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			normalized.map((val, index) => {
                //console.log('......pointer mapping', val);
				dataQualityNormalized.push({
					value: val._source.id,
					label: val._source.description
                });
			});

            this.dataQualityFields = [
                {
                    key: 'pointInTimeAccuracy.id',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Point-in-Time Accuracy:',
                        description: 'Are the provider\'s data offerings point-in-time accurate?',
                        placeholder: '',
                        options: dataQualityPointInTimeAccuracy,
                        attributes: {
                            'placeholderText' : 'Accurate Point-in-Time Data has not been changed after its been captured/generated and published, even if issues with the data are found later on. Fixes can be made available in separate series but the underlying data is never changed. In other words, if corporate actions that affect the company identifiers in the provider\'s data are tracked, are those identifiers accurate at each particular point-in-time?'
                        }
                    },
                },
                {
                    key: 'pointInTimeAccuracyEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'dataGaps.id',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Data Gaps:',
                        description: 'Does the provider data offerings include any data gaps? In other words, are there unexpected missing data?',
                        options: dataQualityDataGaps
                    },
                },
                {
                    key: 'dataGapsEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'dataGapsReasons',
                    type: 'multiselect',
                    // className: 'custom-multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Data Gaps Reasons:',
                        description: 'If there are some gaps in the provider\'s data offerings, what are the reasons that the data is missing?',
                        placeholder: '',
                        options: dataQualityDataGapsReasons,
                        attributes :{
                            'placeholderText' : '',
                            'lookupmodule': 'DATA_GAPS_REASON'
                        }
                    },
                },
                {
                    key: 'dataGapsReasonsEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'duplicatesCleaned.id',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Duplicates Cleaned:',
                        description: 'Has the provider scrubbed out the duplicates from their data offerings?',
                        options: dataQualityDuplicatesCleaned
                    },
                },
                {
                    key: 'duplicatesCleanedEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'includesOutliers.id',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Includes Outliers:',
                        description: 'Does the provider\'s data offerings have any outliers? In other words, are there any data points that do not follow the patterns of the vast majority of the rest of the data?',
                        options: dataQualityIncludesOutliers
                    },
                },
                {
                    key: 'includesOutliersEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'outlierReasons',
                    type: 'multiselect',
                    // className: 'custom-multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Outlier Reasons:',
                        description: 'If there are some outliers in the provider\'s data offerings, what are the reasons that they occur?',
                        placeholder: '',
                        options: dataQualityOutlierReasons,
                        attributes: {
                            'placeholderText' : '',
                            'lookupmodule': 'OUTLIER_REASON'
                        }
                    },
                },
                {
                    key: 'outlierReasonsEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'biases',
                    type: 'multiselect',
                    // className: 'custom-multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Biases:',
                        description: 'What types of biases does the provider\'s data offerings contain?',
                        placeholder: '',
                        options: dataQualityBiases,
                        attributes: {
                            'placeholderText' : '',
                            'lookupmodule': 'BIASES'
                        }
                    },
                },
                {
                    key: 'biasesEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'sampleOrPanelBiases',
                    type: 'textarea',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Sample or Panel Biases:',
                        description: 'Are there any known biases within the data sample or panel, such as certain demographics, geographies or other common characteristics?',
                        attributes: {
                            'trackNames': 'Sample or Panel Biases',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input',
                            'maximumLength': 255
                        },
                        // maxLength : 255,
                        change: this.basicInput
                    },
                },
                {
                    key: 'sampleOrPanelBiasesEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'normalized.id',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Normalized:',
                        description: 'Has the provider normalized their data offerings for things like seasonality, selection biases (e.g. overweighted in a particular demographic), or attrition (ie panel growing or declining over time)?',
                        options: dataQualityNormalized
                    },
                },
                {
                    key: 'normalizedEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'discussionCleanliness',
                    type: 'textarea',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Discussion - Cleanliness:',
                        placeholder: '',
                        description: 'Discuss the effort the data provider has put into clean and/or normalizing their data, such as the work done and the methods used.',
                        change: this.basicInput,
                        attributes:{
                            'placeholderText' : 'For example, have you backfilled or imputed certain data?',
                            'trackNames': 'Discussion - Cleanliness',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input',
                            'maximumLength': 255
                        },
                        // maxLength : 255,
                    },
                },
                {
                    key: 'discussionCleanlinessEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
            ];
		});

        // TRAILLINGPANELFIELDS

        const traillingTrialPricingModel = [];
        const traillingTrialAgreementType = [];

		this.filterService.getLookupCodes('PRICING_MODEL').subscribe(data => {
			const trialPricingModel = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			trialPricingModel.map((val, index) => {
                //console.log('......pointer mapping', val);
				traillingTrialPricingModel.push({
					value: val._source.id,
					label: val._source.description
                });
			});
		});

		this.filterService.getLookupCodes('AGREEMENT_TYPE').subscribe(data => {
			const trialAgreementType = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			trialAgreementType.map((val, index) => {
                //console.log('......pointer mapping', val);
				traillingTrialAgreementType.push({
					value: val._source.id,
					label: val._source.description
                });
			});

            this.triallingFields = [
                {
                    key: 'freeTrialAvailable',
                    type: 'checkbox',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Free Trial Available:',
                        description: 'Is a free trial available for this provider\'s data products?',
                        attributes: {
                            'multiple': 'false'
                        },
                        change: (event) => {
                            // console.log(event, event['modelKey']);
                            this.basicModel.freeTrialAvailable = event['modelKey'];
                            //console.log(this.basicModel.inactive, this.basicModel);
                        }
                    },
                },
                {
                    key: 'freeTrialAvailableEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'trialPricingModel.id',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Trial Pricing Model:',
                        description: 'If applicable, what is the pricing model for a trial to the data provider\'s data offerings?',
                        placeholder: '',
                        options: traillingTrialPricingModel,
                        attributes: {
                            'placeholderText' : 'The general model for how the data provider prices a trial to their data offerings.'
                        }
                    },
                },
                {
                    key: 'trialPricingModelEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'trialDuration',
                    type: 'textarea',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Trial Duration:',
                        minRows: 2,
                        maxRows: 100,
                        description: 'For how long can the data provider offer a trial?',
                        placeholder: '',
                        change: this.basicInput,
                        attributes:{
                            'placeholderText' : 'Length of a trial to the data provider\'s data offerings.',
                            'trackNames': 'Trial Duration',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input',
                            'maximumLength': 25
                        },
                        // maxLength : 25,
                    },
                },
                {
                    key: 'trialDurationEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'trialAgreementType.id',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Trial Agreement Type:',
                        minRows: 2,
                        maxRows: 100,
                        description: 'What type of trial license agreement does the data provider use?',
                        options: traillingTrialAgreementType
                    },
                },
                {
                    key: 'trialAgreementTypeEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'trialPricingDetails',
                    type: 'textarea',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Trial Pricing Details:',
                        minRows: 2,
                        maxRows: 100,
                        description: 'What are the details of the trial pricing?',
                        attributes: {
                            'trackNames': 'Trial Pricing Details',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input',
                            'maximumLength': 512
                        },
                        // maxLength : 512,
                        change: this.basicInput
                    },
                },
                {
                    key: 'trialPricingDetailsEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
            ];
		});

        // PRICINGPANELFIELDS

        const pricingPricingModels = [];
        const pricingSubscriptionModel = [];
        const pricingSubscriptionTimePeriod = [];
        const pricingMonthlyPriceRange = [];
		this.filterService.getLookupCodes('PRICING_MODEL').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				pricingPricingModels.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});
        this.filterService.getLookupCodes('SUBSCRIPTION_MODEL').subscribe(data => {
			const subscriptionModel = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			subscriptionModel.map((val, index) => {
                console.log('......pointer mapping', val);
				pricingSubscriptionModel.push({
					value: val._source.id,
					label: val._source.description
                });
			});
        });

		this.filterService.getLookupCodes('TIME_PERIOD').subscribe(data => {
			const subscriptionTimePeriod = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			subscriptionTimePeriod.map((val, index) => {
                //console.log('......pointer mapping', val);
				pricingSubscriptionTimePeriod.push({
					value: val._source.id,
					label: val._source.description
                });
			});
		});

		this.filterService.getLookupCodes('MONTHLY_PRICE_RANGE').subscribe(data => {
			const monthlyPriceRange = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			monthlyPriceRange.map((val, index) => {
                //console.log('......pointer mapping', val);
				pricingMonthlyPriceRange.push({
					value: val._source.id,
					label: val._source.description
                });
			});

            this.pricingFields = [
                /* {
                    key: 'pricingModels',
                    type: 'dropdown',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Pricing Model:*',
                        placeholder: '',
                        description: 'What\'s the general model for how this provider prices their data products?',
                        required: false,
                        options: this.pricingModel,
                        attributes: {
                            field: 'desc',
                            'dropdown': 'true',
                            'placeholderText' : 'The general model for how you price your data products.'
                        },
                        change: (event: any) => {
                            const queryObj = event;
                            this.pricingModel = [];

                            this.filterService.getPricingModel(queryObj.query).subscribe(d => {
                                const listArray = [];
                                const listItems = (d && d['hits'].total) ? d['hits']['hits'] : [];
                                for (let i = 0; i < listItems.length; ++i) {
                                    const item = listItems[i]._source;

                                    listArray.push({
                                        id: item.id,
                                        desc: item.description,
                                        locked: item.locked ? true : false
                                    });
                                }
                                this.pricingModel = [...listArray];
                                this.pricingFields[0].templateOptions.options = this.pricingModel;
                            });
                        }
                    },
                }, */
                /* {
                    key: 'pricingModel.id',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Pricing Model:',
                        placeholder: '',
                        required: true,
                        description: 'What\'s the general model for how this provider prices their data products?',
                        options: this.providerPricingModel,
                        attributes: {
                            'placeholderText' : ''
                        },
                        change: this.basicInput
                    },
                }, */
                {
                    key: 'pricingModels',
                    type: 'multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Pricing Model:',
                        required: true,
                        description: 'What pricing models does this provider use to price their data products?',
                        options: pricingPricingModels,
						attributes: {
							field:'desc'
					   }
                    },
                },
                {
                    key: 'pricingModelsEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'subscriptionModel.id',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Subscription Model:',
                        description: 'What\'s the general subscription model for this provider\'s data products?',
                        options: pricingSubscriptionModel,
                        attributes: {
                            field: 'label',
                        },
                    },
                },
                {
                    key: 'subscriptionModelEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'subscriptionTimePeriod.id',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Subscription Time Period:',
                        description: 'What is the typical time period offered for a subscription to this provider\'s data products?',
                        options: pricingSubscriptionTimePeriod,
                        attributes: {
                            field: 'label',
                        },
                    },
                },
                {
                    key: 'subscriptionTimePeriodEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'monthlyPriceRange.id',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Monthly Price Range:',
                        description: 'What is the price range for a monthly subscription to the provider\'s data offerings?',
                        options: pricingMonthlyPriceRange,
                        attributes: {
                            field: 'label',
                        },
                    },
                },
                {
                    key: 'monthlyPriceRangeEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'minimumYearlyPrice',
                    type: 'input',
                    wrappers: ['form-field-horizontal'],
                    name:'Minimum Yearly Price',
                    templateOptions: {
                        label: 'Minimum Yearly Price:',
                        description: 'What is the minimum price for a yearly subscription to the provider\'s data products?',
                        attributes: {
                            'trackNames': 'Minimum Yearly Price',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input'
                        },
                        keypress: (event) => {
                        },
                        maxLength : 50,
                        change: this.basicInput
                    },
                    hooks: {
                        // onInit(f) {
                        //     console.log(f);
                        //     document.getElementById(f.id)?.setAttribute('maxlength', '5');
                        // },
                        // afterViewInit(f) {
                        //     console.log(f);
                        //     setTimeout(() => {
                        //         document.getElementById(f.id)?.setAttribute('maxlength', '5');
                        //     }, 20000);
                        // }
                    }
                },
                {
                    key: 'minimumYearlyPriceEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'maximumYearlyPrice',
                    type: 'input',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Maximum Yearly Price:',
                        description: 'What is the maximum price for a yearly subscription to the provider\'s data products?',
                        attributes: {
                            'trackNames': 'Maximum Yearly Price',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input'
                        },
                        keypress: (event) => {
                        },
                        maxLength : 50,
                        change: this.basicInput
                    },
                    hooks: {
                        // onInit(f) {
                        //     console.log(f);
                        //     document.getElementById(f.id)?.setAttribute('maxlength', '50');
                        // },
                        // afterViewInit(f) {
                        //     console.log(f);
                        //     setTimeout(() => {
                        //         document.getElementById(f.id)?.setAttribute('maxlength', '50');
                        //     }, 20000);
                        // }
                    }
                },
                {
                    key: 'maximumYearlyPriceEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'pricingDetails',
                    type: 'textarea',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Pricing Details:',
                        description: 'What are the details of the pricing for the provider\'s data products?',
                        attributes: {
                            'trackNames': 'Pricing Details',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input'
                        },
                        change: this.basicInput
                    },
                },
                {
                    key: 'pricingDetailsEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
            ];
                //this.pricingModel = this.dataProvider;
		});

        // setTimeout(() => {
            this.discussionFields = [
                {
                    key: 'useCasesOrQuestionsAddressed',
                    type: 'inputEditor',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Use Cases:',
                        placeholder: '',
                        description: 'Describe some specific use cases for the provider\'s data products or questions that could be answered by them. How did the provider\'s data products demonstrate something that otherwise wouldn\'t have been known? Ultimately, how can clients use the data products to make money?',
                        change: this.basicInput,
                        attributes: {
                            'placeholderText' : 'Specific use cases for the provider\'s data products or questions that could be answered by them. If possible, focus within the finance industry and, specifically, investment research. For example, how do the data products provide insights into the financial performance of public companies, such as topline revenues or earnings?',
                            'trackNames': 'Use Cases or Questions Addressed',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input'
                        },
                    },
                },
                {
                    key: 'useCasesOrQuestionsAddressedEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'discussionAnalytics',
                    type: 'textarea',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Discussion - Analytics:',
                        minRows: 2,
                        maxRows: 100,
                        description: 'Discuss the effort the data provider has put into analyzing, aggregating and/or productizing their data, such as any backtests performed or research/analytics/summary products created.',
                        attributes: {
                            'trackNames': 'Discussion - Analytics',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input',
                            'maximumLength': 255
                        },
                        // maxLength : 255,
                        change: this.basicInput
                    },
                },
                {
                    key: 'discussionAnalyticsEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'potentialDrawbacks',
                    type: 'textarea',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Potential Drawbacks:',
                        minRows: 2,
                        maxRows: 100,
                        placeholder: '',
                        change: this.basicInput,
                        attributes:{
                            'placeholderText' : 'Downsides to this provider\'s offering that decrease the overall appeal of its products for investment research',
                            'trackNames': 'Potential Drawbacks',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input',
                            'maximumLength': 512
                        },
                        // maxLength : 512,
                    },
                },
                {
                    key: 'potentialDrawbacksEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                /* ------------------------------newly added admin form field---------------------------- */
                {
                    key: 'uniqueValueProps',
                    type: 'textarea',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Unique Value Props:',
                        minRows: 2,
                        maxRows: 100,
                        description: 'What makes the data provider or their data products unique or differentiated?',
                        placeholder: '',
                        change: this.basicInput,
                        attributes:{
                            'placeholderText' : 'Unique aspects of this provider\'s offerings that increase the overall appeal of its products for investment research.',
                            'trackNames': 'Unique Value Props',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input',
                            'maximumLength': 1024
                        },
                        // maxLength : 1024,
                    },
                },
                {
                    key: 'legalAndComplianceIssues',
                    type: 'textarea',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Legal & Compliance Issues:',
                        minRows: 2,
                        maxRows: 100,
                        placeholder: '',
                        change: this.basicInput,
                        attributes:{
                            'placeholderText' : 'Glaring or obvious legal or compliance issues to be aware of',
                            'trackNames': 'Legal & Compliance Issues',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input',
                            'maximumLength': 512
                        },
                        // maxLength : 512,
                    },
                },
                /* ---------------------------------------------------------- */
                {
                    key: 'dataRoadmap',
                    type: 'textarea',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Data Roadmap:',
                        minRows: 2,
                        maxRows: 100,
                        description: 'What are the data provider\'s future plans regarding their data and analytics? Any forthcoming new products?',
                        attributes: {
                            'trackNames': 'Data Roadmap',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input',
                            'maximumLength': 512
                        },
                        // maxLength : 512,
                        change: this.basicInput
                    },
                },
                {
                    key: 'dataRoadmapEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
                {
                    key: 'notes',
                    type: 'textarea',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Notes:',
                        minRows: 2,
                        maxRows: 100,
                        placeholder: '',
                        change: this.basicInput,
                        attributes:{
                            'placeholderText' : 'Other further useful notes that don\'t fit into any other field.',
                            'trackNames': 'Notes',
                            'trackCategory': 'Provider Profile Admin - Textbox',
                            'trackLabel': 'Provider Profile Admin - Input',
                            'maximumLength' : 10000
                        },
                        //maxLength : 5,
                    }
                },
                {
                    key: 'notesEdited',
                    type: 'fieldset',
                    templateOptions: {
                        label: ''
                    }
                },
            ];
        // }, 20000);

        // this.filterService.getLookupCodes('SUBSCRIPTION_MODEL').subscribe(data => {
        //     const rSubscription = [];
		// 	const subscriptionModel = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
		// 	subscriptionModel.map((val, index) => {
        //         console.log('......pointer mapping', val);
		// 		rSubscription.push({
		// 			value: val._source.id,
		// 			label: val._source.description
        //         });
		// 	});

        //     this.pricingFields = [
        //         /* {
        //             key: 'pricingModels',
        //             type: 'dropdown',
        //             wrappers: ['form-field-horizontal'],
        //             templateOptions: {
        //                 label: 'Pricing Model:*',
        //                 placeholder: '',
        //                 description: 'What\'s the general model for how this provider prices their data products?',
        //                 required: false,
        //                 options: this.pricingModel,
        //                 attributes: {
        //                     field: 'desc',
        //                     'dropdown': 'true',
        //                     'placeholderText' : 'The general model for how you price your data products.'
        //                 },
        //                 change: (event: any) => {
        //                     const queryObj = event;
        //                     this.pricingModel = [];

        //                     this.filterService.getPricingModel(queryObj.query).subscribe(d => {
        //                         const listArray = [];
        //                         const listItems = (d && d['hits'].total) ? d['hits']['hits'] : [];
        //                         for (let i = 0; i < listItems.length; ++i) {
        //                             const item = listItems[i]._source;

        //                             listArray.push({
        //                                 id: item.id,
        //                                 desc: item.description,
        //                                 locked: item.locked ? true : false
        //                             });
        //                         }
        //                         this.pricingModel = [...listArray];
        //                         this.pricingFields[0].templateOptions.options = this.pricingModel;
        //                     });
        //                 }
        //             },
        //         }, */
        //         /* {
        //             key: 'pricingModel.id',
        //             type: 'radioButton',
        //             wrappers: ['form-field-horizontal'],
        //             templateOptions: {
        //                 label: 'Pricing Model:',
        //                 placeholder: '',
        //                 required: true,
        //                 description: 'What\'s the general model for how this provider prices their data products?',
        //                 options: this.providerPricingModel,
        //                 attributes: {
        //                     'placeholderText' : ''
        //                 },
        //                 change: this.basicInput
        //             },
        //         }, */
        //         {
        //             key: 'dataProvidersPricingModels',
        //             type: 'multiselect',
        //             wrappers: ['form-field-horizontal'],
        //             templateOptions: {
        //                 label: 'Pricing Model:',
        //                 required: true,
        //                 description: 'What pricing models does this provider use to price their data products?',
        //                 options: this.providerPricingModel
        //             },
        //         },
        //         {
        //             key: 'subscriptionModel.id',
        //             type: 'radioButton',
        //             wrappers: ['form-field-horizontal'],
        //             templateOptions: {
        //                 label: 'Subscription Model:',
        //                 description: 'What\'s the general subscription model for this provider\'s data products?',
        //                 options: rSubscription,
        //                 attributes: {
        //                     field: 'label',
        //                 },
        //             },
        //         },
        //         {
        //             key: 'subscriptionTimePeriod.id',
        //             type: 'radioButton',
        //             wrappers: ['form-field-horizontal'],
        //             templateOptions: {
        //                 label: 'Subscription Time Period:',
        //                 description: 'What is the typical time period offered for a subscription to this provider\'s data products?',
        //                 options: this.providerSubscriptionTimePeriod,
        //                 attributes: {
        //                     field: 'label',
        //                 },
        //             },
        //         },
        //         {
        //             key: 'monthlyPriceRange.id',
        //             type: 'radioButton',
        //             wrappers: ['form-field-horizontal'],
        //             templateOptions: {
        //                 label: 'Monthly Price Range:',
        //                 description: 'What is the price range for a monthly subscription to the provider\'s data offerings?',
        //                 options: this.providerMonthlyPriceRange,
        //                 attributes: {
        //                     field: 'label',
        //                 },
        //             },
        //         },
        //         {
        //             key: 'minimumYearlyPrice',
        //             type: 'input',
        //             wrappers: ['form-field-horizontal'],
        //             name:'Minimum Yearly Price',
        //             templateOptions: {
        //                 label: 'Minimum Yearly Price:',
        //                 description: 'What is the minimum price for a yearly subscription to the provider\'s data products?',
        //                 attributes: {
        //                     'trackNames': 'Minimum Yearly Price',
        //                     'trackCategory': 'Provider Profile Admin - Textbox',
        //                     'trackLabel': 'Provider Profile Admin - Input'
        //                 },
        //                 maxLength : 50,
        //                 change: this.basicInput
        //             },
        //         },
        //         {
        //             key: 'maximumYearlyPrice',
        //             type: 'input',
        //             wrappers: ['form-field-horizontal'],
        //             templateOptions: {
        //                 label: 'Maximum Yearly Price:',
        //                 description: 'What is the maximum price for a yearly subscription to the provider\'s data products?',
        //                 attributes: {
        //                     'trackNames': 'Maximum Yearly Price',
        //                     'trackCategory': 'Provider Profile Admin - Textbox',
        //                     'trackLabel': 'Provider Profile Admin - Input'
        //                 },
        //                 maxLength : 50,
        //                 change: this.basicInput
        //             },
        //         },
        //         {
        //             key: 'pricingDetails',
        //             type: 'textarea',
        //             wrappers: ['form-field-horizontal'],
        //             templateOptions: {
        //                 label: 'Pricing Details:',
        //                 description: 'What are the details of the pricing for the provider\'s data products?',
        //                 attributes: {
        //                     'trackNames': 'Pricing Details',
        //                     'trackCategory': 'Provider Profile Admin - Textbox',
        //                     'trackLabel': 'Provider Profile Admin - Input'
        //                 },
        //                 change: this.basicInput
        //             },
        //         },
        //     ];
        //     this.pricingModel = this.dataProvider;
        // });
	}
	ngAfterViewInit() {
		const uppy = this.uppyService.uppy;
	}

	getLinkImgUrl(desc) {
		if (desc) {
			const fileName = ((desc).toLowerCase()).replace(' ', '_');
			const link = 'content/images/' + fileName  + '.png';
			return link;
		}
	}

	providerProfileTab(event: MatTabChangeEvent) {

	   /*  this.profileTabLabel = 'Provider Profile - Tab ' + event.tab.textLabel;
		this.ProfileCategory = 'Provider Profile ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent(this.ProfileCategory, 'Tab Link Clicked', this.profileTabLabel); */
	}

	openOrganizationDialog(events: any) {
		if(events === 'editOrganization') {
			if(this.orgID) {
				console.log('ORGID', this.orgID);
				this.dataProviderService.findByOrganizationId(this.orgID).subscribe(dataProvider => {
					this.loaderService.display(false);
					this.organizationList = dataProvider['hits'] ? dataProvider['hits'].hits[0]._source : null;
				});
		    } else if(this.organizationID !== null){
				console.log('organizationID', this.organizationID);
				this.dataProviderService.findByOrganizationId(this.organizationID).subscribe(dataProvider => {
					this.loaderService.display(false);
					this.organizationList = dataProvider['hits'] ? dataProvider['hits'].hits[0]._source : null;
				});
			}
			console.log('organizationList', this.organizationList);
			setTimeout(() => {
				this.editOrgDialogRef = this.dialog.open(DataOrganizationAdminFormComponent, {
					width: '1000px',
					disableClose: true
				});
				this.editOrgDialogRef.componentInstance.basicModel = this.organizationList;
				this.editOrgDialogRef.componentInstance.basicProvider = this.dataProviderId;
				this.editOrgDialogRef.componentInstance.providerRecId = this.providerRecordID;
				this.editOrgDialogRef.componentInstance.submissionMessage = false;
				this.editOrgDialogRef.afterClosed().subscribe(result=> {
					console.log('Result', result);
					this.basicModel = {
                        ...this.basicModel,
                        ownerOrganization: {id: result.id, name: result.name}
                    }
				});
				//console.log('Instance', this.orgDialogRef.componentInstance);
			}, 500);
		}
		else if(events === 'addOrganization') {
			setTimeout(() => {
				this.addOrgDialogRef = this.dialog.open(DataOrganizationAdminFormComponent, {
					width: '1000px',
					disableClose: true
				});
				this.addOrgDialogRef.componentInstance.basicProvider = this.dataProviderId;
				this.addOrgDialogRef.componentInstance.submissionMessage = true;
				this.addOrgDialogRef.afterClosed().subscribe(result=> {
					if(result) {
						this.emptyField = false;
						this.basicModel = {
							...this.basicModel,
							ownerOrganization: {id: result.id, name: result.name}
						}
				    }
				});
			}, 1000);
		}
    }

	providerFollow(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	unlockProviders(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	requestMoreInfo(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	providerPartnerTab(event: MatTabChangeEvent) {
		this.profileTabLabel = 'Provider Profile -  ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent('Partner Provider', 'Internal Link Clicked', this.profileTabLabel);
	}

	dataFieldLink(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	profileWebsite(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	profileArticle(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
	samplesTab(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    addResource() {
        this.orgDialogRef = this.dialog.open(AddResourceAdminFormComponent, {
            width: '1200px',
            disableClose: true
		});
		this.orgDialogRef.componentInstance.basicProvider = this.dataProviderId;
		this.orgDialogRef.componentInstance.dataName = this.dataProviderName;
		this.orgDialogRef.componentInstance.providerRecordID = this.providerRecordID;
		this.orgDialogRef.componentInstance.submissionMessage = true;
		this.orgDialogRef.afterClosed().subscribe(result=> {
			console.log('result',result);
			this.filterService.findByResourceId(this.dataProviderId).subscribe(data => {
				this.resources = [];
				this.resourceLink=[];
				this.dataRelevantFiles=[];
				this.dataRelevantLinks=[];
					console.log('Data Resource',data);
					if(data['hits'] && data['hits'].hits) {
						for (let i = 0; i < data['hits'].hits.length; i++) {
							this.resourceObj = data['hits'].hits[i]._source;
							//console.log('Resource Obj', this.resourceObj);
							this.resources.push(this.resourceObj);
							//console.log('Resource ', this.resources);
							if (this.resourceObj.link !== null) {
								this.resourceLink.push(this.resourceObj.link);
							}
							//console.log('Link', this.resourceLink);
						}
					}
					this.resources.forEach(item => {
						//console.log('Item', item);
						this.itemID = item.id;
						console.log('Link', item.link);
						const obj = item;
						if(obj.link) {
							//console.log('OBJECT LINK',obj.link);
							obj.link['resourceID'] = obj.id;
							obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
							obj.link['resLinkName'] = item.name;
							this.dataRelevantLinks.push(obj.link);
						}
						if(obj.file) {
							//console.log('OBJECT FILE',obj.file);
							obj.file['resourceID'] = obj.id;
							obj.file['resFileName'] = item.name;
							this.dataRelevantFiles.push(obj.file);
						}
					});
					//console.log('Data Relevant Links', this.dataRelevantLinks);
					//console.log('Data Relevant Files', this.dataRelevantFiles);
					this.resourceModel = this.dataResources;
				});
		});
    }
    editResource() {
        this.orgDialogRef = this.dialog.open(AddResourceAdminFormComponent, {
            width: '1000px',
            disableClose: false
        });
	}
	editFile(editFileId?: any) {
		this.filterService.findResourceByResourceId(editFileId).subscribe(data => {
			this.organizationList = data['hits'] && data['hits'].hits[0] ? data['hits'].hits[0]._source : null;
		});
		setTimeout(() => {
			this.resourceDialogRef = this.dialog.open(AddResourceAdminFormComponent, {
				width: '1200px',
				disableClose: false
			});
			this.resourceDialogRef.componentInstance.basicModel = this.organizationList;
			this.resourceDialogRef.componentInstance.submissionMessage = false;
			this.resourceDialogRef.afterClosed().subscribe(result=> {
				console.log('result',result);
				this.filterService.findByResourceId(this.dataProviderId).subscribe(data => {
				this.resources = [];
				this.resourceLink=[];
				this.dataRelevantFiles=[];
				this.dataRelevantLinks=[];
					console.log('Data Resource',data);
					if(data['hits'] && data['hits'].hits) {
						for (let i = 0; i < data['hits'].hits.length; i++) {
							this.resourceObj = data['hits'].hits[i]._source;
							//console.log('Resource Obj', this.resourceObj);
							this.resources.push(this.resourceObj);
							//console.log('Resource ', this.resources);
							if (this.resourceObj.link !== null) {
								this.resourceLink.push(this.resourceObj.link);
							}
							//console.log('Link', this.resourceLink);
						}
					}
					this.resources.forEach(item => {
						console.log('Item', item);
						this.itemID = item.id;
						console.log('Link', item.link);
						const obj = item;
						if(obj.link) {
							//console.log('OBJECT LINK',obj.link);
							obj.link['resourceID'] = obj.id;
							obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
							obj.link['resLinkName'] = item.name;
							this.dataRelevantLinks.push(obj.link);
							console.log("Relevant Links", this.dataRelevantLinks);
						}
						if(obj.file) {
							//console.log('OBJECT FILE',obj.file);
							obj.file['resourceID'] = obj.id;
							obj.file['resFileName'] = item.name;
							this.dataRelevantFiles.push(obj.file);
							console.log("Relevant Files", this.dataRelevantFiles);
						}
					});
					//console.log('Data Relevant Links', this.dataRelevantLinks);
					//console.log('Data Relevant Files', this.dataRelevantFiles);
					this.resourceModel = this.dataResources;
				});
			});
		}, 1000);
    }
    // editLink(editLinkId?: any) {
	// 	console.log(editLinkId);
	// 	this.resourceDialogRef = this.dialog.open(AddResourceAdminFormComponent, {
	// 		width: '1200px',
	// 		disableClose: false
	// 	});
	// 	this.resourceDialogRef.componentInstance.resourceID = editLinkId;
	// 	/* this.filterService.findByLinkId(editLinkId).subscribe(data => {
	// 		console.log('data', data);
	// 		this.organizationList = data['hits'] ? data['hits'].hits[0]._source : null;
	// 		console.log('List', this.organizationList);
	// 	}); */
	// }
	editLink(editLinkId?: any) {
		this.filterService.findResourceByResourceId(editLinkId).subscribe(data => {
			this.organizationList = data['hits'] && data['hits'].hits[0] ? data['hits'].hits[0]._source : null;
		});
		setTimeout(() => {
			this.resourceDialogRef = this.dialog.open(AddResourceAdminFormComponent, {
				width: '1200px',
				disableClose: false
			});
			this.resourceDialogRef.componentInstance.basicModel = this.organizationList;
			this.resourceDialogRef.componentInstance.submissionMessage = false;
			this.resourceDialogRef.afterClosed().subscribe(result=> {
				console.log('result',result);
				this.filterService.findByResourceId(this.dataProviderId).subscribe(data => {
				this.resources = [];
				this.resourceLink=[];
				this.dataRelevantFiles=[];
				this.dataRelevantLinks=[];
					console.log('Data Resource',data);
					if(data['hits'] && data['hits'].hits) {
						for (let i = 0; i < data['hits'].hits.length; i++) {
							this.resourceObj = data['hits'].hits[i]._source;
							//console.log('Resource Obj', this.resourceObj);
							this.resources.push(this.resourceObj);
							//console.log('Resource ', this.resources);
							if (this.resourceObj.link !== null) {
								this.resourceLink.push(this.resourceObj.link);
							}
							//console.log('Link', this.resourceLink);
						}
					}
					this.resources.forEach(item => {
						console.log('Item', item);
						this.itemID = item.id;
						console.log('Link', item.link);
						const obj = item;
						if(obj.link) {
							//console.log('OBJECT LINK',obj.link);
							obj.link['resourceID'] = obj.id;
							obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
							obj.link['resLinkName'] = item.name;
							this.dataRelevantLinks.push(obj.link);
							console.log("Relevant Links", this.dataRelevantLinks);
						}
						if(obj.file) {
							//console.log('OBJECT FILE',obj.file);
							obj.file['resourceID'] = obj.id;
							obj.file['resFileName'] = item.name;
							this.dataRelevantFiles.push(obj.file);
							console.log("Relevant Files", this.dataRelevantFiles);
						}
					});
					//console.log('Data Relevant Links', this.dataRelevantLinks);
					//console.log('Data Relevant Files', this.dataRelevantFiles);
					this.resourceModel = this.dataResources;
				});
			});
		}, 1000);
    }

    loadEditedData() {
        this.dataProviderService.editedDatafindByRecId(this.recordID).subscribe(editedDataProvider => {
            console.log('Edited Data',editedDataProvider);
            this.editedDataByUser = editedDataProvider;
            this.approvedProviderData = { ...editedDataProvider };
            const obj = Object.keys(this.editedDataByUser);
            // console.log(obj, typeof(obj));
            obj.forEach((key, index) => {
                // console.log(this.editedDataByUser[key], typeof(this.editedDataByUser[key]));
                // && Object.prototype.toString.call(this.editedDataByUser[key]) !== '[object Array]'
                if(this.editedDataByUser[key] !== null) {
                    switch(key) {
                        case 'mainDataCategory':
                            // console.log(key);
                            const mainDataCategory = this.editedDataByUser['mainDataCategory'];
                            delete this.editedDataByUser['mainDataCategory'];
                            this.editedDataByUser['mainDataCategory' + 'Edited'] = mainDataCategory?.datacategory;
                            break;
                        case 'mainDataIndustry':
                            // console.log(key);
                            const mainDataIndustry = this.editedDataByUser['mainDataIndustry'];
                            delete this.editedDataByUser['mainDataIndustry'];
                            this.editedDataByUser['mainDataIndustry' + 'Edited'] = mainDataIndustry?.dataIndustry;
                            break;
                        case 'providerType':
                            // console.log(key);
                            const providerType = this.editedDataByUser['providerType'];
                            delete this.editedDataByUser['providerType'];
                            this.editedDataByUser['providerType' + 'Edited'] = providerType?.providerType;
                            break;
                        default:
                            if (typeof(this.editedDataByUser[key]) !== 'string' && typeof(this.editedDataByUser[key]) !== 'number'
                            && typeof(this.editedDataByUser[key]) !== 'boolean' && Object.prototype.toString.call(this.editedDataByUser[key]) !== '[object Array]') {
                                // console.log(key);
                                const lookupcode = this.editedDataByUser[key];
                                // console.log(lookupcode);
                                delete this.editedDataByUser[key];
                                this.editedDataByUser[key + 'Edited'] = lookupcode?.lookupcode;
                                break;
                            } else if(Array.isArray(this.editedDataByUser[key])) {
                                const newArr = this.editedDataByUser[key];
                                console.log('NewArr', newArr);
                                delete this.editedDataByUser[key];
                                let finalArr = [];
                                newArr.forEach(element => {
                                    if(element.desc) {
                                        finalArr.push(element.desc);
                                    } else if(element.lookupcode) {
                                        finalArr.push(element.lookupcode);
                                    }
                                });
                                console.log('FinalArr', finalArr);
                                this.editedDataByUser[key + 'Edited'] = finalArr;
                                console.log(this.editedDataByUser);
                                break;
                            } else if(typeof(this.editedDataByUser[key]) === 'string') {
                                // console.log(key);
                                const newData = this.editedDataByUser[key];
                                // console.log(newData);
                                delete this.editedDataByUser[key];
                                this.editedDataByUser[key + 'Edited'] = newData;
                                break;
                            } else if(typeof(this.editedDataByUser[key]) === 'number') {
                                // console.log(key);
                                const newData = this.editedDataByUser[key];
                                // console.log(newData);
                                delete this.editedDataByUser[key];
                                this.editedDataByUser[key + 'Edited'] = newData;
                                break;
                            } else if(typeof(this.editedDataByUser[key]) === 'boolean') {
                                // console.log(key);
                                const newData = this.editedDataByUser[key];
                                // console.log(newData);
                                delete this.editedDataByUser[key];
                                this.editedDataByUser[key + 'Edited'] = newData;
                                // console.log(this.editedDataByUser[key], this.editedDataByUser);
                                break;
                            }
                    }
                }
            });
        });
    }

    approveAllChanges() {
        this.approveSpinner = true;
        this.approvedData = { ...this.dataProvider };
        delete this.approvedData['editedData'];
        // console.log(this.editedDataByUser);
        // console.log(this.approvedProviderData);
        Object.entries(this.editedDataByUser).forEach(([key, value]) => {
            // console.log(key, Array.isArray(this.editedDataByUser[key]), this.editedDataByUser[key]?.length > 0, key !== 'dataProvidersPricingModels');
            if (this.editedDataByUser[key] !== null && this.editedDataByUser[key] !== undefined) {
                switch(true) {
                    case typeof(this.editedDataByUser[key]) === 'string':
                        // delete this.approvedData[key];
                        const newStrKey = key.replace('Edited', '');
                        this.approvedData[newStrKey] = this.approvedProviderData[newStrKey];
                        console.log('String', newStrKey, value, this.approvedData[newStrKey]);
                        // console.log('Object', key, value, this.editedDataByUser[key], this.approvedProviderData[key], this.approvedData);
                        break;
                    case typeof(this.editedDataByUser[key]) === 'number':
                        // delete this.approvedData[key];
                        const newNumKey = key.replace('Edited', '');
                        this.approvedData[newNumKey] = value;
                        console.log('number', newNumKey, value, this.approvedData[newNumKey]);
                        break;
                    case typeof(this.editedDataByUser[key]) === 'boolean':
                        // delete this.approvedData[key];
                        const newBooKey = key.replace('Edited', '');
                        this.approvedData[newBooKey] = value;
                        console.log('number', newBooKey, value, this.approvedData[newBooKey]);
                        break;
                    case typeof(this.approvedProviderData[key.replace('Edited', '')]) !== 'string' && typeof(this.approvedProviderData[key.replace('Edited', '')]) !== 'number'
                    && Object.prototype.toString.call(this.approvedProviderData[key.replace('Edited', '')]) !== '[object Array]' && typeof(this.approvedProviderData[key.replace('Edited', '')]) === 'object':
                        // delete this.approvedData[key];
                        const newObjKey = key.replace('Edited', '');
                        if (newObjKey === 'providerType') {
                            console.log('Key', newObjKey);
                            delete this.approvedData.providerType;
                            this.approvedData['providerType'] = {
                                id: this.approvedProviderData[newObjKey]?.id
                            };
                            console.log('Key', newObjKey, this.approvedData['providerType']);
                        } else if (newObjKey !== 'providerType') {
                            console.log('Key 2', newObjKey);
                            delete this.approvedData[newObjKey];
                            this.approvedData[newObjKey] = {
                                id: this.approvedProviderData[newObjKey]?.id
                            };
                        }
                        console.log('Object', newObjKey, value, this.editedDataByUser[newObjKey], this.approvedProviderData[newObjKey], this.approvedData);
                        break;
                    case Object.prototype.toString.call(this.editedDataByUser[key]) === '[object Array]' && this.editedDataByUser[key]?.length > 0 && key !== 'dataProvidersPricingModels':
                        console.log('Array', this.approvedData[key], key, value);
                        const newKey = key.replace('Edited', '');
                        console.log(this.approvedProviderData[newKey]);
                        let newArr = [];
                        let lastValue = [];
                        let oldValue = [];
                        const newValue = newArr.concat(this.approvedProviderData[newKey]);
                        console.log('Array 1', newValue);
                        // if (newValue.length > 0) {
                            newValue.map(item => {
                                lastValue.push({
                                    id: item?.id,
                                    desc: item && item.desc ? item.desc : item.lookupcode
                                });
                            });
                            this.approvedData[newKey] = lastValue;
                            console.log('Array 2', this.approvedData[newKey], lastValue);
                        // } else {
                        //     this.approvedData[key].map(item => {
                        //         oldValue.push({
                        //             id: item?.id,
                        //             desc: item && item.desc ? item.desc : item.lookupcode
                        //         });
                        //     });
                        //     this.approvedData[key] = oldValue;
                        //     console.log('Array 3', this.approvedData[key], oldValue);
                        // }
                        // console.log('Array 2', this.approvedData[key], lastValue);
                        // const combinedArr = [];
                        // combinedArr.push(...this.approvedData[key], ...lastValue);
                        // console.log('Array 3', combinedArr);
                        // delete this.approvedData[key];
                        console.log('Array 4', this.approvedData[newKey]);
                        console.log('Array 5', newKey, this.approvedData[newKey], value, lastValue);
                        break;
                }
            }
        });
        console.log(this.approvedData);
        delete this.approvedData.editCompetitorProvider;
        delete this.approvedData.editComplementaryDataProviders;
        delete this.approvedData.editConsumerDataPartner;
        delete this.approvedData.editDistributionDataPartner;
        delete this.approvedData.editProviderDataPartner;
        delete this.approvedData.user;
        delete this.approvedData.emailAddress;
        delete this.approvedData.privacy;
        delete this.approvedData.logo;
        this.approvedData['privacy'] = this.dataProviderPrivacy;
        console.log(this.approvedData['privacy'], this.dataProviderPrivacy);
        this.dataProviderService.approveAllProviderData(this.approvedData).subscribe(result => {
            console.log(result);
            this.approveSpinner = false;
            this.formSubmitted = true;
            if(result) {
                this.openSnackBar('All of the Provider’s edits will be immediately applied to this Provider’s Data Profile.', 'Close');
                this.stateService.storeUrl(null);
                this.router.navigate(['../../', result.recordID], { relativeTo: this.route });
            }
        }, error => {
            this.approveSpinner = false;
            this.formSubmitted = false;
            this.openSnackBar('Form Failed to save.', 'Close');
        });
    }

	load(recordID) {
		this.dataProviderService.findByIdEditForm(recordID).subscribe(dataProvider => {
            //console.log('dataProvider', dataProvider);
			this.loaderService.display(false);
			this.dataProvider = dataProvider['hits'].total ? dataProvider['hits'].hits[0]._source : null;
            // this.approvedData = this.dataProvider;
			this.dataProviderId = this.dataProvider ? this.dataProvider.id : null;
			this.providerRecordID = this.dataProvider ? this.dataProvider.recordID : null;
			this.dataProviderName = this.dataProvider ? this.dataProvider.providerName : null;
            this.dataProviderPrivacy = this.dataProvider?.privacy;
            // this.approveChangesBtn = this.dataProvider?.user.length > 0 ? true : false;
			// setTimeout(() => {
			// 	this.providerLogo = this.dataProvider && this.dataProvider.logo && this.dataProvider.logo.logo ? this.dataProvider.logo.logo : '';
			// 	this.basicFields[2].templateOptions.attributes['logo'] = this.providerLogo;
			// 	console.log('Logo', this.providerLogo);
			// }, 4000);
			// this.distributionPartnersArr = this.dataProvider && this.dataProvider.distributionPartners ? this.dataProvider.distributionPartners : null;
			// console.log('distributionPartnersArr', this.distributionPartnersArr);
			//this.relavantLinks = this.dataProvider ? this.dataProvider.link : null;
			//console.log('Links.....', this.relavantLinks);
			//console.log('Provider Id', this.dataProviderId);
			//console.log( this.dataProvider.ownerOrganization.name);

			//console.log('Provider Id', this.dataProviderId);
			this.filterService.findByResourceId(this.dataProviderId).subscribe(data => {
			//	console.log('Data Resource',data);
				if(data['hits'] && data['hits'].hits) {
					for (let i = 0; i < data['hits'].hits.length; i++) {
						this.resourceObj = data['hits'].hits[i]._source;
					//	console.log('Resource Obj', this.resourceObj);
						this.resources.push(this.resourceObj);
					//	console.log('Resource ', this.resources);
						if (this.resourceObj.link !== null) {
							this.resourceLink.push(this.resourceObj.link);
						}
					//	console.log('Link', this.resourceLink);
					}
				}
				this.resources.forEach(item => {
				//	console.log('Item', item);
					this.itemID = item.id;
				//	console.log('Link', item.link);
					const obj = item;
					if(obj.link) {
					//	console.log('OBJECT LINK',obj.link);
						obj.link['resourceID'] = obj.id;
						obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
						obj.link['resLinkName'] = item.name;
						this.dataRelevantLinks.push(obj.link);
					}
					if(obj.file) {
					//	console.log('OBJECT FILE',obj.file);
						obj.file['resourceID'] = obj.id;
						obj.file['resFileName'] = item.name;
						this.dataRelevantFiles.push(obj.file);
					}
				});
			//	console.log('Data Relevant Links', this.dataRelevantLinks);
			//	console.log('Data Relevant Files', this.dataRelevantFiles);
				this.resourceModel = this.dataResources;
			});
			this.organizationID = this.dataProvider.ownerOrganization && this.dataProvider.ownerOrganization.id? this.dataProvider.ownerOrganization.id : null;
			//console.log('LOADORGID',this.organizationID);
            if(this.dataProvider.ownerOrganization && this.dataProvider.ownerOrganization.id) {
				this.organizationButton = true;
				this.emptyField = false;
            } else {
				this.organizationButton = false;
				this.emptyField = true;
            }
			this.isLoading = false;
			this.constructFormModel(this.dataProvider);
            this.companyDetailsModel = this.dataProvider;
            this.pricingModel = this.dataProvider;
            //console.log('Company',this.companyDetailsModel);
            //console.log('city',this.dataProvider.mainDataCategory.datacategory);
            /* if(this.dataProvider.ownerOrganization.city === 'CITY_NULL'){
                this.dataProvider.ownerOrganization.city = '';
            }
            if(this.dataProvider.ownerOrganization.state === 'STATE_NULL'){
                this.dataProvider.ownerOrganization.state = '';
            }
            if(this.dataProvider.ownerOrganization.country === 'COUNTRY_NULL'){
                this.dataProvider.ownerOrganization.country = '';
            }
            if(this.dataProvider.ownerOrganization.zipCode === 'ZIPCODE_NULL'){
                this.dataProvider.ownerOrganization.zipCode = '';
            }
            if(this.dataProvider.ownerOrganization.website === 'DEFAULT_NULL'){
                this.dataProvider.ownerOrganization.website = '';
            } */
            if (this.dataProvider?.emailAddress?.length > 0) {
                this.emailAddressCount = this.dataProvider.emailAddress.length;
				this.numberOfAnalystEmployeesFields[2].templateOptions.label = 'Email Addresses('+this.emailAddressCount+'):';
				const emailAddress = [];
				for (const obj in this.dataProvider.emailAddress) {
					if (obj) {
						// const partnerName = !this.dataProvider.providerPartners[obj].locked ? this.dataProvider.providerPartners[obj].providerName : 'Provider Locked';
						emailAddress.push({
							id: this.dataProvider.emailAddress[obj].id,
							emailAddress: this.dataProvider.emailAddress[obj].emailAddress
						});
					}
				}
				delete this.dataProvider.emailAddress;
				this.dataProvider.emailAddress = emailAddress;
            }
            if (this.dataProvider?.user?.length > 0) {
                this.usersCount = this.dataProvider.user.length;
				this.numberOfAnalystEmployeesFields[3].templateOptions.label = 'Users('+this.usersCount+'):';
				const user = [];
                this.dataProvider.user.map(item => {
                    user.push({
                        user: item
                    })
                });
				delete this.dataProvider.user;
				this.dataProvider.user = user;
            }
            if(this.dataProvider.ownerOrganization && this.dataProvider.ownerOrganization.name === 'NAME_NULL'){
                this.dataProvider.ownerOrganization.name = '';
			}
			if(this.dataProvider.dateCollectionBegan) {
					const setDate = new Date(this.dataProvider.dateCollectionBegan);
					delete this.dataProvider.dateCollectionBegan;
					this.dataProvider.dateCollectionBegan = setDate;
				//	console.log('setDate', setDate);

			}
			if(this.dataProvider.ownerOrganization === null) {
				  this.basicExpansionPanel = false;
				  this.companyExpansionPanel = true;
			}
            if(this.dataProvider.mainDataIndustry && this.dataProvider.mainDataIndustry.dataIndustry === 'DATA_INDUSTRY_NULL'){
                this.dataProvider.mainDataIndustry.dataIndustry = '';
            }
            if(this.dataProvider.mainDataCategory && this.dataProvider.mainDataCategory.datacategory === 'DATA_CATEGORY_NULL'){
                this.dataProvider.mainDataCategory.datacategory = '';
			}
			if (this.dataProvider.providerPartners) {
                this.providerPartnersCount = this.dataProvider.providerPartners.length;
				this.relatedProvidersFields[0].templateOptions.label = 'Provider Data Partners ('+this.providerPartnersCount+'):';
				const providerPartners = [];
				for (const obj in this.dataProvider.providerPartners) {
					if (obj) {
						const partnerName = !this.dataProvider.providerPartners[obj].locked ? this.dataProvider.providerPartners[obj].providerName : 'Provider Locked';
						providerPartners.push({
							id: this.dataProvider.providerPartners[obj].id,
							providerName: partnerName
						});
					}
				}
				delete this.dataProvider.providerPartners;
				this.dataProvider.providerPartners = providerPartners;
            }

            if (this.dataProvider.distributionPartners) {
                this.distributionPartnersCount = this.dataProvider.distributionPartners.length;
				this.relatedProvidersFields[1].templateOptions.label = 'Distribution Data Partners ('+this.distributionPartnersCount+'):';
				const distributionPartners = [];
				for (const obj in this.dataProvider.distributionPartners) {
					if (obj) {
						const partnerName = !this.dataProvider.distributionPartners[obj].locked ? this.dataProvider.distributionPartners[obj].providerName : 'Provider Locked';
						distributionPartners.push({
							id: this.dataProvider.distributionPartners[obj].id,
							providerName: partnerName
						});
					}
				}
				delete this.dataProvider.distributionPartners;
				this.dataProvider.distributionPartners = distributionPartners;
            }

            if (this.dataProvider.consumerPartners) {
                this.consumerPartnersCount = this.dataProvider.consumerPartners.length;
				this.relatedProvidersFields[2].templateOptions.label = 'Consumer Data Partners ('+this.consumerPartnersCount+'):';
				const consumerPartners = [];
				for (const obj in this.dataProvider.consumerPartners) {
					if (obj) {
						const partnerName = !this.dataProvider.consumerPartners[obj].locked ? this.dataProvider.consumerPartners[obj].providerName : 'Provider Locked';
						consumerPartners.push({
							id: this.dataProvider.consumerPartners[obj].id,
							providerName: partnerName
						});
					}
				}
				delete this.dataProvider.consumerPartners;
				this.dataProvider.consumerPartners = consumerPartners;
            }

            if (this.dataProvider.competitors) {
                this.competitorsCount = this.dataProvider.competitors.length;
				this.relatedProvidersFields[3].templateOptions.label = 'Competitors ('+this.competitorsCount+'):';
				const competitors = [];
				for (const obj in this.dataProvider.competitors) {
					if (obj) {
						const partnerName = !this.dataProvider.competitors[obj].locked ? this.dataProvider.competitors[obj].providerName : 'Provider Locked';
						competitors.push({
							id: this.dataProvider.competitors[obj].id,
							providerName: partnerName
						});
					}
				}
				delete this.dataProvider.competitors;
				this.dataProvider.competitors = competitors;
            }

        //    console.log('loading');
			if (this.dataProvider.geographicalFoci) {
				const geographicalFoci = [];
				for (const obj in this.dataProvider.geographicalFoci) {
					if (obj) {
						geographicalFoci.push({
							id: this.dataProvider.geographicalFoci[obj].id,
							desc: this.dataProvider.geographicalFoci[obj].description
						});
					}
				}
				delete this.dataProvider.geographicalFoci;
				this.dataProvider.geographicalFoci = geographicalFoci;
			}
			if (this.dataProvider.researchMethods) {
				//console.log('Research Methods',this.dataProvider.researchMethods)
				const researchMethods = [];
				for (const obj in this.dataProvider.researchMethods) {
					if (obj) {
						let changedId = parseInt(this.dataProvider.researchMethods[obj].id, 10);
						//console.log('Research Methods id', changedId);
						researchMethods.push({
							id: changedId,
							desc: this.dataProvider.researchMethods[obj].desc
						});
						//console.log('0', researchMethods[0]);
					}
				}
				delete this.dataProvider.researchMethods;
				//console.log('Research Methods 1', this.dataProvider.researchMethods);
				this.dataProvider.researchMethods = researchMethods;
				console.log('Research Methods 2', this.dataProvider.researchMethods);
			}
            if (this.dataProvider.assetClasses) {
				const assetClasses = [];
				for (const obj in this.dataProvider.assetClasses) {
					if (obj) {
						// console.log('assetClassesObj', this.dataProvider.assetClasses[obj]);
						let assetClassesId = parseInt(this.dataProvider.assetClasses[obj].id, 10);
                        // console.log('assetClassesId 1', assetClassesId);
                        assetClasses.push({
							id: assetClassesId,
							desc: this.dataProvider.assetClasses[obj].desc
						});
                        // console.log('0', assetClasses[0]);
					}
				}
				delete this.dataProvider.assetClasses;
                // console.log('Deleted', this.dataProvider.assetClasses);
				this.dataProvider.assetClasses = assetClasses;
                // console.log('Added', this.dataProvider.assetClasses);
			}
			if (this.dataProvider.securityTypes) {
				const securityTypes = [];
				for (const obj in this.dataProvider.securityTypes) {
					if (obj) {
						console.log('securityTypesId', this.dataProvider.securityTypes[obj]);
						console.log('securitydesc', this.dataProvider.securityTypes[obj].desc);
						const securityTypesId = parseInt(this.dataProvider.securityTypes[obj]['id'],10);
                    //    console.log('securityTypesId 1', securityTypesId);
						securityTypes.push({
							id: securityTypesId,
							desc: this.dataProvider.securityTypes[obj].desc
						});
					}
				}
				delete this.dataProvider.securityTypes;
				this.dataProvider.securityTypes = securityTypes;
			}
			if (this.dataProvider.relevantSectors) {
				const relevantSectors = [];
				for (const obj in this.dataProvider.relevantSectors) {
					if (obj) {
						//console.log('Object', this.dataProvider.relevantSectors[obj]);
						const relevantSectorsId = parseInt(this.dataProvider.relevantSectors[obj]['id'],10);
                        //console.log('Object 1', relevantSectorsId);
						relevantSectors.push({
							id: relevantSectorsId,
							desc: this.dataProvider.relevantSectors[obj].desc
						});
					}
				}
				delete this.dataProvider.relevantSectors;
				this.dataProvider.relevantSectors = relevantSectors;
			}

			if (this.dataProvider.sources) {
				const sources = [];
				for (const obj in this.dataProvider.sources) {
					if (obj) {
						console.log('Sources', this.dataProvider.sources[obj]);
						let sourcesId = parseInt(this.dataProvider.sources[obj].id, 10);
                        //console.log('Sources 1', sourcesId);
						sources.push({
							id: sourcesId,
							desc: this.dataProvider.sources[obj].desc
						});
					}
				}
				console.log('Sources', sources);

				delete this.dataProvider.sources;
				this.dataProvider.sources = sources;
			}
			if (this.dataProvider.accessOffered) {
				const accessOffered = [];
				for (const obj in this.dataProvider.accessOffered) {
					if (obj) {
						accessOffered.push({
							id: this.dataProvider.accessOffered[obj].id,
							desc: this.dataProvider.accessOffered[obj].description
						});
					}
				}
				delete this.dataProvider.accessOffered;
				this.dataProvider.accessOffered = accessOffered;
			}
			if (this.dataProvider.dataProductTypes) {
				const dataProductTypes = [];
				for (const obj in this.dataProvider.dataProductTypes) {
					if (obj) {
						dataProductTypes.push({
							id: this.dataProvider.dataProductTypes[obj].id,
							desc: this.dataProvider.dataProductTypes[obj].description
						});
					}
				}
				delete this.dataProvider.dataProductTypes;
				this.dataProvider.dataProductTypes = dataProductTypes;
			}
			if (this.dataProvider.dataTypes) {
				const dataTypes = [];
				for (const obj in this.dataProvider.dataTypes) {
					if (obj) {
						dataTypes.push({
							id: this.dataProvider.dataTypes[obj].id,
							desc: this.dataProvider.dataTypes[obj].description
						});
					}
				}
				delete this.dataProvider.dataTypes;
				this.dataProvider.dataTypes = dataTypes;
			}
			if (this.dataProvider.languagesAvailable) {
				const languagesAvailable = [];
				for (const obj in this.dataProvider.languagesAvailable) {
					if (obj) {
						languagesAvailable.push({
							id: this.dataProvider.languagesAvailable[obj].id,
							desc: this.dataProvider.languagesAvailable[obj].description
						});
					}
				}
				delete this.dataProvider.languagesAvailable;
				this.dataProvider.languagesAvailable = languagesAvailable;
			}
			if (this.dataProvider.organizationType) {
				const organizationType = [];
				for (const obj in this.dataProvider.organizationType) {
					if (obj) {
						organizationType.push({
							id: this.dataProvider.organizationType[obj].id,
							desc: this.dataProvider.organizationType[obj].description
						});
					}
				}
				delete this.dataProvider.organizationType;
				this.dataProvider.organizationType = organizationType;
			}
			if (this.dataProvider.deliveryMethods) {
			//	console.log('Methods Response',this.dataProvider.deliveryMethods);
				const deliveryMethods = [];
				for (const obj in this.dataProvider.deliveryMethods) {
				const methodsId = parseInt(this.dataProvider.deliveryMethods[obj]['id'],10);
			//	console.log('Methods Id', methodsId);
                    if (obj) {
			//			console.log('Methods Obj', obj);
						deliveryMethods.push({
							id: methodsId,
						    desc: this.dataProvider.deliveryMethods[obj]['desc']
						});
					}
				}
				delete this.dataProvider.deliveryMethods;
				this.dataProvider.deliveryMethods = deliveryMethods;
			}
			if (this.dataProvider.deliveryFormats) {
			//	console.log('Formats Response',this.dataProvider.deliveryFormats);
				const deliveryFormats = [];
				for (const obj in this.dataProvider.deliveryFormats) {
					if (obj) {
			//			console.log('Formats Obj',obj);
						const formatsId = parseInt(this.dataProvider.deliveryFormats[obj]['id'],10);
			//			console.log('Formats Id', formatsId);
						deliveryFormats.push({
							id: formatsId,
						    desc: this.dataProvider.deliveryFormats[obj]['desc']
						});
					}
				}
				delete this.dataProvider.deliveryFormats;
				this.dataProvider.deliveryFormats = deliveryFormats;
			}
			if (this.dataProvider.managerType) {
				const managerType = [];
				for (const obj in this.dataProvider.managerType) {
					if (obj) {
						managerType.push({
							id: this.dataProvider.managerType[obj].id,
							desc: this.dataProvider.managerType[obj].description
						});
					}
				}
				delete this.dataProvider.managerType;
				this.dataProvider.managerType = managerType;
			}
			if (this.dataProvider.strategies) {
				const strategies = [];
				for (const obj in this.dataProvider.strategies) {
					if (obj) {
						strategies.push({
							id: this.dataProvider.strategies[obj].id,
							desc: this.dataProvider.strategies[obj].description
						});
					}
				}
				delete this.dataProvider.strategies;
				this.dataProvider.strategies = strategies;
			}
			if (this.dataProvider.researchStyles) {
				const researchStyles = [];
				for (const obj in this.dataProvider.researchStyles) {
					if (obj) {
						researchStyles.push({
							id: this.dataProvider.researchStyles[obj].id,
							desc: this.dataProvider.researchStyles[obj].description
						});
					}
				}
				delete this.dataProvider.researchStyles;
				this.dataProvider.researchStyles = researchStyles;
			}
			if (this.dataProvider.identifiersAvailable) {
				const identifiersAvailable = [];
				for (const obj in this.dataProvider.identifiersAvailable) {
					if (obj) {
						identifiersAvailable.push({
							id: this.dataProvider.identifiersAvailable[obj].id,
							desc: this.dataProvider.identifiersAvailable[obj].description
						});
					}
				}
				delete this.dataProvider.identifiersAvailable;
				this.dataProvider.identifiersAvailable = identifiersAvailable;
			}
			if (this.dataProvider.investingTimeFrame) {
				const investingTimeFrame = [];
				for (const obj in this.dataProvider.investingTimeFrame) {
					if (obj) {
						investingTimeFrame.push({
							id: this.dataProvider.investingTimeFrame[obj].id,
							desc: this.dataProvider.investingTimeFrame[obj].description
						});
					}
				}
				delete this.dataProvider.investingTimeFrame;
				this.dataProvider.investingTimeFrame = investingTimeFrame;
			}
			if (this.dataProvider.equitiesMarketCap) {
				const equitiesMarketCap = [];
				for (const obj in this.dataProvider.equitiesMarketCap) {
					if (obj) {
						equitiesMarketCap.push({
							id: this.dataProvider.equitiesMarketCap[obj].id,
							desc: this.dataProvider.equitiesMarketCap[obj].description
						});
					}
				}
				delete this.dataProvider.equitiesMarketCap;
				this.dataProvider.equitiesMarketCap = equitiesMarketCap;
			}
			if (this.dataProvider.equitiesStyle) {
				const equitiesStyle = [];
				for (const obj in this.dataProvider.equitiesStyle) {
					if (obj) {
						equitiesStyle.push({
							id: this.dataProvider.equitiesStyle[obj].id,
							desc: this.dataProvider.equitiesStyle[obj].description
						});
					}
				}
				delete this.dataProvider.equitiesStyle;
				this.dataProvider.equitiesStyle = equitiesStyle;
			}
			if (this.dataProvider.dataGapsReasons) {
				const dataGapsReasons = [];
				for (const obj in this.dataProvider.dataGapsReasons) {
					if (obj) {
						dataGapsReasons.push({
							id: this.dataProvider.dataGapsReasons[obj].id,
							desc: this.dataProvider.dataGapsReasons[obj].lookupcode
						});
					}
				}
				delete this.dataProvider.dataGapsReasons;
				this.dataProvider.dataGapsReasons = dataGapsReasons;
			}
			if (this.dataProvider.outlierReasons) {
				const outlierReasons = [];
				for (const obj in this.dataProvider.outlierReasons) {
					if (obj) {
						outlierReasons.push({
							id: this.dataProvider.outlierReasons[obj].id,
							desc: this.dataProvider.outlierReasons[obj].lookupcode
						});
					}
				}
				delete this.dataProvider.outlierReasons;
				this.dataProvider.outlierReasons = outlierReasons;
			}
			if (this.dataProvider.biases) {
				const biases = [];
				for (const obj in this.dataProvider.biases) {
					if (obj) {
						biases.push({
							id: this.dataProvider.biases[obj].id,
							desc: this.dataProvider.biases[obj].lookupcode
						});
					}
				}
				delete this.dataProvider.biases;
				this.dataProvider.biases = biases;
			}
			if (this.dataProvider.paymentMethodsOffered) {
				const paymentMethodsOffered = [];
				for (const obj in this.dataProvider.paymentMethodsOffered) {
					if (obj) {
						paymentMethodsOffered.push({
							id: this.dataProvider.paymentMethodsOffered[obj].id,
							desc: this.dataProvider.paymentMethodsOffered[obj].description
						});
					}
				}
				delete this.dataProvider.paymentMethodsOffered;
				this.dataProvider.paymentMethodsOffered = paymentMethodsOffered;
			}
			if (this.dataProvider.deliveryFrequencies) {
				const deliveryFrequencies = [];
				for (const obj in this.dataProvider.deliveryFrequencies) {
					if (obj) {
						deliveryFrequencies.push({
							id: this.dataProvider.deliveryFrequencies[obj].id,
							desc: this.dataProvider.deliveryFrequencies[obj].lookupcode
						});
					}
				}
				delete this.dataProvider.deliveryFrequencies;
				this.dataProvider.deliveryFrequencies = deliveryFrequencies;
			}
			/* if (this.dataProvider.dataProvidersPricingModels) {
				const dataProvidersPricingModels = [];
				for (const obj in this.dataProvider.dataProvidersPricingModels) {
					if (obj) {
						dataProvidersPricingModels.push({
							id: this.dataProvider.dataProvidersPricingModels[obj].id,
							desc: this.dataProvider.dataProvidersPricingModels[obj].lookupcode
						});
					}
				}
				delete this.dataProvider.dataProvidersPricingModels;
				this.dataProvider.dataProvidersPricingModels = dataProvidersPricingModels;
			} */
			if (this.dataProvider.pricingModels) {
				const pricingModels = [];
				for (const obj in this.dataProvider.pricingModels) {
					if (obj) {
						console.log('pricingModels', this.dataProvider.pricingModels[obj]);
						console.log('pricingModelsdesc', this.dataProvider.pricingModels[obj].desc);
						const pricingModelsId = parseInt(this.dataProvider.pricingModels[obj]['id'],10);
                    //    console.log('securityTypesId 1', securityTypesId);
					pricingModels.push({
							id: pricingModelsId,
							desc: this.dataProvider.pricingModels[obj].desc
						});
					}
				}
				delete this.dataProvider.pricingModels;
				this.dataProvider.pricingModels = pricingModels;
			}

			if (this.dataProvider.investorTypes) {
				const investorTypes = [];
				for (const obj in this.dataProvider.investorTypes) {
					if (obj) {
						investorTypes.push({
							id: this.dataProvider.investorTypes[obj].id,
							description: this.dataProvider.investorTypes[obj].description
						});
					}
				}
				delete this.dataProvider.investorTypes;
				this.dataProvider.investorTypes = investorTypes;
			}
			if (this.dataProvider.providerTags) {
				const providerTags = [];
				for (const obj in this.dataProvider.providerTags) {
					if (obj) {
						const providerTagsId = parseInt(this.dataProvider.providerTags[obj]['id'],10);
						const label = this.dataProvider.providerTags[obj]['desc'];
						/* console.log('label',label);
                    console.log('Tags', this.providerTags); */
					const arrayWithFilterObjects= this.providerTags.filter((o) => o.label === label);
					//console.log('arrayWithFilterObjects', arrayWithFilterObjects);
						if (arrayWithFilterObjects.length) {
						providerTags.push({
							id: providerTagsId,
							desc: this.dataProvider.providerTags[obj]['desc']
						});
					}
					}
				}
				delete this.dataProvider.providerTags;
				this.dataProvider.providerTags = providerTags;
				//console.log('providerTagsCalled',this.dataProvider.providerTags);
			}
            // if (this.dataProvider.link) {
            //     console.log(this.dataProvider);
            //     console.log(this.dataProvider.link);
            //     this.editLinkBtn = true;
			// 	const link = [];
			// 	for (const obj in this.dataProvider.link) {
			// 		if (obj) {
			// 			link.push({
			// 				id: this.dataProvider.link[obj].id,
			// 				desc: this.dataProvider.link[obj].link
			// 			});
			// 		}
			// 	}
			// 	delete this.dataProvider.link;
			// 	this.dataProvider.link = link;
			// }
			// if (this.dataProvider.providerType) {
			// 	const providerType = {};
			// 	providerType['id'] = this.dataProvider.providerType.id;
			// 	providerType['providerType'] = this.dataProvider.providerType.providerType,
			// 	providerType['explanation'] = this.dataProvider.providerType.explanation;
			// 	delete this.dataProvider.providerType;
			// 	this.dataProvider.providerType = providerType;
			// }
			if (this.dataProvider.pricingModel) {
				/* const pricingModels = this.dataProvider.pricingModels;
				delete this.dataProvider.pricingModels;
				this.dataProvider.pricingModels = pricingModels[0]; */
			}
            this.basicModel = this.dataProvider;
            this.basicModel['editedData'] = this.editedDataByUser;
            console.log('Provider Edited Data', this.basicModel);
			this.providerID = this.dataProvider ? this.dataProvider.id : null;
			//console.log('ORGID',this.dataProvider.ownerOrganization.id);
		}, error => {
			if (error.status === 401) {
                this.stateService.storeUrl(null);
                this.router.navigate(['']);
				this.signinModalService.openDialog('Session expired! Please re-login.');
			}
			this.dataProvider = null;
			this.isLoading = false;
		});

	   // this.loaderService.display(false);
	}

	deleteResourceLink(obj, index) {

		this.deleteLinkObj = obj.resourceID;
		this.resources.map(item => {
		   if(item.id === obj.resourceID) {
			   this.itemObject = item;
			   this.resourceAdminService.deleteResourceLink(this.itemObject, this.deleteLinkObj, this.dataProviderId, this.resources).subscribe(data => {
			   });
		    }
		});
		this.dataRelevantLinks.splice(index, 1);
	}

	deleteResourceFile(obj, index) {

		this.deleteLinkObj = obj.resourceID;
		this.resources.map(item => {
		   if(item.id === obj.resourceID) {
			   this.itemObject = item;
			   this.resourceAdminService.deleteResourceLink(this.itemObject, this.deleteLinkObj, this.dataProviderId, this.resources).subscribe(data => {
			   });
		    }
		});
		this.dataRelevantFiles.splice(index, 1);
	}

    constructFormModel(provider: DataProvider) {
        this.trackname = provider.providerName ? provider.providerName.length : 0;
      //  console.log(this.trackname);
        this.trackwebsite = provider.website ? provider.website.length : 0;
        if (provider.ownerOrganization) {
            this.trackownerOrganizationCity = provider.ownerOrganization.city ? provider.ownerOrganization.city.length : 0;
            this.trackownerOrganizationState = provider.ownerOrganization.state ? provider.ownerOrganization.state.length : 0;
            this.trackownerOrganizationCountry = provider.ownerOrganization.country ? provider.ownerOrganization.country.length : 0;
            this.trackownerOrganizationYearfound = provider.ownerOrganization.yearFounded;
            this.trackownerOrganizationHeadcount = provider.ownerOrganization.headCount ?provider.ownerOrganization.headCount.length : 0;
        }
        this.tracknumberOfAnalystEmployees = provider.numberOfAnalystEmployees? provider.numberOfAnalystEmployees.length : 0;
        this.trackInvestor = provider.investor_clients ? provider.investor_clients.length : 0;
        this.trackNumberOfInvestor = provider.numberOfInvestorClients ? provider.numberOfInvestorClients.length : 0;
        this.trackShortDescription = provider.shortDescription ? provider.shortDescription.length: 0;
        this.trackLongDescription = provider.longDescription ? provider.longDescription.length: 0;
        this.trackSummary = provider.summary ? provider.summary.length: 0;
        this.trackDataCollection = provider.dateCollectionBegan ? provider.dateCollectionBegan.length : 0;
        this.trackcollectionMethodsExplanation = provider.collectionMethodsExplanation ? provider.collectionMethodsExplanation.length : 0;
        this.trackpublicCompaniesCovered = provider.publicCompaniesCovered ? provider.publicCompaniesCovered.length : 0;
        this.trackpublicEquitesCovered = provider.publicEquitiesCoveredCount ? provider.publicEquitiesCoveredCount.length : 0;
        this.tracksampleOrPanelSize = provider.sampleOrPanelSize ? provider.sampleOrPanelSize.length : 0;
        this.trackDateCollectionRangeExplanation = provider.dateCollectionRangeExplanation ? provider.dateCollectionRangeExplanation.length : 0;
        this.trackkeyData = provider.key_data ? provider.key_data.length : 0;
        this.trackOutOfSampleData = provider.outOfSampleData ? provider.outOfSampleData.length: 0;
        this.trackProductDetails = provider.productDetails ? provider.productDetails.length : 0;
        this.trackSampleOrPanelBiasis = provider.sampleOrPanelBiases ? provider.sampleOrPanelBiases.length : 0;
        /* this.trackDiscussionCleanlines = provider.discussionCleanlines? provider.discussionCleanlines.length : 0; */
        this.trackDeliveryFrequencyNotes = provider.deliveryFrequencyNotes ? provider.deliveryFrequencyNotes.length : 0;
        this.trackDataUpdateFrequencyNotes = provider.dataUpdateFrequencyNotes ? provider.dataUpdateFrequencyNotes.length : 0;
        this.trackTrialDuration = provider.trialDuration ? provider.trialDuration.length : 0;
        this.trackTrialPricingDetails = provider.trialPricingDetails ? provider.trialPricingDetails.length : 0;
        this.trackDataLicenseTypeDetails = provider.dataLicenseTypeDetails ? provider.dataLicenseTypeDetails.length : 0;
        this.trackMaximumYearlyPrice = provider.maximumYearlyPrice ? provider.maximumYearlyPrice.length : 0;
        this.trackMinimumYearlyPrice = provider.minimumYearlyPrice ? provider.minimumYearlyPrice.length : 0;
        this.trackPricingDetails = provider.pricingDetails ? provider.pricingDetails.length : 0;
        this.trackUseCasesOrQuestionsAddressed = provider.useCasesOrQuestionsAddressed ? provider.useCasesOrQuestionsAddressed.length : 0;
        this.trackDiscussionsAnalytics = provider.discussionsAnalytics ? provider.discussionsAnalytics.length : 0;
        this.trackPotentialDrawbacks = provider.potentialDrawbacks ? provider.potentialDrawbacks.length : 0;
        this.trackUniqueValueProps = provider.uniqueValueProps ? provider.uniqueValueProps.length : 0;
        this.trackLegalComplianceIssues = provider.legalAndComplianceIssues ? provider.legalAndComplianceIssues.length : 0;
        this.trackDataRoadmap = provider.dataRoadmap ? provider.dataRoadmap.length : 0;
        this.trackNotes = provider.notes ? provider.notes.length : 0;
        this.trackDataSourcesDetails = provider.dataSourcesDetails ? provider.dataSourcesDetails.length : 0;
        this.trackCompetitiveDifferentiators = provider.competitiveDifferentiators ? provider.competitiveDifferentiators.length : 0;
        this.trackYearFirstDataProductLaunched =provider.yearFirstDataProductLaunched ? provider.yearFirstDataProductLaunched.length : 0;
        this.trackdiscussionCleanliness = provider.discussionCleanliness ? provider.discussionCleanliness.length : 0;
        this.trackLagTime = provider.lagTime ? provider.lagTime.length : 0;
        this.providerFormModel = {
            'recordID': provider.recordID,
            'providerName': provider.providerName,
            'website': provider.website
        };
        this.descriptionsModel = {
            'recordID': provider.recordID,
            'shortDescription': provider.shortDescription,
            'longDescription': provider.longDescription
        };
        this.companyDetailsModel = {
            'recordID': provider.recordID,
            'ownerOrganization': provider.ownerOrganization
        };
        this.categorizationsModel = {
            'recordID': provider.recordID,
            'providerType': provider.providerType,
            'mainDataCategory': provider.mainDataCategory,
            'mainDataIndustry': provider.mainDataIndustry
		};
    }

	previousState() {
		this.window.history.back();
	}

	// ngOnDestroy() {
	// 	this.onDestroy$.next();
    //     this.onDestroy$.complete();
    //     this.cookieService.put('logoutClicked', '0');
	// }

	openConfirmationDialog(nextState?: RouterStateSnapshot) {
        const redirect = nextState.url;
        this.dialog.closeAll();
        this.dialogRef = this.dialog.open(ConfirmDialogComponent, {
            disableClose: false
        });
        this.dialogRef.componentInstance.confirmMessage = 'You have some unsaved edits in this form.';
        this.dialogRef.componentInstance.button = {
            leave: true,
            confirm: true,
            leaveText: 'Leave and Don\'t Save',
            confirmText: 'Save and Submit'
        };

        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // do confirmation actions
                if (result === 'leave') {
                    if (redirect === '/') {
                        this.authService.logout().subscribe();
                        this.formSubmitted = true;
                        this.principal.authenticate(null);
                        this.stateService.storeUrl(null);
                        this.router.navigate(['../../']);
                    } else {
                        this.formSubmitted = true;
                        if (this.principal.isAuthenticated()) {
                            this.stateService.storeUrl(null);
                            this.router.navigate([redirect]);
                            this.dialog.closeAll();
                        } else {
                            this.stateService.storeUrl(null);
                            this.router.navigate(['']);
                            this.dialog.closeAll();
                        }
                    }

                }

                if (result === 'save') {
                    if (redirect === '/') {
                        this.submitProviderEdit(this.basicModel);
                        this.authService.logout().subscribe();
                        this.formSubmitted = true;
                        this.principal.authenticate(null);
                        this.stateService.storeUrl(null);
                        this.router.navigate(['../../']);
                    } else {
                        if (this.principal.isAuthenticated()) {
                            this.submitProviderEdit(this.basicModel);
                        } else {
                            this.formSubmitted = true;
                            this.stateService.storeUrl(null);
                            this.router.navigate([redirect]);
                        }
                    }
                }
            }
            this.dialogRef = null;
        });

		return false;
	}

	trackByIndex(index) {
		return index;
	}

	submitProviderEdit(model, panelName?: string, currentPanel?: string, nextPanel?: string, tabIndex?: number, form?: string, skipRedirect?: boolean) {
		this.overAll=panelName;
		if(panelName !=='Overall Details')
		{
			this.panelName = panelName;
		}
		else
		{
			panelName = '';
		}
		const formName = panelName ? panelName + '' : 'Form';
        const submitMessage = 'Your edits have been successfully submitted and have been applied to the profile immediately.';
        const filterArr = Array.prototype.filter;
        if (form && this[form].valid) {
			const autoCompleteFields = this.el.nativeElement.querySelectorAll('#' + form + ' .ui-autocomplete-input');
			let invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
            const filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                return node.value === '';
            });
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length && !this.forceFormSubmit) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
			} else {
                delete model['id'];
				model['id'] = this.providerID;
				this.progressSpinner=true;
                this.dataProviderService.providerAdmin(model).subscribe(data => {
                    if (data) {
						this.progressSpinner=false;
                        this.formSubmitted = true;
                        this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                        if (this.forceFormSubmit === true) {
                            this.onExpireSession(this.forceFormSubmit);
                        }
                        if (currentPanel && currentPanel !== '' && !this.forceFormSubmit) {
                            this[currentPanel] = false;
                            if (nextPanel && nextPanel !== '') {
                                this[nextPanel] = true;
                            }
                            if (tabIndex) {
                                this.tabIndex = tabIndex;
                            }
                        } else {
                            if (skipRedirect) {
                                return true;
                            }
                            this.stateService.storeUrl(null);
                            this.router.navigate(['../../', model.recordID], { relativeTo: this.route });
                        }
                    } else {
                        // console.log(data);
						this.formSubmitted = false;
						this.progressSpinner=false;
                        this.openSnackBar('' + formName + ' failed to save!', 'Close');
                    }
                }, error => {
					if (error.status === 401) {
						this.formSubmitted = false;
						this.progressSpinner=false;
						this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
					} else {
						this.formSubmitted = false;
						this.progressSpinner=false;
						this.openSnackBar('' + formName + ' failed to save!', 'Close');
					}
                });
            }
        } else {
            console.log(this.forceFormSubmit);
            let invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
            let autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
            let filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                return node.required && node.value === '';
            });
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length && !this.forceFormSubmit) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else if (invalidElements.length > 0 && !this.forceFormSubmit) {
                if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                    invalidElements[0].querySelectorAll('input')[0].focus();
                } else {
                    setTimeout(() => {
                        invalidElements[0].focus();
                    }, 1000);
                }
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else {
                this.tabIndex = this.tabIndex === 0 && !this.forceFormSubmit ? 1 : 0;
                setTimeout(() => {
                    invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
                    autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
                    filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                        return node.required && node.value === '';
                    });
                    if (filteredAutoCompleteFields && filteredAutoCompleteFields.length && !this.forceFormSubmit) {
                        setTimeout(() => {
                            filteredAutoCompleteFields[0].focus();
                        }, 1000);
                        this.openSnackBar('Please fill all the required fields!', 'Close');
                        this.expandFormPanels();
                    } else if (invalidElements.length > 0 && !this.forceFormSubmit) {
                        if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                            invalidElements[0].querySelectorAll('input')[0].focus();
                        } else {
                            setTimeout(() => {
                                invalidElements[0].focus();
                            }, 1000);
                        }
                        this.openSnackBar('Please fill all the required fields!', 'Close');
                        this.expandFormPanels();
                    } else {
						delete model['id'];
						model['id'] = this.providerID;
						this.progressSpinner=true;
                        this.dataProviderService.providerAdmin(model).subscribe(data => {
                            if (data) {
								this.progressSpinner=false;
                                this.formSubmitted = true;
                                this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                                if (this.forceFormSubmit === true) {
                                    this.onExpireSession(this.forceFormSubmit);
                                }
                                if (currentPanel && currentPanel !== '' && !this.forceFormSubmit) {
                                    this[currentPanel] = false;
                                    if (nextPanel && nextPanel !== '') {
                                        this[nextPanel] = true;
                                    }
                                    if (tabIndex) {
                                        this.tabIndex = tabIndex;
                                    }
                                } else {
                                    if (skipRedirect) {
                                        return true;
                                    }
                                    this.stateService.storeUrl(null);
                                    this.router.navigate(['../../', model.recordID], { relativeTo: this.route });
                                }
                            } else {
								this.formSubmitted = false;
								this.progressSpinner=false;
                                this.openSnackBar('' + formName + ' failed to save!', 'Close');
                            }
                        }, error => {
							if (error.status === 401) {
								this.formSubmitted = false;
								this.progressSpinner=false;
								this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
							} else {
								this.formSubmitted = false;
								this.progressSpinner=false;
								this.openSnackBar('' + formName + ' failed to save!', 'Close');
							}
                        });
                    }
                }, 2000);
            }
        }
	}

	submitResourceEdit(panelName?: string, form?: string) {
        const formName = panelName ? panelName + '' : 'Form';
        const submitMessage = 'Your edits have been successfully submitted and have been applied to the profile immediately.';
        if (form && this[form].valid) {
			this.resourceAdminService.updateAllResources(this.resources).subscribe(data => {
				if (data) {
					this.formSubmitted = true;
					this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
				} else {
					// console.log(data);
					this.formSubmitted = false;
					this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
				}
			}, error => {
				if (error.status === 401) {
					this.formSubmitted = false;
					this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
				} else {
					this.openSnackBar('' + formName + ' failed to save!', 'Close');
				}
			});
		}
	}

	addAllRelevantLinks() {
        for (let i = 0; i < this.addRelevantLinks.length; i++) {
			this.filterService.findByRelevantLinkId(this.addRelevantLinks[i].id).subscribe(data => {
				if(data['hits'] && data['hits'].hits.length === 0) {
				   const relevantLinkObj = {};
				   relevantLinkObj['link'] = { id: this.addRelevantLinks[i].id };
				   relevantLinkObj['name'] = this.addRelevantLinks[i].link;
				   relevantLinkObj['relavantDataProviders'] = [{id: this.dataProviderId,providerName: this.dataProviderName}];
                   this.resourceAdminService.createResourceLink(relevantLinkObj).subscribe(data=> {
						relevantLinkObj['resourceID'] = data.id;
						relevantLinkObj['createResourceName'] = data.name;
						this.disableTable = false;
						this.dataRelevantLinks.push(relevantLinkObj);
				   });
				} else if(data['hits'] && data['hits'].hits.length > 0){
					const obj = data['hits'] && data['hits'].hits[i] && data['hits'].hits[i]._source;
					if (obj.relavantDataProviders) {
						obj.relavantDataProviders = [{id: this.dataProviderId,providerName: this.dataProviderName}];
					}
					this.resourceAdminService.updateResourceLink(obj).subscribe(data=> {
						obj['resourceID'] = obj.id;
						obj['updateResourceName'] = obj.link && obj.link.link ? obj.link.link : obj.name;
						this.disableTable = false;
						this.dataRelevantLinks.push(obj);
				   });
				}
			}, error => {
			});
		}
	}

    expandFormPanels() {
        this.basicExpansionPanel = true;
        this.companyExpansionPanel = true;
		this.adminExpansionPanel= true;
		this.descriptionsExpansionPanel = true;
		this.categorizationsExpansionPanel = true;
		this.investigationExpansionPanel = true;
		this.relatedProvidersExpansionPanel = true;
		this.dataDetailsExpansionPanel = true;
		this.dataQualityExpansionPanel = true;
		this.dataDeliveryExpansionPanel = true;
		this.triallingExpansionPanel = true;
		this.licensingExpansionPanel = true;
        this.pricingExpansionPanel = true;
        this.resourceProvidersExpansionPanel = true;
    }

    formSubmit(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    formSave(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    basicInput($event) {
        const inputKey = $event;
        /* delete entry */
        if (inputKey.key === 'providerName') {
            if (inputKey.model.providerName.length < _that.trackname) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.providerName.length > _that.trackname) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'website') {
            if ( inputKey.model.website.length < _that.trackwebsite) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.website.length > _that.trackwebsite) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'ownerOrganization.city') {
            if (inputKey.model.ownerOrganization.city.length < _that.trackownerOrganizationCity) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.ownerOrganization.city.length > _that.trackownerOrganizationCity) {
                 _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'ownerOrganization.state') {
            if (inputKey.model.ownerOrganization.state.length < _that.trackownerOrganizationState) {
                 _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.ownerOrganization.state.length > _that.trackownerOrganizationState) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
             }
        } else if (inputKey.key === 'ownerOrganization.country') {
            if (inputKey.model.ownerOrganization.country.length < _that.trackownerOrganizationCountry) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.ownerOrganization.country.length > _that.trackownerOrganizationCountry) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'ownerOrganization.yearFounded') {
            if (inputKey.model.ownerOrganization.yearFounded < _that.trackownerOrganizationYearfound) {
               _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.ownerOrganization.yearFounded > _that.trackownerOrganizationYearfound) {
              _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        }/*  else if (inputKey.key === 'ownerOrganization.headCount') {
            if (inputKey.model.ownerOrganization.headCount.length < _that.trackownerOrganizationHeadcount) {
                 _that.googleAnalyticsService.emitEvent(inputKey.defaultValue, 'Deleted', inputKey.fieldGroupClassName + ' - ' + inputKey.name);
            } else if (inputKey.model.ownerOrganization.headCount.length > _that.trackownerOrganizationHeadcount) {
                _that.googleAnalyticsService.emitEventinputKey.defaultValue, 'Added', inputKey.fieldGroupClassName + ' - ' + inputKey.name);
            }
        } */ else if (inputKey.key === 'investor_clients') {
            if (inputKey.model.investor_clients.length < _that.trackInvestor) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.investor_clients.length > _that.trackInvestor) {
                 _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'investorClientsCount') {
            if (inputKey.model.investorClientsCount.length < _that.trackNumberOfInvestor) {
               _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.investorClientsCount.length > _that.trackNumberOfInvestor) {
               _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'summary') {
            if (inputKey.model.summary.length < _that.trackSummary) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.summary.length > _that.trackSummary) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'shortDescription') {
            if (inputKey.model.shortDescription.length < _that.trackShortDescription) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.shortDescription.length > _that.trackShortDescription) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'longDescription') {
            if (inputKey?.model?.longDescription?.length < _that.trackLongDescription) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey?.model?.longDescription?.length > _that.trackLongDescription) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'dateCollectionBegan') {
            if (inputKey.model.dateCollectionBegan.length < _that.trackDataCollection) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.dateCollectionBegan.length > _that.trackDataCollection) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'publicCompaniesCovered') {
            if (inputKey.model.publicCompaniesCovered.length < _that.trackpublicCompaniesCovered) {
                 _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.publicCompaniesCovered.length > _that.trackpublicCompaniesCovered) {
                 _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }else if (inputKey.key === 'publicEquitiesCoveredCount') {
            if (inputKey.model.publicEquitiesCoveredCount.length < _that.trackpublicEquitesCovered) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.publicEquitiesCoveredCount.length > _that.trackpublicEquitesCovered) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'collectionMethodsExplanation') {
            if (inputKey.model.collectionMethodsExplanation.length < _that.trackcollectionMethodsExplanation) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.collectionMethodsExplanation.length > _that.trackcollectionMethodsExplanation) {
                  _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'sampleOrPanelSize') {
            if (inputKey.model.sampleOrPanelSize.length < _that.tracksampleOrPanelSize) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.sampleOrPanelSize.length > _that.tracksampleOrPanelSize) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
           }
        } else if (inputKey.field && inputKey.field.key === 'dateCollectionRangeExplanation') {
            if (inputKey.model.dateCollectionRangeExplanation.length < _that.trackDateCollectionRangeExplanation) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.dateCollectionRangeExplanation.length > _that.trackDateCollectionRangeExplanation) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'key_data') {
            if (inputKey.model.key_data && inputKey.model.key_data.length < _that.trackkeyData) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
           } else if (inputKey.model.key_data && inputKey.model.key_data.length > _that.trackkeyData) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'outOfSampleData') {
            if (inputKey.model.outOfSampleData.length > _that.trackOutOfSampleData) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.outOfSampleData.length < _that.trackOutOfSampleData) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'productDetails') {
            if (inputKey?.model?.productDetails?.length < _that.trackProductDetails) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey?.model?.productDetails?.length > _that.trackProductDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'sampleOrPanelBiases') {
            if (inputKey.model.sampleOrPanelBiases && inputKey.model.sampleOrPanelBiases.length < _that.trackSampleOrPanelBiasis) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.sampleOrPanelBiases && inputKey.model.sampleOrPanelBiases.length > _that.trackSampleOrPanelBiasis) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'deliveryFrequencyNotes') {
            if (inputKey.model.deliveryFrequencyNotes.length < _that.trackDeliveryFrequencyNotes) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.deliveryFrequencyNotes.length > _that.trackDeliveryFrequencyNotes) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'dataUpdateFrequencyNotes') {
            if (inputKey.model.dataUpdateFrequencyNotes.length < _that.trackDataUpdateFrequencyNotes) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.dataUpdateFrequencyNotes.length > _that.trackDataUpdateFrequencyNotes) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'trialDuration') {
            if (inputKey.model.trialDuration.length < _that.trackTrialDuration) {
              _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.trialDuration.length > _that.trackTrialDuration) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'trialPricingDetails') {
            if (inputKey.model.trialPricingDetails.length < _that.trackTrialPricingDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.trialPricingDetails.length > _that.trackTrialPricingDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'dataLicenseTypeDetails') {
            if (inputKey.model.dataLicenseTypeDetails.length < _that.trackDataLicenseTypeDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.dataLicenseTypeDetails.length > _that.trackDataLicenseTypeDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'maximumYearlyPrice') {
            if (inputKey.model.maximumYearlyPrice.length < _that.trackMaximumYearlyPrice) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.maximumYearlyPrice.length > _that.trackMaximumYearlyPrice) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'minimumYearlyPrice') {
            if (inputKey.model.minimumYearlyPrice.length < _that.trackMinimumYearlyPrice) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.minimumYearlyPrice.length > _that.trackMinimumYearlyPrice) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'pricingDetails') {
            if (inputKey.model.pricingDetails.length < _that.trackPricingDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.pricingDetails.length > _that.trackPricingDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'useCasesOrQuestionsAddressed') {
            if (inputKey.model.useCasesOrQuestionsAddressed.length < _that.trackUseCasesOrQuestionsAddressed) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.useCasesOrQuestionsAddressed.length > _that.trackUseCasesOrQuestionsAddressed) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
       } else if (inputKey.field && inputKey.field.key === 'discussionsAnalytics') {
            if (inputKey.model.discussionsAnalytics.length < _that.trackDiscussionsAnalytics) {
                    _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.discussionsAnalytics.length > _that.trackDiscussionsAnalytics) {
                    _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'potentialDrawbacks') {
            if (inputKey.model.potentialDrawbacks.length < _that.trackPotentialDrawbacks) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.potentialDrawbacks.length > _that.trackPotentialDrawbacks) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'uniqueValueProps') {
            if (inputKey.model.uniqueValueProps.length < _that.trackUniqueValueProps) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.uniqueValueProps.length > _that.trackUniqueValueProps) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'legalComplianceIssues') {
            if (inputKey.model.legalComplianceIssues.length < _that.trackLegalComplianceIssues) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.legalComplianceIssues.length > _that.trackLegalComplianceIssues) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'dataRoadmap') {
            if (inputKey.model.dataRoadmap.length < _that.trackDataRoadmap) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.dataRoadmap.length > _that.trackDataRoadmap) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'notes') {
            if (inputKey.model.notes.length < _that.trackNotes) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.notes.length > _that.trackNotes) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'dataSourcesDetails') {
            if (inputKey.model.dataSourcesDetails && inputKey.model.dataSourcesDetails.length < _that.trackDataSourcesDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.dataSourcesDetails && inputKey.model.dataSourcesDetails.length > _that.trackDataSourcesDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'competitiveDifferentiators') {
            if (inputKey.model.competitiveDifferentiators.length < _that.trackCompetitiveDifferentiators) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.competitiveDifferentiators.length > _that.trackCompetitiveDifferentiators) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'yearFirstDataProductLaunched') {
            if (inputKey.model.yearFirstDataProductLaunched && inputKey.model.yearFirstDataProductLaunched.length < _that.trackYearFirstDataProductLaunched) {
                 _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.yearFirstDataProductLaunched && inputKey.model.yearFirstDataProductLaunched.length > _that.trackYearFirstDataProductLaunched) {
                 _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'discussionCleanliness') {
            if (inputKey.model.discussionCleanliness && inputKey.model.discussionCleanliness.length < _that.trackdiscussionCleanliness) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.discussionCleanliness && inputKey.model.discussionCleanliness.length > _that.trackdiscussionCleanliness) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'lagTime') {
            if (inputKey.model.lagTime.length > _that.trackLagTime) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.lagTime.length < _that.trackLagTime) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey[0]) {
            if (inputKey[0].name) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - File', 'Added', 'Provider Profile Form - File - Provider Logo');
            }
        }
    }

    openSnackBar(message: string, action: string, delay?: number) {
        this.snackBar.open(message, action, {
            duration: delay ? delay : 10000,
			panelClass: ['blue-snackbar']
        });
    }

	submitBasic(model) {
		// console.log(model);
		this.basicExpansionPanel = false;
		this.companyExpansionPanel = true;
	}

	submitCompanyDetails(model) {
		// console.log(model);
		this.companyExpansionPanel = false;
		this.adminExpansionPanel = true;
	}

	submitAdmin(model) {
		// console.log(model);
		this.adminExpansionPanel = false;
		this.descriptionsExpansionPanel = true;
	}

	submitDescriptions(model) {
		// console.log(model);
		this.descriptionsExpansionPanel = false;
		this.categorizationsExpansionPanel = true;
	}

	submitCategorizations(model) {
		// console.log(model);
		this.categorizationsExpansionPanel = false;
		this.investigationExpansionPanel = true;
	}

	submitInvestigationDetails(model) {
		this.investigationExpansionPanel = false;
		this.tabIndex = 1;
	}

	submitRelatedProviders(model) {
		// console.log(model);
		this.relatedProvidersExpansionPanel = false;
		this.dataDetailsExpansionPanel = true;
	}

	submitDataDetails(model) {
		// console.log(model);
		this.dataDetailsExpansionPanel = false;
		this.dataQualityExpansionPanel = true;
	}

	submitDataQuality(model) {
		// console.log(model);
		this.dataQualityExpansionPanel = false;
		this.dataDeliveryExpansionPanel = true;
	}

	submitDataDelivery(model) {
		// console.log(model);
		this.dataDeliveryExpansionPanel = false;
		this.triallingExpansionPanel = true;
	}

	submitTrialling(model) {
		// console.log(model);
		this.triallingExpansionPanel = false;
		this.licensingExpansionPanel = true;
	}

	submitLicensing(model) {
		// console.log(model);
		this.licensingExpansionPanel = false;
		this.pricingExpansionPanel = true;
	}

	submitPricing(model) {
		// console.log(model);
		this.pricingExpansionPanel = false;
		this.discussionExpansionPanel = true;
	}

	submitDiscussion(model) {
		// console.log(model);
		this.discussionExpansionPanel = false;
    }

    beforeSessionExpires(event) {
        // console.log('Before session', event);
        // if (event) {
        //     this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        // }
    }

    onExpireSession(event) {
        console.log('After session',event);
        // if (event) {
		// 	// console.log('After session',event);
        //     this.formSubmitted = true;
        //     this.authService.logout().subscribe(d => {
        //         this.stateService.storeUrl(null);
        //         this.principal.authenticate(null);
        //         this.router.navigate(['../../']);
		// 		setTimeout(() => {
        //             this.signinModalService.openDialog('Your session has expired due to inactivity. Your edits in the form have been automatically saved and submitted and Amass will review your edits over the coming days before they appear within your profile. Please login again.');
        //         }, 2000);
		// 	});
        // }
		// console.log('After session', event);
        // this.formSubmitted = true;
        // this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        // setTimeout(() => {
        //     this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        // }, 0);

        // setTimeout(() => {
        //     if (event && !this.forceFormSubmit) {
        //         // console.log('After session',event);
        //         this.authService.logout().subscribe(d => {
        //             this.stateService.storeUrl(null);
        //             this.principal.authenticate(null);
        //             this.router.navigate(['../../']);
        //             setTimeout(() => {
        //                 this.signinModalService.openDialog('Your session has expired due to inactivity. Your edits in the form have been automatically saved and submitted and Amass will review your edits over the coming days before they appear within your profile. Please login again.');
        //             }, 2000);
        //         });
        //     }
        // }, 10000);

        if (event && this.forceFormSubmit) {
            // console.log('After session',event);
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
                setTimeout(() => {
                    this.signinModalService.openDialog('Your session has expired due to inactivity. Your edits in the form have been automatically saved and submitted and Amass will review your edits over the coming days before they appear within your profile. Please login again.');
                }, 2000);
            });
        }
    }

    @HostListener('document:keyup', [])
    @HostListener('document:click', [])
    @HostListener('document:wheel', [])
    @HostListener('window:scroll', [])
    resetTimer() {
        this.authService.notifyUserAction();
    }

    @HostListener('window:popstate', [])
    onPopState() {
        this.cookieService.put('logoutClicked', '0');

    }

    @HostListener('window:beforeunload', ['$event'])
    preventUser($event) {
        $event.preventDefault();
        return $event.returnValue = "Are you sure you want to exit?";
    }
}
