import { Component, OnInit, OnDestroy, Inject, HostListener, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer, Meta } from '@angular/platform-browser';
import { Subscription, Observable } from 'rxjs';
import { MatAutocomplete } from '@angular/material/autocomplete';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import {
	HTTP_INTERCEPTORS,
	HttpClientModule,
	HttpClient,
	HttpEvent,
	HttpInterceptor,
	HttpHandler,
	HttpRequest
} from '@angular/common/http';
import { DataProvider } from './data-provider.model';
import { DataProviderService } from './data-provider.service';
import { MyHttpLogInterceptor } from '../../loader/interceptor';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, GenericFilterListModel, GenericFilterModel, GenericFilterComponent, StateStorageService, AuthServerProvider, SigninModalService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { ContactDialogComponent } from '../../shared/contactdialog/contact-dialog.component';
import { AmassFilterService } from '../../shared/genericfilter/filters.service';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { LoaderService } from '../../loader/loaders.service';
import { CookieService } from 'ngx-cookie';
import { FormControl } from '@angular/forms';
import { startWith, map, switchMap, tap } from 'rxjs/operators';
import { AmassSettingService } from '../../account/settings/settings.service';
import { SigninDialogComponent } from 'app/dialog/signindialog.component';
import { InvestorDialogComponent } from '../../shared/dialogs/investor-dialog.component';
import { ApplyAccountComponent } from 'app/account';
import { DataResourceTabService } from './data-resourcetab.service';
import { DataProviderSearchService } from './data-provider-search.service';
import { DataProviderFilterService } from './data-provider-filter.service';
import { DatePipe } from '@angular/common';
import * as _ from 'lodash';
const bob = require('elastic-builder');
const minDates = new Date(1970, 10, 1);
const currentDate = new Date();
const minUpdatedDate = new Date(1970, 10, 1);
const currentUpdatedDate = new Date();
import { forkJoin, zip } from 'rxjs';

@Component({
	selector: 'jhi-data-provider',
	templateUrl: './data-provider.component.html',
	styleUrls: [
		'./data-provider.component.scss',
		'../../shared/share-buttons/share-buttons.scss',
		'../../../content/scss/amass-form.scss'
	]
})
export class DataProviderComponent implements OnInit, OnDestroy {

	// public loader: any;
	dataProviders: DataProvider[];
	mainAssetClassFilterArray = [];
	// willingnessInvetorsFilterArray1 = [];
	priorityFilter = [];
	pricingModelFilter = [];
	currentAccount: any;
	eventSubscriber: Subscription;
	itemsPerPage: number;
	links: any;
	page: any;
	predicate: any;
	queryCount: any;
	reverse: any;
	totalItems: number;
	providerCount: any;
	itemsFrom: number;
	providerSearchControl: FormControl = new FormControl();
	providerSearchControlTag: FormControl = new FormControl();
	providerNameOptions: any;
	providerNameOptionsTag: any;
	currentSearch: any;
	currentSearchQuery: string;
	currentFilter: string;
	currentCoronaFilter: string;
	currentSorting: any;
	coronaSorting: any;
	sortingArray = [];
	sortedIndex: any;
	isList: boolean;
	paramFilter: string;
	paramID: string;
	paramType: string;
	paramCreatedDate: any;
	paramUpdatedDate: any;
	params: any;
	grid = {
		color: '#fff'
	};
	subscription: any;
	statusFilter: any;
	domSanitizer: any;
	isAdmin: boolean;
	loader: any;
	loading: any;
	loaderInfinite: any;
	limitedLogoutUsersEvent: boolean;
	HttpClient;
	reSubmitValues: any;
	completenessTooltip: any[];

	progressBarColor = 'primary';
	progressBarMode = 'determinate';
	pageName = 'list';
	mdTooltipDelay: number;

	filters: GenericFilterModel[];
	providerTypes: any;
	// dataProviderTag: any;
	mainDataCategories: any;
	followLabel: any;
	sortCategory: string;
	clearFilter: boolean;
	filterCheckBox: any;
	limitedProviders: any;
	currentUrl: any;
	cookiejson: any;
	timeout: boolean;
	searchData: any;
	totalItemsTooltip: string;
	sortByTooltip: string;
	baseUrl: string;
	customTags: boolean;
	newTagValue: any;
	userLevel: boolean;
	corona: boolean;
	coronaActive:boolean;
	tagDropDownValues: any;
	tagCheckItem: boolean;
	tagItemName: any;
	dropDownAutoClose: boolean;
	expandStatus: boolean;
	loaderActive: any;
	loaderTag: any;
	loaderProviderId: any;
	follow: boolean
	providerUserPermission: any;
	tagProviderMatch: any;
	tagNotInterstedMatch: any;
	tagInUseMatch: any;
	tagTestingMatch: any;
	tagInterstedArray: any[] = [];
	tagNotInterstedArray: any[] = [];
	tagInUseArray: any[] = [];
	tagTestingArray: any[] = [];
	tagValue: any;
	comments: any[] = [];
	commentsButtonType: string = '';
	displayCommentsSidebar: boolean;
	focusForm: boolean;
	filterStatus: any;
	filterCode: any;
	commentProviderId: any;
	tagProviderId: number;
	providersId: number;
	onPostPrivacy: any;
	public selectFilterData: any;
	public selectFilters: any;
	//coronaId = [1045,45];
	//coronaId = [2776,3001];
	menuClicked: boolean;
	interested: boolean;
	dataProviderId: number;
	investor: boolean;
	myMatches: boolean;
	onBoarding: boolean;
	myMatchesCount: boolean;
	myMatchesActivated: boolean;
	unregisterUserMsg: boolean;
	searchDatamsg: string;
	commonSearch: string;
	searchDataProvider: boolean;
	transactional: boolean;
	jobs: boolean;
	location: boolean;
	shipping: boolean;
	searchActivated: boolean;
	coronaId = [7324, 6787];
	@ViewChild(GenericFilterComponent) genericFilter;
	@ViewChild(MatAutocomplete) matAutocomplete: MatAutocomplete;
	mdDialogRef: MatDialogRef<any>;
	tagAction: any;
	dataValue: any;
	filteredData: any;
	myMatchesData: any;
	uniqueData: any;
	dataProviderObj: any;
	dataProviderTagObj: any;
	searchCommonValue: boolean;
	searchHeader: string;
	ipAddressCount = 0;
	@HostListener('window:resize', ['$event'])
	onResize(event) { // onresize methods used for window dynamic resize depend upon the screen
		const view = event.target.innerWidth;
		if (view > 700) {
			this.expandStatus = true;
			// this.ngOnInit();
		} else if (view < 700) {
			this.expandStatus = false;
			// this.ngOnInit();
		}
	}
	constructor(
		private filtersService: AmassFilterService,
		private dataProviderService: DataProviderService,
		private alertService: JhiAlertService,
		private dataUtils: JhiDataUtils,
		private eventManager: JhiEventManager,
		private parseLinks: JhiParseLinks,
		private activatedRoute: ActivatedRoute,
		private principal: Principal,
		public dialog: MatDialog,
		public _DomSanitizer: DomSanitizer,
		public meta: Meta,
		private http: HttpClient,
		private router: Router,
		public googleAnalyticsService: GoogleAnalyticsEventsService,
		private loaderService: LoaderService,
		private cookieService: CookieService,
		private amassSettingService: AmassSettingService,
		private stateService: StateStorageService,
		private datePipe: DatePipe,
		private dataresourceTabService: DataResourceTabService,
		private dataProviderSearchService: DataProviderSearchService,
		private dataProviderfilterService: DataProviderFilterService,
		private authService: AuthServerProvider,
		private signinModalService: SigninModalService,

		//  private loadingIndicatorService: LoadingIndicatorService
		//  public loaderService: LoaderService
	) {
		this.dataProviders = [];
		this.selectFilterData = [];
		this.selectFilters = [];
		this.itemsPerPage = ITEMS_PER_PAGE;
		this.page = 0;
		this.itemsFrom = 0;
		this.links = {
			last: 0
		};
		this.predicate = 'id';
		this.reverse = true;
		this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
		this.currentFilter = '';
		this.currentCoronaFilter = '';
		this.currentSorting = '';
		this.coronaSorting = '';
		this.isList = true;
		this.domSanitizer = DomSanitizer;
		this.mdTooltipDelay = 500;
		this.expandStatus = true;
		this.displayCommentsSidebar = false;
		// this.loadingIndicatorService.onLoadingChanged.subscribe(isLoading => this.loading = isLoading);
		this.baseUrl = window.location.origin;

		this.meta.updateTag({
			name: 'author',
			content: 'Amass Insights'
		});
		this.meta.updateTag({
			name: 'keywords',
			content: 'Amass Insights, Data Provider, Investors'
		});
		this.meta.updateTag({
			name: 'description',
			content: 'Our web-based \'Insights\' platform, built exclusively for the capital markets industry, is the largest, most comprehensive, and most accurate source of alternative data provider information available anywhere in the world. It provides asset managers with an ever-expanding stream of unique, unbiased, alpha-generating data, geared towards their firm\'s specific interests and needs. These features, combined with Amass\'s proven sourcing ability and value-added analytics, save research teams valuable time searching for, understanding, vetting, and onboarding alternative data.'
		});
	}

	loadAll() {
		this.limitedProviders = '';
		this.loader = true;
		let queryBody = {};
		if (this.isAuthenticated() || this.searchActivated) {
			//this.logoutUsers = true;
			if (this.myMatchesActivated) {
				this.myMatchesActivated = false;
				this.checkMymatches();
			}

			/* 	if(this.corona)
				{
				this.currentSorting={
							key: 'createdDate',
								label: 'createdDate',
								order: 'desc'
							}
						}
						if(this.userLevel && !this.corona){
							this.currentSorting={
							 key: 'recommended',
						   label: 'Recommended',
						   type: 'recommended',
						   recommendedSorting: [
							   {
								   key: 'scoreOverall.sortorder',
								   label: 'Overall Score',
								   order: 'asc',
								   type: 'recommended'
							   },
							   {
								   key: 'scoreInvestorsWillingness.sortorder',
								   label: 'Willingness to Provide to Investors Score',
								   order: 'asc',
								   type: 'recommended'
							   },
							   {
								   key: 'completeness',
								   label: 'Completeness',
								   order: 'desc',
								   type: 'recommended'
							   }
						   ],
						   order: 'desc',
						   userLevel:'unrestricted',
					  }
					   }
					   else if(!this.userLevel && !this.corona) {
					   this.currentSorting={
						key: 'updatedDate',
						   label: 'Updated',
						   order: 'desc',
						   userLevel:'unrestricted',
						   }
					 }
		 */

			/* else if(this.corona)
			{
				this.loadAllCorona();
			} */

			else {
				if (this.currentSorting && this.currentFilter && this.currentSearch) {
					const requestBody = bob.requestBodySearch();
					// requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
					if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
						requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
					} else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
						for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
							requestBody.sort(bob.sort(this.currentSorting.recommendedSorting[i]['key']).order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')).size(20);
						}
					}
					const queryJSON = requestBody.toJSON();
					queryJSON['query'] = {
						query_string: this.currentSearch
					};
					queryJSON['filter'] = this.currentFilter;
					queryBody = queryJSON;
					console.log('queryBody', queryBody);
				} else if (this.currentSorting && this.currentFilter) {
					const requestBody = bob.requestBodySearch();
					// requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
					if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
						requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
					} else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
						for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
							requestBody.sort(bob.sort(this.currentSorting.recommendedSorting[i]['key']).order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')).size(20);
						}
					}
					const queryJSON = requestBody.toJSON();
					queryJSON['filter'] = this.currentFilter;
					queryBody = queryJSON;
				} else if (this.currentFilter && this.currentSearch) {
					queryBody = {
						size: 20,
						query: {
							query_string: this.currentSearch
						},
						filter: this.currentFilter['query']
					};
				} else if (this.currentSorting && this.currentSearch) {
					const requestBody = bob.requestBodySearch();
					// requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
					if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
						requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
					} else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
						for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
							requestBody.sort(bob.sort(this.currentSorting.recommendedSorting[i]['key']).order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')).size(20);
						}
					}
					const queryJSON = requestBody.toJSON();
					queryJSON['query'] = {
						query_string: this.currentSearch
					};
					queryBody = queryJSON;
				} else {
					if (this.currentSearch && this.isAuthenticated() && !this.searchCommonValue) {
						console.log('1');
						queryBody = this.currentSearch;
                        queryBody['size'] = 20;
					}
					if (this.currentSearch && this.isAuthenticated() && this.searchCommonValue) {
						console.log('2');
						queryBody = {
							size: 20,
							query: {
								query_string: this.currentSearch
							}
						};
						//queryBody =this.currentSearch;
					}
					else if (this.currentSearch && !this.isAuthenticated() && !this.searchCommonValue) {
						/* queryBody = {
							size: 10,
							query: {
								query_string: this.currentSearch
							}
						}; */
						queryBody = this.currentSearch;
					}
					else if (this.currentSearch && !this.isAuthenticated() && this.searchCommonValue) {
						queryBody = {
							size: 10,
							query: {
								query_string: this.currentSearch
							}
						};
						//queryBody =this.currentSearch;
					}
					if (this.currentFilter) {
						console.log('3');
						queryBody = {
							size: 20,
							filter: this.currentFilter['query']
						};
					}
					if (this.currentSorting) {
						console.log('4');
						const requestBody = bob.requestBodySearch();
						if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
							requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
						} else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
							for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
								requestBody.sort(bob.sort(this.currentSorting.recommendedSorting[i]['key']).order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')).size(20);
							}
						}
						const queryJSON = requestBody.toJSON();
						queryBody = queryJSON;
					}
				}
				if (this.page) {
					console.log('5');
					if (this.currentSearch && this.searchCommonValue === false) {
                        // Check for provider list button filter query.
						// queryBody = {};
						// queryBody = {
						// 	size: 20,
						// 	query: {
						// 		query_string: this.currentSearch
						// 	}
						// };
                        queryBody = this.currentSearch;
                        queryBody['size'] = 20;
                        console.log(this.currentSearch, queryBody);
					}
					queryBody['from'] = this.itemsFrom;
				}
				if (this.subscription) {
					this.subscription.unsubscribe();
				}

				if (this.isAuthenticated() && localStorage.getItem('SearchData') === null && !this.commonSearch) {
					console.log('querybody', queryBody);

					this.subscription = this.dataProviderService.queryWithSearchFilter(queryBody, this.statusFilter).subscribe(
						(res) => this.onSuccessFilter(res, res.headers),
						(res) => this.onError(res)
					);
					// this.searchCommonValue = false;
				}
				else if (!this.isAuthenticated() && this.searchActivated) {
					this.dataProviderService.checkUserIpAddresss().subscribe(data => {
						console.log('datacheck', data);
						this.ipAddressCount = data.count;

						if (this.ipAddressCount <= 5) {
							this.subscription = this.dataProviderService.queryWithSearchFilter(queryBody, this.statusFilter).subscribe(
								(res) => this.onSuccessFilter(res, res.headers),
								(res) => this.onError(res)
							);
						}
						else {
							this.loader = false;
							this.loaderInfinite = false;
						}
					});
					this.searchCommonValue = false;
				}
                else if (localStorage.getItem('SearchData') || this.commonSearch) {
                    this.subscription = this.dataProviderService.searchData(queryBody,this.commonSearch).subscribe(
                        (res) => this.onSuccessFilter(res, res.headers),
                        (res) => this.onError(res)
                    );
                }
			}
		}
		else {
			if (localStorage.getItem('SearchData')) {
				this.subscription = this.dataProviderService.searchData(queryBody,localStorage.getItem('SearchData')).subscribe(
					(res) => this.onSuccessFilter(res, res.headers),
					(res) => this.onError(res)
				);
			}
			else {
				this.subscription = this.dataProviderService.searchData(queryBody,'unregistered').subscribe(
					(res) => this.onSuccessFilter(res, res.headers),
					(res) => this.onError(res)
				);
			}
			//localStorage.removeItem('SearchData');
		}
		return;
	}
	reset() {
		this.page = 0;
		this.dataProviders = [];
		this.loadAll();
	}

	loadPage(page) {
		this.limitedProviders = '';
		this.page = page;
		console.log('page', this.page);
		this.itemsFrom = (this.page === 0) ? 0 : (this.page * 20);
		console.log('itemsFrom', this.itemsFrom);
		console.log('totalItems', this.totalItems);
		if (this.itemsFrom <= this.totalItems && this.isAuthenticated()) {
			this.limitedProviders = '';
			if (this.page <= 4 && this.currentFilter === '') {
				if (!this.corona) {
					this.loadAll();
				}
				else if (this.myMatches) {
					this.checkMymatches();
				}
				else {
					this.loadAllCorona();
				}
				this.loaderInfinite = true;
				this.loader = false;
				this.limitedProviders = '';
			}
			else if (this.page >= 5) {
				this.limitedProviders = 'Maximum number of entries listed. Please refine your search/filtering criteria to display the most appropriate entries.';
				this.limitedLogoutUsersEvent = false;
				this.loaderInfinite = false;
			}

			else if (this.page > 0 && this.currentFilter && this.page <= 4) {
				if (!this.corona) {
					this.loadAll();
				}
				else if (this.myMatches) {
					this.checkMymatches();
				}
				else {
					this.loadAllCorona();
				}
				this.loaderInfinite = true;
				this.loader = false;
				this.limitedProviders = '';
			}
		}
		else if (this.page <= -1 && !this.isAuthenticated()) {
			this.loadAll();
			this.loaderInfinite = true;
			this.loader = false;
		}
		else {
			if (!this.isAuthenticated()) {
				//this.logoutUsers = true;
				this.limitedLogoutUsersEvent = true;
				this.loaderInfinite = false;
				this.loader = false;
			}
			/* 	else
				{
				this.limitedProviders = 'Maximum number of entries listed. Please refine your search/filtering criteria to display the most appropriate entries.';
				this.limitedLogoutUsersEvent = false;
				this.loaderInfinite = false;
			} */
		}
	}
	clear() {
		this.dataProviders = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.predicate = 'id';
		this.reverse = true;
		this.currentSearch = '';
		this.currentSearchQuery = '';
		this.searchActivated = false;
		this.searchCommonValue = false;
		if (localStorage.getItem('SearchData')) {
			localStorage.removeItem('SearchData');
		}
		this.loadAll();
		console.log('before',localStorage.getItem('search'));
		if (localStorage.getItem('search')) {
			localStorage.removeItem('search');
			console.log('after',localStorage.getItem('search'));
		}
		if (localStorage.getItem('searchheader')) {
			localStorage.removeItem('searchheader');
		}


	}
	searchCommon(query) {
		console.log('query', query)
		if (query) {
			this.searchCommonValue = true;
			this.search(query);
		}
		/* if(localStorage.getItem('searchheader'))
		{
			localStorage.removeItem('searchheader');
		} */
	}
	search(query) {
		console.log('query', query);
		this.searchActivated = true;
		if (this.isAuthenticated() || this.searchActivated) {
			this.googleAnalyticsService.emitEvent('Providers', 'Search Submitted', 'Providers List - Search - Search Button - ' + "'" + query + "'");
			if (!query) {
				console.log('query',query);
				return this.clear();
			}
			this.dataProviders = [];
			this.links = {
				last: 0
			};
			this.page = 0;
			this.predicate = '_score';
			this.reverse = false;
			this.itemsFrom = 0;
			this.sortBy('none');
			this.currentSearchQuery = query.providerTag ? query.providerTag : query;
			if (this.searchCommonValue) {
				this.currentSearch = {
					'query': query + '*', // this.reSubmitValues ? '*' + query + '*' : query,
					'fields': ['_all', 'providerName^2']
				};
			}
			else {
				if (this.currentFilter) {
					const event = 'clear Filter';
					this.dataProviderfilterService.clearFilterRemoveSearch(event);
					this.clearFilter = true;
					this.onClearFilter();
				}
				this.currentSearch = {
					'query': {
						'multi_match': {
							'fields': [
								'providerName',
								'providerTags.desc'
							],
							'query': query.providerTag ? query.providerTag : query,
							'type': 'phrase_prefix'
						}
					}
				};
			}
			console.log('currentFilter', this.currentFilter);
			this.loadAll();


		}
		/* else
		{
			this.openSignUp();
	   } */
	}
	onKeydown(event: any) {
		this.googleAnalyticsService.emitEvents(event.keyword, this.currentUrl);
	}
	/* searchQuery(events: any) {
		this.loader = true;
		// this.googleAnalyticsService.emitEvent(events.category, events.event, events.label + this.searchData);
	} */
	searchClear(events: any) {
		this.loader = true;
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
	customTag(events) {
		// this.dropDownAutoClose = false;
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
		this.customTags = true;
	}
	ngOnInit() {
		//this.userLevel = this.principal.hasAnyPermissionAboveDirect(200);

		this.dataProviderSearchService.headerSearch$.subscribe(event => {
			console.log('event', event);
			if (event) {
				this.searchCommon(event);


			}
		})
		/* this.dataProviderSearchService.messageSearch$.subscribe(event =>{
			console.log('event',event);
			if(event === 'clear')
			{
				this.clear();

			}
		}) */
		this.dataProviderSearchService.messageSearch$.subscribe(event => {
			console.log('event', event);
			if (event === 'clear') {
				this.clear();

			}
			if (event !== 'clear') {
				if (event && event.option && event.option.value === '<strong class="warn-text">At least one provider matches this query but is locked for you. Please contact Amass for access.</strong>') {
					this.currentSearchQuery = '';
				}
				else {
					//console.log(event.option.value.providerTag);
					this.currentSearchQuery = event.option.value.providerTag ? event.option.value.providerTag : event.option.value;
					this.search(this.currentSearchQuery);
				}
			}

		})
		this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
		// if (!this.isAuthenticated()) {
			this.searchDatamsg = localStorage.getItem('SearchData');
			if (this.searchDatamsg === 'Transactional') {
				this.commonSearch = this.searchDatamsg;
				this.transactional = true;
			}
			else if (this.searchDatamsg === 'Shipping') {
				this.commonSearch = this.searchDatamsg;
				this.shipping = true;
			}
			else if (this.searchDatamsg === 'Jobs') {
				this.commonSearch = this.searchDatamsg;
				this.jobs = true;
			}
			else if (this.searchDatamsg === 'Location') {
				this.commonSearch = this.searchDatamsg;
				this.location = true;
			}
			else {
				console.log('searchdata', this.searchDatamsg);
				//localStorage.removeItem('SearchData');
				this.currentSearchQuery = this.searchDatamsg;
				if(this.searchDatamsg)
				{
					this.search(this.searchDatamsg);
				}
			}
		//}
		this.dataProviderSearchService.headerUnrestrictedSearch$.subscribe(event => {
			console.log('event', event);
			console.log('localstorage', localStorage.getItem('SearchData'))
			this.onFilterChangesSearchData(event);
		})
		this.dataProviderSearchService.headerUnrestrictedCommonSearch$.subscribe(event => {
			console.log('event', event);
			console.log('localstorage', localStorage.getItem('searchheader'))
			this.searchCommon(event);
		})


		this.dialog.closeAll();
		this.investor = this.principal.hasAnyAuthorityDirect(['ROLE_INVESTOR']);
		/* if(this.investor)
		{
			this.myMatches = true;
			this.myMatchesActivated = true;
		} */
		if (!this.isAdmin && this.isAuthenticated())
			this.amassSettingService.checkUser().subscribe(data => {
				this.myMatchesData = data;
				this.onBoarding = data.onBoarding;
				this.myMatchesCount = data.myMatchesCount;
				if (data.userOnboarding && this.investor && !data.onBoarding && !data.myMatchesCount) {
					//this.myMatches = true;
					this.myMatches = true;
					this.myMatchesActivated = true;
				}
				if (this.investor && this.myMatchesCount && data.userOnboarding && data.mymatchSearch) {
					this.myMatchesData.mymatchSearch = false;
					this.amassSettingService.save(this.myMatchesData).subscribe(data => {
					});
					this.dialog.open(InvestorDialogComponent);
				}
			});
		if (!this.isAuthenticated()) {
			this.unregisterUserMsg = true;
			//this.loadAll(); // dont remove this variable because of it is related to notification
			this.sortingArray = [
				{
					key: 'sortorder',
					label: 'Name',
					order: '',
					userLevel: 'unrestricted',
					DropDown: 'Name',
					mouseoverExplanation: 'Name of the data provider.'
				},
				{
					key: 'mainDataIndustry.sortorder',
					label: 'Industry',
					order: '',
					userLevel: 'unrestricted',
					DropDown: 'Industry',
					mouseoverExplanation: 'Main data industry of the data provider.'
				},
				{
					key: 'mainDataCategory.sortorder',
					label: 'Category',
					order: '',
					userLevel: 'unrestricted',
					DropDown: 'Category',
					mouseoverExplanation: 'Main data category of the data provider.'
				},
				{
					key: 'createdDate',
					label: 'Added',
					order: '',
					userLevel: 'unrestricted',
					DropDown: 'Added',
					mouseoverExplanation: 'Date the data provider was discovered and added.'
				},
				{
					key: 'updatedDate',
					label: 'Updated',
					order: '',
					userLevel: 'unrestricted',
					DropDown: 'Updated',
					mouseoverExplanation: 'Date the data providers profile was last updated.'
				},
				{
					key: 'completeness',
					label: 'Completeness',
					order: '',
					userLevel: 'unrestricted',
					DropDown: 'Completeness',
					mouseoverExplanation: 'Percent of the data providers profile that has been filled in.'
				},
				{
					key: 'recommended',
					label: 'Recommended',
					type: 'recommended',
					DropDown: 'recommended',
					recommendedSorting: [
						{
							key: 'scoreOverall.sortorder',
							label: 'Overall Score',
							order: 'asc',
							DropDown: 'Overall',
							type: 'recommended'
						},
						{
							key: 'scoreInvestorsWillingness.sortorder',
							label: 'Willingness to Provide to Investors Score',
							order: 'asc',
							DropDown: 'Willingness',
							type: 'recommended'
						},
						{
							key: 'completeness',
							label: 'Completeness',
							order: 'desc',
							DropDown: 'Completeness',
							type: 'recommended'
						}
					],
					order: 'desc',
					userLevel: 'unrestricted',
				}
				/* {
					key: 'scores',
					label: 'scores',
					order: '',
					mouseoverExplanation: 'Percent of the data providers profile that has been filled in.',
					userLevel: this.userLevel? 'scores' : 'scores',
				} */
			];
			this.filtersService.setUnAuthorisedUrl();
			if (localStorage.getItem('SearchData')) {
				this.subscription = this.dataProviderService.searchData('null',localStorage.getItem('SearchData')).subscribe(
					(res) => this.onSuccessFilter(res, res.headers),
					(res) => this.onError(res)
				);
				localStorage.removeItem('SearchData');
			}
			else {
				this.subscription = this.dataProviderService.searchData('null','unregistered').subscribe(
					(res) => this.onSuccessFilter(res, res.headers),
					(res) => this.onError(res)
				);
			}

		}
		if (this.isAuthenticated()) {
			console.log('called');
			this.amassSettingService.userauthorities().subscribe(data => {
				this.userLevel = data;
				if (this.userLevel) {
					this.sortBy({
						key: 'recommended',
						label: 'Recommended',
						type: 'recommended',
						recommendedSorting: [
							{
								key: 'scoreOverall.sortorder',
								label: 'Overall Score',
								order: 'asc',
								type: 'recommended'
							},
							{
								key: 'scoreInvestorsWillingness.sortorder',
								label: 'Willingness to Provide to Investors Score',
								order: 'asc',
								type: 'recommended'
							},
							{
								key: 'completeness',
								label: 'Completeness',
								order: 'desc',
								type: 'recommended'
							}
						],
						order: 'desc',
						userLevel: 'unrestricted',
					});
				} else if (!this.userLevel) {
					this.sortBy({
						key: 'updatedDate',
						label: 'Updated',
						order: 'desc',
						DropDown: 'Updated',
						userLevel: 'unrestricted',
					});
				}
				this.sortingArrayMethod();
				if (localStorage.getItem('search')) {
					this.sortBy('none');
				}
				if (localStorage.getItem('searchheader')) {
					this.sortBy('none');
				}
			});
		}
		//  console.log(this.userLevel);
		this.focusForm = false;
		// this.dropDownAutoClose = false;


		/* this.providerNameOptions = this.providerSearchControl.valueChanges
	  .pipe(
		startWith(''),
		switchMap(val => {
			if (val.length > 2) {
				return this.dataProviderService.queryTag({
					query: {
						prefix: {
							'providerTag': val
						}
					},
					size: 5
				}).pipe(
					map(data => {
						const result = data.body;
						const providerWarningText = '<strong class="warn-text">At least one provider matches this query but is locked for you. Please contact Amass for access.</strong>';
						if (result['hits']['total'] && result['hits']['hits']) {
							const dataObj = result['hits']['hits'].map(item => {
								if (item._source.providerTag) {
									return item._source.providerTag;
								} 
							});
							var uniqueData = dataObj.filter((elem, index, self) => {
								return index === self.indexOf(elem);
							});
							const filteredData = uniqueData;
							filteredData.push(filteredData.splice(filteredData.indexOf(providerWarningText), 1)[0]);
						  //  console.log(filteredData);
							return filteredData;
						} else {
							return [];
						}
					}),
					tap(data => {
						// console.log(data);
					}),
				);
			} else {
				return [];
			}
		}),
	  )
	  .pipe(
		  startWith([]),
	  );     */


		this.loaderActive = false;
		this.totalItemsTooltip = 'Number of data providers matching active search and filter criteria.';
		this.sortByTooltip = 'Sort list of data providers by clicking any of the sort fields.';
		if (this.principal.isAuthenticated()) {
			this.filtersService.setAuthorisedUrl();
		}
		this.limitedProviders = '';
		this.currentUrl = this.router.url;
		this.completenessTooltip = [];
		this.loaderService.display(true);
		this.loader = true;

		this.activatedRoute.queryParams.subscribe(params => {
			this.params = params;
			if (params && params['filter'] && params['id'] && !this.isAuthenticated()) {
				this.router.navigate(['providers']);
			}
			if (params['filter'] && params['id'] && params['type']) {
				this.paramFilter = params['filter'];
				this.paramID = params['id'];
				this.paramType = params['type'];
			}
			if (params['cd']) {
				this.paramCreatedDate = params['cd'];
				this.paramUpdatedDate = null;
			}
			if (params['ud']) {
				this.paramCreatedDate = null;
				this.paramUpdatedDate = params['ud'];
			}
			if (this.params && this.isAuthenticated()) {
				setTimeout(() => {
					this.onFilterChanges({
						selectedFilterData: this.genericFilter.selectedFilterData
					});
				}, 300);
			}
		});
		this.isList = true;
		this.reSubmitValues = false;
		// this.loadAll();
		this.principal.identity().then(account => {
			this.currentAccount = account;
		});
         if(this.isAuthenticated())
		 {
			this.amassSettingService.setAuthorisedUrl().subscribe(data => {
				this.providerUserPermission = data;
			});
		 }
		
		if (this.isAuthenticated()) {
			this.amassSettingService.providerTagCount('Interested').subscribe(data => {
				this.tagProviderMatch = data;
				this.tagInterstedArray = this.tagProviderMatch.interested;
			});
			this.amassSettingService.providerTagCount('Not Interested').subscribe(data => {
				this.tagNotInterstedMatch = data;
				this.tagNotInterstedArray = this.tagNotInterstedMatch.notInterested;
			});
			this.amassSettingService.providerTagCount('In Use').subscribe(data => {
				this.tagInUseMatch = data;
				this.tagInUseArray = this.tagInUseMatch.inUse;
			});
			this.amassSettingService.providerTagCount('Testing').subscribe(data => {
				this.tagTestingMatch = data;
				this.tagTestingArray = this.tagTestingMatch.testing;
			});
		}
		this.registerChangeInDataProviders();

		this.sortCategory = 'Providers';
		this.filterCheckBox = 'Providers';
		this.sortedIndex = '';
		this.providerTypes = [];
		this.mainDataCategories = [];
		this.filters = [];
        if(this.isAuthenticated())
		{
		const mainDataCategoryFilterArray = [];
		this.filtersService.getMainDataCategories().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].providerfilter) ? data['aggregations'].providerfilter.buckets : [];

			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i].filterelement.hits.hits[0]._source.mainDataCategory;
				if (this.isAdmin && item.datacategory !== 'DATA_CATEGORY_NULL') {
					mainDataCategoryFilterArray.push({
						id: listItems[i].key,
						count: listItems[i].doc_count,
						value: item.datacategory, // === 'DATA_CATEGORY_NULL' ? 'No Category' : item.datacategory,
						checked: false,
						locked: item.locked,
						explanation: item.explanation
					});
				} else if (item.datacategory !== 'DATA_CATEGORY_NULL') {
					mainDataCategoryFilterArray.push({
						id: listItems[i].key,
						count: listItems[i].doc_count,
						value: item.datacategory,
						checked: false,
						locked: item.locked,
						explanation: item.explanation
					});
				}
			}
		});

		/*const profileStatusFilterArray = [];
		this.filtersService.getProfileStatus().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			const listItemsLength = listItems.length;
			for (let i = 0; i < listItemsLength; ++i) {
				const item = listItems[i];
				profileStatusFilterArray.push({
					id: item._source.id,
					count: item._source.recordCount? item._source.recordCount : 0,
					value: item._source.description,
					checked: false,
					explanation: item.explanation
				});
			}
		});*/

		this.filtersService.getProvidersTypes().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data;
			const listItemsLength = listItems.length;
			for (let i = 0; i < listItemsLength; ++i) {
				const item = listItems[i];
				this.providerTypes.push({
					id: item.id,
					count: null,
					value: item.providerType,
					checked: false,
					explanation: item.explanation
				});
			}
		});

		const personalTagFilterArray = [];
		this.filtersService.getDataProviderTagUrl().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data;
			const listItemsLength = listItems.length;
			for (let i = 0; i < listItemsLength; ++i) {
				const item = listItems[i];
				//  console.log(item);
				if (item.privacy === 'Private') {
					//  console.log(item);
					personalTagFilterArray.push({
						id: item.id,
						count: null,
						value: item.providerTag,
						checked: false,
						explanation: item.explanation
					});
				}
			}
			this.tagAction = personalTagFilterArray;
		});

		/*const relatedDataCategoryFilterArray = [];
		this.filtersService.getRelatedDataCategory().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				if (this.isAdmin) {
					relatedDataCategoryFilterArray.push({
						id: item._source.id,
						count: item._source.relevantDataCategoryProviderCount,
						value: item._source.datacategory, // === 'DATA_CATEGORY_NULL' ? 'No Category' : item._source.datacategory,
						checked: false,
						locked: item._source.locked,
						explanation: item._source.explanation
					});
				} else if (item._source.datacategory !== 'DATA_CATEGORY_NULL') {
					relatedDataCategoryFilterArray.push({
						id: item._source.id,
						count: item._source.relevantDataCategoryProviderCount,
						value: item._source.datacategory,
						checked: false,
						locked: item._source.locked,
						explanation: item._source.explanation
					});
				}
			}
		});
		const relatedDataIndustryFilterArray = [];
		this.filtersService.getRelatedDataIndustry().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				if (this.isAdmin && item._source.dataIndustry !== 'DATA_INDUSTRY_NULL') {
					relatedDataIndustryFilterArray.push({
						id: item._source.id,
						count: item._source.relevantDataIndustryProviderCount,
						value: item._source.dataIndustry, // === 'DATA_INDUSTRY_NULL' ? 'No Industry' : item._source.dataIndustry,
						checked: false,
						explanation: item._source.explanation
					});
				} else if (item._source.dataIndustry !== 'DATA_INDUSTRY_NULL') {
					relatedDataIndustryFilterArray.push({
						id: item._source.id,
						count: item._source.relevantDataIndustryProviderCount,
						value: item._source.dataIndustry,
						checked: false,
						explanation: item._source.explanation
					});
				}
			}
		});
		const mainDataIndustryArray = [];
		this.filtersService.getMainDataIndustry().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].dataindustries) ? data['aggregations'].dataindustries.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i].mainDataIndustry.hits.hits[0]._source.mainDataIndustry;
				if (this.isAdmin) {
					mainDataIndustryArray.push({
						id: listItems[i].key,
						count: listItems[i].doc_count,
						value: item.dataIndustry, // === 'DATA_INDUSTRY_NULL' ? 'No Industry' : item.dataIndustry,
						checked: false,
						explanation: item.explanation
					});
				} else if (item.dataIndustry !== 'DATA_INDUSTRY_NULL') {
					mainDataIndustryArray.push({
						id: listItems[i].key,
						count: listItems[i].doc_count,
						value: item.dataIndustry,
						checked: false,
						explanation: item.explanation
					});
				}
			}
		});
		const dataFeaturesFilterArray = [];
		this.filtersService.getDataFeatures().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				dataFeaturesFilterArray.push({
					id: item._source.id,
					count: item._source.providerCount,
					value: item._source.dataFeature,
					checked: false,
					explanation: item._source.explanation
				});
			}
		});
		const geographicFocusFilterArray = [];
		this.filtersService.getGeographicFocus().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				geographicFocusFilterArray.push({
					id: item._source.id,
					count: item._source.recordCount,
					value: item._source.lookupcode,
					checked: false
				});
			}
		});*/
		const assetMainClassFilterArray = [];
		const listAssetMainClassFilterArray = [];
		this.filtersService.getMainAssetClass().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].mainAssetclass) ? data['aggregations'].mainAssetclass.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				const asset = item.mainAssetClass.hits.hits[0]._source.mainAssetClass;
				if (this.isAdmin) {
					assetMainClassFilterArray.push({
						id: item.key,
						count: item.doc_count,
						value: asset, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//	console.log(assetMainClassFilterArray);
				} else if (item.mainAssetClass.hits.hits[0]._source.mainAssetClass !== 'DEFAULT_NULL') {
					assetMainClassFilterArray.push({
						id: item.key,
						count: item.doc_count,
						value: item.mainAssetClass.hits.hits[0]._source.mainAssetClass,
						checked: false
					});
				}
				//		console.log('Main', assetMainClassFilterArray);
				this.mainAssetClassFilterArray = _.uniqBy(assetMainClassFilterArray, 'value');
			}
			//	console.log('Main 1', this.mainAssetClassFilterArray);
			for (let i = 0; i < this.mainAssetClassFilterArray.length; i++) {
				const list = this.mainAssetClassFilterArray[i];
				//console.log(list);
				listAssetMainClassFilterArray.push({
					id: list.id,
					count: list.count,
					value: list.value,
					checked: false
				});
				//console.log(listAssetMainClassFilterArray);
			}

		});
		/*  Ranjith commented
				const relatedAssetClassFilterArray = [];
				this.filtersService.getRelatedAssetClass().subscribe(data => {
					this.loaderService.display(false);
					const listItems = data['hits'].hits;
					for (let i = 0; i < listItems.length; ++i) {
						const item = listItems[i];
						if (this.isAdmin && item._source.description !== 'DEFAULT_NULL') {
							relatedAssetClassFilterArray.push({
								id: item._source.id,
								count: item._source.recordCount,
								value: item._source.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : item._source.description,
								checked: false
							});
						} else if (item._source.description !== 'DEFAULT_NULL') {
							relatedAssetClassFilterArray.push({
								id: item._source.id,
								count: item._source.recordCount,
								value: item._source.description,
								checked: false
							});
						}
					}
				});
		
				const researchStatusFilterArray = [];
				this.filtersService.getResearchStatus().subscribe(data => {
					this.loaderService.display(false);
					const listItems = data['hits'].hits;
					for (let i = 0; i < listItems.length; ++i) {
						const item = listItems[i];
						if (this.isAdmin) {
							researchStatusFilterArray.push({
								id: item._source.id,
								count: item._source.recordCount? item._source.recordCount : 0,
								value: item._source.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : item._source.description,
								checked: false
							});
						}
					}
				});
		
			   const priorityFilterArray = [];
				this.filtersService.getPriority().subscribe(data => {
					this.loaderService.display(false);
					const listItems = data['hits'].hits;
					for (let i = 0; i < listItems.length; ++i) {
						const item = listItems[i];
						if (this.isAdmin) {
							priorityFilterArray.push({
								id: item._source.id,
								count: item._source.recordCount,
								value: item._source.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : item._source.description,
								checked: false
							});
						}
					}
				});
		
		
				const securityTypeFilterArray = [];
				this.filtersService.getSecurityType().subscribe(data => {
					this.loaderService.display(false);
					const listItems = data['hits'].hits;
					for (let i = 0; i < listItems.length; ++i) {
						const item = listItems[i];
						securityTypeFilterArray.push({
							id: item._source.id,
							count: item._source.recordCount,
							value: item._source.lookupcode,
							checked: false
						});
					}
				});
				const relatedSectorsArray = [];
				this.filtersService.getRelatedSectors().subscribe(data => {
					this.loaderService.display(false);
					const listItems = data['hits'].hits;
					for (let i = 0; i < listItems.length; ++i) {
						const item = listItems[i];
						relatedSectorsArray.push({
							id: item._source.id,
							count: item._source.recordCount,
							value: item._source.lookupcode,
							checked: false
						});
					}
				});
				const investorTypeFilterArray = [];
				this.filtersService.getInvestorType().subscribe(data => {
					this.loaderService.display(false);
					const listItems = data['hits'].hits;
					for (let i = 0; i < listItems.length; ++i) {
						const item = listItems[i];
						investorTypeFilterArray.push({
							id: item._source.id,
							count: item._source.recordCount,
							value: item._source.lookupcode,
							checked: false
						});
					}
				});
				const dataSourceFilterArray = [];
				this.filtersService.getDataSource().subscribe(data => {
					this.loaderService.display(false);
					const listItems = data['hits'].hits;
					for (let i = 0; i < listItems.length; ++i) {
						const item = listItems[i];
						dataSourceFilterArray.push({
							id: item._source.id,
							count: item._source.providerCount,
							value: item._source.dataSource,
							checked: false,
							explanation: item._source.explanation
						});
					}
				});*/
		const dataProviderTag = [];
		this.filtersService.getDataProviderTagUrl().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data;
			const listItemsLength = listItems.length;
			for (let i = 0; i < listItemsLength; ++i) {
				const item = listItems[i];
				dataProviderTag.push({
					id: item.id,
					count: null,
					value: item.providerTag,
					checked: false,
					explanation: item.explanation
				});
			}
		});
		/*const pricingModelFilterArray = [];
		this.filtersService.getPricingModel().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				pricingModelFilterArray.push({
					id: item._source.id,
					count: item._source.recordCount,
					value: item._source.description,
					checked: false
				});
			}
		});
		const willingnessInvetorsFilterArray = [];
		this.filtersService.getWillingnessToProvideToInvestors().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			//console.log('ListItems', listItems);
			listItems.pop();
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				//console.log('Item', item);
				willingnessInvetorsFilterArray.push({
					id: item._source.id,
					count: item._source.recordCount,
					value: item._source.description,
					checked: false
				});
			}
		});
		const uniquenessFilterArray = [];
		this.filtersService.getUniqueness().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			listItems.pop();
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				uniquenessFilterArray.push({
					id: item._source.id,
					count: item._source.recordCount,
					value: item._source.description,
					checked: false
				});
			}
		});
		const valueFilterArray = [];
		this.filtersService.getValue().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			listItems.pop();
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				valueFilterArray.push({
					id: item._source.id,
					count: item._source.recordCount,
					value: item._source.description,
					checked: false
				});
			}
		});
		const readinessFilterArray = [];
		this.filtersService.getReadiness().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			listItems.pop();
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				readinessFilterArray.push({
					id: item._source.id,
					count: item._source.recordCount,
					value: item._source.description,
					checked: false
				});
			}
		});
		const overallFilterArray = [];
		this.filtersService.getOverall().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			listItems.pop();
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				overallFilterArray.push({
					id: item._source.id,
					count: item._source.recordCount,
					value: item._source.description,
					checked: false
				});
			}
		});
		const deliveryMethodFilterArray = [];
		this.filtersService.getDeliveryMethods().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				deliveryMethodFilterArray.push({
					id: item._source.id,
					count: item._source.recordCount,
					value: item._source.lookupcode,
					checked: false
				});
			}
		});
	   const deliveryFormatFilterArray = [];
		this.filtersService.getDeliveryFormat().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				deliveryFormatFilterArray.push({
					id: item._source.id,
					count: item._source.recordCount,
					value: item._source.lookupcode,
					checked: false
				});
			}
		});
		const deliveryFrequencyArray = [];
		this.filtersService.deliveryFrequency().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data['hits'].hits;
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				deliveryFrequencyArray.push({
					id: item._source.id,
					count: item._source.recordCount,
					value: item._source.lookupcode,
					checked: false
				});
			}
		});
		this.filtersService.getDataProviderTagUrl().subscribe(data => {
			this.loaderService.display(false);
			const listItems = data;
			this.tagDropDownValues = listItems;
			console.log(this.tagDropDownValues);
		}); */


		/*------------------Filter List and count - Ranjith - Start-----------------------*/

		const relatedDataCategoryFilterArray = [];
		this.filtersService.getCategoriesForFilter().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].categories) ? data['aggregations'].categories.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const category = listItems[i];
				const categoryId = parseInt(category.key, 10);
				const categoryList = category.categories.hits.hits[0]._source.categories;
				//  console.log('Ranjith testing aggregation ', deliveryFormat);
				const filteredCategoryDescription = categoryList.filter((item) => item.id === category.key);
				//   console.log('Dformat 1',filteredDeliveryFormatDescription);
				if (this.isAdmin) {
					relatedDataCategoryFilterArray.push({
						id: categoryId,
						count: category.doc_count,
						value: filteredCategoryDescription && filteredCategoryDescription[0] && filteredCategoryDescription[0].desc ? filteredCategoryDescription[0].desc : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (category.categories.hits.hits[0]._source.categories !== 'DEFAULT_NULL') {
					relatedDataCategoryFilterArray.push({
						id: categoryId,
						count: category.doc_count,
						value: filteredCategoryDescription && filteredCategoryDescription[0] && filteredCategoryDescription[0].desc ? filteredCategoryDescription[0].desc : '',
						checked: false
					});
				}

			}
			// console.log('Dformat ',deliveryFormatsArrayForFilter);
		});
		const pricingModelArrayForFilter = [];
		this.filtersService.getPricingModelForFilter().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].dataProvidersPricingModels) ? data['aggregations'].dataProvidersPricingModels.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				const dataProviderPriceId = parseInt(item.key, 10);
				const priceModel = item.dataProvidersPricingModels.hits.hits[0]._source.dataProvidersPricingModels;
				const priceModelDescription = priceModel.filter((items) => items.id === item.key);
				if (this.isAdmin) {
					pricingModelArrayForFilter.push({
						id: dataProviderPriceId,
						count: item.doc_count,
						value: priceModelDescription && priceModelDescription[0] && priceModelDescription[0].description ? priceModelDescription[0].description : '',  // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (item.dataProvidersPricingModels.hits.hits[0]._source.dataProvidersPricingModels !== 'DEFAULT_NULL') {
					pricingModelArrayForFilter.push({
						id: dataProviderPriceId,
						count: item.doc_count,
						value: priceModelDescription && priceModelDescription[0] && priceModelDescription[0].description ? priceModelDescription[0].description : '',
						checked: false
					});
				}

			}
		});
		const deliveryFormatsArrayForFilter = [];
		this.filtersService.getDeliveryFormatsForFilter().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].deliveryFormats) ? data['aggregations'].deliveryFormats.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const dFormats = listItems[i];
				const changedId = parseInt(dFormats.key, 10);
				const deliveryFormat = dFormats.deliveryFormats.hits.hits[0]._source.deliveryFormats;
				//  console.log('Ranjith testing aggregation ', deliveryFormat);
				const filteredDeliveryFormatDescription = deliveryFormat.filter((item) => item.id === dFormats.key);
				//   console.log('Dformat 1',filteredDeliveryFormatDescription);
				if (this.isAdmin) {
					deliveryFormatsArrayForFilter.push({
						id: changedId,
						count: dFormats.doc_count,
						value: filteredDeliveryFormatDescription && filteredDeliveryFormatDescription[0] && filteredDeliveryFormatDescription[0].desc ? filteredDeliveryFormatDescription[0].desc : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (deliveryFormat !== 'DEFAULT_NULL') {
					deliveryFormatsArrayForFilter.push({
						id: changedId,
						count: dFormats.doc_count,
						value: filteredDeliveryFormatDescription && filteredDeliveryFormatDescription[0] && filteredDeliveryFormatDescription[0].desc ? filteredDeliveryFormatDescription[0].desc : '',
						checked: false
					});
				}

			}
			// console.log('Dformat ',deliveryFormatsArrayForFilter);
		});

		const mainDataIndustryArray = [];
		this.filtersService.getMainDataIndustry().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].dataindustries) ? data['aggregations'].dataindustries.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i].mainDataIndustry.hits.hits[0]._source.mainDataIndustry;
				if (this.isAdmin) {
					mainDataIndustryArray.push({
						id: listItems[i].key,
						count: listItems[i].doc_count,
						value: item.dataIndustry, // === 'DATA_INDUSTRY_NULL' ? 'No Industry' : item.dataIndustry,
						checked: false,
						explanation: item.explanation
					});
				} else if (item.dataIndustry !== 'DATA_INDUSTRY_NULL') {
					mainDataIndustryArray.push({
						id: listItems[i].key,
						count: listItems[i].doc_count,
						value: item.dataIndustry,
						checked: false,
						explanation: item.explanation
					});
				}
			}
		});


		const profileStatusFilterArray = [];
		this.filtersService.getProfileStatus().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].marketplaceStatus) ? data['aggregations'].marketplaceStatus.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				const price = item.marketplaceStatus.hits.hits[0]._source.marketplaceStatus;
				if (this.isAdmin) {
					profileStatusFilterArray.push({
						id: item.key,
						count: item.doc_count,
						value: price.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (item.marketplaceStatus.hits.hits[0]._source.marketplaceStatus !== 'DEFAULT_NULL') {
					profileStatusFilterArray.push({
						id: item.key,
						count: item.doc_count,
						value: price.description,
						checked: false
					});
				}

			}
		});


		const researchStatusFilterArray = [];
		this.filtersService.getResearchStatus().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].ieiStatus) ? data['aggregations'].ieiStatus.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				const price = item.ieiStatus.hits.hits[0]._source.ieiStatus;
				if (this.isAdmin) {
					researchStatusFilterArray.push({
						id: item.key,
						count: item.doc_count,
						value: price.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (item.ieiStatus.hits.hits[0]._source.ieiStatus !== 'DEFAULT_NULL') {
					researchStatusFilterArray.push({
						id: item.key,
						count: item.doc_count,
						value: price.description,
						checked: false
					});
				}

			}
		});


		const geographicFocusFilterArray = [];
		this.filtersService.getGeographicalFocus().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].geographicalFoci) ? data['aggregations'].geographicalFoci.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const geographicalFocus = listItems[i];
				const changedId = parseInt(geographicalFocus.key, 10);
				const geographical = geographicalFocus.geographicalFoci.hits.hits[0]._source.geographicalFoci;
				//  console.log('Ranjith testing aggregation ', deliveryFormat);
				const geographicalDescription = geographical.filter((item) => item.id === geographicalFocus.key);
				//   console.log('Dformat 1',filteredDeliveryFormatDescription);
				if (this.isAdmin) {
					geographicFocusFilterArray.push({
						id: changedId,
						count: geographicalFocus.doc_count,
						value: geographicalDescription && geographicalDescription[0] && geographicalDescription[0].description ? geographicalDescription[0].description : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (geographical !== 'DEFAULT_NULL') {
					geographicFocusFilterArray.push({
						id: changedId,
						count: geographicalFocus.doc_count,
						value: geographicalDescription && geographicalDescription[0] && geographicalDescription[0].description ? geographicalDescription[0].description : '',
						checked: false
					});
				}

			}
			// console.log('Dformat ',deliveryFormatsArrayForFilter);
		});

		const priorityFilterArray = [];
		const listPriorityFilterArray = [];
		this.filtersService.getPriorityFilter().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].priority) ? data['aggregations'].priority.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i];
				const asset = item.priority.hits.hits[0]._source.priority;
				if (this.isAdmin) {
					priorityFilterArray.push({
						id: item.key,
						count: item.doc_count,
						value: asset, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//	console.log(assetMainClassFilterArray);
				} else if (item.priority.hits.hits[0]._source.priority !== 'DEFAULT_NULL') {
					priorityFilterArray.push({
						id: item.key,
						count: item.doc_count,
						value: item.priority.hits.hits[0]._source.priority,
						checked: false
					});
				}
				//		console.log('Main', assetMainClassFilterArray);
				this.priorityFilter = _.uniqBy(priorityFilterArray, 'value');
			}
			//	console.log('Main 1', this.mainAssetClassFilterArray);
			for (let i = 0; i < this.priorityFilter.length; i++) {
				const list = this.priorityFilter[i];
				//console.log(list);
				listPriorityFilterArray.push({
					id: list.id,
					count: list.count,
					value: list.value,
					checked: false
				});
				//console.log(listAssetMainClassFilterArray);
			}

		});


		const relatedDataIndustryFilterArray = [];
		this.filtersService.getIndustriesForFilter().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].industries) ? data['aggregations'].industries.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const industry = listItems[i];
				const industryId = parseInt(industry.key, 10);
				const industryList = industry.industries.hits.hits[0]._source.industries;
				//  console.log('Ranjith testing aggregation ', deliveryFormat);
				const filteredIndustryDescription = industryList.filter((item) => item.id === industry.key);
				//   console.log('Dformat 1',filteredDeliveryFormatDescription);
				if (this.isAdmin) {
					relatedDataIndustryFilterArray.push({
						id: industryId,
						count: industry.doc_count,
						value: filteredIndustryDescription && filteredIndustryDescription[0] && filteredIndustryDescription[0].desc ? filteredIndustryDescription[0].desc : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (industry.industries.hits.hits[0]._source.industries !== 'DEFAULT_NULL') {
					relatedDataIndustryFilterArray.push({
						id: industryId,
						count: industry.doc_count,
						value: filteredIndustryDescription && filteredIndustryDescription[0] && filteredIndustryDescription[0].desc ? filteredIndustryDescription[0].desc : '',
						checked: false
					});
				}

			}
			// console.log('Dformat ',deliveryFormatsArrayForFilter);
		});


		const dataFeaturesFilterArray = [];
		this.filtersService.getFeaturesForFilter().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].features) ? data['aggregations'].features.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const feature = listItems[i];
				const featureId = parseInt(feature.key, 10);
				const featureList = feature.features.hits.hits[0]._source.features;
				//  console.log('Ranjith testing aggregation ', deliveryFormat);
				const filteredFeatureDescription = featureList.filter((item) => item.id === feature.key);
				//   console.log('Dformat 1',filteredDeliveryFormatDescription);
				if (this.isAdmin) {
					dataFeaturesFilterArray.push({
						id: featureId,
						count: feature.doc_count,
						value: filteredFeatureDescription && filteredFeatureDescription[0] && filteredFeatureDescription[0].desc ? filteredFeatureDescription[0].desc : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (feature.features.hits.hits[0]._source.features !== 'DEFAULT_NULL') {
					dataFeaturesFilterArray.push({
						id: featureId,
						count: feature.doc_count,
						value: filteredFeatureDescription && filteredFeatureDescription[0] && filteredFeatureDescription[0].desc ? filteredFeatureDescription[0].desc : '',
						checked: false
					});
				}

			}
			// console.log('Dformat ',deliveryFormatsArrayForFilter);
		});

		const relatedAssetClassFilterArray = [];
		this.filtersService.getAssetClassesForFilter().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].assetClasses) ? data['aggregations'].assetClasses.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const assetClasse = listItems[i];
				const assetClasseId = parseInt(assetClasse.key, 10);
				const assetClasseList = assetClasse.assetClasses.hits.hits[0]._source.assetClasses;
				//  console.log('Ranjith testing aggregation ', deliveryFormat);
				const assetClasseDescription = assetClasseList.filter((item) => item.id === assetClasse.key);
				//   console.log('Dformat 1',filteredDeliveryFormatDescription);
				if (this.isAdmin) {
					relatedAssetClassFilterArray.push({
						id: assetClasseId,
						count: assetClasse.doc_count,
						value: assetClasseDescription && assetClasseDescription[0] && assetClasseDescription[0].desc ? assetClasseDescription[0].desc : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (assetClasse.assetClasses.hits.hits[0]._source.assetClasses !== 'DEFAULT_NULL') {
					relatedAssetClassFilterArray.push({
						id: assetClasseId,
						count: assetClasse.doc_count,
						value: assetClasseDescription && assetClasseDescription[0] && assetClasseDescription[0].desc ? assetClasseDescription[0].desc : '',
						checked: false
					});
				}

			}
			// console.log('Dformat ',deliveryFormatsArrayForFilter);
		});



		const securityTypeFilterArray = [];
		this.filtersService.getSecurityTypesForFilter().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].securityTypes) ? data['aggregations'].securityTypes.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const securityType = listItems[i];
				const securityTypeId = parseInt(securityType.key, 10);
				const securityTypeList = securityType.securityTypes.hits.hits[0]._source.securityTypes;
				//  console.log('Ranjith testing aggregation ', deliveryFormat);
				const securityTypeDescription = securityTypeList.filter((item) => item.id === securityType.key);
				//   console.log('Dformat 1',filteredDeliveryFormatDescription);
				if (this.isAdmin) {
					securityTypeFilterArray.push({
						id: securityTypeId,
						count: securityType.doc_count,
						value: securityTypeDescription && securityTypeDescription[0] && securityTypeDescription[0].desc ? securityTypeDescription[0].desc : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (securityType.securityTypes.hits.hits[0]._source.securityTypes !== 'DEFAULT_NULL') {
					securityTypeFilterArray.push({
						id: securityTypeId,
						count: securityType.doc_count,
						value: securityTypeDescription && securityTypeDescription[0] && securityTypeDescription[0].desc ? securityTypeDescription[0].desc : '',
						checked: false
					});
				}

			}
			// console.log('Dformat ',deliveryFormatsArrayForFilter);
		});


		const relatedSectorsArray = [];
		this.filtersService.getRelevantSectorsForFilter().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].relevantSectors) ? data['aggregations'].relevantSectors.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const relevantSector = listItems[i];
				const relevantSectorId = parseInt(relevantSector.key, 10);
				const relevantSectorList = relevantSector.relevantSectors.hits.hits[0]._source.relevantSectors;
				const relevantSectorDescription = relevantSectorList.filter((item) => item.id === relevantSector.key);
				if (this.isAdmin) {
					relatedSectorsArray.push({
						id: relevantSectorId,
						count: relevantSector.doc_count,
						value: relevantSectorDescription && relevantSectorDescription[0] && relevantSectorDescription[0].desc ? relevantSectorDescription[0].desc : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (relevantSector.relevantSectors.hits.hits[0]._source.relevantSectors !== 'DEFAULT_NULL') {
					relatedSectorsArray.push({
						id: relevantSectorId,
						count: relevantSector.doc_count,
						value: relevantSectorDescription && relevantSectorDescription[0] && relevantSectorDescription[0].desc ? relevantSectorDescription[0].desc : '',
						checked: false
					});
				}

			}
			// console.log('Dformat ',deliveryFormatsArrayForFilter);
		});

		const investorTypeFilterArray = [];
		this.filtersService.getInvestorTypes().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].investorTypes) ? data['aggregations'].investorTypes.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const investorType = listItems[i];
				const investorTypeId = parseInt(investorType.key, 10);
				const investor = investorType.investorTypes.hits.hits[0]._source.investorTypes;
				//  console.log('Ranjith testing aggregation ', deliveryFormat);
				const investorTypeDescription = investor.filter((item) => item.id === investorType.key);
				//   console.log('Dformat 1',filteredDeliveryFormatDescription);
				if (this.isAdmin) {
					investorTypeFilterArray.push({
						id: investorTypeId,
						count: investorType.doc_count,
						value: investorTypeDescription && investorTypeDescription[0] && investorTypeDescription[0].description ? investorTypeDescription[0].description : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (investorType.investorTypes.hits.hits[0]._source.investorTypes !== 'DEFAULT_NULL') {
					investorTypeFilterArray.push({
						id: investorTypeId,
						count: investorType.doc_count,
						value: investorTypeDescription && investorTypeDescription[0] && investorTypeDescription[0].description ? investorTypeDescription[0].description : '',
						checked: false
					});
				}

			}
			// console.log('Dformat ',deliveryFormatsArrayForFilter);
		});

		const dataSourceFilterArray = [];
		this.filtersService.getSourcesForFilter().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].sources) ? data['aggregations'].sources.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const source = listItems[i];
				const sourceId = parseInt(source.key, 10);
				const sourceIdList = source.sources.hits.hits[0]._source.sources;
				//  console.log('Ranjith testing aggregation ', relevantSector);
				const sourceDescription = sourceIdList.filter((item) => item.id === source.key);
				//   console.log('Dformat 1',relevantSectorDescription);
				if (this.isAdmin) {
					dataSourceFilterArray.push({
						id: sourceId,
						count: source.doc_count,
						value: sourceDescription && sourceDescription[0] && sourceDescription[0].desc ? sourceDescription[0].desc : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (source.sources.hits.hits[0]._source.sources !== 'DEFAULT_NULL') {
					dataSourceFilterArray.push({
						id: sourceId,
						count: source.doc_count,
						value: sourceDescription && sourceDescription[0] && sourceDescription[0].desc ? sourceDescription[0].desc : '',
						checked: false
					});
				}

			}
			// console.log('Dformat ',deliveryFormatsArrayForFilter);
		});


		const willingnessInvetorsFilterArray = [];
		this.filtersService.getScoreInvestorsWillingness().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].scoreInvestorsWillingness) ? data['aggregations'].scoreInvestorsWillingness.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const willingness = listItems[i];
				const scoreInvestorsWilling = willingness.scoreInvestorsWillingness.hits.hits[0]._source.scoreInvestorsWillingness;
				//   console.log('scoreInvestorsWilling',scoreInvestorsWilling);
				if (this.isAdmin) {
					if (scoreInvestorsWilling.description !== '') {
						willingnessInvetorsFilterArray.push({
							id: willingness.key,
							count: willingness.doc_count,
							value: scoreInvestorsWilling.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
							checked: false
						});
					}
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (willingness.scoreInvestorsWillingness.hits.hits[0]._source.scoreInvestorsWillingness !== 'DEFAULT_NULL') {
					if (scoreInvestorsWilling.description !== '') {
						willingnessInvetorsFilterArray.push({
							id: willingness.key,
							count: willingness.doc_count,
							value: scoreInvestorsWilling.description,
							checked: false
						});
					}
				}

			}
		});

		const uniquenessFilterArray = [];
		this.filtersService.getUniquenessFilterArray().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].scoreUniqueness) ? data['aggregations'].scoreUniqueness.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const scoreUnique = listItems[i];
				const scores = scoreUnique.scoreUniqueness.hits.hits[0]._source.scoreUniqueness;
				//    console.log('scoreInvestorsWilling',scoreInvestorsWilling);
				if (this.isAdmin) {
					if (scores.description !== '') {
						uniquenessFilterArray.push({
							id: scoreUnique.key,
							count: scoreUnique.doc_count,
							value: scores.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
							checked: false
						});
					}
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (scoreUnique.scoreUniqueness.hits.hits[0]._source.scoreUniqueness !== 'DEFAULT_NULL') {
					if (scores.description !== '') {
						uniquenessFilterArray.push({
							id: scoreUnique.key,
							count: scoreUnique.doc_count,
							value: scores.description,
							checked: false
						});
					}
				}

			}
		});

		const valueFilterArray = [];
		this.filtersService.getValueFilterArray().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].scoreValue) ? data['aggregations'].scoreValue.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const scoreValues = listItems[i];
				const scoreValueList = scoreValues.scoreValue.hits.hits[0]._source.scoreValue;
				//    console.log('scoreInvestorsWilling',scoreInvestorsWilling);
				if (this.isAdmin) {
					if (scoreValueList.description !== '') {
						valueFilterArray.push({
							id: scoreValues.key,
							count: scoreValues.doc_count,
							value: scoreValueList.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
							checked: false
						});
					}
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (scoreValues.scoreValue.hits.hits[0]._source.scoreValue !== 'DEFAULT_NULL') {
					if (scoreValueList.description !== '') {
						valueFilterArray.push({
							id: scoreValues.key,
							count: scoreValues.doc_count,
							value: scoreValueList.description,
							checked: false
						});
					}
				}

			}
		});

		const readinessFilterArray = [];
		this.filtersService.getScoreReadinessFilterArray().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].scoreReadiness) ? data['aggregations'].scoreReadiness.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const readiness = listItems[i];
				const scoreReadinessList = readiness.scoreReadiness.hits.hits[0]._source.scoreReadiness;
				//    console.log('scoreInvestorsWilling',scoreInvestorsWilling);
				if (this.isAdmin) {
					if (scoreReadinessList.description !== '') {
						readinessFilterArray.push({
							id: readiness.key,
							count: readiness.doc_count,
							value: scoreReadinessList.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
							checked: false
						});
					}
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (readiness.scoreReadiness.hits.hits[0]._source.scoreReadiness !== 'DEFAULT_NULL') {
					if (scoreReadinessList.description !== '') {
						readinessFilterArray.push({
							id: readiness.key,
							count: readiness.doc_count,
							value: scoreReadinessList.description,
							checked: false
						});
					}
				}

			}
		});

		const overallFilterArray = [];
		this.filtersService.getScoreOverallFilterArray().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].scoreOverall) ? data['aggregations'].scoreOverall.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const score = listItems[i];
				const scoreOverallList = score.scoreOverall.hits.hits[0]._source.scoreOverall;
				if (this.isAdmin) {
					if (scoreOverallList.description !== '') {
						overallFilterArray.push({
							id: score.key,
							count: score.doc_count,
							value: scoreOverallList.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
							checked: false
						});
					}
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (score.scoreOverall.hits.hits[0]._source.scoreOverall !== 'DEFAULT_NULL') {
					if (scoreOverallList.description !== '') {
						overallFilterArray.push({
							id: score.key,
							count: score.doc_count,
							value: scoreOverallList.description,
							checked: false
						});
					}
				}

			}
		});

		const deliveryMethodFilterArray = [];
		this.filtersService.getDeliveryMethodsForFilter().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].deliveryMethods) ? data['aggregations'].deliveryMethods.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const dMethods = listItems[i];
				const deliveryMethodId = parseInt(dMethods.key, 10);
				const deliveryMethod = dMethods.deliveryMethods.hits.hits[0]._source.deliveryMethods;
				//  console.log('Ranjith testing aggregation ', deliveryFormat);
				const filteredDeliveryMethodDescription = deliveryMethod.filter((item) => item.id === dMethods.key);
				//   console.log('Dformat 1',filteredDeliveryFormatDescription);
				if (this.isAdmin) {
					deliveryMethodFilterArray.push({
						id: deliveryMethodId,
						count: dMethods.doc_count,
						value: filteredDeliveryMethodDescription && filteredDeliveryMethodDescription[0] && filteredDeliveryMethodDescription[0].desc ? filteredDeliveryMethodDescription[0].desc : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (dMethods.deliveryMethods.hits.hits[0]._source.deliveryMethods !== 'DEFAULT_NULL') {
					deliveryMethodFilterArray.push({
						id: deliveryMethodId,
						count: dMethods.doc_count,
						value: filteredDeliveryMethodDescription && filteredDeliveryMethodDescription[0] && filteredDeliveryMethodDescription[0].desc ? filteredDeliveryMethodDescription[0].desc : '',
						checked: false
					});
				}

			}
			// console.log('Dformat ',deliveryFormatsArrayForFilter);
		});

		const deliveryFrequencyArray = [];
		this.filtersService.getDeliveryFrequenciesForFilter().subscribe(data => {
			this.loaderService.display(false);
			const listItems = (data['aggregations'] && data['aggregations'].deliveryFrequencies) ? data['aggregations'].deliveryFrequencies.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const frequencies = listItems[i];
				const deliveryFrequencyId = parseInt(frequencies.key, 10);
				const deliveryFrequency = frequencies.deliveryFrequencies.hits.hits[0]._source.deliveryFrequencies;
				//  console.log('Ranjith testing aggregation ', deliveryFormat);
				const deliveryFrequencyDescription = deliveryFrequency.filter((item) => item.id === frequencies.key);
				//   console.log('Dformat 1',filteredDeliveryFormatDescription);
				if (this.isAdmin) {
					deliveryFrequencyArray.push({
						id: deliveryFrequencyId,
						count: frequencies.doc_count,
						value: deliveryFrequencyDescription && deliveryFrequencyDescription[0] && deliveryFrequencyDescription[0].description ? deliveryFrequencyDescription[0].description : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
						checked: false
					});
					//  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
				} else if (frequencies.deliveryFrequencies.hits.hits[0]._source.deliveryFrequencies !== 'DEFAULT_NULL') {
					deliveryFrequencyArray.push({
						id: deliveryFrequencyId,
						count: frequencies.doc_count,
						value: deliveryFrequencyDescription && deliveryFrequencyDescription[0] && deliveryFrequencyDescription[0].description ? deliveryFrequencyDescription[0].description : '',
						checked: false
					});
				}

			}
			// console.log('Dformat ',deliveryFormatsArrayForFilter);
		});
		
		/*------------------Filter List and count - Ranjith - End-----------------------*/
		this.filters.push({
			filterName: 'Related Data Category',
			filterList: relatedDataCategoryFilterArray,
			filterType: 'checkbox',
			filterTerm: 'categories.id',
			expanded: true,
			order: 1,
			mouseoverExplanation: 'All the data categories this provider collects/provides/analyzes.'
		});
		if (this.isAdmin) {
			this.filters.push({
				filterName: 'Profile Status',
				filterList: profileStatusFilterArray,
				filterType: 'checkbox',
				filterTerm: 'marketplaceStatus.id',
				expanded: true,
				order: 2,
				mouseoverExplanation: 'The main type of data or information that the provider collects/provides/analyzes.'
			});
		}
		if (!this.isAdmin) {
			this.filters.push({
				filterName: 'Status',
				filterTerm: 'marketplaceStatus',
				filterType: 'radio',
				filterList: [
					{
						value: 'Followed',
						checked: false,
						show: true
					},
					{
						value: 'Unlocked',
						checked: false,
						show: true
					}
				],
				expanded: true,
				order: 3
			});
		}

		if (this.isAdmin) {
			this.filters.push({
				filterName: 'Research Status',
				filterList: researchStatusFilterArray,
				filterType: 'checkbox',
				filterTerm: 'ieiStatus.id',
				expanded: false,
				order: 4,
				mouseoverExplanation: 'Tags used to keep track of providers for your own personal use (private to your account).'
			});
			this.filters.push({
				filterName: 'Priority',
				filterList: listPriorityFilterArray,
				filterType: 'checkbox',
				filterTerm: 'priority',
				expanded: false,
				order: 4,
				mouseoverExplanation: 'Tags used to keep track of providers for your own personal use (private to your account).'
			});
			this.filters.push({
				filterName: 'Status',
				filterTerm: 'marketplaceStatus',
				filterType: 'radio',
				filterList: [
					{
						value: 'Unlocked',
						checked: false,
						show: true
					}
				],
				expanded: true,
				order: 3
			});
		}

		this.filters.push({
			filterName: 'Personal Tags',
			filterList: personalTagFilterArray,
			filterType: 'horizontal-checkbox',
			filterTerm: 'providerTags.id',
			expanded: false,
			order: 5,
			mouseoverExplanation: 'Tags used to keep track of providers for your own personal use (private to your account).'
		});
		this.filters.push({
			filterName: 'Profile Completeness',
			filterTerm: 'completeness',
			filterType: 'rangeFromTo',
			filterNgModel: [0, 100],
			filterID: 'completeness',
			minRange: 0,
			maxRange: 100,
			minRangeInit: 0,
			maxRangeInit: 100,
			rangeStep: 0.1,
			expanded: false,
			order: 6,
			mouseoverExplanation: 'Percent of the data providers profile that has been filled in.'
		});

		this.filters.push({
			filterName: 'Main Data Category',
			filterList: mainDataCategoryFilterArray,
			filterType: 'checkbox',
			filterTerm: 'mainDataCategory.id',
			expanded: false,
			order: 7,
			mouseoverExplanation: 'The main type of data or information that the provider collects/provides/analyzes.'
		});

		this.filters.push({
			filterName: 'Main Data Industry',
			filterList: mainDataIndustryArray,
			filterType: 'checkbox',
			filterTerm: 'mainDataIndustry.id',
			expanded: false,
			order: 8,
			mouseoverExplanation: 'Primary industry that the provider operates in and provides data to.'
		});

		this.filters.push({
			filterName: 'Related Data Industry',
			filterList: relatedDataIndustryFilterArray,
			filterTerm: 'industries.id',
			filterType: 'checkbox',
			expanded: false,
			order: 9,
			mouseoverExplanation: 'All the industries that the provider operates in and provides data to.'
		});

		this.filters.push({
			filterName: 'Data Features',
			filterList: dataFeaturesFilterArray,
			filterTerm: 'features.id',
			filterType: 'checkbox',
			expanded: false,
			order: 10,
			mouseoverExplanation: 'The specific data or information being collected or organized by the provider.'
		});

		this.filters.push({
			filterName: 'Geographical Focus',
			filterList: geographicFocusFilterArray,
			filterTerm: 'geographicalFoci.id',
			filterType: 'checkbox',
			expanded: false,
			order: 11,
			mouseoverExplanation: 'The geographies that the data provider focuses on collecting/providing/analyzing data about.'
		});

		this.filters.push({
			filterName: 'Main Asset Class',
			filterList: listAssetMainClassFilterArray,
			filterTerm: 'mainAssetClass',
			filterType: 'checkbox',
			expanded: false,
			order: 12,
			mouseoverExplanation: 'The financial security asset class that is the main focus or most relevant for the data provider.'
		});

		this.filters.push({
			filterName: 'Related Asset Class',
			filterList: relatedAssetClassFilterArray,
			filterTerm: 'assetClasses.id',
			filterType: 'checkbox',
			expanded: false,
			order: 13,
			mouseoverExplanation: 'All of the financial security asset classes that the providers data can be used for or has any relevance towards.'
		});

		this.filters.push({
			filterName: 'Security Type',
			filterList: securityTypeFilterArray,
			filterTerm: 'securityTypes.id',
			filterType: 'checkbox',
			hidden: true,
			order: 14,
			mouseoverExplanation: 'All of the financial security types the providers data can be used for or has any relevance towards.'
		});
		this.filters.push({
			filterName: 'Related Sector',
			filterList: relatedSectorsArray,
			filterTerm: 'relevantSectors.id',
			filterType: 'checkbox',
			hidden: true,
			order: 15,
			mouseoverExplanation: 'All of the sectors the providers data could be used for or has any relevance towards.'
		});

		this.filters.push({
			filterName: 'Investor Type',
			filterList: investorTypeFilterArray,
			filterTerm: 'investorTypes.id',
			filterType: 'checkbox',
			hidden: true,
			order: 16,
			mouseoverExplanation: 'The types of investors the providers data would be useful for or would have the ability to use.'
		});

		this.filters.push({
			filterName: 'Data Source',
			filterList: dataSourceFilterArray,
			filterTerm: 'sources.id',
			filterType: 'checkbox',
			hidden: true,
			order: 17,
			mouseoverExplanation: 'The original (physical) source of the data being produced by this provider, not to be confused with external Provider Providers.'
		});

		this.filters.push({
			filterName: 'All Tags',
			filterList: dataProviderTag,
			filterTerm: 'providerTags.id',
			filterType: 'horizontal-checkbox',
			hidden: true,
			order: 18,
			mouseoverExplanation: 'All of the attributes/classifications for the provider, including some special circumstances.'
		});

		this.filters.push({
			filterName: 'Provider Type',
			filterTerm: 'providerType.id',
			filterType: 'horizontal-checkbox',
			filterList: this.providerTypes,
			hidden: true,
			order: 19,
			mouseoverExplanation: 'At what step in the data monetization process does the provider sit?' +
				' In other words, how does the provider handle data and what does it do with it?'
		});

		const currentYear = new Date().getFullYear();
		this.filters.push({
			filterName: 'Year Founded',
			filterTerm: 'ownerOrganization.yearFounded',
			filterType: 'rangeFromTo',
			filterNgModel: [1965, currentYear],
			filterID: 'ownerOrganization_yearFounded',
			hidden: true,
			minRange: 1965,
			maxRange: currentYear,
			minRangeInit: 1965,
			maxRangeInit: currentYear,
			rangeStep: 5,
			order: 20,
			mouseoverExplanation: 'Year the provider or its owner was founded.'
		});

		this.filters.push({
			filterName: 'Pricing Model',
			filterList: pricingModelArrayForFilter,
			filterTerm: 'dataProvidersPricingModels.id',
			filterType: 'checkbox',
			hidden: true,
			order: 21,
			mouseoverExplanation: 'The general model for how the provider prices its data products.'
		});
		this.filters.push({
			filterName: 'Free Trial Available',
			filterList: [{
				label: 'Free Trial',
				value: 'checked',
				checked: false
			}],
			filterTerm: 'freeTrialAvailable',
			filterType: 'slide-toggle',
			hidden: true,
			order: 22,
			mouseoverExplanation: 'Is a free trial available for the providers data products?'
		});

		this.filters.push({
			filterName: 'Willingness to Provide to Investors',
			filterList: willingnessInvetorsFilterArray,
			filterTerm: 'scoreInvestorsWillingness.id',
			filterType: 'checkbox',
			hidden: true,
			order: 23,
			mouseoverExplanation: 'How willing the provider is to provide or sell their data products to investors.'
		});

		this.filters.push({
			filterName: 'Uniqueness',
			filterList: uniquenessFilterArray,
			filterTerm: 'scoreUniqueness.id',
			filterType: 'checkbox',
			hidden: true,
			order: 24,
			mouseoverExplanation: 'How unique the providers data products are compared to traditional investment research techniques.'
		});

		this.filters.push({
			filterName: 'Value',
			filterList: valueFilterArray,
			filterTerm: 'scoreValue.id',
			filterType: 'checkbox',
			hidden: true,
			order: 25,
			mouseoverExplanation: 'How valuable the providers data products is compared to traditional investment research techniques.'
		});
		this.filters.push({
			filterName: 'Readiness',
			filterList: readinessFilterArray,
			filterTerm: 'scoreReadiness.id',
			filterType: 'checkbox',
			hidden: true,
			order: 26,
			mouseoverExplanation: 'How ready the providers data products are to be deployed within investment research.'
		});
		this.filters.push({
			filterName: 'Overall',
			filterList: overallFilterArray,
			filterTerm: 'scoreOverall.id',
			filterType: 'checkbox',
			hidden: true,
			order: 27,
			mouseoverExplanation: 'The overall importance of integrating the providers data products into an investment research process.'
		});
		this.filters.push({
			filterName: 'Delivery Method',
			filterList: deliveryMethodFilterArray,
			filterTerm: 'deliveryMethods.id',
			filterType: 'checkbox',
			hidden: true,
			order: 28,
			mouseoverExplanation: ''
		});
		this.filters.push({
			filterName: 'Delivery Format',
			filterList: deliveryFormatsArrayForFilter,
			filterTerm: 'deliveryFormats.id',
			filterType: 'checkbox',
			hidden: true,
			order: 29,
			mouseoverExplanation: ''
		});
		this.filters.push({
			filterName: 'Delivery Frequency',
			filterList: deliveryFrequencyArray,
			filterTerm: 'deliveryFrequencies.id',
			filterType: 'checkbox',
			hidden: true,
			order: 30,
			mouseoverExplanation: ''
		});
		this.filters.push({
			filterName: 'Date Added',
			filterTerm: 'createdDate',
			filterType: 'datepicker',
			filterNgModel: [minDates, currentDate],
			filterID: 'createdDate',
			hidden: true,
			minDate: minDates,
			maxDate: currentDate,
			minRange: '',
			maxRange: '',
			minRangeInit: '',
			maxRangeInit: '',
			order: 31,
			mouseoverExplanation: 'Date the data provider was discovered and added'
		});
		this.filters.push({
			filterName: 'Last Updated Date',
			filterTerm: 'updatedDate',
			filterType: 'datepicker',
			filterNgModel: [minUpdatedDate, currentUpdatedDate],
			filterID: 'updatedDate',
			hidden: true,
			minDate: minUpdatedDate,
			maxDate: currentUpdatedDate,
			minRange: '',
			maxRange: '',
			minRangeInit: '',
			maxRangeInit: '',
			order: 32,
			mouseoverExplanation: 'Date the data providers profile was last updated'
		});
	}
	if(!this.isAuthenticated())
	{
		this.filters.push({
			filterName: 'Related Data Category',
			filterList: [],
			filterType: 'checkbox',
			filterTerm: 'categories.id',
			expanded: true,
			order: 1,
			mouseoverExplanation: 'All the data categories this provider collects/provides/analyzes.'
		});
		if (this.isAdmin) {
			this.filters.push({
				filterName: 'Profile Status',
				filterList: [],
				filterType: 'checkbox',
				filterTerm: 'marketplaceStatus.id',
				expanded: true,
				order: 2,
				mouseoverExplanation: 'The main type of data or information that the provider collects/provides/analyzes.'
			});
		}
		if (!this.isAdmin) {
			this.filters.push({
				filterName: 'Status',
				filterTerm: 'marketplaceStatus',
				filterType: 'radio',
				filterList: [
					{
						value: 'Followed',
						checked: false,
						show: true
					},
					{
						value: 'Unlocked',
						checked: false,
						show: true
					}
				],
				expanded: true,
				order: 3
			});
		}

		if (this.isAdmin) {
			this.filters.push({
				filterName: 'Research Status',
				filterList: [],
				filterType: 'checkbox',
				filterTerm: 'ieiStatus.id',
				expanded: false,
				order: 4,
				mouseoverExplanation: 'Tags used to keep track of providers for your own personal use (private to your account).'
			});
			this.filters.push({
				filterName: 'Priority',
				filterList: [],
				filterType: 'checkbox',
				filterTerm: 'priority',
				expanded: false,
				order: 4,
				mouseoverExplanation: 'Tags used to keep track of providers for your own personal use (private to your account).'
			});
			this.filters.push({
				filterName: 'Status',
				filterTerm: 'marketplaceStatus',
				filterType: 'radio',
				filterList: [
					{
						value: 'Unlocked',
						checked: false,
						show: true
					}
				],
				expanded: true,
				order: 3
			});
		}

		this.filters.push({
			filterName: 'Personal Tags',
			filterList: [],
			filterType: 'horizontal-checkbox',
			filterTerm: 'providerTags.id',
			expanded: false,
			order: 5,
			mouseoverExplanation: 'Tags used to keep track of providers for your own personal use (private to your account).'
		});
		this.filters.push({
			filterName: 'Profile Completeness',
			filterTerm: 'completeness',
			filterType: 'rangeFromTo',
			filterNgModel: [0, 100],
			filterID: 'completeness',
			minRange: 0,
			maxRange: 100,
			minRangeInit: 0,
			maxRangeInit: 100,
			rangeStep: 0.1,
			expanded: false,
			order: 6,
			mouseoverExplanation: 'Percent of the data providers profile that has been filled in.'
		});

		this.filters.push({
			filterName: 'Main Data Category',
			filterList: [],
			filterType: 'checkbox',
			filterTerm: 'mainDataCategory.id',
			expanded: false,
			order: 7,
			mouseoverExplanation: 'The main type of data or information that the provider collects/provides/analyzes.'
		});

		this.filters.push({
			filterName: 'Main Data Industry',
			filterList: [],
			filterType: 'checkbox',
			filterTerm: 'mainDataIndustry.id',
			expanded: false,
			order: 8,
			mouseoverExplanation: 'Primary industry that the provider operates in and provides data to.'
		});

		this.filters.push({
			filterName: 'Related Data Industry',
			filterList: [],
			filterTerm: 'industries.id',
			filterType: 'checkbox',
			expanded: false,
			order: 9,
			mouseoverExplanation: 'All the industries that the provider operates in and provides data to.'
		});

		this.filters.push({
			filterName: 'Data Features',
			filterList: [],
			filterTerm: 'features.id',
			filterType: 'checkbox',
			expanded: false,
			order: 10,
			mouseoverExplanation: 'The specific data or information being collected or organized by the provider.'
		});

		this.filters.push({
			filterName: 'Geographical Focus',
			filterList: [],
			filterTerm: 'geographicalFoci.id',
			filterType: 'checkbox',
			expanded: false,
			order: 11,
			mouseoverExplanation: 'The geographies that the data provider focuses on collecting/providing/analyzing data about.'
		});

		this.filters.push({
			filterName: 'Main Asset Class',
			filterList: [],
			filterTerm: 'mainAssetClass',
			filterType: 'checkbox',
			expanded: false,
			order: 12,
			mouseoverExplanation: 'The financial security asset class that is the main focus or most relevant for the data provider.'
		});

		this.filters.push({
			filterName: 'Related Asset Class',
			filterList: [],
			filterTerm: 'assetClasses.id',
			filterType: 'checkbox',
			expanded: false,
			order: 13,
			mouseoverExplanation: 'All of the financial security asset classes that the providers data can be used for or has any relevance towards.'
		});

		this.filters.push({
			filterName: 'Security Type',
			filterList: [],
			filterTerm: 'securityTypes.id',
			filterType: 'checkbox',
			hidden: true,
			order: 14,
			mouseoverExplanation: 'All of the financial security types the providers data can be used for or has any relevance towards.'
		});
		this.filters.push({
			filterName: 'Related Sector',
			filterList: [],
			filterTerm: 'relevantSectors.id',
			filterType: 'checkbox',
			hidden: true,
			order: 15,
			mouseoverExplanation: 'All of the sectors the providers data could be used for or has any relevance towards.'
		});

		this.filters.push({
			filterName: 'Investor Type',
			filterList: [],
			filterTerm: 'investorTypes.id',
			filterType: 'checkbox',
			hidden: true,
			order: 16,
			mouseoverExplanation: 'The types of investors the providers data would be useful for or would have the ability to use.'
		});

		this.filters.push({
			filterName: 'Data Source',
			filterList: [],
			filterTerm: 'sources.id',
			filterType: 'checkbox',
			hidden: true,
			order: 17,
			mouseoverExplanation: 'The original (physical) source of the data being produced by this provider, not to be confused with external Provider Providers.'
		});

		this.filters.push({
			filterName: 'All Tags',
			filterList: [],
			filterTerm: 'providerTags.id',
			filterType: 'horizontal-checkbox',
			hidden: true,
			order: 18,
			mouseoverExplanation: 'All of the attributes/classifications for the provider, including some special circumstances.'
		});

		this.filters.push({
			filterName: 'Provider Type',
			filterTerm: 'providerType.id',
			filterType: 'horizontal-checkbox',
			filterList: this.providerTypes,
			hidden: true,
			order: 19,
			mouseoverExplanation: 'At what step in the data monetization process does the provider sit?' +
				' In other words, how does the provider handle data and what does it do with it?'
		});

		const currentYear = new Date().getFullYear();
		this.filters.push({
			filterName: 'Year Founded',
			filterTerm: 'ownerOrganization.yearFounded',
			filterType: 'rangeFromTo',
			filterNgModel: [1965, currentYear],
			filterID: 'ownerOrganization_yearFounded',
			hidden: true,
			minRange: 1965,
			maxRange: currentYear,
			minRangeInit: 1965,
			maxRangeInit: currentYear,
			rangeStep: 5,
			order: 20,
			mouseoverExplanation: 'Year the provider or its owner was founded.'
		});

		this.filters.push({
			filterName: 'Pricing Model',
			filterList: [],
			filterTerm: 'dataProvidersPricingModels.id',
			filterType: 'checkbox',
			hidden: true,
			order: 21,
			mouseoverExplanation: 'The general model for how the provider prices its data products.'
		});
		this.filters.push({
			filterName: 'Free Trial Available',
			filterList: [{
				label: 'Free Trial',
				value: 'checked',
				checked: false
			}],
			filterTerm: 'freeTrialAvailable',
			filterType: 'slide-toggle',
			hidden: true,
			order: 22,
			mouseoverExplanation: 'Is a free trial available for the providers data products?'
		});

		this.filters.push({
			filterName: 'Willingness to Provide to Investors',
			filterList: [],
			filterTerm: 'scoreInvestorsWillingness.id',
			filterType: 'checkbox',
			hidden: true,
			order: 23,
			mouseoverExplanation: 'How willing the provider is to provide or sell their data products to investors.'
		});

		this.filters.push({
			filterName: 'Uniqueness',
			filterList: [],
			filterTerm: 'scoreUniqueness.id',
			filterType: 'checkbox',
			hidden: true,
			order: 24,
			mouseoverExplanation: 'How unique the providers data products are compared to traditional investment research techniques.'
		});

		this.filters.push({
			filterName: 'Value',
			filterList: [],
			filterTerm: 'scoreValue.id',
			filterType: 'checkbox',
			hidden: true,
			order: 25,
			mouseoverExplanation: 'How valuable the providers data products is compared to traditional investment research techniques.'
		});
		this.filters.push({
			filterName: 'Readiness',
			filterList: [],
			filterTerm: 'scoreReadiness.id',
			filterType: 'checkbox',
			hidden: true,
			order: 26,
			mouseoverExplanation: 'How ready the providers data products are to be deployed within investment research.'
		});
		this.filters.push({
			filterName: 'Overall',
			filterList: [],
			filterTerm: 'scoreOverall.id',
			filterType: 'checkbox',
			hidden: true,
			order: 27,
			mouseoverExplanation: 'The overall importance of integrating the providers data products into an investment research process.'
		});
		this.filters.push({
			filterName: 'Delivery Method',
			filterList: [],
			filterTerm: 'deliveryMethods.id',
			filterType: 'checkbox',
			hidden: true,
			order: 28,
			mouseoverExplanation: ''
		});
		this.filters.push({
			filterName: 'Delivery Format',
			filterList: [],
			filterTerm: 'deliveryFormats.id',
			filterType: 'checkbox',
			hidden: true,
			order: 29,
			mouseoverExplanation: ''
		});
		this.filters.push({
			filterName: 'Delivery Frequency',
			filterList: [],
			filterTerm: 'deliveryFrequencies.id',
			filterType: 'checkbox',
			hidden: true,
			order: 30,
			mouseoverExplanation: ''
		});
		this.filters.push({
			filterName: 'Date Added',
			filterTerm: 'createdDate',
			filterType: 'datepicker',
			filterNgModel: [minDates, currentDate],
			filterID: 'createdDate',
			hidden: true,
			minDate: minDates,
			maxDate: currentDate,
			minRange: '',
			maxRange: '',
			minRangeInit: '',
			maxRangeInit: '',
			order: 31,
			mouseoverExplanation: 'Date the data provider was discovered and added'
		});
		this.filters.push({
			filterName: 'Last Updated Date',
			filterTerm: 'updatedDate',
			filterType: 'datepicker',
			filterNgModel: [minUpdatedDate, currentUpdatedDate],
			filterID: 'updatedDate',
			hidden: true,
			minDate: minUpdatedDate,
			maxDate: currentUpdatedDate,
			minRange: '',
			maxRange: '',
			minRangeInit: '',
			maxRangeInit: '',
			order: 32,
			mouseoverExplanation: 'Date the data providers profile was last updated'
		});
		this.loaderService.display(false);
	}

		/* this.filters.push({
			filterName: 'Corona',
			filterList: dataProviderTag,
			filterTerm: 'providerTags.id',
			filterType: 'checkbox',
			hidden: true,
			order: 33,
			mouseoverExplanation: 'All of the attributes/classifications for the provider, including some special circumstances.'
		}); */
		console.log('searchvalue',localStorage.getItem('search'));

		if (localStorage.getItem('search')) {
			console.log('searchvaluechecked',localStorage.getItem('search'));
			const event = localStorage.getItem('search');
			if (event && event === '<strong class="warn-text">At least one provider matches this query but is locked for you. Please contact Amass for access.</strong>') {
				this.currentSearchQuery = '';
			}
			else {
				console.log(event);
				this.currentSearchQuery = event;
				this.search(this.currentSearchQuery);
			}
		}
		if (localStorage.getItem('searchheader')) {
			this.searchHeader = localStorage.getItem('searchheader')
			this.searchCommon(this.searchHeader);
		}
	}
	openCommentsWindow(buttonType: string, providerId, privacy) {
		this.commentProviderId = providerId;
		this.filterStatus = privacy;
		this.commentsButtonType = buttonType;
		this.comments = [];
		this.displayCommentsSidebar = !this.displayCommentsSidebar;
		if (this.filterStatus === 'All') {
			this.filterCode = 1;
			this.onPostPrivacy = 1;
		}
		/* this.dataProviderService.getCommentsByID(this.commentProviderId, 1).subscribe(comments => {
			console.log(comments);
			this.comments = comments;
		}, error => {
			this.comments = [];
		}); */
		this.loadComments(this.commentProviderId);
	}
   sortingArrayMethod()
   {
	if(this.coronaActive)
	{
		this.coronaSorting = {
			key: 'createdDate',
			label: 'createdDate',
			order: 'desc',
			
		}
	}
	this.sortingArray = [
		{
			key: 'sortorder',
			label: 'Name',
			order: '',
			userLevel: 'unrestricted',
			DropDown: 'Name',
			mouseoverExplanation: 'Name of the data provider.'
		},
		{
			key: 'mainDataIndustry.sortorder',
			label: 'Industry',
			order: '',
			userLevel: 'unrestricted',
			DropDown: 'Industry',
			mouseoverExplanation: 'Main data industry of the data provider.'
		},
		{
			key: 'mainDataCategory.sortorder',
			label: 'Category',
			order: '',
			userLevel: 'unrestricted',
			DropDown: 'Category',
			mouseoverExplanation: 'Main data category of the data provider.'
		},
		{
			key: 'createdDate',
			label: 'Added',
			order:  this.coronaActive ? 'desc' : '',
			userLevel: 'unrestricted',
			DropDown: 'Added',
			mouseoverExplanation: 'Date the data provider was discovered and added.'
		},
		{
			key: 'updatedDate',
			label: 'Updated',
			order: this.userLevel || this.coronaActive ? '' : 'desc',
			userLevel: 'unrestricted',
			DropDown: 'Updated',
			mouseoverExplanation: 'Date the data providers profile was last updated.'
		},
		{
			key: 'completeness',
			label: 'Completeness',
			order: '',
			userLevel: 'unrestricted',
			DropDown: 'Completeness',
			mouseoverExplanation: 'Percent of the data providers profile that has been filled in.'
		},
		{
			key: 'recommended',
			label: 'Recommended',
			type: 'recommended',
			DropDown: 'recommended',
			recommendedSorting: [
				{
					key: 'scoreOverall.sortorder',
					label: 'Overall Score',
					order: 'asc',
					DropDown: 'Overall',
					type: 'recommended'
				},
				{
					key: 'scoreInvestorsWillingness.sortorder',
					label: 'Willingness to Provide to Investors Score',
					order: 'asc',
					DropDown: 'Willingness',
					type: 'recommended'
				},
				{
					key: 'completeness',
					label: 'Completeness',
					order: this.userLevel ? 'desc' : '',
					DropDown: 'Completeness',
					type: 'recommended'
				}
			],
			order: this.userLevel && !this.coronaActive ? 'desc' : '',
			userLevel: this.userLevel ? 'unrestricted' : 'restricted',
		},
		{
			key: 'Scores',
			label: 'Scores',
			order: 'desc',
			DropDown: 'Scores',
			userLevel: this.userLevel ? 'unrestricted' : 'restricted',
		}
	];
   }



	isAuthenticated() {
		return this.principal.isAuthenticated();
	}
	reloadComments(event) {
		const privacyValue = event;
		console.log(privacyValue);
		console.log(this.commentProviderId);
		if (privacyValue === 'Private') {
			this.onPostPrivacy = 581;
			console.log(this.onPostPrivacy);
			this.loadComments(this.commentProviderId);
		} else if (privacyValue === 'Public') {
			this.onPostPrivacy = 582;
			console.log(this.onPostPrivacy);
			this.loadComments(this.commentProviderId);
		} else {
			this.onPostPrivacy = 1;
			this.loadComments(this.commentProviderId);
		}
		/* if (this.onPostPrivacy) {
			this.loadComments();
		} */
	}
	/*  reloadComments(event) {
	   console.log(event);
	   console.log(this.commentProviderId);
	   if (event) {
		   this.loadComments(this.commentProviderId);
	   }
   } */
	toggle(id: number) {
		/* console.log('button called',id);
		if(this.menuClicked)
		{
			this.menuClicked = false;
		}
		else
		{ */
		this.menuClicked = true;

		/* 	} */
		this.dataProviderId = id;
		//console.log('menuclicked',this.menuClicked);
		//console.log('dataproviderid',this.dataProviderId);
	}
	loadComments(providerID) {
		console.log('postprivacy', this.onPostPrivacy);
		this.dataProviderService.getCommentsByID(providerID, this.onPostPrivacy).subscribe(comments => {
			this.comments = comments;
		}, error => {
			this.comments = [];
		});
	}
	openNotesComments(buttonType: string, providerId, privacy) {
		this.filterStatus = privacy;
		this.commentsButtonType = buttonType;
		this.commentProviderId = providerId;
		this.comments = [];
		this.displayCommentsSidebar = !this.displayCommentsSidebar;
		if (this.filterStatus === 'private') {
			this.filterCode = 581;
		} else if (this.filterStatus === 'public') {
			this.filterCode = 582;
		} else {
			this.filterCode = 1;
		}
		this.dataProviderService.getCommentsByID(this.commentProviderId, this.filterCode).subscribe(comments => {
			const commentsList = comments;
			if (privacy === 'private') {
				for (let i = 0; i < commentsList.length; i++) {
					if (commentsList[i].privacy === 'private') {
						//       console.log('...Private...', commentsList[i].privacy);
						this.comments = comments;
					}
				}
			} else if (privacy === 'public') {
				for (let i = 0; i < commentsList.length; i++) {
					if (commentsList[i].privacy === 'public') {
						//       console.log('...Public...', commentsList[i].privacy);
						this.comments = comments;
					}
				}
			}
		}, error => {
			this.comments = [];
		});
	}
	currentSearchQueryValue(event) {
		console.log('evcent', event);
		this.currentSearchQuery = event;
	}
	/* blockAutocomplete(event) {
		
	} */
	getTagList(providerID?: number) {
		this.loaderActive = false;
		this.tagDropDownValues = [];
		this.providersId = providerID;
		this.filtersService.getDataProviderTagUrl().subscribe(data => {
			this.tagDropDownValues = [];
			const listItems = data;
			for (let i = 0; i < listItems.length; i++) {
				const dataList = listItems[i];
				this.tagCheckItem = false;
				if (dataList.tagProviders && dataList.tagProviders !== 'null') {
					const providerTag = dataList.tagProviders.filter((item, index) => {
						this.tagProviderId = parseInt(item.id);
						//   console.log('new tag...', this.tagProviderId);
						if (this.providersId === this.tagProviderId) {
							this.tagCheckItem = true;
							//       console.log('new tag...', this.tagCheckItem);
						}
						return this.providersId === this.tagProviderId;
					});
				}
				this.tagDropDownValues.push({
					tagName: dataList.providerTag,
					'providerTag': this.tagCheckItem,
					privacy: dataList.privacy,
					id: dataList.id
				});
				//    console.log(this.tagDropDownValues);
			}
		});
	}
	resource() {
		this.dataresourceTabService.sendMessage('resources');
	}
	resourcesTag(event) {
		// console.log(event);
		localStorage.setItem('resources', 'resources');
		this.router.navigate(['../providers', event])
	}
	partnersTag(event) {
		//console.log(event);
		localStorage.setItem('partners', 'partners');
		this.router.navigate(['../providers', event])
	}
	onKeydownTag(values) {
		this.customTags = false;
		const providerId = values.providerID;
		//  console.log(providerId);
		//  console.log('new tag...', values);
		this.dataProviderService.tagList(values).subscribe(data => {
			const tags = data;
			this.getTagList(providerId);
			//    console.log('providerId...', providerId);
			this.customTags = false;
			/* this.dropDownAutoClose = true;
			console.log(this.dropDownAutoClose); */
		});
		this.newTagValue = '';
	}
	providerNewTag(values, index) {
		//  console.log('...tagmapping....', values);
		if (!this.tagDropDownValues[index].providerTag) {
			const providerID = values.providerId;
			this.loaderActive = true;
			this.dataProviderService.newTagList(values).subscribe(data => {
				this.tagCheckItem = true;
				this.tagDropDownValues[index].providerTag = true;
				this.loaderActive = false;
				if (this.tagDropDownValues[index].providerTag) {
					this.loaderActive = false;
				}
				// this.getTagList(providerID);
			});
		}
		else if (this.tagDropDownValues[index].providerTag) {
			this.loaderActive = true;
			this.dataProviderService.removeTagList(values).subscribe(data => {
				this.tagCheckItem = false;
				this.tagDropDownValues[index].providerTag = false;
				this.loaderActive = false;
			});
		}
		// this.dropDownAutoClose = true;
	}
	providerActionTag(values) {
		if (this.tagInterstedArray.indexOf(values.providerId) !== -1) {
			this.loaderTag = true;
			this.loaderProviderId = values.providerId;
			this.dataProviderService.removeTagList(values).subscribe(data => {
				const index = this.tagInterstedArray.indexOf(values.providerId);
				this.tagInterstedArray.splice(index, 1);
				this.loaderTag = false;
			});
		}
		else {
			this.loaderTag = true;
			this.loaderProviderId = values.providerId;
			this.dataProviderService.newTagList(values).subscribe(data => {
				this.tagInterstedArray.push(values.providerId);
				this.loaderTag = false;
			});
			this.dataProviderService.followActionTag(this.loaderProviderId).subscribe(data => {
			});
		}

	}
	providerActionTagnotIntersted(values) {
		if (this.tagNotInterstedArray.indexOf(values.providerId) !== -1) {
			this.loaderTag = true;
			this.loaderProviderId = values.providerId;
			this.dataProviderService.removeTagList(values).subscribe(data => {
				const index = this.tagNotInterstedArray.indexOf(values.providerId);
				this.tagNotInterstedArray.splice(index, 1);
				this.loaderTag = false;
			});

		}
		else {
			this.loaderTag = true;
			this.loaderProviderId = values.providerId;
			this.dataProviderService.newTagList(values).subscribe(data => {
				this.tagNotInterstedArray.push(values.providerId);
				this.loaderTag = false;
			});
		}
	}
	providerActionTagInuse(values) {
		if (this.tagInUseArray.indexOf(values.providerId) !== -1) {
			this.loaderTag = true;
			this.loaderProviderId = values.providerId;
			this.dataProviderService.removeTagList(values).subscribe(data => {
				const index = this.tagInUseArray.indexOf(values.providerId);
				this.tagInUseArray.splice(index, 1);
				this.loaderTag = false;
			});
		}
		else {
			this.loaderTag = true;
			this.loaderProviderId = values.providerId;
			this.dataProviderService.newTagList(values).subscribe(data => {
				this.tagInUseArray.push(values.providerId);
				this.loaderTag = false;
			});
			this.dataProviderService.followActionTag(this.loaderProviderId).subscribe(data => {
			});
		}
	}
	providerActionTagTesting(values) {
		if (this.tagTestingArray.indexOf(values.providerId) !== -1) {
			this.loaderTag = true;
			this.loaderProviderId = values.providerId;
			this.dataProviderService.removeTagList(values).subscribe(data => {
				const index = this.tagTestingArray.indexOf(values.providerId);
				this.tagTestingArray.splice(index, 1);
				this.loaderTag = false;
			});
		}
		else {
			this.loaderTag = true;
			this.loaderProviderId = values.providerId;
			this.dataProviderService.newTagList(values).subscribe(data => {
				this.tagTestingArray.push(values.providerId);
				this.loaderTag = false;
			});
			this.dataProviderService.followActionTag(this.loaderProviderId).subscribe(data => {
			});
		}
	}


	providerProfileNav(events: any) {
		console.log(events);
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
		// if (!this.principal.isAuthenticated()) {
		// 	this.openSignUp();
		// }
	}

    moreDataCategories(events: any) {
        console.log(events);
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
		if (!this.principal.isAuthenticated()) {
			this.openSignUp();
		}
    }

	providerFollow(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	requestMoreInfo(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
	unlockProviders(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	providerListLink(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
		if (!this.principal.isAuthenticated()) {
			// this.router.navigate(['/providers']);
			this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
				width: '570px'
			});
		}
	}
	shreDropDown(events: any) {
		// console.log(events);
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	ngOnDestroy() {
		this.eventSubscriber.unsubscribe();
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
		this.eventManager.destroy(this.eventSubscriber);
	}

	trackId(index: number, item: DataProvider) {
		return item.id;
	}

	byteSize(field) {
		return this.dataUtils.byteSize(field);
	}

	openFile(contentType, field) {
		return this.dataUtils.openFile(contentType, field);
	}
	registerChangeInDataProviders() {
		this.eventSubscriber = this.eventManager.subscribe('dataProviderListModification', response => this.reset());
	}

	sort() {
		const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
		if (this.predicate !== 'id') {
			result.push('id');
		}
		return result;
	}

	sortBy(column: any) {
		if (this.isAuthenticated() || this.searchActivated) {
			console.log('sortby',column);
			if (localStorage.getItem('search')) {
				column = 'none';
			}
			if (localStorage.getItem('searchheader')) {
				column = 'none';
			}
			this.dataProviders = [];
			this.links = {
				last: 0
			};
			this.page = 0;
			this.itemsFrom = 0;
			if (!column) {
				return this.clear();
			}
			const index = column === 'none' ? -1 : this.sortingArray.indexOf(column);
			this.sortedIndex = index;
			for (let i = 0; i < this.sortingArray.length; i++) {
				if (i === index) {
					this.sortingArray[index].order = (column.order === 'desc') ? 'asc' : 'desc';
				} else {
					this.sortingArray[i].order = '';
				}
			}

			if (column.key === 'recommended') {
				for (let i = 0; i < column.recommendedSorting.length; i++) {
					if (column.recommendedSorting[i].key !== 'scoreInvestorsWillingness.sortorder' && column.recommendedSorting[i].key !== 'scoreOverall.sortorder') {
						column.recommendedSorting[i].order = column.order;
					}
				}
			}
			// if(column.key === 'createdDate')
			// {
			// 	console.log('created Date');
			// 	for(let i =0; i<column.recommendSorting.length; i++)
			// 	{
			// 		column.recommendedSorting[i].key = column.key;
			// 		column.recommendedSorting[i].order = column.order;
			// 	}
			// }
			if (!this.corona) {
				this.currentSorting = column !== 'none' ? column : '';
				this.coronaSorting = column !== 'none' ? column : '';

			}
			if (column === 'none') {
				this.currentSorting = '';
				return;
			}
			if (!this.corona && !this.myMatches) {
				this.loadAll();
			}
			else if (this.corona) {
				this.coronaSorting = column !== 'none' ? column : '';
				this.currentSorting = column !== 'none' ? column : '';
				this.coronaActive=false;
				this.loadAllCorona();
			}
			else if (this.myMatches) {
				this.checkMymatches();
			}
		}
		else {
			this.openSignUp();
		}
	}

	onFilter(options: any) {
		if (!options.query || options.query === 'clearFilter') {
			this.currentFilter = '';
			this.onClearFilter();
		} else {
			this.currentFilter = options.query ? options.query : '';
		}
		if (this.currentSearch) {
			this.clear();
		}
		this.statusFilter = options.statusFilter ? options.statusFilter : '';
		this.dataProviders = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.itemsFrom = 0;
		this.reverse = false;
		if (this.myMatches) {
			this.myMatches = false;
		}
		if (this.corona) {
			this.corona = false;
		}
		if (this.corona && !this.currentFilter) {
			this.loadAllCorona();
		}
		else if (this.myMatches && !this.currentFilter) {
			this.checkMymatches();
		}
		else {
			this.loadAll();
		}
	}
	onFilterCorona(options: any) {
		if (!options.query || options.query === 'clearFilter') {
			//this.currentFilter = '';
			this.onClearFilterCorona();
		} else {
			console.log('options',options,options.query);
			this.currentCoronaFilter = options.query ? options.query : '';

		}
		this.statusFilter = options.statusFilter ? options.statusFilter : '';
		this.dataProviders = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.itemsFrom = 0;
		this.reverse = false;
		if (this.corona) {
			this.loadAllCorona();
		}
		else if (!this.corona && this.myMatches) {
			this.checkMymatches();
		}
		else {
			this.loadAll();
		}
	}

	onClearFilter() {
		//	console.log('onClearFilter...');
		this.paramFilter = '';
		this.paramID = '';
		this.currentFilter = '';
		this.statusFilter = '';
		this.dataProviders = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.itemsFrom = 0;
		this.reverse = false;
		/* this.corona=false;*/
		this.selectFilters = [];
		this.selectFilterData = [];
		if (!this.myMatches) {
			this.loadAll();
			this.router.navigate(['/providers']);
		}
		return;
	}
	onClearFilterCorona() {
		//	console.log('onClearFilter...');
		this.paramFilter = '';
		this.paramID = '';
		//this.currentFilter = '';
		this.currentCoronaFilter = '';
		this.statusFilter = '';
		this.dataProviders = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.itemsFrom = 0;
		this.reverse = false;
		this.corona = false;
		this.selectFilters = [];
		this.selectFilterData = [];
		this.coronaActive=false;
        this.sortingArrayMethod();
		if (this.myMatches) {
			this.checkMymatches();
			this.router.navigate(['/providers']);

		}
		else {
			this.loadAll();
			this.router.navigate(['/providers']);
		}
		return;
	}
	onClearFilterMatches() {
		this.paramFilter = '';
		this.paramID = '';
		//this.currentFilter = '';
		//this.currentCoronaFilter = '';
		//this.statusFilter = '';
		this.dataProviders = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.itemsFrom = 0;
		this.reverse = false;
		//this.corona=false;
		//this.selectFilters = [];
		//this.selectFilterData =[];
		this.myMatches = false;
		this.myMatchesActivated = false;
		if (this.corona) {
			//console.log('3');
			this.loadAllCorona();
		}
		else {
			this.loadAll();
			this.router.navigate(['/providers']);
		}
		return;
	}

	switchViewType(type: string) {
		switch (type) {
			case 'list':
				this.isList = true;
				break;
			case 'grid':
				this.isList = false;
				break;
			default:
				this.isList = true;
				break;
		}
	}

	followAction(providerID: number) {
		this.dataProviderService.followAction(providerID).subscribe();
	}

	expandAction(providerID: number) {
		this.dataProviderService.expandAction(providerID).subscribe();
	}

	unlockAction(providerID: number) {
		this.dataProviderService.unlockAction(providerID).subscribe();
	}

	private onError(error) {
		this.loader = false;
		this.loaderInfinite = false;
	}

	private onSuccessFilter(data, headers) {
		this.loader = false;
		this.loaderInfinite = false;
		this.totalItems = data['hits'] ? data['hits'].total : 0;
		this.links.last = Math.ceil(this.totalItems / 20);
		if (!this.page) {
			this.dataProviders = [];
		}
		if (data['hits'] && data['hits'].hits) {
			for (let i = 0; i < data['hits'].hits.length; i++) {
				const providerObj = data['hits'].hits[i]._source;
				//	console.log('HashTag', providerObj);
				const hashTagText = (providerObj.mainDataIndustry && providerObj.mainDataIndustry.hashtag) ? providerObj.mainDataIndustry.hashtag : '';
				const categoryHashTagText = (providerObj.mainDataCategory && providerObj.mainDataCategory.hashtag) ? providerObj.mainDataCategory.hashtag : '';
				//	console.log('HashTag 1', categoryHashTagText);
				const providerUrl = window.location.origin + '/#/providers/' + providerObj.recordID;
				const categoryText = (providerObj.mainDataCategory &&
					!providerObj.mainDataCategory.locked && providerObj.mainDataCategory.datacategory !== 'DATA_CATEGORY_NULL') ? providerObj.mainDataCategory.datacategory : '';
				const industryText = (providerObj.mainDataIndustry && !providerObj.mainDataIndustry.locked &&
					providerObj.mainDataIndustry.dataIndustry !== 'DATA_INDUSTRY_NULL') ? providerObj.mainDataIndustry.dataIndustry : '';
				if (this.isAuthenticated()) {
					const providerNewObj = Object.assign({
						emailText: 'Hi,\n I\'d like to share this interesting ' + providerUrl + ' data profile for ' + (!providerObj.locked ? providerObj.providerName : '') + ' within the ' + categoryText + ' Data Category I found on the Amass Insights Platform.\n\n' + 'Regards,\n ' + this.currentAccount.firstName + ' ' + this.currentAccount.lastName + '\n\n\n P.S. If you\'re not already an Insights user, https://amassinsights.com?signup=true, in order to view this data profile. Subsequently, you\'ll receive instant access to Alt-Data news, events, though-leadership, and new, interesting data providers entering our ecosystem.',
						linkedInText: 'Check out this interesting ' + categoryHashTagText + ' data profile I found via the Amass Insights Platform: ' + providerUrl + (categoryHashTagText ? categoryHashTagText : '') + ' #AmassInsights #AlternativeData ' + (hashTagText ? hashTagText : ''),
						twitterText: 'Check out this interesting ' + categoryHashTagText + ' data profile I found via the Amass Insights Platform: ' + providerUrl + ' ' + ' ' + (categoryHashTagText ? categoryHashTagText : '') + ' #AmassInsights #AlternativeData ' + (hashTagText ? hashTagText : ''),
						url: providerUrl
					}, providerObj);
					this.dataProviders.push(providerNewObj);
				}
				else {
					const providerNewObj = Object.assign({
					}, providerObj);
					this.dataProviders.push(providerNewObj);
				}
				// console.log('follow....', this.dataProviders);
				this.completenessTooltip.push(data['hits'].hits[i]._source.completeness + '% ' + 'percent of this data providers profile that has been filled in');
			}
		}
		if (this.totalItems === 0 && this.currentSearchQuery && !this.reSubmitValues) {
			this.reSubmitValues = true;
			setTimeout(() => {
				this.search(this.currentSearchQuery);
			}, 3000);
		} else {
			this.reSubmitValues = false;
		}
	}
	onFilterCheck(event) {
		if (!this.isAuthenticated()) {
			if (event) {
				this.router.navigate(['/unproviders']);
				this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
					width: '570px'
				});
			}
		}
	}

	onFilterChanges(event) {
		if (this.isAuthenticated()) {
			if (event === 'clearFilter') {
				this.onFilter({
					query: 'clearFilter'
				});
				return;
			}
			if (event && event.selectedFilterData && event.selectedFilterData.length) {
				const requestBody = bob.requestBodySearch();
				const allFilterQuery = bob.boolQuery();
				const filters = event.selectedFilterData;
				const filtersLength = filters.length;
				for (let i = 0; i < filtersLength; i++) {
					const term = filters[i].queryTerm;
					const filter = filters[i].filters;
					// console.log(filter);
					if (filters[i].filters && filter.length) {
						if ((filters[i].queryType === 'must' || filters[i].queryType === 'should') && term === 'marketplaceStatus') {
							this.statusFilter = '';
							if (filter === 'Followed' || filter === 'Unlocked') {
								this.statusFilter = filter;
							} else if (filter === 'Complete') {
								allFilterQuery.should(bob.matchQuery(term, 'Expanded'));
								allFilterQuery.should(bob.matchQuery(term, 'Detailed'));
								allFilterQuery.should(bob.matchQuery(term, 'Followed'));
								allFilterQuery.should(bob.matchQuery(term, 'Unlocked'));
								this.statusFilter = '';
							} else if (filter !== 'Followed' && filter !== 'Unlocked') {
								allFilterQuery.should(bob.matchQuery(term, filter));
							}
						} else if (filters[i].queryType === 'range' || filters[i].queryType === 'rangeFromTo' || filters[i].queryType === 'datepicker') {
							//console.log(filters[i].queryType, typeof filter);
							if (typeof filter === 'string') {
								if (filters[i].queryType === 'datepicker') {
									allFilterQuery.must(bob.rangeQuery(term).from(this.datePipe.transform(filter, 'yyyy-MM-dd', 'EST')));
								} else {
									allFilterQuery.must(bob.rangeQuery(term).from(filter));
								}
							} else {
								allFilterQuery.must(bob.rangeQuery(term).from(filter[0][0]).to(filter[0][1]));
							}
						} else if (filters[i].queryType === 'rangeBetween') {
							allFilterQuery.must(bob.rangeQuery(term).gte(filter[0][0]).lte(filter[0][1]));
						} else {
							allFilterQuery.must(bob.termsQuery(term, filter));
						}
					}
				}

				requestBody.query(allFilterQuery);

				this.onFilter({
					query: requestBody.toJSON(),
					'statusFilter': this.statusFilter
				});
			}
		}
		else {
			this.activatedRoute.queryParams.subscribe(params => {
				if (params && params['utm_medium'] !== 'email' && !this.isAuthenticated()) {
					this.router.navigate(['/unproviders']);
					this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
						width: '570px'
					});

				} else if (params && params['utm_medium'] === 'email' && !this.isAuthenticated()) {
					this.stateService.storeUrl(this.router.url);
					this.router.navigate(['accessdenied']);
					this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
						width: '570px'
					});
				}
			});
		}

		return;
	}

	openContactDialog(dialogTitle: string, dialogSubTitle: string, dialogType: string, providerID: number, categoryID?: number, index?: any, requestType?: number) {
		if (this.isAuthenticated()) {
			const dialogRef = this.dialog.open(ContactDialogComponent, {
				width: '400px'
			});
			dialogRef.componentInstance.providerID = providerID ? providerID : null;
			dialogRef.componentInstance.categoryID = categoryID ? categoryID : null;
			dialogRef.componentInstance.dialogTitle = dialogTitle ? dialogTitle : 'Contact Us';
			dialogRef.componentInstance.dialogSubTitle = dialogSubTitle ? dialogSubTitle : null;
			dialogRef.componentInstance.dialogType = dialogType ? dialogType : 'contactUs';
			dialogRef.componentInstance.requestType = requestType ? requestType : null;

			dialogRef.componentInstance.onButtonAction.subscribe((action: any) => {
				if (action === 'success') {
					if (!this.dataProviders[index]['userProviderStatus']) {
						this.dataProviders[index]['userProviderStatus'] = [];
					}
					switch (dialogType) {
						case 'unlockProvider':
							this.dataProviders[index]['userProviderStatus'].push(262);
							break;
						case 'moreInformation':
							this.dataProviders[index]['userProviderStatus'].push(2086);
							break;
						case 'requestDirectContact':
							this.dataProviders[index]['userProviderStatus'].push(566);
							break;
						case 'requestDataSample':
							this.dataProviders[index]['userProviderStatus'].push(565);
							break;
					}

				}
			});
		}
		else {
			this.openSignUp();
		}
	}
	openContactDialogSuggestion(dialogTitle: string, dialogSubTitle: string, dialogType: string, requestType: number) {
		const dialogRef = this.dialog.open(ContactDialogComponent, {
			width: '400px'
		});
		dialogRef.componentInstance.requestType = requestType ? requestType : null;
		dialogRef.componentInstance.dialogTitle = dialogTitle ? dialogTitle : 'Contact Us';
		dialogRef.componentInstance.dialogSubTitle = dialogSubTitle ? dialogSubTitle : null;
		dialogRef.componentInstance.dialogType = dialogType ? dialogType : 'contactUs';

		dialogRef.componentInstance.onButtonAction.subscribe((action: any) => {
			if (action === 'success') {
				//   console.log('request button');
			}
		});
	}
	suggestProvider(events: any) {
		//  console.log(events);
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	cancelMoreInformationAction(providerID: number, index: number, interestType: number) {
		//console.log(providerID, interestType, index, this.dataProviders);
		if (!this.dataProviders[index]['userProviderStatus']) {
			this.dataProviders[index]['userProviderStatus'] = [];
		}
        
        if(interestType === 2086) {
            if (this.dataProviders[index]['userProviderStatus'].indexOf(2086) > -1) {
                this.dataProviderService.cancelMoreInformationAction(providerID, interestType).subscribe(resp => {
                    this.dataProviders[index]['userProviderStatus'].push(270);
                    this.loadAll();
                    this.loader = false;
                    this.dataProviders[index]['userProviderStatus'].splice(this.dataProviders[index]['userProviderStatus'].indexOf(2086), 1);
                }, err => {
    
                });
            }
        } else if(interestType === 566) {
            //console.log('TRUE', this.dataProviders[index]['userProviderStatus'].indexOf(566));
            if (this.dataProviders[index]['userProviderStatus'].indexOf(566) > -1) {
                this.dataProviderService.cancelMoreInformationAction(providerID, interestType).subscribe(resp => {
                    console.log(resp, this.dataProviders[index]['userProviderStatus'].push(272));
                    this.dataProviders[index]['userProviderStatus'].push(272);
                    this.loadAll();
                    this.loader = false;
                    this.dataProviders[index]['userProviderStatus'].splice(this.dataProviders[index]['userProviderStatus'].indexOf(566), 1);
                }, err => {
    
                });
                
            }
        } else if(interestType === 565) {
            if (this.dataProviders[index]['userProviderStatus'].indexOf(565) > -1) {
                this.dataProviderService.cancelMoreInformationAction(providerID, interestType).subscribe(resp => {
                    this.dataProviders[index]['userProviderStatus'].push(271);
                    this.loadAll();
                    this.loader = false;
                    this.dataProviders[index]['userProviderStatus'].splice(this.dataProviders[index]['userProviderStatus'].indexOf(565), 1);
                }, err => {
    
                });
                
            }
        }
	}

	trackByRecordID(index, provider) {
		return provider.recordId;
	}

	checkMymatches() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
		let changes = this.currentSorting.key;
		let order = this.currentSorting.order;
		/* const query = {
			'sort': [
				{
					changes: order
				}
			]
		}
		console.log('sort',query); */
		if (!this.onBoarding) {
			this.amassSettingService.activateMymatches().subscribe(data => {
				//console.log('dataActivated',data);
			});
		}
		this.amassSettingService.myMatches(changes, order, this.itemsFrom).subscribe(data => {
			this.myMatches = true;
			this.myMatchesActivated = true;
			this.onSuccessFilter(data, data);
		});
	}

	onFilterChangesCorona(event, corona) {
		// console.log('onFilterChanges...', event);
		if (this.isAuthenticated()) {
			if (corona === 'enableclearfilter') {
				this.corona = true;
                this.coronaActive=true;
				this.sortingArrayMethod();
			}
			if (event === 'clearFilter') {
				this.onFilter({
					query: 'clearFilter'
				});
				return;
			}
			const term = "providerTags.id";
			//const id=[1045,10];
			this.selectFilters[term] = this.selectFilters[term] ? this.selectFilters[term] : [];
			if (typeof (this.selectFilters[term]) === 'object' && typeof (this.selectFilters[term]) !== 'string') {
				this.selectFilters[term].push(this.coronaId);
			} else {
				this.selectFilters[term] = this.coronaId;
			}
			this.selectFilterData.push({
				queryType: 'must',
				queryTerm: term,
				filters: this.selectFilters[term]
			});
			if (event && this.selectFilterData && this.selectFilterData.length) {
				const requestBody = bob.requestBodySearch();
				const allFilterQuery = bob.boolQuery();
				//const filters = event.selectedFilterData;
				const filters = this.selectFilterData;
				const filtersLength = filters.length;
				for (let i = 0; i < filtersLength; i++) {
					const term = filters[i].queryTerm;
					const filter = filters[i].filters;
					// console.log(filter);
					if (filters[i].filters && filter.length) {
						if ((filters[i].queryType === 'must' || filters[i].queryType === 'should') && term === 'marketplaceStatus') {
							this.statusFilter = '';
							if (filter === 'Followed' || filter === 'Unlocked') {
								this.statusFilter = filter;
							} else if (filter === 'Complete') {
								allFilterQuery.should(bob.matchQuery(term, 'Expanded'));
								allFilterQuery.should(bob.matchQuery(term, 'Detailed'));
								allFilterQuery.should(bob.matchQuery(term, 'Followed'));
								allFilterQuery.should(bob.matchQuery(term, 'Unlocked'));
								this.statusFilter = '';
							} else if (filter !== 'Followed' && filter !== 'Unlocked') {
								allFilterQuery.should(bob.matchQuery(term, filter));
							}
						} else if (filters[i].queryType === 'range' || filters[i].queryType === 'rangeFromTo' || filters[i].queryType === 'datepicker') {
							//console.log(filters[i].queryType, typeof filter);
							if (typeof filter === 'string') {
								if (filters[i].queryType === 'datepicker') {
									allFilterQuery.must(bob.rangeQuery(term).from(this.datePipe.transform(filter, 'yyyy-MM-dd', 'EST')));
								} else {
									allFilterQuery.must(bob.rangeQuery(term).from(filter));
								}
							} else {
								allFilterQuery.must(bob.rangeQuery(term).from(filter[0][0]).to(filter[0][1]));
							}
						} else if (filters[i].queryType === 'rangeBetween') {
							allFilterQuery.must(bob.rangeQuery(term).gte(filter[0][0]).lte(filter[0][1]));
						} else {
							allFilterQuery.must(bob.termsQuery(term, filter));
						}
					}
				}

				requestBody.query(allFilterQuery);
				this.onFilterCorona({
					query: requestBody.toJSON(),
					'statusFilter': this.statusFilter
				});
			}
		}
		else {
			this.openSignUp();
		}
		return;
	}
	onClearChangesSearchData() {
		this.commonSearch = null;
		this.transactional = false;
		this.shipping = false;
		this.jobs = false;
		this.location = false;
        if (localStorage.getItem('SearchData')) {
            localStorage.removeItem('SearchData');
        }
        this.dataProviderService.sendClearSearch('Clear');
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
		this.page = 0;
		this.dataProviders = [];
		if(!this.isAuthenticated())
		{
			console.log('swea2');
			this.subscription = this.dataProviderService.searchData('null','unregistered').subscribe(
				(res) => this.onSuccessFilter(res, res.headers),
				(res) => this.onError(res)
			);
		}
		else if(this.isAuthenticated())
		{
			this.loadAll();
		}
		
	}
	onFilterChangesSearchData(searchDatamsg) {
		console.log(searchDatamsg);
		console.log('commonsearch', this.commonSearch);
		if (this.commonSearch === searchDatamsg) {
			this.onClearChangesSearchData();
		}
		else {
			this.commonSearch = searchDatamsg;
			if (this.subscription) {
				this.subscription.unsubscribe();
			}
			if (searchDatamsg === 'Transactional') {
				this.transactional = true;
				this.shipping = false;
				this.jobs = false;
				this.location = false;
			}
			else if (searchDatamsg === 'Shipping') {
				this.shipping = true;
				this.jobs = false;
				this.location = false;
				this.transactional = false;
			}
			else if (searchDatamsg === 'Jobs') {
				this.jobs = true;
				this.location = false;
				this.transactional = false;
				this.shipping = false;
			}
			else if (searchDatamsg === 'Location') {
				this.location = true;
				this.transactional = false;
				this.shipping = false;
				this.jobs = false;
			}
			if (this.jobs || this.transactional || this.shipping || this.jobs || this.location) {
				this.page = 0;
				this.subscription = this.dataProviderService.searchData('null',searchDatamsg).subscribe(
					(res) => this.onSuccessFilter(res, res.headers),
					(res) => this.onError(res)
				);
			}
			else {
				console.log('searchmsg', searchDatamsg);
				this.search(searchDatamsg);
			}
		}
	}
	loadAllCorona() {
		
		console.log('corona');
		this.limitedProviders = '';
		this.loader = true;
		let queryBody = {};
		if (this.corona) {
			if (!this.coronaSorting) {
				this.coronaSorting = {
					key: 'createdDate',
					label: 'createdDate',
					order: 'desc',
					
				}
			}
		}
		console.log('corona',this.currentCoronaFilter,this.coronaSorting);
		if (this.coronaSorting && this.currentCoronaFilter) {
			const requestBody = bob.requestBodySearch();
			// requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
			if (typeof this.coronaSorting === 'object' && this.coronaSorting['key'] !== 'recommended') {
				console.log('1');
				requestBody.sort(bob.sort(this.coronaSorting['key']).order(this.coronaSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
			}
			/* else if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
				console.log('2');
				requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
			} */
				const queryJSON = requestBody.toJSON();
			queryJSON['filter'] = this.currentCoronaFilter;
			queryBody = queryJSON;
		}
		if (this.page) {
			queryBody['from'] = this.itemsFrom;
		}
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
		this.subscription = this.dataProviderService.queryWithSearchFilter(queryBody, this.statusFilter).subscribe(
			(res) => this.onSuccessFilter(res, res.headers),
			(res) => this.onError(res)
		);
		return;
	}
	openSigninDialog() {
		this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
			width: '400px'
		});
	}

	openApplyDialog() {
		this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '570px'
		});
	}
	openSignUp() {
		if (this.dialog) {
			this.dialog.closeAll();
		}
		this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '570px'
		});
		return;
	}

	mouseEnter(p) { // This event used for open and close the popup while mouseover the locked providers
		setTimeout(() => {
			p.close();
		}, 7000);
	}

	upgrade() {
		this.router.navigate(['/upgrade']);
	}

	onExpireSession(event) {
		//console.log('eventcheck',event);
		if (event) {
			//console.log('eventcheck',event);
			// this.formSubmitted = true;
			this.authService.logout().subscribe(d => {
				this.stateService.storeUrl(null);
				this.principal.authenticate(null);
				this.router.navigate(['../../']);
				setTimeout(() => {
					this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
				}, 2000);
			});
		}
	}
	beforeSessionExpires(event) {
		if (event) {
			// this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
		}
	}
}
  