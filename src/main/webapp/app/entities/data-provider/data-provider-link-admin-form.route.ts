import { Route, Routes } from '@angular/router';
import { CanDeactivateGuard, UserRouteAccessService } from 'app/shared';
import { DataProviderLinkAdminFormComponent } from './data-provider-link-admin-form.component';

export const addLinkAdminRoute: Route = {
    path: 'add-link-admin',
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'Data Links',
        breadcrumb: 'Resources > Link > Add New > Admin',
        type: 'dynamic'
    },
    component: DataProviderLinkAdminFormComponent,
    canActivate: [UserRouteAccessService],
    canDeactivate: [CanDeactivateGuard]
};