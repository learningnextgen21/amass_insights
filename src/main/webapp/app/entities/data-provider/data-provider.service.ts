import { Injectable } from '@angular/core';

import { Observable, Subject } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { DataProvider } from './data-provider.model';
import { ResponseWrapper, createRequestOption,Principal } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { DataProviderTag } from '../data-provider-tag';

@Injectable()
export class DataProviderService {
    private resourceUrl = 'api/providers';
    private lookupCodeUrl='api/lookup-code';
    private resourceElasticSearchUrl = 'api/es/providersearch';
    private investorElasticSearchUrl = 'api/es/investorsearch';
    private editedDataByUserUrl = 'api/providerAdmin';
    private approveAllProviderDataUrl = 'api/providerUpdate';
    private SearchUrl = 'api/search';
    private resourceElasticSearchTagUrl = 'api/es/providerTagsearch';
    private unrestrictedElasticSearchTagUrl = 'api/ur/es/providerTagsearch';
    private unrestrcitedresourceElasticSearchUrl = 'api/ur/es/providersearch';
    private unrestrictedUrl = 'api/ur/providerDetail';
    private resourceFollowUrl = 'api/follow-provider?providerID';
    private resourceFollowUrlTag = 'api/follow-providerTag?providerID';
    private resourceExpandUrl = 'api/expand-provider?providerID';
    private resourceUnlockUrl = 'api/unlock-provider?providerID';
    private resourceConfirmExpandUrl = 'api/confirm-expand-provider?providerID';
    private resourceConfirmUnlockUrl = 'api/confirm-unlock-provider?providerID';
    private resourceCancelExpandUrl = 'api/cancel-expand-provider?providerID';
    private resourceCancelUnlockUrl = 'api/cancel-unlock-provider?providerID';
    private elasticSearchGeneric = '/api/es/qsearch';
    private resourceCancelMoreInfoUrl = 'api/cancel-contact-us-provider-more-info?providerID';
    private categoriesExplanationUrl = 'api/data-categories/desc/';
    private featuresExplanationUrl = 'api/data-features/desc/';
    private industriesExplanationUrl = 'api/data-industries/desc/';
    private providerTagsExplanationUrl = 'api/data-provider-tags/desc/';
    private providerTypesExplanationUrl = 'api/data-provider-types/desc/';
    private dataSourcesExplanationUrl = 'api/data-sources/desc/';
    private fileDownloadUrl = 'api/filedownload/';
    private providerTypesUrl = 'api/data-provider-types';
    private providerEditUrl = 'api/provider_profile_edit';
    private providerTagUrl = 'api/data-provider-tags';
    private dataProviderTagUrl = 'api/data-provider-tags';
    private providerNewTagUrl = 'api/provider-tag';
    private providerRemoveTagUrl = 'api/remove-provider-tag';
    private providerAdminUrl = 'api/providers';
    private resourceFileUpload = '/api/ur/resources-multiple-file-upload';
    private providerLogo = '/api/provider-logo-upload';
    private resourceSingeFile = '/api/ur/resource-file-upload';
    private featureProvider = '/api/ur/providersearch';
    private ipAddressUser = '/api/ur/ipaddress';
    private searchDataProvider = '/api/ur/searchdata';
    private organizationAdminUrl = 'api/data-organizations';
    private getOrganizationIDUrl = '/api/es/organizationsearch';
    private linkAdminUrl = '/api/links';
    private dataLinksUrl = '/api/es/qsearch?type=datalinks';
    private resourceLock = 'api/resourceLock';
    private changeLogoUpload = new Subject<any>();
    changeLogoUpload$ = this.changeLogoUpload.asObservable();
    private clearHeaderSearch = new Subject<any>();
    clearHeaderSearch$ = this.clearHeaderSearch.asObservable();
    public recordUpdated$ = new Subject<boolean>();
    public newUser: any;
	constructor(private http: HttpClient, private dateUtils: JhiDateUtils, private datePipe: DatePipe,private principal: Principal) { }

	create(dataProvider: DataProvider): Observable<DataProvider> {
		const copy = this.convert(dataProvider);
		return this.http.post(this.resourceUrl, copy);
	}

	update(dataProvider: DataProvider): Observable<DataProvider> {
		const copy = this.convert(dataProvider);
		return this.http.put(this.resourceUrl, copy);
    }

    getProvidersTypes() {
        return this.http.get(this.providerTypesUrl);
    }

	find(recordID: string): Observable<DataProvider> {
		return this.http.get(`${this.resourceUrl}/${recordID}`);
    }
    findLookupCode(value: string): Observable<any> {
		return this.http.get(`${this.lookupCodeUrl}/${value}`);
    }
    getDataLinks() {
        return this.http.post(this.dataLinksUrl, {});
    }
    featureProviderData(value): Observable<any> {
        return this.http.post(this.featureProvider + '?type=' + value, {});
    }
    findByResources(value): Observable<any> {
        return this.http.post(this.resourceLock,value);
    }

    findById(recordID: string): Observable<DataProvider> {
        const req = {
            _source: {
                exclude: ['ownerOrganization.description'],
                include: [
                    'assetClasses',
                    'authoredArticles',
                    'authoredArticles.publishedDate',
                    'authoredArticles.purpose',
                    'authoredArticles.summaryHTML',
                    'authoredArticles.title',
                    'authoredArticles.url',
                    'categories',
                    'collectionMethodsExplanation',
                    'completeness',
                    'consumerPartners.id',
                    'consumerPartners.mainDataIndustry.dataIndustry',
                    'consumerPartners.mainDataCategory.datacategory',
                    'consumerPartners.logo.logo',
                    'consumerPartners.ownerOrganization.name',
                    'consumerPartners.shortDescription',
                    'consumerPartners.providerName',
                    'consumerPartners.recordID',
                    'consumerPartners.completeness',
                    'consumerPartners.createdDate',
                    'consumerPartners.updatedDate',
                    'consumerPartners.scoreOverall',
                    'consumerPartners.scoreOverall.description',
                    'competitors.id',
                    'competitors.mainDataIndustry.dataIndustry',
                    'competitors.mainDataCategory.datacategory',
                    'competitors.logo.logo',
                    'competitors.ownerOrganization.name',
                    'competitors.shortDescription',
                    'competitors.providerName',
                    'competitors.recordID',
                    'competitors.completeness',
                    'competitors.createdDate',
                    'competitors.updatedDate',
                    'competitors.scoreOverall',
                    'competitors.scoreOverall.description',
                    'creatorUserId',
                    'createdDate',
                    'dataProvider',
                    'dataRoadmap',
                    'summary',
                    'dataUpdateFrequency',
                    'deliveryFrequencyNotes',
                    'dataUpdateFrequencyNotes',
                    'dateCollectionBegan',
                    'dateCollectionRangeExplanation',
                    'deliveryFormats',
                    'deliveryMethods',
                    'distributionPartners.id',
                    'distributionPartners.mainDataIndustry.dataIndustry',
                    'distributionPartners.mainDataCategory.datacategory',
                    'distributionPartners.logo.logo',
                    'distributionPartners.ownerOrganization.name',
                    'distributionPartners.recordID',
                    'distributionPartners.shortDescription',
                    'distributionPartners.providerName',
                    'distributionPartners.completeness',
                    'distributionPartners.createdDate',
                    'distributionPartners.updatedDate',
                    'distributionPartners.scoreOverall',
                    'distributionPartners.scoreOverall.description',
                    'documentsMetadata.description',
                    'documentsMetadata.fileID',
                    'documentsMetadata.fileName',
                    'documentsMetadata.fileSize',
                    'documentsMetadata.dateCreated',
                    'documentsMetadata.dateModified',
                    'documentsMetadata.dataResourcePurpose.purpose',
                    'documentsMetadata.dataResourcePurposeType.purposeType',
                    'features',
                    'freeTrialAvailable',
                    'geographicalFoci.description',
                    'geographicalFoci.id',
                    'historicalDateRange',
                    'id',
                    'industries',
                    'investor_clients',
                    'scoreInvestorsWillingness.description',
                    'investorTypes.id',
                    'investorTypes.description',
                    'keyDataFields',
                    'logo.logo',
                    'longDescription',
                    'mainAssetClass',
                    'mainDataCategory.datacategory',
                    'mainDataCategory.id',
                    'mainDataCategory.explanation',
                    'mainDataCategory.hashtag',
                    'mainDataIndustry.dataIndustry',
                    'mainDataIndustry.id',
                    'mainDataIndustry.explanation',
                    'mainDataIndustry.hashtag',
                    'marketplaceStatus',
                    'notes',
                    'orgRelationshipStatus',
                    'scoreOverall.description',
                    'ownerOrganization',
                    'potentialDrawbacks',
                    'pricingModels',
                    'productDetails',
                    'providerName',
                    'providerType.providerType',
                    'providerType.id',
                    'providerType.explanation',
                    'providerPartners.id',
                    'providerPartners.mainDataIndustry',
                    'providerPartners.mainDataIndustry.id',
                    'providerPartners.mainDataIndustry.dataIndustry',
                    'providerPartners.mainDataCategory',
                    'providerPartners.mainDataCategory.id',
                    'providerPartners.mainDataCategory.datacategory',
                    'providerPartners.logo.logo',
                    'providerPartners.ownerOrganization.name',
                    'providerPartners.providerName',
                    'providerPartners.shortDescription',
                    'providerPartners.completeness',
                    'providerPartners.createdDate',
                    'providerPartners.updatedDate',
                    'providerPartners.scoreOverall',
                    'providerPartners.scoreOverall.description',
                    'providerTags',
                    'providerPartners.recordID',
                    'languagesAvailable',
                    'sampleOrPanelSize',
                    'deliveryFrequencies',
                    'dataLicenseType',
                    'dataLicenseTypeDetails',
                    'useCasesOrQuestionsAddressed',
                    'legalAndComplianceIssues',
                    'publicCompaniesCovered',
                    'scoreReadiness.description',
                    'recordID',
                    'relevantArticles',
                    'securityTypes',
                    'shortDescription',
                    'sources',
                    'scoreUniqueness.description',
                    'uniqueValueProps',
                    'updatedDate',
                    'userPermission',
                    'scoreValue.description',
                    'website',
                    'relevantSectors',
                    'scoreReadiness',
                    'scoreUniqueness',
                    'scoreInvestorsWillingness',
                    'scoreValue',
                    'scoreOverall',
                    'deliveryMethods',
                    'deliveryFormats',
                    'deliveryFrequencies',
                    'freeTrialAvailable',
                    'inactive',
                    'link',
                    'link.link',
                    'relevantSectors',
					'createdDate',
					'profileVerificationStatus',
                    'numberOfDataSources',
                    'dataSourcesDetails',
					'numberOfAnalystEmployees',
					'ownerOrganization.name',
                    'ownerOrganization.financialPosition',
                    'yearFirstDataProductLaunched',
                    'secRegulated',
                    'investorClientBucket',
                    'investorClientBucket.id',
                    'investorClientBucket.description',
                    'investorClientsCount',
                    'organizationType',
                    'managerType',
                    'strategies.id',
                    'strategies.description',
                    'researchStyles',
                    'investingTimeFrame',
                    'equitiesMarketCap',
                    'equitiesStyle',
                    'accessOffered',
                    'dataProductTypes',
                    'dataTypes',
                    'publicEquitiesCoveredRange',
                    'identifiersAvailable',
                    'pointInTimeAccuracy',
                    'dataGaps',
                    'dataGapsReasons',
                    'duplicatesCleaned',
                    'includesOutliers',
                    'outlierReasons',
                    'biases',
                    'normalized',
                    'lagTime',
                    'paymentMethodsOffered',
                    'subscriptionModel',
                    'subscriptionTimePeriod',
                    'monthlyPriceRange',
                    'minimumYearlyPrice',
                    'maximumYearlyPrice',
                    'pricingDetails',
                    'trialPricingModel',
                    'trialDuration',
                    'trialAgreementType',
                    'trialPricingDetails',
                    'competitiveDifferentiators',
                    'publicEquitiesCoveredCount',
                    'discussionCleanliness',
                    'datasetSize',
                    'datasetNumberOfRows',
                    'discussionAnalytics',
                    'outOfSampleData',
                    'sampleOrPanelBiases',
                    'pricingModel',
                    'researchMethods',
                    'ieiStatus',
                    'ieiStatus.id',
                    'ieiStatus.description',
                    'priority',
                    'priority.id',
                    'priority.description',
                    'outOfSampleData',
                    'sampleOrPanelBiases',
                    'pricingModel',
                    'researchMethods',
                    'researchMethodsCompleted',
                    'dataProvidersPricingModels'
				]
			},
			'filter': {
				'query': {
					'match': {
						'recordID': recordID
					}
				}
			}
		};
		return this.http.post(this.resourceElasticSearchUrl + '?detail=true', req);
    }
    findByUnrestrictedId(recordID: string): Observable<any>{
		return this.http.post(this.unrestrictedUrl + '?recordID=' + recordID, {});
    }
    findByOrganizationId(id: string): Observable<DataProvider> {
        const req = {
            _source: {
                exclude: ['ownerOrganization'],
                include: [
                    'id',
                    'recordID',
                    'name',
                    'city',
                    'state',
                    'country',
                    'zipCode',
                    'yearFounded',
                    'yearExit',
                    'headcountNumber',
                    'headCount',
                    'headcountBucket',
                    'ieiStatus',
                    'researchMethodsCompleted',
                    'marketplaceStatus'
				]
			},
			'filter': {
				'query': {
					'match': {
						'id': id
					}
				}
			}
		};
		return this.http.post(this.getOrganizationIDUrl, req);
    }

    findByIdForForm(recordID: string): Observable<DataProvider> {
		const req = {
			'_source': {
				'exclude': [
					'ownerOrganization.description',
                    'researchMethodsCompleted',
                    'authoredArticles',
                    'consumerPartners',
                    'distributionPartners',
                    'documentsMetadata',
                    'providerPartners'
				],
				'include': [
					'assetClasses',
					'categories',
					'collectionMethodsExplanation',
					'completeness',
                    'createdDate',
                    'creatorUserId',
					'dataProvider',
					'dataRoadmap',
					'summary',
					'dataUpdateFrequency',
					'deliveryFrequencyNotes',
					'dataUpdateFrequencyNotes',
					'dateCollectionBegan',
					'dateCollectionRangeExplanation',
					'deliveryFormats',
					'deliveryMethods',
					'features',
					'freeTrialAvailable',
					'geographicalFoci.description',
					'geographicalFoci.id',
					'historicalDateRange',
					'id',
					'industries',
					'investor_clients',
					'scoreInvestorsWillingness.description',
					'investorTypes.id',
					'investorTypes.description',
					'keyDataFields',
					'logo.logo',
					'longDescription',
					'mainAssetClass',
					'mainDataCategory.datacategory',
					'mainDataCategory.id',
					'mainDataCategory.explanation',
					'mainDataIndustry.dataIndustry',
					'mainDataIndustry.id',
					'mainDataIndustry.explanation',
					'marketplaceStatus',
					'notes',
					'orgRelationshipStatus',
					'scoreOverall.description',
					'ownerOrganization',
					'potentialDrawbacks',
					'pricingModels',
					'productDetails',
					'providerName',
					'providerType.providerType',
					'providerType.id',
					'providerType.explanation',
					'providerTags',
					'languagesAvailable',
					'sampleOrPanelSize',
					'deliveryFrequencies',
					'dataLicenseType',
					'dataLicenseTypeDetails',
					'useCasesOrQuestionsAddressed',
					'legalAndComplianceIssues',
					'publicCompaniesCovered',
					'scoreReadiness.description',
					'recordID',
					'relevantArticles',
					'securityTypes',
					'shortDescription',
					'sources',
					'scoreUniqueness.description',
					'uniqueValueProps',
					'userPermission',
                    'scoreValue.description',
                    'deliveryMethod.lookupcode',
                    'deliveryFormat.lookupcode',
					'website',
					'relevantSectors',
					'scoreReadiness',
					'scoreUniqueness',
					'scoreInvestorsWillingness',
					'scoreValue',
                    'scoreOverall',
                    'deliveryMethods',
                    'deliveryFormats',
                    'deliveryFrequencies',
                    'freeTrialAvailable',
                    'link',
                    'link.link',
                    'profileVerificationStatus',
                    'numberOfDataSources',
                    'dataSourcesDetails',
					'numberOfAnalystEmployees',
                    'ownerOrganization.name',
                    'ownerOrganization.headCount',
                    'ownerOrganization.financialPosition',
                    'yearFirstDataProductLaunched',
                    'secRegulated',
                    'investorClientBucket',
                    'investorClientsCount',
                    'organizationType',
                    'managerType',
                    'strategies',
                    'researchStyles',
                    'investingTimeFrame',
                    'equitiesMarketCap',
                    'equitiesStyle',
                    'accessOffered',
                    'dataProductTypes',
                    'dataTypes',
                    'publicEquitiesCoveredRange',
                    'identifiersAvailable',
                    'pointInTimeAccuracy',
                    'dataGaps',
                    'dataGapsReasons',
                    'duplicatesCleaned',
                    'includesOutliers',
                    'outlierReasons',
                    'biases',
                    'normalized',
                    'lagTime',
                    'paymentMethodsOffered',
                    'subscriptionModel',
                    'subscriptionTimePeriod',
                    'monthlyPriceRange',
                    'minimumYearlyPrice',
                    'maximumYearlyPrice',
                    'pricingDetails',
                    'trialPricingModel',
                    'trialDuration',
                    'trialAgreementType',
                    'trialPricingDetails',
                    'competitiveDifferentiators',
                    'publicEquitiesCoveredCount',
                    'dataProvidersPricingModels'
				]
			},
			'filter': {
				'query': {
					'match': {
						'recordID': recordID
					}
				}
			}
		};
		return this.http.post(this.resourceElasticSearchUrl + '?detail=true', req);
	}

    editedDatafindByRecId(recordID: string): Observable<DataProvider> {
		return this.http.get(`${this.editedDataByUserUrl}/${recordID}`);
	}

    approveAllProviderData(newProviderData: any): Observable<DataProvider> {
        console.log(newProviderData);
		return this.http.put(this.approveAllProviderDataUrl, newProviderData);
	}

    findByIdEditForm(recordID: string): Observable<DataProvider> {
		const req = {
			'_source': {
				'exclude': [
					'ownerOrganization.description',
                    'authoredArticles',
                    'documentsMetadata',
                    'consumerPartners.mainDataIndustry.dataIndustry',
                    'consumerPartners.mainDataCategory.datacategory',
                    'consumerPartners.logo.logo',
                    'consumerPartners.ownerOrganization.name',
                    'consumerPartners.shortDescription',
                    'distributionPartners.mainDataIndustry.dataIndustry',
                    'distributionPartners.mainDataCategory.datacategory',
                    'distributionPartners.logo.logo',
                    'distributionPartners.ownerOrganization.name',
                    'distributionPartners.recordID',
                    'distributionPartners.shortDescription',
                    'providerPartners.mainDataIndustry.dataIndustry',
                    'providerPartners.mainDataCategory.datacategory',
                    'providerPartners.logo.logo',
                    'providerPartners.ownerOrganization.name',
                    'providerPartners.shortDescription',
                    'competitors.mainDataIndustry.dataIndustry',
                    'competitors.mainDataCategory.datacategory',
                    'competitors.logo.logo',
                    'competitors.ownerOrganization.name',
                    'competitors.shortDescription',
				],
				'include': [
					'assetClasses',
					'categories',
					'collectionMethodsExplanation',
					'completeness',
                    'createdDate',
                    'creatorUserId',
					'dataProvider',
					'dataRoadmap',
					'summary',
					'dataUpdateFrequency',
					'deliveryFrequencyNotes',
					'dataUpdateFrequencyNotes',
					'dateCollectionBegan',
					'dateCollectionRangeExplanation',
					'deliveryFormats',
					'deliveryMethods',
					'features',
					'freeTrialAvailable',
					'geographicalFoci.description',
					'geographicalFoci.id',
					'historicalDateRange',
					'id',
					'industries',
					'investor_clients',
					'scoreInvestorsWillingness.description',
					'investorTypes.id',
					'investorTypes.description',
					'keyDataFields',
					'logo.logo',
					'longDescription',
					'mainAssetClass',
					'mainDataCategory.datacategory',
					'mainDataCategory.id',
					'mainDataCategory.explanation',
					'mainDataIndustry.dataIndustry',
					'mainDataIndustry.id',
					'mainDataIndustry.explanation',
					'marketplaceStatus',
					'notes',
					'orgRelationshipStatus',
					'scoreOverall.description',
					'ownerOrganization',
					'potentialDrawbacks',
					'pricingModels',
					'productDetails',
					'providerName',
					'providerType.providerType',
					'providerType.id',
					'providerType.explanation',
					'providerTags',
					'languagesAvailable',
					'sampleOrPanelSize',
					'deliveryFrequencies',
					'dataLicenseType',
					'dataLicenseTypeDetails',
					'useCasesOrQuestionsAddressed',
					'legalAndComplianceIssues',
					'publicCompaniesCovered',
					'scoreReadiness.description',
					'recordID',
					'relevantArticles',
					'securityTypes',
					'shortDescription',
					'sources',
					'scoreUniqueness.description',
					'uniqueValueProps',
					'userPermission',
                    'scoreValue.description',
                    'deliveryMethod.lookupcode',
                    'deliveryFormat.lookupcode',
					'website',
					'relevantSectors',
					'scoreReadiness',
					'scoreUniqueness',
					'scoreInvestorsWillingness',
					'scoreValue',
                    'scoreOverall',
                    'deliveryMethods',
                    'deliveryFormats',
                    'deliveryFrequencies',
                    'freeTrialAvailable',
                    'inactive',
                    'link',
                    'link.link',
                    'numberOfDataSources',
                    'dataSourcesDetails',
					'numberOfAnalystEmployees',
                    'ownerOrganization.name',
                    'ownerOrganization.headCount',
                    'ownerOrganization.financialPosition',
                    'yearFirstDataProductLaunched',
                    'secRegulated',
                    'investorClientBucket',
                    'investorClientBucket.id',
                    'investorClientBucket.description',
                    'investorClientsCount',
                    'organizationType',
                    'managerType',
                    'strategies',
                    'strategies.id',
                    'strategies.description',
                    'researchStyles',
                    'investingTimeFrame',
                    'equitiesMarketCap',
                    'equitiesStyle',
                    'accessOffered',
                    'dataProductTypes',
                    'dataTypes',
                    'publicEquitiesCoveredRange',
                    'identifiersAvailable',
                    'pointInTimeAccuracy',
                    'dataGaps',
                    'dataGapsReasons',
                    'duplicatesCleaned',
                    'includesOutliers',
                    'outlierReasons',
                    'biases',
                    'normalized',
                    'lagTime',
                    'paymentMethodsOffered',
                    'subscriptionModel',
                    'subscriptionTimePeriod',
                    'monthlyPriceRange',
                    'minimumYearlyPrice',
                    'maximumYearlyPrice',
                    'pricingDetails',
                    'trialPricingModel',
                    'trialDuration',
                    'trialAgreementType',
                    'trialPricingDetails',
                    'competitiveDifferentiators',
                    'publicEquitiesCoveredCount',
                    'emails',
                    'ownerOrganization.name',
                    'ownerOrganization.yearExit',
                    'ownerOrganization.headcountBucket',
                    'ownerOrganization.headcountNumber',
                    'ownerOrganization.clientBase',
                    'consumerPartners.id',
                    'consumerPartners.providerName',
                    'consumerPartners.recordID',
                    'providerPartners.id',
                    'providerPartners.recordID',
                    'providerPartners.providerName',
                    'distributionPartners.id',
                    'distributionPartners.recordID',
                    'distributionPartners.providerName',
                    'complementaryProviders.id',
                    'competitors.id',
                    'competitors.recordID',
                    'competitors.providerName',
                    'privacy',
                    'permission',
                    'discussionCleanliness',
                    'discussionAnalytics',
                    'datasetSize',
                    'datasetNumberOfRows',
                    'outOfSampleData',
                    'sampleOrPanelBiases',
                    'pricingModel',
                    'researchMethods',
                    'researchMethodsCompleted',
                    'ieiStatus',
                    'ieiStatus.id',
                    'ieiStatus.description',
                    'priority',
                    'priority.id',
                    'priority.description',
                    'consent',
                    'visibility',
                    'dataProvidersPricingModels'
				]
			},
			'filter': {
				'query': {
					'match': {
						'recordID': recordID
					}
				}
			}
		};
		return this.http.post(this.resourceElasticSearchUrl + '?detail=true', req);
	}
	query(req?: any): Observable<HttpResponse<DataProvider[]>> {
        //console.log(req);
        const options = createRequestOption(req);
        if(this.principal.isAuthenticated()) {
            return this.http.post<DataProvider[]>(this.resourceElasticSearchUrl, req, { observe: 'response' });
        }
        else
        {
            return this.http.post<DataProvider[]>(this.unrestrcitedresourceElasticSearchUrl, req, { observe: 'response' });
        }
	}
    queryinvestor(req?: any): Observable<HttpResponse<any>> {
        //console.log(req);
        const options = createRequestOption(req);
        if(this.principal.isAuthenticated()) {
            return this.http.post<any>(this.investorElasticSearchUrl, req, { observe: 'response' });
        }
	}
    queryTag(req?: any): Observable<HttpResponse<DataProviderTag[]>> {
        const options = createRequestOption(req);
        if(this.principal.isAuthenticated()) {
            return this.http.post<DataProviderTag[]>(this.resourceElasticSearchTagUrl, req, { observe: 'response' });
        }
        else{
            return this.http.post<DataProviderTag[]>(this.unrestrictedElasticSearchTagUrl, req, { observe: 'response' });
        }
	}
	elasticSearchAuthoredArticles(recordID: string, from: number): Observable<any> {
		const query = {
			'_source': {
				'include': [
					'id',
					'title',
					'url',
					'author',
					'authorOrganization.name',
					'publishedDate',
					'authorDataProvider.id',
					'visualUrl',
					'authorDataProvider.providerName',
					'createdDate',
					'summary',
					'summaryHTML',
					'stream',
					'purpose',
					'articleTopics'
				]
			},
			'sort': [
				{
					'publishedDate': 'desc'
				}
			],
            'from': from,
            'size': 500,
			'filter': {
				'query': {
					'bool': {
						'should': {
							'match':
								{ 'authorDataProvider.recordID': recordID }
						}
					}
				}
			}
		};
		return this.http.post(this.elasticSearchGeneric + '?type=dataarticle', query);
    }


	elasticSearchRelevantArticles(recordID: string, from: number): Observable<any> {
		const query = {
			'_source': {
				'include': [
					'id',
					'title',
					'url',
					'author',
					'authorOrganization.name',
					'publishedDate',
					'authorDataProvider.id',
					'visualUrl',
					'authorDataProvider.providerName',
					'createdDate',
					'summary',
					'summaryHTML',
					'stream',
					'purpose',
					'articleTopics'
				]
			},
			'sort': [
				{
					'publishedDate': 'desc'
				}
			],
            'from': from,
            'size': 500,
			'filter': {
				'query': {
					'bool': {
						'should': {
							'match':
								{ 'relevantProviders.recordID': recordID }
						}
					}
				}
			}
		};
		return this.http.post(this.elasticSearchGeneric + '?type=dataarticle', query);
	}

    searchData(sort?: any,req?: string): Observable<any> {
        let  newReq=null;
        if(sort !=='null')
        {
            newReq= Object.assign({
            }, sort);
            const options = createRequestOption(newReq);
        console.log('newreq',newReq);
        console.log('options',options);
        }

	//	return this.http.post(this.searchDataProvider + '?type=' + req, {});

        return this.http.post(this.searchDataProvider, sort, {
            params: {
                type:req
            }
        });
    }
    queryFilter(req: any): Observable<any> {
		return this.http.post(this.featureProvider + '?type=' + 'unregistered', {});
    }
    checkUserIpAddresss():Observable<any> {
		return this.http.post(this.ipAddressUser,{});
    }
    searchAdded(req:any):Observable<any>{
        return this.http.post(this.SearchUrl,req);
    }
	queryWithSearchFilter(req?: any, statusFilter?: any): Observable<any> {
		const newReq = Object.assign({
			'_source': {
				'include': [
					'id',
					'createdDate',
					'completeness',
                    'categories.id',
					'providerName',
					'mainDataCategory',
					'mainDataIndustry',
					'shortDescription',
					'marketplaceStatus',
					'userPermission',
					'recordID',
					'logo.logo',
					'updatedDate',
					'ownerOrganization',
					'scoreReadiness',
					'scoreInvestorsWillingness',
					'scoreUniqueness',
					'scoreValue',
                    'scoreOverall',
                    'deliveryMethods',
                    'deliveryFormats',
                    'deliveryFrequencies',
                    'freeTrialAvailable',
                    'relevantSectors',
                    'createdDate',
                    'providerTags',
                    'tagsProvider',
                    'pricingModel',
                    'followed',
                    "consumerPartners.id",
                    "distributionPartners.id",
                    "providerPartners.id"
				]
			}
		}, req);
		const options = createRequestOption(newReq);
        let elasticSearchUrl = '';
        if(this.principal.isAuthenticated())
        {
            switch (statusFilter) {
                case 'Followed':
                    elasticSearchUrl = this.resourceElasticSearchUrl + '?followedProviders=true';
                    break;
                case 'Unlocked':
                    elasticSearchUrl = this.resourceElasticSearchUrl + '?unlockedProviders=true';
                    break;
                default:
                    elasticSearchUrl = this.resourceElasticSearchUrl;
                    break;
            }
        }
        else if(!this.principal.isAuthenticated())
        {
            switch (statusFilter) {
                case 'Followed':
                    elasticSearchUrl = this.unrestrcitedresourceElasticSearchUrl + '?followedProviders=true';
                    break;
                case 'Unlocked':
                    elasticSearchUrl = this.unrestrcitedresourceElasticSearchUrl + '?unlockedProviders=true';
                    break;
                default:
                    elasticSearchUrl = this.unrestrcitedresourceElasticSearchUrl;
                    break;
            }
        }

		return this.http.post(elasticSearchUrl, newReq);
	}

	getMouseOverExplanation(id: number, type: string): Observable<any> {
		let url;
		switch (type) {
			case 'dataCategory':
			url = this.categoriesExplanationUrl + id;
			break;
			case 'dataFeature':
			url = this.featuresExplanationUrl + id;
			break;
			case 'dataIndustry':
			url = this.industriesExplanationUrl + id;
			break;
			case 'dataProviderTag':
			url = this.providerTagsExplanationUrl + id;
			break;
			case 'dataProviderType':
			url = this.providerTypesExplanationUrl + id;
			break;
			case 'dataSource':
			url = this.dataSourcesExplanationUrl + id;
			break;
			default:
			url = null;
			break;
		}
		return this.http.get(url, {responseType: 'text'});
    }

    getMouseOverExplanationByType(url: string): Observable<any> {
		return this.http.get('api/' + url + '?size=500');
	}

	downloadFile(fileID: string): Observable<any> {
		return this.http.get(this.fileDownloadUrl + fileID);
	}

	delete(id: number): Observable<any> {
		return this.http.delete(`${this.resourceUrl}/${id}`);
	}

	search(req?: any): Observable<HttpResponse<any>> {
		const options = createRequestOption(req);
		return this.http.post(this.resourceElasticSearchUrl, req, { observe: 'response' });
    }

    providerEdit(req?: any, formName?: string): Observable<any> {
        const model = req;
        console.log('...Model..',model);
        console.log('formName',formName);
        delete model['id'];
        if (model && model['dataUpdateFrequency.id']) {
            model.dataUpdateFrequency = model['dataUpdateFrequency.id'];

        }
        if (model && model.ownerOrganization) {
            console.log('..model 1..',model);
            console.log('...model 1',model.ownerOrganization);
            model['city'] = model.ownerOrganization.city;
            model['state'] = model.ownerOrganization.state;
            model['country'] = model.ownerOrganization.country;
            model['zipCode'] = model.ownerOrganization.zipCode;
            model['headCount'] = model.ownerOrganization.headCount;
            model['yearFounded'] = model.ownerOrganization.yearFounded;
            model['name'] = model.ownerOrganization.name;
            model['yearExit'] = model.ownerOrganization.yearExit;
           /*  if(model.ownerOrganization && model.ownerOrganization.headcountBucket && model.ownerOrganization.headcountBucket.id) {
                model['headcountBucket'] = model.ownerOrganization.headcountBucket.id;
                delete model.ownerOrganization.headcountBucket;
            } else if (model.ownerOrganization.headcountBucket) {
                model['headcountBucket'] = model.ownerOrganization.headcountBucket;
                 model.ownerOrganization.headcountBucket =null;
            } */
            if (model && model.ownerOrganization && model.ownerOrganization.headcountBucket) {
                model['headcountBucket'] = model.ownerOrganization.headcountBucket;
                delete model.ownerOrganization.headcountBucket;
            }else if (model && model.ownerOrganization && model.ownerOrganization.headcountBucket === null) {
                model['headcountBucket'] =null
                model.ownerOrganization.headcountBucket  = null;
            }

            // if (model['ownerOrganization.headcountBucket']) {
            //     model['headcountBucket'] = model['ownerOrganization.headcountBucket']['id'];
            // }
            // if (model.ownerOrganization.headcountBucket) {
            //     delete model.ownerOrganization.headcountBucket;
            // }
            model['headcountNumber'] = model.ownerOrganization.headcountNumber;
            model['financialPosition'] = model.ownerOrganization.financialPosition;
            model['clientBase'] = model.ownerOrganization.clientBase;
        }
        if (model && model.dataUpdateFrequency && model.dataUpdateFrequency.value) {
            model.dataUpdateFrequency = model.dataUpdateFrequency.value;
        }

        // if (model && model['scoreInvestorsWillingness.id']) {
        //     model.scoreInvestorsWillingness = model['scoreInvestorsWillingness.id'];
        // }
        // if (model && model.scoreInvestorsWillingness) {
        //     model.scoreInvestorsWillingness = model.scoreInvestorsWillingness.id;
        // }
       /*  if (model && model.scoreInvestorsWillingness && model.scoreInvestorsWillingness.value) {
            model.scoreInvestorsWillingness = model.scoreInvestorsWillingness.value;
        }

        if (model && model.scoreInvestorsWillingness && model.scoreInvestorsWillingness.id) {
            model.scoreInvestorsWillingness = model.scoreInvestorsWillingness.id;
        }
         else {
			delete model.scoreInvestorsWillingness;
		}
 */
        if (model && model.scoreInvestorsWillingness && model.scoreInvestorsWillingness.id) {
            model.scoreInvestorsWillingness = model.scoreInvestorsWillingness.id;
        }else if (model && model.scoreInvestorsWillingness && model.scoreInvestorsWillingness.id === null) {
            model.scoreInvestorsWillingness = null;
        }
       /*  if (model && model.secRegulated && model.secRegulated.value) {
            model.secRegulated = model.secRegulated.value;
        } */
        if (model && model.secRegulated && model.secRegulated.id) {
            model.secRegulated = model.secRegulated.id;
        } else if (model && model.secRegulated && model.secRegulated.id === null) {
            model.secRegulated = null;
        }

      /*   else {
			delete model.secRegulated;
		} */

        if (model && model.investorClientBucket && model.investorClientBucket.value) {
            model.investorClientBucket = model.investorClientBucket.value;
        }
        if (model && model.scoreOverall && model.scoreOverall.id) {
            delete model.scoreOverall;
        }
        if (model && model.profileVerificationStatus) {
            delete model.profileVerificationStatus;
        }
        if (model && model.scoreUniqueness && model.scoreUniqueness.id) {
            delete model.scoreUniqueness;
        }
        if (model && model.scoreValue && model.scoreValue.id) {
            delete model.scoreValue;
        }
        if (model && model.scoreReadiness && model.scoreReadiness.id) {
            delete model.scoreReadiness;
        }
        if (model && model.privacy) {
            delete model.privacy;
        }
        if (model && model.permission) {
            delete model.permission;
        }
        if (model && model.publicEquitiesCoveredRange && model.publicEquitiesCoveredRange.value) {
            model.publicEquitiesCoveredRange = model.publicEquitiesCoveredRange.value;
        }

        if (model && model.datasetsize && model.datasetSize.id) {
            model.datasetSize = model.datasetsize.id;
        }

        if (model && model.datasetNumberOfRows && model.datasetNumberOfRows.id) {
            model.datasetNumberOfRows = model.datasetNumberOfRows.id;
        }
console.log('obj',typeof(model.trialPricingModel));
         if (model && model.trialPricingModel && model.trialPricingModel.id) {
            model.trialPricingModel = model.trialPricingModel.id;
        }else if (model && model.trialPricingModel && typeof(model.trialPricingModel)=== 'object' && model.trialPricingModel.id === undefined) {
            model.trialPricingModel = null;
        } else if (model && model.trialPricingModel && model.trialPricingModel ===null) {
            model.trialPricingModel = null;
        }
        /* else {
			delete model.trialPricingModel;
        } */

        if (model && model.trialAgreementType && model.trialAgreementType.id) {
            model.trialAgreementType = model.trialAgreementType.id;
        }
        else if (model && model.trialAgreementType && typeof(model.trialAgreementType)=== 'object' && model.trialAgreementType.id === undefined) {
            model.trialAgreementType = null;
        } else if (model && model.trialAgreementType && model.trialAgreementType ===null) {
            model.trialAgreementType = null;
        }

       /*  else {
			delete model.trialAgreementType;
        } */
        /* if (model && model.pricingModels && model.pricingModels.id) {
            model.pricingModels = model.pricingModels.id;
        } */

        if (model && model.subscriptionModel && model.subscriptionModel.id) {
            model.subscriptionModel = model.subscriptionModel.id;
        }
        if (model && model.subscriptionModel && model.subscriptionModel.id) {
            model.subscriptionModel = model.subscriptionModel.id;
        }
        else if (model && model.subscriptionModel && typeof(model.subscriptionModel)=== 'object' && model.subscriptionModel.id === undefined) {
            model.subscriptionModel = null;
        } else if (model && model.subscriptionModel && model.subscriptionModel ===null) {
            model.subscriptionModel = null;
        }
        /* else {
			delete model.subscriptionModel;
        } */
        if (model && model.subscriptionTimePeriod && model.subscriptionTimePeriod.id) {
            model.subscriptionTimePeriod = model.subscriptionTimePeriod.id;
        }
        else if (model && model.subscriptionTimePeriod && typeof(model.subscriptionTimePeriod)=== 'object' && model.subscriptionTimePeriod.id === undefined) {
            model.subscriptionTimePeriod = null;
        } else if (model && model.subscriptionTimePeriod && model.subscriptionTimePeriod ===null) {
            model.subscriptionTimePeriod = null;
        }
        if (model && model.providerType && model.providerType.id) {
            model.providerType = {
                id: model.providerType.id
            };
        } else if (model && model.providerType && typeof(model.providerType)=== 'object' && model.providerType.id === undefined) {
            model.providerType = null;
        } else if (model && model.providerType && model.providerType ===null) {
            model.providerType = null;
        }
       /*  else {
			delete model.subscriptionTimePeriod;
        } */
        if (model && model.monthlyPriceRange && model.monthlyPriceRange.id) {
            model.monthlyPriceRange = model.monthlyPriceRange.id;
        }
        else if (model && model.monthlyPriceRange && typeof(model.monthlyPriceRange)=== 'object' && model.monthlyPriceRange.id === undefined) {
            model.monthlyPriceRange = null;
        } else if (model && model.monthlyPriceRange && model.monthlyPriceRange ===null) {
            model.monthlyPriceRange = null;
        }
        /* else {
			delete model.monthlyPriceRange;
		} */
        if (model && model.pointInTimeAccuracy && model.pointInTimeAccuracy.id) {
            model.pointInTimeAccuracy = model.pointInTimeAccuracy.id;
        }else if (model && model.pointInTimeAccuracy && typeof(model.pointInTimeAccuracy)=== 'object' && model.pointInTimeAccuracy.id === undefined) {
            model.pointInTimeAccuracy = null;
        } else if (model && model.pointInTimeAccuracy && model.pointInTimeAccuracy ===null) {
            model.pointInTimeAccuracy = null;
        }

        /* else {
			delete model.pointInTimeAccuracy;
        } */
        if (model && model.dataGaps && model.dataGaps.id) {
            model.dataGaps = model.dataGaps.id;
        }else if (model && model.dataGaps && typeof(model.dataGaps)=== 'object' && model.dataGaps.id === undefined) {
            model.dataGaps = null;
        } else if (model && model.dataGaps && model.dataGaps ===null) {
            model.dataGaps = null;
        }

       /*  else {
			delete model.dataGaps;
        } */
        if (model && model.duplicatesCleaned && model.duplicatesCleaned.id) {
            model.duplicatesCleaned = model.duplicatesCleaned.id;
        }else if (model && model.duplicatesCleaned && typeof(model.duplicatesCleaned)=== 'object' && model.duplicatesCleaned.id === undefined) {
            model.duplicatesCleaned = null;
        } else if (model && model.duplicatesCleaned && model.duplicatesCleaned ===null) {
            model.duplicatesCleaned = null;
        }
       /*  else {
			delete model.duplicatesCleaned;
        } */
        if (model && model.includesOutliers && model.includesOutliers.id) {
            model.includesOutliers = model.includesOutliers.id;
        }else if (model && model.includesOutliers && typeof(model.includesOutliers)=== 'object' && model.includesOutliers.id === undefined) {
            model.includesOutliers = null;
        } else if (model && model.includesOutliers && model.includesOutliers ===null) {
            model.includesOutliers = null;
        }

       /*  else {
			delete model.includesOutliers;
        } */
        if (model && model.normalized && model.normalized.id) {
            model.normalized = model.normalized.id;
        }else if (model && model.normalized && typeof(model.normalized)=== 'object' && model.normalized.id === undefined) {
            model.normalized = null;
        } else if (model && model.normalized && model.normalized ===null) {
            model.normalized = null;
        }

        /* else {
			delete model.normalized;
        } */
        // if (model && model['dataLicenseType.id']) {
        //     model.dataLicenseType = model['dataLicenseType.id'];
        // }

        // if (model && model.dataLicenseType) {
        //     model.dataLicenseType = model.dataLicenseType.id;
        // }
        if (model && model.dataLicenseType && model.dataLicenseType.value) {
            model.dataLicenseType = model.dataLicenseType.value;
        }

        if (model && model.dataLicenseType && model.dataLicenseType.id) {
            model.dataLicenseType = model.dataLicenseType.id;
        }

        if (model && model.marketplaceStatus) {
            delete model.marketplaceStatus;
        }

        /* if (model && model.numberOfDataSources && model.numberOfDataSources.value) {
            model.numberOfDataSources = model.numberOfDataSources.value;
        } */

        if (model && model.numberOfDataSources && model.numberOfDataSources.id) {
            model.numberOfDataSources = model.numberOfDataSources.id;
        }  else if (model && model.numberOfDataSources && model.numberOfDataSources.id === null) {
            model.numberOfDataSources = null;
        }
        /* else {
			delete model.numberOfDataSources;
        } */
       /*  if (model && model.pricingModels && model.pricingModels.length > 0) {
            model.pricingModels = model.pricingModels[0].desc;
        } else {
            model.pricingModels = '';
        } */
        if (model && model['mainAssetClass.id']) {
            model.mainAssetClass = model['mainAssetClass.id'];
        }
        if (model && model.mainAssetClass && model.mainAssetClass.value) {
            model.mainAssetClass = model.mainAssetClass.value;
        }
        if (model && model.city === '') {
            model.city = null;
        }
        if (model && model.state === '') {
            model.state = null;
        }
        if (model && model.country === '') {
            model.country = null;
        }
        if (model && model.zipCode === '') {
            model.zipCode = null;
        }
        if (model && model.yearFounded === '') {
            model.yearFounded = null;
        }
        if (model && model.headCount === '') {
            model.headCount = null;
        }
        if (model && model.headcountNumber === '') {
            model.headcountNumber = null;
        }
        if (model && model.publicEquitesCovered === '') {
            model.publicEquitesCovered = null;
        }
        if (model && model.secRegulated === '') {
            model.secRegulated = null;
        }
        if (model && model.yearFirstDataProductLaunched === '') {
            model.yearFirstDataProductLaunched = null;
        }
        if (model && model.numberOfDataSources === '') {
            model.numberOfDataSources = null;
        }
        if(model && model.dataLicenseType && model.dataLicenseType.id === null)
        {
            model.dataLicenseType = null;
        }
        if (model && model.yearExit === '') {
            model.yearExit = null;
        }
        if (model && model.financialPosition === '') {
            model.financialPosition = null;
        }
        if (model && model.clientBase === '') {
            model.clientBase = null;
        }
        if (model && model.headcountNumber === '') {
            model.headcountNumber = null;
        }
        if (model && model.headcountBucket === '') {
            model.headcountBucket = null;
        }
        if (model && model.mainAssetClass === '') {
            model.mainAssetClass = null;
        }
        if (model && model.investorClientBucket === '') {
            model.investorClientBucket = null;
        }
        if (model && model.publicEquitiesCoveredRange === '') {
            model.publicEquitiesCoveredRange = null;
        }
        // if (model && model.summary === '') {
        //     model.summary = null;
        // }
        if (model && model.investor_clients === '') {
            model.investor_clients = null;
        }
        if (model && model.publicCompaniesCovered === '') {
            model.publicCompaniesCovered = null;
        }
        if (model && model.competitiveDifferentiators === '') {
            model.competitiveDifferentiators = null;
        }
        if (model && model.collectionMethodsExplanation === '') {
            model.collectionMethodsExplanation = null;
        }
        if (model && model.dateCollectionRangeExplanation === '') {
            model.dateCollectionRangeExplanation = null;
        }
        if (model && model.outOfSampleData === '') {
            model.outOfSampleData = null;
        }
        if (model && model.lagTime === '') {
            model.lagTime = null;
        }
        if (model && model.deliveryFrequencyNotes === '') {
            model.deliveryFrequencyNotes = null;
        }
        if (model && model.dataUpdateFrequencyNotes === '') {
            model.dataUpdateFrequencyNotes = null;
        }
        if (model && model.trialDuration === '') {
            model.trialDuration = null;
        }
        if (model && model.trialPricingDetails === '') {
            model.trialPricingDetails = null;
        }
        if (model && model.dataLicenseTypeDetails === '') {
            model.dataLicenseTypeDetails = null;
        }
        if (model && model.minimumYearlyPrice === '') {
            model.minimumYearlyPrice = null;
        }
        if (model && model.maximumYearlyPrice === '') {
            model.maximumYearlyPrice = null;
        }
        if (model && model.pricingDetails === '') {
            model.pricingDetails = null;
        }
        if (model && model.sampleOrPanelBiases === '') {
            model.sampleOrPanelBiases = null;
        }
        if (model && model.discussionCleanliness === '') {
            model.discussionCleanliness = null;
        }
        if (model && model.discussionAnalytics === '') {
            model.discussionAnalytics = null;
        }
        if (model && model.potentialDrawbacks === '') {
            model.potentialDrawbacks = null;
        }
        if (model && model.dataRoadmap === '') {
            model.dataRoadmap = null;
        }
        if (model && model.notes === '') {
            model.notes = null;
        }

        let queryString = '?formName=' + formName;
        console.log('model',model);
		return this.http.post(this.providerEditUrl+queryString, model);
    }
    providerAdmin(request?: any): Observable<any> {
        console.log('admin..............', request);
        const model = request;
        // if(request && request.dateCollectionBegan) {
        //     model.dateCollectionBegan = this.datePipe.transform(request.dateCollectionBegan,'yyyy-MM-ddTHH:mm:ss.sssZ');
        // }
        if(request && request.dateCollectionBegan) {
            model.dateCollectionBegan = this.datePipe.transform(request.dateCollectionBegan,'yyyy-MM-dd');
            console.log('1', this.datePipe.transform(request.dateCollectionBegan,'yyyy-MM-dd','en-US'));
            console.log('2', this.datePipe.transform(request.dateCollectionBegan,'yyyy-MM-dd','+1030','en-US'));
        }
        if (request && request.user && request.user.length) {
            console.log(request.user);
            this.newUser = request.user;
            console.log(this.newUser);
            model.user = [];
            this.newUser.forEach(element => {
                if(element?.user) {
                    model.user.push(element.user);
                } else {
                    model.user = this.newUser;
                }
            });
        }
        if (request && request.ownerOrganization) {
            model.ownerOrganization = request.ownerOrganization;
        } else {
            delete model.ownerOrganization;
        }
        if (request && request.mainAssetClass && request.mainAssetClass.value) {
            model.mainAssetClass = request.mainAssetClass.value;
        }
        if (request && request.marketplaceStatus && request.marketplaceStatus.id) {
            model.marketplaceStatus = {
                id: request.marketplaceStatus.id
            };
        } else {
			delete model.marketplaceStatus;
        }
        if (request && request.ieiStatus && request.ieiStatus.id) {
            model.ieiStatus = {
                id: request.ieiStatus.id
            };
        } else {
			delete model.ieiStatus;
        }
        if (request && request.priority && request.priority.value) {
            model.priority = request.priority.value;
        }
        if (request && request.trialPricingModel && request.trialPricingModel.id) {
            model.trialPricingModel = {
                id: request.trialPricingModel.id
            };
        } else {
			delete model.trialPricingModel;
        }
        if (request && request.dataLicenseType && request.dataLicenseType.id) {
            model.dataLicenseType = {
                id: request.dataLicenseType.id
            };
            console.log('DLT', model.dataLicenseType);
        } else {
            delete model.dataLicenseType;
            console.log('DLT Deleted');
        }
        // if (request && request.dataLicenseType) {
        //     if(request.dataLicenseType.hasOwnProperty('id')){
        //         model.dataLicenseType = request.dataLicenseType;
        //     }else{
        //         model.dataLicenseType = {
        //             id: request.dataLicenseType
        //         };
        //     }
        //    } else {
        //        delete model.dataLicenseType;
        //    }
        if (request && request.dataUpdateFrequency && request.dataUpdateFrequency.value) {
            model.dataUpdateFrequency = request.dataUpdateFrequency.value;
            console.log('DUF', model.dataUpdateFrequency);
        }
        if (request && request.dataGaps && request.dataGaps.id) {
            model.dataGaps = {
                id: request.dataGaps.id
            };
        } else {
            //console.log('Gaps Deleted');
			delete model.dataGaps;
        }
        if (request && request.duplicatesCleaned && request.duplicatesCleaned.id) {
            model.duplicatesCleaned = {
                id: request.duplicatesCleaned.id
            };
        } else {
			delete model.duplicatesCleaned;
        }
        if (request && request.numberOfDataSources && request.numberOfDataSources.id) {
            model.numberOfDataSources = {
                id: request.numberOfDataSources.id
            };
        } else {
			delete model.numberOfDataSources;
        }
        if (request && request.includesOutliers && request.includesOutliers.id) {
            model.includesOutliers = {
                id: request.includesOutliers.id
            };
        } else {
			delete model.includesOutliers;
        }
        if (request && request.investorClientBucket && request.investorClientBucket.value) {
            model.investorClientBucket = request.investorClientBucket.value;
        }
        if (request && request.normalized && request.normalized.id) {
            model.normalized = {
                id: request.normalized.id
            };
        } else {
			delete model.normalized;
        }
        if (request && request.pointInTimeAccuracy && request.pointInTimeAccuracy.id) {
            model.pointInTimeAccuracy = {
                id: request.pointInTimeAccuracy.id
            };
        } else {
            //console.log('PIT Deleted');
			delete model.pointInTimeAccuracy;
        }
        if (request && request.secRegulated && request.secRegulated.id) {
            model.secRegulated = {
                id: request.secRegulated.id
            };
        } else {
			delete model.secRegulated;
        }
        if (request && request.trialAgreementType && request.trialAgreementType.id) {
            model.trialAgreementType = {
                id: request.trialAgreementType.id
            };
        } else {
			delete model.trialAgreementType;
        }
        if (request && request.subscriptionModel && request.subscriptionModel.id) {
            model.subscriptionModel = {
                id: request.subscriptionModel.id
            };
        } else {
			delete model.subscriptionModel;
        }
        if (request && request.subscriptionTimePeriod && request.subscriptionTimePeriod.id) {
            model.subscriptionTimePeriod = {
                id: request.subscriptionTimePeriod.id
            };
        } else {
			delete model.subscriptionTimePeriod;
        }
        if (request && request.monthlyPriceRange && request.monthlyPriceRange.id) {
            model.monthlyPriceRange = {
                id: request.monthlyPriceRange.id
            };
        } else {
			delete model.monthlyPriceRange;
		}
        if (request && request.scoreInvestorsWillingness && request.scoreInvestorsWillingness.id) {
            model.scoreInvestorsWillingness = {
                id: request.scoreInvestorsWillingness.id
            };
        } else {
			delete model.scoreInvestorsWillingness;
		}
        if (request && request.scoreOverall && request.scoreOverall.id) {
            model.scoreOverall = {
                id: request.scoreOverall.id
            };
        } else {
			delete model.scoreOverall;
        }
        if (request && request.pricingModel && request.pricingModel.id) {
            model.pricingModel = {
                id: request.pricingModel.id
            };
        } else {
			delete model.pricingModel;
		}
        if (request && request.scoreReadiness && request.scoreReadiness.id) {
            model.scoreReadiness = {
                id: request.scoreReadiness.id
            };
        } else {
			delete model.scoreReadiness;
		}
        if (request && request.scoreUniqueness && request.scoreUniqueness.id) {
            model.scoreUniqueness = {
                id: request.scoreUniqueness.id
            };
        } else {
			delete model.scoreUniqueness;
        }
        if (request && request.scoreValue && request.scoreValue.id) {
            model.scoreValue = {
                id: request.scoreValue.id
            };
        } else {
			delete model.scoreValue;
        }
        if (request && request.providerType && request.providerType.id) {
            model.providerType = {
                id: request.providerType.id
			};
        } else {
			delete model.providerType;
        }
        if (request && request.datasetSize && request.datasetSize.value) {
            model.datasetSize = request.datasetSize.value;
        }
        if (request && request.datasetNumberOfRows && request.datasetNumberOfRows.value) {
            model.datasetNumberOfRows = request.datasetNumberOfRows.value;
        }
        if (request && request.deliveryFormats) {
            model.dataProvidersDeliveryFormats = request.deliveryFormats;
        }
        if (request && request.geographicalFoci) {
            model.geographicalFoci = request.geographicalFoci;
        }
        if (request && request.strategies) {
            model.strategies = request.strategies;
        }
        if (request && request.investorTypes) {
            model.investorTypes = request.investorTypes;
        }
        if (request && request.industries) {
            model.providerIndustries = request.industries;
        }
        if (request && request.researchMethods) {
            model.researchMethodsCompleted = request.researchMethods;
        }
        if (request && request.deliveryMethods) {
            model.dataProvidersDeliveryMethods = request.deliveryMethods;
        }
        if (request && request.website === '') {
            model.website = null;
        }
        if (request && request.uniqueValueProps === '') {
            model.uniqueValueProps = null;
        }
        if (request && request.notes === '') {
            model.notes = null;
        }
        if (request && request.dataUpdateFrequencyNotes === '') {
            model.dataUpdateFrequencyNotes = null;
        }
        if (request && request.dateCollectionRangeExplanation === '') {
            model.dateCollectionRangeExplanation = null;
        }
        if (request && request.useCasesOrQuestionsAddressed === '') {
            model.useCasesOrQuestionsAddressed = null;
        }
        if (request && request.collectionMethodsExplanation === '') {
            model.collectionMethodsExplanation = null;
        }
        if (request && request.potentialDrawbacks === '') {
            model.potentialDrawbacks = null;
        }
        if (request && request.summary === '') {
            model.summary = null;
        }
        if (request && request.investor_clients === '') {
            model.investor_clients = null;
        }
        if (request && request.publicCompaniesCovered === '') {
            model.publicCompaniesCovered = null;
        }
        if (request && request.sampleOrPanelSize === '') {
            model.sampleOrPanelSize = null;
        }
        if (request && request.dataLicenseTypeDetails === '') {
            model.dataLicenseTypeDetails = null;
        }
        if (request && request.dataRoadmap === '') {
            model.dataRoadmap = null;
        }
        if (request && request.deliveryFrequencyNotes === '') {
            model.deliveryFrequencyNotes = null;
        }
        if (request && request.legalAndComplianceIssues === '') {
            model.legalAndComplianceIssues = null;
        }
        if (request && request.trialDuration === '') {
            model.trialDuration = null;
        }
        if (request && request.trialPricingDetails === '') {
            model.trialPricingDetails = null;
        }
        if (request && request.lagTime === '') {
            model.lagTime = null;
        }
        if (request && request.competitiveDifferentiators === '') {
            model.competitiveDifferentiators = null;
        }
        if (request && request.discussionAnalytics === '') {
            model.discussionAnalytics = null;
        }
        if (request && request.discussionCleanliness === '') {
            model.discussionCleanliness = null;
        }
        if (request && request.sampleOrPanelBiases === '') {
            model.sampleOrPanelBiases = null;
        }
        if (request && request.dataSourcesDetails === '') {
            model.dataSourcesDetails = null;
        }
        if (request && request.pricingDetails === '') {
            model.pricingDetails = null;
        }
        if (request && request.minimumYearlyPrice === '') {
            model.minimumYearlyPrice = null;
        }
        if (request && request.maximumYearlyPrice === '') {
            model.maximumYearlyPrice = null;
        }
        if (request && request.outOfSampleData === '') {
            model.outOfSampleData = null;
        }
        if (request && request.pricingModels) {
            model.dataProvidersPricingModels = request.pricingModels;
        }
        if (request && request.assetClasses) {
            model.dataProvidersAssetClasses = request.assetClasses;
        }
        if (request && request.categories) {
            model.dataProviderCategories = request.categories;
        }
        if (request && request.features) {
            model.dataProviderFeatures = request.features;
        }
        if (request && request.providerTags) {
            model.dataProviderTags = request.providerTags;
        }
        if (request && request.relevantSectors) {
            model.dataProvidersRelevantSectors = request.relevantSectors;
        }
        if (request && request.securityTypes) {
            model.dataProvidersSecurityTypes = request.securityTypes;
        }
        if (request && request.sources) {
            model.dataProviderSources = request.sources;
        }
        if (request && request.userPermission) {
            model.userPermission = request.userPermission;
        }
        if (request && request.logo) {
            delete model['logo'];
        }
        if(request && request.distributionPartners) {
            model.distributionPartners = request.distributionPartners;
        }
        if(request && request.providerPartners){
            model.providerPartners = request.providerPartners;
        }
        if(request && request.consumerPartners){
            model.consumerPartners = request.consumerPartners;
        }
        if(request && request.complementaryProviders){
            delete model.complementaryProviders;
        }
        if(request && request.competitors){
            model.competitors = request.competitors;
        }
        if(request && request.profileVerificationStatus){
            delete model.profileVerificationStatus;
        }
       /*  if (request && request.privacy && request.privacy.lookupcode) {
            model.privacy = request.privacy.lookupcode;
        } */
        if (request && request.permission && request.permission.name) {
            model.permission = request.permission.name;
        }
        if (request && request.privacy && request.privacy.id === 582) {
            model.privacy = 'Public';
        } else if(request && request.privacy && request.privacy.id === 581) {
            model.privacy = 'Private';
        } else if(request && request.privacy && request.privacy.id === 584) {
            model.privacy = 'Team';
        } else if(request && request.privacy && request.privacy.id === 583) {
            model.privacy = 'Direct';
        }
       /*  if (request && request.recordID) {
            delete model['recordID'];
        } */
        return this.http.put(this.providerAdminUrl, model);
    }

    addProviderAdmin(request?: any): Observable<any> {
		console.log('Addadmin..............', request);
        const model = request;
        console.log('model.....', model);
        /* if(request && request.deliveryFrequencies) {
            console.log('Inside');
            delete request.deliveryFrequencies;
        } */
        delete model.deliveryFrequencies;
        delete model.dataUpdateFrequency;
		if(request && request.ownerOrganization && (!request.ownerOrganization.city || !request.ownerOrganization.state || !request.ownerOrganization.country || !request.ownerOrganization.zipCode || !request.ownerOrganization.yearFounded || !request.ownerOrganization.headCount)){
			delete request.ownerOrganization;
			//console.log("...deleted...");
		}
		if (request && request.providerType && request.providerType.id) {
            model.providerType = {
                id: request.providerType.id
			};
        }else {
			//console.log('deleted main asset class....');
			delete model.providerType;
		}
        if (request && request.mainAssetClass && request.mainAssetClass.id) {
            model.mainAssetClass = {
                id: request.mainAssetClass.id
			};
        }else {
			//console.log('deleted main asset class....');
			delete model.mainAssetClass;
		}
		if (request && request.scoreInvestorsWillingness && request.scoreInvestorsWillingness.id) {
            model.scoreInvestorsWillingness = {
                id: request.scoreInvestorsWillingness.id
            };
        }else {
			delete model.scoreInvestorsWillingness;
		}
        if (request && request.scoreOverall && request.scoreOverall.id) {
            model.scoreOverall = {
                id: request.scoreOverall.id
            };
        }else {
			delete model.scoreOverall;
		}
        if (request && request.scoreReadiness && request.scoreReadiness.id) {
            model.scoreReadiness = {
                id: request.scoreReadiness.id
            };
        }else {
			delete model.scoreReadiness;
		}
        if (request && request.scoreUniqueness && request.scoreUniqueness.id) {
            model.scoreUniqueness = {
                id: request.scoreUniqueness.id
            };
        }else {
			delete model.scoreUniqueness;
		}
        if (request && request.scoreValue && request.scoreValue.id) {
            model.scoreValue = {
                id: request.scoreValue.id
            };
        }else {
			delete model.scoreValue;
		}
		if (request && request.pricingModels && request.pricingModels.id) {
            model.pricingModels = {
                id: request.pricingModels.id
            }
        } else {
            delete model.pricingModels;
        }
        return this.http.post(this.providerAdminUrl, model);
    }

    addOrganizationAdmin(request?: any): Observable<any> {
		console.log('Addadmin..............', request);
        const model = request;
        console.log('model.....', model);
       if(request && request.researchMethodsCompleted) {
           model.researchMethodsCompleted = request.researchMethodsCompleted;
       }
       if (request && request.headcountBucket && request.headcountBucket.id) {
           model.headcountBucket = {
               id: request.headcountBucket.id
            };
       } else {
           delete model.headcountBucket;
       }
       if (request && request.ieiStatus && request.ieiStatus.id) {
           model.ieiStatus = {
               id: request.ieiStatus.id
           };
       } else {
           delete model.ieiStatus;
       }
       if (request && request.marketplaceStatus && request.marketplaceStatus.id) {
           model.marketplaceStatus = {
               id: request.marketplaceStatus.id
           };
        } else {
            delete model.marketplaceStatus;
        }
        return this.http.post(this.organizationAdminUrl, model);
    }

    updateOrganizationAdmin(request?: any,action?: string, basicProvider?:any): Observable<any> {
		console.log('Addadmin..............', request);
        const model = request;
        const ProviderId = basicProvider;
        console.log('model.....', model);
       if(request && request.researchMethodsCompleted) {
           model.researchMethodsCompleted = request.researchMethodsCompleted;
       }

       if (request && request.headcountBucket && request.headcountBucket !== null && !request.headcountBucket.id) {
            model.headcountBucket = {
                id: request.headcountBucket
            };
            console.log('Headcount',model.headcountBucket);
        } else if(request && request.headcountBucket && request.headcountBucket.id && request.headcountBucket.id.id) {
            model.headcountBucket = {
                id: request.headcountBucket.id && request.headcountBucket.id.id ? request.headcountBucket.id.id : null
            };
            console.log('test',model.headcountBucket);
        } else {
            if (request.headcountBucket && request.headcountBucket.id && request.headcountBucket.id !== null && request.headcountBucket.id !== '') {
              //  model.headcountBucket.id =  request.headcountBucket;
                model.headcountBucket = {
                    id: request.headcountBucket.id
                };
             console.log('Headcount ID',model.headcountBucket);
            } else {
                delete model.headcountBucket;
                console.log('Deleted Headcount',model.headcountBucket);
            }
        }
        if (request && request.ieiStatus && request.ieiStatus != null && !request.ieiStatus.id) {
            model.ieiStatus = {
                id: request.ieiStatus
            }
            console.log('ieiStatus',model.ieiStatus);
        } else if(request && request.ieiStatus && request.ieiStatus.id && request.ieiStatus.id.id) {
            model.ieiStatus = {
                id: request.ieiStatus.id && request.ieiStatus.id.id ? request.headcountBucket.id.id : null
            };
            console.log('test',model.headcountBucket);
        }else {
            if (request.ieiStatus && request.ieiStatus.id && request.ieiStatus.id != null && request.ieiStatus.id != '') {
                model.ieiStatus = {
                    id: request.ieiStatus.id
                };
             } else {
                delete model.ieiStatus;
            }
        }
       if (request && request.marketplaceStatus && request.marketplaceStatus.id) {
           model.marketplaceStatus = {
               id: request.marketplaceStatus.id
           };
        } else {
            delete model.marketplaceStatus;
        }
        switch (action) {
            case 'save':
                return this.http.put(this.organizationAdminUrl, model);
            break;

            case 'submit':
                return this.http.put('api/data-organizations-providermapping', model, {
                    params: {
                        ProviderId: ProviderId
                    }
                });
            break;

            default:
                return this.http.put(this.organizationAdminUrl, model);
            break;
        }
    }


    addLinkAdmin(request?: any): Observable<any> {
		console.log('Addadmin..............', request);
        const model = request;
        console.log('model.....', model);
        if (request && request.linkCategory) {
            delete model.linkCategory;
        }
        if (request && request.dataLinksCategory) {
            model.dataLinksCategory = request.dataLinksCategory;
        }
        return this.http.post(this.linkAdminUrl, model);
    }

    updateLinkAdmin(request?: any): Observable<any> {
		console.log('Update admin..............', request);
        const model = request;
        console.log('model.....', model);
        if (request && request.linkCategory) {
            delete model.linkCategory;
        }
        if (request && request.dataLinksCategory) {
            model.dataLinksCategory = request.dataLinksCategory;
        }
        return this.http.put(this.linkAdminUrl, model);
    }

    getProviderEdit(recordID?: string): Observable<DataProvider> {
        return this.http.get('api/provider_profile_edit/' + recordID);
    }

    getProviderId(recordID?: string): Observable<DataProvider> {
        return this.http.get('api/providers/' + recordID);
    }

	elasticSearch(req?: any): Observable<HttpResponse<any>> {
		const options = createRequestOption(req);
		return this.http.post(this.resourceElasticSearchUrl, req, {observe: 'response'});
	}

	getTotal(): Observable<HttpResponse<any>> {
		return this.http.post(this.resourceElasticSearchUrl, {
			size: 0,
			aggs: {

			}
		}, {observe: 'response'});
    }

     tagList(list): Observable<any> {
         console.log(list);
        return this.http.post(this.dataProviderTagUrl, {
                'tagProviders': [
                    {
                        id: list.providerID
                      }
                    ],
                  'providerTag': list.tag,
                  'privacy': 'Private'
        });
    }
    tagMultiselect(name, id): Observable<any> {
       return this.http.post(this.dataProviderTagUrl, {
               'tagProviders': [
                   {
                       id: id
                     }
                   ],
                 'providerTag': name,
                 'privacy': 'Public'
       });
   }
    newTagList(newtag): Observable<any> {
        console.log(newtag);
        return this.http.post(this.providerNewTagUrl, {}, {
            params: {
                tagId: newtag.tagId,
                providerId: newtag.providerId
            }
        });
    }
    removeTagList(newtag): Observable<any> {
        console.log(newtag);
        return this.http.post(this.providerRemoveTagUrl, {}, {
            params: {
                tagId: newtag.tagId,
                providerId: newtag.providerId
            }
        });
    }

    onUpload(recordID? : string, fileUploads?: any): Observable<any> {
        console.log('hello');
        const formData = new FormData();
        formData.append('fileUploads', fileUploads);
        return this.http.post(this.providerLogo, formData, {
            params: {
                providerId: recordID
            }
        });
    }

    onRelevantFileUpload(recordID? : string, fileUploads?: any[]): Observable<any> {
        console.log('hello');
        const formData = new FormData();
        for (let i = 0; i < fileUploads.length; i++) {
            formData.append('fileUploads', fileUploads[i]);
        }
        // formData.append('fileUploads', fileUploads);
        return this.http.post(this.resourceFileUpload, formData, {
            params: {
                providerId: recordID
            }
        });
    }

    onResourceFileUpload(id? : any, fileUploads?: any): Observable<any> {
        console.log('hello');
        const formData = new FormData();
        formData.append('fileUploads', fileUploads);
        return this.http.post(this.resourceSingeFile, formData, {
            params: {
                providerId: id
            }
        });
    }

	followAction(id?: number): Observable<any> {
		return this.http.get(`${this.resourceFollowUrl}=${id}`, {});
	}
    followActionTag(id?: number): Observable<any> {
		return this.http.post(`${this.resourceFollowUrlTag}=${id}`, {});
	}
	expandAction(id?: number): Observable<any> {
		return this.http.post(`${this.resourceExpandUrl}=${id}`, '');
	}

	confirmExpandAction(id?: number): Observable<any> {
		return this.http.post(`${this.resourceConfirmExpandUrl}=${id}`, '');
	}

	cancelExpandAction(id?: number): Observable<any> {
		return this.http.post(`${this.resourceCancelExpandUrl}=${id}`, '');
	}

	unlockAction(id?: number): Observable<any> {
		return this.http.post(`${this.resourceUnlockUrl}=${id}`, '');
	}

	confirmUnlockAction(id?: number): Observable<any> {
		return this.http.post(`${this.resourceConfirmUnlockUrl}=${id}`, '');
	}

	cancelUnlockAction(id?: number): Observable<any> {
		return this.http.post(`${this.resourceCancelUnlockUrl}=${id}`, '');
	}

	cancelMoreInformationAction(id?: number, interest?): Observable<any> {
		return this.http.get(`${this.resourceCancelMoreInfoUrl}=${id}&interestType=${interest}`, {});
    }

    getAllDataCategories(): Observable<any> {
        return this.http.get('api/all-data-categories');
    }

    getCommentsByID(id: number, privacy?: number): Observable<any> {
        let params = 'id=' + id;
        if (privacy) {
            params += '&privacy=' + privacy;
        }
        return this.http.get('api/comment?' + params, {});
    }

	private convert(dataProvider: DataProvider): DataProvider {
		const copy: DataProvider = Object.assign({}, dataProvider);
		copy.createdDate = this.dateUtils
			.convertLocalDateToServer(dataProvider.createdDate);

        copy.updatedDate = this.dateUtils.toDate(dataProvider.updatedDate);
        return copy;
    }

    sendChangeSignal(change: any) {
        console.log('Change',change);
        this.changeLogoUpload.next(change);
    }

    sendClearSearch(change: any) {
        console.log('Change',change);
        this.clearHeaderSearch.next(change);
    }
}

