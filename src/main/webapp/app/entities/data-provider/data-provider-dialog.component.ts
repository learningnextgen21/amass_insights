import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { DataProvider } from './data-provider.model';
import { DataProviderPopupService } from './data-provider-popup.service';
import { DataProviderService } from './data-provider.service';
import { DataCategory, DataCategoryService } from '../data-category';
import { DataOrganization, DataOrganizationService } from '../data-organization';
import { DataIndustry, DataIndustryService } from '../data-industry';
import { DataProviderType, DataProviderTypeService } from '../data-provider-type';
import { LookupCode, LookupCodeService } from '../lookup-code';
import { DataFeature, DataFeatureService } from '../data-feature';
import { DataArticle, DataArticleService } from '../data-article';
import { DataProviderTag, DataProviderTagService } from '../data-provider-tag';
import { DataSource, DataSourceService } from '../data-source';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-data-provider-dialog',
    templateUrl: './data-provider-dialog.component.html'
})
export class DataProviderDialogComponent implements OnInit {

    dataProvider: DataProvider;
    isSaving: boolean;

    datacategories: DataCategory[];

    dataorganizations: DataOrganization[];

    dataindustries: DataIndustry[];

    dataprovidertypes: DataProviderType[];

    lookupcodes: LookupCode[];

    dataproviders: DataProvider[];

    datafeatures: DataFeature[];

    dataarticles: DataArticle[];

    dataprovidertags: DataProviderTag[];

    datasources: DataSource[];
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private dataProviderService: DataProviderService,
        private dataCategoryService: DataCategoryService,
        private dataOrganizationService: DataOrganizationService,
        private dataIndustryService: DataIndustryService,
        private dataProviderTypeService: DataProviderTypeService,
        private lookupCodeService: LookupCodeService,
        private dataFeatureService: DataFeatureService,
        private dataArticleService: DataArticleService,
        private dataProviderTagService: DataProviderTagService,
        private dataSourceService: DataSourceService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.dataCategoryService.query()
            .subscribe((res) => { this.datacategories = res; }, (res) => this.onError(res));
        this.dataOrganizationService.query()
            .subscribe((res) => { this.dataorganizations = res; }, (res) => this.onError(res));
        this.dataIndustryService.query()
            .subscribe((res) => { this.dataindustries = res; }, (res) => this.onError(res));
        this.dataProviderTypeService.query()
            .subscribe((res) => { this.dataprovidertypes = res.body; }, (res) => this.onError(res));
        this.lookupCodeService.query()
            .subscribe((res) => { this.lookupcodes = res.body; }, (res) => this.onError(res));
        this.dataProviderService.query()
            .subscribe((res) => { this.dataproviders = res.body; }, (res) => this.onError(res));
        this.dataFeatureService.query()
            .subscribe((res) => { this.datafeatures = res; }, (res) => this.onError(res));
        this.dataArticleService.query()
            .subscribe((res) => { this.dataarticles = res; }, (res) => this.onError(res));
        this.dataProviderTagService.query()
            .subscribe((res) => { this.dataprovidertags = res.body; }, (res) => this.onError(res));
        this.dataSourceService.query()
            .subscribe((res) => { this.datasources = res.body; }, (res) => this.onError(res));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dataProvider.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dataProviderService.update(this.dataProvider));
        } else {
            this.subscribeToSaveResponse(
                this.dataProviderService.create(this.dataProvider));
        }
    }

    private subscribeToSaveResponse(result: Observable<DataProvider>) {
        result.subscribe((res: DataProvider) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DataProvider) {
        this.eventManager.broadcast({ name: 'dataProviderListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackDataCategoryById(index: number, item: DataCategory) {
        return item.id;
    }

    trackDataOrganizationById(index: number, item: DataOrganization) {
        return item.id;
    }

    trackDataIndustryById(index: number, item: DataIndustry) {
        return item.id;
    }

    trackDataProviderTypeById(index: number, item: DataProviderType) {
        return item.id;
    }

    trackLookupCodeById(index: number, item: LookupCode) {
        return item.id;
    }

    trackDataProviderById(index: number, item: DataProvider) {
        return item.id;
    }

    trackDataFeatureById(index: number, item: DataFeature) {
        return item.id;
    }

    trackDataArticleById(index: number, item: DataArticle) {
        return item.id;
    }

    trackDataProviderTagById(index: number, item: DataProviderTag) {
        return item.id;
    }

    trackDataSourceById(index: number, item: DataSource) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-data-provider-popup',
    template: ''
})
export class DataProviderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataProviderPopupService: DataProviderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dataProviderPopupService
                    .open(DataProviderDialogComponent as Component, params['id']);
            } else {
                this.dataProviderPopupService
                    .open(DataProviderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
