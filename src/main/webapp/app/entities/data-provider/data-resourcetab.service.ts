import { Injectable } from '@angular/core';
import { Subject} from 'rxjs';

@Injectable()
export class DataResourceTabService {

    private messageSource = new Subject<String>();
      messageResource$ = this.messageSource.asObservable();
      private messageSourceTab = new Subject<String>();
      messageResourceTAb$ = this.messageSourceTab.asObservable();
    constructor()
    {

    }
    sendMessage(message: String)
    {
        this.messageSource.next(message);
    }
    sendResourceTab(message: String)
    {
        this.messageSourceTab.next(message);
    }
}