import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
    DatamarketplaceSharedModule,
    FormlyEditorFieldComponent,
    FormlyAutoCompleteFieldComponent,
    FormlyMultiSelectFieldComponent,
    FormlyDropdownFieldComponent,
    FormlyMaskFieldComponent,
    FormlySliderFieldComponent,
    FormlyRadioButtonFieldComponent,
    FormlyTextAreaFieldComponent,
} from '../../shared';
import { FormlyFieldButton } from '../../shared/custom-form-fields/button-field.component';
import {
    DataProviderService,
    DataProviderPopupService,
    DataProviderComponent,
    DataProviderDetailComponent,
    DataProviderEditComponent,
    DataProviderDialogComponent,
    DataProviderPopupComponent,
    DataProviderDeletePopupComponent,
    DataProviderDeleteDialogComponent,
    dataProviderRoute,
    dataProviderPopupRoute,
    TreeFlatOverviewExampleComponent
} from './';
import {DataProviderAdminFormComponent} from './data-provider-admin-form.component';
import { DataProviderLinkAdminFormComponent } from './data-provider-link-admin-form.component';
import { FormlyModule } from '@ngx-formly/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyHorizontalWrapperComponent } from 'app/entities/data-provider/horizontal-wrapper';
import { UppyModule } from 'app/uppy/uppy.module';
import { UppyService } from 'app/uppy';
import { GenericProfileModule } from 'app/shared/generic-profile';
// import { LoadingIndicatorService, LoadingIndicatorInterceptor} from '../../loader/loader.service';

const ENTITY_STATES = [...dataProviderRoute, ...dataProviderPopupRoute];
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { MatMenuModule } from '@angular/material/menu';
import { CommentsModule } from '../../comments';
import { InactivityTimerComponent, GenericConfirmDialogComponent } from 'app/shared/auth/inactivity-timer.component';
import { from } from 'rxjs';
import { FormlyDatePickerFieldComponent } from 'app/shared/custom-form-fields/datepicker-field.component';
import { FormlyFileUploadFieldComponent } from 'app/shared/custom-form-fields/fileupload-form-field.component';
import { DataOrganizationAdminFormComponent } from './data-organization-admin-form.component';
import { UppyDialogComponent } from 'app/uppy/uppy/uppy-dialog.component';
import { FormlyFieldCustomInput } from 'app/shared/custom-form-fields/input-form-field.component';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import { DataResourceTabService } from './data-resourcetab.service';
import { DataProviderSearchService } from './data-provider-search.service';
import { DataProviderFilterService } from './data-provider-filter.service';
import { FormlyFieldCheckboxComponent } from 'app/shared/custom-form-fields/checkbox-form-field.component';
import { FormlyFieldsetFormComponent } from 'app/shared/custom-form-fields/fieldset-form-field.component';

export function maxlengthValidationMessage(err, field) {
    return `This field has a maximum length of  ${field.templateOptions.maxLength}  characters. Please shorten this text to fit under this length.`;
  }
@NgModule({
    imports: [
        CommentsModule,
        DatamarketplaceSharedModule,
        // HttpClientModule,
        ReactiveFormsModule,
        MatMenuModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        FormlyModule.forRoot({
            wrappers: [{ name: 'form-field-horizontal', component: FormlyHorizontalWrapperComponent  }],
            validationMessages: [
                { name: 'required', message: 'This field is required' },
                { name: 'maxlength', message: maxlengthValidationMessage },
            ],
            types: [{
                name: 'inputEditor', component: FormlyEditorFieldComponent
            }, {
                name: 'autocomplete', component: FormlyAutoCompleteFieldComponent
            }, {
                name: 'multiselect', component: FormlyMultiSelectFieldComponent
            }, {
                name: 'dropdown', component: FormlyDropdownFieldComponent
            }, {
                name: 'mask', component: FormlyMaskFieldComponent
            }, {
                name: 'slider', component: FormlySliderFieldComponent
            }, {
                name: 'radioButton', component: FormlyRadioButtonFieldComponent
            }, {
                name: 'textarea', component: FormlyTextAreaFieldComponent
            }, {
                name: 'datepicker', component: FormlyDatePickerFieldComponent
            }, {
                name: 'fileUpload', component: FormlyFileUploadFieldComponent
            }, {
                name: 'button', component: FormlyFieldButton
            }, {
                name: 'input', component: FormlyFieldCustomInput
            }, {
                name: 'checkbox', component: FormlyFieldCheckboxComponent
            }, {
                name: 'fieldset', component: FormlyFieldsetFormComponent
            }]
        }),
        FormlyBootstrapModule,
        UppyModule,
        GenericProfileModule,
        ShareButtonModule,
        ShareButtonsModule.withConfig({
            debug: true
        }),
        ProgressSpinnerModule
    ],
    declarations: [
        DataProviderComponent,
        DataProviderDetailComponent,
        DataProviderEditComponent,
        DataProviderDialogComponent,
        DataProviderDeleteDialogComponent,
        DataProviderPopupComponent,
        DataProviderDeletePopupComponent,
        TreeFlatOverviewExampleComponent,
        DataProviderAdminFormComponent,
        DataOrganizationAdminFormComponent,
        UppyDialogComponent,
        InactivityTimerComponent,
        GenericConfirmDialogComponent
    ],
    entryComponents: [
        DataProviderComponent,
        DataProviderDialogComponent,
        DataProviderPopupComponent,
        DataProviderDeleteDialogComponent,
        DataProviderDeletePopupComponent,
        TreeFlatOverviewExampleComponent,
        DataOrganizationAdminFormComponent,
        UppyDialogComponent,
        GenericConfirmDialogComponent
    ],
    exports: [
        GenericConfirmDialogComponent
    ],
    providers: [
        DataProviderService,
        DataProviderPopupService,
        DataResourceTabService,
        DataProviderSearchService,
        DataProviderFilterService,
        UppyService
        // { provide: HTTP_INTERCEPTORS, useClass: MyHttpLogInterceptor, multi: true }
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceDataProviderModule {}
