import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { DomSanitizer } from '@angular/platform-browser';

import { DataProvider } from './data-provider.model';
import { DataProviderService } from './data-provider.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, GenericFilterListModel, GenericFilterModel, ShareButtonsModel, StateStorageService, GenericFilterComponent } from '../../shared';
import { LoaderService } from '../../loader/loaders.service';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
const bob = require('elastic-builder');
import { ApplyAccountComponent } from '../../account/apply/apply.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SigninDialogComponent } from '../../dialog/signindialog.component';

@Component({
	selector: 'jhi-data-feature-provider',
	templateUrl: './data-feature-provider.component.html',
	styleUrls: ['../data-article/data-article.component.css', './data-feature-provider.component.scss', '../../shared/share-buttons/share-buttons.scss']
})
export class DataFeatureProviderComponent implements OnInit, OnDestroy {

    /* dataFeatureProvider: DataProvider[];
    dataRecentProvider: DataProvider[]; */
	currentAccount: any;
	eventSubscriber: Subscription;
	itemsPerPage: number;
	links: any;
	page: any;
	predicate: any;
	subscription: any;
	reverse: any;
	totalItems: number;
    params: any;
	paramFilter: string;
    paramID: string;
    loader: any;
    loaderInfinite: any;
    filters: GenericFilterModel[];
    isAdmin: boolean;
    recordID: string;

    isSortByBarCollapsed: boolean;
    currentUrl: any;
    shareButtons: ShareButtonsModel[];
    emailText: string;
    logoutUsers: boolean;
    logoutUserClick: boolean;
    mdDialogRef: MatDialogRef<any>;
    sortLabel: any;
    relevantProviderLogo: any;
    dataArticle: any;
    progressBarColor = 'primary';
	progressBarMode = 'determinate';
    @ViewChild(GenericFilterComponent) genericFilter;
    @Input() dataFeatureProvider: any;
    @Input() dataRecentProvider: any;

	constructor(
		private dataProviderService: DataProviderService,
		private alertService: JhiAlertService,
		private dataUtils: JhiDataUtils,
		private eventManager: JhiEventManager,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private principal: Principal,
        public _DomSanitizer: DomSanitizer,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private loaderService: LoaderService,
        public dialog: MatDialog,
	) {
        /* this.dataFeatureProvider = [];
        this.dataRecentProvider = []; */
		this.itemsPerPage = ITEMS_PER_PAGE;
		this.page = 0;
		this.links = {
			last: 0
		};
		this.predicate = 'id';
		this.reverse = true;
        this.isSortByBarCollapsed = true;
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
	}

	load() {
    
       /*  this.dataProviderService.featureProviderData('featured').subscribe(data => {
            console.log(data);
           // this.dataFeatureHome = data['hits'].total ? data['hits'].hits[0]._source : null;
            if (data['hits'] && data['hits'].hits) {
                for (let i = 0; i < data['hits'].hits.length; i++) {
                    const featureProvider = data['hits'].hits[i]._source;
                    this.dataFeatureProvider.push(featureProvider);
                }
            }
            this.loaderService.display(false);
        });
        this.dataProviderService.featureProviderData('recent').subscribe(data => {
            console.log(data);
           // this.dataFeatureHome = data['hits'].total ? data['hits'].hits[0]._source : null;
            if (data['hits'] && data['hits'].hits) {
                for (let i = 0; i < data['hits'].hits.length; i++) {
                    const recentProvider = data['hits'].hits[i]._source;
                    this.dataRecentProvider.push(recentProvider);
                }
            }
            this.loaderService.display(false);
        }); */
        const queryBody = {};
		if ( this.subscription ) {
            this.subscription.unsubscribe();
        }
	}

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }
    shreDropDown(events: any) {
         this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
         /* this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        }); */
     }
     providerFollow(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        });
    }
    featureSignup() {
       /*  this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        }); */
        if(!this.isAuthenticated())
        {
            this.router.navigateByUrl('unproviders');

        }
    }
    featureSignups()
    {
         this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        }); 
    }
    featureRequest() {
      /*   this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        }); */
        if(!this.isAuthenticated())
        {
            this.router.navigateByUrl('unproviders');

        }
    }
	ngOnInit() {
        this.dialog.closeAll();
        this.currentUrl = this.router.url;
        this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
        /* this.loaderService.display(true); */
        this.loader = false;
		this.activatedRoute.params.subscribe(params => {
            this.params = params;
            /* if (params['recordID'] && this.isAuthenticated()) {
                this.load(params['recordID']);
            } else {
                this.router.navigate(['accessdenied']);
                this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
                    width: '400px'
                });
            } */
            this.load();
        });
    }
    unrestrictedUsers() {
           /*  this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            }); */
            if(!this.isAuthenticated())
            {
                this.router.navigateByUrl('unproviders');
    
            }
    }

	ngOnDestroy() {
        if (this.eventSubscriber) {
            this.eventSubscriber.unsubscribe();
        }
	}
}
