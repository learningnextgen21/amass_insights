import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService,UserRouteAccessUnrestrictedService, CanDeactivateGuard, ProviderRouteAccessService, CanDeactivateAdminFormGuard } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DataProviderComponent } from './data-provider.component';
import { DataProviderDetailComponent } from './data-provider-detail.component';
import { DataProviderPopupComponent } from './data-provider-dialog.component';
import { DataProviderDeletePopupComponent } from './data-provider-delete-dialog.component';
import { PageNotFoundComponent } from '../../layouts';
import { DataProviderEditComponent } from 'app/entities/data-provider/data-provider-edit.component';
import { DataProviderAdminFormComponent } from 'app/entities/data-provider/data-provider-admin-form.component';
import { DataProviderLinkAdminFormComponent } from './data-provider-link-admin-form.component';

export const dataProviderRoute: Routes = [
	{
		path: 'providers',
		data: {
			authorities: ['ROLE_USER'],
			pageTitle: 'datamarketplaceApp.dataProvider.home.title',
			breadcrumb: 'Providers',
			type: 'dynamic'
		},
		//canActivate: [UserRouteAccessService],
		canActivate: [UserRouteAccessUnrestrictedService],

		children: [
			{
			  path: ':recordID',
			  data: {
				authorities: ['ROLE_USER'],
				pageTitle: 'datamarketplaceApp.dataProvider.home.title',
				breadcrumb: 'Data Profile',
				type: 'dynamic'
			  },
              children: [
                {
                  path: 'edit',
                  data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'datamarketplaceApp.dataProvider.home.title',
                    breadcrumb: 'Data Profile Edit',
                    type: 'dynamic',
                    interestTypes: [561]
                  },
                  component: DataProviderEditComponent,
                  canActivate: [ProviderRouteAccessService],
                  canDeactivate: [CanDeactivateGuard]
                }, {
                    path: 'admin',
                    data: {
                      authorities: ['ROLE_ADMIN'],
                      pageTitle: 'datamarketplaceApp.dataProvider.home.title',
                      breadcrumb: 'Data Profile Admin',
                      type: 'dynamic'
                    },
                    component: DataProviderAdminFormComponent,
                    canActivate: [UserRouteAccessService],
                    canDeactivate: [CanDeactivateAdminFormGuard]
                  }, {
                    path: 'link',
                    data: {
                      authorities: ['ROLE_USER'],
                      pageTitle: 'datamarketplaceApp.dataProvider.home.title',
                      breadcrumb: 'Link',
                      type: 'dynamic'
                    },
                    component: DataProviderLinkAdminFormComponent,
                    canActivate: [UserRouteAccessService],
                    canDeactivate: [CanDeactivateAdminFormGuard]
                  }, {
                    path: '',
                    component: DataProviderDetailComponent,
                    data: {
                        breadcrumb: '',
                        type: 'dynamic'
                    }
                }
              ],
			  canActivate: [UserRouteAccessService]
			}, {
				path: '',
				component: DataProviderComponent,
				data: {
					breadcrumb: '',
					type: 'dynamic'
                }
			}
		  ]
	}, 
	{
		path: 'unproviders',
		data: {
			authorities: ['ROLE_USER'],
			pageTitle: 'datamarketplaceApp.dataProvider.home.title',
			breadcrumb: 'Providers',
			type: 'dynamic'
		},
		//canActivate: [UserRouteAccessService],
		canActivate: [UserRouteAccessUnrestrictedService],

		children: [
			{
			  path: ':recordID',
			  data: {
				authorities: ['ROLE_USER'],
				pageTitle: 'datamarketplaceApp.dataProvider.home.title',
				breadcrumb: 'Data Profile',
				type: 'dynamic'
			  },
              children: [
                {
                  path: 'edit',
                  data: {
                    authorities: ['ROLE_ADMIN'],
                    pageTitle: 'datamarketplaceApp.dataProvider.home.title',
                    breadcrumb: 'Data Profile Edit',
                    type: 'dynamic',
                    interestTypes: [561]
                  },
                  component: DataProviderEditComponent,
                  canActivate: [ProviderRouteAccessService],
                  canDeactivate: [CanDeactivateGuard]
                }, {
                    path: 'admin',
                    data: {
                      authorities: ['ROLE_ADMIN'],
                      pageTitle: 'datamarketplaceApp.dataProvider.home.title',
                      breadcrumb: 'Data Profile Admin',
                      type: 'dynamic'
                    },
                    component: DataProviderAdminFormComponent,
                    canActivate: [UserRouteAccessService],
                    canDeactivate: [CanDeactivateAdminFormGuard]
                  }, {
                    path: 'link',
                    data: {
                      authorities: ['ROLE_USER'],
                      pageTitle: 'datamarketplaceApp.dataProvider.home.title',
                      breadcrumb: 'Link',
                      type: 'dynamic'
                    },
                    component: DataProviderLinkAdminFormComponent,
                    canActivate: [UserRouteAccessService],
                    canDeactivate: [CanDeactivateAdminFormGuard]
                  }, {
                    path: '',
                    component: DataProviderDetailComponent,
                    data: {
                        breadcrumb: '',
                        type: 'dynamic'
                    }
                }
              ],
			  canActivate: [UserRouteAccessUnrestrictedService]
			}, {
				path: '',
				component: DataProviderComponent,
				data: {
					breadcrumb: '',
					type: 'dynamic'
                }
			}
		  ]
	},
	{
        path: '**',
        redirectTo: '/404'
    }
];

export const dataProviderPopupRoute: Routes = [
	{
		path: 'data-provider-new',
		component: DataProviderPopupComponent,
		data: {
			authorities: ['ROLE_USER'],
			pageTitle: 'datamarketplaceApp.dataProvider.home.title'
		},
		canActivate: [UserRouteAccessService],
		outlet: 'popup'
	},
	{
		path: 'providers/:id/edit',
		component: DataProviderPopupComponent,
		data: {
			authorities: ['ROLE_USER'],
			pageTitle: 'datamarketplaceApp.dataProvider.home.title'
		},
		canActivate: [UserRouteAccessService],
		outlet: 'popup'
	},
	{
		path: 'providers/:id/delete',
		component: DataProviderDeletePopupComponent,
		data: {
			authorities: ['ROLE_USER'],
			pageTitle: 'datamarketplaceApp.dataProvider.home.title'
		},
		canActivate: [UserRouteAccessService],
		outlet: 'popup'
	}
];
