import { Injectable } from '@angular/core';
import { Subject} from 'rxjs';

@Injectable()
export class DataProviderSearchService {

    private messageSearch = new Subject<any>();
    messageSearch$ = this.messageSearch.asObservable();
    private investormessageSearch = new Subject<any>();
    investormessageSearch$ = this.investormessageSearch.asObservable();
    private headerSearch = new Subject<any>();
    headerSearch$ = this.headerSearch.asObservable();
    private headerUnrestrictedSearch = new Subject<any>();
    headerUnrestrictedSearch$ = this.headerUnrestrictedSearch.asObservable();
    private headerUnrestrictedCommonSearch = new Subject<any>();
    headerUnrestrictedCommonSearch$ = this.headerUnrestrictedCommonSearch.asObservable();
    private investorSearch = new Subject<any>();
    investorSearch$ = this.investorSearch.asObservable();
    constructor()
    {

    }
    sendSearch(message: any)
    {
        console.log('message',message);
        this.messageSearch.next(message);
    }
    headersendSearch(headerSearch: any)
    {
        console.log('message',headerSearch);
        this.headerSearch.next(headerSearch);
    }
    headerUnrestrictedSendSearch(headerUnrestrictedSearch: any)
    {
        console.log('message',headerUnrestrictedSearch);
        this.headerUnrestrictedSearch.next(headerUnrestrictedSearch);
    }
    headerUnrestrictedCommonSendSearch(headerUnrestrictedCommonSearch: any)
    {
        console.log('message',headerUnrestrictedCommonSearch);
        this.headerUnrestrictedCommonSearch.next(headerUnrestrictedCommonSearch);
    }
    sendInvestorSearch(message:any)
    {
      this.investorSearch.next(message);
    }
    sendInvestorMessageSearch(message: any)
    {
        console.log('message',message);
        this.investormessageSearch.next(message);
    }
}