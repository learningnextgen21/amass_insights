
import {filter, takeUntil} from 'rxjs/operators';

import {of as observableOf,  Subscription, Observable, BehaviorSubject ,  Subject, from } from 'rxjs';
import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, Inject, Injectable, Input, SimpleChange, OnChanges, SimpleChanges, ElementRef, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import {MatDialog,MatDialogRef} from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { DomSanitizer, Meta } from '@angular/platform-browser';
import { DataProvider } from './data-provider.model';
import { DataProviderService } from './data-provider.service';
import { ContactDialogComponent } from '../../shared/contactdialog/contact-dialog.component';
import { Principal,AuthServerProvider,StateStorageService } from '../../shared';
import { DataSource } from '@angular/cdk/table';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { LoaderService } from '../../loader/loaders.service';
import { NestedTreeControl, FlatTreeControl } from '@angular/cdk/tree';
import { WINDOW } from 'app/layouts';
import { SigninModalService } from 'app/shared';
import {UppyService} from '../../uppy/uppy.service';
import { contains } from 'ramda';
import { GenericProfileTabsModel, TabContentType, GenericSubTabListItem, GenericProfileButtonsModel } from 'app/shared/generic-profile';
import { DatePipe } from '@angular/common';
import { AmassFilterService } from '../../shared/genericfilter/filters.service';
import { AmassSettingService } from '../../account/settings/settings.service';
import { GenericProfileCardModel } from 'app/shared/generic-profile-card';
import { DataResource } from 'app/add-resource-admin/add-resource-admin-form.model';
import { DataResourceTabService } from './data-resourcetab.service';
import { ApplyAccountComponent } from 'app/account';
import { SigninDialogComponent } from 'app/dialog/signindialog.component';

@Component({
    selector: 'jhi-data-provider-detail',
    templateUrl: './data-provider-detail.component.html',
    styleUrls: [ './data-provider-detail.component.css', './data-provider.component.scss', '../../shared/share-buttons/share-buttons.scss' ]
})
export class DataProviderDetailComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
    @ViewChild('test') test: ElementRef;
    @ViewChild('test1') test1: ElementRef;
    @ViewChild('editpending') editpending: ElementRef;
    @ViewChild('pnl') pnl:ElementRef;
    dataProvider: DataProvider;
    lookupcode:any;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    isLoading: boolean;
    resourceTab: number;
    isAdmin: boolean;
    isPendingUnlock: boolean;
    isPendingExpand: boolean;
    isUserPermission: boolean;
    locked: boolean;
    unlockOperatorPermission: boolean;
    domSanitizer: any;
    providerID: any;
    dataProviderName: any;
    resourceLink: DataResource[] = [];
    resources: DataResource[] = [];
    dataResource: DataResource[] = [];
    dataResourceObj: any;
    resourceObj: any;
    authoredArticles: any;
    relevantArticles: any;
    tabIndex: number;
    relevantArticlePage: number;
    relevantArticleLinks: any;
    relevantArticleFrom: number;
    totalRelevantArticles: number;
    authoredArticlePage: number;
    authoredArticleLinks: any;
    authoredArticleFrom: number;
    totalAuthoredArticles: number;
    recordID: string;
    dataProviderRecID: any;
    dataLinksCategory: DataResource[] = [];
    profileTabLabel: any;
    ProfileCategory: any;
    currentAccount: any;
    isLocked: boolean;
    progressBarColor = 'primary';
    progressBarMode = 'determinate';
    mdTooltipDelay: number;
	unregisterUserMsg: boolean;

    displayedColumns = ['fileName', 'purpose', 'purposeType', 'dateCreated'];
    exampleDatabase = null;
    dataSource: DocumentsDataSource | null;

    genericProfileCard: GenericProfileCardModel[] = [];

    categoryExplanation: any[];
    featureExplanation: any[];
    industryExplanation: any[];
    providerTagExplanation: any[];
    providerTypeExplanation: any[];
    dataSourceExplanation: any[];
    dataProvidersPricingModels: any[] = [];
    tc = TreeControl;
    dataIndustryTree;
    dataCategoryTree;
    dataFeaturesTree;
    dataProviderType;
    dataSourceTree;
    dataProviderTagsTree;
    dataGeographicalFociTree;
    dataSecurityTypesTree;
    dataRelevantSectorsTree;
    dataInvestorTypesTree;
    dataMainAssetClassTree;
    dataSampleOrPanelSizeTree;
    dataPublicCompaniesCoveredTree;
    dataHistoricalDateRangeEstimateTree;
    dataDateCollectionRangeExplanationTree;
    dataCollectionMethodsExplanationTree;
    datadatasetSizeTree;
    datadatasetNumberOfRowsTree;
    dataDataLanguagesAvailableTree;
    dataDeliveryMethodsTree;
    dataDeliveryFrequencyNotesTree;
    dataDeliveryFrequenciesTree;
    dataDataUpdateFrequencyTree;
    dataKeyDataFieldsTree;
    datadataUpdateFrequencyNotesTree;
    dataDeliveryFormatsTree;
    dataLicensingAndTrialingTree;
    datanumberOfDataSourcesTree;
    dataDataSourcesDetailsTree;
    datanumberOfAnalystEmployeesTree;
    datayearFirstDataProductLaunchedTree;
    datasecRegulatedTree;
    dataProductDetailsTree;
    datainvestorClientBucketTree;
    dataInvestorClientsCountTree;
    dataTargetOrganizationTypeTree;
    dataManagerTypeTree;
    dataStrategiesTree;
    dataResearchStylesTree;
    dataInvestingTimeFrameTree;
    dataEquitiesMarketCapTree;
    dataEquitiesStyleTree;
    dataAccessOfferedTree;
    dataDataProductTypesTree;
    dataDataTypesTree;
    dataPublicEquitiesCoveredRangeTree;
    dataPublicEquitiesCoveredCountTree;
    dataIdentifiersAvailableTree;
    dataPointInTimeAccuracyTree;
    dataDataGapsTree;
    dataDataGapsReasonsTree;
    dataDuplicatesCleanedTree;
    dataIncludesOutliersTree;
    dataOutlierReasonsTree;
    dataBiasesTree;
    dataSampleOrPanelBiasesTree;
    dataNormalizedTree;
    dataLagTimeTree;
    dataOurViewsTree;
    dataCompetitiveDifferentiatorsTree;
    dataDataLicenseTypeDetailsTree;
    dataDataLicenseTypeTree;
    dataPotentialDrawbacksTree;
    dataUniqueValuePropsTree;
    dataLegalAndComplianceIssuesTree;
    dataNotesTree;
    datascoreInvestorsWillingnessTree;
    datascoreUniquenessTree;
    datascoreValueTree;
    datascoreReadinessTree;
    dataUseCasesOrQuestionsAddressedTree;
    dataScoreOverallTree;
    dataDataRoadmapTree;
    dataCleanlinessTree;
    dataAnalyticsTree;
    dataOwnerOrganizationTree;
    dataOwnerOrganizationNameTree;
    dataOwnerOrganizationLocationTree;
    dataOwnerOrganizationYearFoundedTree;
    dataOwnerOrganizationheadcountBucketTree;
    dataOwnerOrganizationheadcountNumberTree;
    dataOwnerOrganizationyearExitTree;
    dataOwnerOrganizationFinancialPositionTree;
    dataOwnerOrganizationHeadCountTree;
    dataOwnerOrganizationClientBaseTree;
    dataShortDescriptionTree;
    dataLongDescriptionTree;
    dataProfileVerificationStatusTree;
    dataSummaryTree;
    dataInvestorClientsTree;
    dataLicensingTree;
    dataFreeTrialAvailableTree;
    // samples: any;
    id: number;
    link: any;
    urlLinks: any;
    imageNull: any;
    // @ViewChild('tree') tree;
    allowEditAccess: boolean;
    categoryLogo: any[];
    customTags: boolean;
    displayCommentsSidebar: boolean;
    comments: any[] = [];
    commentsButtonType: string = '';
    focusForm: boolean;
    newTagValue: any;
    userLevel: boolean;
    userRole: boolean;
    providerPermission: boolean;
    tagDropDownValues: any;
    tagCheckItem: boolean;
    dropDownAutoClose: boolean;
    loaderActive: any
    editAccessPending: boolean;
    editAccessDisable: boolean;
    filterCode: any;
    filterStatus: any;
    tagCount: any;
    operatorPermissionId: any;
    tagProviderId: number;
    providersId: number;
    dataProviderId: number;
    onPostPrivacy: any;
    tagProviderMatch: any;
	tagNotInterstedMatch: any;
	tagInUseMatch: any;
	tagTestingMatch: any;
	tagInterstedArray : any[] = [];
	tagNotInterstedArray : any[] = [];
	tagInUseArray : any[] = [];
    tagTestingArray : any[] = [];
    restrictedAuthority: boolean;
    loaderTag: any;
	loaderProviderId: any;
    pageName = 'list';
   // @Output() privacyFilter = new EventEmitter();
    uppyEvent = new Subject<[string, any, any, any]>();
    mdDialogRef: MatDialogRef<any>;

    uppyPlugins = [
        ['Dashboard', {
            target: '.DashboardContainer',
            replaceTargetContent: true,
            inline: true,
            restrictions: {
                maxFileSize: 200000000,
                maxNumberOfFiles: 10,
                minNumberOfFiles: 1,
                allowedFileTypes: ['image/*', 'video/*']
            }
        }],
        ['Tus', { endpoint: 'https://master.tus.io/files/' }],
        ['GoogleDrive', { target: 'Dashboard', serverUrl: 'https://companion.uppy.io' }],
        ['Dropbox', { target: 'Dashboard', serverUrl: 'https://companion.uppy.io' }]
    ];

    onDestroy$ = new Subject<void>();

    tabsArray: BehaviorSubject<GenericProfileTabsModel[]> = new BehaviorSubject<GenericProfileTabsModel[]>([]);
    //resourcesArray: BehaviorSubject<GenericProfileCardModel[]> = new BehaviorSubject<GenericProfileCardModel[]>([]);
    tabs: GenericProfileTabsModel[];
    //resource: GenericProfileCardModel[];

    resourcesCount: number;
    allDataAccess: boolean;
    constructor(
        private filtersService: AmassFilterService,
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private dataProviderService: DataProviderService,
        private route: ActivatedRoute,
        private router: Router,
        public dialog: MatDialog,
        private principal: Principal,
        private stateService: StateStorageService,
        private authService: AuthServerProvider,
        public _DomSanitizer: DomSanitizer,
        public meta: Meta,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private loaderService: LoaderService,
        private signinModalService: SigninModalService,
        @Inject(WINDOW) private window: Window,
        private uppyService: UppyService,
        private datePipe: DatePipe,
        private filterService: AmassFilterService,
        private amassSettingService: AmassSettingService,
        private dataresourceTabService: DataResourceTabService,
    ) {
        this.domSanitizer = DomSanitizer;
        this.meta.updateTag({
            name: 'author',
            content: 'Amass Insights'
        });
        this.meta.updateTag({
            name: 'keywords',
            content: 'Amass Insights, Data Provider, Investors'
        });
        this.meta.updateTag({
            name: 'description',
            content: 'Our web-based \'Insights\' platform, built exclusively for the capital markets industry, is the largest, most comprehensive, and most accurate source of alternative data provider information available anywhere in the world. It provides asset managers with an ever-expanding stream of unique, unbiased, alpha-generating data, geared towards their firm\'s specific interests and needs. These features, combined with Amass\'s proven sourcing ability and value-added analytics, save research teams valuable time searching for, understanding, vetting, and onboarding alternative data.'
        });

        this.dataSource = new DocumentsDataSource(this.exampleDatabase);
        this.categoryExplanation = [];
        this.featureExplanation = [];
        this.industryExplanation = [];
        this.providerTagExplanation = [];
        this.providerTypeExplanation = [];
        this.dataSourceExplanation = [];
        this.resourcesCount = 0;
        this.displayCommentsSidebar = false;

        this.uppyEvent.pipe(
            takeUntil(this.onDestroy$),
            filter(([ev]) => contains(ev, ['complete'])),)
            .subscribe(
                ([ev, data1, data2, data3]) =>
                err => console.dir(err),
                () => console.log('done')
            );

        this.tabsArray.subscribe( value => {
          //  console.log('tab changed...', value);
            this.tabs = value;
        });
        // this.resourcesArray.subscribe( value => {
        //     console.log('resources changed...', value);
        //     this.resource = value;
        // });
    }
    ngOnInit() {
        this.resourceTab=0;
        this.filterCode = 1;
        this.loaderActive = false;
        this.loaderService.display(true);
        this.isLoading = true;
        this.isPendingUnlock = false;
        this.isPendingExpand = false;
        this.unlockOperatorPermission = false;
        this.tabIndex = 0;
        this.focusForm = false;
        this.categoryLogo = [];
        this.tagCount = 0;
        /* if(this.isAuthenticated())
        {
            this.getMouseOverExplanationByType('data-industries');
            this.getMouseOverExplanationByType('data-categories');
            this.getMouseOverExplanationByType('data-features');
            this.getMouseOverExplanationByType('data-sources');
        } */
        if(!this.isAuthenticated())
        {
            this.unregisterUserMsg = true;
                this.subscription = this.route.params.subscribe((params) => {
                    this.recordID = params['recordID'];
                    this.load(this.recordID);
                });
        }
        this.route.queryParams.subscribe(params => {
            if (params['pending-unlock']) {
                this.isPendingUnlock = true;
            }

            if (params['pending-expand']) {
                this.isPendingExpand = true;
            }
        });
        this.dataresourceTabService.messageResourceTAb$.subscribe(message =>{
            console.log('message',message);
            if(message === 'resources')
            {
                this.load(this.recordID);
            }
        });
        if(this.isAuthenticated())
        {
        this.subscription = this.route.params.subscribe((params) => {
            this.recordID = params['recordID'];
           // console.log('rout id....', this.recordID);
            this.load(params['recordID']);
            this.amassSettingService.setAuthorisedUrl().subscribe(data => {
                this.operatorPermissionId = data;
               // console.log('Operator Permission',this.operatorPermissionId);

                if (this.operatorPermissionId && this.operatorPermissionId.length) {
                for(let i = 0; i < this.operatorPermissionId.length; i++) {
                    const permissionmId = this.operatorPermissionId[i].providerRecordID;
                     if(permissionmId === this.recordID) {
                        this.unlockOperatorPermission = true;
                        //this.load(this.recordID);
                      //  console.log('looping.....', permissionmId);
                        this.dataProviderService.getProviderEdit(permissionmId).subscribe(data => {
                          //  console.log('Datas', data);
                            if (data && data.recordUpdated) {
                                this.allowEditAccess = false;
                              //  console.log(this.allowEditAccess);
                                this.editAccessPending = true;
                            } else {
                                this.allowEditAccess = true;
                                this.editAccessPending = false;
                              //  console.log(this.allowEditAccess);
                            }
                        }, error => {
                          //  this.allowEditAccess = true;
                          this.editAccessDisable = true;
                        });
                    }
                }
            }
            });
        });
   
        this.principal.identity().then(account => {
			this.currentAccount = account;
        });
        this.userLevel = this.principal.hasAnyPermissionAboveDirect(200);
        this.focusForm = this.userLevel;
        this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
    }
    this.registerChangeInDataProviders();

       // this.userRole = this.principal.hasAnyAuthorityDirect(['ROLE_USER']);
       if(this.isAuthenticated())
       {
        this.dataProviderService.getProviderId(this.recordID).subscribe(data => {
            this.dataProviderId = data.id;
            if (data && data.userPermission === 'ROLE_USER' || 'ROLE_SILVER') {
              this.userRole = true;
            }
            else
            {
                this.userRole = false;
            }
            this.amassSettingService.checkProviderPermission(this.dataProviderId).subscribe(data => {
                //console.log('dataproviderpermission',this.providerPermission);
                if(data)
                {
                    this.providerPermission = true;
                }
                else{
                    this.providerPermission = false;
                }
             });
        });
    }
        this.relevantArticlePage = 0;
        this.relevantArticleLinks = {
            last: 0
        };
        this.authoredArticlePage = 0;
        this.authoredArticleLinks = {
            last: 0
        };
        this.mdTooltipDelay = 500;
        this.loadTabs();

        /* this.filterService.getDataProviderTagUrl().subscribe(data => {
			this.loaderService.display(false);
            const listItems = data;
            this.tagDropDownValues = listItems;
            console.log(this.tagDropDownValues);
        }); */
        if(this.isAuthenticated())
        {
        this.amassSettingService.providerTagCount('Interested').subscribe(data => {
			this.tagProviderMatch = data;
			this.tagInterstedArray=this.tagProviderMatch.interested;
			//this.tagValue = this.tagProviderMatch.providerTags
			//console.log('tagvalue',this.tagValue.interested);
		});
		this.amassSettingService.providerTagCount('Not Interested').subscribe(data => {
			this.tagNotInterstedMatch = data;
			this.tagNotInterstedArray=this.tagNotInterstedMatch.notInterested;
			//this.tagValue = this.tagProviderMatch.providerTags
			//console.log('tagvalue',this.tagValue.interested);
		});
		this.amassSettingService.providerTagCount('In Use').subscribe(data => {
			this.tagInUseMatch = data;
			this.tagInUseArray=this.tagInUseMatch.inUse;
			//this.tagValue = this.tagProviderMatch.providerTags
			//console.log('tagvalue',this.tagValue.interested);
		});
		this.amassSettingService.providerTagCount('Testing').subscribe(data => {
			this.tagTestingMatch = data;
			this.tagTestingArray=this.tagTestingMatch.testing;
			//this.tagValue = this.tagProviderMatch.providerTags
			//console.log('tagvalue',this.tagValue.interested);
        });
        this.amassSettingService.restrictedAuthority().subscribe(data => {
			this.restrictedAuthority = data;
        });   
    }
     }

    ngAfterViewInit() {
        const uppy = this.uppyService.uppy;
    }

    ngOnChanges(changes: SimpleChanges) {
        /* for (let propName in changes) {
            let chng = changes[propName];
            let cur  = JSON.stringify(chng.currentValue);
            let prev = JSON.stringify(chng.previousValue);
            console.log(`${propName}: currentValue = ${cur}, previousValue = ${prev}`);
        } */
    }
    isAuthenticated() {
        return this.principal.isAuthenticated();
    }
    authenticationCheck()
    {
        if(!this.isAuthenticated())
        {
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        });
    }
    }
    getLinkUrl(url) {
        if (url) {
            this.urlLinks = (url.indexOf('http://') !== -1 || url.indexOf('https://') !== -1) ? url : 'http://' + url;
            window.open(this.urlLinks,'_blank');
        }
        // return this.urlLinks;
    }

    getLinkImgUrl(desc) {
        let link = '';
        if (desc) {
            const fileName = ((desc).toLowerCase()).replace(' ', '_');
            link = 'content/images/' + fileName  + '.png';
        }

        return link;
    }

    getDefaultFileUrl() {
        let file = '';
            //const fileName = ((desc).toLowerCase()).replace(' ', '_');
            file = 'content/images/default_file.png';
        return file;
    }

    providerProfileTab(event: MatTabChangeEvent) {

       /*  this.profileTabLabel = 'Provider Profile - Tab ' + event.tab.textLabel;
        this.ProfileCategory = 'Provider Profile ' + event.tab.textLabel;
        this.googleAnalyticsService.emitEvent(this.ProfileCategory, 'Tab Link Clicked', this.profileTabLabel); */
    }

    providerFollow(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    unlockProviders(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    requestMoreInfo(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    providerPartnerTab(event: MatTabChangeEvent) {
        this.profileTabLabel = 'Provider Profile -  ' + event.tab.textLabel;
        this.googleAnalyticsService.emitEvent('Partner Provider', 'Internal Link Clicked', this.profileTabLabel);
    }

    dataFieldLink(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    profileWebsite(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    profileArticle(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    samplesTab(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    customTag(events) {
       // this.dropDownAutoClose = false;
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
        this.customTags = true;
    }
    shreDropDown(events: any) {
        if(this.isAuthenticated())
        {
            this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
        }
        else{
            this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            });
        }
    }
    /* getTagList event list out the all tags */
    getTagList(providerID?: number) {
        this.providersId = providerID;
        this.loaderActive = false;
        this.tagDropDownValues = [];
        this.tagCount = 0;
        if(this.isAuthenticated())
        {
        this.filtersService.getDataProviderTagUrl().subscribe(data => {
            const listItems = data;
            this.tagDropDownValues = [];
            for (let i = 0; i < listItems.length; i++) {
                const dataList = listItems[i];
                this.tagCheckItem = false;
                if(dataList.tagProviders && dataList.tagProviders !== 'null') {
                    const providerTag = dataList.tagProviders.filter((item, index) => {
                        this.tagProviderId = parseInt(item.id);
                       // console.log('new tag...', this.tagProviderId);
                        if (this.providersId === this.tagProviderId) { // mapped provider tags highlited with tick mark
                            this.tagCheckItem = true;
                            this.tagCount = this.tagCount + 1; // how many tags mapped with related provider it is diplayed tag count
                          //  console.log('new tag...', this.tagCheckItem);
                        }
                        return this.providersId === this.tagProviderId;
                    });
                }
                    this.tagDropDownValues.push({
                        tagName: dataList.providerTag,
                        'providerTag': this.tagCheckItem,
                        privacy: dataList.privacy,
                        id: dataList.id
                    });
              //  console.log(this.tagDropDownValues);
            }
            /* for(let i = 0; i<this.tagDropDownValues.length; i++) {
                const tagList = this.tagDropDownValues[i].providerTag;
                if(tagList.providerTag) {
                    this.tagCount = this.tagCount + 1;
                }
            } */
        });
    }
    }
    /* onKeydownTag used for new tag creation */
    onKeydownTag(values) {
        this.customTags = false;
        const providerId = values.providerID;
       // console.log(providerId);
       // console.log('new tag...', values);
        this.dataProviderService.tagList(values).subscribe(data => {
            const tags = data;
              this.getTagList(providerId);
            //  console.log('...providerId....', providerId);
              this.customTags = false;
            /* this.dropDownAutoClose = true;
            console.log(this.dropDownAutoClose); */
        });
        this.newTagValue = '';
    }
    providerNewTag(values, index) {
        if(!this.tagDropDownValues[index].providerTag) {
        this.loaderActive = true;
        this.dataProviderService.newTagList(values).subscribe(data => {
            this.tagDropDownValues[index].providerTag = true;
            this.loaderActive = false;
            if (this.tagDropDownValues[index].providerTag) {
                this.loaderActive = false;
            }
          //  this.getTagList(providerID);
         /* this.dropDownAutoClose = true;
         console.log(this.dropDownAutoClose); */
        });
    }
        else if(this.tagDropDownValues[index].providerTag) {
           // console.log('........removetag.......');
            this.dataProviderService.removeTagList(values).subscribe(data => {
                this.tagCheckItem = false;
                this.tagDropDownValues[index].providerTag = false;
                this.loaderActive = false;
            });
        }
    }
providerActionTag(values)
	{
if(this.tagInterstedArray.indexOf(values.providerId) !== -1 )
{
	this.loaderTag = true;
	this.loaderProviderId = values.providerId;
	this.dataProviderService.removeTagList(values).subscribe(data => {
		const index = this.tagInterstedArray.indexOf(values.providerId);
	this.tagInterstedArray.splice(index,1);
	this.loaderTag = false;
	});

}
else
{
	this.loaderTag = true;
	this.loaderProviderId = values.providerId;
	this.dataProviderService.newTagList(values).subscribe(data => {
		this.tagInterstedArray.push(values.providerId);
		this.loaderTag = false;
    });
    this.dataProviderService.followActionTag(this.loaderProviderId).subscribe(data => {
    });
}

	}
	providerActionTagnotIntersted(values)
	{
if(this.tagNotInterstedArray.indexOf(values.providerId) !== -1 )
{
	this.loaderTag = true;
	this.loaderProviderId = values.providerId;
	this.dataProviderService.removeTagList(values).subscribe(data => {
		const index = this.tagNotInterstedArray.indexOf(values.providerId);
	this.tagNotInterstedArray.splice(index,1);
	this.loaderTag = false;
	});

}
else{
	this.loaderTag = true;
	this.loaderProviderId = values.providerId;
this.dataProviderService.newTagList(values).subscribe(data => {
	this.tagNotInterstedArray.push(values.providerId);
	this.loaderTag = false;
});
}
	}
	providerActionTagInuse(values)
	{
if(this.tagInUseArray.indexOf(values.providerId) !== -1 )
{
	this.loaderTag = true;
	this.loaderProviderId = values.providerId;
	this.dataProviderService.removeTagList(values).subscribe(data => {
		const index = this.tagInUseArray.indexOf(values.providerId);
	this.tagInUseArray.splice(index,1);
	this.loaderTag = false;
	});
}
else{
	this.loaderTag = true;
	this.loaderProviderId = values.providerId;
this.dataProviderService.newTagList(values).subscribe(data => {
	this.tagInUseArray.push(values.providerId);
	this.loaderTag = false;
});
this.dataProviderService.followActionTag(this.loaderProviderId).subscribe(data => {
});
}
	}
	providerActionTagTesting(values)
	{
if(this.tagTestingArray.indexOf(values.providerId) !== -1 )
{
	this.loaderTag = true;
	this.loaderProviderId = values.providerId;
	this.dataProviderService.removeTagList(values).subscribe(data => {
		const index = this.tagTestingArray.indexOf(values.providerId);
	this.tagTestingArray.splice(index,1);
	this.loaderTag = false;
	});
}
else{
	this.loaderTag = true;
	this.loaderProviderId = values.providerId;
this.dataProviderService.newTagList(values).subscribe(data => {
	this.tagTestingArray.push(values.providerId);
	this.loaderTag = false;
});
this.dataProviderService.followActionTag(this.loaderProviderId).subscribe(data => {
});
}
	}
    load(recordID) { // recordid provide all provider profile page details
        if(this.isAuthenticated())
        {
            console.log('called');
        this.dataProviderService.findById(recordID).subscribe(dataProvider => { // just pass recordid through param then get profile information
            this.dataProvider = dataProvider['hits'].total ? dataProvider['hits'].hits[0]._source : null;
            this.providerID = this.dataProvider ? this.dataProvider.id : ''; // providerid passed as a input detail page to comments side bar
            this.dataProviderName = this.dataProvider ? this.dataProvider.providerName : '';
            this.dataProviderRecID = this.dataProvider ? this.dataProvider.recordID : '';
            const providerObj = dataProvider['hits'].total ? dataProvider['hits'].hits[0]._source : null;
            const providerUrl = window.location.origin + '/#/providers/' + recordID;
            let categoryText = '';
            let industryText = '';
            let hashTagText = '';
            let categoryHashTagText = '';
            this.resourcesCount = this.dataProvider && this.dataProvider.documentsMetadata ? this.dataProvider.documentsMetadata.length : 0;
            if (this.dataProvider) {
                this.getTagList(this.dataProvider.id);
                hashTagText = (providerObj.mainDataIndustry && providerObj.mainDataIndustry.hashtag) ? providerObj.mainDataIndustry.hashtag : '';
				categoryHashTagText = (providerObj.mainDataCategory && providerObj.mainDataCategory.hashtag) ? providerObj.mainDataCategory.hashtag : '';
                categoryText = (providerObj.mainDataCategory && !providerObj.mainDataCategory.locked && providerObj.mainDataCategory.datacategory !== 'DATA_CATEGORY_NULL') ? providerObj.mainDataCategory.datacategory : '';
                industryText = (providerObj.mainDataIndustry && !providerObj.mainDataIndustry.locked && providerObj.mainDataIndustry.dataIndustry !== 'DATA_INDUSTRY_NULL') ? providerObj.mainDataIndustry.dataIndustry : '';
                this.dataProvider = Object.assign({
                    emailText: 'Hi,\n I\'d like to share this interesting ' + providerUrl + ' data profile for '+ (!providerObj.locked ? providerObj.providerName : '') +' within the '+categoryText+' Data Category I found on the Amass Insights Platform.\n\n' + 'Regards,\n ' + this.currentAccount.firstName + ' ' + this.currentAccount.lastName +'\n\n\n P.S. If you\'re not already an Insights user, https://amassinsights.com?signup=true, in order to view this data profile. Subsequently, you\'ll receive instant access to Alt-Data news, events, though-leadership, and new, interesting data providers entering our ecosystem.',
                    linkedInText: 'Check out this interesting ' + categoryText + ' data profile I found via the Amass Insights Platform: ' + providerUrl + (categoryText ? ' #' + categoryText : '') + ' #AmassInsights #AlternativeData ' + (industryText ? '#' + industryText : ''),
                     twitterText: 'Check out this interesting ' + categoryHashTagText + ' data profile I found via the Amass Insights Platform: ' + providerUrl + ' ' + ' ' + (categoryHashTagText ? categoryHashTagText : '') + ' #AmassInsights #AlternativeData ' + (hashTagText ? hashTagText : ''),
                    url: providerUrl
                }, providerObj);
                if(providerObj && providerObj.mainDataCategory && providerObj.mainDataCategory.id) {
                    this.getCategoryLogo(providerObj.mainDataCategory.id);
                }
                if (this.dataProvider.categories) {
                    this.dataProvider.categories.forEach((val, index) => {
                        this.getCategoryLogo(val.id);
                    });
                }
               /*  if (this.dataProvider.providerTags) {
                    this.dataProvider.providerTags.forEach(item => {
                        if (this.providerTagExplanation[item.id]) delete this.providerTagExplanation[item.id];
                        this.getMouseOverExplanation(item.id, 'dataProviderTag');
                    });
                } */
            }
            if (this.dataProvider && this.dataProvider.mainAssetClass) {
                this.dataProviderService.findLookupCode(this.dataProvider.mainAssetClass).subscribe(value =>{ 
                   // console.log('datalookupcode',value);
                   this.lookupcode=value;
                });
            }
            if (this.dataProvider && this.dataProvider.locked) {
                this.isLocked = this.dataProvider.locked;
            }
           // console.log('islocked',this.isLocked);
            this.genericProfileCard = [];
            this.resources = [];
            this.resourceLink = [];
            this.dataResource = [];
            this.resourceObj = [];
            this.genericProfileCard=[];
            this.dataResourceObj=[];
            this.filtersService.findByResourceId(this.providerID).subscribe(data => {
                //console.log('Data Resource',data);
               // console.log('length', data['hits'].hits.length);
                if(data['hits'] && data['hits'].hits) {
                    for (let i = 0; i < data['hits'].hits.length; i++) {
                        this.resourceObj = data['hits'].hits[i]._source;
                        // this.resources.push({...this.resourceObj, type: this.resourceObj.name.includes('http://') || this.resourceObj.name.includes('https://') ? 'link' : 'file'});
                     
                        this.resources.push({...this.resourceObj, type: this.resourceObj && this.resourceObj.link && this.resourceObj.link !== null ? 'link' : 'file'});
                        if (this.resourceObj.link !== null) {
                            this.resourceLink.push(this.resourceObj.link);
                        }
                    }
                }
                //console.log('resourcelenth',this.resources);
           this.dataProviderService.findByResources(this.resources).subscribe(data => {
                       //this.dataResource = data;
                       this.resources= data;

                this.resources.map(item =>{
					if(item.link) {
						this.dataLinksCategory.push(item.link);
					}
				});
			 
                //console.log('resources',this.resources);
                this.resources.forEach((item, index) => {
                    this.dataResourceObj = item;
                    this.dataResource.push({...this.dataResourceObj, type: this.dataResourceObj && this.dataResourceObj.link && this.dataResourceObj.link !== null ? 'link' : 'file'});
                });
               // console.log('dataResource',this.dataResource);
                 this.dataResource.forEach((item, index) => {
                     
                    var url = item.link && item.link.link ? item.link.link : null;
                    if(url!==null){
                        //var url = prompt("url: ");
                        //url = url.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "").split('/')[0];
                        url = url.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "");

                    }
                    const img = (item['type'] === 'link' && item.link && item.link.dataLinksCategory && item.link.dataLinksCategory.length) ? this.getLinkImgUrl(item.link.dataLinksCategory[0].description) : null;
                    const fileImg = (item['type'] === 'file' && item.file && item.file.fileID) ? this.getDefaultFileUrl() : null;
                    let columnCount = 0,
                        columnSize = [12];
                    if (img || fileImg) {
                        columnCount = 2;
                        columnSize = [1, 11];
                    } else {
                        columnCount = 1;
                        columnSize = [12];
                    }
                    const linkUrl="http://"+url;
                    if(item.userPermission ==='ROLE_ADMIN' && !this.isAdmin && !this.unlockOperatorPermission)
                    {
                      this.isUserPermission = true;
                      //console.log('user',this.isUserPermission);
                    }
                    else
                    {
                      this.isUserPermission = false;
                    }
                    if(item.locked && !this.isAdmin && !this.providerPermission && !this.unlockOperatorPermission)
                    {
                      this.locked = true;

                    }
                    else if(item.locked && !this.isAdmin && this.providerPermission && !this.unlockOperatorPermission)
                    {
                      if(item.userPermission ==='ROLE_ADMIN')
                      {
                        this.locked = true;
                      }
                      else{
                          this.locked = false;
                      }
                    }
                    else
                    {
                        this.locked = false;
                    }
                    this.genericProfileCard.push({
                        accessPage: 'sample',
                        locked: this.locked,
                        content: [{
                            columns: columnCount,
                            'columnSize': columnSize,
                            columnContent: [
                                {
                                    image: item['type'] === 'link' ? img : fileImg,
                                    clickable: item['type'] === 'link' ? false : true,
                                    url: item['type'] === 'link' ? linkUrl : null,
                                },
                                {
                                    title: item.name,
                                    url: item['type'] === 'link' ? linkUrl : null,
                                    toolTip: item.name,
                                    clickable: item['type'] === 'link' ? false : true,
                                    clickableObj: item.file,
                                    userPermission: this.isUserPermission,
                                }]
                        },{
                            columns: 3,
                            columnSize: [4 ,4 ,4],
                            columnContent: [
                                {
                                    label: item.purposes ? 'Purposes:' : null,
                                    labelToolTip: 'Categorization of the focus of the Resource',
                                    arrayText: item.purposes ? item.purposes.map(it => {return {id: it.id, desc: it.purpose}}) : '',
                                    filterKey: 'purposes.id',
                                    url: '/#/',
                                    filterType: 'checkbox'
                            },
                            {
                                    label: item.purposeTypes ? 'Purpose Types:' : null,
                                    labelToolTip: 'Categorization of the focus of the Resource',
                                    arrayText: item.purposeTypes ? item.purposeTypes.map(it => {return {id: it.id, desc: it.purposeType}}) : '',
                                    filterKey: 'purposeTypes.id',
                                    url: '/#/',
                                    filterType: 'checkbox'
                            },
                            {
                                    label: item.topics ? 'Topics:' : null,
                                    arrayText: item.topics ? item.topics.map(it => {return {id: it.id, desc: it.topicName}}) : '',
                                    filterKey: 'topics.id',
                                    url: '/#/',
                                    filterType: 'checkbox'
                            }
                            ]
                        },{
                            columns: 1,
                            columnSize: [12],
                            columnContent:[
                            {
                                plainText: item.description ? item.description : '',
                            }]
                        },
                        {
                            columns: 3,
                            columnSize: [4, 4, 4],
                            columnContent:[
                            {
                                label: 'Added:',
                                labelToolTip: 'Date this Resource was created',
                                plainText: this.datePipe.transform(item.createdTimeFormula, 'mediumDate', 'EST')
                            },
                            ]
                        }
                    ],
                    });

                 
                    this.loadTabs();
                });
                 });
            });
            setTimeout(() => {
                this.loadAllTree(this.dataProvider);
                this.loadComments();
            }, 1000);

            if (this.dataProvider && !this.dataProvider.locked) {
                this.dataProviderService.elasticSearchAuthoredArticles(recordID, 0).subscribe((authoredArticles) => {
                    this.authoredArticles = authoredArticles['hits'].total ? authoredArticles['hits'].hits : null;
                    this.totalAuthoredArticles = authoredArticles['hits'].total;
                    this.authoredArticleLinks.last = Math.ceil(this.totalAuthoredArticles / 10);
                    this.loadTabs();
                }, error => {
                    this.authoredArticles = null;
                });

                this.dataProviderService.elasticSearchRelevantArticles(recordID, 0).subscribe((relevantArticles) => {
                    this.relevantArticles = relevantArticles['hits'].total ? relevantArticles['hits'].hits : null;
                    this.totalRelevantArticles = relevantArticles['hits'].total;
                    this.relevantArticleLinks.last = Math.ceil(this.totalRelevantArticles / 10);
                    this.loadTabs();
                }, error => {
                    this.relevantArticles = null;
                });
            } else {
                this.authoredArticles = null;
                this.relevantArticles = null;
            }
           // this.loaderService.display(false);
            //this.dataSource = this.dataProvider && this.dataProvider.documentsMetadata && this.dataProvider.documentsMetadata.length ? new DocumentsDataSource(this.dataProvider.documentsMetadata) : null;
        }, error => {
            this.dataProvider = null;
            this.isLoading = false;
            this.loaderService.display(false);
            if (error.status === 401) {
                this.router.navigate(['']);
                this.signinModalService.openDialog('Session expired! Please re-login.');
            }
        });
    }
    else if(!this.isAuthenticated())
    {
        this.dataProviderService.findByUnrestrictedId(this.recordID).subscribe(dataProvider => {     
            this.dataProvider = dataProvider['hits'].total ? dataProvider['hits'].hits[0]._source : null;
            console.log('UNRESTRICTED PRO', this.dataProvider);
            this.allDataAccess = this.dataProvider && this.dataProvider.recordID ? true : false;
            this.providerID = this.dataProvider ? this.dataProvider.id : ''; // providerid passed as a input detail page to comments side bar
            this.dataProviderName = this.dataProvider ? this.dataProvider.providerName : '';
            this.dataProviderRecID = this.dataProvider ? this.dataProvider.recordID : '';
            const providerObj = dataProvider['hits'].total ? dataProvider['hits'].hits[0]._source : null;
            const providerUrl = window.location.origin + '/#/providers/' + recordID;
            let categoryText = '';
            let industryText = '';
            let hashTagText = '';
            let categoryHashTagText = '';
            this.resourcesCount = this.dataProvider && this.dataProvider.documentsMetadata ? this.dataProvider.documentsMetadata.length : 0;
            if (this.dataProvider) {
                this.getTagList(this.dataProvider.id);
                hashTagText = (providerObj.mainDataIndustry && providerObj.mainDataIndustry.hashtag) ? providerObj.mainDataIndustry.hashtag : '';
				categoryHashTagText = (providerObj.mainDataCategory && providerObj.mainDataCategory.hashtag) ? providerObj.mainDataCategory.hashtag : '';
                categoryText = (providerObj.mainDataCategory && !providerObj.mainDataCategory.locked && providerObj.mainDataCategory.datacategory !== 'DATA_CATEGORY_NULL') ? providerObj.mainDataCategory.datacategory : '';
                industryText = (providerObj.mainDataIndustry && !providerObj.mainDataIndustry.locked && providerObj.mainDataIndustry.dataIndustry !== 'DATA_INDUSTRY_NULL') ? providerObj.mainDataIndustry.dataIndustry : '';
                this.dataProvider = Object.assign({
/*                     emailText: 'Hi,\n I\'d like to share this interesting ' + providerUrl + ' data profile for '+ (!providerObj.locked ? providerObj.providerName : '') +' within the '+categoryText+' Data Category I found on the Amass Insights Platform.\n\n' + 'Regards,\n ' + this.currentAccount.firstName + ' ' + this.currentAccount.lastName +'\n\n\n P.S. If you\'re not already an Insights user, https://amassinsights.com?signup=true, in order to view this data profile. Subsequently, you\'ll receive instant access to Alt-Data news, events, though-leadership, and new, interesting data providers entering our ecosystem.',
 */                    linkedInText: 'Check out this interesting ' + categoryText + ' data profile I found via the Amass Insights Platform: ' + providerUrl + (categoryText ? ' #' + categoryText : '') + ' #AmassInsights #AlternativeData ' + (industryText ? '#' + industryText : ''),
                     twitterText: 'Check out this interesting ' + categoryHashTagText + ' data profile I found via the Amass Insights Platform: ' + providerUrl + ' ' + ' ' + (categoryHashTagText ? categoryHashTagText : '') + ' #AmassInsights #AlternativeData ' + (hashTagText ? hashTagText : ''),
                    url: providerUrl
                }, providerObj);
                if(providerObj && providerObj.mainDataCategory && providerObj.mainDataCategory.id) {
                    this.getCategoryLogo(providerObj.mainDataCategory.id);
                }
                if (this.dataProvider.categories) {
                    this.dataProvider.categories.forEach((val, index) => {
                        this.getCategoryLogo(val.id);
                    });
                }
              /*   if (this.dataProvider.providerTags) {
                    this.dataProvider.providerTags.forEach(item => {
                        if (this.providerTagExplanation[item.id]) delete this.providerTagExplanation[item.id];
                        this.getMouseOverExplanation(item.id, 'dataProviderTag');
                    });
                } */
                this.loadTabs();
            }
            this.isLocked = this.dataProvider.locked
            this.genericProfileCard = [];
            this.resources = [];
            this.resourceLink = [];
            this.dataResource = [];
setTimeout(() => {
    this.loadAllTree(this.dataProvider);
}, 1000);           
        }, error => {
            this.dataProvider = null;
            this.isLoading = false;
            this.loaderService.display(false);
            if (error.status === 401) {
                this.router.navigate(['']);
                this.signinModalService.openDialog('Session expired! Please re-login.');
            }
        });
        
    }
    }
    getCategoryLogo(id) {
        this.filterService.getDtaCategoryImage(id).subscribe(category => {
            const cat = category['hits']['hits'] ? category['hits']['hits'][0]._source : null;
            if (cat) {
                this.categoryLogo.push({id: cat.id, logo: cat.logo ? cat.logo.logo : null, explanation: cat.explanation});
            }
        });
    }

    loadAllRelevantArticles(from) {
        this.dataProviderService.elasticSearchRelevantArticles(this.recordID, from).subscribe(data => {
            this.totalRelevantArticles = data['hits'] ? data['hits'].total : 0;
            this.relevantArticleLinks.last = Math.ceil(this.totalRelevantArticles / 10);
            if (data['hits'] && data['hits'].hits) {
                for (let i = 0; i < data['hits'].hits.length; i++) {
                    this.relevantArticles.push(data['hits'].hits[i]);
                }
            }
            this.loadTabs();
        }, error => {
            this.relevantArticles = null;
        });
    }

    loadRelevantArticlesPage(page) {
        this.relevantArticlePage = page;
        this.relevantArticleFrom = (this.relevantArticlePage === 0) ? 0 : (this.relevantArticlePage * 10);
        if (this.relevantArticleFrom <= this.totalRelevantArticles) {
            this.loadAllRelevantArticles(this.relevantArticleFrom);
        }
    }

    loadAllAuthoredArticles(from) {
        this.dataProviderService.elasticSearchAuthoredArticles(this.recordID, from).subscribe(data => {
            this.totalAuthoredArticles = data['hits'] ? data['hits'].total : 0;
            this.authoredArticleLinks.last = Math.ceil(this.totalAuthoredArticles / 10);
            if (data['hits'] && data['hits'].hits) {
                for (let i = 0; i < data['hits'].hits.length; i++) {
                    this.authoredArticles.push(data['hits'].hits[i]);
                }
            }
            this.loadTabs();
        }, error => {
            this.authoredArticles = null;
        });
    }

    loadAuthoredArticlesPage(page) {
        this.authoredArticlePage = page;
        this.authoredArticleFrom = (this.authoredArticlePage === 0) ? 0 : (this.authoredArticlePage * 10);
        if (this.authoredArticleFrom <= this.totalAuthoredArticles) {
            this.loadAllAuthoredArticles(this.authoredArticleFrom);
        }
    }

    resetTree() {
        this.dataIndustryTree = null;
        this.dataCategoryTree = null;
        this.dataFeaturesTree = null;
        this.dataProviderType = null;
        this.dataSourceTree = null;
        this.dataProviderTagsTree = null;
        this.dataGeographicalFociTree = null;
        this.dataSecurityTypesTree = null;
        this.dataRelevantSectorsTree = null;
        this.dataInvestorTypesTree = null;
        this.dataMainAssetClassTree = null;
        this.dataSampleOrPanelSizeTree = null;
        this.dataPublicCompaniesCoveredTree = null;
        this.dataHistoricalDateRangeEstimateTree = null;
        this.dataDateCollectionRangeExplanationTree = null;
        this.dataCollectionMethodsExplanationTree = null;
        this.datadatasetSizeTree = null;
        this.datadatasetNumberOfRowsTree = null;
        this.datanumberOfDataSourcesTree = null;
        this.datanumberOfAnalystEmployeesTree = null;
        this.dataDataSourcesDetailsTree = null;
        this.datayearFirstDataProductLaunchedTree = null;
        this.datasecRegulatedTree = null;
        this.datainvestorClientBucketTree = null;
        this.dataInvestorClientsCountTree = null;
        this.dataTargetOrganizationTypeTree = null;
        this.dataManagerTypeTree = null;
        this.dataProfileVerificationStatusTree = null;
        this.dataStrategiesTree = null;
        this.dataResearchStylesTree = null;
        this.dataInvestingTimeFrameTree = null;
        this.dataEquitiesMarketCapTree = null;
        this.dataEquitiesStyleTree = null;
        this.dataAccessOfferedTree = null;
        this.dataDataProductTypesTree = null;
        this.dataDataTypesTree = null;
        this.dataPublicEquitiesCoveredRangeTree = null;
        this.dataPublicEquitiesCoveredCountTree = null;
        this.dataIdentifiersAvailableTree = null;
        this.dataPointInTimeAccuracyTree = null;
        this.dataDataGapsTree = null;
        this.dataDataGapsReasonsTree = null;
        this.dataDuplicatesCleanedTree = null;
        this.dataIncludesOutliersTree = null;
        this.dataOutlierReasonsTree = null;
        this.dataBiasesTree = null;
        this.dataSampleOrPanelBiasesTree = null;
        this.dataNormalizedTree = null;
        this.dataLagTimeTree = null;
        this.dataCompetitiveDifferentiatorsTree = null;
        this.dataDataLanguagesAvailableTree = null;
        this.dataDeliveryMethodsTree = null;
        this.dataDeliveryFrequencyNotesTree = null;
        this.dataDeliveryFrequenciesTree = null;
        this.dataDataUpdateFrequencyTree = null;
        this.dataKeyDataFieldsTree = null;
        this.datadataUpdateFrequencyNotesTree = null;
        this.dataDeliveryFormatsTree = null;
        this.dataLicensingAndTrialingTree = null;
        this.dataProductDetailsTree = null;
        this.dataDataLicenseTypeDetailsTree = null;
        this.dataDataLicenseTypeTree = null;
        this.dataPotentialDrawbacksTree = null;
        this.dataUniqueValuePropsTree = null;
        this.dataLegalAndComplianceIssuesTree = null;
        this.dataNotesTree = null;
        this.datascoreInvestorsWillingnessTree = null;
        this.dataOurViewsTree = null;
        this.datascoreUniquenessTree = null;
        this.datascoreValueTree = null;
        this.datascoreReadinessTree = null;
        this.dataUseCasesOrQuestionsAddressedTree = null;
        this.dataScoreOverallTree = null;
        this.dataDataRoadmapTree = null;
        this.dataCleanlinessTree = null;
        this.dataAnalyticsTree = null;
        this.dataOwnerOrganizationTree = null;
        this.dataOwnerOrganizationNameTree = null;
        this.dataOwnerOrganizationLocationTree = null;
        this.dataOwnerOrganizationYearFoundedTree = null;
        this.dataOwnerOrganizationyearExitTree = null;
        this.dataOwnerOrganizationheadcountBucketTree = null;
        this.dataOwnerOrganizationheadcountNumberTree = null;
        this.dataOwnerOrganizationFinancialPositionTree = null;
        this.dataOwnerOrganizationHeadCountTree = null;
        this.dataOwnerOrganizationClientBaseTree = null;
        this.dataShortDescriptionTree = null;
        this.dataLongDescriptionTree = null;
        this.dataSummaryTree = null;
        this.dataInvestorClientsTree = null;
        this.dataLicensingTree = null;
        this.dataFreeTrialAvailableTree = null;
    }

    loadAllTree(data) {
        this.resetTree();
        
        if(this.restrictedAuthority)
        {
            //data.pricingModel = '';
            data.monthlyPriceRange = '';
            data.pricingDetails = '';
            data.minimumYearlyPrice = '';
            data.maximumYearlyPrice = '';
            data.freeTrialAvailable = ''
            data.trialPricingModel = '';
            data.trialPricingDetails = '';
            data.competitiveDifferentiators = '';
            //console.log('pricingmodel',data.pricingModel)
        }
       // this.getTagList(this.dataProvider.id);
        if (this.dataProvider && this.dataProvider.shortDescription) {
            this.dataShortDescriptionTree = [
                {
                    desc: 'In Brief:',
                    children: [{desc: data.shortDescription}],
                    tooltip: 'Short description of the specific types of data or information that the provider collects/provides/analyzes.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.longDescription) {
            this.dataLongDescriptionTree = [
                {
                    desc: 'In-Depth:',
                    children: [{desc: data.longDescription, locked: data.locked}],
                    tooltip: 'Longer description of the provider, its datasets, and wider business.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.profileVerificationStatus) {
            this.dataProfileVerificationStatusTree = [
                {
                    desc: data.profileVerificationStatus && data.profileVerificationStatus.description !=='' ? 'Profile Verification Status:' : '',
                    children: data.profileVerificationStatus && data.profileVerificationStatus.description !=='' ? [{desc: data.profileVerificationStatus.description, locked: data.locked}] : null,
                    tooltip: 'The amount of input the provider has had on the information in this data profile.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.providerType && this.dataProvider.providerType) {
            this.dataProviderType = [
                {
                    desc: 'Data Provider Type:',
                    children: [{ id: data.providerType.id, desc: data.providerType.providerType, tooltip: data.providerType.explanation }],
                    tooltip: 'At what step in the data monetization process does the provider sit? In other words, how does the provider handle data and what does it do with it?',
                    params: {
                        filter: 'providerType.id',
                        type: 'horizantal-checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Overview - Provider Type - ',
                        labelSuffix: 'desc',
                        category: 'Data Provider Type',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if ((this.dataProvider && this.dataProvider.mainDataIndustry && this.dataProvider.mainDataIndustry.id && this.dataProvider.mainDataIndustry.dataIndustry !== 'DATA_INDUSTRY_NULL') || (this.dataProvider && this.dataProvider.mainDataIndustry && this.dataProvider.industries && this.dataProvider.industries.length)) {
            const industry = [];
            const mainDataIndustry = {
                id: data.mainDataIndustry.id,
                desc: data.mainDataIndustry.dataIndustry,
                tooltip: data.mainDataIndustry.explanation,
                locked: data.mainDataIndustry.locked,
                font: 'bold'
            };
            if (data.mainDataIndustry && data.mainDataIndustry.dataIndustry !== 'DATA_INDUSTRY_NULL') {
                industry.push(mainDataIndustry);
            }
            if (this.dataProvider.industries) {
                for(let i=0; i < data.industries.length; i++) {
                    if (data.industries[i].desc !== data.mainDataIndustry.dataIndustry) {
                        data.industries[i].tooltip = this.getTooltip(this.industryExplanation, data.industries[i].id)
                        industry.push(data.industries[i]);
                    }
                }
            }
            this.dataIndustryTree = [{
                    label: 'Data Industries:',
                    children: industry,
                    tooltip: 'All the industries that the provider operates in and provides data to.',
                    params: {
                        filter: 'mainDataIndustry.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Overview - Data Industries - ',
                        labelSuffix: 'desc',
                        category: 'Data Industries',
                        event: 'Internal Link Clicked'
                    }
                }];
        }
        if (
            (this.dataProvider && this.dataProvider.mainDataCategory && this.dataProvider.mainDataCategory.datacategory !== 'DATA_CATEGORY_NULL') ||
             (this.dataProvider && this.dataProvider.categories && this.dataProvider.categories.length)) {

                const category = [];
                if(this.dataProvider && this.dataProvider.mainDataCategory && this.dataProvider.mainDataCategory.datacategory !== 'DATA_CATEGORY_NULL') {
                    const mainCat = this.categoryLogo.filter(v => {
                        return v.id === data.mainDataCategory.id;
                    });
                    const mainDataCategory = {
                        id: data.mainDataCategory.id,
                        desc: data.mainDataCategory.datacategory,
                        tooltip: data.mainDataCategory.explanation,
                        locked: data.mainDataCategory.locked,
                        font: 'bold',
                        logo: mainCat && mainCat.length ? mainCat[0].logo : null
                    };
                    if (data.mainDataCategory && data.mainDataCategory.datacategory !== 'DATA_CATEGORY_NULL') {
                        category.push(mainDataCategory);
                    }
                }

                if (data.categories) {
                    for(let i=0; i < data.categories.length; i++) {
                        const value = data.mainDataCategory && data.mainDataCategory.datacategory;
                        if (data.categories[i].desc !== value) {
                            const cat = this.categoryLogo.filter(v => {
                                return v.id.toString() === data.categories[i].id;
                            });
                            category.push({
                                id: data.categories[i].id,
                                desc: data.categories[i].desc,
                                locked: data.categories[i].locked,
                                logo: cat && cat.length ? cat[0].logo : null,
                                tooltip: this.getTooltip(this.categoryExplanation, data.categories[i].id)
                            });
                        }
                    }
                }
            this.dataCategoryTree = [
                {
                    desc: 'Data Categories:',
                    children: category,
                    tooltip: 'All the data categories this provider collects/provides/analyzes.',
                    params: {
                        filter: 'mainDataCategory.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Overview - Data Categories - ',
                        labelSuffix: 'desc',
                        category: 'Data Categories',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.features && this.dataProvider.features.length) {
            this.dataFeaturesTree = [
                {
                    desc: 'Data Features:',
                    children: data.features.map(item => {
                        item.tooltip = this.getTooltip(this.featureExplanation, item.id);
                        return item;
                    }),
                    tooltip: 'The specific data or information being collected or organized by the provider.',
                    params: {
                        filter: 'features.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Overview - Data Features - ',
                        labelSuffix: 'desc',
                        category: 'Data Features',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.sources && this.dataProvider.sources.length) {
            this.dataSourceTree = [
                {
                    desc: 'Data Sources:',
                    children: data.sources.map(item => {
                        item.tooltip = this.getTooltip(this.dataSourceExplanation, item.id);
                        return item;
                    }),
                    tooltip: 'The original (physical) source of the data being produced by this provider, not to be confused with external Provider Providers.',
                    params: {
                        filter: 'sources.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Overview - Data Sources - ',
                        labelSuffix: 'desc',
                        category: 'Data Sources',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.numberOfDataSources) {
            this.datanumberOfDataSourcesTree = [
                {
                    desc: data.numberOfDataSources && data.numberOfDataSources.description !=='' ? 'Approximate Number of Data Sources:' : '',
                   // children: data.numberOfDataSources && data.numberOfDataSources.description !=='' ? [{ desc: data.numberOfDataSources.description, locked: data.locked }] : null,
                    children: data.numberOfDataSources && data.numberOfDataSources.description !=='' ? [{ desc: data.numberOfDataSources.description}] : null,
                    tooltip: 'The approximate number of unique sources the provider receives/gathers data from.',
                    params: {
                        filter: 'numberOfDataSources.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Overview - Number of Data Sources - ',
                        labelSuffix: 'description',
                        category: 'Number of Data Sources',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.dataSourcesDetails) {
            this.dataDataSourcesDetailsTree = [
                {
                    desc: 'Data Sources Details:',
                    children: [{ desc: data.dataSourcesDetails, locked: data.locked }],
                    tooltip: 'More details on exactly where and how the data provider acquired their data.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.providerTags && this.dataProvider.providerTags.length) {
            this.dataProviderTagsTree = [
                {
                    desc: 'All Tags:',
                    children: data.providerTags.map(item => {
                        item.tooltip = this.providerTagExplanation[item.id];
                        return item;
                    }),
                    tooltip: 'Special circumstances for the provider.',
                    params: {
                        filter: 'providerTags.id',
                        type: 'horizontal-checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Overview - Tags - ',
                        labelSuffix: 'desc',
                        category: 'Tags',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.geographicalFoci && this.dataProvider.geographicalFoci.length) {
            this.dataGeographicalFociTree = [
                {
                    desc: 'Geographies Covered:',
                    children: data.geographicalFoci,
                    tooltip: 'The geographies that the data provider focuses on collecting/providing/analyzing data about.',
                    params: {
                        filter: 'geographicalFoci.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Overview - Geographies Covered - ',
                        labelSuffix: 'description',
                        category: 'Geographies Covered',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if ((this.dataProvider && this.dataProvider.mainAssetClass && this.dataProvider.mainAssetClass !== 'DEFAULT_NULL') && this.dataProvider.mainAssetClass.length ||
        (this.dataProvider && this.dataProvider.assetClasses &&  this.dataProvider.assetClasses.length)) {
            const assetclass = [];
             console.log('lookpupcoder',this.lookupcode);
            const mainAssetClass ={
                id: this.lookupcode && this.lookupcode.id ? this.lookupcode.id : '',
                desc: data.mainAssetClass ? data.mainAssetClass : '',
                locked: data.mainAssetClass && data.mainAssetClass.locked ? data.mainAssetClass.locked : false,
                font: 'bold'
            };
       
          if (data.mainAssetClass !== 'DEFAULT_NULL' && mainAssetClass) {
                assetclass.push(mainAssetClass);
            }  
            /* if (data.assetClasses && data.mainAssetClass) {
                for(let i=0; i < data.assetClasses.length; i++) {
                        if(data.mainAssetClass && data.mainAssetClass !== 'DEFAULT_NULL' && data.assetClasses[i].desc === data.mainAssetClass){
                            assetclass.push({
                                id: data.assetClasses[i].id,
                                desc: data.assetClasses[i].desc,
                                locked: data.assetClasses[i].locked ? data.assetClasses[i].locked : false,
                                font: 'bold'
                            });
                        }
                }
            } */
            if (data.assetClasses) {
                for(let i=0; i < data.assetClasses.length; i++) {
                        if (data.assetClasses[i].desc !== data.mainAssetClass) {
                            assetclass.push({
                                id: data.assetClasses[i].id,
                                desc: data.assetClasses[i].desc,
                                locked: data.assetClasses[i].locked ? data.assetClasses[i].locked : false
                            });
                        }
                }
            }
            console.log('assetClass',assetclass);
            this.dataMainAssetClassTree = [
                {
                    desc: 'Asset Classes:',
                    children: assetclass,
                    tooltip: 'All of the financial security asset classes that the providers data can be used for or has any relevance towards.',
                    params: {
                        filter: 'assetClasses.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Overview - Asset Classes - ',
                        labelSuffix: 'description',
                        category: 'Asset Classes',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.securityTypes &&  this.dataProvider.securityTypes.length) {
            this.dataSecurityTypesTree = [
                {
                    desc: 'Security Types:',
                    children: data.securityTypes,
                    tooltip: 'All of the financial security types the providers data can be used for or has any relevance towards.',
                    params: {
                        filter: 'securityTypes.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Overview - Security Types - ',
                        labelSuffix: 'desc',
                        category: 'Security Types',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.relevantSectors &&  this.dataProvider.relevantSectors.length) {
            this.dataRelevantSectorsTree = [
                {
                    desc: 'Sectors:',
                    children: data.relevantSectors,
                    tooltip: 'All of the sectors the providers data could be used for or has any relevance towards.',
                    params: {
                        filter: 'relevantSectors.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Overview - Sectors - ',
                        labelSuffix: 'desc',
                        category: 'Sectors ',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.ownerOrganization && this.dataProvider.ownerOrganization.parentOrganization) {
            this.dataOwnerOrganizationTree = [
                {
                    desc: 'Owners Parent:',
                    children: [{desc: data.ownerOrganization.parentOrganization.name, locked: data.locked? data.locked : false}],
                    tooltip: 'Parent company of the owner organization (if there is one).',
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.ownerOrganization) {
            if(this.dataProvider.ownerOrganization.name && this.dataProvider.ownerOrganization.name.length && this.dataProvider.ownerOrganization.name !=='NAME_NULL') {
                this.dataOwnerOrganizationNameTree = [
                    {
                        desc: 'Owner:',
                        children: [{desc: data.ownerOrganization.name, locked: data.locked ? data.locked : false}],
                        tooltip: 'Organization that owns the provider (if different than the provider itself).'
                    }
                ];
            }
        }
        if (this.dataProvider && this.dataProvider.ownerOrganization && (this.dataProvider.ownerOrganization.city ||
            this.dataProvider.ownerOrganization.state || this.dataProvider.ownerOrganization.country || this.dataProvider.ownerOrganization.zipCode)) {
                if ((this.dataProvider.ownerOrganization.city && this.dataProvider.ownerOrganization.city.length && this.dataProvider.ownerOrganization.city !=='CITY_NULL') ||
                    (this.dataProvider.ownerOrganization.state && this.dataProvider.ownerOrganization.state.length && this.dataProvider.ownerOrganization.state !=='STATE_NULL') ||
                    (this.dataProvider.ownerOrganization.country && this.dataProvider.ownerOrganization.country.length && this.dataProvider.ownerOrganization.country !=='COUNTRY_NULL') ||
                    (this.dataProvider.ownerOrganization.zipCode && this.dataProvider.ownerOrganization.zipCode.length && this.dataProvider.ownerOrganization.zipCode !=='ZIPCODE_NULL')) {
                    this.dataOwnerOrganizationLocationTree = [
                        {
                            desc: 'Headquarters Location:',
                            children: [
                                {desc: [data.ownerOrganization.city, data.ownerOrganization.state, data.ownerOrganization.country, data.ownerOrganization.zipCode].filter(Boolean).join(', '), locked: data.locked}
                            ],
                            tooltip: 'Location of the provider/owners headquarters.'
                        }
                    ];
                }
        }
        if (this.dataProvider && this.dataProvider.ownerOrganization && this.dataProvider.ownerOrganization.yearFounded) {
            this.dataOwnerOrganizationYearFoundedTree = [
                {
                    desc: 'Year Founded:',
                    children: [{id: data.ownerOrganization.yearFounded, desc: data.ownerOrganization.yearFounded, locked: data.locked ? data.locked : false}],
                    tooltip: 'Year the provider or its owner was founded.',
                    params: {
                        filter: 'ownerOrganization.yearFounded',
                        type: 'rangeFromTo'
                    },
                    routing: '/providers'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.ownerOrganization && this.dataProvider.ownerOrganization.yearExit) {
            this.dataOwnerOrganizationyearExitTree = [
                {
                    desc: 'Year Exited:',
                    children: [{id: data.ownerOrganization.yearExit, desc: data.ownerOrganization.yearExit, locked: data.locked ? data.locked : false}],
                    tooltip: 'Year the company underwent an ownership change, whether through acquisition or public offering.',
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.ownerOrganization && this.dataProvider.ownerOrganization.headcountBucket && this.dataProvider.ownerOrganization.headcountBucket.description) {
            this.dataOwnerOrganizationheadcountBucketTree = [
                {
                    desc: 'Headcount Bucket:',
                    children: [{id: data.ownerOrganization.headcountBucket.description, desc: data.ownerOrganization.headcountBucket.description}],
                    tooltip: 'An approximate range of the number of people that work at the organization.',
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.ownerOrganization && this.dataProvider.ownerOrganization.headcountNumber) {
            this.dataOwnerOrganizationheadcountNumberTree = [
                {
                    desc: 'Headcount:',
                    children: [{id: data.ownerOrganization.headcountNumber, desc: data.ownerOrganization.headcountNumber, locked: data.locked ? data.locked : false}],
                    tooltip: 'The precise number of people that work at the organization.',
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.ownerOrganization && this.dataProvider.ownerOrganization.headCount) {
            this.dataOwnerOrganizationHeadCountTree = [
                {
                    desc: 'Headcount Details:',
                    children: [{id: data.ownerOrganization.headCount, desc: data.ownerOrganization.headCount, locked: data.locked}],
                    tooltip: 'Details regarding the number of people that work at the organization.',
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.numberOfAnalystEmployees) {
            this.datanumberOfAnalystEmployeesTree = [
                {
                    desc: 'Number of Analyst Employees:',
                    children: [{desc: data.numberOfAnalystEmployees, locked: data.locked}]
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.ownerOrganization && this.dataProvider.ownerOrganization.financialPosition) {
            this.dataOwnerOrganizationFinancialPositionTree = [
                {
                    desc: 'Financial Position:',
                    children: [{id: data.ownerOrganization.financialPosition, desc: data.ownerOrganization.financialPosition, locked: data.locked}],
                    tooltip: 'Summary of the financial situation of the provider or its owner..'
                }
            ];
        }

        /* Detail page */
        if (this.dataProvider && this.dataProvider.summary) {
            this.dataSummaryTree = [
                {
                    desc: 'Summary:',
                    children: [{ desc: data.summary, locked: data.locked }],
                    tooltip: 'Brief summary of providers offerings.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.yearFirstDataProductLaunched) {
            this.datayearFirstDataProductLaunchedTree = [
                {
                    desc: 'Year First Data Product Launched:',
                    children: [{ desc: data.yearFirstDataProductLaunched, locked: data.locked }],
                    tooltip: 'The year the provider launched their first data product to external clients.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.secRegulated) {
            this.datasecRegulatedTree = [
                {
                    desc: data.secRegulated && data.secRegulated.description !=='' ? 'SEC - Regulated:' :'',
                   // children: data.secRegulated && data.secRegulated.description !=='' ? [{ desc: data.secRegulated.description, locked: data.locked }]: null,
                    children: data.secRegulated && data.secRegulated.description !=='' ? [{ desc: data.secRegulated.description}]: null,
                    params: {
                        filter: 'secRegulated.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - SEC - Regulated - ',
                        labelSuffix: 'description',
                        category: 'SEC - Regulated',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.investorClientBucket) {
            this.datainvestorClientBucketTree = [
                {
                    desc: data.investorClientBucket && data.investorClientBucket !=='' ? 'Approximate Number of Investor Clients:' : '',
                    //children: data.investorClientBucket && data.investorClientBucket !=='' ? [{ desc: data.investorClientBucket, locked: data.locked }]: null,
                    children: data.investorClientBucket && data.investorClientBucket !=='' ? [{ desc: data.investorClientBucket}]: null,
                    tooltip: 'Approximate number of clients the provider has in the investment management industry.',
                    params: {
                        filter: 'investorClientBucket.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details -  Investor Client Bucket - ',
                        labelSuffix: 'description',
                        category: 'Investor Client Bucket',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.investorClientsCount) {
            this.dataInvestorClientsCountTree = [
                {
                    desc: 'Number of Investor Clients:',
                   // children: [{ desc: data.investorClientsCount, locked: data.locked }],
                    children: [{ desc: data.investorClientsCount}],
                    tooltip: 'Precise number of clients the provider has in the investment management industry.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.investor_clients) {
            this.dataInvestorClientsTree = [
                {
                    desc: 'Number of Investor Clients Details:',
                    children: [{ desc: data.investor_clients, locked: data.locked }],
                    tooltip: 'Details related to the approximate number of clients the provider has in the investment management industry.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.organizationType && this.dataProvider.organizationType.length) {
            this.dataTargetOrganizationTypeTree = [
                {
                    desc: 'Target Organization Type:',
                    children: data.organizationType,
                    tooltip: 'The types of organizations the provider\'s offerings would be useful to or would have the ability to use.',
                    params: {
                        filter: 'organizationType.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details -  Target Organization Type  - ',
                        labelSuffix: 'description',
                        category: 'Target Organization Type',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.ownerOrganization && this.dataProvider.ownerOrganization.clientBase) {
            this.dataOwnerOrganizationClientBaseTree = [
                {
                    desc: 'Client Base',
                    children: [{desc: data.ownerOrganization.clientBase, locked: data.locked}],
                    tooltip: 'Details about the company\'s current client base (both within investment management and otherwise), such as the number of clients and their common characteristics.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.managerType && this.dataProvider.managerType.length) {
            this.dataManagerTypeTree = [
                {
                    desc: 'Target Investment Manager Type:',
                    children: data.managerType,
                    tooltip: 'The types of investors the provider\'s data would be useful to or would have the ability to use.',
                    params: {
                        filter: 'managerType.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details -  Target Investment Manager Type  - ',
                        labelSuffix: 'description',
                        category: 'Target Investment Manager Type',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.investorTypes && this.dataProvider.investorTypes.length) {
            this.dataInvestorTypesTree = [
                {
                    desc: 'Investor Types:',
                    children: data.investorTypes,
                    tooltip: 'The types of investors the providers data would be useful for or would have the ability to use.',
                    params: {
                        filter: 'investorTypes.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Overview - Investor Types - ',
                        labelSuffix: 'description',
                        category: 'Investor Types',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.strategies && this.dataProvider.strategies.length) {
            this.dataStrategiesTree = [
                {
                    desc: 'Target Investment Managers\' Strategies:',
                    children: data.strategies,
                    tooltip: 'The investment strategies of investors the provider\'s data would be useful to or would have the ability to use.',
                    params: {
                        filter: 'strategies.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details -  Target Investment Manager Strategies  - ',
                        labelSuffix: 'description',
                        category: 'Target Investment Manager Strategies',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.researchStyles && this.dataProvider.researchStyles.length) {
            this.dataResearchStylesTree = [
                {
                    desc: 'Target Investment Managers\' Research Styles:',
                    children: data.researchStyles,
                    tooltip: 'The research styles of investors the provider\'s data would be useful to or would have the ability to use.',
                    params: {
                        filter: 'researchStyles.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details -  Target Investment Manager Research Styles - ',
                        labelSuffix: 'description',
                        category: 'Target Investment Manager Research Styles',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.investingTimeFrame && this.dataProvider.investingTimeFrame.length) {
            this.dataInvestingTimeFrameTree = [
                {
                    desc: 'Target Investment Managers\' Investing Time Frame:',
                    children: data.investingTimeFrame,
                    tooltip: 'The typical investment time frame of investors the provider\'s data would be useful to or would have the ability to use.',
                    params: {
                        filter: 'investingTimeFrame.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details -   Target Investment Manager Investing Time Frame - ',
                        labelSuffix: 'description',
                        category: ' Target Investment Manager Investing Time Frame',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.equitiesMarketCap && this.dataProvider.equitiesMarketCap.length) {
            this.dataEquitiesMarketCapTree = [
                {
                    desc: 'Target Investment Managers\' Public Equities Market Cap Focus:',
                    children: data.equitiesMarketCap,
                    tooltip: 'The size of the public companies that investors could use the provider\'s data to research.',
                    params: {
                        filter: 'equitiesMarketCap.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details -  Target Investment Manager Public Equities Market Cap - ',
                        labelSuffix: 'description',
                        category: 'Target Investment Manager Public Equities Market Cap',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.equitiesStyle && this.dataProvider.equitiesStyle.length) {
            this.dataEquitiesStyleTree = [
                {
                    desc: 'Target Investment Managers\' Public Equities Style:',
                    children: data.equitiesStyle,
                    tooltip: 'The style of public companies that investors could use the provider\'s data to research.',
                    params: {
                        filter: 'equitiesStyle.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details -  Target Investment Manager Public Equities Style - ',
                        labelSuffix: 'description',
                        category: 'Target Investment Manager Public Equities Style',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.accessOffered && this.dataProvider.accessOffered.length) {
            this.dataAccessOfferedTree = [
                {
                    desc: 'Access Offered:',
                    children: data.accessOffered,
                    tooltip: 'The level of access and visibility into the provider\'s research/analysis offered to clients.',
                    params: {
                        filter: 'accessOffered.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Details - Access Offered - ',
                        labelSuffix: 'description',
                        category: 'Access Offered',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.dataProductTypes && this.dataProvider.dataProductTypes.length) {
            this.dataDataProductTypesTree = [
                {
                    desc: 'Data Product Types:',
                    children: data.dataProductTypes,
                    tooltip: 'The types (or general format of delivery) of data products the provider offers to their clients.',
                    params: {
                        filter: 'dataProductTypes.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Details - Data Product Types - ',
                        labelSuffix: 'description',
                        category: 'Data Product Types ',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.dataTypes && this.dataProvider.dataTypes.length) {
            this.dataDataTypesTree = [
                {
                    desc: 'Data Types:',
                    children: data.dataTypes,
                    tooltip: 'The types of datasets (and their maturity) the provider offers to their clients. "N/A" indicates they don\'t offer any datasets externally.',
                    params: {
                        filter: 'dataTypes.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Details - Data Types - ',
                        labelSuffix: 'description',
                        category: 'Data Types',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.sampleOrPanelSize) {
            this.dataSampleOrPanelSizeTree = [
                {
                    desc: 'Sample or Panel Size:',
                    children: [{ desc: data.sampleOrPanelSize, locked: data.locked }],
                    tooltip: 'Details about the sample or panel size of the providers raw data.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.publicEquitiesCoveredRange && this.dataProvider.publicEquitiesCoveredRange.length) {
            this.dataPublicEquitiesCoveredRangeTree = [
                {
                    desc: data.publicEquitiesCoveredRange && data.publicEquitiesCoveredRange !=='' ? 'Approximate Number of Public Equities Covered:' : '',
                   // children: data.publicEquitiesCoveredRange && data.publicEquitiesCoveredRange !=='' ? [{ desc: data.publicEquitiesCoveredRange, locked: data.locked }] : null,
                    children: data.publicEquitiesCoveredRange && data.publicEquitiesCoveredRange !=='' ? [{ desc: data.publicEquitiesCoveredRange}] : null,
                    tooltip: 'The range of the number of public equities the provider\'s data offerings can be used to research.',
                    params: {
                        filter: 'publicEquitiesCoveredRange.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Details - Public Equities Covered Range - ',
                        labelSuffix: 'description',
                        category: 'Public Equities Covered Range',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.publicEquitiesCoveredCount) {
            this.dataPublicEquitiesCoveredCountTree = [
                {
                    desc: 'Public Equities Covered:',
                   // children: [{ desc: data.publicEquitiesCoveredCount, locked: data.locked }],
                    children: [{ desc: data.publicEquitiesCoveredCount}],
                    tooltip: 'The range of the number of public equities the provider\'s data offerings can be used to research.',
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.publicCompaniesCovered) {
            this.dataPublicCompaniesCoveredTree = [
                {
                    desc: 'Public Equities Covered Details:',
                  //  children: [{ desc: data.publicCompaniesCovered, locked: (data.locked || (!this.userLevel && !this.allowEditAccess && !this.unlockOperatorPermission && !this.userRole))}],
                    children: [{ desc: data.publicCompaniesCovered,locked: data.locked ? data.locked : false}],
                    tooltip: 'The public companies/tickers the providers data can be used for or are relevant towards.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.historicalDateRange) {
            this.dataHistoricalDateRangeEstimateTree = [
                {
                    desc: 'Historical Date Range Estimate:',
                    children: [{desc: data.historicalDateRange,locked: data.locked ? data.locked : false}],
                    tooltip: 'The estimated maximum amount of historical data the provider can provide.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.dateCollectionRangeExplanation) {
            this.dataDateCollectionRangeExplanationTree = [
                {
                    desc: 'Date Collection Range Explanation:',
                    children: [{ desc: data.dateCollectionRangeExplanation, locked: data.locked }],
                    tooltip: 'More details about how much historical data the provider can provide.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.collectionMethodsExplanation) {
            this.dataCollectionMethodsExplanationTree = [
                {
                    desc: 'Collection Methods Explanation:',
                    children: [{ desc: data.collectionMethodsExplanation, locked: data.locked }],
                    tooltip: 'Details about how and where the provider gets its raw data.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.datasetSize) {
            this.datadatasetSizeTree = [
                {
                    desc: 'Dataset Size:',
                    children: [{ desc: data.datasetSize}],
                    tooltip: ''
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.datasetNumberOfRows) {
            this.datadatasetNumberOfRowsTree = [
                {
                    desc: 'Dataset Number of Rows:',
                    children: [{ desc: data.datasetNumberOfRows}],
                    tooltip: ''
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.languagesAvailable && this.dataProvider.languagesAvailable.length) {
            this.dataDataLanguagesAvailableTree = [
                {
                    desc: 'Languages Available:',
                    children: data.languagesAvailable.map(item => ({
                        desc: item.description
                    })),
                    tooltip: 'Languages the providers data products are offered in.',
                    params: {
                        filter: 'languagesAvailable.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Details - Languages Available - ',
                        labelSuffix: 'description',
                        category: 'Languages Available',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.productDetails) {
            this.dataProductDetailsTree = [
                {
                    desc: 'Product Details:',
                    children: [{ desc: data.productDetails, locked: data.locked }],
                    tooltip: 'Further details regarding the provider\'s data products, such as details specific to certain products.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.identifiersAvailable && this.dataProvider.identifiersAvailable.length) {
            this.dataIdentifiersAvailableTree = [
                {
                    desc: 'Public Equity Identifiers Included:',
                    children: data.identifiersAvailable.map(item => ({
                        desc: item.description,
                        //locked: data.locked
                    })),
                    tooltip: 'The types of public company identifiers available in the dataset. "None" indicates the provider has not standardized the public company identifiers in their dataset.',
                    params: {
                        filter: 'identifiersAvailable.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Quality - Identifiers Available - ',
                        labelSuffix: 'description',
                        category: 'Identifiers Available',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.pointInTimeAccuracy) {
            this.dataPointInTimeAccuracyTree = [
                {
                    desc: data.pointInTimeAccuracy && data.pointInTimeAccuracy.description !=='' ? 'Point-in-Time Accuracy:' : '',
                    //children: data.pointInTimeAccuracy && data.pointInTimeAccuracy.description !=='' ? [{ desc: data.pointInTimeAccuracy.description, locked: data.locked }] : null,
                    children: data.pointInTimeAccuracy && data.pointInTimeAccuracy.description !=='' ? [{ desc: data.pointInTimeAccuracy.description}] : null,
                    tooltip: 'If corporate actions that affect the company identifiers in the provider\'s data are tracked, are those identifiers accurate at each particular point-in-time? Would their dataset suffer from any survivorship bias?',
                    params: {
                        filter: 'pointInTimeAccuracy.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Quality - Point-in-Time Accuracy - ',
                        labelSuffix: 'description',
                        category: 'Point-in-Time Accuracy',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.dataGaps) {
            this.dataDataGapsTree = [
                {
                    desc: 'Data Gaps:',
                  //  children: [{ desc: data.dataGaps.description, locked: data.locked }],
                    children: [{ desc: data.dataGaps.description}],
                    tooltip: 'Does the provider data offerings include any data gaps? In other words, are there unexpected missing data?',
                    params: {
                        filter: 'dataGaps.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Quality - Data Gaps - ',
                        labelSuffix: 'description',
                        category: 'Data Gaps',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.dataGapsReasons && this.dataProvider.dataGapsReasons.length) {
            this.dataDataGapsReasonsTree = [
                {
                    desc: 'Data Gaps Reasons:',
                    children: data.dataGapsReasons.map(item => ({
                        //desc: item.description
                        desc: item.lookupcode
                    })),
                    tooltip: 'What are the reasons that there is missing data in this dataset?:"Missing completely at random": missing data has no relation to outcomes."Missing at random": missing data tends to happen in certain situations but has no relation to outcomes."Missing not at random": missing data is usually due to a common connection between the outcomes.',
                    params: {
                        filter: 'dataGapsReasons.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Quality - Data Gaps Reasons - ',
                        labelSuffix: 'description',
                        category: 'Data Gaps Reasons',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.duplicatesCleaned) {
            this.dataDuplicatesCleanedTree = [
                {
                    desc: 'Duplicates Cleaned:',
                   // children: [{ desc: data.duplicatesCleaned.description, locked: data.locked }],
                    children: [{ desc: data.duplicatesCleaned.description}],
                    tooltip: 'Has the provider scrubbed out the duplicates from their data offerings?',
                    params: {
                        filter: 'duplicatesCleaned.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Quality - Duplicates Cleaned - ',
                        labelSuffix: 'description',
                        category: 'Duplicates Cleaned',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.includesOutliers) {
            this.dataIncludesOutliersTree = [
                {
                    desc: 'Includes Outliers:',
                   // children: [{ desc: data.includesOutliers.description, locked: data.locked }],
                    children: [{ desc: data.includesOutliers.description}],
                    tooltip: 'Are any data points that do not follow the patterns of the vast majority of the rest of the data?',
                    params: {
                        filter: 'includesOutliers.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Quality - Includes Outliers - ',
                        labelSuffix: 'description',
                        category: 'Includes Outliers',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.outlierReasons && this.dataProvider.outlierReasons.length) {
            this.dataOutlierReasonsTree = [
                {
                    desc: 'Outlier Reasons:',
                    children: data.outlierReasons.map(item => ({
                        //desc: item.description
                        desc: item.lookupcode
                    })),
                    tooltip: 'Why do outliers occur in their dataset?:"Expected": outliers are expected based on unique nature of the dataset."Inaccurate measurements": the way the data has been measured (e.g. sensors) produce inaccurate measurements."Collection Methodology": the collection methodology produce inaccurate data."Clerical": the human element (e.g. transcription) produce inaccurate data."Technical": the technical element (e.g. software bugs) produce inaccurate data.',
                    params: {
                        filter: 'outlierReasons.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Quality -  Outlier Reasons - ',
                        labelSuffix: 'description',
                        category: ' Outlier Reasons',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.biases && this.dataProvider.biases.length) {
            this.dataBiasesTree = [
                {
                    desc: 'Biases:',
                    children: data.biases.map(item => ({
                        //desc: item.description
                        desc: item.lookupcode
                    })),
                    tooltip: 'All of the types of biases the provider\'s data offerings contain:"Selection - Demographic": participants are more likely to be selected for the study based on their demographic makeup "Selection - Geographic": participants are more likely to be selected for the study based on their geographical location "Selection - Time": a study being terminated or started due to timing "Selection - Exclusion": the systematic exclusion of certain individuals or data from the study "Selection - Other": any other biases related to the selection of the study participants, such as Self-Selection Bias "Survivorship": concentrating the study on things that made it past some selection process and overlooking those that did not, typically because of their lack of visibility (e.g. excluding companies that no longer exist) "Overfitting": modeling error which occurs when a function is too closely fit to a limited set of data points, thus making the model overly complex "Underfitting": modeling error which occurs when a function cannot capture the underlying trend of the data, thus making the model overly simple "Omitted Variable": the study leaves out one or more relevant variables "Confirmation": the tendency to favor information in a way that confirms one\'s preexisting beliefs or hypotheses "Detection": when a phenomenon is more likely to be observed for a particular set of study subjects "Recall": systematic errors cause by differences in the accuracy or completeness of the recollections retrieved by study participants regarding events or experiences from the past "Observer": arises when the researcher subconsciously influences the experiment "Funding": the tendency of the study to support the interests of the study\'s financial sponsor "Methodological": arising from the way results are collected (e.g. asking survey questions in a specific order to incite specific responses) "Reporting": the tendency of a study to support the interests of the study\'s financial sponsor "Analytical": arising from the way results are evaluated (e.g. assuming normality of a dataset)',
                    params: {
                        filter: 'biases.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Quality -  Biases - ',
                        labelSuffix: 'description',
                        category: 'Biases',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.sampleOrPanelBiases) {
            this.dataSampleOrPanelBiasesTree = [
                {
                    desc: 'Sample or Panel Biases Details:',
                    children: [{ desc: data.sampleOrPanelBiases, locked: data.locked }],
                    tooltip: ''
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.normalized) {
            this.dataNormalizedTree = [
                {
                    desc: 'Normalized:',
                    //children: [{ desc: data.normalized.description, locked: data.locked }],
                    children: [{ desc: data.normalized.description}],
                    tooltip: 'Have you normalized your dataset for things like seasonality, selection biases (e.g. overweighted in a particular demographic), or attrition (ie panel growing or declining over time)?',
                    params: {
                        filter: 'normalized.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Quality -  Normalized - ',
                        labelSuffix: 'description',
                        category: 'Normalized',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.keyDataFields) {
            this.dataKeyDataFieldsTree = [
                {
                    desc: 'Key Data Fields:',
                    children: [{ desc: data.keyDataFields, locked: data.locked }],
                    tooltip: 'The most important data fields within the providers data.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.dataUpdateFrequency) {
            this.dataDataUpdateFrequencyTree = [
                {
                    desc: 'Data Update Frequency:',
                    //children: [{ desc: data.dataUpdateFrequency, locked: ((data.locked || !this.userLevel) && !this.allowEditAccess && !this.unlockOperatorPermission && !this.userRole)}],
                    children: [{ desc: data.dataUpdateFrequency}],
                    tooltip: 'How often does the provider typically update its data products?'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.dataUpdateFrequencyNotes) {
            this.datadataUpdateFrequencyNotesTree = [
                {
                    desc: 'Data Update Frequency Notes:',
                    children: [{ desc: data.dataUpdateFrequencyNotes, locked: data.locked }],
                    tooltip: 'More details about the frequency that the provider updates their data products.'
                }
            ];
        }
        /* if (this.dataProvider && this.dataProvider.deliveryFrequencies && this.dataProvider.deliveryFrequencies.length) {
            for (let i = 0; i < this.dataProvider.deliveryFrequencies.length; i++) {
                const frequencyList = this.dataProvider.deliveryFrequencies[i].description;
                console.log('...delivery frequency...', frequencyList);
            }
            this.dataDeliveryFrequenciesTree = [
                {
                    desc: 'Delivery Frequency:',
                    children: [{desc: frequencyList, locked:((data.locked || !this.userLevel) && !this.allowEditAccess)}],
                    tooltip: 'How often does the provider typically deliver its data or analytics?',
                    params: {
                        filter: 'deliveryFrequencies.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Products Details - Delivery Frequency - ',
                        labelSuffix: 'description',
                        category: 'Delivery Frequency',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        } */
       
        
        if ((this.dataProvider && this.dataProvider.deliveryFrequencies && this.dataProvider.deliveryFrequencies.length)) {
            const deliveryFrequency = [];
            if (this.dataProvider.deliveryFrequencies) {
                for(let i=0; i < data.deliveryFrequencies.length; i++) {
                    const frequencies = {
                        id: data.deliveryFrequencies[i].id,
                        desc: data.deliveryFrequencies[i].description,
                        //locked: ((data.deliveryFrequencies[i].locked ) || (!this.userLevel && !this.allowEditAccess && !this.unlockOperatorPermission &&!this.userRole))
                    };
                    deliveryFrequency.push(frequencies);
                }

            }
            this.dataDeliveryFrequenciesTree = [
                {
                    desc: 'Delivery Frequency:',
                    children: deliveryFrequency,
                   /*  children: data.deliveryFrequencies, */
                    tooltip: 'How often does the provider typically deliver its data or analytics?',
                    params: {
                        filter: 'deliveryFrequencies.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Products Details - Delivery Frequency - ',
                        labelSuffix: 'description',
                        category: 'Delivery Frequency',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.lagTime) {
            this.dataLagTimeTree = [
                {
                    desc: 'Lag Time:',
                    children: [{desc: data.lagTime, locked: data.locked}],
                    tooltip: 'The time period between when the provider collects or observes the data and the data buyer can retrieve their data.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.deliveryFrequencyNotes) {
            this.dataDeliveryFrequencyNotesTree = [
                {
                    desc: 'Delivery Frequency Details:',
                    children: [{desc: data.deliveryFrequencyNotes, locked: data.locked}],
                    tooltip: 'The methods the provider typically uses to deliver its data products.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.deliveryMethods && this.dataProvider.deliveryMethods.length) {
            this.dataDeliveryMethodsTree = [
                {
                    desc: 'Delivery Methods:',
                    children:  data.deliveryMethods,
                    tooltip: 'The methods the provider typically uses to deliver its data products.',
                    params: {
                        filter: 'deliveryMethods.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Products Details - Delivery Methods - ',
                        labelSuffix: 'desc',
                        category: 'Delivery Methods',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.deliveryFormats && this.dataProvider.deliveryFormats.length) {
            this.dataDeliveryFormatsTree = [
                {
                    desc: 'Delivery Formats:',
                    children: data.deliveryFormats,
                    tooltip: 'The formats the provider typically deliver its data products in.',
                    params: {
                        filter: 'deliveryFormats.id',
                        type: 'checkbox'
                    },
                    routing: '/providers',
                    ga: {
                        labelPrefix: 'Provider Profile - Details - Data Products Details - Delivery Formats -  ',
                        labelSuffix: 'desc',
                        category: 'Delivery Formats',
                        event: 'Internal Link Clicked'
                    }
                }
            ];
        }
        if ((this.dataProvider && this.dataProvider.dataLicenseType) ||
        (this.dataProvider && this.dataProvider.dataLicenseTypeDetails) ||
        (this.dataProvider && this.dataProvider.dataProvidersPricingModels) ||
        (this.dataProvider && this.dataProvider.dataRoadmap) ||
        (this.dataProvider &&  this.dataProvider.freeTrialAvailable)) {

            // if(data.pricingModel) {
            //     data.pricingModel.forEach(element => {
            //      element.locked = data.locked;
            //      return element;
            //     });
            // }
            const dataLicensingAndTrialingTreeChildren = [];
            if((data.dataLicenseType) || (data.dataLicenseTypeDetails) || (data.paymentMethodsOffered && data.paymentMethodsOffered.length)) {
                dataLicensingAndTrialingTreeChildren.push({
                    desc: (data.dataLicenseType || data.dataLicenseTypeDetails || data.paymentMethodsOffered) ? 'Licensing:' : '',
                    children: (data.dataLicenseType || data.dataLicenseTypeDetails || data.paymentMethodsOffered) ? [
                        {
                            desc: data.dataLicenseType && data.dataLicenseType.description ? 'License Type:' : '',
                            children: data.dataLicenseType && data.dataLicenseType.description ? [{desc: data.dataLicenseType.description, locked: data.locked}] : null,
                            tooltip: 'The type of data license offered by the provider for its data products.',
                            params: {
                                filter: 'dataLicenseType.id',
                                type: 'checkbox'
                            },
                            routing: '/providers',
                            ga: {
                                labelPrefix: 'Provider Profile - Licensing & Pricing - Licensing - License Type - ',
                                labelSuffix: 'desc',
                                category: 'License Type',
                                event: 'Internal Link Clicked'
                            }
                        },
                        {
                            desc: data.dataLicenseTypeDetails ? 'License Details:' : '',
                            children: data.dataLicenseTypeDetails ? [{desc: data.dataLicenseTypeDetails, locked: data.locked}] : null,
                            tooltip: 'Relevant additional details regarding the provider\'s data licenses.'
                        },
                        {
                            desc: data.paymentMethodsOffered && data.paymentMethodsOffered.length ? 'Payment Methods Offered:' : '',
                            children: data.paymentMethodsOffered && data.paymentMethodsOffered.length ? data.paymentMethodsOffered.map(item => ({
                                desc: item.description,
                                locked: data.locked
                            })) : null,
                            params: {
                                filter: 'paymentMethodsOffered.id',
                                type: 'checkbox'
                            },
                            routing: '/providers',
                            ga: {
                                labelPrefix: 'Provider Profile - Licensing & Pricing - Licensing - Payment Methods Offered  - ',
                                labelSuffix: 'desc',
                                category: 'Payment Methods Offered',
                                event: 'Internal Link Clicked'
                            }
                        }
                    ] : null
                })
            }
            // if(data.pricingModel) {
            //     data.pricingModel.forEach(element => {
            //      element.locked = data.locked;
            //      return element;
            //     });
            // }
           
            if ((this.dataProvider && this.dataProvider.dataProvidersPricingModels && this.dataProvider.dataProvidersPricingModels.length)) {
                this.dataProvidersPricingModels = [];
                const dataProvidersPricingModels = [];
                if (this.dataProvider.dataProvidersPricingModels) {
                    for(let i=0; i < data.dataProvidersPricingModels.length; i++) {
                        const PricingModels = {
                            id: data.dataProvidersPricingModels[i].id,
                            desc: data.dataProvidersPricingModels[i].description,
                            //locked: ((data.deliveryFrequencies[i].locked ) || (!this.userLevel && !this.allowEditAccess && !this.unlockOperatorPermission &&!this.userRole))
                        };
                        this.dataProvidersPricingModels.push(PricingModels);
                    }
    
                }
                }


            if (( data.dataProvidersPricingModels && data.dataProvidersPricingModels.length) || (data.subscriptionModel && data.subscriptionModel.description) || (data.subscriptionTimePeriod && data.subscriptionTimePeriod.description) || (data.monthlyPriceRange && data.monthlyPriceRange.description) || (data.minimumYearlyPrice) || (data.maximumYearlyPrice) || (data.pricingDetails)) {
                dataLicensingAndTrialingTreeChildren.push({
                    desc: (data.dataProvidersPricingModels|| data.subscriptionModel || data.subscriptionTimePeriod || data.monthlyPriceRange || data.minimumYearlyPrice || data.maximumYearlyPrice || data.pricingDetails)? 'Pricing:' : '',
                    children: (data.dataProvidersPricingModels || data.subscriptionModel || data.subscriptionTimePeriod || data.monthlyPriceRange || data.minimumYearlyPrice || data.maximumYearlyPrice || data.pricingDetails) ? [
                        /* {
                            desc: data.pricingModel && data.pricingModel.description ? 'Pricing Model:' : '',
                            children: data.pricingModel && data.pricingModel.description ? [{ desc: data.pricingModel.description }] : null,
                            tooltip: 'The general model for how the provider prices its data products.',
                            params: {
                                filter: 'pricingModel.id',
                                type: 'checkbox'
                            },
                            routing: '/providers',
                            ga: {
                                labelPrefix: 'Provider Profile - Details - Licensing & Pricing - Pricing Model - ',
                                labelSuffix: 'desc',
                                category: 'Pricing Model',
                                event: 'Internal Link Clicked'
                            }
                        }, */
                        
                        {
                            desc: data.dataProvidersPricingModels && data.dataProvidersPricingModels.length? 'Pricing Model:' : '',
                            //children: this.dataProvidersPricingModels,
                            children:this.dataProvidersPricingModels && this.dataProvidersPricingModels.length ? this.dataProvidersPricingModels : null,
                           /*  children: data.deliveryFrequencies, */
                           tooltip: 'The general model for how the provider prices its data products.',
                           params: {
                                filter: 'dataProvidersPricingModels.id',
                                type: 'checkbox'
                            },
                            routing: '/providers',
                            ga: {
                                labelPrefix: 'Provider Profile - Details - Licensing & Pricing - Pricing Model - ',
                                labelSuffix: 'desc',
                                category: 'Pricing Model',
                                event: 'Internal Link Clicked'
                            }
                        },
                        {
                            desc: data.subscriptionModel ? 'Subscription Model:' : '',
                            children: data.subscriptionModel ? [{ desc: data.subscriptionModel.description }] : null,
                            tooltip: 'The general subscription model for the provider\'s data products.',
                            params: {
                                filter: 'subscriptionModel.id',
                                type: 'checkbox'
                            },
                            routing: '/providers',
                            ga: {
                                labelPrefix: 'Provider Profile - Details - Licensing & Pricing - Subscription Model  - ',
                                labelSuffix: 'desc',
                                category: 'Subscription Model',
                                event: 'Internal Link Clicked'
                            }
                        },
                        {
                            desc: data.subscriptionTimePeriod ? 'Subscription Time Period:' : '',
                            children: data.subscriptionTimePeriod ? [{ desc: data.subscriptionTimePeriod.description }] : null,
                            tooltip: 'The typical time period offered for a subscription to the provider\'s data products.',
                            params: {
                                filter: 'subscriptionTimePeriod.id',
                                type: 'checkbox'
                            },
                            routing: '/providers',
                            ga: {
                                labelPrefix: 'Provider Profile - Details - Licensing & Pricing - Subscription Time Period  - ',
                                labelSuffix: 'desc',
                                category: 'Subscription Time Period',
                                event: 'Internal Link Clicked'
                            }
                        },
                        {
                            desc: data.monthlyPriceRange ? 'Monthly Price Range:' : '',
                            children: data.monthlyPriceRange ? [{ desc: data.monthlyPriceRange.description }] : null,
                            tooltip: 'The price range for a monthly subscription to the provider\'s data offerings.',
                            params: {
                                filter: 'monthlyPriceRange.id',
                                type: 'checkbox'
                            },
                            routing: '/providers',
                            ga: {
                                labelPrefix: 'Provider Profile - Details - Licensing & Pricing - Monthly Price Range  - ',
                                labelSuffix: 'desc',
                                category: 'Monthly Price Range',
                                event: 'Internal Link Clicked'
                            }
                        },
                        {
                            desc: data.minimumYearlyPrice ? 'Minimum Yearly Price:' : '',
                            children: data.minimumYearlyPrice ? [{desc: data.minimumYearlyPrice, locked: data.locked}] : null,
                            tooltip: 'The minimum price for a yearly subscription to the provider\'s data products.'
                        },
                        {
                            desc: data.maximumYearlyPrice ? 'Maximum Yearly Price:' : '',
                            children: data.maximumYearlyPrice ? [{desc: data.maximumYearlyPrice, locked: data.locked}] : null,
                            tooltip: 'The maximum price for a yearly subscription to the provider\'s data products.'
                        },
                        {
                            desc: data.pricingDetails ? 'Pricing Details:' : '',
                            children: data.pricingDetails ? [{desc: data.pricingDetails, locked: data.locked}] : null,
                            tooltip: 'Additional details related to the pricing of the provider\'s data products.'
                        },
                    ] : null
                });
            }
        
            if ((data.freeTrialAvailable) || (data.trialPricingModel && data.trialPricingModel !== '')
            || (data.trialDuration && data.trialDuration !== '') || (data.trialAgreementType && data.trialAgreementType !== '') || (data.trialPricingDetails && data.trialPricingDetails !== '')) {
                const dataLicensingChildren = [];
                if (data.freeTrialAvailable) {
                    dataLicensingChildren.push({
                        desc: data.freeTrialAvailable !== false ? 'Free Trial Available:' : '',
                        children: data.freeTrialAvailable !== false ? [
                            {
                                desc: (data.freeTrialAvailable && data.freeTrialAvailable === true) ? 'Yes' : 'No',
                                /*  (!this.dataProvider.locked ? 'Yes' : 'xhse') : (this.dataProvider.locked ? 'xhse' : 'No'), */
                                id:(data.freeTrialAvailable && data.freeTrialAvailable === true) ? 'Yes' : 'No',
                               // locked: data.locked
                            }
                        ] : null,
                        tooltip: 'Is a free trial available for the providers data products?',
                        params: {
                            filter: 'freeTrialAvailable',
                            type: 'slide-toggle',
                        },
                        routing: '/providers'
                    });
                }
                if (data.trialPricingModel && data.trialPricingModel !== '') {
                    dataLicensingChildren.push({
                        desc: data.trialPricingModel !== '' ? 'Trial Pricing Model:' : '',
                        children: data.trialPricingModel !== '' ? [{ desc: data.trialPricingModel.description}] : null,
                        tooltip: '',
                        params: {
                            filter: 'trialPricingModel.id',
                            type: 'checkbox'
                        },
                        routing: '/providers',
                        ga: {
                            labelPrefix: 'Provider Profile - Details - Licensing & Pricing - Trialling - Trial Pricing Model - ',
                            labelSuffix: 'desc',
                            category: 'Trial Pricing Model',
                            event: 'Internal Link Clicked'
                        }
                    });
                }
                if (data.trialDuration && data.trialDuration !== '') {
                    dataLicensingChildren.push({
                        desc: data.trialDuration !== '' ? 'Trial Duration:' : '',
                        children: data.trialDuration !== '' ? [{ desc: data.trialDuration, locked: data.locked }] : null,
                        tooltip: '',
                        params: {
                            filter: 'trialDuration.id',
                            type: 'checkbox'
                        },
                        routing: '/providers',
                        ga: {
                            labelPrefix: 'Provider Profile - Details - Licensing & Pricing - Trialling -  Trial Duration - ',
                            labelSuffix: 'desc',
                            category: 'Trial Duration',
                            event: 'Internal Link Clicked'
                        }
                    });
                }
                if (data.trialAgreementType && data.trialAgreementType !== '') {
                    dataLicensingChildren.push({
                        desc: data.trialAgreementType !== '' ? 'Trial Agreement Type:' : '',
                        children: data.trialAgreementType !== '' ? [{ desc: data.trialAgreementType.description, locked: data.locked }] : null,
                        tooltip: '',
                        params: {
                            filter: 'trialAgreementType.id',
                            type: 'checkbox'
                        },
                        routing: '/providers',
                        ga: {
                            labelPrefix: 'Provider Profile - Details - Licensing & Pricing - Trialling - Trial Agreement Type - ',
                            labelSuffix: 'desc',
                            category: 'Trial Agreement Type',
                            event: 'Internal Link Clicked'
                        }
                    });
                }
                if (data.trialPricingDetails && data.trialPricingDetails !== '') {
                    dataLicensingChildren.push({
                        desc: (data.trialPricingDetails !== '') ? 'Trial Pricing Details:' : '',
                        children: data.trialPricingDetails !== '' ? [{ desc: data.trialPricingDetails, locked: data.locked }] : null,
                        tooltip: 'Additional details related to the pricing of a trial for the provider\'s data products.',
                    });
                }
                dataLicensingAndTrialingTreeChildren.push({
                    desc: ((data.freeTrialAvailable && data.freeTrialAvailable !== '') || (data.trialPricingModel && data.trialPricingModel !== '') || (data.trialDuration && data.trialDuration !== '') || (data.trialAgreementType && data.trialAgreementType !== '') || (data.trialPricingDetails && data.trialPricingDetails !== '')) ? 'Trialing:' : '',
                    children: (dataLicensingChildren && dataLicensingChildren.length) ? dataLicensingChildren : null
                });
            }
            this.dataLicensingAndTrialingTree = dataLicensingAndTrialingTreeChildren;
            console.log('datalicense',this.dataLicensingAndTrialingTree);
        }     
        /* if (this.dataProvider && this.dataProvider.dataLicenseTypeDetails) {
            this.dataDataLicenseTypeDetailsTree = [
                {
                    desc: 'License Details:',
                    children: [{desc: data.dataLicenseTypeDetails, locked: data.locked}],
                    tooltip: 'The type of data license offered by the provider for its data products.'
                }
            ];
        }
         if (this.dataProvider && this.dataProvider.dataLicenseType) {
            this.dataDataLicenseTypeTree = [
                {
                    desc: 'License Type:',
                    children: [{desc: data.dataLicenseType, locked: data.locked}],
                    tooltip: 'Relevant additional details regarding the providers data licenses.'
                }
            ];
        } */
        if (this.dataProvider && this.dataProvider.potentialDrawbacks) {
            this.dataPotentialDrawbacksTree = [
                {
                    desc: 'Potential Drawbacks:',
                    children: [{ desc: data.potentialDrawbacks}],
                    tooltip: 'Downsides to the providers offerings that decrease the overall appeal of its products for investment research.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.uniqueValueProps) {
            this.dataUniqueValuePropsTree = [
                {
                    desc: 'Unique Value Props:',
                    children: [{ desc: data.uniqueValueProps}],
                    tooltip: 'Unique aspects of the providers offerings that increase the overall appeal of its products for investment research.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.legalAndComplianceIssues) {
            this.dataLegalAndComplianceIssuesTree = [
                {
                    desc: 'Legal & Compliance Issues:',
                    children: [{ desc: data.legalAndComplianceIssues}],
                    tooltip: 'General legal or compliance concerns we would have about the provider if we were using it within an investment process.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.notes) {
            this.dataNotesTree = [
                {
                    desc: 'Notes:',
                    children: [{ desc: data.notes}],
                    tooltip: 'Other further useful notes that dont fit into any other field.'
                }
            ];
        }
        if ((this.dataProvider && this.dataProvider.scoreInvestorsWillingness && this.dataProvider.scoreInvestorsWillingness.description) ||
        (this.dataProvider && this.dataProvider.scoreUniqueness && this.dataProvider.scoreUniqueness.description) ||
        (this.dataProvider && this.dataProvider.scoreValue && this.dataProvider.scoreValue.description) ||
        (this.dataProvider && this.dataProvider.scoreReadiness && this.dataProvider.scoreReadiness.description) ||
        (this.dataProvider && this.dataProvider.scoreOverall && this.dataProvider.scoreOverall.description)) {
            this.datascoreInvestorsWillingnessTree = [
                {
                    desc: (this.dataProvider.scoreInvestorsWillingness && this.dataProvider.scoreInvestorsWillingness.description) ||
                    (this.dataProvider.scoreUniqueness && this.dataProvider.scoreUniqueness.description) ||
                    (this.dataProvider.scoreValue && this.dataProvider.scoreValue.description) ||
                    (this.dataProvider.scoreReadiness && this.dataProvider.scoreReadiness.description) ||
                    (this.dataProvider.scoreOverall && this.dataProvider.scoreOverall.description) ? 'Scores:' : '',
                    children: (this.dataProvider.scoreInvestorsWillingness && this.dataProvider.scoreInvestorsWillingness.description) ||
                    (this.dataProvider.scoreUniqueness && this.dataProvider.scoreUniqueness.description) ||
                    (this.dataProvider.scoreValue && this.dataProvider.scoreValue.description) ||
                    (this.dataProvider.scoreReadiness && this.dataProvider.scoreReadiness.description) ||
                    (this.dataProvider.scoreOverall && this.dataProvider.scoreOverall.description) ? [
                        {
                            desc: data.scoreInvestorsWillingness && data.scoreInvestorsWillingness.description !== '' ? 'Willingness to Provide to Asset Managers:' : '',
                           // children: data.scoreInvestorsWillingness && data.scoreInvestorsWillingness.description !== '' ? [{ desc: data.scoreInvestorsWillingness.description, id: data.scoreInvestorsWillingness.id, locked: data.locked }] : null,
                            children: data.scoreInvestorsWillingness && data.scoreInvestorsWillingness.description !== '' ? [{ desc: data.scoreInvestorsWillingness.description, id: data.scoreInvestorsWillingness.id}] : null,
                            tooltip: 'How willing the provider is to provide or sell their data products to asset managers or institutional investors.',
                            params: {
                                filter: 'scoreInvestorsWillingness.id',
                                type: 'checkbox'
                            },
                            routing: '/providers'
                       },
                       {
                            desc: (data.scoreUniqueness && data.scoreUniqueness.description !== '') ? 'Uniqueness:' : '',
                            //children: (data.scoreUniqueness && data.scoreUniqueness.description !== '') ? [{ desc: data.scoreUniqueness.description, id: data.scoreUniqueness.id, locked: data.locked }] : null,
                            children: (data.scoreUniqueness && data.scoreUniqueness.description !== '') ? [{ desc: data.scoreUniqueness.description, id: data.scoreUniqueness.id}] : null,
                            tooltip: 'How unique the providers data products are compared to traditional investment research techniques.',
                            params: {
                                filter: 'scoreUniqueness.id',
                                type: 'checkbox'
                            },
                            routing: '/providers'
                       },
                       {
                            desc: data.scoreValue && data.scoreValue.description !== '' ? 'Value:' : '',
                          //  children: data.scoreValue && data.scoreValue.description !== '' ? [{ desc: data.scoreValue.description, id: data.scoreValue.id, locked: data.locked }] : null,
                            children: data.scoreValue && data.scoreValue.description !== '' ? [{ desc: data.scoreValue.description, id: data.scoreValue.id}] : null,
                            tooltip: 'How valuable the providers data products is compared to traditional investment research techniques.',
                            params: {
                                filter: 'scoreValue.id',
                                type: 'checkbox'
                            },
                            routing: '/providers'
                       },
                       {
                            desc: data.scoreReadiness && data.scoreReadiness.description !== '' ? 'Readiness:' : '',
                          //  children: data.scoreReadiness && data.scoreReadiness.description !== '' ? [{ desc: data.scoreReadiness.description, id: data.scoreReadiness.id, locked: data.locked }] : null,
                            children: data.scoreReadiness && data.scoreReadiness.description !== '' ? [{ desc: data.scoreReadiness.description, id: data.scoreReadiness.id}] : null,
                            tooltip: 'How ready the providers data products are to be deployed within investment research.',
                            params: {
                                filter: 'scoreReadiness.id',
                                type: 'checkbox'
                            },
                            routing: '/providers'
                       },
                       {
                            desc: data.scoreOverall && data.scoreOverall.description !== '' ? 'Overall:' : '',
                            //children: data.scoreOverall && data.scoreOverall.description !== '' ? [{ desc: data.scoreOverall.description, id: data.scoreOverall.id, locked: data.locked }] : null,
                            children: data.scoreOverall && data.scoreOverall.description !== '' ? [{ desc: data.scoreOverall.description, id: data.scoreOverall.id}] : null,
                            tooltip: 'The overall importance of integrating the providers data products into an investment research process.',
                            params: {
                                filter: 'scoreOverall.id',
                                type: 'checkbox'
                            },
                            routing: '/providers'
                       }
                    ] : null,
                    tooltip: 'Amass-created subjective scoring of various important aspects of a data provider\'s offerings.'
                }
            ];
        }
        if (this.dataProvider && this.dataProvider.competitiveDifferentiators) {
            if(this.isAuthenticated())
            {
                this.dataCompetitiveDifferentiatorsTree = [
                    {
                        desc: data.competitiveDifferentiators  ?'Competitive Differentiators:': '',
                        children: [{ desc: data.competitiveDifferentiators, locked: (data.locked || (!this.userLevel && !this.allowEditAccess && !this.unlockOperatorPermission && !this.userRole))}],
                        //children: [{ desc: data.competitiveDifferentiators}],
                        tooltip: ''
                    }
                ];
            }
           else
           {
            this.dataCompetitiveDifferentiatorsTree = [
                {
                    desc: data.competitiveDifferentiators  ?'Competitive Differentiators:': '',
                    children: [{ desc: data.competitiveDifferentiators}],
                    tooltip: ''
                }
            ];
           }
        }
        if (this.dataProvider && this.dataProvider.useCasesOrQuestionsAddressed) {
            if(this.isAuthenticated())
            {
                this.dataUseCasesOrQuestionsAddressedTree = [
                    {
                        desc: 'Use Cases:',
                        children: [{ desc: data.useCasesOrQuestionsAddressed, locked: (data.locked || (!this.userLevel && !this.allowEditAccess && !this.unlockOperatorPermission && !this.userRole))}],
                        //children: [{ desc: data.useCasesOrQuestionsAddressed}],
                        tooltip: 'The providers opinion on the ideas for use cases or questions that could be answered by the providers data products.'
                    }
                ];
            }
           else
           {
            this.dataUseCasesOrQuestionsAddressedTree = [
                {
                    desc: 'Use Cases:',
                    children: [{ desc: data.useCasesOrQuestionsAddressed}],
                    tooltip: 'The providers opinion on the ideas for use cases or questions that could be answered by the providers data products.'
                }
            ];
           }
        }
        if (this.dataProvider && this.dataProvider.dataRoadmap) {
            if(this.isAuthenticated())
            {
                this.dataDataRoadmapTree = [
                    {
                        desc: 'Data Roadmap:',
                        children: [{ desc: data.dataRoadmap, locked: (data.locked || (!this.userLevel && !this.allowEditAccess && !this.unlockOperatorPermission && !this.userRole))}],
                       // children: [{ desc: data.dataRoadmap}],
                        tooltip: 'The provider\'s plans for future improvements of their data products.'
                    }
                ];
            }
         else
         {
            this.dataDataRoadmapTree = [
                {
                    desc: 'Data Roadmap:',
                    children: [{ desc: data.dataRoadmap}],
                    tooltip: 'The provider\'s plans for future improvements of their data products.'
                }
            ];
         }
        }
        if (this.dataProvider && this.dataProvider.discussionCleanliness) {
            if(this.isAuthenticated())
            {
                this.dataCleanlinessTree = [
                    {
                        desc: 'Cleanliness:',
                        children: [{ desc: data.discussionCleanliness, locked: ((data.locked || !this.userLevel) && !this.allowEditAccess && !this.unlockOperatorPermission && !this.userRole)}],
                        //children: [{ desc: data.discussionCleanliness}],
                        tooltip: ''
                    }
                ];
            }
          else
          {
            this.dataCleanlinessTree = [
                {
                    desc: 'Cleanliness:',
                    children: [{ desc: data.discussionCleanliness}],
                    tooltip: ''
                }
            ];
          }
        }
        if (this.dataProvider && this.dataProvider.discussionAnalytics) {
            if(this.isAuthenticated())
            {
                this.dataAnalyticsTree = [
                    {
                        desc: 'Analytics:',
                        children: [{ desc: data.discussionAnalytics, locked: ((data.locked || !this.userLevel) && !this.allowEditAccess && !this.unlockOperatorPermission && !this.userRole)}],
                        //children: [{ desc: data.discussionAnalytics}],
                        tooltip: ''
                    }
                ];
            }
            else
            {
                this.dataAnalyticsTree = [
                    {
                        desc: 'Analytics:',
                        children: [{ desc: data.discussionAnalytics}],
                        tooltip: ''
                    }
                ];
            }
        }
        this.loadTabs();
        this.loaderService.display(false);
        this.isLoading = false;
    }

    getTooltip(obj, id) {
        let tooltip = null;
        if (obj && id) {
            tooltip = obj.filter(i => {
                return i.id === parseInt(id);
            });
        }
        return tooltip && tooltip.length ? tooltip[0].explanation : null;
    }

    loadTabs() {
        const relevantArticlesArray: GenericSubTabListItem[] = [];
        const relevantArticlesButtons: GenericProfileButtonsModel[] =[];
        const partnersButtons: GenericProfileButtonsModel[] = [];
        if (this.relevantArticles) {
            for (let i = 0; i < this.relevantArticles.length; i++) {
                const element = this.relevantArticles[i]._source;
                let subTitle = '',
                    mainContent = '',
                    topLeft = '',
                    topRight = '';
                if (element.authorDataProvider && element.authorDataProvider.providerName && element.authorDataProvider.providerName !== 'RECORD_NULL') {
                    subTitle = element.authorDataProvider.providerName;
                } else if (element.authorOrganization && element.authorOrganization.name && element.authorOrganization.name !== 'NAME_NULL') {
                    subTitle = element.authorOrganization.name
                } else if (element.stream) {
                    subTitle = element.stream;
                }

                if (element.visualUrl && element.summary) {
                    mainContent = element.summary;
                } else {
                    mainContent = element.summaryHTML;
                }
                if (element.purpose && element.purpose.length) {
                    topLeft = '<strong>Purpose: </strong>' + element.purpose.map(item => item.purpose).join(', ');
                }
                if (element.articleTopics && element.articleTopics.length) {
                    topRight = '<strong>Topic: </strong>' + element.articleTopics.map(item => item.topicName).join(', ');
                }
                relevantArticlesArray.push({
                    id: element.id,
                    title: element.title,
                    'subTitle': subTitle,
                    image: element.visualUrl,
                    url: element.url,
                    mainContent: (element.summary && !element.summaryHTML) ? element.summary : '',
                    mainContentHTML: element.summaryHTML ? element.summaryHTML : '',
                    topLeftLabel: 'Purpose:',
                    topLeftLabelTooltip: 'Categorization of the focus of the article.',
                    topLeftContent: element.purpose && element.purpose.length ? element.purpose.map(item => ({
                        title: item.purpose,
                        explanation: item.explanation
                    })) : null,
                    topRightLabel: 'Topic:',
                    topRightContent: element.articleTopics && element.articleTopics.length ? element.articleTopics.map(item => ({
                        title: item.topicName,
                        explanation: item.summary ? item.summary : null
                    })) : null,
                    bottomRightLabel: 'Published:',
                    bottomRightLabelTooltip: 'Date the article was published',
                    bottomRightContent: [{
                        title: this.datePipe.transform(element.publishedDate, 'MMM dd, yyyy'),
                        explanation: 'Date the article was published'
                    }],
                    'topLeft': topLeft,
                    'topRight': topRight,
                    bottomRight: '<strong>Published: </strong>' + this.datePipe.transform(element.publishedDate, 'MMM dd, yyyy'),
                    articlesTabs: 'Articles'
                });
            }
        }
        if (this.dataProvider && this.dataProvider.locked) {
            if (!this.dataProvider.userProviderStatus) {
                relevantArticlesButtons.push({
                    label: 'Request Access to this Provider\'s Data Profile',
                    actionArguments: ['Request for Access to Provider', 'Press submit or add more context about your request for access to this Provider\'s Data Profile.', 'unlockProvider', this.dataProvider.id],
                    action: 'openContactDialog',
                });
            }
            if (this.dataProvider.userProviderStatus && this.dataProvider.userProviderStatus.indexOf(2087) !== -1) {
                relevantArticlesButtons.push({
                    label: 'Access to this Data Profile in Review.'
                });
                partnersButtons.push({
                    label: 'Access to this Data Profile in Review.'
                });
            }
        }

        const authoredArticlesArray: GenericSubTabListItem[] = [];
        if (this.authoredArticles) {
            for (let i = 0; i < this.authoredArticles.length; i++) {
                const element = this.authoredArticles[i]._source;
                let subTitle = '',
                    topLeft = '',
                    topRight = '';
                if (element.authorDataProvider && element.authorDataProvider.providerName && element.authorDataProvider.providerName !== 'RECORD_NULL') {
                    subTitle = element.authorDataProvider.providerName;
                } else if (element.authorOrganization && element.authorOrganization.name && element.authorOrganization.name !== 'NAME_NULL') {
                    subTitle = element.authorOrganization.name
                } else if (element.stream) {
                    subTitle = element.stream;
                }
                if (element.purpose && element.purpose.length) {
                    topLeft = '<strong>Purpose: </strong>' + element.purpose.map(item => item.purpose).join(', ');
                }
                if (element.articleTopics && element.articleTopics.length) {
                    topRight = '<strong>Topic: </strong>' + element.articleTopics.map(item => item.topicName).join(', ');
                }
                authoredArticlesArray.push({
                    id: element.id,
                    title: element.title,
                    'subTitle': subTitle,
                    image: element.visualUrl,
                    url: element.url,
                    mainContent: (element.summary && !element.summaryHTML) ? element.summary : '',
                    mainContentHTML: element.summaryHTML ? element.summaryHTML : '',
                    topLeftLabel: 'Purpose:',
                    topLeftLabelTooltip: 'Categorization of the focus of the article.',
                    topLeftContent: element.purpose && element.purpose.length ? element.purpose.map(item => ({
                        title: item.purpose,
                        explanation: item.explanation
                    })) : null,
                    topRightLabel: 'Topic:',
                    topRightContent: element.articleTopics && element.articleTopics.length ? element.articleTopics.map(item => ({
                        title: item.topicName,
                        explanation: item.summary ? item.summary : null
                    })) : null,
                    bottomRightLabel: 'Published:',
                    bottomRightLabelTooltip: 'Date the article was published',
                    bottomRightContent: [{
                        title: this.datePipe.transform(element.publishedDate, 'MMM dd, yyyy'),
                        explanation: 'Date the article was published'
                    }],
                    'topLeft': topLeft,
                    'topRight': topRight,
                    bottomRight: '<strong>Published: </strong>' + this.datePipe.transform(element.publishedDate, 'MMM dd, yyyy'),
                    articlesTabs: 'Articles'
                });
            }
        }

        // Overview Tab Buttons
        const overviewTabButtons: GenericProfileButtonsModel[] = [];

        // Details Tab Buttons
        const detailsTabButtons: GenericProfileButtonsModel[] = [];
        const providerPartnersArray: GenericSubTabListItem[] = [];
        const distributionPartnersArray: GenericSubTabListItem[] = [];
        const consumerPartnersArray: GenericSubTabListItem[] = [];
        const competitorsArray: GenericSubTabListItem[] = [];
        // Resources Buttons
        const resourcesButtons: GenericProfileButtonsModel[] = [];
        if (this.dataProvider) {
            // Overview
            if (!this.dataProvider.locked && !this.isAdmin && this.isAuthenticated() && (!this.dataProvider.userProviderStatus || (this.dataProvider.userProviderStatus && this.dataProvider.userProviderStatus.indexOf(561) === -1))) {
                if (!this.dataProvider.locked && (!this.dataProvider.userProviderStatus || this.dataProvider.userProviderStatus && this.dataProvider.userProviderStatus.indexOf(261) === -1)) {
                    overviewTabButtons.push({
                        label: 'Request More Information about this Provider',
                        actionArguments: ['Request for More Information', 'Press submit or add more context about your information request.', 'moreInformation', this.dataProvider.id, null, null, 261],
                        action: 'openContactDialog'
                    });
                    detailsTabButtons.push({
                        label: 'Request More Information about this Provider',
                        actionArguments: ['Request for More Information', 'Press submit or add more context about your information request.', 'moreInformation', this.dataProvider.id, null, null, 261],
                        action: 'openContactDialog'
                    });
                }
                if (this.dataProvider.userProviderStatus && this.isAuthenticated() && this.dataProvider.userProviderStatus.indexOf(261) !== -1) {
                    overviewTabButtons.push({
                        label: 'More Information Request In Progress. Click to Cancel',
                        actionArguments: [this.dataProvider.id, null, 261],
                        action: 'cancelMoreInformationAction'
                    });
                    detailsTabButtons.push({
                        label: 'More Information Request In Progress. Click to Cancel',
                        actionArguments: [this.dataProvider.id, null, 261],
                        action: 'cancelMoreInformationAction'
                    });
                }
            }
            if (this.dataProvider.locked && !this.dataProvider.userProviderStatus || !this.isAuthenticated()) {
                overviewTabButtons.push({
                    label: 'Request Access to this Provider\'s Data Profile',
                    actionArguments: ['Request for Access to Provider', 'Press submit or add more context about your request for access to this Provider\'s Data Profile.', 'unlockProvider', this.dataProvider.id],
                    action: 'openContactDialog'
                });
                detailsTabButtons.push({
                    label: 'Request Access to this Provider\'s Data Profile',
                    actionArguments: ['Request for Access to Provider', 'Press submit or add more context about your request for access to this Provider\'s Data Profile.', 'unlockProvider', this.dataProvider.id],
                    action: 'openContactDialog'
                });
                resourcesButtons.push({
                    label: 'Request Access to this Provider\'s Data Profile',
                    actionArguments: ['Request for Access to Provider', 'Press submit or add more context about your request for access to this Provider\'s Data Profile.', 'unlockProvider', this.dataProvider.id],
                    action: 'openContactDialog'
                });
                partnersButtons.push({
                    label: 'Request Access to this Provider\'s Data Profile',
                    actionArguments: ['Request for Access to Provider', 'Press submit or add more context about your request for access to this Provider\'s Data Profile.', 'unlockProvider', this.dataProvider.id],
                    action: 'openContactDialog'
                });
            }
            if (this.dataProvider.userProviderStatus && this.dataProvider.userProviderStatus.indexOf(2087) !== -1) {
                overviewTabButtons.push({
                    label: 'Access to this Data Profile in Review.'
                });
                detailsTabButtons.push({
                    label: 'Access to this Data Profile in Review.'
                });
            }
            if (this.isPendingUnlock) {
                overviewTabButtons.push({
                    label: 'Confirm Unlock',
                    actionArguments: [this.dataProvider.id],
                    action: 'confirmUnlockAction'
                });
                overviewTabButtons.push({
                    label: 'Cancel Unlock',
                    actionArguments: [this.dataProvider.id],
                    action: 'cancelUnlockAction'
                });
            }
            if (this.isPendingExpand) {
                overviewTabButtons.push({
                    label: 'Confirm Expand',
                    actionArguments: [this.dataProvider.id],
                    action: 'confirmExpandAction'
                });
                overviewTabButtons.push({
                    label: 'Cancel Expand',
                    actionArguments: [this.dataProvider.id],
                    action: 'cancelExpandAction'
                });
            }

            if ((this.dataProvider && !this.dataProvider.locked && (this.dataProvider.userProviderStatus && this.dataProvider.userProviderStatus.indexOf(261) === -1 && this.dataProvider.userProviderStatus.indexOf(561) === -1)) || (this.dataProvider && !this.dataProvider.locked && !this.dataProvider.userProviderStatus)) {
                if (!this.dataSource && !this.isAdmin) {
                    resourcesButtons.push({
                        label: 'Request Samples & Resources for this Provider',
                        actionArguments: ['Request for More Information', 'Press submit or add more context about your information request.', 'moreInformation', this.dataProvider.id, null, null, 261],
                        action: 'openContactDialog'
                    });
                } else if ((this.dataSource && (this.dataProvider && this.dataProvider.userProviderStatus && this.dataProvider.userProviderStatus.indexOf(261) === -1) || (this.dataProvider && !this.dataProvider.locked && !this.dataProvider.userProviderStatus && this.isAuthenticated())) && !this.isAdmin) {
                    resourcesButtons.push({
                        label: 'Think something is missing? Request Additional Samples & Resources for this Provider',
                        actionArguments: ['Request for More Information', 'Press submit or add more context about your information request.', 'moreInformation', this.dataProvider.id, null, null, 261],
                        action: 'openContactDialog'
                    });
                }
            }

            if (this.dataProvider && !this.dataProvider.locked && (this.dataProvider.userProviderStatus && this.dataProvider.userProviderStatus.indexOf(261) !== -1 && this.dataProvider.userProviderStatus.indexOf(561) === -1)) {
                resourcesButtons.push({
                    label: 'More Information Request In Progress. Click to Cancel',
                    actionArguments: [this.dataProvider.id, null, 261],
                    action: 'cancelMoreInformationAction',
                });
            }
            if (this.dataProvider && this.dataProvider.locked && (this.dataProvider.userProviderStatus && this.dataProvider.userProviderStatus.indexOf(2087) !== -1 && this.dataProvider.userProviderStatus.indexOf(561) === -1)) {
                resourcesButtons.push({
                    label: 'Access to this Data Profile in Review.'
                });
            }

            // Details
            if(!this.restrictedAuthority)
            {
            if(this.dataProvider && this.dataProvider.providerPartners && this.dataProvider.providerPartners.length){
                    for (let i = 0; i < this.dataProvider.providerPartners.length; i++) {
                    const element = this.dataProvider.providerPartners[i];
                    let subTitle = '',
                        topLeft = '',
                        topRight = '';
                    if (element.ownerOrganization && element.ownerOrganization.name !==element.providerName && element.ownerOrganization.name !== 'NAME_NULL') {
                        subTitle = element.ownerOrganization.name;
                    }
                    if (element.mainDataIndustry && element.mainDataIndustry.dataIndustry !== 'DATA_INDUSTRY_NULL') {
                        topLeft = element.mainDataIndustry.dataIndustry;  /* '<strong>Main Data Industry: </strong><a [routerLink]="[\'/providers\']" [queryParams]="{ filter1: \'mainDataIndustry.id\', value1: ' + element.mainDataIndustry.id + ', type1: \'checkbox\' }" [ngClass]="{\'blurText\': ' + element.mainDataIndustry.locked + '}">' + element.mainDataIndustry.dataIndustry + '</a>'; */
                    }
                    if (element.mainDataCategory && element.mainDataCategory.datacategory !== 'DATA_CATEGORY_NULL') {
                        topRight = element.mainDataCategory.datacategory; /* '<strong>Main Data Category: </strong><a [routerLink]="[\'/providers\']" [queryParams]="{ filter1: \'mainDataCategory.id\', value1: ' + element.mainDataCategory.id + ', type1: \'checkbox\' }" [ngClass]="{\'blurText\': ' + element.mainDataCategory.locked + '}">' + element.mainDataCategory.datacategory + '</a>'; */
                    }
                    providerPartnersArray.push({
                        id: element.id,
                            title: element.providerName,
                        'subTitle': subTitle,
                        base64Image: element.logo ? element.logo.logo : null,
                        url: this.isAuthenticated() ? '/#/providers/' + element.recordID:'/#/unproviders/' + element.recordID,
                        mainContent: element.shortDescription !== 'DESCRIPTION_NULL' ? element.shortDescription : '',
                        'topLeft': topLeft,
                        'topRight': topRight,
                        locked: element.locked,
                        completenessBar: element.completeness,
                        overAllScore: element.scoreOverall && element.scoreOverall.description ? element.scoreOverall : '',
                        addedDate: element.createdDate != null ? this.datePipe.transform(element.createdDate, 'mediumDate', 'EST') : '',
                        updatedDate: element.updatedDate != null ? this.datePipe.transform(element.updatedDate, 'mediumDate', 'EST') : '',
                        providerStatus: element.userProviderStatus,
                        providersTabs: 'Providers'
                    });
                }
            }
        }
        if(!this.restrictedAuthority)
        {
            if(this.dataProvider && this.dataProvider.distributionPartners && this.dataProvider.distributionPartners.length){
                for (let i = 0; i < this.dataProvider.distributionPartners.length; i++) {
                    const element = this.dataProvider.distributionPartners[i];
                    let subTitle = '',
                        topLeft = '',
                        topRight = '';
                    if (element.ownerOrganization && element.ownerOrganization.name !==element.providerName && element.ownerOrganization.name !== 'NAME_NULL') {
                        subTitle = element.ownerOrganization.name;
                    }
                    if (element.mainDataIndustry && element.mainDataIndustry.dataIndustry !== 'DATA_INDUSTRY_NULL') {
                        topLeft = element.mainDataIndustry.dataIndustry ;
                    }
                    if (element.mainDataCategory && element.mainDataCategory.datacategory !== 'DATA_CATEGORY_NULL') {
                        topRight = element.mainDataCategory.datacategory ;
                    }
                    distributionPartnersArray.push({
                        id: element.id,
                        title: element.providerName,
                        'subTitle': subTitle,
                        base64Image: element.logo ? element.logo.logo: null,
                        url: this.isAuthenticated() ? '/#/providers/' + element.recordID:'/#/unproviders/' + element.recordID,
                        mainContent: element.shortDescription !== 'DESCRIPTION_NULL' ? element.shortDescription : '',
                        'topLeft': topLeft,
                        'topRight': topRight,
                        locked: element.locked,
                        completenessBar: element.completeness,
                        overAllScore: element.scoreOverall && element.scoreOverall.description ? element.scoreOverall : '',
                        addedDate: element.createdDate != null ? this.datePipe.transform(element.createdDate, 'mediumDate', 'EST') : '',
                        updatedDate: element.updatedDate != null ? this.datePipe.transform(element.updatedDate, 'mediumDate', 'EST') : '',
                        providerStatus: element.userProviderStatus,
                        providersTabs: 'Providers'
                    });
                }
            }
        }
        if(!this.restrictedAuthority)
        {
            if(this.dataProvider && this.dataProvider.consumerPartners && this.dataProvider.consumerPartners.length) {
                for (let i = 0; i < this.dataProvider.consumerPartners.length; i++) {
                    const element = this.dataProvider.consumerPartners[i];
                    let subTitle = '',
                        topLeft = '',
                        topRight = '';
                    if (element.ownerOrganization && element.ownerOrganization.name !==element.providerName && element.ownerOrganization.name !== 'NAME_NULL') {
                        subTitle = element.ownerOrganization.name;
                    }
                    if (element.mainDataIndustry && element.mainDataIndustry.dataIndustry !== 'DATA_INDUSTRY_NULL') {
                        topLeft = element.mainDataIndustry.dataIndustry;
                    }
                    if (element.mainDataCategory && element.mainDataCategory.datacategory !== 'DATA_CATEGORY_NULL') {
                        topRight = element.mainDataCategory.datacategory;
                    }
                    consumerPartnersArray.push({
                        id: element.id,
                        title: element.providerName,
                        'subTitle': subTitle,
                        base64Image: element.logo ? element.logo.logo : null,
                        url: this.isAuthenticated() ? '/#/providers/' + element.recordID:'/#/unproviders/' + element.recordID,
                        mainContent: element.shortDescription !=='DESCRIPTION_NULL' ? element.shortDescription : '',
                        'topLeft': topLeft,
                        'topRight': topRight,
                        locked: element.locked,
                        completenessBar: element.completeness,
                        overAllScore: element.scoreOverall && element.scoreOverall.description ? element.scoreOverall : '',
                        addedDate: element.createdDate != null ? this.datePipe.transform(element.createdDate, 'mediumDate', 'EST') : '',
                        updatedDate: element.updatedDate != null ? this.datePipe.transform(element.updatedDate, 'mediumDate', 'EST') : '',
                        providerStatus: element.userProviderStatus,
                        providersTabs: 'Providers'
                    });
                }
            }
        }
        if(!this.restrictedAuthority)
        {
            if(this.dataProvider.competitors && this.dataProvider.competitors.length){
                for (let i = 0; i < this.dataProvider.competitors.length; i++) {
                    const element = this.dataProvider.competitors[i];
                    let subTitle = '',
                        topLeft = '',
                        topRight = '';
                    if (element.ownerOrganization && element.ownerOrganization.name !==element.providerName && element.ownerOrganization.name !== 'NAME_NULL') {
                        subTitle = element.ownerOrganization.name;
                    }
                    if (element.mainDataIndustry && element.mainDataIndustry.dataIndustry !== 'DATA_INDUSTRY_NULL') {
                        topLeft = element.mainDataIndustry.dataIndustry;
                    }
                    if (element.mainDataCategory && element.mainDataCategory.datacategory !== 'DATA_CATEGORY_NULL') {
                        topRight = element.mainDataCategory.datacategory;
                    }
                    if(this.userLevel) {
                        competitorsArray.push({
                            id: element.id,
                            title: element.providerName,
                            'subTitle': subTitle,
                            base64Image: element.logo ? element.logo.logo : null,
                            url: this.isAuthenticated() ? '/#/providers/' + element.recordID:'/#/unproviders/' + element.recordID,
                            mainContent: element.shortDescription !=='DESCRIPTION_NULL' ? element.shortDescription : '',
                            'topLeft': topLeft,
                            'topRight': topRight,
                            locked: element.locked,
                            completenessBar: element.completeness,
                            overAllScore: element.scoreOverall && element.scoreOverall.description ? element.scoreOverall : '',
                            addedDate: element.createdDate != null ? this.datePipe.transform(element.createdDate, 'mediumDate', 'EST') : '',
                            updatedDate: element.updatedDate != null ? this.datePipe.transform(element.updatedDate, 'mediumDate', 'EST') : '',
                            providerStatus: element.userProviderStatus,
                            providersTabs: 'Providers'
                        });
                    }
                }
            }
        }
        }

        this.tabs = [{
            tabLabelName: 'Overview',
            tabLabelTooltip: 'High-level overview of the data provider\'s characteristics.',
            tabContent: [{
                isTreeNodesWithPanel: false,
                treeNodes: [this.dataShortDescriptionTree, this.dataLongDescriptionTree,this.dataProfileVerificationStatusTree],
            }, {
                isTreeNodesWithPanel: true,
                treeNodes: [
                    this.dataProviderType,
                    this.dataIndustryTree,
                    this.dataCategoryTree,
                    this.dataFeaturesTree,
                    this.dataSourceTree,
                    this.datanumberOfDataSourcesTree,
                    this.dataDataSourcesDetailsTree,
                    this.dataGeographicalFociTree,
                    this.dataMainAssetClassTree,
                    this.dataSecurityTypesTree,
                    this.dataRelevantSectorsTree,
                    this.dataProviderTagsTree
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (this.dataProviderType || this.dataIndustryTree || this.dataCategoryTree || this.dataFeaturesTree || this.dataSourceTree || this.datanumberOfDataSourcesTree || this.dataDataSourcesDetailsTree || this.dataProviderTagsTree || this.dataGeographicalFociTree || this.dataMainAssetClassTree || this.dataSecurityTypesTree || this.dataRelevantSectorsTree) ? 'Data Provider Classifications' : '',
                    tooltip: 'High-level classifications of the provider\'s data or analytics.'
                }
            }, {
                isTreeNodesWithPanel: true,
                treeNodes: [
                    this.dataOwnerOrganizationTree,
                    this.dataOwnerOrganizationNameTree,
                    this.dataOwnerOrganizationLocationTree,
                    this.dataOwnerOrganizationYearFoundedTree,
                    this.dataOwnerOrganizationyearExitTree,
                    this.dataOwnerOrganizationheadcountBucketTree,
                    this.dataOwnerOrganizationheadcountNumberTree,
                    this.dataOwnerOrganizationHeadCountTree,
                    this.datanumberOfAnalystEmployeesTree,
                    this.dataOwnerOrganizationFinancialPositionTree
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (this.dataOwnerOrganizationTree || this.dataOwnerOrganizationNameTree || this.dataOwnerOrganizationLocationTree || this.dataOwnerOrganizationYearFoundedTree || this.dataOwnerOrganizationheadcountBucketTree || this.dataOwnerOrganizationyearExitTree || this.dataOwnerOrganizationheadcountNumberTree || this.datanumberOfAnalystEmployeesTree || this.dataOwnerOrganizationHeadCountTree || this.dataOwnerOrganizationFinancialPositionTree) ? 'Company Details' : '',
                    tooltip: 'Information about organizations or organizations that owns/runs the data provider.'
                },
                buttons: overviewTabButtons
            }],
            subTabs: [{
                tabLabelName: 'Relevant Articles',
                tabLabelRight: '( ' + relevantArticlesArray.length + ' )',
                tabLabelRightTooltip: 'Number of articles matching active search and filter criteria.',
                tabLabelTooltip: 'Articles about this provider or specifically relevant to this provider.',
                tabContent: [{
                    contentType: TabContentType.LIST,
                    list: {
                        data: relevantArticlesArray,
                        loadPage: this.loadRelevantArticlesPage,
                        page: this.relevantArticlePage,
                        itemFrom: this.relevantArticleFrom,
                        pageLinks: this.relevantArticleLinks,
                        totalItems: this.totalRelevantArticles
                    },
                    noDataFoundMsg: (this.dataProvider && this.dataProvider.locked || !this.isAuthenticated()) ? 'Your account is not permitted to access articles for this provider.' : 'No articles found.' && (this.dataProvider && this.dataProvider.locked || !this.isAuthenticated()) ? ' You do not have permission to view the articles .' : 'No articles found.',
                    buttons: (this.dataProvider && this.dataProvider.locked) ? relevantArticlesButtons : [],
                    shareButtons: [{
                        type: 'email',
                        buttonTooltip: 'Share This Article Through Email'
                    }, {
                        type: 'linkedin',
                        buttonTooltip: 'Share This Article Through LinkedIn'
                    }, {
                        type: 'twitter',
                        buttonTooltip: 'Share This Article Through Twitter'
                    }, {
                        type: 'copy',
                        buttonTooltip: 'Copy the Link to this Article'
                    }]
                }]
            }, {
                tabLabelName: 'Authored Articles',
                tabLabelRight: '( ' + authoredArticlesArray.length + ' )',
                tabLabelRightTooltip: 'Number of articles matching active search and filter criteria.',
                tabLabelTooltip: 'Articles written by this provider.',
                tabContent: [{
                    contentType: TabContentType.LIST,
                    list: {
                        data: authoredArticlesArray,
                        loadPage: this.loadAllAuthoredArticles,
                        page: this.authoredArticlePage,
                        itemFrom: this.authoredArticleFrom,
                        pageLinks: this.authoredArticleLinks,
                        totalItems: this.totalAuthoredArticles
                    },
                    noDataFoundMsg: (this.dataProvider && this.dataProvider.locked || !this.isAuthenticated()) ? 'Your account is not permitted to access articles for this provider.' : 'No articles found.' && (this.dataProvider && this.dataProvider.locked || !this.isAuthenticated()) ? ' You do not have permission to view the articles .' : 'No articles found.',
                    buttons: (this.dataProvider && this.dataProvider.locked) ? partnersButtons : [],
                    shareButtons: [{
                        type: 'email',
                        buttonTooltip: 'Share This Article Through Email'
                    }, {
                        type: 'linkedin',
                        buttonTooltip: 'Share This Article Through LinkedIn'
                    }, {
                        type: 'twitter',
                        buttonTooltip: 'Share This Article Through Twitter'
                    }, {
                        type: 'copy',
                        buttonTooltip: 'Copy the Link to this Article'
                    }]
                }]
            }]
        }, {
            tabLabelName: 'Details',
            tabLabelTooltip: 'Important details of the data provider\'s offerings.',
            tabContent: [{
                isTreeNodesWithPanel: true,
                treeNodes: [
                    this.dataSummaryTree,
                    this.datayearFirstDataProductLaunchedTree,
                    this.datasecRegulatedTree,
                    this.datainvestorClientBucketTree,
                    this.dataInvestorClientsCountTree,
                    this.dataInvestorClientsTree,
                    this.dataOwnerOrganizationClientBaseTree,
                    this.dataTargetOrganizationTypeTree,
                    this.dataManagerTypeTree,
                    this.dataInvestorTypesTree,
                    this.dataStrategiesTree,
                    this.dataResearchStylesTree,
                    this.dataInvestingTimeFrameTree,
                    this.dataEquitiesMarketCapTree,
                    this.dataEquitiesStyleTree
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (this.dataSummaryTree || this.datayearFirstDataProductLaunchedTree ||  this.datasecRegulatedTree ||this.dataInvestorClientsTree ||  this.datainvestorClientBucketTree ||  this.dataInvestorClientsCountTree || this.dataTargetOrganizationTypeTree || this.dataManagerTypeTree || this.dataInvestorTypesTree || this.dataStrategiesTree ||  this.dataResearchStylesTree || this.dataInvestingTimeFrameTree || this.dataEquitiesMarketCapTree || this.dataEquitiesStyleTree || this.dataOwnerOrganizationClientBaseTree) ? 'Provider Details' : ''
                }
            }, {
                isTreeNodesWithPanel: true,
                treeNodes: [
                    this.dataAccessOfferedTree,
                    this.dataDataProductTypesTree,
                    this.dataDataTypesTree,
                    this.dataSampleOrPanelSizeTree,
                    this.dataPublicEquitiesCoveredRangeTree,
                    this.dataPublicEquitiesCoveredCountTree,
                    this.dataPublicCompaniesCoveredTree,
                    this.dataHistoricalDateRangeEstimateTree,
                    this.dataDateCollectionRangeExplanationTree,
                    this.dataCollectionMethodsExplanationTree,
                    this.datadatasetSizeTree,
                    this.datadatasetNumberOfRowsTree,
                    this.dataDataLanguagesAvailableTree,
                    this.dataKeyDataFieldsTree,
                    this.dataProductDetailsTree
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (this.dataAccessOfferedTree || this.dataDataProductTypesTree || this.dataDataTypesTree ||this.dataSampleOrPanelSizeTree || this.dataPublicEquitiesCoveredRangeTree || this.dataPublicEquitiesCoveredCountTree  || this.dataPublicCompaniesCoveredTree || this.dataHistoricalDateRangeEstimateTree || this.dataCollectionMethodsExplanationTree || this.dataDataLanguagesAvailableTree || this.dataKeyDataFieldsTree || this.dataDeliveryMethodsTree || this.dataDeliveryFormatsTree || this.dataProductDetailsTree || this.datadatasetSizeTree || this.datadatasetNumberOfRowsTree) ? 'Data Details' : '',
                    tooltip: 'Details related to the provider\'s data products.'
                }
            },{
                isTreeNodesWithPanel: true,
                treeNodes: [
                    this.dataIdentifiersAvailableTree,
                    this.dataPointInTimeAccuracyTree,
                    this.dataDataGapsTree,
                    this.dataDataGapsReasonsTree,
                    this.dataDuplicatesCleanedTree,
                    this.dataIncludesOutliersTree,
                    this.dataOutlierReasonsTree,
                    this.dataBiasesTree,
                    this.dataSampleOrPanelBiasesTree,
                    this.dataNormalizedTree
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (this.dataIdentifiersAvailableTree || this.dataPointInTimeAccuracyTree || this.dataDataGapsTree || this.dataDataGapsReasonsTree || this.dataDuplicatesCleanedTree || this.dataIncludesOutliersTree || this.dataOutlierReasonsTree || this.dataBiasesTree || this.dataSampleOrPanelBiasesTree || this.dataNormalizedTree) ? 'Data Quality' : '',
                }
            },{
                isTreeNodesWithPanel: true,
                treeNodes: [
                    this.dataDataUpdateFrequencyTree,
                    this.datadataUpdateFrequencyNotesTree,
                    this.dataDeliveryFrequenciesTree,
                    this.dataLagTimeTree,
                    this.dataDeliveryFrequencyNotesTree,
                    this.dataDeliveryMethodsTree,
                    this.dataDeliveryFormatsTree
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (this.dataDataUpdateFrequencyTree ||  this.datadataUpdateFrequencyNotesTree ||  this.dataDeliveryFrequenciesTree || this.dataLagTimeTree || this.dataDeliveryFrequencyNotesTree || this.dataDeliveryMethodsTree || this.dataDeliveryFormatsTree) ? 'Data Delivery' : '',
                }
            },{
                isTreeNodesWithPanel: true,
                treeNodes: [
                    this.dataLicensingAndTrialingTree,
                    this.dataDataLicenseTypeTree,
                    this.dataDataLicenseTypeDetailsTree
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (this.dataLicensingAndTrialingTree &&  this.dataLicensingAndTrialingTree.length ||  this.dataDataLicenseTypeTree ||  this.dataDataLicenseTypeDetailsTree ) ? 'Licensing & Pricing' : '',
                    tooltip: 'Details about the data products and how they are priced and licensed.'
                }
            },{
                isTreeNodesWithPanel: true,
                treeNodes: [
                    this.dataPotentialDrawbacksTree,
                    this.dataUniqueValuePropsTree,
                    this.dataLegalAndComplianceIssuesTree,
                    this.dataNotesTree,
                    //this.dataOurViewsTree,
                    this.datascoreInvestorsWillingnessTree,
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (this.dataPotentialDrawbacksTree || this.dataUniqueValuePropsTree || this.dataLegalAndComplianceIssuesTree || this.dataNotesTree || this.dataOurViewsTree || this.datascoreInvestorsWillingnessTree ) ? 'Amass Views' : '',
                    tooltip: 'Amass\'s opinions about the provider\'s offerings.',
                    editAccess: this.allowEditAccess
                }
            },{
                isTreeNodesWithPanel: true,
                treeNodes: [
                    this.dataCompetitiveDifferentiatorsTree,
                    this.dataCleanlinessTree,
                    this.dataAnalyticsTree,
                    this.dataUseCasesOrQuestionsAddressedTree,
                    this.dataDataRoadmapTree,
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (this.dataCompetitiveDifferentiatorsTree || this.dataUseCasesOrQuestionsAddressedTree || this.dataDataRoadmapTree || this.dataCleanlinessTree || this.dataAnalyticsTree) ? 'Provider\'s Views' : '',
                    tooltip: 'Provider\'s opinions about their offerings.',
                    editAccess: this.allowEditAccess,
                },
                buttons: detailsTabButtons
            }],
            subTabs: [{
                tabLabelName: (!this.restrictedAuthority) ? 'Provider Partners' : '',
                tabLabelRight: (!this.restrictedAuthority) ? '( ' + providerPartnersArray.length + ' )' : '',
                tabLabelTooltip: (!this.restrictedAuthority) ? 'External providers that the provider receives data from.':'',
                tabLabelRightTooltip: (!this.restrictedAuthority) ? 'Number of providers partnered with this provider.':'',
                tabContent: [{
                    contentType: TabContentType.LIST,
                    list: {
                        data: providerPartnersArray
                    },
                    noDataFoundMsg: (this.dataProvider && this.dataProvider.locked || !this.isAuthenticated()) ? 'Your account is not permitted to access the related providers for this provider.' : 'Provider Partners not found.',
                    buttons: (this.dataProvider && this.dataProvider.locked) ? partnersButtons : []
                }]
            }, {
                tabLabelName: (!this.restrictedAuthority) ? 'Distribution Partners': '',
                tabLabelRight: (!this.restrictedAuthority) ? '( ' + distributionPartnersArray.length + ' )':'',
                tabLabelTooltip: (!this.restrictedAuthority) ? 'External providers that have partnered with this this provider in order to distribute its data.':'',
                tabLabelRightTooltip:  (!this.restrictedAuthority) ? 'Number of providers partnered with this provider.':'',
                tabContent: [{
                    contentType: TabContentType.LIST,
                    list: {
                        data: distributionPartnersArray
                    },
                    noDataFoundMsg: (this.dataProvider && this.dataProvider.locked ||!this.isAuthenticated()) ? 'Your account is not permitted to access the related providers for this provider.' : 'Distribution Partners not found.',
                    buttons: (this.dataProvider && this.dataProvider.locked) ? partnersButtons : []
                }]
            }, {
                tabLabelName: (!this.restrictedAuthority) ?'Consumer Partners' :'',
                tabLabelRight: (!this.restrictedAuthority) ? '( ' + consumerPartnersArray.length + ' )':'',
                tabLabelTooltip: (!this.restrictedAuthority) ?'External providers that receive data from this provider and do not distribute, but use internally.':'',
                tabLabelRightTooltip: (!this.restrictedAuthority) ?'Number of providers partnered with this provider.':'',
                tabContent: [{
                    contentType: TabContentType.LIST,
                    list: {
                        data: consumerPartnersArray
                    },
                    noDataFoundMsg: (this.dataProvider && this.dataProvider.locked ||!this.isAuthenticated()) ? 'Your account is not permitted to access the related providers for this provider.' : 'Consumer Partners not found.',
                    buttons: (this.dataProvider && this.dataProvider.locked) ? partnersButtons : []
                }]
            }, {
                tabLabelName:(!this.restrictedAuthority) ? 'Competitors':'',
                tabLabelRight: (!this.restrictedAuthority) ?'( ' + competitorsArray.length + ' )':'',
                tabLabelTooltip: (!this.restrictedAuthority) ?'The closest direct competitors of this provider, self-reported by this provider.':'',
                tabLabelRightTooltip: (!this.restrictedAuthority) ? 'Number of providers partnered with this provider.':'',
                tabContent: [{
                    contentType: TabContentType.LIST,
                    list: {
                        data: competitorsArray
                    },
                    noDataFoundMsg: (this.dataProvider && this.dataProvider.locked) ? 'Your account is not permitted to access the related providers for this provider.' : 'Consumer Partners not found.',
                    buttons: (this.dataProvider && this.dataProvider.locked) ? partnersButtons : []
                }]
            }]
        }, {
            tabLabelName: this.genericProfileCard? 'Samples & Resources   ( ' + this.genericProfileCard.length + ' )' : 'Samples & Resources',
            tabLabelTooltip: 'Documents, multimedia, and data related to or written by this provider.',
            tabLabelRightTooltip: 'Number of resources matching active search and filter criteria.',
            tabContent: [{
                dataTable: this.genericProfileCard,
                // dataTableDisplayColumns: this.displayedColumns,
                //blockContent: this.allowEditAccess
            }, {
                plainText: (!this.dataSource && this.dataProvider && !this.dataProvider.locked) ? 'Samples & resources such as marketing materials, data dictionaries, data snapshots, full data samples, technical documentation, multimedia and others are not available for this provider yet.' : (this.dataProvider && this.dataProvider.locked || !this.isAuthenticated()) ? 'Your account is not permitted to access samples & resources for this provider.' : '' && (this.dataProvider && this.dataProvider.locked || !this.isAuthenticated()) ? 'You do not have permission to view the resources.' : '',
                buttons: resourcesButtons
            }]
        }];
        //this.resourcesArray.next(this.resource);
        this.tabsArray.next(this.tabs);
    }

    buttonActions(event: GenericProfileButtonsModel) {
        if (event && event.action && event.actionArguments) {
            this[event.action].apply(this, event.actionArguments);
        }
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        this.window.history.back();
    }
    resource()
    {
        this.dataresourceTabService.sendMessage('resources');
    }
    partners()
    {
        setTimeout(() => {
            window.scroll(0,document.body.scrollHeight);       
        }, 500);
         this.dataresourceTabService.sendMessage('partners');
       // window.scrollTo(0,document.body.scrollHeight);

    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    registerChangeInDataProviders() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dataProviderListModification',
            (response) => this.load(this.dataProvider.recordID)
        );
    }
    openSigninDialog() {
        this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
            width: '400px'
        });
    }

    openApplyDialog() {
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        });
	}
    followAction(providerID: number) {
        if(this.isAuthenticated())
        {
            this.dataProviderService.followAction(providerID).subscribe();
        }
        else
        {
            this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            });
        }
    }

    expandAction(providerID: number) {
        this.dataProviderService.expandAction(providerID).subscribe();
    }

    confirmExpandAction(providerID: number) {
        this.dataProviderService.confirmExpandAction(providerID).subscribe();
    }

    cancelExpandAction(providerID: number) {
        this.dataProviderService.cancelExpandAction(providerID).subscribe();
    }

    unlockAction(providerID: number) {
        this.dataProviderService.unlockAction(providerID).subscribe();
    }

    confirmUnlockAction(providerID: number) {
        this.dataProviderService.confirmUnlockAction(providerID).subscribe();
    }

    cancelUnlockAction(providerID: number) {
        this.dataProviderService.cancelUnlockAction(providerID).subscribe();
    }

    cancelMoreInformationAction(providerID: number, index: number, interestType) {
        //console.log(providerID, interestType, index, this.dataProviders);
		if (!this.dataProvider['userProviderStatus']) {
			this.dataProvider['userProviderStatus'] = [];
		}
        
        if(interestType === 261) {
            if (this.dataProvider['userProviderStatus'].indexOf(261) > -1) {
                this.dataProviderService.cancelMoreInformationAction(providerID, interestType).subscribe(resp => {
                    this.dataProvider['userProviderStatus'].push(270);
                    this.load(this.recordID);
                    this.loadTabs();
                    this.dataProvider['userProviderStatus'].splice(this.dataProvider['userProviderStatus'].indexOf(261), 1);
                }, err => {
    
                });
            }
        } else if(interestType === 566) {
            //console.log('TRUE', this.dataProviders[index]['userProviderStatus'].indexOf(566));
            if (this.dataProvider['userProviderStatus'].indexOf(566) > -1) {
                this.dataProviderService.cancelMoreInformationAction(providerID, interestType).subscribe(resp => {
                    this.dataProvider['userProviderStatus'].push(272);
                    this.load(this.recordID);
                    this.loadTabs();
                    this.dataProvider['userProviderStatus'].splice(this.dataProvider['userProviderStatus'].indexOf(566), 1);
                }, err => {
    
                });
                
            }
        } else if(interestType === 565) {
            if (this.dataProvider['userProviderStatus'].indexOf(565) > -1) {
                this.dataProviderService.cancelMoreInformationAction(providerID, interestType).subscribe(resp => {
                    this.dataProvider['userProviderStatus'].push(271);
                    this.load(this.recordID);
                    this.loadTabs();
                    this.dataProvider['userProviderStatus'].splice(this.dataProvider['userProviderStatus'].indexOf(565), 1);
                }, err => {
    
                });
                
            }
        }
		// if (!this.dataProvider['userProviderStatus']) {
		// 	this.dataProvider['userProviderStatus'] = [];
		// } switch (interestType) {
        //     case interestType === 261:
        //     this.dataProvider['userProviderStatus'].splice(this.dataProvider['userProviderStatus'].indexOf(261), 1);
        //     break;
        //     case interestType === 566:
        //     this.dataProvider['userProviderStatus'].splice(this.dataProvider['userProviderStatus'].indexOf(566), 1);
        //     break;
        //     case interestType === 565:
        //     this.dataProvider['userProviderStatus'].splice(this.dataProvider['userProviderStatus'].indexOf(565), 1);
        //     break;
        // }
        // if (interestType === 261) {
        //     this.dataProviderService.cancelMoreInformationAction(providerID, interestType).subscribe(resp => {
        //         this.dataProvider['userProviderStatus'].push(270);
        //         this.load(this.recordID);
        //         this.loadTabs();
        //     }, err => {

        //     });
        // } else if (interestType === 566) {
        //     this.dataProviderService.cancelMoreInformationAction(providerID, interestType).subscribe(resp => {
        //         this.dataProvider['userProviderStatus'].push(272);
        //         this.load(this.recordID);
        //         this.loadTabs();
        //     }, err => {

        //     });
        // } else if (interestType === 565) {
        //     this.dataProviderService.cancelMoreInformationAction(providerID, interestType).subscribe(resp => {
        //         this.dataProvider['userProviderStatus'].push(271);
        //         this.load(this.recordID);
        //         this.loadTabs();
        //     }, err => {

        //     });
        // }
	}

    openContactDialog(dialogTitle: string, dialogSubTitle: string, dialogType: string, providerID: number, categoryID?: number, index?: any, requestType?: number) {
        if(this.isAuthenticated())
        {
		const dialogRef = this.dialog.open(ContactDialogComponent, {
			width: '400px'
		});
		dialogRef.componentInstance.providerID = providerID ? providerID : null;
		dialogRef.componentInstance.categoryID = categoryID ? categoryID : null;
		dialogRef.componentInstance.dialogTitle = dialogTitle ? dialogTitle : 'Contact Us';
		dialogRef.componentInstance.dialogSubTitle = dialogSubTitle ? dialogSubTitle : null;
        dialogRef.componentInstance.dialogType = dialogType ? dialogType : 'contactUs';
        dialogRef.componentInstance.requestType = requestType ? requestType : null;

		dialogRef.componentInstance.onButtonAction.subscribe((action: any) => {
			if (action === 'success') {
				if (!this.dataProvider['userProviderStatus']) {
					this.dataProvider['userProviderStatus'] = [];
				}
				switch (dialogType) {
					case 'unlockProvider':
					this.dataProvider['userProviderStatus'].push(262);
					break;
					case 'moreInformation':
					this.dataProvider['userProviderStatus'].push(261);
                    break;
                    case 'requestDirectContact':
					this.dataProvider['userProviderStatus'].push(566);
                    break;
                    case 'requestDataSample':
					this.dataProvider['userProviderStatus'].push(565);
					break;
                }
                this.load(this.recordID);
                this.loadTabs();
			}
        });
    }
    else{
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        });
    }
    }

    getMouseOverExplanationByType(api: string) {
        this.dataProviderService.getMouseOverExplanationByType(api).subscribe(data => {
            switch (api) {
                case 'data-categories':
                this.categoryExplanation = data;
                break;
                case 'data-features':
                this.featureExplanation = data;
                break;
                case 'data-industries':
                this.industryExplanation = data;
                break;
                case 'provider-tags':
                this.providerTagExplanation = data;
                break;
                case 'data-provider-types':
                this.providerTypeExplanation = data;
                break;
                case 'data-sources':
                this.dataSourceExplanation = data;
                break;
                default:
                break;
            }
        });
    }

    getMouseOverExplanation(id: any, type: string) {
        if(this.isAuthenticated())
        {
        if (id) {
            this.dataProviderService.getMouseOverExplanation(id, type).subscribe((data) => {
                switch (type) {
                    case 'dataCategory':
                    this.categoryExplanation[id] = data;
                    break;
                    case 'dataFeature':
                    this.featureExplanation[id] = data;
                    break;
                    case 'dataIndustry':
                    this.industryExplanation[id] = data;
                    break;
                    case 'dataProviderTag':
                    this.providerTagExplanation[id] = data;
                    break;
                    case 'dataProviderType':
                    this.providerTypeExplanation[id] = data;
                    break;
                    case 'dataSource':
                    this.dataSourceExplanation[id] = data;
                    break;
                    default:
                    break;
                }
            });
        }
    }
}

    trackByIndex(index) {
        return index;
    }

    hasChild(_: number, node: TreeDataModel) {
        return node.children !==null && node.children.length > 0;
    }

    subTabLoadData(event) {
        this.loadRelevantArticlesPage(event);
    }

    openCommentsWindow(buttonType: string, privacy) {
        if(this.isAuthenticated())
        {
            this.filterStatus = privacy;
            this.commentsButtonType = buttonType;
            this.comments = [];
            this.displayCommentsSidebar = !this.displayCommentsSidebar;
            if(this.filterStatus === 'All') {
                this.filterCode = 1;
            }
            this.loadComments();
        }
       else
       {
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        });
       }
    }
    reloadComments(event) {
        console.log('event',event);
        const privacyValue = event;
        if(privacyValue === 'Private') {
            this.filterCode = 581;
            this.loadComments();
        } else if(privacyValue === 'Public') {
            this.filterCode = 582;
            this.loadComments();
        } else{
            this.filterCode = 1;
            this.loadComments();
        }
        /* if (this.filterCode) {
            this.loadComments();
        } */
    }
    loadComments() {
        this.dataProviderService.getCommentsByID(this.dataProvider?.id,  this.filterCode).subscribe(comments => {
            this.comments = comments;
        }, error => {
            this.comments = [];
        });
    }
    openNotesComments(buttonType: string, providerId, privacy) {
        console.log('privacy',privacy);
        this.filterStatus = privacy;
        if(this.filterStatus === 'private') {
            this.filterCode = 581;
        } else if(this.filterStatus === 'public') {
            this.filterCode = 582;
        } else{
            this.filterCode = 1;
        }
        this.commentsButtonType = buttonType;
        this.comments = [];
        this.displayCommentsSidebar = !this.displayCommentsSidebar;
        this.dataProviderService.getCommentsByID(providerId, this.filterCode).subscribe(comments => {
            const commentsList = comments;
            if(privacy === 'private') {
                for(let i=0; i<commentsList.length; i++) {
                    if(commentsList[i].privacy === 'private') {
                        this.comments = comments;
                    }
                }
            } else if(privacy === 'public') {
                for(let i=0; i<commentsList.length; i++) {
                    if(commentsList[i].privacy === 'public') {
                        this.comments = comments;
                    }
                }
            }
        }, error => {
            this.comments = [];
        });
    }

    mouseEnter(p) { // This event used for open and close the popup while mouseover the locked providers
        setTimeout(() => {
            p.close();
        }, 5000);
    }

    upgrade() {
        this.router.navigate(['/upgrade']);
    }

    onExpireSession(event) {
		//console.log('eventcheck',event);
        if (event) {
			//console.log('eventcheck',event);
          //  this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
				setTimeout(() => {
				this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
			}, 2000);
			}); 
        }
    }
	  beforeSessionExpires(event) {
        if (event) {
           // this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }
}

export interface DocumentsData {
    fileName: string;
    dataResourcePurpose: any;
    dataResourcePurposeType: any;
    dateCreated: string;
    fileID: string;
}

export class DocumentsDataSource extends DataSource <DocumentsData> {

    constructor(private data: DocumentsData[]) {
        super();
    }

    connect(): Observable <DocumentsData[]> {
        return observableOf(this.data);
    }

    disconnect() {}

  }

export interface TreeDataModel {
    id?: number;
    name: string;
    level?: number;
    children?: TreeDataModel[];
    tooltip?: string;
    desc?: string;
    tree: any;
}

const GetChildren = (node: TreeDataModel) => observableOf(node.children);
const TreeControl = new NestedTreeControl(GetChildren);

/**
 * File node data with nested structure.
 * Each node has a filename, and a type or a list of children.
 */
export class FileNode {
    children: FileNode[];
    filename: string;
    type: any;
    tooltip: any;
    dataLinks: any;
    locked: boolean;
}

/**
 * Tree node data with nested structure.
 * Each node has a label, and a tooltip or a list of children.
 */
export class AmassTreeNode {
    children: AmassTreeNode[];
    label: string;
    desc: string;
    id: any;
    tooltip: any;
    dataLinks: any;
    locked: boolean;
    params: any;
    ga: any;
    font: string;
}

/** Flat node with expandable and level information */
export class AmassTreeFlatNode {
    label: string;
    id: any;
    tooltip: any;
    params: any;
    ga: any;
    locked: boolean;
    level: number;
    expandable: boolean;
    font: string;
}

/** Flat node with expandable and level information */
export class FileFlatNode {
    filename: string;
    type: any;
    tooltip: any;
    dataLinks: any;
    locked: boolean;
    level: number;
    expandable: boolean;
}

/**
 * The file structure tree data in string. The data could be parsed into a Json object
 */
let TREE_DATA = {};
export class TreeNodes {
    data?: any;
    dataLinks?: any;
    tooltip?: string;
}

/**
 * File database, it can build a tree structured Json object from string.
 * Each node in Json object represents a file or a directory. For a file, it has filename and type.
 * For a directory, it has filename and children (a list of files or directories).
 * The input will be a json object string, and the output is a list of `FileNode` with nested
 * structure.
 */
@Injectable()
export class FileDatabase {
    dataChange: BehaviorSubject<AmassTreeNode[]> = new BehaviorSubject<AmassTreeNode[]>([]);
    treeData: any = {};

    get data(): AmassTreeNode[] { return this.dataChange.value; }

    constructor() {
        this.initialize();
    }

    initialize(treeData?: any) {
        if (treeData) {
            TREE_DATA = treeData;
        }
        // Parse the string to json object.
        const dataObject = TREE_DATA;
        /* const dataTooltip = TREE_DATA['tooltip'];
        const dataLinks = TREE_DATA['dataLinks']; */

        // Build the tree nodes from Json object. The result is a list of `FileNode` with nested
        //     file node as children.
        const data = this.buildAmassTree(dataObject, 0);

        // Notify the change.
        this.dataChange.next(data);
    }

    /**
     * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
     * The return value is the list of `FileNode`.
     */
    buildFileTree(value: any, level: number, tooltip?: any, dataLinks?: any): AmassTreeNode[] {
        const data: any[] = [];
        for (const k in value) {
            if (k) {
                const v = value[k];
                const node = new AmassTreeNode();
                node.label = `${k}`;
                node.tooltip = tooltip;
                node.locked = true;
                if (v === null || v === undefined) {
                    // no action
                } else if (typeof v === 'object') {
                    if (v.hasOwnProperty('length')) {
                        const obj = v.reduce(function(n, object) {
                            n[object.id] = object.desc;
                            return n;
                        }, {});
                        node.children = this.buildFileTree(obj, level + 1, null, dataLinks);
                        if (dataLinks) {
                            node.dataLinks = dataLinks.filter(i => {
                                return node.label === i.node;
                            })[0];
                        }
                    } else {
                        node.children = this.buildFileTree(v, level + 1);
                        if (dataLinks) {
                            node.dataLinks = dataLinks.filter(i => {
                                return node.label === i.node;
                            })[0];
                        }
                    }
                } else {
                    node.label = v;
                }
                data.push(node);
            }
        }
        return data;
    }

    buildAmassTree(array: any, level: number): AmassTreeNode[] {
        const data: any[] = [];
        //console.log('Detail', array);
        for (let obj of Object.keys(array)) {
            const node = new AmassTreeNode();
            var newObj = array[obj];
            if (newObj) {
                node.label = newObj.label || newObj.label || newObj.desc || newObj.description;
                node.tooltip = newObj.tooltip ? newObj.tooltip : null;
                node.params = newObj.params ? newObj.params : null;
                node.ga = newObj.ga ? newObj.ga : null;
                node.font = newObj.font ? newObj.font : null;
                if (newObj.children) {
                    node.children = this.buildAmassTree(newObj.children, level + 1);
                } else {
                    for (const k in newObj) {
                        if (k) {
                            node[k] = newObj[k];
                        }
                    }
                }
            }
            data.push(node);
        }
        return data;
    }
}

/**
 * @title Tree with flat nodes
 */
@Component({
    selector: 'jhi-flat-tree',
    templateUrl: './tree.component.html',
    styleUrls: ['./tree.component.css'],
    providers: [FileDatabase]
})
export class TreeFlatOverviewExampleComponent implements OnInit, AfterViewInit {

    treeControl: FlatTreeControl<AmassTreeFlatNode>;
    treeFlattener: MatTreeFlattener<AmassTreeNode, AmassTreeFlatNode>;
    dataSource: MatTreeFlatDataSource<AmassTreeNode, AmassTreeFlatNode>;
    @ViewChild('tree') tree;
    @Input() treeData;
    @Input() treeNodeData: boolean;
    updatedTreeData: BehaviorSubject<TreeNodes> = new BehaviorSubject<TreeNodes>({});
    constructor(database: FileDatabase, private router: Router, private googleAnalyticsService: GoogleAnalyticsEventsService,) {
        this.treeFlattener = new MatTreeFlattener(this.transformer, this._getLevel,
            this._isExpandable, this._getChildren);
        this.treeControl = new FlatTreeControl<AmassTreeFlatNode>(this._getLevel, this._isExpandable);
        this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
        database.dataChange.subscribe(data => {
            this.dataSource.data = data;
        });
        this.updatedTreeData.subscribe(data => {
            database.treeData = data;
            database.initialize(data);
        });
    }

    transformer = (node: AmassTreeNode, level: number) => {
        const flatNode = new AmassTreeFlatNode();
        flatNode.label = node.label;
        flatNode.id = node.id;
        flatNode.tooltip = node.tooltip;
        flatNode.params = node.params;
        flatNode.ga = node.ga;
        flatNode.font = node.font;
        flatNode.level = level;
        flatNode.expandable = !!node.children;
        flatNode.locked = node.locked;
        return flatNode;
    }

    private _getLevel = (node: AmassTreeFlatNode) => node.level;
    private _isExpandable = (node: AmassTreeFlatNode) => node.expandable;
    private _getChildren = (node: AmassTreeNode): Observable<AmassTreeNode[]> => {
        return observableOf(node.children);
    }

    hasChild = (_: number, _nodeData: AmassTreeFlatNode) => {
        return _nodeData.expandable;
    };

    ngOnInit() {
        this.updatedTreeData.next(this.treeData);
    }

    ngAfterViewInit() {
        this.tree.treeControl.expandAll();
    }

    anchorLink(node) {
        const parentNode = this.getParent(node);
        if (parentNode.ga) {
            this.gaEvent({
                label: parentNode.ga.labelPrefix + node.id + node[parentNode.ga.labelSuffix],
                category: parentNode.ga.category,
                event: parentNode.ga.event
            });
        }
        if (parentNode.params) {
            this.router.navigate(['/providers'], {
                queryParams: {
                    filter1: parentNode.params.filter,
                    value1: node.id,
                    type1: parentNode.params.type
                }
            });
        }
    }

    /**
     * Iterate over each node in reverse order and return the first node that has a lower level than the passed node.
     */
    getParent(node: AmassTreeFlatNode) {
        const { treeControl } = this;
        const currentLevel = treeControl.getLevel(node);

        if (currentLevel < 1) {
            return null;
        }

        const startIndex = treeControl.dataNodes.indexOf(node) - 1;

        for (let i = startIndex; i >= 0; i--) {
            const currentNode = treeControl.dataNodes[i];

            if (treeControl.getLevel(currentNode) < currentLevel) {
                return currentNode;
            }
        }
    }

    gaEvent(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
}
