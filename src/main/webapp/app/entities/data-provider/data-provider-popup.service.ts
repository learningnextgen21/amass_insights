import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DataProvider } from './data-provider.model';
import { DataProviderService } from './data-provider.service';

@Injectable()
export class DataProviderPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private dataProviderService: DataProviderService
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dataProviderService.find(id).subscribe(dataProvider => {
                    if (dataProvider.createdDate) {
                        dataProvider.createdDate = {
                            year: dataProvider.createdDate.getFullYear(),
                            month: dataProvider.createdDate.getMonth() + 1,
                            day: dataProvider.createdDate.getDate()
                        };
                    }
                    dataProvider.updatedDate = this.datePipe.transform(dataProvider.updatedDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.dataProviderModalRef(component, dataProvider);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dataProviderModalRef(component, new DataProvider());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dataProviderModalRef(component: Component, dataProvider: DataProvider): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.dataProvider = dataProvider;
        modalRef.result.then(
            result => {
                this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true });
                this.ngbModalRef = null;
            },
            reason => {
                this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true });
                this.ngbModalRef = null;
            }
        );
        return modalRef;
    }
}
