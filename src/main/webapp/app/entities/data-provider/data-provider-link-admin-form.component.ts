
import {of as observableOf, Subject} from 'rxjs';
import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, Inject, Injectable, Input, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { DomSanitizer } from '@angular/platform-browser';
import { DataProvider } from './data-provider.model';
import { DataProviderService } from './data-provider.service';
import { ContactDialogComponent } from '../../shared/contactdialog/contact-dialog.component';
import { Principal } from '../../shared/auth/principal.service';
import { DataSource } from '@angular/cdk/table';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { LoaderService } from '../../loader/loaders.service';
import { NestedTreeControl, FlatTreeControl } from '@angular/cdk/tree';
import { WINDOW } from 'app/layouts';
import { SigninModalService, AmassFilterService, CSRFService, AuthServerProvider, StateStorageService } from 'app/shared';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig, FormlyTemplateOptions } from '@ngx-formly/core';
import { ConfirmDialogComponent } from 'app/shared/dialogs/confirm-dialog.component';
import { UppyService, UppyComponent } from 'app/uppy';
import { contains } from 'ramda';
let _that = null;
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie';
import { DataOrganizationAdminFormComponent } from './data-organization-admin-form.component';

@Component({
	selector: 'jhi-data-provider-link-admin-form',
	templateUrl: './data-provider-link-admin-form.component.html',
	styleUrls: [ '../../../content/css/material-tab.css']
})
export class DataProviderLinkAdminFormComponent implements OnInit {
    dialogRef: MatDialogRef<ConfirmDialogComponent>;
    //linkDialogRef: MatDialogRef<DataProviderLinkAdminFormComponent>;
	orgDialogRef: MatDialogRef<any>;
    formSubmitted: boolean;
    routerOutletRedirect: string;

    dataLinks: DataProvider;
	providerID: any;
    dataProviderId: any;
    valueChanged: boolean;
    linkModel: any;
    editLinkRecordId: any;
	private subscription: Subscription;
	private eventSubscriber: Subscription;
	isLoading: boolean;
	isAdmin: boolean;
	isPendingUnlock: boolean;
	isPendingExpand: boolean;
	emptyField: boolean;
	domSanitizer: any;
	recordID: string;
    organizationID: any;
    editLinkTitle: any;
    basicModelID: any;
    linkRecordID: any;
    editLinkId: any;
	orgID: any;
	id: string;
	profileTabLabel: any;
    ProfileCategory: any;
    trackname: any;
    linkTitle: any;
    linkCategories: any;
    trackwebsite: any;
    trackownerOrganizationCity: any;
    trackownerOrganizationState: any;
    trackownerOrganizationCountry: any;
    trackownerOrganizationYearfound: number;
    equitesDelete: any;
    tabIndex: any;
    closePopup: boolean;
    redirect: any;
    defaultValue: any;

	progressBarColor = 'primary';
	progressBarMode = 'determinate';
	mdTooltipDelay: number;
    linkCategory: any[]= [];
    panelName:string;
    overAll:string;
    progressSpinner:boolean;

	basicExpansionPanel: boolean;
	companyExpansionPanel: boolean;
	adminExpansionPanel: boolean;
	basicForm = new FormGroup({});
	basicModel: any = {};
	providerFormModel: any = {};
	basicOptions: FormlyFormOptions = {};

	basicFields: FormlyFieldConfig[] = [
		{
			key: 'link',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Link',
				placeholder: '',
				description: '',
				required: true,
				attributes: {
					'placeholderText' : 'Link that has some important/relevant information that needs to be captured for research purposes.',
					'labelTooltip': 'This should be a clean link without "http://" or "https://". And if the link contains variables after a "?" with "utm" in them, erase everything after the "?"e.g. "http://www.google.com/?utm_source=abc&query=123" should be changed to "www.google.com"',
                    'descriptionTooltip': '',
                    'placeholderTooltip': 'This should be a clean link without "http://" or "https://". And if the link contains variables after a "?" with "utm" in them, erase everything after the "?"e.g. "http://www.google.com/?utm_source=abc&query=123" should be changed to "www.google.com"',
                    'trackNames': 'Link',
                    'trackCategory': 'Link Admin - Input',
                    'trackLabel': 'Link Admin - Textbox'
                },
                change: this.basicInput,
				keypress:(event)=> {
                    // console.log('Event', event);
                    if(event) {
                        this.valueChanged = true;
                        // console.log('Event 1', this.valueChanged);
                    }
                },
			},
		},
		{
			key: 'linkTitle',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Link Title:',
				type: 'input',
				placeholder: '',
				description: '',
				attributes: {
					'placeholderText' : 'Title of the webpage.',
					'labelTooltip': 'This usually appears in the tab header.',
                    'descriptionTooltip': '',
                    'placeholderTooltip': 'This usually appears in the tab header.',
                    'trackNames': 'Link Title',
                    'trackCategory': 'Link Admin - Input',
                    'trackLabel': 'Link Admin - Textbox'
                },
                maxLength : 256,
                change: this.basicInput,
                keypress:(event)=> {
                    // console.log('Event', event);
                    if(event) {
                        this.valueChanged = true;
                        // console.log('Event 1', this.valueChanged);
                    }
                },
            }
        },
        {
			key: 'dataLinksCategory',
            type: 'multiselect',
            className: 'custom-multiselect',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Link Category:',
				placeholder: '',
				description: 'What type of link is this?',
                required: true,
                options: this.linkCategory,
				attributes: {
                    field: 'desc',
					'placeholderText' : '',
					'labelTooltip': '',
                    'descriptionTooltip': '',
                    'placeholderTooltip': '',
                    'trackNames': 'Link Category',
                    'trackCategory': 'Link Admin - Input',
                    'trackLabel': 'Link Admin - Dropdown - Link Category'
				},
				change: (event: any) => {
                   // console.log('MULTI', event);
                    if(event) {
                        this.valueChanged = true;
                    }
                }
            },
            validation: {
                messages: {
                    required: 'Link Category is required'
                }
            }
		}
	];

	csrf: string;
	constructor(
		private dataProviderService: DataProviderService,
		private filterService: AmassFilterService,
		private route: ActivatedRoute,
		private router: Router,
        public dialog: MatDialog,
        public linkDialogRef: MatDialogRef<DataProviderLinkAdminFormComponent>,
        private principal: Principal,
        private authService: AuthServerProvider,
		public _DomSanitizer: DomSanitizer,
		public googleAnalyticsService: GoogleAnalyticsEventsService,
		private loaderService: LoaderService,
		private signinModalService: SigninModalService,
		@Inject(WINDOW) private window: Window,
		public snackBar: MatSnackBar,
		private uppyService: UppyService,
        private csrfService: CSRFService,
        private el: ElementRef,
        private stateService: StateStorageService,
		private cookieService: CookieService
	) {
        this.routerOutletRedirect = '';
		this.domSanitizer = DomSanitizer;
		this.formSubmitted = false;
		// this.uppyEvent
		// 	.takeUntil(this.onDestroy$)
		// 	.filter(([ev]) => contains(ev, ['file-added']))
		// 	.subscribe(
		// 		([ev, data1, data2, data3]) => {
		// 			/* console.log("Received '" + ev + "' event from instance 1", 'Upload complete');
		// 			console.log(data1);
		// 			console.log(data2);
		// 			console.log(data3);
		// 			console.log(ev); */
		// 		},
		// 		err => console.log(err),
		// 		() => console.log('done')
		// 	);
	}
	openUppyDialog(events: any) {
		// this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        const dialogRef = this.dialog.open(UppyComponent, {
			width: '500px',
			height: '3000px'
		});
		// dialogRef.afterClosed().subscribe(result => {
		// 	console.log('The dialog was closed');
		//   });
	}
	ngOnInit() {
        console.log(this.defaultValue);
        console.log('Link', this.basicModel);
        this.editLinkTitle = this.basicModel.link;
        this.editLinkId = this.basicModel.id;
        this.editLinkRecordId = this.basicModel.recordID;
		_that = this;
        this.csrf = this.csrfService.getCSRF();
        this.filterService.setAuthorisedUrl();
        this.loaderService.display(false);
        this.valueChanged = false;
		this.isLoading = false;
		this.isPendingUnlock = false;
		this.isPendingExpand = false;
		this.tabIndex = 0;
		this.basicExpansionPanel = true;
		this.companyExpansionPanel = false;
		this.adminExpansionPanel = false;
		this.route.queryParams.subscribe(params => {
			if (params['pending-unlock']) {
				this.isPendingUnlock = true;
			}

			if (params['pending-expand']) {
				this.isPendingExpand = true;
			}
		});
		this.subscription = this.route.params.subscribe(params => {
            this.recordID = params['recordID'];
            if (this.defaultValue === 'editLink') {
                //this.load(params['recordID']);
            }
			// this.load(params['recordID']);
        });
		this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
		/* this.relevantArticlePage = 0;
		this.relevantArticleLinks = {
			last: 0
		};
		this.authoredArticlePage = 0;
		this.authoredArticleLinks = {
			last: 0
		}; */
        this.mdTooltipDelay = 500;
        this.linkCategory = [];
		this.filterService.getLookupCodes('LINK_CATEGORY').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.linkCategory.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
                });
                //console.log(this.linkCategory);
			}
			 this.basicFields[2].templateOptions.options = this.linkCategory;
        });
        if(this.basicModel) {
            if (this.basicModel.dataLinksCategory) {
                console.log('Workflows',this.basicModel.dataLinksCategory);
                const dataLinksCategory = [];
                for (const obj in this.basicModel.dataLinksCategory) {
                    if (obj) {
                        dataLinksCategory.push({
                            id: this.basicModel.dataLinksCategory[obj].id,
                            desc: this.basicModel.dataLinksCategory[obj].lookupcode
                        });
                    }
                }
                delete this.basicModel.dataLinksCategory;
                this.basicModel.dataLinksCategory = dataLinksCategory;
            }
        }
        this.dataProviderService.getDataLinks().subscribe(dataLink => {
                     this.dataLinks = dataLink['hits'].total ? dataLink['hits'].hits[0]._source : null;
                      console.log(this.dataLinks);
             		this.isLoading = false;
             		this.constructFormModel(this.dataLinks);
     	});
    }

	providerProfileTab(event: MatTabChangeEvent) {

	   /*  this.profileTabLabel = 'Provider Profile - Tab ' + event.tab.textLabel;
		this.ProfileCategory = 'Provider Profile ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent(this.ProfileCategory, 'Tab Link Clicked', this.profileTabLabel); */
	}

	providerFollow(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	unlockProviders(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	requestMoreInfo(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	providerPartnerTab(event: MatTabChangeEvent) {
		this.profileTabLabel = 'Provider Profile -  ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent('Partner Provider', 'Internal Link Clicked', this.profileTabLabel);
	}

	dataFieldLink(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	profileWebsite(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	profileArticle(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
	samplesTab(events) {
		console.log(events);
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	// load(recordID) {
	// 	this.dataProviderService.getDataLinks().subscribe(dataLink => {
    //         // console.log('dataProvider', dataProvider);
	// 		this.loaderService.display(false);
    //         this.dataLinks = dataLink['hits'].total ? dataLink['hits'].hits[0]._source : null;
    //         //console.log(this.dataLinks);
	// 		this.dataProviderId = this.dataLinks ? this.dataLinks.id : null;
	// 		this.isLoading = false;
	// 		this.constructFormModel(this.dataLinks);
    //         this.basicModel = this.dataLinks;
    //         //console.log(this.basicModel);
	// 		this.providerID = this.dataLinks ? this.dataLinks.id : null;
	// 	}, error => {
	// 		if (error.status === 401) {
    //             this.stateService.storeUrl(null);
    //             this.router.navigate(['']);
	// 			this.signinModalService.openDialog('Session expired! Please re-login.');
	// 		}
	// 		this.dataLinks = null;
	// 		this.isLoading = false;
	// 	});
	//    // this.loaderService.display(false);
    // }

     constructFormModel(provider: DataProvider) {
         console.log('link provider......', provider);
         if(!this.editLinkTitle) {
            this.trackname = provider.link ? 0 : 0;
            this.linkTitle = provider.linkTitle ? 0 : 0;
         } else{
            console.log('link provider......', provider);
            this.trackname = provider.link ? provider.link : 0;
            this.linkTitle = provider.linkTitle ? provider.linkTitle: 0;
         }
     }

	previousState() {
		this.window.history.back();
	}

	// ngOnDestroy() {
	// 	this.onDestroy$.next();
    //     this.onDestroy$.complete();
    //     this.cookieService.put('logoutClicked', '0');
	// }

	openConfirmationDialog(nextState?: RouterStateSnapshot) {
        console.log(nextState);
        if(nextState) {
        this.redirect = nextState.url;
        }
        //this.dialog.closeAll();
        this.dialogRef = this.dialog.open(ConfirmDialogComponent, {
            disableClose: false
        });
        this.dialogRef.componentInstance.confirmMessage = 'You have some unsaved edits in this form.';
        this.dialogRef.componentInstance.button = {
            leave: true,
            confirm: true,
            leaveText: 'Leave and Don\'t Save',
            confirmText: 'Save and Submit'
        };
        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {
                console.log(result);
                // do confirmation actions
                if (this.redirect === '/') {
                    this.authService.logout().subscribe();
                    this.formSubmitted = true;
                    this.principal.authenticate(null);
                    this.stateService.storeUrl(null);
                    this.router.navigate(['../../']);
                } else {
                    this.formSubmitted = true;
                    if(this.redirect) {
                        this.linkDialogRef.close();
                    } else if (this.principal.isAuthenticated()) {
                        this.stateService.storeUrl(null);
                        if(this.redirect) {
                            this.router.navigate([this.redirect]);
                            this.dialog.closeAll();
                            this.linkDialogRef.close();
                        }
                    } else {
                        this.stateService.storeUrl(null);
                        this.router.navigate(['']);
                        this.dialog.closeAll();
                    }
                }

                if (result === 'save') {
                    //console.log('Popup', this.basicProvider, this.action);
                    this.submitAdminEditLink(this.basicModel);
                    this.linkDialogRef.close();
                }
                if (result === 'leave') {
                    this.linkDialogRef.close();
                }
                this.dialogRef = null;
            }
        });
		return false;
    }

	trackByIndex(index) {
		return index;
    }
    closeSignUpDialog() {
        if(this.valueChanged === false) {
            this.linkDialogRef.close();
        }
        if(this.valueChanged === true) {
            if(this.formSubmitted) {
                this.openConfirmationDialog();
            } else {
                console.log('close');
                if(this.formSubmitted === true) {
                    console.log('Checked');
                    this.openConfirmationDialog();
                } else if(this.formSubmitted === false) {
                    console.log('Opened');
                    this.openConfirmationDialog(); 
                }
            }
        }
    }

    onClose() {
        this.linkDialogRef.close(this.linkModel);
    }

	updateLink(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, submitMessage) {
         delete model['id'];
         delete model['recordID'];
        if(this.editLinkId && this.editLinkRecordId) {
            model['id'] = this.editLinkId;
            model['recordID'] = this.editLinkRecordId;
        } else {
            model['id'] = this.basicModelID;
            model['recordID'] = this.linkRecordID;
        }
        this.progressSpinner=true;
        this.dataProviderService.updateLinkAdmin(model).subscribe(data => {
            if (data) {
                this.progressSpinner=false;
                this.formSubmitted = true;
                //this.basicModel = data;
                this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                this.linkModel = data;
                this.onClose();
                this.loaderService.display(false);
                if (currentPanel && currentPanel !== '') {
                    this[currentPanel] = false;
                    if (nextPanel && nextPanel !== '') {
                        this[nextPanel] = true;
                    }
                    if (tabIndex) {
                        this.tabIndex = tabIndex;
                    }
                }
            } else {
                this.progressSpinner=false;
                this.formSubmitted = false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        }, error => {
            if (error.status === 401) {
                this.progressSpinner=false;
                this.formSubmitted = false;
                this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
            } else {
                this.progressSpinner=false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        });
    }

    insertLink(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, submitMessage) {
       this.progressSpinner=true;
        this.dataProviderService.addLinkAdmin(model).subscribe(data => {
            if (data) {
                this.progressSpinner=false;
                this.formSubmitted = true;
                console.log('Insert Data',data);
                this.basicModelID = data.id;
                this.linkRecordID = data.recordID;
                this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                this.linkModel = data;
                this.onClose();
                this.loaderService.display(false);


                if (currentPanel && currentPanel !== '') {
                    this[currentPanel] = false;
                    if (nextPanel && nextPanel !== '') {
                        this[nextPanel] = true;
                    }
                    if (tabIndex) {
                        this.tabIndex = tabIndex;
                    }
                }
            } else {
                this.formSubmitted = false;
                this.progressSpinner=false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        }, error => {
            if (error.status === 401) {
                this.formSubmitted = false;
                this.progressSpinner=false;
                this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
            } else {
                this.progressSpinner=false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        });
    }

	submitAdminEditLink(model, panelName?: string, currentPanel?: string, nextPanel?: string, tabIndex?: number, form?: string, skipRedirect?: boolean) {
		if(panelName !=='Overall Details')
		{
			this.panelName = panelName;
            this.overAll='';
		}
		else
		{
		
            this.overAll= panelName;
            panelName = '';
		}
       // this.panelName=panelName;
        const formName = panelName ? panelName + '' : 'Form';
       // const submitMessage = model.id ? 'Your edits have been successfully submitted and have been applied to the profile immediately.' : `The Link with the name "${model.link}" has been successfully submitted and has been added to the database immediately.`;
        const filterArr = Array.prototype.filter;
        if (form && this[form].valid) {
            const autoCompleteFields = this.el.nativeElement.querySelectorAll('#' + form + ' .ui-autocomplete-input');
            const filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                return node.value === '';
            });
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else {
                console.log('SubmitModel', model);
                if (model && !this.basicModelID && !this.linkRecordID) {
                    console.log('Model', model, model.recordID);
                    const insertSubmitMessage = `The Link with the name "${model.link}" has been successfully submitted and has been added to the database immediately.`;
                    this.insertLink(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, insertSubmitMessage);
                } else {
                    const updateSubmitMessage = 'Your edits have been successfully submitted and have been applied to the profile immediately.';
                    this.updateLink(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, updateSubmitMessage);
                }
            }
        } else {
            let invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
            let autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
            let filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                return node.required && node.value === '';
            });
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else if (invalidElements.length > 0) {
                if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                    invalidElements[0].querySelectorAll('input')[0].focus();
                } else {
                    setTimeout(() => {
                        invalidElements[0].focus();
                    }, 1000);
                }
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else {
                this.tabIndex = this.tabIndex === 0 ? 1 : 0;
                setTimeout(() => {
                    invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
                    autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
                    filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                        return node.required && node.value === '';
                    });
                    if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                        setTimeout(() => {
                            filteredAutoCompleteFields[0].focus();
                        }, 1000);
                        this.openSnackBar('Please fill all the required fields!', 'Close');
                        this.expandFormPanels();
                    } else if (invalidElements.length > 0) {
                        if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                            invalidElements[0].querySelectorAll('input')[0].focus();
                        } else {
                            setTimeout(() => {
                                invalidElements[0].focus();
                            }, 1000);
                        }
                        this.openSnackBar('Please fill all the required fields!', 'Close');
                        this.expandFormPanels();
                    } else {
                        console.log('Post', model);
                        console.log('BasicModelID', this.basicModelID);
                        console.log('BasicModelID', this.linkRecordID);
                        if (model && !this.basicModelID && !this.linkRecordID && !this.editLinkId) {
                            console.log('Insert');
                            const insertSubmitMessage = `The Link with the name "${model.link}" has been successfully submitted and has been added to the database immediately.`;
                            this.insertLink(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, insertSubmitMessage);
                        } else {
                            console.log('Update');
                            const updateSubmitMessage = 'Your edits have been successfully submitted and have been applied to the profile immediately.';
                            this.updateLink(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, updateSubmitMessage);
                        }
                    }
                }, 2000);
            }
        }
    }

    expandFormPanels() {
        this.basicExpansionPanel = true;
        this.companyExpansionPanel = true;
		this.adminExpansionPanel= true;
    }

    formSubmit(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    formSave(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    basicInput($event) {
        const inputKey = $event;
         console.log(inputKey);
        /* delete entry */
        if (inputKey.key === 'link') {
            console.log('......', _that.trackname)
            if (inputKey.model.link.length < _that.trackname.length) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
                console.log(inputKey.templateOptions.attributes.trackCategory, 'Deleted' ,inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.link.length > _that.trackname) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
                console.log(inputKey.templateOptions.attributes.trackCategory, 'Added' ,inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'linkTitle') {
            if ( inputKey.model.linkTitle.length < _that.linkTitle.length) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
                console.log(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.linkTitle.length > _that.linkTitle) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
                console.log(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } 
    }

    openSnackBar(message: string, action: string, delay?: number) {
        this.snackBar.open(message, action, {
            duration: delay ? delay : 10000,
            panelClass: ['blue-snackbar']
        });
    }

	submitBasic(model) {
		// console.log(model);
		this.basicExpansionPanel = false;
		this.companyExpansionPanel = true;
	}

	submitCompanyDetails(model) {
		// console.log(model);
		this.companyExpansionPanel = false;
		this.adminExpansionPanel = true;
	}

	submitAdmin(model) {
		// console.log(model);
		this.adminExpansionPanel = false;
		/* this.descriptionsExpansionPanel = true; */
	}

	submitDescriptions(model) {
		// console.log(model);
		/* this.descriptionsExpansionPanel = false;
		this.categorizationsExpansionPanel = true; */
	}

	submitCategorizations(model) {
		// console.log(model);
	/* 	this.categorizationsExpansionPanel = false;
		this.investigationExpansionPanel = true; */
	}

	submitInvestigationDetails(model) {
		console.log(model);
	/* 	this.investigationExpansionPanel = false; */
		this.tabIndex = 1;
	}

	submitRelatedProviders(model) {
		// console.log(model);
		/* this.relatedProvidersExpansionPanel = false;
		this.dataDetailsExpansionPanel = true; */
	}

	submitDataDetails(model) {
		// console.log(model);
		/* this.dataDetailsExpansionPanel = false;
		this.dataQualityExpansionPanel = true; */
	}

	submitDataQuality(model) {
		// console.log(model);
		/* this.dataQualityExpansionPanel = false;
		this.dataDeliveryExpansionPanel = true; */
	}

	submitDataDelivery(model) {
		// console.log(model);
		/* this.dataDeliveryExpansionPanel = false;
		this.triallingExpansionPanel = true; */
	}

	submitTrialling(model) {
		// console.log(model);
		/* this.triallingExpansionPanel = false;
		this.licensingExpansionPanel = true; */
	}

	submitLicensing(model) {
		// console.log(model);
		/* this.licensingExpansionPanel = false;
		this.pricingExpansionPanel = true; */
	}

	submitPricing(model) {
		// console.log(model);
		/* this.pricingExpansionPanel = false;
		this.discussionExpansionPanel = true; */
	}

	submitDiscussion(model) {
		// console.log(model);
		/* this.discussionExpansionPanel = false; */
    }

    beforeSessionExpires(event) {
        if (event) {
            this.submitAdminEditLink(this.basicModel, '', '', '', 0, '', true);
        }
    }

    onExpireSession(event) {
        if (event) {
            this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
                this.signinModalService.openDialog('Session expired! Please re-login.');
            });
        }
    }

    @HostListener('document:keyup', [])
    @HostListener('document:click', [])
    @HostListener('document:wheel', [])
    @HostListener('window:scroll', [])
    resetTimer() {
        this.authService.notifyUserAction();
    }

    @HostListener('window:popstate', [])
    onPopState() {
        this.cookieService.put('logoutClicked', '0');

    }
}
