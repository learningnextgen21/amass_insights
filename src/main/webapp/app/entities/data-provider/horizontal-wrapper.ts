import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
	selector: 'jhi-formly-horizontal-wrapper',
	template: `
	<div class="form-group row">
	  <label [attr.for]="id" class="col-sm-2 col-form-label opensans-bold" *ngIf="to.label" [ngStyle]="{'padding-right': '22px'}">
        <span *ngIf="to.attributes" [matTooltip]="to.attributes['labelTooltip']">{{ to.label }}</span>
        <span *ngIf="!to.attributes">{{to.label}}</span>
		<ng-container *ngIf="to.required && to.hideRequiredMarker !== true">*</ng-container>
	  </label>
	  <div class="col-sm-10">
        <label *ngIf="to.description" class="form-description">
            <span *ngIf="to.attributes" [matTooltip]="to.attributes['descriptionTooltip']">{{ to.description }}</span>
            <span *ngIf="!to.attributes">{{to.description}}</span>
        </label>
        <ng-template #fieldComponent></ng-template>
        <span class="form-placeholder-explanation" *ngIf="to.attributes && to.attributes['explanation']; else showPlaceholderText">{{to.attributes['explanation']}}</span>
        <ng-template #showPlaceholderText>
            <span *ngIf="to.attributes && to.attributes['placeholderText']" [matTooltip]="to.attributes['placeholderTooltip']" [innerHTML]="to.attributes['placeholderText']"></span>
            <div *ngIf="to.attributes && to.attributes['placeholderText1'] && to.attributes['placeholderText2'] && to.attributes['placeholderText3']">
              <span *ngIf="to.attributes && to.attributes['placeholderText1']">{{to.attributes['placeholderText1']}}</span><br>
              <span *ngIf="to.attributes && to.attributes['placeholderText2']"  [innerHTML]="to.attributes['placeholderText2']" ></span><br>
              <span *ngIf="to.attributes && to.attributes['placeholderText3']"  [innerHTML]="to.attributes['placeholderText3']"></span>
            </div>
            <span *ngIf="!to.attributes && to.placeholder">{{to.placeholder}}</span>
        </ng-template>
    </div>
  </div>
  `,
	styleUrls: [
		'./horizontal-wrapper.css'
	]
})
export class FormlyHorizontalWrapperComponent extends FieldWrapper {
	@ViewChild('fieldComponent', { read: ViewContainerRef, static: true }) fieldComponent: ViewContainerRef;
}

