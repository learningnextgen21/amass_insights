import { Injectable } from '@angular/core';
import { Subject} from 'rxjs';

@Injectable()
export class DataProviderFilterService {
    private clearFilterSearch = new Subject<any>();
    clearFilterSearch$ = this.clearFilterSearch.asObservable();

    constructor()
    {

    }

    clearFilterRemoveSearch(clearFilterSearch: any)
    {
        console.log('message',clearFilterSearch);
        this.clearFilterSearch.next(clearFilterSearch);
    }
}