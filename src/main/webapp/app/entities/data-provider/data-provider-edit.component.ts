
import {filter, takeUntil} from 'rxjs/operators';

import {of as observableOf, Subject,  Subscription, Observable, BehaviorSubject } from 'rxjs';
import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, Inject, Injectable, Input, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { DomSanitizer } from '@angular/platform-browser';
import { DataProvider } from './data-provider.model';
import { DataProviderService } from './data-provider.service';
import { ContactDialogComponent } from '../../shared/contactdialog/contact-dialog.component';
import { Principal } from '../../shared/auth/principal.service';
import { DataSource } from '@angular/cdk/table';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { LoaderService } from '../../loader/loaders.service';
import { NestedTreeControl, FlatTreeControl } from '@angular/cdk/tree';
import { WINDOW } from 'app/layouts';
import { SigninModalService, AmassFilterService, CSRFService, AuthServerProvider, StateStorageService } from 'app/shared';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig, Field } from '@ngx-formly/core';
import { ConfirmDialogComponent } from 'app/shared/dialogs/confirm-dialog.component';
import { UppyService } from 'app/uppy';
import { contains } from 'ramda';
let _that = null;
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie';
import { AccountService } from 'app/shared/auth/account.service';
import { UppyDialogComponent } from 'app/uppy/uppy/uppy-dialog.component';
import { AddResourceAdminService } from 'app/add-resource-admin/add-resource-admin.service';
import { DataResource } from 'app/add-resource-admin/add-resource-admin-form.model';
import { HttpClient } from '@angular/common/http';
import { ProviderUserOnboardingService } from 'app/account/provider-user-onboarding/provider-user-onboarding.service';
import { UserOnboardingService } from 'app/account/user-onboarding/user-onboarding.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'jhi-data-provider-edit',
    templateUrl: './data-provider-edit.component.html',
    styleUrls: ['../../../content/css/material-tab.css']
})
export class DataProviderEditComponent implements OnInit, OnDestroy, AfterViewInit {
	dialogRef: MatDialogRef<ConfirmDialogComponent>;
	orgDialogRef:  MatDialogRef<any>;
    formSubmitted: boolean;
	routerOutletRedirect: string;

	resourceObj: any;
	agreeValidationMessage: any;
	dataProvider: DataProvider;
	dataProviderID: any;
	marketingLiteratureList: DataResource[] = [];
	dataDictionaryList: DataResource[] = [];
	samplesAndExamplesList: DataResource[] = [];
	reportsAnalysesList: DataResource[] = [];
	marketingLiteratureResource: DataResource[] = [];
	private subscription: Subscription;
	private eventSubscriber: Subscription;
    unsavedSubscription: Subscription;
	hideProviderInputField: boolean;
	hideDistributionInputField: boolean;
	hideConsumerInputField: boolean;
	hideComplementaryInputField: boolean;
	hideCompetitorsInputField: boolean;
	hideMarketingSection: boolean;
	hideDocumentSection: boolean;
	hideSamplesSection: boolean;
	hideReportSection: boolean;
	pendingEdits: boolean;
	isLoading: boolean;
	isAdmin: boolean;
    forceFormSubmit: boolean = false;
    userObj: any;
	isPendingUnlock: boolean;
	isPendingExpand: boolean;
	domSanitizer: any;
	authoredArticles: any;
	relevantArticles: any;
	tabIndex: number;
	relevantArticlePage: number;
	relevantArticleLinks: any;
	relevantArticleFrom: number;
	totalRelevantArticles: number;
	authoredArticlePage: number;
	authoredArticleLinks: any;
	authoredArticleFrom: number;
	totalAuthoredArticles: number;
	recordID: string;
	profileTabLabel: any;
    ProfileCategory: any;
    trackname: any;
    trackwebsite: any;
    trackownerOrganizationCity: any;
    trackownerOrganizationState: any;
    trackownerOrganizationCountry: any;
    trackownerOrganizationYearfound: number;
    trackownerOrganizationHeadcount: any;
    trackShortDescription: any;
    trackLongDescription: any;
    trackInvestor: any;
    trackNumberOfInvestor: any;
    trackDataCollection: any;
    trackpublicCompaniesCovered: any;
    trackpublicEquitesCovered: any;
    trackYearFirstDataProductLaunched: any;
    trackNumberOfAnalystEmployees: any;
    trackDataSourcesDetails: any;
    trackCompetitiveDifferentiators: any;
    trackcollectionMethodsExplanation: any;
    tracksampleOrPanelSize: any;
    trackDateCollectionRangeExplanation: any;
    trackOutOfSampleData: any;
    trackProductDetails: any;
    trackLagTime: any;
    trackDeliveryFrequencyNotes: any;
    trackDataUpdateFrequencyNotes: any;
    trackTrialDuration: any;
    trackTrialPricingDetails: any;
    trackDataLicenseTypeDetails: any;
    trackMaximumYearlyPrice: any;
    trackMinimumYearlyPrice: any;
    trackPricingDetails: any;
    trackSampleOrPanelBiases: any;
    trackUseCasesOrQuestionsAddressed: any;
    trackQualityMethods: any;
    trackDiscussionCleanlines: any;
    trackDiscussionAnalytics: any;
    trackPotentialDrawbacks: any;
    trackDataRoadmap: any;
    trackNotes: any;
	equitesDelete: any;
	activeMarkettingLink: boolean;
	activeDataLink: boolean;
	activeSamplesLink: boolean;
	activeReportsLink: boolean;
	providerNumberOfDataSources: any[];
	providerInvestorClientBucket: any[];
	providerOrganizationType: any[];
	providerManagerType: any[];
	providerStrategies: any[];
	providerResearchStyles: any[];
	providerInvestingTimeFrame: any[];
	providerEquitiesMarketCap: any[];
	providerEquitiesStyle: any[];
	providerIdentifiersAvailable: any[];
	providerAccessOffered: any[];
	providerDataProductTypes: any[];
	providerDataTypes: any[];
	providerDataLanguagesAvailable: any[];
	providerDataLicenseType: any[];
	providerPaymentMethodsOffered: any[];
	providerDataGapsReasons: any[];
	providerOutlierReasons: any[];
	providerBiases: any[];
	providerDatasetSize: any[];
	providerSecRegulated: any[];
	providerDatasetNumberOfRows: any[];
	providerCoverageRange: any[];
	providerTrialPricingModel: any[];
	providerPartnershipModel: any[];
	providerPricingModel: any[];
	providerTrialAgreementType: any[];
	providerSubscriptionModel: any[];
	providerSubscriptionTimePeriod: any[];
	providerMonthlyPriceRange: any[];
	providerPointInTimeAccuracy: any[];
	providerDataGaps: any[];
	providerDuplicatesCleaned: any[];
	providerIncludesOutliers: any[];
	providerNormalized: any[];
	providerPartners: any[];
	providerDistributionPartner: any[];
	providerConsumerPartner: any[];
	providerComplementaryPartner: any[];
    providerCompetitors: any[];
    providerPartnersCount: number = 0;
    distributionPartnersCount: number = 0;
    consumerPartnersCount: number = 0;
    complementaryProvidersCount: number = 0;
	competitorsCount: number = 0;
	selectedValue: any[] = [];

    completeness: any = 0;
    totalFieldsFilled: any = 0;
	progressBarColor = 'primary';
	progressBarMode = 'determinate';
	mdTooltipDelay: number;

	dataProviderTypes: any[];
	mainDataIndustriesArr: any[];
	dataIndustries: any[];
	mainDataCategories: any[];
	dataCategories: any[];
	dataFeatures: any[];
	dataSources: any[];
	geographicalFocus: any[];
	providerTags: any[];
	providerWillingness: any[];
	providerApproximateHeadcount: any[];
	providerRelevantInvestors: any[];
	providerMainAssetClass: any[];
	providerRelevantAssetClass: any[] = [];
	relevantSecurityTypes: any[] = [];
	relevantSectors: any[] = [];
	deliveryMethods: any[] = [];
	deliveryFormats: any[] = [];
	deliveryFrequencies: any[];
	dataUpdateFrequency: any[];
	pricingModel: any[];
	panelName: string;
	overAll: string;
	progressSpinner:boolean;
	privacySecurityPanel: boolean;
	basicExpansionPanel: boolean;
	documentExpansionPanel: boolean;
	companyExpansionPanel: boolean;
	descriptionsExpansionPanel: boolean;
	categorizationsExpansionPanel: boolean;
	investigationExpansionPanel: boolean;
	relatedProvidersExpansionPanel: boolean;
	dataDetailsExpansionPanel: boolean;
	dataDeliveryExpansionPanel: boolean;
	triallingExpansionPanel: boolean;
	licensingExpansionPanel: boolean;
	pricingExpansionPanel: boolean;
	dataQualityExpansionPanel: boolean;
	discussionExpansionPanel: boolean;
    navigateNextPage: boolean = false;
    unsavedEdits: boolean = false;
    dataDocument: any;
	privacySecurityForm = new FormGroup({});
	privacySecurityOptions: FormlyFormOptions = {};

	basicForm = new FormGroup({});
	basicModel: any = {};
	providerFormModel: any = {};
	basicOptions: FormlyFormOptions = {};

	marketingForm = new FormGroup({});
	documentForm = new FormGroup({});
	samplesForm = new FormGroup({});
	reportForm = new FormGroup({});
	documentModel: any = {};
	documentMarketting: any = {};
	documentData: any = {};
	documentSample: any = {};
	documentReport: any = {};
	documentOptions: FormlyFormOptions = {};

    companyDetailsForm = new FormGroup({});
    companyDetailsModel: any = {};
    companyDetailsOptions: FormlyFormOptions = {};

    descriptionsForm = new FormGroup({});
    descriptionsModel: any = {};
    descriptionsOptions: FormlyFormOptions = {};

	categorizationsForm = new FormGroup({});
	categorizationsModel: any = {};
	categorizationsOptions: FormlyFormOptions = {};

    investigationForm = new FormGroup({});
    investigationOptions: FormlyFormOptions = {};

    relatedProvidersForm = new FormGroup({});
    relatedProvidersOptions: FormlyFormOptions = {};

    dataDetailsForm = new FormGroup({});
    dataDetailsOptions: FormlyFormOptions = {};

    dataDeliveryForm = new FormGroup({});
    dataDeliveryOptions: FormlyFormOptions = {};

	triallingForm = new FormGroup({});
	triallingOptions: FormlyFormOptions = {};

	licensingForm = new FormGroup({});
	licensingOptions: FormlyFormOptions = {};

	pricingForm = new FormGroup({});
	pricingOptions: FormlyFormOptions = {};

	dataQualityForm = new FormGroup({});
	dataQualityOptions: FormlyFormOptions = {};

	discussionForm = new FormGroup({});
	discussionOptions: FormlyFormOptions = {};

	privacySecurityFields: FormlyFieldConfig[] = [
		{
			key: 'visibility',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Visibility:',
				description: 'Who would you like to be able to see your data profile and the resources you upload?',
				options: [{
					value: 'Anyone (Public)',
					label: 'Anyone (Public)'
				}, {
					value: 'Verified Users Only',
					label: 'Verified Users Only'
				}, {
					value: 'Verified Investment Managers Only',
					label: 'Verified Investment Managers Only'
				}, {
					value: 'People that I Directly Give Access to Only',
					label: 'People that I Directly Give Access to Only'
				}, {
					value: 'Just Me (Private)',
					label: 'Just Me (Private)'
				}],
				click: (event) => {
					this.googleAnalyticsService.emitEvent('Provider Profile Form - Visibility', 'Changed', 'Provider Profile Form - Dropdown - User Permissions');
				}
			},
		},
		{
			key: 'partnership',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Partnership with Amass:',
				placeholder: '',
				description: 'Would you like to partner with Amass to enhance your ability to sell your data products?',
				options:this.providerPartnershipModel,
				attributes:{
					'placeholderText1' : 'In brief:',
					'placeholderText2' : 'For referrals, Amass takes 15% of the revenue and takes a hands-off approach following an intro to a qualified lead, as laid out in the Referral Agreement.',
					'placeholderText3' : 'For reselling, Amass takes 30% of the revenue and takes the lead on every step of the sales process, as laid out in the Reseller Agreement.'
                },
                change: this.inputChanges
			},
		},
	];

	basicFields: FormlyFieldConfig[] = [

		/* {
			key: 'emails',
			type: 'input',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Emails I\'d Like to Share My Profile With:',
				placeholder: '',
				description: '',
				required: true,
				pattern: '',
				attributes: {
					'placeholderText' : '',
					'labelTooltip': 'Email addresses you\'d like to share the information that you submit in the form below.',
                    'descriptionTooltip': '',
                    'placeholderTooltip': ''
				},
				change: this.basicInput
			},
		}, */
		{
			key: 'providerName',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Provider Name:',
				placeholder: '',
				description: 'What is the official name of your brand which acts as a data provider? This is often the name of your company.',
				required: true,
				attributes: {
					'placeholderText' : 'Please use correct spelling and casing.',
					'labelTooltip': 'The name of the brand or division of your organization that collects/provides/analyzes data and/or research. It is not always the same name as your organization (but it usually is).',
                    'descriptionTooltip': 'The name of the brand or division of your organization that collects/provides/analyzes data and/or research. It is not always the same name as your organization (but it usually is).',
                    'placeholderTooltip': 'The name of the brand or division of your organization that collects/provides/analyzes data and/or research. It is not always the same name as your organization (but it usually is).',
                    'trackNames': 'Provider Name',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
                keypress: (event) => {
				},
				change: this.basicInput
			},
		},
		{
			key: 'website',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Home Website:',
				type: 'input',
				placeholder: '',
				description: 'What is the link to the home webpage of your brand that acts as a data provider?',
				required: true,
				pattern: /^(?:https?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/,
				attributes: {
					'placeholderText' : 'The URL of the website of your data provider business in form of "www.domain.com"',
					'labelTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                    'descriptionTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                    'placeholderTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                    'trackNames': 'Home Website',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
                keypress: (event) => {
				},
				change: this.basicInput
            },
            validation: {
                messages: {
                    required: 'Website is required'
                }
            }
		},
		{
			key: '',
			type: 'fileUpload',
			className: 'customWidth',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Provider Logo:',
				attributes: {
					'logoType': 'Logo',
                    'accept': 'image/*',
					'trackNames': 'Provider Logo',
                    'trackCategory': 'Provider Profile Form - File',
                    'trackLabel': 'Provider Profile Form - File - Provider Logo'
				},
				change: ( event : any) => {
					if(event && event[0].name) {
						this.basicInput(event);
					}
					this.dataProviderService.onUpload(this.recordID, event[0]).subscribe(data=>{
					});
				}
			}
		}
	];

	documentMarkettingLiterature: FormlyFieldConfig[] = [
		{
			key: '',
			type: 'fileUpload',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Marketing Literature:',
				description: 'Overviews of your company, data provider brand and data products, including marketing decks/presentations, data profile/briefs/one-pagers, pre-filled questionnaires, and your About/Products websites.',
                attributes: {
					'fileType':'File',
					'placeholderText' : '',
					'labelTooltip': 'Upload file attachments and/or submit the URLs of websites.',
                    'descriptionTooltip': 'Upload file attachments and/or submit the URLs of websites.',
				},
				change: ( event : any) => {
					if(event && event[0].name) {
						this.basicInput(event);
					}
					this.dataProviderService.onResourceFileUpload(this.recordID, event[0]).subscribe(data=> {
						const marketingFileData = data;
						this.resourceObj = {};
						this.resourceObj['file'] = {id:data.id};
						this.resourceObj['name'] = data.fileName;
						this.resourceObj['purposes'] = [{id: 70}];
						this.resourceObj['relavantDataProviders'] = [{id: this.dataProviderID}];
						setTimeout(() => {
							this.dataResourceService.addResourceAdmin(this.resourceObj).subscribe(data => {
								const  dataObj = data;
								const createdResource = {};
								createdResource['name'] = dataObj.name;
								createdResource['id'] = dataObj.id;
								createdResource['fileType'] = marketingFileData.fileID;
								createdResource['resourceID'] = dataObj.id;
								this.marketingLiteratureList.push(createdResource);
							});
						}, 500);
					});
				}
			},
		},
		{
			key: 'link',
			type: 'input',
			className: 'customInputDoc',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: '',
				description: '',
                attributes: {
					'placeholders' : 'https://example.com/about',
					'labelTooltip': '',
					'descriptionTooltip': '',
				},
				keypress: (event) => {
					if(event) {
						this.hideMarketingSection = false;
					}
				}
			}
		},
		{
			key: '',
			type: 'button',
			className: 'addCustomButton',
			templateOptions: {
				label: '+ Add Link',
				attributes: {
				   'btnType' : 'button'
				},
				click: (event: any) => {
					this.googleAnalyticsService.emitEvent('Provider Profile Form - Resource', 'Added', 'Provider Profile Form - Marketing Literature - Button - Add Link');
					if(event) {
						this.dataProviderService.addLinkAdmin(this.documentMarketting).subscribe(data => {
							const resourceObj = {};
							resourceObj['link'] = {id:data.id};
							resourceObj['name'] = data.link;
							resourceObj['purposes'] = [{id: 70}];
							resourceObj['relavantDataProviders'] = [{id: this.dataProviderID}];

						if(resourceObj) {
								this.dataResourceService.addResourceAdmin(resourceObj).subscribe(data => {
									const  dataObj = data;
									const createdResource = {};
									createdResource['name'] = dataObj.name;
									createdResource['id'] = dataObj.id;
									createdResource['linkType'] = dataObj.name;
									createdResource['resourceID'] = dataObj.id;
									this.marketingLiteratureList.push(createdResource);
								});
							}
						});
	                }
                    this.marketingForm.get('link').setValue('');
				},
			},
			hideExpression: ((model: any, formState: any) => this.hideMarketingSection)
		},
		/* {
			type: 'button',
			className: 'customButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: '',
				text: '+ Add Link',
        		btnType: 'info',
				description: '',
                attributes: {
					'placeholderText' : '',
					'labelTooltip': '',
                    'descriptionTooltip': '',
                }
			},
		} */
	]

	documentDataDictionary: FormlyFieldConfig[] = [
		{
			key: '',
			type: 'fileUpload',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Documentation & Data Dictionary:',
				description: 'Detailed documentation on your data products and datasets, including data dictionaries, API documentation, and coverage lists (companies/tickers).',
                attributes: {
					'fileType':'File',
					'placeholderText' : '',
					'labelTooltip': 'Upload file attachments and/or submit the URLs of websites.',
                    'descriptionTooltip': 'Upload file attachments and/or submit the URLs of websites.',
                },
                change: ( event : any) => {
					if(event && event[0].name) {
						this.basicInput(event);
					}
					this.dataProviderService.onResourceFileUpload(this.recordID, event[0]).subscribe(data=> {
						const documentFileData = data;
						this.resourceObj = {};
						this.resourceObj['file'] = {id:data.id};
						this.resourceObj['name'] = data.fileName;
						this.resourceObj['purposes'] = [{id: 72}];
						this.resourceObj['relavantDataProviders'] = [{id: this.dataProviderID}];
						setTimeout(() => {
							this.dataResourceService.addResourceAdmin(this.resourceObj).subscribe(data => {
								const  dataObj = data;
								const createdResource = {};
								createdResource['name'] = dataObj.name;
								createdResource['id'] = dataObj.id;
								createdResource['fileType'] = documentFileData.fileID;
								createdResource['resourceID'] = dataObj.id;
								this.dataDictionaryList.push(createdResource);
							});
						}, 500);
					});
				}
			},
		},
		{
			key: 'link',
			type: 'input',
			className: 'customInputDoc',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: '',
				description: '',
                attributes: {
					'placeholders' : ' https://example.com/documentation',
					'labelTooltip': '',
                    'descriptionTooltip': '',
                },
				change: this.inputChanges,
				keypress:(event) => {
					if(event) {
						this.hideDocumentSection = false;
					}

				}
			},
		},
		{
			key: '',
			type: 'button',
			className: 'addCustomButton',
			templateOptions: {
				label: '+ Add Link',
				attributes: {
				   'btnType' : 'button'
				},
				click: (event: any) => {
					this.googleAnalyticsService.emitEvent('Provider Profile Form - Resource', 'Added', 'Provider Profile Form - Documentation & Data Dictionary - Button - Add Link');
					if(event) {
						this.dataProviderService.addLinkAdmin(this.documentData).subscribe(data => {
							const resourceObj = {};
							resourceObj['link'] = {id:data.id};
							resourceObj['name'] = data.link;
						    resourceObj['purposes'] = [{id: 72}];
							resourceObj['relavantDataProviders'] = [{id: this.dataProviderID}];

							if(resourceObj) {
								this.dataResourceService.addResourceAdmin(resourceObj).subscribe(data => {
									const  dataObj = data;
									const createdResource = {};
									createdResource['name'] = dataObj.name;
									createdResource['id'] = dataObj.id;
									createdResource['linkType'] = dataObj.name;
									createdResource['resourceID'] = dataObj.id;
									this.dataDictionaryList.push(createdResource);
							    });
							}
						});
					}
					this.documentForm.get('link').setValue('');
				},
			},
			hideExpression: ((model: any, formState: any) => this.hideDocumentSection)
		},
		/* {
			type: 'button',
			className: 'customButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: '',
				text: '+ Add Link',
        		btnType: 'info',
				description: '',
                attributes: {
					'placeholderText' : '',
					'labelTooltip': '',
                    'descriptionTooltip': '',
                }
			},
		} */
	]

	documentSamples: FormlyFieldConfig[] = [
		{
			key: '',
			type: 'fileUpload',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Samples & Examples:',
				description: 'Examples of your data products and datasets, including data snapshots/samples, product screenshots, demo videos/presentations, sample reports, and trial websites.',
                attributes: {
					'fileType':'File',
					'placeholderText' : '',
					'labelTooltip': 'Upload file attachments and/or submit the URLs of websites.',
                    'descriptionTooltip': 'Upload file attachments and/or submit the URLs of websites.',
                },
                change: ( event : any) => {
					if(event && event[0].name) {
						this.basicInput(event);
					}
					this.dataProviderService.onResourceFileUpload(this.recordID, event[0]).subscribe(data=> {
						const samplesFileData = data;
						this.resourceObj = {};
						this.resourceObj['file'] = {id:data.id};
						this.resourceObj['name'] = data.fileName;
						this.resourceObj['purposes'] = [{id: 93}];
						this.resourceObj['relavantDataProviders'] = [{id: this.dataProviderID}];
						setTimeout(() => {
							this.dataResourceService.addResourceAdmin(this.resourceObj).subscribe(data => {
								const  dataObj = data;
								const createdResource = {};
								createdResource['name'] = dataObj.name;
								createdResource['id'] = dataObj.id;
								createdResource['fileType'] = samplesFileData.fileID;
								createdResource['resourceID'] = dataObj.id;
								this.samplesAndExamplesList.push(createdResource);
							});
						}, 500);
					});
				}
			},
		},
		{
			key: 'link',
			type: 'input',
			className: 'customInputDoc',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: '',
				description: '',
                attributes: {
					'placeholders' : ' https://example.com/data-sample',
					'labelTooltip': '',
                    'descriptionTooltip': '',
                },
				change: this.inputChanges,
				keypress:(event) => {
					if(event) {
						this.hideSamplesSection = false;
					}

				}
			},
		},
		{
			key: '',
			type: 'button',
			className: 'addCustomButton',
			templateOptions: {
				label: '+ Add Link',
				attributes: {
				   'btnType' : 'button'
				},
				click: (event: any) => {
					this.googleAnalyticsService.emitEvent('Provider Profile Form - Resource', 'Added', 'Provider Profile Form - Samples & Examples - Button - Add Link');
					if(event) {
						this.dataProviderService.addLinkAdmin(this.documentSample).subscribe(data => {
						const resourceObj = {};
						resourceObj['link'] = {id:data.id};
						resourceObj['name'] = data.link;
						resourceObj['purposes'] = [{id: 93}];
					    resourceObj['relavantDataProviders'] = [{id: this.dataProviderID}];
                    if(resourceObj){
						this.dataResourceService.addResourceAdmin(resourceObj).subscribe(data => {
			const  dataObj = data;
					const createdResource = {};
					createdResource['name'] = dataObj.name;
					createdResource['id'] = dataObj.id;
					createdResource['linkType'] = dataObj.name;
					createdResource['resourceID'] = dataObj.id;
					this.samplesAndExamplesList.push(createdResource);
							});
						}
					});
				}
					this.samplesForm.get('link').setValue('');
				},
			},
			hideExpression: ((model: any, formState: any) => this.hideSamplesSection)
		},
		/* {
			type: 'button',
			className: 'customButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: '',
				text: '+ Add Link',
        		btnType: 'info',
				description: '',
                attributes: {
					'placeholderText' : '',
					'labelTooltip': '',
                    'descriptionTooltip': '',
                }
			},
		} */
	]

	documentReportsAnalyses: FormlyFieldConfig[] = [
		{
			key: '',
			type: 'fileUpload',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Reports & Analyses:',
				description: 'Demonstrations of the value or utility of your data products and datasets, including backtests, case studies, white papers, research reports & articles, and analytics dashboards.',
                attributes: {
					'fileType':'File',
					'placeholderText' : '',
					'labelTooltip': 'Upload file attachments and/or submit the URLs of websites.',
                    'descriptionTooltip': 'Upload file attachments and/or submit the URLs of websites.',
                },
                change: ( event : any) => {
					if(event && event[0].name) {
						this.basicInput(event);
					}
					this.dataProviderService.onResourceFileUpload(this.recordID, event[0]).subscribe(data=> {
						const reportFileData = data;
						this.resourceObj = {};
						this.resourceObj['file'] = {id:data.id};
						this.resourceObj['name'] = data.fileName;
						this.resourceObj['purposes'] = [{id: 91}];
						this.resourceObj['relavantDataProviders'] = [{id: this.dataProviderID}];
						setTimeout(() => {
							this.dataResourceService.addResourceAdmin(this.resourceObj).subscribe(data => {
								const  dataObj = data;
								const createdResource = {};
								createdResource['name'] = dataObj.name;
								createdResource['id'] = dataObj.id;
								createdResource['fileType'] = reportFileData.fileID;
								createdResource['resourceID']=dataObj.id;
								this.reportsAnalysesList.push(createdResource);
							});
						}, 500);
					});
				}
			},
		},
		{
			key: 'link',
			type: 'input',
			className: 'customInputDoc',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				 label: '',
				 description: '',
                attributes: {
					'placeholders' : ' https://example.com/report',
					'labelTooltip': '',
                     'descriptionTooltip': '',
                },
				change: this.inputChanges,
				keypress:(event) => {
					if(event) {
						this.hideReportSection = false;
					}

				}
			},
		},
		{
			key: '',
			type: 'button',
			className: 'addCustomButton',
			templateOptions: {
				label: '+ Add Link',
				attributes: {
				   'btnType' : 'button'
				},
				click: (event: any) => {
					this.googleAnalyticsService.emitEvent('Provider Profile Form - Resource', 'Added', 'Provider Profile Form - Reports & Analyses - Button - Add Link');
					if(event) {
						this.dataProviderService.addLinkAdmin(this.documentReport).subscribe(data => {
							const resourceObj = {};
							resourceObj['link'] = {id:data.id};
							resourceObj['name'] = data.link;
							resourceObj['purposes'] = [{id: 91}];
							resourceObj['relavantDataProviders'] = [{id: this.dataProviderID}];
						  if(resourceObj)
						  {
							this.dataResourceService.addResourceAdmin(resourceObj).subscribe(data => {
								const  dataObj = data;
								const createdResource = {};
								createdResource['name'] = dataObj.name;
								createdResource['id'] = dataObj.id;
								createdResource['linkType'] = dataObj.name;
								createdResource['resourceID'] = dataObj.id;
								this.reportsAnalysesList.push(createdResource);
							});
						}
					});
				}
					this.reportForm.get('link').setValue('');
				},
			},
			hideExpression: ((model: any, formState: any) => this.hideReportSection)
		},
		/* {
			type: 'button',
			className: 'customButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: '',
				text: '+ Add Link',
        		btnType: 'info',
				description: '',
                attributes: {
					'placeholderText' : '',
					'labelTooltip': '',
                    'descriptionTooltip': '',
                }
			},
		} */
	]

	companyDetailsFields: FormlyFieldConfig[] = [
		// {
		// 	key: 'ownerOrganization.name',
		// 	type: 'input',
		// 	wrappers: ['form-field-horizontal'],
		// 	templateOptions: {
		// 		label: 'Owner Organization:',
        //         type: 'input',
        //         required: true,
		// 		description: 'What organization (company) owns your brand that acts a data provider?',
        //         attributes: {
		// 			'placeholderText' : 'This is often the same as your Provider Name.',
        //         },
        //         change: this.inputChanges
		// 	},
		// },
		{
			key: 'ownerOrganization.city',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'City:',
				type: 'input',
                description: 'In what city is your organization\'s headquarters located?',
                attributes: {
                    'trackNames': 'City',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
				maxLength : 50,
				change: this.basicInput,
				keypress: (event) => {
				}
			},
		},
		{
			key: 'ownerOrganization.state',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'State:',
				type: 'input',
                description: 'In what state is your organization\'s headquarters located?',
                attributes: {
                    'trackNames': 'State',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
				maxLength : 50,
				change: this.basicInput,
				keypress: (event) => {
				}
			},
		},
		{
			key: 'ownerOrganization.country',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Country:',
				type: 'input',
                description: 'In what country is your organizations headquarters located?',
                attributes: {
                    'trackNames': 'Country',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
				maxLength : 50,
				change: this.basicInput,
				keypress: (event) => {
				}
			},
		},
		{
			key: 'ownerOrganization.zipCode',
			type: 'mask',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Zip Code:',
				description: 'In what zip code is your organization\'s headquarters located?',
				attributes: {
                    'mask': '99999',
                    'trackNames': 'Zip Code',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
				maxLength : 50,
                change: this.basicInput
			},
		},
		{
			key: 'ownerOrganization.yearFounded',
			type: 'mask',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Year Founded:',
                description: 'What year was your organization founded in?',
                attributes: {
                    'mask': '9999',
                    'trackNames': 'Year Founded',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
                },
				change: (event: Field, value: number) => {
                    const currentDate = new Date();
                    if (value && (value < 1700 || value > currentDate.getFullYear())) {
                        this.openSnackBar('Year must be between 1700 and ' + currentDate.getFullYear() + '!', 'Close');
                    }
                    this.basicInput(event);
                }
            }
		},
		{
			key: 'ownerOrganization.yearExit',
			type: 'mask',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Year Exited:',
				description: 'What year did your company undergo an ownership change, whether through acquisition or public offering? Leave blank if this is not applicable.',
				attributes: {
                    'mask': '9999'
                },
                change: (event: Field, value: number) => {
                    const currentDate = new Date();
                    if (value && (value < 1700 || value > currentDate.getFullYear())) {
                        this.openSnackBar('Year must be between 1700 and ' + currentDate.getFullYear() + '!', 'Close');
                    }
                    this.basicInput(event);
                }
			},
		},
		{
			key: 'yearFirstDataProductLaunched',
			type: 'mask',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Year First Data Product Launched:',
                description: 'In what year did you launch your first data product to external clients?',
                attributes: {
                    'mask': '9999'
                    // 'trackNames': 'Year First Data Product Launched',
                    // 'trackCategory': 'Provider Profile Form - Textbox',
                    // 'trackLabel': 'Provider Profile Form - Input'
				},
				change: (event: Field, value: number) => {
                    const currentDate = new Date();
                    if (value && (value < 1700 || value > currentDate.getFullYear())) {
                        this.openSnackBar('Year must be between 1700 and ' + currentDate.getFullYear() + '!', 'Close');
                    }
                    this.basicInput(event);
                }			},
		},
		{
			key: 'ownerOrganization.headcountBucket',
			type: 'dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Approximate Headcount:',
				description: 'What is the approximate range of the number of employees your organization employs?',
				options: this.providerApproximateHeadcount,
				attributes: {
					field: 'label'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'ownerOrganization.headcountNumber',
			type: 'input',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Headcount:',
				type: 'number',
				description: 'Precisely, how many employees does your organization employ?',
				change: this.basicInput,
				maxLength : 25,
				keypress: (event) => {
				}
			},

		},
		{
			key: 'ownerOrganization.headCount',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Headcount Details:',
                description: 'Please provide any more details regarding the number of employees your organization employs.',
                attributes: {
                    'trackNames': 'Headcount Details',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
				maxLength : 50,
				change: this.basicInput,
				keypress: (event) => {
				}
			},
		},
		{
			key: 'numberOfAnalystEmployees',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Number of Analyst Employees:',
				type: 'number',
                description: 'How many employees do you have focused on improving the value of your data (such as collecting, cleaning, or analyzing it)?',
                attributes: {
                    'trackNames': 'Number of Analyst Employees',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
				maxLength : 19,
				change: this.basicInput,
				keypress: (event) => {
				}
			},
		},
		{
			key: 'ownerOrganization.financialPosition',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Financial Position:',
				description: 'Please describe the financial situation of your company, such as the total amount of funding, yearly revenue, and/or valuation.',
				change: this.basicInput,
                attributes: {
                    'maximumLength' : 1024
                },
				keypress: (event) => {
				}
			},
		},
	];

	descriptionsFields: FormlyFieldConfig[] = [
		{
			key: 'summary',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Summary:',
				description: 'Please provide a brief summary (approximately one paragraph) of your data offerings.',
				change: this.basicInput,
				attributes:{
					'placeholderText': 'If possible, please describe your offerings in relation to the institutional investment and alternative data space.',
                    'maximumLength' : 1024
				},
			},
		},
		{
			key: 'shortDescription',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Short Description:',
				minRows: 2,
				maxRows: 100,
				required: true,
				placeholder: '',
                description: 'Please provide a very brief (one sentence or less) summary of your data offerings.',
				attributes:{
                    'placeholderText' : 'This should be a brief (1 sentence or less) summary of the specific data/analytics/products that you offer which is relevant to investment research or analysis. This is not just a general description of the company and everything that they do.',
                    'trackNames': 'Short Description',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 400
				},
                change: this.basicInput,
			},
		},
		{
			key: 'longDescription',
			type: 'inputEditor',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Long Description:',
				placeholder: '',
                description: 'Please provide a comprehensive description (approximately one to three paragraphs) of your company\'s offerings and wider business model.',

				attributes: {
                    'placeholderText' : 'This should be a more general description of the your business and operations. But it should be also specific enough in nature to have a pretty thorough understanding of what your business does.',
                    'trackNames': 'Long Description',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
                },
                change: this.basicInput
			},
		},
	];

	categorizationsFields: FormlyFieldConfig[];

	investigationFields: FormlyFieldConfig[] = [
		{
			key: 'scoreInvestorsWillingness.id',
			type: 'dropdown',
			className: 'customWidth-dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Willingness to Provide to Asset Managers:',
				description: 'How willing are you to sell your data or analytics products to the institutional investment industry (e.g. hedge funds)?',
				required: true,
				options: this.providerWillingness,
				attributes: {
					field: 'label'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'secRegulated.id',
			type: 'dropdown',
			className: 'customSmallWidth-dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'SEC-Regulated:',
				description: 'Are you registered with the SEC?',
				options: this.providerSecRegulated,
				attributes: {
					field: 'label'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'investorClientBucket',
			type: 'dropdown',
			className: 'customSmallWidth-dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Approximate Number of Investor Clients:',
				description: 'Approximately, how many clients in the asset management industry do you have?',
				options: this.providerInvestorClientBucket,
				attributes: {
					field: 'label'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'investorClientsCount',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Number of Investor Clients:',
				type: 'number',
				placeholder: '',
                description: 'How many clients in the asset management industry do you have?',
				attributes: {
                    'placeholderText' : 'The precise number of clients you have in the investment management industry.',
                    'trackNames': 'Number of Investor Clients',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
				maxLength : 19,
                change: this.basicInput,
				keypress: (event) => {
				}
			},
		},
		{
			key: 'investor_clients',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Number of Investor Clients Details:',
                description: 'Please provide any more details related to the approximate number of clients you have in the investment management industry.',
                attributes: {
                    'trackNames': 'Number of Investor Clients Details',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 512
				},
				change: this.basicInput
			},
		},
		{
			key: 'ownerOrganization.clientBase',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Client Base:',
				description: 'Please describe your organization\'s client base (both within investment management and otherwise), including the number of clients and their common characteristics.',
				change: this.basicInput,
                attributes: {
                    'maximumLength' : 128
                }
			},
		},
		{
			key: 'organizationType',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Target Organization Type:',
				placeholder: '',
				description: 'What types of companies/organizations are you focusing on selling your data offerings to?',
				options: this.providerOrganizationType,
				attributes: {
                    'placeholderText' : 'If applicable, use your past experience with clients to fill this in.',
                    'lookupmodule':'ORGANIZATION_TAG'
                },
                change: this.basicInput
			},
		},
		{
			key: 'managerType',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Target Investment Manager Type:',
				placeholder: '',
				description: 'What types of investors are your offerings most useful to?',
				options: this.providerManagerType,
				attributes:{
                    'placeholderText' : 'If applicable, use your past experience with clients to fill this in.',
                    'lookupmodule':'INVESTMENT_MANAGER_TYPE'
				},
				change: this.basicInput
			},
		},
		{
			key: 'investorTypes',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Relevant Investor Type:',
				placeholder: '',
				description: 'What types of investors will your offerings be useful for?',
				options: this.providerRelevantInvestors,
				attributes:{
                    'placeholderText' : 'The types of investors your data would be useful for or would have the ability to use. If applicable, use your past experience with clients to fill this in.',
                    'lookupmodule':'RLVNT_INVESTOR_TYPE'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'strategies',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Target Investment Managers\' Strategies:',
				placeholder: '',
				description: 'Which investor strategies are your offerings most applicable to or useful for?',
				options: this.providerStrategies,
				change: this.basicInput,
				attributes: {
                    'placeholderText' : 'If applicable, use your past experience with clients to fill this in.',
                    'lookupmodule':'INVESTMENT_MANAGER_STRATEGY'
				}
			},
		},
		{
			key: 'researchStyles',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Target Investment Managers\' Research Styles:',
				placeholder: '',
				description: 'Which investor research styles are your offerings most applicable or useful to?',
				options: this.providerResearchStyles,
				change: this.basicInput,
				attributes: {
                    'placeholderText' : 'If applicable, use your past experience with clients to fill this in.',
                    'lookupmodule':'INVESTMENT_MANAGER_RESEARCH_STYLE'
				}
			},
		},
		{
			key: 'investingTimeFrame',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Target Investment Managers\' Investing Time Frame:',
				placeholder: '',
				description: 'Which investing time frames are your offerings most applicable to or useful for?',
				options: this.providerInvestingTimeFrame,
				change: this.basicInput,
				attributes: {
                    'placeholderText' : 'If applicable, use your past experience with clients to fill this in.',
                    'lookupmodule':'INVESTING_TIME_FRAME'
                }
			},
		},
		{
			key: 'equitiesMarketCap',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Target Investment Managers\' Public Equities Market Cap Focus:',
				placeholder: '',
				description: 'What size of public companies are your offerings most applicable to or useful for researching/understanding',
				options: this.providerEquitiesMarketCap,
				change: this.basicInput,
				attributes:{
                    'placeholderText' : 'If applicable, use your past experience with clients to fill this in.',
                    'lookupmodule':'EQUITY_MARKET_CAP'
				}
			},
		},
		{
			key: 'equitiesStyle',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Target Investment Managers\' Public Equities Style:',
				placeholder: '',
				description: 'What type of public companies are your offerings most applicable to or useful for researching/understanding',
				options: this.providerEquitiesStyle,
				change: this.basicInput,
				attributes: {
                    'placeholderText' : 'If applicable, use your past experience with clients to fill this in.',
                    'lookupmodule':'EQUITY_STYLE'
				}
			},
		},
		{
			key: 'mainAssetClass',
			type: 'dropdown',
			className: 'customWidth-dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Main Asset Class:',
				placeholder: '',
				description: 'What security asset class is your data offerings the most useful to research?',
				options: this.providerMainAssetClass,
				attributes: {
					field: 'label',
					'placeholderText' : 'Sometimes you will belong in more than one asset class - please choose the primary asset class, and add all other asset classes in the following question.'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'assetClasses',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Relevant Asset Classes:',
				placeholder: '',
				description: 'What are all the security asset classes your data offerings can be used to research?',
				options: this.providerRelevantAssetClass,
				attributes: {
                    'placeholderText' : 'List all the security asset classes your data focuses on.',
                    'lookupmodule':'ASSET_CLASS'
				}
			},
		},
		{
			key: 'securityTypes',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Relevant Security Types:',
				placeholder: '',
				description: 'What are all the security types your data offerings could be used to research?',
				options: this.relevantSecurityTypes,
				attributes: {
                    'placeholderText' : 'List of all the financial security types your data can be used for or has any relevance towards.',
                    'lookupmodule':'RLVNT_SECURITY_TYPE'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'relevantSectors',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Relevant Sectors:',
				placeholder: '',
				description: 'What are all the sectors your data offerings could be used to research?',
				options: this.relevantSectors,
				attributes: {
                    'placeholderText' : 'List of all the sectors your data can be used for or has any relevance towards.',
                    'lookupmodule':'RLVNT_SECTOR'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'publicEquitiesCoveredRange',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Approximate Number of Public Equities Covered:',
				description: 'What is the range of the number of public equities your data offerings can be used to research?',
                options: this.providerCoverageRange,
                change: this.inputChanges
			},
		},
		{
			key: 'publicEquitiesCoveredCount',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Number of Public Equities Covered:',
				placeholder: '',
				type: 'number',
				description: 'How many public equities can your data offerings be used to research?',
				attributes: {
					'min': 0,
					'max': 10000,
                    'step': 1,
                    'trackNames': 'Number of Public Equities Covered',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
				maxLength : 10,
				change: this.basicInput,
				keypress: (event) => {
				}
			}
		},
		{
			key: 'publicCompaniesCovered',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Public Companies Covered Details:',
				minRows: 2,
				maxRows: 100,
				placeholder: '',
                description: 'Which public companies (tickers) can your data offerings be used to research?',
				attributes:{
                    'placeholderText' : 'List of all the public companies/tickers your data can be used for or are relevant towards.',
                    'trackNames': 'Public Companies Covered Details',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 256
				},
                change: this.basicInput
			},
		},
		{
			key: 'identifiersAvailable',
			type: 'multiselect',
			className: 'custom-multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Identifiers Available:',
				placeholder: '',
				description: 'What types of public company identifiers are available within your data offerings?',
				options: this.providerIdentifiersAvailable,
				attributes:{
                    'placeholderText' : 'If you have not standardized the public company identifiers in your dataset, please answer "None".',
                    'lookupmodule':'IDENTIFIERS'
                },
                change: this.inputChanges
			},
		},
	];

	relatedProvidersFields: FormlyFieldConfig[] = [
		//{
		// 	//key: 'providerPartners',
		// 	//type: 'textarea',
		// //	wrappers: ['form-field-horizontal'],
	    //  	//	wrappers: ['form-field-horizontal'],
		// 	templateOptions: {
		// 		label: 'Provider Data Partners',
		// 		description: 'Which external data providers (organizations) do you receive data from?',

        //        /*  description: 'Which external data providers (organizations) do you receive data from?',
		// 		options: this.providerPartners,
		// 		attributes:{
		// 			field : 'providerName',
		// 			'multiple': 'true',
		// 			'placeholderText' : 'Your external data suppliers.'
		// 		}, */
		// 		/* change: (event: any) => {
		// 			const queryObj = event;
		// 			this.providerPartners = [];

		// 			const selected = this.basicModel['providerPartners'];
		// 			console.log('Selected Providers', selected);
		// 			///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
		// 			if(selected && selected.length) {
		// 				this.selectedValue = selected.map(item => item.id);
		// 			}
        //             if(queryObj.query) {
		// 				this.filterService.getAllProviders(queryObj.query).subscribe(d => {
		// 				const listItems = d && d['hits'] ? d['hits']['hits'] : [];
		// 				for (let i = 0; i < listItems.length; ++i) {
		// 					const item = listItems[i]._source;
		// 					if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL' && !listItems[i]._source.locked) {
		// 						if (!this.selectedValue.includes(item.id)) {
		// 							this.providerPartners.push({
		// 								id: item.id,
		// 								providerName: item.providerName,
		// 								img: item.logo ? item.logo.logo : null
		// 							});
		// 						}
		// 					}
		// 				}
		// 					this.relatedProvidersFields[0].templateOptions.options = this.providerPartners;
		// 				});
		// 			}
        //             this.inputChanges(event);
		// 		},
		// 		blur: (event: any) => {
		// 			if (event && event.target && !event['selectedVal']) {
		// 				event.target.value = '';
		// 			}
		// 		}*/
	    //     }
		//},
		/* {
			key: '',
			type: 'button',
			wrappers: ['form-field-horizontal'],
			className: 'addCustomButton',
			templateOptions: {
				//label: 'Provider Data Partne',
				description: 'Which external data providers (organizations) do you receive data from?',
				attributes: {
					'placeholderText' : 'Your external data suppliers.',
				   'btnType' : 'button'
				},
				btnlabel: '+ Add New Data Provider',
				click: (event: any) => {
					if(event) {
						this.hideProviderInputField = false;
					}
				}
		    }
		},
		{
			key: 'providerDataPartner',
			type: 'input',
			className: 'customInputDoc',
			templateOptions: {

			},
			hideExpression: ((model: any, formState: any) => this.hideProviderInputField)
		}, */
		{
			key: 'providerDataPartner',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Provider Data Partners:',
                description: 'Which external data providers (organizations) do you receive data from?',
                attributes: {
					'placeholderText' : 'Your external data suppliers.',
				},
				maxLength : 45,
                change: this.basicInput,
				keypress: (event) => {
				}
			},
		},

		//{
		// 	key: 'distributionPartners',
		// 	type: 'button',
		// //	wrappers: ['form-field-horizontal'],
		// 	className: 'providerCustomButton',
		// 	templateOptions: {
		// 		label: 'Distribution Data Partners ('+this.distributionPartnersCount+'):',
        //       /*   description: 'Which organizations have you partnered with to distribute your data offerings?',
		// 		options: this.providerDistributionPartner,
		// 		attributes:{
		// 			field : 'providerName',
		// 			'multiple': 'true',
		// 			'placeholderText' : 'Other data providers you have partnered with in order to distribute your data.'
		// 		},
		// 		change: (event: any) => {
		// 			const queryObj = event;
		// 			this.providerDistributionPartner = [];

		// 			const selected = this.basicModel['distributionPartners'];
		// 			console.log('Selected Providers', selected);
		// 			///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
		// 			if(selected && selected.length) {
		// 				this.selectedValue = selected.map(item => item.id);
		// 			}
		// 			if(queryObj.query) {
		// 				this.filterService.getAllProviders(queryObj.query).subscribe(d => {
		// 				const listItems = d && d['hits'] ? d['hits']['hits'] : [];
		// 				for (let i = 0; i < listItems.length; ++i) {
		// 					const item = listItems[i]._source;
		// 					if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL' && !listItems[i]._source.locked) {
		// 						if (!this.selectedValue.includes(item.id)) {
		// 							this.providerDistributionPartner.push({
		// 								id: item.id,
		// 								providerName: item.providerName,
		// 								img: item.logo ? item.logo.logo : null
		// 							});
		// 						}
		// 					}
		// 				}
		// 					this.relatedProvidersFields[3].templateOptions.options = this.providerDistributionPartner;
		// 				});
		// 			}
        //             this.inputChanges(event);
		// 		},
		// 		blur: (event: any) => {
		// 			if (event && event.target && !event['selectedVal']) {
		// 				event.target.value = '';
		// 			}
		// 		} */
		// 	}
		//},
		/* {
			key: '',
			type: 'button',
			className: 'addCustomButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Distribution Data Partners',
				btnlabel: '+ Add New Data Provider',
				description: 'Which organizations have you partnered with to distribute your data offerings?',
				attributes: {
					'btnType' : 'button',
					'placeholderText' : 'Other data providers you have partnered with in order to distribute your data.'
				},
				click: (event: any) => {
					if(event) {
						this.hideDistributionInputField = false;
					}
				}
		    }
		},
		{
			key: 'distributionDataPartner',
			type: 'input',
			className: 'customInputDoc',
			templateOptions: {

			},
			hideExpression: ((model: any, formState: any) => this.hideDistributionInputField)
		}, */
		{
			key: 'distributionDataPartner',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Distribution Data Partners:',
                description: 'Which organizations have you partnered with to distribute your data offerings?',
                attributes: {
					'placeholderText' : 'Other data providers you have partnered with in order to distribute your data.',
				},
				maxLength : 45,
                change: this.basicInput,
				keypress: (event) => {
				}
			},
		},

	//	{
		// 	key: 'consumerPartners',
		// 	type: 'button',
		// 	className: 'providerCustomButton',
		// //	wrappers: ['form-field-horizontal'],
		// 	templateOptions: {
		// 		label: 'Consumer Data Partners ('+this.consumerPartnersCount+'):',
        //        /*  description: 'Which organizations do you supply your data offerings to, but do not have the rights to distribute? In other words, which organizations use your data internally?',
		// 		options: this.providerConsumerPartner,
		// 		attributes:{
		// 			field : 'providerName',
		// 			'multiple': 'true',
		// 			'placeholderText' : 'Other data providers you receive data from but do not have the rights to redistribute, and therefore, use their data internally.'
		// 		},
		// 		change: (event: any) => {
		// 			const queryObj = event;
		// 			this.providerConsumerPartner = [];

		// 			const selected = this.basicModel['consumerPartners'];
		// 			console.log('Selected Providers', selected);
		// 			///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
		// 			if(selected && selected.length) {
		// 				this.selectedValue = selected.map(item => item.id);
		// 			}
		// 			if(queryObj.query) {
		// 				this.filterService.getAllProviders(queryObj.query).subscribe(d => {
		// 				const listItems = d && d['hits'] ? d['hits']['hits'] : [];
		// 				for (let i = 0; i < listItems.length; ++i) {
		// 					const item = listItems[i]._source;
		// 					if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL' && !listItems[i]._source.locked) {
		// 						if (!this.selectedValue.includes(item.id)) {
		// 							this.providerConsumerPartner.push({
		// 								id: item.id,
		// 								providerName: item.providerName,
		// 								img: item.logo ? item.logo.logo : null
		// 							});
		// 					    }
		// 					}
		// 				}
		// 					this.relatedProvidersFields[6].templateOptions.options = this.providerConsumerPartner;
		// 				});
		// 			}
        //             this.inputChanges(event);
		// 		},
		// 		blur: (event: any) => {
		// 			if (event && event.target && !event['selectedVal']) {
		// 				event.target.value = '';
		// 			}
		// 		} */
		// 	}
	//	},
		/* {
			key: '',
			type: 'button',
			className: 'addCustomButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Consumer Data Partners',
				btnlabel: '+ Add New Data Provider',
				description: 'Which organizations do you supply your data offerings to, but do not have the rights to distribute? In other words, which organizations use your data internally?',
				attributes: {
					'btnType' : 'button',
					'placeholderText' : 'Other data providers you receive data from but do not have the rights to redistribute, and therefore, use their data internally.'
				},
				click: (event: any) => {
					if(event) {
						this.hideConsumerInputField = false;
					}
				}
		    }
		},
		{
			key: 'consumerDataPartner',
			type: 'input',
			className: 'customInputDoc',
			templateOptions: {

			},
			hideExpression: ((model: any, formState: any) => this.hideConsumerInputField)
		}, */
		{
			key: 'consumerDataPartner',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Consumer Data Partners:',
                description: 'Which organizations do you supply your data offerings to, but do not have the rights to distribute? In other words, which organizations use your data internally?',
                attributes: {
					'placeholderText' : 'Other data providers you receive data from but do not have the rights to redistribute, and therefore, use their data internally.',
				},
				maxLength : 45,
                change: this.basicInput,
				keypress: (event) => {
				}
			},
		},
		//{
			// key: 'complementaryProviders',
			// type: 'button',
			// className: 'providerCustomButton',
			// //wrappers: ['form-field-horizontal'],
			// templateOptions: {
			// 	label: 'Complementary Providers ('+this.complementaryProvidersCount+'):',
            //    /*  description: 'What other data providers do you feel are good complements to your data offerings?',
			// 	options: this.providerComplementaryPartner,
			// 	attributes:{
			// 		field : 'providerName',
			// 		'multiple': 'true',
			// 		'placeholderText' : ''
			// 	},
			// 	change: (event: any) => {
			// 		const queryObj = event;
			// 		this.providerComplementaryPartner = [];

			// 		const selected = this.basicModel['complementaryProviders'];
			// 		console.log('Selected Providers', selected);
			// 		///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
			// 		if(selected && selected.length) {
			// 			this.selectedValue = selected.map(item => item.id);
			// 		}
			// 		if(queryObj.query) {
			// 			this.filterService.getAllProviders(queryObj.query).subscribe(d => {
			// 			const listItems = d && d['hits'] ? d['hits']['hits'] : [];
			// 			for (let i = 0; i < listItems.length; ++i) {
			// 				const item = listItems[i]._source;
			// 				if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL' && !listItems[i]._source.locked) {
			// 					if (!this.selectedValue.includes(item.id)) {
			// 						this.providerComplementaryPartner.push({
			// 							id: item.id,
			// 							providerName: item.providerName,
			// 							img: item.logo ? item.logo.logo : null
			// 						});
			// 					}
			// 				}
			// 			}
			// 				this.relatedProvidersFields[9].templateOptions.options = this.providerComplementaryPartner;
			// 			});
			// 		}
            //         this.inputChanges(event);
			// 	},
			// 	blur: (event: any) => {
			// 		if (event && event.target && !event['selectedVal']) {
			// 			event.target.value = '';
			// 		}
			// 	} */
			// }
		//},
		/* {
			key: '',
			type: 'button',
			className: 'addCustomButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Complementary Providers',
				btnlabel: '+ Add New Data Provider',
				description: 'What other data providers do you feel are good complements to your data offerings?',
				attributes: {
					'btnType' : 'button',
					'placeholderText' : ''
				},
				click: (event: any) => {
					if(event) {
						this.hideComplementaryInputField = false;
					}
				}
		    }
		},
		{
			key: 'complementaryDataProviders',
			type: 'input',
			className: 'customInputDoc',
			templateOptions: {

			},
			hideExpression: ((model: any, formState: any) => this.hideComplementaryInputField)
		}, */
		{
			key: 'complementaryDataProviders',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Complementary Providers:',
                description: 'What other data providers do you feel are good complements to your data offerings?',
                attributes: {
					'placeholderText' : '',
				},
				maxLength : 45,
                change: this.basicInput,
				keypress: (event) => {
				}
			},
		},
		//{
			// key: 'competitors',
			// type: 'button',
			// className: 'providerCustomButton',
			// //wrappers: ['form-field-horizontal'],
			// templateOptions: {
			// 	label: 'Competitors ('+this.competitorsCount+'):',
            //    /*  description: 'Who are your three closest competitors?',
			// 	options: this.providerCompetitors,
			// 	attributes:{
			// 		field : 'providerName',
			// 		'multiple': 'true',
			// 		'placeholderText' : ''
			// 	},
			// 	change: (event: any) => {
			// 		const queryObj = event;
			// 		this.providerCompetitors = [];

			// 		const selected = this.basicModel['competitors'];
			// 		console.log('Selected Providers', selected);
			// 		///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
			// 		if(selected && selected.length) {
			// 			this.selectedValue = selected.map(item => item.id);
			// 		}
			// 		if(queryObj.query) {
			// 			this.filterService.getAllProviders(queryObj.query).subscribe(d => {
			// 			const listItems = d && d['hits'] ? d['hits']['hits'] : [];
			// 			for (let i = 0; i < listItems.length; ++i) {
			// 				const item = listItems[i]._source;
			// 				if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL' && !listItems[i]._source.locked) {
			// 					if (!this.selectedValue.includes(item.id)) {
			// 						this.providerCompetitors.push({
			// 							id: item.id,
			// 							providerName: item.providerName,
			// 							img: item.logo ? item.logo.logo : null
			// 						});
			// 					}
			// 				}
			// 			}
			// 				this.relatedProvidersFields[12].templateOptions.options = this.providerCompetitors;
			// 			});
			// 		}
            //         this.inputChanges(event);
			// 	},
			// 	blur: (event: any) => {
			// 		if (event && event.target && !event['selectedVal']) {
			// 			event.target.value = '';
			// 		}
			// 	} */
			// }
		//},
/* 		{
			key: '',
			type: 'button',
			className: 'addCustomButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Competitors ('+this.competitorsCount+'):',
				btnlabel: '+ Add New Data Provider',
				description: 'Who are your three closest competitors?',
				attributes: {
					'btnType' : 'button',
					'placeholderText' : ''
				},
				click: (event: any) => {
					if(event) {
						this.hideCompetitorsInputField = false;
					}
				}
		    }
		},
		{
			key: 'competitorProvider',
			type: 'input',
			className: 'customInputDoc',
			templateOptions: {

			},
			hideExpression: ((model: any, formState: any) => this.hideCompetitorsInputField)
		}, */
		{
			key: 'competitorProvider',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Competitors:',
                description: 'Who are your three closest competitors?',
                attributes: {
					'placeholderText' : '',
				},
				maxLength : 45,
                change: this.basicInput,
				keypress: (event) => {
				}
			},
		},
		{
			key: 'competitiveDifferentiators',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Competitive Differentiators:',
				minRows: 2,
				maxRows: 100,
				description: 'What sets your data offerings apart from your competition?',
				attributes: {
					'trackNames': 'Competitive Differentiators',
					'trackCategory': 'Provider Profile Form - Textbox',
					'trackLabel': 'Provider Profile Form - Input'
				},
				change: this.basicInput,
			},
		},
	];

	dataDetailsFields: FormlyFieldConfig[] = [
		/* {
			key: 'competitiveDifferentiators',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Competitive Differentiators:',
				description: 'In what languages do you offer your data products?',
				required: true,
			},
		},*/
		{
			key: 'accessOffered',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Access Offered:',
				description: 'What level of access and visibility into your research/analysis process do you offer to clients?',
				options: this.providerAccessOffered,
                required: false,
                change: this.inputChanges,
                attributes:{
                    'lookupmodule':'PROVIDER_ACCESS'
                }
			},
		},
		{
			key: 'dataProductTypes',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data Product Types:',
				description: 'What best describes the types of data products you offer to your clients? In what general format do you deliver information to your clients?',
				options: this.providerDataProductTypes,
                required: false,
                change: this.inputChanges,
                attributes:{
                    'lookupmodule':'DATA_PRODUCT_TYPE'
                }
			},
		},
		{
			key: 'dataTypes',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data Types:',
				placeholder: '',
				description: 'What best describes the types of datasets you offer to your clients? How mature are your datasets?',
				options: this.providerDataTypes,
				required: false,
				attributes:{
                    'placeholderText' : 'If you don\'t offer any datasets externally, please input "N/A".',
                    'lookupmodule':'DATA_TYPE'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'languagesAvailable',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data Languages Available:',
				description: 'In what languages do you offer your data products?',
				required: true,
                options: this.providerDataLanguagesAvailable,
                //
                attributes:{
                    'lookupmodule':'LANGUAGE'
				},
				change: this.inputChanges
			},
		},
		{
			key: 'datasetSize',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Dataset Size:',
				description: 'Approximately how much storage is needed for your entire dataset?',
				required: false,
                options: this.providerDatasetSize,
                change: this.inputChanges
			},
		},
		{
			key: 'datasetNumberOfRows',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Dataset # of Rows:',
				description: 'Approximately how many rows are in your dataset?',
				required: false,
                options: this.providerDatasetNumberOfRows,
                change: this.inputChanges
			},
		},
		{
			key: 'collectionMethodsExplanation',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Collection Methods Explanation:',
				minRows: 2,
				maxRows: 100,
				placeholder: '',
                description: 'Please provide an overview explanation of how you collect your data. Has there been any changes to your collection methods over time? If so, does this affect the ability to compare between time periods?',
				attributes:{
                    'placeholderText' : 'Details about how and where you get your raw data.',
                    'trackNames': 'Collection Methods Explanation',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 1000
				},
                change: this.basicInput
			},
		},
		{
			key: 'sampleOrPanelSize',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Sample or Panel Size:',
				minRows: 2,
				maxRows: 100,
                description: 'What is the sample or panel size of your raw data?',
                attributes: {
                    'trackNames': 'Sample or Panel Size',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 512
				},
                change: this.basicInput
			},
		},
		{
			key: 'dateCollectionBegan',
			type: 'datepicker',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Date Collection Began:',
				description: 'What is the approximate earliest date of consistent data within your offerings?',
				change: this.basicInput
			},
		},
		{
			key: 'dateCollectionRangeExplanation',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Date Collection Range Explanation:',
				minRows: 2,
				maxRows: 100,
                description: 'Please provide any further details about the amount of historical data you offer.',
                attributes: {
                    'trackNames': 'Date Collection Range Explanation',
                    'trackCategory': 'Provider Profile Form - Textbox ',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 1024
				},
                change: this.basicInput
			},
		},
		{
			key: 'keyDataFields',
			type: 'inputEditor',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Key Data Fields:',
                description: 'If applicable, what are the most critical or important data fields in your offerings?',
                attributes: {
                    'trackNames': 'Key Data Fields',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 512
				},
                change: this.basicInput
			},
		},
		{
			key: 'outOfSampleData',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Out-of-Sample Data:',
				minRows: 2,
				maxRows: 100,
                description: 'If applicable, when did your data become out-of-sample (or when did people first begin trading on it)?',
                attributes: {
                    'trackNames': 'Out-of-Sample Data',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
                },
                change: this.basicInput
			},
		},
		{
			key: 'productDetails',
			type: 'inputEditor',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Product Details:',
                description: 'Please provide any further relevant details about your data products, particularly regarding pricing, or details specific to certain products.',
                attributes: {
                    'trackNames': 'Product Details',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 3000
				},
                change: this.basicInput
			},
		},
	];

	dataDeliveryFields: FormlyFieldConfig[] = [
		{
			key: 'deliveryMethods',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Delivery Methods:',
				description: 'What are your preferred methods of delivering data?',
                options: this.deliveryMethods,
                change: this.inputChanges,
                attributes:{
                    //field: 'id',
                    'lookupmodule':'DELIVERY_METHOD'
                }
			},
		},
		{
			key: 'deliveryFormats',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Delivery Formats:',
				description: 'In what formats do you typically deliver your data products?',
                options: this.deliveryFormats,
                change: this.inputChanges,
                attributes: {
                    //field: 'id',
                    'lookupmodule':'DELIVERY_FORMAT'
                }
			},
		},
		{
			key: 'deliveryFrequencies',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Delivery Frequency:',
				description: 'How often does you typically deliver your data offerings?',
                options: this.deliveryFrequencies,
                change: this.inputChanges,
                attributes:{
                    'lookupmodule':'DELIVERY_FREQUENCY'
                }
			},
		},
		{
			key: 'lagTime',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Lag Time:',
                description: 'What is the lag time (i.e. latency) of the delivery of your data? What is the time period between when you collect or observe the data and the data buyer can retrieve the data?',
                attributes: {
                    'trackNames': 'Lag Time',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 50
				},
                change: this.basicInput
			},
		},
		{
			key: 'deliveryFrequencyNotes',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Delivery Frequency Notes:',
				minRows: 2,
				maxRows: 100,
                description: 'Please provide any additional details regarding how often you\'re able to deliver your data offerings.',
                attributes: {
                    'trackNames': 'Delivery Frequency Notes',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
				maxLength : 255,
                change: this.basicInput
			},
		},
		{
			key: 'dataUpdateFrequency',
			type: 'dropdown',
			className: 'widthSticky-dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data Update Frequency:',
				description: 'How often do you update your data?',
				options: this.dataUpdateFrequency,
				attributes: {
					field: 'label'
                },
                change: this.inputChanges
				/* change: (event: any) => {
					const queryObj = event;
					this.dataUpdateFrequency = [];

					this.filterService.getDataUpdateFrequency(queryObj.query).subscribe(d => {
						const listArray = [];
						const listItems = (d && d['hits'].total) ? d['hits']['hits'] : [];
						for (let i = 0; i < listItems.length; ++i) {
							const item = listItems[i]._source;

							listArray.push({
								id: item.id,
								lookupcode: item.lookupcode,
								recordCount: item.recordCount
							});
						}
						this.dataUpdateFrequency = [...listArray];
						this.dataDeliveryFields[5].templateOptions.options = this.dataUpdateFrequency;
					});
				},
                blur: (event: any) => {
                    if (event && event.target && !event['selectedVal']) {
                        event.target.value = '';
                    }
                } */
			},
		},
		{
			key: 'dataUpdateFrequencyNotes',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data Update Frequency Notes:',
				minRows: 2,
				maxRows: 100,
                description: 'Please provide any further details about the frequency you update your data.',
                attributes: {
                    'trackNames': 'Provider Name',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 512
				},
                change: this.basicInput
			},
		},
	];

	triallingFields: FormlyFieldConfig[] = [
		{
			key: 'freeTrialAvailable',
			type: 'checkbox',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Free Trial Availability:',
                description: 'Do you offer a free trial for evaluation purposes?',
                attributes: {
                    'multiple': 'false',
                },
                change: (event) => {
                    // this.inputChanges;
                    // console.log(event, event['modelKey']);
                    this.basicModel.freeTrialAvailable = event['modelKey'];
                    //console.log(this.basicModel.inactive, this.basicModel);
                }
			},
		},
		 {
			key: 'trialPricingModel.id',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Trial Pricing Model:',
				placeholder: '',
				description: 'If applicable, what is the pricing model for a trial to your data offerings?',
				options:this.providerTrialPricingModel,
				//required: true,
				attributes:{
					'placeholderText' : 'The general model for how you price a trial do your data offerings.'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'trialDuration',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Trial Duration:',
				minRows: 2,
				maxRows: 100,
				placeholder: '',
                description: 'For how long can you offer a trial?',
				attributes:{
                    'placeholderText' : 'Length of a trial to your data offerings.',
                    'trackNames': 'Trial Duration',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
                },
                change: this.basicInput
			},
		},
		 {
			key: 'trialAgreementType.id',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Trial Agreement Type:',
				description: 'What type of trial license agreement do you use?',
                options: this.providerTrialAgreementType,
                change: this.inputChanges
			},
		},
		{
			key: 'trialPricingDetails',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Trial Pricing Details:',
                description: 'What are the details of the trial pricing?',
                attributes: {
                    'trackNames': 'Trial Pricing Details',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 512
                },
				options: [],
                change: this.basicInput
			},
		},
	];

	licensingFields: FormlyFieldConfig[] = [
		{
			key: 'dataLicenseType.id',
			type: 'dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data License Type:',
				description: 'What type of data license do you offer?',
				options: this.providerDataLicenseType,
				attributes: {
					field: 'label'
				},
				change: this.inputChanges

            }
		},
		{
			key: 'dataLicenseTypeDetails',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data License Type Details:',
				minRows: 2,
				maxRows: 100,
                description: 'Please provide any additional relevant details regarding your data licenses.',
                attributes: {
                    'trackNames': 'Data License Type Details',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 10000
				},
                change: this.basicInput
			},
		},
		{
			key: 'paymentMethodsOffered',
			type: 'multiselect',
			className: 'custom-multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Payment Methods Offered:',
				description: 'What payment methods do you offer to your customers?',
                options: this.providerPaymentMethodsOffered,
                change: this.inputChanges,
                attributes:{
                    'lookupmodule':'PAYMENT_METHOD'
                }
			},
		},
	];

	pricingFields: FormlyFieldConfig[] = [
		/* {
			key: 'pricingModels.0',
			type: 'autocomplete',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Pricing Model:',
				placeholder: 'The general model for how you price your data products.',
				description: 'What\'s the general model for how you price your data products?',
				required: true,
				options: this.pricingModel,
				attributes: {
					field: 'desc',
					'dropdown': 'true'
				},
				change: (event: any) => {
					const queryObj = event;
					this.pricingModel = [];

					this.filterService.getPricingModel(queryObj.query).subscribe(d => {
						const listArray = [];
						const listItems = (d && d['hits'].total) ? d['hits']['hits'] : [];
						for (let i = 0; i < listItems.length; ++i) {
							const item = listItems[i]._source;

							listArray.push({
								id: item.id,
								desc: item.description,
								locked: item.locked ? true : false
							});
						}
						this.pricingModel = [...listArray];
						this.pricingFields[0].templateOptions.options = this.pricingModel;
					});
				},
                blur: (event: any) => {
                    if (event && event.target && !event['selectedVal']) {
                        event.target.value = '';
                    }
                }
			},
		}, */
		/* {
			key: 'pricingModel.id',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Pricing Model:',
				placeholder: '',
				description: 'What\'s the general model for how you price your data products?',
                options: this.providerPricingModel,
                required: true,
				attributes: {
					'placeholderText' : 'The general model for how you price your data products.'
				},
				change: this.basicInput
			},
		}, */
		{
			key: 'dataProvidersPricingModels',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Pricing Model:',
				required: true,
				description: 'What pricing models do you use to price your data products?',
				attributes: {
					'placeholderText' : 'General models for how you price your data products.'
				},
                options: this.providerPricingModel
			},
		},
		{
			key: 'subscriptionModel.id',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Subscription Model:',
				description: 'What\'s the general subscription model your data products?',
                options: this.providerSubscriptionModel,
                change: this.inputChanges
			},
		},
		{
			key: 'subscriptionTimePeriod.id',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Subscription Time Period:',
				description: 'What is the typical time period offered for a subscription to your data products?',
                options: this.providerSubscriptionTimePeriod,
                change: this.inputChanges
			},
		},
		{
			key: 'monthlyPriceRange.id',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Monthly Price Range:',
				description: 'What is the price range for a monthly subscription to your data offerings?',
                options: this.providerMonthlyPriceRange,
                change: this.inputChanges
			},
		},
		{
			key: 'minimumYearlyPrice',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Minimum Yearly Price:',
                description: 'What is the minimum price for a yearly subscription to your data products?',
                attributes: {
                    'trackNames': 'Minimum Yearly Price',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
				maxLength : 50,
                change: this.basicInput,
				keypress: (event) => {
				}
			},
		},
		{
			key: 'maximumYearlyPrice',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Maximum Yearly Price:',
                description: 'What is the maximum price for a yearly subscription to your data products?',
                attributes: {
                    'trackNames': 'Maximum Yearly Price',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
				},
				maxLength : 50,
                change: this.basicInput,
				keypress: (event) => {
				}
			},
		},
		{
			key: 'pricingDetails',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Pricing Details:',
                description: 'What are the details of the pricing for your data products?',
                attributes: {
                    'trackNames': 'Pricing Details',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
                },
                change: this.basicInput
			},
        },
	];

	dataQualityFields: FormlyFieldConfig[] = [
		{
			key: 'pointInTimeAccuracy.id',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Point-in-Time Accuracy:',
				description: 'Is your data point-in-time accurate?',
				placeholder: '',
				options: this.providerPointInTimeAccuracy,
				attributes:{
					'placeholderText' : 'Accurate Point-in-Time Data has not been changed after its been captured/generated and published, even if issues with the data are found later on. Fixes can be made available in separate series but the underlying data is never changed. In other words, if corporate actions that affect the company identifiers in your data are tracked, are those identifiers accurate at each particular point-in-time?'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'dataGaps.id',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data Gaps:',
				description: 'Does your dataset include any data gaps? In other words, are there unexpected missing data?',
                options: this.providerDataGaps,
                change: this.inputChanges
			},
		},
		{
			key: 'dataGapsReasons',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data Gaps Reasons:',
				placeholder: '',
				description: 'If there are some gaps in your dataset, what are the reasons that the data is missing?',
				options: this.providerDataGapsReasons,
				attributes: {
                    'placeholderText' : '"Missing completely at random" : missing data has no relation to outcomes."Missing at random": missing data tends to happen in certain situations but has no relation to outcomes."Missing not at random": missing data is usually due to a common connection between the outcomes.',
                    'lookupmodule':'DATA_GAPS_REASON'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'duplicatesCleaned.id',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Duplicates Cleaned:',
				description: 'Have you scrubbed out the duplicates from your dataset?',
                options: this.providerDuplicatesCleaned,
                change: this.inputChanges
			},
		},
		{
			key: 'includesOutliers.id',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Includes Outliers:',
				description: 'Does your dataset have any outliers? In other words, are there any data points that do not follow the patterns of the vast majority of the rest of the data?',
                options: this.providerIncludesOutliers,
                change: this.inputChanges
			},
		},
		{
			key: 'outlierReasons',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Outlier Reasons:',
				placeholder: '',
				description: 'If there are some outliers in your dataset, what are the reasons that they occur?',
				options: this.providerOutlierReasons,
				attributes:{
                    'placeholderText' : '"Expected": outliers are expected based on unique nature of the dataset."Inaccurate measurements": the way the data has been measured (e.g. sensors) produce inaccurate measurements."Collection Methodology": the collection methodology produce inaccurate data."Clerical": the human element (e.g. transcription) produce inaccurate data."Technical": the technical element (e.g. software bugs) produce inaccurate data.',
                    'lookupmodule':'OUTLIER_REASON'
                },
                change: this.inputChanges
			},
		},
		{
			key: 'biases',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Biases:',
				placeholder: '',
				description: 'What types of biases does your dataset contain?',
				options: this.providerBiases,
				attributes: {
                    'placeholderText' : '"Selection - Demographic": participants are more likely to be selected for the study based on their demographic makeup "Selection - Geographic": participants are more likely to be selected for the study based on their geographical location Selection - Time: a study being terminated or started due to timing "Selection - Exclusion": the systematic exclusion of certain individuals or data from the study "Selection - Other": any other biases related to the selection of the study participants, such as Self-Selection Bias "Survivorship": concentrating the study on things that made it past some selection process and overlooking those that did not, typically because of their lack of visibility (e.g. excluding companies that no longer exist) "Overfitting": modeling error which occurs when a function is too closely fit to a limited set of data points, thus making the model overly complex "Underfitting": modeling error which occurs when a function cannot capture the underlying trend of the data, thus making the model overly simple "Omitted Variable": the study leaves out one or more relevant variables "Confirmation": the tendency to favor information in a way that confirms one\'s preexisting beliefs or hypotheses "Detection": when a phenomenon is more likely to be observed for a particular set of study subjects "Recall": systematic errors cause by differences in the accuracy or completeness of the recollections retrieved by study participants regarding events or experiences from the past "Observer": arises when the researcher subconsciously influences the experiment "Funding": the tendency of the study to support the interests of the study\'s financial sponsor "Methodological": arising from the way results are collected (e.g. asking survey questions in a specific order to incite specific responses)"Reporting": the tendency of a study to support the interests of the study\'s financial sponsor "Analytical": arising from the way results are evaluated (e.g. assuming normality of a dataset)',
                    'lookupmodule':'BIASES'
                },
                change: this.inputChanges
			},
        },
        {
			key: 'sampleOrPanelBiases',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Sample or Panel Biases:',
                description: 'Do you know of any particular biases within your sample or panel, such as certain demographics, geographies or other common characteristics?',
                attributes: {
                    'trackNames': 'Sample or Panel Biases',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 512
				},
                change: this.basicInput
			},
		},
		{
			key: 'normalized.id',
			type: 'radioButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Normalized:',
				description: 'Have you normalized your dataset for things like seasonality, selection biases (e.g. overweighted in a particular demographic), or attrition (ie panel growing or declining over time)?',
                options: this.providerNormalized,
                change: this.inputChanges
			},
        },
        {
			key: 'discussionCleanliness',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Data Quality Details:',
                description: 'Please discuss the effort you have put into clean and/or normalizing your data, such as the work done and the methods used.',
                attributes: {
                    'trackNames': 'Data Quality Details',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 255
				},
                change: this.basicInput
			},
		},
	];
	discussionFields: FormlyFieldConfig[] = [
		{
			key: 'useCasesOrQuestionsAddressed',
			type: 'inputEditor',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Use Cases:',
				placeholder: '',
                description: 'Please describe some specific use cases for your data products or questions that could be answered by them. How did your data products demonstrate something that otherwise wouldn\'t have been known? Ultimately, how can clients use your data products to make money?',
				attributes:{
                    'placeholderText' : 'If possible, focus your response within the finance industry and, specifically, investment research. For example, how do your data products provide insights into the financial performance of public companies, such as topline revenues or earnings?',
                    'trackNames': 'Use Cases or Questions Addressed',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 512
				},
                change: this.basicInput
			},
		},
		/* {
			key: 'discussionCleanlines',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Discussion - Cleanliness:',
				minRows: 2,
				maxRows: 100,
                description: 'Please discuss the effort you have put into clean and/or normalizing your data, such as the work done and the methods used.',
                attributes: {
                    'trackNames': 'Discussion - Cleanliness',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input'
                },
                change: this.basicInput
			},
		}, */
		{
			key: 'discussionAnalytics',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Discussion - Analytics:',
				minRows: 2,
				maxRows: 100,
                description: 'Please discuss the effort you have put into analyzing and/or productizing your data, such as any backtests performed or research/analytics products created.',
                attributes: {
                    'trackNames': 'Discussion - Analytics',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 255
				},
                change: this.basicInput
			},
		},
		{
			key: 'potentialDrawbacks',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Potential Drawbacks:',
				minRows: 2,
				maxRows: 100,
                description: 'Which factors decrease the overall appeal of your products for investment research?',
                attributes: {
                    'trackNames': 'Potential Drawbacks',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 512
				},
                change: this.basicInput
			},
		},
		{
			key: 'dataRoadmap',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
            name:'Data Roadmap',
			templateOptions: {
				label: 'Data Roadmap:',
				minRows: 2,
				maxRows: 100,
                description: 'What are your future plans regarding your data offerings? Any forthcoming, noteworthy products or developments?',
                attributes: {
                    'trackNames': 'Unique Value Props',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 512
				},
                change: this.basicInput
			},
		},
		{
			key: 'notes',
			type: 'textarea',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Notes:',
				minRows: 2,
				maxRows: 100,
                description: 'Please add any further useful information not supplied in any other response.',
                attributes:{
                    'trackNames': 'Legal & Compliance Issues',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input',
                    'maximumLength' : 10000
				},
                change: this.basicInput
			},
		},
	];

	csrf: string;
	uppyEvent = new Subject<[string, any, any, any]>();
	uppyPlugins = [
		['Dashboard', {
			target: '.DashboardContainer',
			replaceTargetContent: true,
			inline: true,
			restrictions: {
				maxFileSize: 200000000,
				maxNumberOfFiles: 10,
				minNumberOfFiles: 1,
				allowedFileTypes: ['image/*', 'video/*']
			}
		}],
		['XHRUpload', {
			endpoint: '/api/multiple-file-upload?providerId=' + this.recordID,
			formData: true,
			bundle: true,
			fieldName: 'fileUploads'
        }]/* ,
		['GoogleDrive', {
			target: 'Dashboard',
			serverUrl: 'https://companion.uppy.io'
		}],
		['Dropbox', { target: 'Dashboard', serverUrl: 'https://companion.uppy.io' }] */
	];
	onDestroy$ = new Subject<void>();

	constructor(
		private dataProviderService: DataProviderService,
		private dataResourceService: AddResourceAdminService,
        private providerUserOnboardingService: ProviderUserOnboardingService,
        private userOnboardingService: UserOnboardingService,
		private filterService: AmassFilterService,
		private route: ActivatedRoute,
		private router: Router,
		public dialog: MatDialog,
        private principal: Principal,
        private authService: AuthServerProvider,
		public _DomSanitizer: DomSanitizer,
		public googleAnalyticsService: GoogleAnalyticsEventsService,
		private loaderService: LoaderService,
		private signinModalService: SigninModalService,
		@Inject(WINDOW) private window: Window,
		public snackBar: MatSnackBar,
		private uppyService: UppyService,
        private csrfService: CSRFService,
        private el: ElementRef,
        private stateService: StateStorageService,
		private cookieService: CookieService,
		private account: AccountService,
		private http: HttpClient,
        private datePipe: DatePipe
	) {
        this.routerOutletRedirect = '';
		this.domSanitizer = DomSanitizer;
		this.formSubmitted = false;
		this.hideProviderInputField = true;
		this.hideDistributionInputField = true;
		this.hideConsumerInputField = true;
		this.hideComplementaryInputField = true;
		this.hideCompetitorsInputField = true;
		this.hideMarketingSection = true;
		this.hideDocumentSection = true;
		this.hideSamplesSection = true;
		this.hideReportSection = true;
		this.uppyEvent.pipe(
			takeUntil(this.onDestroy$),
			filter(([ev]) => contains(ev, ['file-added'])),)
			.subscribe(
				([ev, data1, data2, data3]) => {
					/* console.log("Received '" + ev + "' event from instance 1", 'Upload complete');
					console.log(data1);
					console.log(data2);
					console.log(data3);
					console.log(ev); */
				},
				err => console.log(err),
				() => console.log('done')
			);
	}

	openUppyDialog(events:any){
		this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        this.orgDialogRef = this.dialog.open(UppyDialogComponent, {
			disableClose: true
		});
		this.orgDialogRef.componentInstance.providerRecordID = this.recordID;
	}

	ngOnInit() {
		_that = this;
        this.csrf = this.csrfService.getCSRF();
        this.filterService.setAuthorisedUrl();
		this.loaderService.display(true);
		this.agreeValidationMessage = null;
		this.isLoading = true;
		this.isPendingUnlock = false;
		this.isPendingExpand = false;
		this.tabIndex = 0;
		this.privacySecurityPanel = true;
		this.basicExpansionPanel = false;
        this.documentExpansionPanel = false;
		this.companyExpansionPanel = false;
		this.descriptionsExpansionPanel = false;
		this.categorizationsExpansionPanel = false;
		this.investigationExpansionPanel = false;
		this.relatedProvidersExpansionPanel = true;
		this.dataDetailsExpansionPanel = false;
		this.dataDeliveryExpansionPanel = false;
		this.triallingExpansionPanel = false;
		this.licensingExpansionPanel = false;
        this.pendingEdits = false;

		this.route.queryParams.subscribe(params => {
			if (params['pending-unlock']) {
				this.isPendingUnlock = true;
			}

			if (params['pending-expand']) {
				this.isPendingExpand = true;
			}
        });
        this.providerPartnersCount = 0;
        this.distributionPartnersCount = 0;
        this.consumerPartnersCount = 0;
        this.complementaryProvidersCount = 0;
        this.competitorsCount = 0;
		this.subscription = this.route.params.subscribe(params => {
			this.recordID = params['recordID'];
			this.account.getProviderEdit(params['recordID']).subscribe(data => {
				if (data && !data.recordUpdated) {
					this.pendingEdits = false;
				} else {
					this.pendingEdits = true;
				}
			}, error => {
				 this.pendingEdits = false;
			});
			this.load(params['recordID']);
        });

        this.principal.inactivityFormSubmit$.subscribe(result => {
            console.log('Result Edit Form 1', result);
            if (result === true) {
                this.forceFormSubmit = true;
                this.formSubmitted = true;
                this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
            }
        });

        this.unsavedSubscription = this.principal.unsavedPopup$.subscribe(result => {
            // console.log(result);
            if (result === true) {
                this.unsavedEdits = true;
                this.navigateNextPage = false;
            }
        });

        this.userOnboardingService.checkUser().subscribe(data => {
            this.userObj = data;
        });

        this.uppyPlugins = [
            ['Dashboard', {
                target: '.DashboardContainer',
                replaceTargetContent: true,
                inline: true,
                restrictions: {
                    maxFileSize: 200000000,
                    maxNumberOfFiles: 10,
                    minNumberOfFiles: 1,
                    allowedFileTypes: ['image/*', 'video/*']
                }
            }],
            ['XHRUpload', {
                endpoint: '/api/multiple-file-upload?providerId=' + this.recordID,
                formData: true,
                bundle: true,
                fieldName: 'fileUploads'
            }]/* ,
            ['GoogleDrive', {
                target: 'Dashboard',
                serverUrl: 'https://companion.uppy.io'
            }],
            ['Dropbox', { target: 'Dashboard', serverUrl: 'https://companion.uppy.io' }] */
        ];
		this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
		this.relevantArticlePage = 0;
		this.relevantArticleLinks = {
			last: 0
		};
		this.authoredArticlePage = 0;
		this.authoredArticleLinks = {
			last: 0
		};
		this.mdTooltipDelay = 500;

		this.mainDataIndustriesArr = [];
		this.filterService.getMainDataIndustry().subscribe(data => {
			const listItems = (data['aggregations'] && data['aggregations'].dataindustries) ? data['aggregations'].dataindustries.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i].mainDataIndustry.hits.hits[0]._source.mainDataIndustry;

				this.mainDataIndustriesArr.push({
					id: listItems[i].key,
					dataIndustry: item.dataIndustry
				});
			}
		});

		this.dataIndustries = [];
		this.filterService.getRelatedDataIndustry().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.dataIndustry !== '' && listItems[i]._source.dataIndustry !== 'DATA_INDUSTRY_NULL') {
					this.dataIndustries.push({
						id: listItems[i]._source.id,
						desc: listItems[i]._source.dataIndustry
					});
				}
			}
		});

		this.mainDataCategories = [];
		this.filterService.getAllDataCategories().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i]._source;
				if (item.datacategory !== '' && item.datacategory !== 'DATA_CATEGORY_NULL') {
					this.mainDataCategories.push({
						id: item.id,
						datacategory: item.datacategory,
						img: item.logo ? item.logo.logo : null
					});
				}
			}
		});

		this.dataCategories = [];
		this.filterService.getAllDataCategories().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.datacategory !== '' && listItems[i]._source.datacategory !== 'DATA_CATEGORY_NULL') {
					this.dataCategories.push({
						id: listItems[i]._source.id,
						desc: listItems[i]._source.datacategory,
						locked: listItems[i]._source.locked,
						img: listItems[i]._source.logo ? listItems[i]._source.logo.logo : null
					});
				}
			}
		});

		this.dataFeatures = [];
		this.filterService.getDataFeatures().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.dataFeatures.push({
					id: listItems[i]._source.id,
					desc: listItems[i]._source.dataFeature
				});
			}
		});

		this.dataSources = [];
		this.filterService.getDataSource().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.dataSources.push({
					value: {
						id: listItems[i]._id,
						desc: listItems[i]._source.dataSource,
						locked: listItems[i]._source.locked ? listItems[i]._source.locked : false
					},
					label: listItems[i]._source.dataSource
				});
			}
		});

		this.geographicalFocus = [];
		this.filterService.getGeographicFocus().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.geographicalFocus.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});
		this.providerApproximateHeadcount = [];
		this.filterService.getLookupCodes('HEADCOUNT_BUCKET').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			// this.providerApproximateHeadcount.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerApproximateHeadcount.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.description
				});
			}
			this.companyDetailsFields[7].templateOptions.options = this.providerApproximateHeadcount;
		});
		/* this.providerTags = []; */
		/* this.filterService.getDataProviderTagUrl().subscribe(data => {
			const listItems = data;
			for (let i = 0; i < listItems.length; i++) {
                const item = listItems[i];
                if (item.privacy === 'Public') {
                    this.providerTags.push({
                        value: {
                            id: listItems[i].id,
                            desc: listItems[i].providerTag,
                            locked: listItems[i].locked ? listItems[i].locked : false
                          //  locked: listItems[i]._source.locked ? listItems[i]._source.locked : false
                        },
                        label: listItems[i].providerTag
                    });
                }
			}
		}); */

		this.providerPointInTimeAccuracy = [];
		this.filterService.getLookupCodes('YES_OR_NO_SOFT').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			/* for (let i = 0; i < listItems.length; i++) {
				this.providerPointInTimeAccuracy.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			} */
			const pointInTimeAccuracy = data ? data['hits']['hits'] : [];
			// const providerPointInTime = [];
			pointInTimeAccuracy.map((val, index) => {
               // console.log('......pointer mapping', val);
				this.providerPointInTimeAccuracy.push({
					value: val._source.id,
					label: val._source.description
                });
			});
			this.dataQualityFields[0].templateOptions.options = this.providerPointInTimeAccuracy;
		});

		this.providerDataGaps = [];
		this.filterService.getLookupCodes('YES_NO_UNKNOWN').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			/* for (let i = 0; i < listItems.length; i++) {
				this.providerDataGaps.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			} */
			const dataGaps = data ? data['hits']['hits'] : [];
			// const providerPointInTime = [];
			dataGaps.map((val, index) => {
              //  console.log('......DataGaps mapping', val);
				this.providerDataGaps.push({
					value: val._source.id,
					label: val._source.description
                });
			});
			this.dataQualityFields[1].templateOptions.options = this.providerDataGaps;
		});

		this.providerSubscriptionModel = [];
		this.filterService.getLookupCodes('SUBSCRIPTION_MODEL').subscribe(data => {
		/* 	const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerSubscriptionModel.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			} */
			const subscriptionModel = data ? data['hits']['hits'] : [];
			subscriptionModel.map((val, index) => {
                //console.log('......pointer mapping', val);
				this.providerSubscriptionModel.push({
					value: val._source.id,
					label: val._source.description
                });
			});
			this.pricingFields[1].templateOptions.options = this.providerSubscriptionModel;
		});

		this.providerSubscriptionTimePeriod = [];
		this.filterService.getLookupCodes('TIME_PERIOD').subscribe(data => {
			/* const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerSubscriptionTimePeriod.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			} */
			const subscriptionTimePeriod = data ? data['hits']['hits'] : [];
			subscriptionTimePeriod.map((val, index) => {
                //console.log('......pointer mapping', val);
				this.providerSubscriptionTimePeriod.push({
					value: val._source.id,
					label: val._source.description
                });
			});
			this.pricingFields[2].templateOptions.options = this.providerSubscriptionTimePeriod;
		});

		this.providerMonthlyPriceRange = [];
		this.filterService.getLookupCodes('MONTHLY_PRICE_RANGE').subscribe(data => {
			/* const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerMonthlyPriceRange.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			} */
			const monthlyPriceRange = data ? data['hits']['hits'] : [];
			monthlyPriceRange.map((val, index) => {
                //console.log('......pointer mapping', val);
				this.providerMonthlyPriceRange.push({
					value: val._source.id,
					label: val._source.description
                });
			});
			this.pricingFields[3].templateOptions.options = this.providerMonthlyPriceRange;
		});

		this.providerTrialPricingModel = [];
		this.filterService.getLookupCodes('PRICING_MODEL').subscribe(data => {
			/* const listItems = data && data['hits']  ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerTrialPricingModel.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			} */
			const trialPricingModel = data ? data['hits']['hits'] : [];
			trialPricingModel.map((val, index) => {
                //console.log('......pointer mapping', val);
				this.providerTrialPricingModel.push({
					value: val._source.id,
					label: val._source.description
                });
			});
			this.triallingFields[1].templateOptions.options = this.providerTrialPricingModel;
		});

	   this.providerPartnershipModel = [];
	   this.filterService.getLookupCodes('PARTNERSHIP_TYPE').subscribe(data =>{
		   const listItems = data && data['hits'] ? data['hits']['hits'] : [];
		   for(let i = 0; i< listItems.length; i++){
			   this.providerPartnershipModel.push({
				   value: listItems[i]._source.id,
				   label: listItems[i]._source.description
			   });
		   }
		   this.privacySecurityFields[1].templateOptions.options = this.providerPartnershipModel;
	   });

		this.providerTrialAgreementType = [];
		this.filterService.getLookupCodes('AGREEMENT_TYPE').subscribe(data => {
			/* const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerTrialAgreementType.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			} */
			const trialAgreementType = data ? data['hits']['hits'] : [];
			trialAgreementType.map((val, index) => {
                //console.log('......pointer mapping', val);
				this.providerTrialAgreementType.push({
					value: val._source.id,
					label: val._source.description
                });
			});
			this.triallingFields[3].templateOptions.options = this.providerTrialAgreementType;
		});
		this.providerTags = [];
        this.filterService.getDataProviderPublicTagUrl().subscribe(data => {
            const listItems = data;
			for (let i = 0; i < listItems.length; i++) {
                const item = listItems[i];
				if (item.privacy === 'Public' && item.type === 'Tag') {
                    this.providerTags.push({
                        value: {
                            id: listItems[i].id,
                            desc: listItems[i].providerTag,
                           // locked: listItems[i].locked ? listItems[i].locked : false
                        },
                        label: listItems[i].providerTag
                    });
			}
		}
		});
		this.providerWillingness = [];
		this.filterService.getWillingnessToProvideToInvestors().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			// this.providerWillingness.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.description !== '') {
					this.providerWillingness.push({
						value: listItems[i]._source.id,
						label: listItems[i]._source.description
					});
				}
			}
			this.investigationFields[0].templateOptions.options = this.providerWillingness;
		});
        this.providerSecRegulated = [];
		this.filterService.getLookupCodes('YES_OR_NO').subscribe(data => {
		/* 	const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			this.providerSecRegulated.push({label : 'Select', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerSecRegulated.push({
					value: listItems[i]._source,
					label: listItems[i]._source.lookupcode
				});
			} */
			const listItems = data ? data['hits']['hits'] : [];
			// this.providerSecRegulated.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerSecRegulated.push({
					// value: {
					// 	id: listItems[i]._source.id,
					// 	desc: listItems[i]._source.lookupcode
					// },
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[1].templateOptions.options = this.providerSecRegulated;
		});
		this.providerInvestorClientBucket = [];
		this.filterService.getLookupCodes('CLIENT_RANGE').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			// this.providerInvestorClientBucket.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerInvestorClientBucket.push({
					value: listItems[i]._source.lookupcode,
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[2].templateOptions.options = this.providerInvestorClientBucket;
		});

		this.providerOrganizationType = [];
		this.filterService.getLookupCodes('ORGANIZATION_TAG').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerOrganizationType.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[6].templateOptions.options = this.providerOrganizationType;
		});

		this.providerManagerType = [];
		this.filterService.getLookupCodes('INVESTMENT_MANAGER_TYPE').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerManagerType.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[7].templateOptions.options = this.providerManagerType;
		});

		this.providerRelevantInvestors = [];
		this.filterService.getInvestorType().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerRelevantInvestors.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}

			this.investigationFields[8].templateOptions.options = this.providerRelevantInvestors;
		});

		this.providerStrategies = [];
		this.filterService.getLookupCodes('INVESTMENT_MANAGER_STRATEGY').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerStrategies.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[9].templateOptions.options = this.providerStrategies;
		});

		this.providerResearchStyles = [];
		this.filterService.getLookupCodes('INVESTMENT_MANAGER_RESEARCH_STYLE').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerResearchStyles.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[10].templateOptions.options = this.providerResearchStyles;
		});

		this.providerInvestingTimeFrame= [];
		this.filterService.getLookupCodes('INVESTING_TIME_FRAME').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerInvestingTimeFrame.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[11].templateOptions.options = this.providerInvestingTimeFrame;
		});

		this.providerEquitiesMarketCap = [];
		this.filterService.getLookupCodes('EQUITY_MARKET_CAP').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerEquitiesMarketCap.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[12].templateOptions.options = this.providerEquitiesMarketCap;
		});

		this.providerEquitiesStyle = [];
		this.filterService.getLookupCodes('EQUITY_STYLE').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerEquitiesStyle.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[13].templateOptions.options = this.providerEquitiesStyle;
		});

		// this.providerMainAssetClass = [];
		// this.filterService.getMainAssetClass().subscribe(data => {
		// 	const listItems = (data['aggregations'] && data['aggregations'].mainAssetclass
		// ) ? data['aggregations'].mainAssetclass.buckets : [];
		// this.providerMainAssetClass.push({label : 'Select', value: null });
		// 	for (let i = 0; i < listItems.length; ++i) {
		// 		const item = listItems[i].mainAssetClass.hits.hits[0]._source.mainAssetClass;
		// 		this.providerMainAssetClass.push({
		// 			value: listItems[i].key,
		// 			label: item.description
		// 		});
		// 	}
		// 	this.investigationFields[14].templateOptions.options = this.providerMainAssetClass;
		// });

		this.providerMainAssetClass = [];
		this.filterService.getLookupCodes('ASSET_CLASS').subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			// this.providerMainAssetClass.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerMainAssetClass.push({
					value: listItems[i]._source.description,
					label: listItems[i]._source.description
				});
			}
			this.investigationFields[14].templateOptions.options = this.providerMainAssetClass;
		});

		this.providerRelevantAssetClass = [];
		this.filterService.getRelatedAssetClass().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerRelevantAssetClass.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.description
					},
					label: listItems[i]._source.description
				});
			}
			this.investigationFields[15].templateOptions.options = this.providerRelevantAssetClass;
		});

		this.relevantSecurityTypes = [];
		this.filterService.getSecurityType().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.relevantSecurityTypes.push({
					value: {
						id: listItems[i]._source.id,
						 desc: listItems[i]._source.lookupcode,
						// locked: listItems[i]._source.locked ? listItems[i]._source : false
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[16].templateOptions.options = this.relevantSecurityTypes;
		});

		this.relevantSectors = [];
		this.filterService.getRelevantSectors().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.relevantSectors.push({
					value: {
						id: listItems[i]._source.id,
						 desc: listItems[i]._source.description,
						// locked: listItems[i]._source.locked ? listItems[i]._source : false
					},
					label: listItems[i]._source.description
				});
			}
			this.investigationFields[17].templateOptions.options = this.relevantSectors;
		});

		this.providerCoverageRange = [];
		this.filterService.getLookupCodes('COVERAGE_RANGE').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerCoverageRange.push({
					value: listItems[i]._source.lookupcode,
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[18].templateOptions.options = this.providerCoverageRange;
		});

		this.providerIdentifiersAvailable = [];
		this.filterService.getLookupCodes('IDENTIFIERS').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerIdentifiersAvailable.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[21].templateOptions.options = this.providerIdentifiersAvailable;
		});

		this.providerNumberOfDataSources = [];
		this.filterService.getLookupCodes('DATA_SOURCES_RANGE').subscribe(data => {
			/* const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			this.providerNumberOfDataSources.push({label : 'Select', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerNumberOfDataSources.push({
					value: listItems[i]._source,
					label: listItems[i]._source.lookupcode
				});
			} */
			const listItems = data ? data['hits']['hits'] : [];
			// this.providerNumberOfDataSources.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerNumberOfDataSources.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			}
		});

		this.providerAccessOffered = [];
		this.filterService.getLookupCodes('PROVIDER_ACCESS').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerAccessOffered.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDetailsFields[0].templateOptions.options = this.providerAccessOffered;
		});

		this.providerDataProductTypes = [];
		this.filterService.getLookupCodes('DATA_PRODUCT_TYPE').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerDataProductTypes.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDetailsFields[1].templateOptions.options = this.providerDataProductTypes;
		});

		this.providerDataTypes = [];
		this.filterService.getLookupCodes('DATA_TYPE').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerDataTypes.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDetailsFields[2].templateOptions.options = this.providerDataTypes;
		});

		this.providerDataLanguagesAvailable = [];
		this.filterService.getLookupCodes('LANGUAGE').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerDataLanguagesAvailable.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDetailsFields[3].templateOptions.options = this.providerDataLanguagesAvailable;
			if (Array.isArray(this.dataDetailsFields[3].model['languagesAvailable']) || this.dataDetailsFields[3].model['languagesAvailable']  === undefined) {
				this.dataDetailsFields[3].model['languagesAvailable'] = [];
				this.dataDetailsFields[3].model['languagesAvailable'].push( this.providerDataLanguagesAvailable[0].value);
			} else {
				this.dataDetailsFields[3].model['languagesAvailable']
			}
		});

		this.providerDatasetSize = [];
		this.filterService.getLookupCodes('DATA_SIZE_RANGE').subscribe(data => {
			/* const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerDatasetSize.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			} */
			const datasetSize = data ? data['hits']['hits'] : [];
			datasetSize.map((val, index) => {
                //console.log('......pointer mapping', val);
				this.providerDatasetSize.push({
					value: val._source.description,
					label: val._source.description
                });
			});
			this.dataDetailsFields[4].templateOptions.options = this.providerDatasetSize;
		});

		this.providerDatasetNumberOfRows = [];
		this.filterService.getLookupCodes('ROWS_RANGE').subscribe(data => {
			/* const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerDatasetNumberOfRows.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			} */
			const datasetNumberOfRows = data ? data['hits']['hits'] : [];
			datasetNumberOfRows.map((val, index) => {
                //console.log('......pointer mapping', val);
				this.providerDatasetNumberOfRows.push({
					value: val._source.description,
					label: val._source.description
                });
			});
			this.dataDetailsFields[5].templateOptions.options = this.providerDatasetNumberOfRows;
		});

		this.providerDataLicenseType = [];
		this.filterService.getLookupCodes('LICENSE_TYPE').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			// this.providerDataLicenseType.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.providerDataLicenseType.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			}
			this.licensingFields[0].templateOptions.options = this.providerDataLicenseType;
		});

		this.providerPaymentMethodsOffered = [];
		this.filterService.getLookupCodes('PAYMENT_METHOD').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerPaymentMethodsOffered.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.licensingFields[2].templateOptions.options = this.providerPaymentMethodsOffered;
		});

		this.providerDataGapsReasons = [];
		this.filterService.getLookupCodes('DATA_GAPS_REASON').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerDataGapsReasons.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataQualityFields[2].templateOptions.options = this.providerDataGapsReasons;
		});

		this.providerDuplicatesCleaned = [];
		this.filterService.getLookupCodes('YES_NO_UNKNOWN').subscribe(data => {
			/* const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerDuplicatesCleaned.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			} */
			const duplicatesCleaned = data ? data['hits']['hits'] : [];
			duplicatesCleaned.map((val, index) => {
                //console.log('......pointer mapping', val);
				this.providerDuplicatesCleaned.push({
					value: val._source.id,
					label: val._source.description
                });
			});
			this.dataQualityFields[3].templateOptions.options = this.providerDuplicatesCleaned;
		});

		this.providerIncludesOutliers = [];
		this.filterService.getLookupCodes('YES_NO_UNKNOWN').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
		/* 	for (let i = 0; i < listItems.length; i++) {
				this.providerIncludesOutliers.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			} */
			const includesOutliers = data ? data['hits']['hits'] : [];
			includesOutliers.map((val, index) => {
                //console.log('......pointer mapping', val);
				this.providerIncludesOutliers.push({
					value: val._source.id,
					label: val._source.description
                });
			});
			this.dataQualityFields[4].templateOptions.options = this.providerIncludesOutliers;
		});

		this.providerOutlierReasons = [];
		this.filterService.getLookupCodes('OUTLIER_REASON').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerOutlierReasons.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataQualityFields[5].templateOptions.options = this.providerOutlierReasons;
		});

		this.providerBiases = [];
		this.filterService.getLookupCodes('BIASES').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerBiases.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataQualityFields[6].templateOptions.options = this.providerBiases;
		});

		this.providerNormalized = [];
		this.filterService.getLookupCodes('YES_NO_UNKNOWN').subscribe(data => {
			/* const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerNormalized.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			} */
			const normalized = data ? data['hits']['hits'] : [];
			normalized.map((val, index) => {
                //console.log('......pointer mapping', val);
				this.providerNormalized.push({
					value: val._source.id,
					label: val._source.description
                });
			});
			this.dataQualityFields[8].templateOptions.options = this.providerNormalized;
		});

		this.dataProviderService.getProvidersTypes().subscribe((data: any[]) => {
			const providerTypes = data;
			const pTypesArr = [];
			providerTypes.map((val, index) => {
				pTypesArr.push({
					value: val.id,
					label: val.providerType
				});
			});

			this.dataProviderTypes = pTypesArr;
			this.categorizationsFields = [
				{
					key: 'providerType.id',
					type: 'radioButton',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Provider Type:',
						description: 'How do you handle data and what do you do with it? Are you creating your own analytics, strictly acting as a pass-through data provider, or just collecting data? In other words, at what step in the data monetization process do you sit?',
						required: true,
                        options: pTypesArr,
                        change: this.inputChanges
					},
				},
				{
					key: 'mainDataIndustry',
					type: 'autocomplete',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Main Data Industry:',
						type: 'autocomplete',
						placeholder: '',
						description: 'What is the primary industry you provide data to?',
						required: true,
						attributes: {
							field: 'dataIndustry',
							'dropdown': 'true',
							'placeholderText' : 'Sometimes you belong in more than one industry, e.g. marketing and retail - please choose the primary industry that you provide data to, and all other industries in the following question.'
						},
						options: this.mainDataIndustriesArr,
						change: (event: any) => {
							const queryObj = event;
							this.mainDataIndustriesArr = [];
							this.filterService.getMainDataIndustry(queryObj.query).subscribe(d => {
								const listArray = [];
								const listItems = (d['aggregations'] && d['aggregations'].dataindustries) ? d['aggregations'].dataindustries.buckets : [];
								for (let i = 0; i < listItems.length; ++i) {
									const item = listItems[i].mainDataIndustry.hits.hits[0]._source.mainDataIndustry;
									if (item.dataIndustry !== 'DATA_INDUSTRY_NULL') {
										listArray.push({
											id: listItems[i].key,
											dataIndustry: item.dataIndustry
										});
									}
								}
								this.mainDataIndustriesArr = [...listArray];
								setTimeout(() => {
									this.categorizationsFields[1].templateOptions.options = this.mainDataIndustriesArr;
								}, 2500);
							});
                            this.inputChanges(event);
						},
                        blur: (event: any) => {
                            if (event && event.target && !event['selectedVal']) {
                                event.target.value = '';
                            }
                        }
					},
				},
				{
					key: 'industries',
					type: 'autocomplete',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Data Industries:',
						placeholder: '',
						description: 'What are all the industries you provide data to, aside from your main industry?',
						options: this.dataIndustries,
						attributes: {
							field: 'desc',
							'explanation': 'If you provide data for use in investing, please add Investment Management as one of your industries.',
							'multiple': 'true',
							'dropdown': 'true',
							'placeholderText' : 'If you provide data for use in investing, please add Investment Management as one of your industries.'
						},
						change: (event: any) => {
							const queryObj = event;
							this.dataIndustries = [];

							const selected = this.basicModel['industries'];
							///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
							if(selected && selected.length) {
								this.selectedValue = selected.map(item => item.id);
							}
							if(queryObj.query)
							{
								this.filterService.getRelatedDataIndustry(queryObj.query).subscribe(d => {
									const listItems = d ? d['hits']['hits'] : [];
									for (let i = 0; i < listItems.length; i++) {
										const item = listItems[i]._source;
										if (listItems[i]._source.dataIndustry !== '' && listItems[i]._source.dataIndustry !== 'DATA_INDUSTRY_NULL') {
											if (!this.selectedValue.includes(item.id)) {
												this.dataIndustries.push({
													id: item.id,
													desc: item.dataIndustry
												});
											}
										}
									}
									//console.log(this.dataIndustries);
									setTimeout(() => {
										this.categorizationsFields[2].templateOptions.options = this.dataIndustries;
									}, 2500);
								});
						}
						else
						{
							this.filterService.getIndustriesForFilter().subscribe(data => {
								//	const listItems = d ? d['hits']['hits'] : [];
									const listItems = (data['aggregations'] && data['aggregations'].industries) ? data['aggregations'].industries.buckets : [];

									for (let i = 0; i < listItems.length; ++i) {
										const item = listItems[i];
										const industryId = parseInt(item.key, 10);
										const industryList = item.industries.hits.hits[0]._source.industries;
										const filteredIndustryDescription = industryList.filter( (items) => items.id === item.key);
										if (item.industries.hits.hits[0]._source.industries !== '' && item.industries.hits.hits[0]._source.industries !== 'DATA_INDUSTRY_NULL') {
											if (!this.selectedValue.includes(industryId)) {
												this.dataIndustries.push({
													id: industryId,
													desc: filteredIndustryDescription && filteredIndustryDescription[0] && filteredIndustryDescription[0].desc ? filteredIndustryDescription[0].desc : ''
												});
											}
										}
									}
									setTimeout(() => {
										this.categorizationsFields[2].templateOptions.options = this.dataIndustries;
									}, 2500);
								});

							this.inputChanges(event);
						}
						},
                        blur: (event: any) => {
                            if (event && event.target && !event['selectedVal']) {
                                event.target.value = '';
                            }
                        }
					},
				},
				{
					key: 'mainDataCategory',
					type: 'autocomplete',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Main Data Category:',
						placeholder: '',
						description: 'Which data category do the majority of your data offerings fit into? Which classification best describes the main type of data/information/research you collect/provide/analyze?',
						required: true,
						options: this.mainDataCategories,
						attributes: {
							field: 'datacategory',
							'dropdown': 'true',
							'placeholderText' : 'Sometimes you will belong in more than one category - please choose the primary data category, and add all other categories in the following question.',
							'labelTooltip': 'Data Categories are meant to be an overarching classification that can encompass several Data Features and are meant to generally describe what the data is or can be used to understand more deeply.',
                            'descriptionTooltip': 'Data Categories are meant to be an overarching classification that can encompass several Data Features and are meant to generally describe what the data is or can be used to understand more deeply.',
                            'placeholderTooltip': 'Data Categories are meant to be an overarching classification that can encompass several Data Features and are meant to generally describe what the data is or can be used to understand more deeply.'
						},
						change: (event: any) => {
							const queryObj = event;
							this.mainDataCategories = [];
							//if(queryObj.query) {
								this.filterService.getAllDataCategories(queryObj.query).subscribe(d => {
									const listItems = d && d['hits'] ? d['hits']['hits'] : [];
									for (let i = 0; i < listItems.length; ++i) {
										const item = listItems[i]._source;
										if (listItems[i]._source.datacategory !== '' && listItems[i]._source.datacategory !== 'DATA_CATEGORY_NULL') {
											this.mainDataCategories.push({
												id: item.id,
												datacategory: item.datacategory,
												img: item.logo ? item.logo.logo : null
											});
										}
									}
									setTimeout(() => {
										this.categorizationsFields[3].templateOptions.options = this.mainDataCategories;
									}, 2500);
								});
							//}
                            this.inputChanges(event);
						},
                        blur: (event: any) => {
                            if (event && event.target && !event['selectedVal']) {
                                event.target.value = '';
                            }
                        }
					},
				},
				{
					key: 'categories',
					type: 'autocomplete',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Data Categories:',
						placeholder: '',
						description: 'What are all the data categories your data offerings fit into, aside from your main data category? Which classifications describe all of the types of data/information/research you collect/provide/analyze?',
						options: this.dataCategories,
						attributes: {
							field: 'desc',
							'placeholderText' : 'All the data categories your business collects/provides/analyzes.',
							'labelTooltip': 'Data Categories are meant to be an overarching classification that can encompass several Data Features and are meant to generally describe what the data is or can be used to understand more deeply.',
							'descriptionTooltip': 'Data Categories are meant to be an overarching classification that can encompass several Data Features and are meant to generally describe what the data is or can be used to understand more deeply.',
							'multiple': 'true',
							'template': 'true',
                            'placeholderTooltip': 'Data Categories are meant to be an overarching classification that can encompass several Data Features and are meant to generally describe what the data is or can be used to understand more deeply.'
						},
						change: (event: any) => {
							const queryObj = event;
							this.dataCategories = [];

							const selected = this.basicModel['categories'];
							///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
							if(selected && selected.length) {
								this.selectedValue = selected.map(item => item.id);
							}

							if(queryObj.query) {
								this.filterService.getAllDataCategories(queryObj.query).subscribe(d => {
									const listItems = d ? d['hits']['hits'] : [];
									for (let i = 0; i < listItems.length; i++) {
										const item = listItems[i]._source;
										if (listItems[i]._source.datacategory !== '' && listItems[i]._source.datacategory !== 'DATA_CATEGORY_NULL') {
											if (!this.selectedValue.includes(item.id)) {
												this.dataCategories.push({
													id: item.id,
													desc: item.datacategory,
													img: item.logo ? item.logo.logo : null
												});
											}
										}
									}
									setTimeout(() => {
										this.categorizationsFields[4].templateOptions.options = this.dataCategories;
									}, 2500);
								});
							}
                            this.inputChanges(event);
						},
                        blur: (event: any) => {
                            if (event && event.target && !event['selectedVal']) {
                                event.target.value = '';
                            }
                        }
					},
				},
				{
					key: 'features',
					type: 'autocomplete',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Data Features:',
						placeholder: '',
						description: 'What is the specific data, research or information you are collecting, providing, or analyzing?',
						options: this.dataFeatures,
						attributes: {
							field: 'desc',
							'placeholderText' : 'A more detailed/granular version of a data category.',
							'labelTooltip': 'If you think of a table or spreadsheet or any structured data (data with cleanly organized columns and rows), a data feature usually fits into one or a combination of only a few columns. Example: ""Store Location"" would be one or a few fields describing the address of a store (address, city, state, zip, etc.). ""Product Ratings"" is likely one columns with the number of stars a consumer rates a product, but it could also be a count of the number of ratings each product has. The important thing is that the provider possesses information about product ratings.',
							'descriptionTooltip': 'If you think of a table or spreadsheet or any structured data (data with cleanly organized columns and rows), a data feature usually fits into one or a combination of only a few columns.  Example: ""Store Location"" would be one or a few fields describing the address of a store (address, city, state, zip, etc.). ""Product Ratings"" is likely one columns with the number of stars a consumer rates a product, but it could also be a count of the number of ratings each product has. The important thing is that the provider possesses information about product ratings.',
							'multiple': 'true',
                            'placeholderTooltip': 'If you think of a table or spreadsheet or any structured data (data with cleanly organized columns and rows), a data feature usually fits into one or a combination of only a few columns. Example: ""Store Location"" would be one or a few fields describing the address of a store (address, city, state, zip, etc.). ""Product Ratings"" is likely one columns with the number of stars a consumer rates a product, but it could also be a count of the number of ratings each product has. The important thing is that the provider possesses information about product ratings.'
						},
						change: (event: any) => {
							const queryObj = event;
							this.dataFeatures = [];

							const selected = this.basicModel['features'];
							///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
							if(selected && selected.length) {
								this.selectedValue = selected.map(item => item.id);
							}
                            /* if(queryObj.query) {
								this.filterService.getFeaturesForFilter().subscribe(data => {
									//const listArray = [];
									//const listItems = d ? d['hits']['hits'] : [];
									const listItems = (data['aggregations'] && data['aggregations'].features) ? data['aggregations'].features.buckets : [];
									//this.dataFeatures = listItems;
									for (let i = 0; i < listItems.length; i++) {
										const feature = listItems[i];
										const featureId = parseInt(feature.key, 10);
										const featureList = feature.features.hits.hits[0]._source.features;
										const filteredFeatureDescription = featureList.filter( (item) => item.id === feature.key);
										//const item = listItems[i]._source;
										if (!this.selectedValue.includes(featureId)) {
											this.dataFeatures.push({
												id: featureId,
												dataFeature: featureList.dataFeature,
												desc: filteredFeatureDescription && filteredFeatureDescription[0] && filteredFeatureDescription[0].desc ? filteredFeatureDescription[0].desc : '',
											});
										}
									}
									setTimeout(() => {
										this.categorizationsFields[5].templateOptions.options = this.dataFeatures;
									}, 2500);
								});
							} */
							if(queryObj.query) {
								this.filterService.getDataFeatures(queryObj.query).subscribe(d => {
									const listArray = [];
									const listItems = d ? d['hits']['hits'] : [];
									this.dataFeatures = listItems;
									for (let i = 0; i < listItems.length; i++) {
										const item = listItems[i]._source;
										if (!this.selectedValue.includes(item.id)) {
											listArray.push({
												id: item.id,
												dataFeature: item.dataFeature,
												desc: item.dataFeature
											});
										}
									}
									this.dataFeatures = [...listArray];
									setTimeout(() => {
										this.categorizationsFields[5].templateOptions.options = this.dataFeatures;
									}, 2500);
								});
						    }
                            this.inputChanges(event);
						},
                        blur: (event: any) => {
                            if (event && event.target && !event['selectedVal']) {
                                event.target.value = '';
                            }
                        }
					},
				},
				{
					key: 'sources',
					type: 'multiselect',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Data Sources:',
						placeholder: '',
						description: 'What is the original source of your data?',
						options: this.dataSources,
						attributes: {
							field: 'desc',
							'placeholderText' : 'The original (physical) source of your data, not to be confused with external Provider Partners (the companies you receive data from).',
							'labelTooltip': 'This is meant to be a more generic indicator of where the data came from, so not the external provider of the data, but the original type of data producer.',
							'descriptionTooltip': 'This is meant to be a more generic indicator of where the data came from, so not the external provider of the data, but the original type of data producer.',
                            'placeholderTooltip': 'This is meant to be a more generic indicator of where the data came from, so not the external provider of the data, but the original type of data producer.',
                            'lookupmodule':'BIASES'
                        },
                        change: this.inputChanges,
                       // keypress: this.addLookupCode
					},
				},
				{
					key: 'numberOfDataSources.id',
					type: 'dropdown',
					className: 'customSmallWidth-dropdown',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Approximate Number of Data Sources:',
						description: 'From approximately how many unique sources do you receive/gather data from?',
						options: this.providerNumberOfDataSources,
						attributes: {
							field: 'label',
                        },
                        change: this.inputChanges
					},
				},
				{
					key: 'dataSourcesDetails',
					type: 'textarea',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Data Sources Details:',
						placeholder: '',
                        description: 'Please provide more details on exactly where and how you acquired your data.',
                        attributes: {
							'trackNames': 'Data Sources Details',
                            'trackCategory': 'Provider Profile Form - Textbox',
                            'trackLabel': 'Provider Profile Form - Input',
                            'maximumLength' : 255
						},
                        change: this.basicInput
					},
				},
				{
					key: 'geographicalFoci',
					type: 'multiselect',
					className: 'custom-multiselect',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Geographical Focus:',
						placeholder: '',
						description: 'Which geographies do your data offerings focus on?',
						required: true,
						options: this.geographicalFocus,
						attributes: {
							'placeholderText' : 'If you cover numerous countries across continents, then add "Global". If you only cover one continent, a few countries or a specific region, then select those.',
							'labelTooltip': 'Please note, this is not the location of your company.',
                            'descriptionTooltip': 'Please note, this is not the location of your company.',
                            'placeholderTooltip': 'Please note, this is not the location of your company.',
                            'lookupmodule':'GEOGRAPHICAL_FOCUS'
                        },
                        change: this.inputChanges
                    },
                    validation: {
                        messages: {
                            required: 'Geographical Focus is required'
                        }
                    }
				},
				 {
					key: 'providerTags',
					type: 'multiselect',
					className: 'custom-multiselect',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Tags:',
						placeholder: '',
						description: 'Please choose any of these tags if any apply to your data offerings.',
						options: this.providerTags,
						attributes: {
							field: 'id',
                            'placeholderText' : 'Particular attributes that differentiate your offerings.'
                        },
                        change: this.inputChanges,
                      //  keypress: this.addLookupCode
					},
				},
			];
			this.companyDetailsModel = this.dataProvider;
		});

		this.deliveryMethods = [];
		this.filterService.getDeliveryMethods().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.deliveryMethods.push({
					value: {
						id: listItems[i]._source.id,
						 desc: listItems[i]._source.lookupcode,
						// locked: listItems[i]._source.locked ? listItems[i]._source.locked : false
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDeliveryFields[0].templateOptions.options = this.deliveryMethods;
		});

		this.deliveryFormats = [];
		this.filterService.getDeliveryFormat().subscribe(data => {
			const listItems = data && data['hits'] && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.deliveryFormats.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode,
						// locked: listItems[i]._source.locked ? listItems[i]._source.locked : false
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDeliveryFields[1].templateOptions.options = this.deliveryFormats;
		});

		this.deliveryFrequencies = [];
		this.filterService.deliveryFrequency().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.deliveryFrequencies.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDeliveryFields[2].templateOptions.options = this.deliveryFrequencies;
		});

		this.dataUpdateFrequency = [];
		this.filterService.getLookupCodes('DATA_UPDATE_FREQUENCY').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			// this.dataUpdateFrequency.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.dataUpdateFrequency.push({
					value: listItems[i]._source.lookupcode,
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDeliveryFields[5].templateOptions.options = this.dataUpdateFrequency;
		});

		this.providerPricingModel = [];
		this.filterService.getLookupCodes('PRICING_MODEL').subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerPricingModel.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			/* const pricingModel = data ? data['hits']['hits'] : [];
			pricingModel.map((val, index) => {
                //console.log('......pointer mapping', val);
				this.providerPricingModel.push({
					value: val._source.id,
					label: val._source.description
                });
			}); */
			this.pricingFields[0].templateOptions.options = this.providerPricingModel;
		});

		/* this.pricingModel = [];
		this.filterService.getPricingModel().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.pricingModel.push({
					id: listItems[i]._source.id,
					desc: listItems[i]._source.description,
					locked: listItems[i]._source.locked ? true : false
				});
			}
			this.pricingFields[0].templateOptions.options = this.pricingModel;
		}); */
	}

	ngAfterViewInit() {
        const uppy = this.uppyService.uppy;
        setTimeout(() => {
            const inputFields = this.el.nativeElement.querySelectorAll('input, textarea');
            const totalFields = inputFields.length;
            let totalFieldsFilled = 0;
            inputFields.forEach(element => {
                if (element.value !== '') {
                    totalFieldsFilled = totalFieldsFilled + 1;
                }
            });
            totalFieldsFilled = totalFieldsFilled;
            const completenessPercentage = Math.round((totalFieldsFilled * 100) / totalFields);
            _that.completeness = completenessPercentage;
        }, 5000);
	}

	getLinkImgUrl(desc) {
		if (desc) {
			const fileName = ((desc).toLowerCase()).replace(' ', '_');
			const link = 'content/images/' + fileName  + '.png';
			return link;
		}
	}

	providerProfileTab(event: MatTabChangeEvent) {
        /*  this.profileTabLabel = 'Provider Profile - Tab ' + event.tab.textLabel;
		this.ProfileCategory = 'Provider Profile ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent(this.ProfileCategory, 'Tab Link Clicked', this.profileTabLabel); */
	}

	providerFollow(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	unlockProviders(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	requestMoreInfo(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	providerPartnerTab(event: MatTabChangeEvent) {
		this.profileTabLabel = 'Provider Profile -  ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent('Partner Provider', 'Internal Link Clicked', this.profileTabLabel);
	}

	dataFieldLink(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	profileWebsite(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	profileArticle(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
	samplesTab(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	load(recordID) {
		this.dataProviderService.findByIdEditForm(recordID).subscribe(dataProvider => {
			this.loaderService.display(false);
			this.dataProvider = dataProvider['hits'].total ? dataProvider['hits'].hits[0]._source : null;
			this.dataProviderID = this.dataProvider ? this.dataProvider.id : null;
            this.completeness = this.dataProvider.completeness;
            this.isLoading = false;
			this.constructFormModel(this.dataProvider);
			this.companyDetailsModel = this.dataProvider;

			this.filterService.findByResourceId(this.dataProviderID).subscribe(data => {
                if(data['hits'] && data['hits'].hits) {
                    for (let i = 0; i < data['hits'].hits.length; i++) {
                        const marketingResourceObj = data['hits'].hits[i]._source;
                        this.marketingLiteratureResource.push(marketingResourceObj);
                        // if (this.resourceObj.link !== null) {
                        // 	this.resourceLink.push(this.resourceObj.link);
                        // }
                        // console.log('Link', this.resourceLink);
                    }
                }
					this.marketingLiteratureResource.forEach((item,index) => {
					//	this.itemID = item.id;
					//	console.log('Link', item.link);
						const obj = item;
						if(obj && obj.purposes[0] && obj.purposes[0].purpose === 'Description & Details') {
							if(obj.link) {
								obj.link['resourceID'] = obj.id;
								obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
								this.marketingLiteratureList.push(obj.link);
							}
							if(obj.file) {
								obj.file['resourceID'] = obj.id;
								obj.file['filesName'] = obj.file && obj.file.fileName ? obj.file.fileName : null;
								this.marketingLiteratureList.push(obj.file);
							}
						}
						if(obj && obj.purposes[0] && obj.purposes[0].purpose === 'Documentation or Data Dictionary') {
							if(obj.link) {
								obj.link['resourceID'] = obj.id;
								obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
								this.dataDictionaryList.push(obj.link);
							}
							if(obj.file) {
								obj.file['resourceID'] = obj.id;
								obj.file['filesName'] = obj.file && obj.file.fileName ? obj.file.fileName : null;
								this.dataDictionaryList.push(obj.file);
							}
						}
						if(obj && obj.purposes[0] && obj.purposes[0].purpose === 'Snapshot') {
							if(obj.link) {
								obj.link['resourceID'] = obj.id;
								obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
								this.samplesAndExamplesList.push(obj.link);
							}
							if(obj.file) {
								obj.file['resourceID'] = obj.id;
								obj.file['filesName'] = obj.file && obj.file.fileName ? obj.file.fileName : null;
								this.samplesAndExamplesList.push(obj.file);
							}
						}
						if(obj && obj.purposes[0] && obj.purposes[0].purpose === 'Research & Analysis') {
							if(obj.link) {
								obj.link['resourceID'] = obj.id;
								obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
								this.reportsAnalysesList.push(obj.link);
							}
							if(obj.file) {
								obj.file['resourceID'] = obj.id;
								obj.file['filesName'] = obj.file && obj.file.fileName ? obj.file.fileName : null;
								this.reportsAnalysesList.push(obj.file);
							}
					    }
					});
					// console.log('Data Relevant Links', this.dataRelevantLinks);
					// console.log('Data Relevant Files', this.dataRelevantFiles);
				// 	this.resourceModel = this.dataResources;
			});
            // console.log('dateCollectionBegan', this.dataProvider.dateCollectionBegan);
            if(this.dataProvider.dateCollectionBegan) {
                const setDate = new Date(this.dataProvider.dateCollectionBegan);
                delete this.dataProvider.dateCollectionBegan;
                this.dataProvider.dateCollectionBegan = this.datePipe.transform(setDate,'MM/dd/yyyy');;
                console.log('setDate', setDate, this.dataProvider.dateCollectionBegan);
            }

            if (this.dataProvider.providerPartners) {
                this.providerPartnersCount = this.dataProvider.providerPartners.length;
				//this.relatedProvidersFields[0].templateOptions.label = 'Provider Data Partners ('+this.providerPartnersCount+'):';
			//	this.relatedProvidersFields[0].templateOptions.label = 'Provider Data Partners';
				const providerPartners = [];
				for (const obj in this.dataProvider.providerPartners) {
					if (obj) {
						const partnerName = !this.dataProvider.providerPartners[obj].locked ? this.dataProvider.providerPartners[obj].providerName : 'Provider Locked';
						providerPartners.push({
							id: this.dataProvider.providerPartners[obj].id,
							providerName: partnerName
						});
					}
				}
				delete this.dataProvider.providerPartners;
				this.dataProvider.providerPartners = providerPartners;
            }

            if (this.dataProvider.distributionPartners) {
                this.distributionPartnersCount = this.dataProvider.distributionPartners.length;
				//this.relatedProvidersFields[3].templateOptions.label = 'Distribution Data Partners ('+this.distributionPartnersCount+'):';
				//this.relatedProvidersFields[3].templateOptions.label = 'Distribution Data Partners';
				const distributionPartners = [];
				for (const obj in this.dataProvider.distributionPartners) {
					if (obj) {
						const partnerName = !this.dataProvider.distributionPartners[obj].locked ? this.dataProvider.distributionPartners[obj].providerName : 'Provider Locked';
						distributionPartners.push({
							id: this.dataProvider.distributionPartners[obj].id,
							providerName: partnerName
						});
					}
				}
				delete this.dataProvider.distributionPartners;
				this.dataProvider.distributionPartners = distributionPartners;
            }

            if (this.dataProvider.consumerPartners) {
                this.consumerPartnersCount = this.dataProvider.consumerPartners.length;
			//	this.relatedProvidersFields[6].templateOptions.label = 'Consumer Data Partners ('+this.consumerPartnersCount+'):';
			//	this.relatedProvidersFields[6].templateOptions.label = 'Consumer Data Partners';
				const consumerPartners = [];
				for (const obj in this.dataProvider.consumerPartners) {
					if (obj) {
						const partnerName = !this.dataProvider.consumerPartners[obj].locked ? this.dataProvider.consumerPartners[obj].providerName : 'Provider Locked';
						consumerPartners.push({
							id: this.dataProvider.consumerPartners[obj].id,
							providerName: partnerName
						});
					}
				}
				delete this.dataProvider.consumerPartners;
				this.dataProvider.consumerPartners = consumerPartners;
            }

            if (this.dataProvider.complementaryProviders) {
                this.complementaryProvidersCount = this.dataProvider.complementaryProviders.length;
			//	this.relatedProvidersFields[9].templateOptions.label = 'Complementary Providers ('+this.complementaryProvidersCount+'):';
				//this.relatedProvidersFields[9].templateOptions.label = 'Complementary Providers';
				const complementaryProviders = [];
				for (const obj in this.dataProvider.complementaryProviders) {
					if (obj) {
						const partnerName = !this.dataProvider.complementaryProviders[obj].locked ? this.dataProvider.complementaryProviders[obj].providerName : 'Provider Locked';
						complementaryProviders.push({
							id: this.dataProvider.complementaryProviders[obj].id,
							providerName: partnerName
						});
					}
				}
				delete this.dataProvider.complementaryProviders;
				this.dataProvider.complementaryProviders = complementaryProviders;
            }

            if (this.dataProvider.competitors) {
                this.competitorsCount = this.dataProvider.competitors.length;
				//this.relatedProvidersFields[12].templateOptions.label = 'Competitors ('+this.competitorsCount+'):';
				//this.relatedProvidersFields[12].templateOptions.label = 'Competitors';
				const competitors = [];
				for (const obj in this.dataProvider.competitors) {
					if (obj) {
						const partnerName = !this.dataProvider.competitors[obj].locked ? this.dataProvider.competitors[obj].providerName : 'Provider Locked';
						competitors.push({
							id: this.dataProvider.competitors[obj].id,
							providerName: partnerName
						});
					}
				}
				delete this.dataProvider.competitors;
				this.dataProvider.competitors = competitors;
            }

			if (this.dataProvider.geographicalFoci) {
				const geographicalFoci = [];
				for (const obj in this.dataProvider.geographicalFoci) {
					if (obj) {
						geographicalFoci.push({
							id: this.dataProvider.geographicalFoci[obj].id,
							desc: this.dataProvider.geographicalFoci[obj].description
						});
					}
				}
				delete this.dataProvider.geographicalFoci;
				this.dataProvider.geographicalFoci = geographicalFoci;
			}
			if (this.dataProvider.providerTags) {
				const providerTags = [];
				/* for (const obj in this.dataProvider.providerTags) {
					if (obj) {
						//console.log('Tags', this.dataProvider.providerTags[obj]);
						//const providerTagsId = parseInt(this.dataProvider.providerTags[obj]['id'],10);
                        //console.log('Tags 1', providerTagsId);
						providerTags.push({
							id:  this.dataProvider.providerTags[obj].id,
							desc: this.dataProvider.providerTags[obj].description
						});
					}
				} */
				delete this.dataProvider.providerTags;
				this.dataProvider.providerTags = providerTags;
				//console.log('tags',this.dataProvider.providerTags);
			}

            if (this.dataProvider.deliveryMethods) {
                // console.log('Methods Response',this.dataProvider.deliveryMethods);
                const deliveryMethods = [];
                for (const obj in this.dataProvider.deliveryMethods) {
                    const deliveryMethodsId = parseInt(this.dataProvider.deliveryMethods[obj]['id'],10);
                    // console.log('Methods Id', deliveryMethodsId);
                    if (obj) {
                        // console.log(obj);
                        deliveryMethods.push({
                            id: deliveryMethodsId,
                            desc: this.dataProvider.deliveryMethods[obj]['desc']
                        });
                    }
                }
                delete this.dataProvider.deliveryMethods;
                // console.log('Delivery Methods', deliveryMethods);
                this.dataProvider.deliveryMethods = deliveryMethods;
            }

            if (this.dataProvider.deliveryFormats) {
                // console.log('Formats Response',this.dataProvider.deliveryFormats);
                const deliveryFormats = [];
                for (const obj in this.dataProvider.deliveryFormats) {
                    const deliveryFormatsId = parseInt(this.dataProvider.deliveryFormats[obj]['id'],10);
                    // console.log('Formats Id', deliveryFormatsId);
                    if (obj) {
                        // console.log(obj);
                        deliveryFormats.push({
                            id: deliveryFormatsId,
                            desc: this.dataProvider.deliveryFormats[obj]['desc']
                        });
                    }
                }
                delete this.dataProvider.deliveryFormats;
                // console.log('Delivery Formats', deliveryFormats);
                this.dataProvider.deliveryFormats = deliveryFormats;
            }
			if (this.dataProvider.deliveryFrequencies) {
				const deliveryFrequencies = [];
				for (const obj in this.dataProvider.deliveryFrequencies) {
					if (obj) {
						deliveryFrequencies.push({
							id: this.dataProvider.deliveryFrequencies[obj].id,
							desc: this.dataProvider.deliveryFrequencies[obj].lookupcode
						});
					}
				}
				delete this.dataProvider.deliveryFrequencies;
				this.dataProvider.deliveryFrequencies = deliveryFrequencies;
			}
			/* if (this.dataProvider.pricingModels) {
				const pricingModels = [];
				for (const obj in this.dataProvider.pricingModels) {
                    const pricingModelsId = parseInt(this.dataProvider.pricingModels[obj]['id'],10);
                    console.log('Pricing 1', pricingModelsId)
					if (obj) {
                        console.log('Pricing 2',this.dataProvider.pricingModels);
						pricingModels.push({
							id: pricingModelsId,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ,
							desc: this.dataProvider.pricingModels[obj].desc
						});
					}
				}
				delete this.dataProvider.pricingModels;
                console.log('Pricing 3', pricingModels);
				this.dataProvider.dataProvidersPricingModels = pricingModels;
			} */
			if (this.dataProvider.investorTypes) {
				const investorTypes = [];
				for (const obj in this.dataProvider.investorTypes) {
					if (obj) {
						investorTypes.push({
							id: this.dataProvider.investorTypes[obj].id,
							desc: this.dataProvider.investorTypes[obj].description
						});
					}
				}
				delete this.dataProvider.investorTypes;
				this.dataProvider.investorTypes = investorTypes;
			}

            if (this.dataProvider.assetClasses) {
                // console.log('Formats Response',this.dataProvider.assetClasses);
                const assetClasses = [];
                for (const obj in this.dataProvider.assetClasses) {
                    const assetClassesId = parseInt(this.dataProvider.assetClasses[obj]['id'],10);
                    // console.log('Formats Id', assetClassesId);
                    if (obj) {
                        // console.log(obj);
                        assetClasses.push({
                            id: assetClassesId,
                            desc: this.dataProvider.assetClasses[obj]['desc']
                        });
                    }
                }
                delete this.dataProvider.assetClasses;
                // console.log('Delivery Formats', assetClasses);
                this.dataProvider.assetClasses = assetClasses;
            }

            if (this.dataProvider.securityTypes) {
                // console.log('Formats Response',this.dataProvider.securityTypes);
                const securityTypes = [];
                for (const obj in this.dataProvider.securityTypes) {
                    const securityTypesId = parseInt(this.dataProvider.securityTypes[obj]['id'],10);
                    // console.log('Formats Id', securityTypesId);
                    if (obj) {
                        // console.log(obj);
                        securityTypes.push({
                            id: securityTypesId,
                            desc: this.dataProvider.securityTypes[obj]['desc']
                        });
                    }
                }
                delete this.dataProvider.securityTypes;
                // console.log('Delivery Formats', securityTypes);
                this.dataProvider.securityTypes = securityTypes;
            }

            if (this.dataProvider.relevantSectors) {
                // console.log('Formats Response',this.dataProvider.relevantSectors);
                const relevantSectors = [];
                for (const obj in this.dataProvider.relevantSectors) {
                    const relevantSectorsId = parseInt(this.dataProvider.relevantSectors[obj]['id'],10);
                    // console.log('Formats Id', relevantSectorsId);
                    if (obj) {
                        // console.log(obj);
                        relevantSectors.push({
                            id: relevantSectorsId,
                            desc: this.dataProvider.relevantSectors[obj]['desc']
                        });
                    }
                }
                delete this.dataProvider.relevantSectors;
                // console.log('Delivery Formats', relevantSectors);
                this.dataProvider.relevantSectors = relevantSectors;
            }

			if (this.dataProvider.accessOffered) {
				const accessOffered = [];
				for (const obj in this.dataProvider.accessOffered) {
					if (obj) {
						accessOffered.push({
							id: this.dataProvider.accessOffered[obj].id,
							desc: this.dataProvider.accessOffered[obj].description
						});
					}
				}
				delete this.dataProvider.accessOffered;
				this.dataProvider.accessOffered = accessOffered;
			}
			if (this.dataProvider.dataProductTypes) {
				const dataProductTypes = [];
				for (const obj in this.dataProvider.dataProductTypes) {
					if (obj) {
						dataProductTypes.push({
							id: this.dataProvider.dataProductTypes[obj].id,
							desc: this.dataProvider.dataProductTypes[obj].description
						});
					}
				}
				delete this.dataProvider.dataProductTypes;
				this.dataProvider.dataProductTypes = dataProductTypes;
			}
			if (this.dataProvider.dataTypes) {
				const dataTypes = [];
				for (const obj in this.dataProvider.dataTypes) {
					if (obj) {
						dataTypes.push({
							id: this.dataProvider.dataTypes[obj].id,
							desc: this.dataProvider.dataTypes[obj].description
						});
					}
				}
				delete this.dataProvider.dataTypes;
				this.dataProvider.dataTypes = dataTypes;
			}
			if (this.dataProvider.languagesAvailable) {
				const languagesAvailable = [];
				for (const obj in this.dataProvider.languagesAvailable) {
					if (obj) {
						languagesAvailable.push({
							id: this.dataProvider.languagesAvailable[obj].id,
							desc: this.dataProvider.languagesAvailable[obj].description
						});
					}
				}
				delete this.dataProvider.languagesAvailable;
				this.dataProvider.languagesAvailable = languagesAvailable;
			}
			if (this.dataProvider.organizationType) {
				const organizationType = [];
				for (const obj in this.dataProvider.organizationType) {
					if (obj) {
						organizationType.push({
							id: this.dataProvider.organizationType[obj].id,
							desc: this.dataProvider.organizationType[obj].description
						});
					}
				}
				delete this.dataProvider.organizationType;
				this.dataProvider.organizationType = organizationType;
			}
			if (this.dataProvider.managerType) {
				const managerType = [];
				for (const obj in this.dataProvider.managerType) {
					if (obj) {
						managerType.push({
							id: this.dataProvider.managerType[obj].id,
							desc: this.dataProvider.managerType[obj].description
						});
					}
				}
				delete this.dataProvider.managerType;
				this.dataProvider.managerType = managerType;
			}
			if (this.dataProvider.strategies) {
				const strategies = [];
				for (const obj in this.dataProvider.strategies) {
					if (obj) {
						strategies.push({
							id: this.dataProvider.strategies[obj].id,
							desc: this.dataProvider.strategies[obj].description
						});
					}
				}
				delete this.dataProvider.strategies;
				this.dataProvider.strategies = strategies;
			}
			if (this.dataProvider.researchStyles) {
				const researchStyles = [];
				for (const obj in this.dataProvider.researchStyles) {
					if (obj) {
						researchStyles.push({
							id: this.dataProvider.researchStyles[obj].id,
							desc: this.dataProvider.researchStyles[obj].description
						});
					}
				}
				delete this.dataProvider.researchStyles;
				this.dataProvider.researchStyles = researchStyles;
			}
			if (this.dataProvider.identifiersAvailable) {
				const identifiersAvailable = [];
				for (const obj in this.dataProvider.identifiersAvailable) {
					if (obj) {
						identifiersAvailable.push({
							id: this.dataProvider.identifiersAvailable[obj].id,
							desc: this.dataProvider.identifiersAvailable[obj].description
						});
					}
				}
				delete this.dataProvider.identifiersAvailable;
				this.dataProvider.identifiersAvailable = identifiersAvailable;
			}
			if (this.dataProvider.investingTimeFrame) {
				const investingTimeFrame = [];
				for (const obj in this.dataProvider.investingTimeFrame) {
					if (obj) {
						investingTimeFrame.push({
							id: this.dataProvider.investingTimeFrame[obj].id,
							desc: this.dataProvider.investingTimeFrame[obj].description
						});
					}
				}
				delete this.dataProvider.investingTimeFrame;
				this.dataProvider.investingTimeFrame = investingTimeFrame;
			}
			if (this.dataProvider.equitiesMarketCap) {
				const equitiesMarketCap = [];
				for (const obj in this.dataProvider.equitiesMarketCap) {
					if (obj) {
						equitiesMarketCap.push({
							id: this.dataProvider.equitiesMarketCap[obj].id,
							desc: this.dataProvider.equitiesMarketCap[obj].description
						});
					}
				}
				delete this.dataProvider.equitiesMarketCap;
				this.dataProvider.equitiesMarketCap = equitiesMarketCap;
			}
			if (this.dataProvider.equitiesStyle) {
				const equitiesStyle = [];
				for (const obj in this.dataProvider.equitiesStyle) {
					if (obj) {
						equitiesStyle.push({
							id: this.dataProvider.equitiesStyle[obj].id,
							desc: this.dataProvider.equitiesStyle[obj].description
						});
					}
				}
				delete this.dataProvider.equitiesStyle;
				this.dataProvider.equitiesStyle = equitiesStyle;
			}
			if (this.dataProvider.dataGapsReasons) {
				const dataGapsReasons = [];
				for (const obj in this.dataProvider.dataGapsReasons) {
					if (obj) {
						dataGapsReasons.push({
							id: this.dataProvider.dataGapsReasons[obj].id,
							desc: this.dataProvider.dataGapsReasons[obj].lookupcode
						});
					}
				}
				delete this.dataProvider.dataGapsReasons;
				this.dataProvider.dataGapsReasons = dataGapsReasons;
			}
			if (this.dataProvider.outlierReasons) {
				const outlierReasons = [];
				for (const obj in this.dataProvider.outlierReasons) {
					if (obj) {
						outlierReasons.push({
							id: this.dataProvider.outlierReasons[obj].id,
							desc: this.dataProvider.outlierReasons[obj].lookupcode
						});
					}
				}
				delete this.dataProvider.outlierReasons;
				this.dataProvider.outlierReasons = outlierReasons;
			}
			if (this.dataProvider.biases) {
				const biases = [];
				for (const obj in this.dataProvider.biases) {
					if (obj) {
						biases.push({
							id: this.dataProvider.biases[obj].id,
							desc: this.dataProvider.biases[obj].lookupcode
						});
					}
				}
				delete this.dataProvider.biases;
				this.dataProvider.biases = biases;
			}
			if (this.dataProvider.paymentMethodsOffered) {
				const paymentMethodsOffered = [];
				for (const obj in this.dataProvider.paymentMethodsOffered) {
					if (obj) {
						paymentMethodsOffered.push({
							id: this.dataProvider.paymentMethodsOffered[obj].id,
							desc: this.dataProvider.paymentMethodsOffered[obj].description
						});
					}
				}
				delete this.dataProvider.paymentMethodsOffered;
				this.dataProvider.paymentMethodsOffered = paymentMethodsOffered;
			}
			// if (this.dataProvider.dataUpdateFrequency) {
			// 	const updateFrequency = {};
			// 	updateFrequency['id'] = this.dataProvider.dataUpdateFrequency.id;
			// 	updateFrequency['lookupcode'] = this.dataProvider.dataUpdateFrequency.lookupcode,
			// 	updateFrequency['recordCount'] = this.dataProvider.dataUpdateFrequency.recordCount;
			// 	delete this.dataProvider.dataUpdateFrequency;
			// 	this.dataProvider.dataUpdateFrequency = updateFrequency;
			// }
			if (this.dataProvider.pricingModel) {
				/* const pricingModel = {};
				delete this.dataProvider.pricingModel;
				this.dataProvider.pricingModel = pricingModel[0]; */
			}
			this.basicModel = this.dataProvider;

		}, error => {
			if (error.status === 401) {
                this.stateService.storeUrl(null);
                this.router.navigate(['']);
				this.signinModalService.openDialog('Session expired! Please re-login.');
			}
			this.dataProvider = null;
			this.isLoading = false;
		});
	   // this.loaderService.display(false);
    }

    constructFormModel(provider: DataProvider) {
        this.trackname = provider && provider.providerName ? provider.providerName.length : 0;
      //  console.log(this.trackname);
        this.trackwebsite = provider && provider.website ? provider.website.length : 0;
        this.trackownerOrganizationCity = provider.ownerOrganization && provider.ownerOrganization.city ? provider.ownerOrganization.city.length : 0;
        this.trackownerOrganizationState = provider.ownerOrganization && provider.ownerOrganization.state ? provider.ownerOrganization.state.length : 0;
        this.trackownerOrganizationCountry = provider.ownerOrganization && provider.ownerOrganization.country ? provider.ownerOrganization.country.length : 0;
        this.trackownerOrganizationYearfound = provider.ownerOrganization.yearFounded;
        this.trackownerOrganizationHeadcount = provider.ownerOrganization && provider.ownerOrganization.headCount ? provider.ownerOrganization.headCount.length : 0;
        this.trackInvestor = provider.investor_clients ? provider.investor_clients.length : 0;
        this.trackNumberOfInvestor = provider.investorClientsCount ? provider.investorClientsCount : 0;
        this.trackDataCollection = provider.dateCollectionBegan ? provider.dateCollectionBegan.length : 0;
        this.trackYearFirstDataProductLaunched = provider.yearFirstDataProductLaunched ? provider.yearFirstDataProductLaunched.length : 0;
        this.trackNumberOfAnalystEmployees = provider.numberOfAnalystEmployees ? provider.numberOfAnalystEmployees.length : 0;
        this.trackDataSourcesDetails = provider.dataSourcesDetails ? provider.dataSourcesDetails.length : 0;
        this.trackCompetitiveDifferentiators = provider.competitiveDifferentiators ? provider.competitiveDifferentiators.length : 0;
        this.trackcollectionMethodsExplanation = provider.collectionMethodsExplanation? provider.collectionMethodsExplanation.length : 0;
        this.tracksampleOrPanelSize = provider.sampleOrPanelSize? provider.sampleOrPanelSize.length : 0;
        this.trackDateCollectionRangeExplanation = provider.dateCollectionRangeExplanation? provider.dateCollectionRangeExplanation.length : 0;
        this.trackOutOfSampleData = provider.outOfSampleData? provider.outOfSampleData.length: 0;
        this.trackProductDetails = provider.productDetails? provider.productDetails.length: 0;
        this.trackLagTime = provider.lagTime ? provider.lagTime.length : 0;
        this.trackDeliveryFrequencyNotes = provider.deliveryFrequencyNotes ? provider.deliveryFrequencyNotes.length : 0;
        this.trackDataUpdateFrequencyNotes = provider.dataUpdateFrequencyNotes ? provider.dataUpdateFrequencyNotes.length : 0;
        this.trackTrialDuration = provider.trialDuration ? provider.trialDuration.length : 0;
        this.trackTrialPricingDetails = provider.trialPricingDetails ? provider.trialPricingDetails.length : 0;
        this.trackDataLicenseTypeDetails = provider.dataLicenseTypeDetails? provider.dataLicenseTypeDetails.length : 0;
        this.trackMaximumYearlyPrice = provider.maximumYearlyPrice? provider.maximumYearlyPrice.length : 0;
        this.trackMinimumYearlyPrice = provider.minimumYearlyPrice? provider.minimumYearlyPrice.length : 0;
        this.trackPricingDetails = provider.pricingDetails? provider.pricingDetails.length : 0;
        this.trackSampleOrPanelBiases = provider.sampleOrPanelBiases ? provider.sampleOrPanelBiases.length :0;
        this.trackUseCasesOrQuestionsAddressed = provider.useCasesOrQuestionsAddressed? provider.useCasesOrQuestionsAddressed.length : 0;
        this.trackQualityMethods = provider.dataQualityDetails ? provider.dataQualityDetails.length : 0;
        this.trackDiscussionCleanlines = provider.discussionCleanlines? provider.discussionCleanlines.length : 0;
        this.trackDiscussionAnalytics = provider.discussionsAnalytics? provider.discussionsAnalytics.length : 0;
        this.trackPotentialDrawbacks = provider.potentialDrawbacks? provider.potentialDrawbacks.length : 0;
        this.trackDataRoadmap = provider.dataRoadmap? provider.dataRoadmap.length : 0;
        this.trackNotes = provider.notes? provider.notes.length : 0;
        this.trackShortDescription = provider.shortDescription? provider.shortDescription.length : 0;
        this.trackLongDescription = provider.longDescription? provider.longDescription.length : 0;
        this.trackpublicCompaniesCovered = provider.publicCompaniesCovered? provider.publicCompaniesCovered.length : 0;
        this.trackpublicEquitesCovered = provider.publicEquitesCovered? provider.publicEquitesCovered.length : 0;


        this.providerFormModel = {
            'recordID': provider.recordID,
            'providerName': provider.providerName,
            'website': provider.website
        };
        this.descriptionsModel = {
            'recordID': provider.recordID,
            'shortDescription': provider.shortDescription,
            'longDescription': provider.longDescription
        };
        this.companyDetailsModel = {
            'recordID': provider.recordID,
            'ownerOrganization': provider.ownerOrganization
        };
        this.categorizationsModel = {
            'recordID': provider.recordID,
            'providerType': provider.providerType,
            'mainDataCategory': provider.mainDataCategory,
            'mainDataIndustry': provider.mainDataIndustry
        };
    }

    previousState() {
        this.window.history.back();
    }

	ngOnDestroy() {
		this.onDestroy$.next();
        this.onDestroy$.complete();
        this.cookieService.put('logoutClicked', '0');
        this.unsavedSubscription.unsubscribe();
	}

	openConfirmationDialog(nextState?: RouterStateSnapshot) {
        // console.log('Next State', nextState);
        const redirect = nextState.url;
        this.dialog.closeAll();
        // console.log('trueee', this.formSubmitted, redirect);
        this.dialogRef = this.dialog.open(ConfirmDialogComponent, {
            disableClose: false
        });
        this.dialogRef.componentInstance.confirmMessage = 'You have some unsaved edits in this form.';
        this.dialogRef.componentInstance.button = {
            leave: true,
            confirm: true,
            leaveText: 'Leave and Don\'t Save',
            confirmText: 'Save and Submit'
        };

        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // do confirmation actions
                // console.log('Leave', result);
                if (result === 'leave') {
                    if (redirect === '/') {
                        this.authService.logout().subscribe();
                        this.formSubmitted = true;
                        this.principal.authenticate(null);
                        this.stateService.storeUrl(null);
                        this.router.navigate(['../../']);
                    } else {
                        this.formSubmitted = true;
                        this.navigateNextPage = true;
                        if (this.principal.isAuthenticated()) {
                            // console.log('Navigated');
                            this.stateService.storeUrl(null);
                            this.router.navigate([redirect]);
                            this.dialog.closeAll();
                        } else {
                            this.stateService.storeUrl(null);
                            this.router.navigate(['']);
                            this.dialog.closeAll();
                        }
                    }

                }

                if (result === 'save') {
                    if (redirect === '/') {
                        this.submitProviderEdit(this.basicModel);
                        this.authService.logout().subscribe();
                        this.formSubmitted = true;
                        this.principal.authenticate(null);
                        this.stateService.storeUrl(null);
                        this.router.navigate(['../../']);
                    } else {
                        this.navigateNextPage = true;
                        if (this.principal.isAuthenticated()) {
                            this.submitProviderEdit(this.basicModel);
                        } else {
                            this.formSubmitted = true;
                            this.stateService.storeUrl(null);
                            this.router.navigate([redirect]);
                        }
                    }
                }
            }
            this.dialogRef = null;
        });
        return false;
	}

	trackByIndex(index) {
		return index;
	}
	documentCheck(event)
	{
		this.dataDocument= event;
	}
	submitProviderEdit(model, panelName?: string, currentPanel?: string, nextPanel?: string, tabIndex?: number, form?: string, skipRedirect?: boolean) {
		console.log('edit called', model);
		this.overAll= panelName;
		if(panelName !=='Overall Details') {
			this.panelName = panelName;
		}
		else {
			panelName = '';
		}
        if (this.forceFormSubmit === true) {
            this.navigateNextPage = true;
        }
		const formName = panelName ? panelName + '' : 'Form';
        const submitMessage = 'Your edits have been successfully submitted. Amass will review these edits over the coming days before they appear within your profile.';
		const filterArr = Array.prototype.filter;

        if (form && this[form].valid) {
            console.log('1');
            const autoCompleteFields = this.el.nativeElement.querySelectorAll('#' + form + ' .ui-autocomplete-input');
            const filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                return node.value === '';
			});
			/* if(this.privacySecurityForm && !model.consent) {
				console.log("2");
				this.agreeValidationMessage = '* This field is required';
				this.openSnackBar('Please fill all the required fields!', 'Close');
				this.expandFormPanels();
			}  */
			if(form ==='privacySecurityForm' && !model.consent && !this.forceFormSubmit) {
                this.agreeValidationMessage = '* This field is required';
                this.openSnackBar('Please fill all the required fields!', 'Close');
            } else if(this.overAll == 'Overall Details' && !model.consent && !this.forceFormSubmit) {
                this.agreeValidationMessage = '* This field is required';
                this.openSnackBar('Please fill all the required fields!', 'Close');
            } else if (filteredAutoCompleteFields && filteredAutoCompleteFields.length && !this.forceFormSubmit) {
					setTimeout(() => {
						filteredAutoCompleteFields[0].focus();
					}, 1000);
					this.openSnackBar('Please fill all the required fields!', 'Close');
					this.expandFormPanels();
				} else {
                    this.progressSpinner = true;
					this.dataProviderService.providerEdit(model,formName).subscribe(data => {
                        console.log('First');
						if (data) {
                            this.progressSpinner = false;
							this.formSubmitted = true;
                            this.unsavedEdits = false;
                            this.navigateNextPage = true;
							this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
							if(formName === 'Document Details' && this.dataDocument === 'Document')
							{
								this.router.navigate(['../../', model.recordID], { relativeTo: this.route });

							}
                            if (this.forceFormSubmit === true) {
                                this.navigateNextPage = true;
                                this.onExpireSession(this.forceFormSubmit);
                            }
							if (currentPanel && currentPanel !== '' && !this.forceFormSubmit) {
								this[currentPanel] = false;
								if (nextPanel && nextPanel !== '') {
									this[nextPanel] = true;
                                    if (panelName === 'Data Quality') {
                                        document.documentElement.scrollTop = 350;
                                    } else if (panelName === 'Pricing') {
                                        document.documentElement.scrollTop = 350;
                                    } else if (panelName === 'Licensing') {
                                        document.documentElement.scrollTop = 350;
                                    } else {
                                        document.documentElement.scrollTop = 200;
                                    }
								}
								if (tabIndex) {
									this.tabIndex = tabIndex;
								}
							}

							else if (!this.forceFormSubmit) {
								if (skipRedirect) {
									return true;
								}
                                this.navigateNextPage = true;
                                // this.userObj.dataProfile = true;
                                // this.providerUserOnboardingService.providerUserOnboarding(this.userObj).subscribe(data => console.log(data));
								this.stateService.storeUrl(null);
								this.router.navigate(['../../', model.recordID], { relativeTo: this.route });
							}
						} else {
							// console.log(data);
							this.formSubmitted = false;
							this.progressSpinner=false;
							this.openSnackBar('' + formName + ' failed to save!', 'Close');
						}
					}, error => {
						// console.log(error);
						this.formSubmitted = false;
						this.progressSpinner=false;
						this.openSnackBar('' + formName + ' failed to save!', 'Close');
					});
				}
		} else {
			let invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
			let autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
			let filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
				return node.required && node.value === '';
			});
			if (filteredAutoCompleteFields && filteredAutoCompleteFields.length && !this.forceFormSubmit) {
				setTimeout(() => {
					filteredAutoCompleteFields[0].focus();
				}, 1000);
				this.openSnackBar('Please fill all the required fields!', 'Close');
				this.expandFormPanels();
                console.log('3');
			} else if (invalidElements.length > 0 && !this.forceFormSubmit) {
				if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
					invalidElements[0].querySelectorAll('input')[0].focus();
				} else {
					setTimeout(() => {
						invalidElements[0].focus();
					}, 1000);
				}
				this.openSnackBar('Please fill all the required fields!', 'Close');
				this.expandFormPanels();
                console.log('4');
			} else {
				this.tabIndex = this.tabIndex === 0 ? 1 : 0;
				setTimeout(() => {
					invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
					autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
					filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
						return node.required && node.value === '';
					});
					if (filteredAutoCompleteFields && filteredAutoCompleteFields.length && !this.forceFormSubmit) {
						setTimeout(() => {
							filteredAutoCompleteFields[0].focus();
						}, 1000);
						this.openSnackBar('Please fill all the required fields!', 'Close');
						this.expandFormPanels();
                        console.log('5');
					} else if (invalidElements.length > 0 && !this.forceFormSubmit) {
						if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
							invalidElements[0].querySelectorAll('input')[0].focus();
						} else {
							setTimeout(() => {
								invalidElements[0].focus();
							}, 1000);
						}
						this.openSnackBar('Please fill all the required fields!', 'Close');
						this.expandFormPanels();
                        console.log('6');
					}
					else if(form ==='privacySecurityForm' && !model.consent && !this.forceFormSubmit) {
                        this.agreeValidationMessage = '* This field is required';
                        this.openSnackBar('Please fill all the required fields!', 'Close');
                    } else if(this.overAll =='Overall Details' && !model.consent && !this.forceFormSubmit) {
                        this.agreeValidationMessage = '* This field is required';
                        this.openSnackBar('Please fill all the required fields!', 'Close');
                    }
					else {
						this.progressSpinner=true;
						this.dataProviderService.providerEdit(model,formName).subscribe(data => {
                            console.log('Second');
							if (data) {
                                this.progressSpinner = false;
								this.formSubmitted = true;
                                this.unsavedEdits = false;
                                this.navigateNextPage = true;
								this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                                if (this.forceFormSubmit === true) {
                                    this.navigateNextPage = true;
                                    this.onExpireSession(this.forceFormSubmit);
                                }
								if (currentPanel && currentPanel !== '' && !this.forceFormSubmit) {
									this[currentPanel] = false;
									if (nextPanel && nextPanel !== '') {
										this[nextPanel] = true;
                                        if (panelName === 'Data Quality') {
                                            document.documentElement.scrollTop = 350;
                                        } else if (panelName === 'Pricing') {
                                            document.documentElement.scrollTop = 350;
                                        } else if (panelName === 'Licensing') {
                                            document.documentElement.scrollTop = 350;
                                        } else {
                                            document.documentElement.scrollTop = 200;
                                        }
									}
									if (tabIndex) {
										this.tabIndex = tabIndex;
									}
								} else if (!this.forceFormSubmit) {
									if (skipRedirect) {
										return true;
									}
									this.stateService.storeUrl(null);
									this.router.navigate(['../../', model.recordID], { relativeTo: this.route });
								}
							} else {
								this.formSubmitted = false;
								this.progressSpinner=false;
								this.openSnackBar('' + formName + ' failed to save!', 'Close');
							}
						}, error => {
							this.formSubmitted = false;
							this.progressSpinner=false;
							this.openSnackBar('' + formName + ' failed to save!', 'Close');
						});
					}
				}, 2000);
			}
		}
    }

    submitWhileNavigation() {
        if(this.overAll !== 'Overall Details') {
            this.submitProviderEdit(this.basicModel,'Overall Details');
            return true;
        } else if(this.overAll === 'Overall Details') {
            return true;
        }
    }

    expandFormPanels() {
		this.privacySecurityPanel = true;
        this.basicExpansionPanel = true;
        this.documentExpansionPanel = true;
		this.companyExpansionPanel = true;
		this.descriptionsExpansionPanel = true;
		this.categorizationsExpansionPanel = true;
		this.investigationExpansionPanel = true;
		this.relatedProvidersExpansionPanel = true;
		this.dataDetailsExpansionPanel = true;
		this.dataDeliveryExpansionPanel = true;
		this.triallingExpansionPanel = true;
		this.licensingExpansionPanel = true;
	}

	marketingLiterature(model) {
		this.dataProviderService.addLinkAdmin(model).subscribe(data => {
			this.resourceObj = {};
			this.resourceObj['link'] = {id:data.id};
			this.resourceObj['name'] = data.link;
			this.resourceObj['purposes'] = [{id: 70}];
			this.resourceObj['relavantDataProviders'] = [{id: this.dataProviderID}];
		});
		setTimeout(() => {
			this.dataResourceService.addResourceAdmin(this.resourceObj).subscribe(data => {
				const  dataObj = data;
				const createdResource = {};
				createdResource['name'] = dataObj.name;
				createdResource['id'] = dataObj.id;
				this.marketingLiteratureList.push(createdResource);
			});
		}, 500);
	}

	dataDictionary(model) {
		this.dataProviderService.addLinkAdmin(model).subscribe(data => {
			this.resourceObj = {};
			this.resourceObj['link'] = {id:data.id};
			this.resourceObj['name'] = data.link;
			this.resourceObj['purposes'] = [{id: 72}];
			this.resourceObj['relavantDataProviders'] = [{id: this.dataProviderID}];
		});
		setTimeout(() => {
			this.dataResourceService.addResourceAdmin(this.resourceObj).subscribe(data => {
				const  dataObj = data;
				const createdResource = {};
				createdResource['name'] = dataObj.name;
				createdResource['id'] = dataObj.id;
				this.dataDictionaryList.push(createdResource);
			});
		}, 500);
	}

	samples(model) {
		this.dataProviderService.addLinkAdmin(model).subscribe(data => {
			this.resourceObj = {};
			this.resourceObj['link'] = {id:data.id};
			this.resourceObj['name'] = data.link;
			this.resourceObj['purposes'] = [{id: 93}];
			this.resourceObj['relavantDataProviders'] = [{id: this.dataProviderID}];
		});
		setTimeout(() => {
			this.dataResourceService.addResourceAdmin(this.resourceObj).subscribe(data => {
				const  dataObj = data;
				const createdResource = {};
				createdResource['name'] = dataObj.name;
				createdResource['id'] = dataObj.id;
				this.samplesAndExamplesList.push(createdResource);
			});
		}, 500);
	}

	reportsAnalyses(model) {
		this.dataProviderService.addLinkAdmin(model).subscribe(data => {
			this.resourceObj = {};
			this.resourceObj['link'] = {id:data.id};
			this.resourceObj['name'] = data.link;
			this.resourceObj['purposes'] = [{id: 91}];
			this.resourceObj['relavantDataProviders'] = [{id: this.dataProviderID}];
		});
		setTimeout(() => {
			this.dataResourceService.addResourceAdmin(this.resourceObj).subscribe(data => {
				const  dataObj = data;
				const createdResource = {};
				createdResource['name'] = dataObj.name;
				createdResource['id'] = dataObj.id;
				this.reportsAnalysesList.push(createdResource);
			});
		}, 500);
	}

	deleteMarketingLiterature(obj, index) {

		const deleteLinkObj = obj.resourceID;
		const resourcesId=obj.id;
		this.marketingLiteratureList.map(item => {

		   if(item.id === obj.id) {
			   const itemObject = item;
			   this.dataResourceService.deleteResourceLink(itemObject, obj.resourceID, this.dataProviderID, this.marketingLiteratureResource).subscribe(data => {
				if(data)
				{
					this.marketingLiteratureList.splice(index, 1);

				}

			   });
		    }
		});
		// this.dataDictionaryList.splice(index, 1);
		// this.samplesAndExamplesList.splice(index, 1);
		// this.reportsAnalysesList.splice(index, 1);
	}
	deleteDocumentationDataDictionary(obj, index)
	{

		const deleteLinkObj = obj.resourceID;
		const resourcesId=obj.id;
		this.dataDictionaryList.map(item => {

		   if(item.id === obj.id) {
			   const itemObject = item;
			   this.dataResourceService.deleteResourceLink(itemObject, obj.resourceID, this.dataProviderID, this.marketingLiteratureResource).subscribe(data => {
				if(data)
				{
					this.dataDictionaryList.splice(index, 1);

				}

			   });
		    }
		});
	}
	deleteSamplesExamples(obj,index)
	{

		const deleteLinkObj = obj.resourceID;
		const resourcesId=obj.id;
		this.samplesAndExamplesList.map(item => {

		   if(item.id === obj.id) {
			   const itemObject = item;
			   this.dataResourceService.deleteResourceLink(itemObject, obj.resourceID, this.dataProviderID, this.marketingLiteratureResource).subscribe(data => {
				if(data)
				{
					this.samplesAndExamplesList.splice(index, 1);

				}

			   });
		    }
		});
	}
	deleteReportAnalyses(obj,index)
	{

		const deleteLinkObj = obj.resourceID;
		const resourcesId=obj.id;
		this.reportsAnalysesList.map(item => {

		   if(item.id === obj.id) {
			   const itemObject = item;
			   this.dataResourceService.deleteResourceLink(itemObject, obj.resourceID, this.dataProviderID, this.marketingLiteratureResource).subscribe(data => {
				if(data)
				{
					this.reportsAnalysesList.splice(index, 1);

				}

			   });
		    }
		});
	}

	onclick(event) {
        this.fileDownload(event);
    }

    fileDownload(fileID) {
        this.http.get('api/filedownload/' + fileID, {responseType: 'text'}).subscribe(data => {
            if (data && data !== 'file not found') {
                window.location.href = 'api/filedownload/' + fileID;
            } else {
                this.snackBar.open('File not found!', 'Close', {
                    duration: 5000
                });
            }
        }, error => {
            this.snackBar.open('File not found!', 'Close', {
                duration: 5000
            });
        });
    }

    formSubmit(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    formSave(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
	removeLink(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
    basicInput($event) {
        const inputKey = $event;
        _that.inputChanges($event);
        /* delete entry */
        if (inputKey.key === 'providerName') {
            if (inputKey.model.providerName.length < _that.trackname) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.providerName.length > _that.trackname) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'website') {
             if ( inputKey.model.website.length < _that.trackwebsite) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.website.length > _that.trackwebsite) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'ownerOrganization.city') {
            if (inputKey.model.ownerOrganization.city.length < _that.trackownerOrganizationCity) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.ownerOrganization.city.length > _that.trackownerOrganizationCity) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'ownerOrganization.state') {
             if (inputKey.model.ownerOrganization.state.length < _that.trackownerOrganizationState) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.ownerOrganization.state.length > _that.trackownerOrganizationState) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'ownerOrganization.country') {
            if (inputKey.model.ownerOrganization.country.length < _that.trackownerOrganizationCountry) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.ownerOrganization.country.length > _that.trackownerOrganizationCountry) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'ownerOrganization.yearFounded') {
            if (inputKey.model.ownerOrganization.yearFounded < _that.trackownerOrganizationYearfound) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.ownerOrganization.yearFounded > _that.trackownerOrganizationYearfound) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'ownerOrganization.headCount') {
            if (inputKey.model.ownerOrganization.headCount.length < _that.trackownerOrganizationHeadcount) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.ownerOrganization.headCount.length > _that.trackownerOrganizationHeadcount) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
		}
		// else if (inputKey.key === 'yearFirstDataProductLaunched') {
        //     if (inputKey.model.yearFirstDataProductLaunched && inputKey.model.yearFirstDataProductLaunched.length < _that.trackYearFirstDataProductLaunched) {
        //         _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
        //     } else if (inputKey.model.yearFirstDataProductLaunched && inputKey.model.yearFirstDataProductLaunched.length > _that.trackYearFirstDataProductLaunched) {
        //         _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
        //     }
		// }
		else if (inputKey.key === 'numberOfAnalystEmployees') {
            if (inputKey.model.numberOfAnalystEmployees.length < _that.trackNumberOfAnalystEmployees) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.numberOfAnalystEmployees.length > _that.trackNumberOfAnalystEmployees) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'shortDescription') {
            if (inputKey.model.shortDescription.length < _that.trackShortDescription) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.shortDescription.length > _that.trackShortDescription) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'longDescription') {
            if (inputKey.model.longDescription.length < _that.trackLongDescription) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.longDescription.length > _that.trackLongDescription) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'dataSourcesDetails') {
            if (inputKey.model.dataSourcesDetails.length < _that.trackDataSourcesDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.dataSourcesDetails.length > _that.trackDataSourcesDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.key === 'investor_clients') {
            if (inputKey.model.investor_clients.length < _that.trackInvestor) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.investor_clients.length > _that.trackInvestor) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'investorClientsCount') {
            if (inputKey.model.investorClientsCount < _that.trackNumberOfInvestor) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.investorClientsCount > _that.trackNumberOfInvestor) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'dateCollectionBegan') {
            if (inputKey.model.dateCollectionBegan.length < _that.trackDataCollection) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.dateCollectionBegan.length > _that.trackDataCollection) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'publicCompaniesCovered') {
            if (inputKey.model.publicCompaniesCovered.length > _that.trackpublicCompaniesCovered) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.publicCompaniesCovered.length < _that.trackpublicCompaniesCovered) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'publicEquitesCovered') {
            if (inputKey.model.publicEquitesCovered.length > _that.trackpublicEquitesCovered) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.publicEquitesCovered.length < _that.trackpublicEquitesCovered) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'competitiveDifferentiators') {
            if (inputKey.model.competitiveDifferentiators.length > _that.trackCompetitiveDifferentiators) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.competitiveDifferentiators.length < _that.trackCompetitiveDifferentiators) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'collectionMethodsExplanation') {
            if (inputKey.model.collectionMethodsExplanation.length < _that.trackcollectionMethodsExplanation) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.collectionMethodsExplanation.length > _that.trackcollectionMethodsExplanation) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'sampleOrPanelSize') {
            if (inputKey.model.sampleOrPanelSize.length < _that.tracksampleOrPanelSize) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.sampleOrPanelSize.length > _that.tracksampleOrPanelSize) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'dateCollectionRangeExplanation') {
            if (inputKey.model.dateCollectionRangeExplanation.length < _that.trackDateCollectionRangeExplanation) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.dateCollectionRangeExplanation.length > _that.trackDateCollectionRangeExplanation) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'outOfSampleData') {
            if (inputKey.model.outOfSampleData.length > _that.trackOutOfSampleData) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.outOfSampleData.length < _that.trackOutOfSampleData) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'productDetails') {
            if (inputKey.model.productDetails.length < _that.trackProductDetails) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.productDetails.length > _that.trackProductDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'lagTime') {
            if (inputKey.model.lagTime.length > _that.trackLagTime) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.lagTime.length < _that.trackLagTime) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'deliveryFrequencyNotes') {
            if (inputKey.model.deliveryFrequencyNotes > _that.trackDeliveryFrequencyNotes) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.deliveryFrequencyNotes.length < _that.trackDeliveryFrequencyNotes) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'dataUpdateFrequencyNotes') {
            if (inputKey.model.dataUpdateFrequencyNotes.length >_that.trackDataUpdateFrequencyNotes) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.dataUpdateFrequencyNotes.length < _that.trackDataUpdateFrequencyNotes) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'trialDuration') {
            if (inputKey.model.trialDuration.length < _that.trackTrialDuration) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.trialDuration.length > _that.trackTrialDuration) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'trialPricingDetails') {
            if (inputKey.model.trialPricingDetails.length < _that.trackTrialPricingDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.trialPricingDetails.length > _that.trackTrialPricingDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'dataLicenseTypeDetails') {
            if (inputKey.model.dataLicenseTypeDetails.length < _that.trackDataLicenseTypeDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.dataLicenseTypeDetails.length > _that.trackDataLicenseTypeDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'maximumYearlyPrice') {
            if (inputKey.model.maximumYearlyPrice.length < _that.trackMaximumYearlyPrice) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.maximumYearlyPrice.length > _that.trackMaximumYearlyPrice) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'minimumYearlyPrice') {
            if (inputKey.model.minimumYearlyPrice.length < _that.trackMinimumYearlyPrice) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.minimumYearlyPrice.length > _that.trackMinimumYearlyPrice) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'pricingDetails') {
            if (inputKey.model.pricingDetails.length < _that.trackPricingDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.pricingDetails.length > _that.trackPricingDetails) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'sampleOrPanelBiases') {
            if (inputKey.model.sampleOrPanelBiases.length < _that.trackSampleOrPanelBiases) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.sampleOrPanelBiases.length > _that.trackSampleOrPanelBiases) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        }  else if (inputKey.field && inputKey.field.key === 'useCasesOrQuestionsAddressed') {
            if (inputKey.model.useCasesOrQuestionsAddressed.length < _that.trackUseCasesOrQuestionsAddressed) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.useCasesOrQuestionsAddressed.length > _that.trackUseCasesOrQuestionsAddressed) {
               _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
       } else if (inputKey.field &&inputKey.field.key === 'discussionCleanlines') {
            if (inputKey.model.discussionCleanlines.length < _that.trackDiscussionCleanlines) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.discussionCleanlines.length > _that.trackDiscussionCleanlines) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'discussionAnalytics') {
            if (inputKey.model.discussionAnalytics.length < _that.trackDiscussionAnalytics) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.discussionAnalytics.length > _that.trackDiscussionAnalytics) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'potentialDrawbacks') {
            if (inputKey.model.potentialDrawbacks.length < _that.trackPotentialDrawbacks) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.potentialDrawbacks.length > _that.trackPotentialDrawbacks) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'dataRoadmap') {
            if (inputKey.model.dataRoadmap.length > _that.trackDataRoadmap) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.dataRoadmap.length < _that.trackDataRoadmap) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.field && inputKey.field.key === 'notes') {
            if (inputKey.model.notes.length > _that.trackNotes) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Deleted', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
            } else if (inputKey.model.notes.length < _that.trackNotes) {
                _that.googleAnalyticsService.emitEvent(inputKey.field.templateOptions.attributes.trackCategory, 'Added', inputKey.field.templateOptions.attributes.trackLabel + ' - ' + inputKey.field.templateOptions.attributes.trackNames);
			}
		} else if (inputKey[0]?.name) {
            if (inputKey[0].name) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - File', 'Added', 'Provider Profile Form - File - Provider Logo');
            }
        }
    }

    inputChanges(event) {
            const inputFields = _that.el.nativeElement.querySelectorAll('input:not([readonly]), textarea, input[type=text], div.ql-editor');
        const totalFields = inputFields.length;
        let totalFieldsFilled = 0;
        let totalEmptyFields = 0;



        inputFields.forEach(element => {
            if (element.value !== '' && element.value !== undefined) {
                totalFieldsFilled = totalFieldsFilled + 1;
			} else if (element.value === ''){
                totalEmptyFields = totalEmptyFields - 1;
            } else if(element.textContent) {
                totalFieldsFilled = totalFieldsFilled + 1;
			} else if (!element.textContent){
                totalEmptyFields = totalEmptyFields - 1;
			}

			/*  if (element.value === '' && !element.textContent){
                totalEmptyFields = totalEmptyFields - 1;
                console.log('... deleted...', totalFieldsFilled, element.value, !element.textContent, element.textContent);
            } */

           /*  if(element.value) {
                totalFieldsFilled = totalFieldsFilled + 1;
                console.log('...textarea added...', totalFieldsFilled, element.value);
            } */


           /*  if (element.value !== '' || element.textContent) {
                totalFieldsFilled = totalFieldsFilled + 1;
                console.log('... added...', totalFieldsFilled, element.value);
            } else if(!element.value || !element.textContent) {
                totalEmptyFields = totalEmptyFields - 1;
                console.log('... deleted...', totalFieldsFilled, element.value);
            } */
        });
        const completenessPercentage = Math.round((totalFieldsFilled * 100) / totalFields);
        _that.completeness = completenessPercentage;
    }

    openSnackBar(message: string, action: string, delay?: number) {
        this.snackBar.open(message, action, {
            duration: delay ? delay : 10000,
			panelClass: ['blue-snackbar']
        });
    }

	submitPrivacySecurity(model) {
		// console.log(model);
		this.pricingExpansionPanel = false;
		this.basicExpansionPanel = true;
	}

	submitBasic(model) {
		// console.log(model);
		this.basicExpansionPanel = false;
		this.documentExpansionPanel = true;
	}
	submitDocument() {
		this.documentExpansionPanel = false;
		this.companyExpansionPanel = true;
	}

	submitCompanyDetails(model) {
		// console.log(model);
		this.companyExpansionPanel = false;
		this.descriptionsExpansionPanel = true;
	}

	submitDescriptions(model) {
		// console.log(model);
		this.descriptionsExpansionPanel = false;
		this.categorizationsExpansionPanel = true;
	}

	submitCategorizations(model) {
		// console.log(model);
		this.categorizationsExpansionPanel = false;
		this.investigationExpansionPanel = true;
	}

    submitInvestigationDetails(model) {
        this.investigationExpansionPanel = false;
        this.tabIndex = 1;
    }

	submitRelatedProviders(model) {
		// console.log(model);
		this.relatedProvidersExpansionPanel = false;
		this.dataDetailsExpansionPanel = true;
	}

	submitDataDetails(model) {
		// console.log(model);
		this.dataDetailsExpansionPanel = false;
		this.dataDeliveryExpansionPanel = true;
	}

	submitDataDelivery(model) {
		// console.log(model);
		this.dataDeliveryExpansionPanel = false;
		this.triallingExpansionPanel = true;
	}

	submitTrialling(model) {
		// console.log(model);
		this.triallingExpansionPanel = false;
		this.licensingExpansionPanel = true;
	}

	submitLicensing(model) {
		// console.log(model);
		this.licensingExpansionPanel = false;
		this.pricingExpansionPanel = true;
	}

	submitPricing(model) {
		// console.log(model);
		this.pricingExpansionPanel = false;
		this.dataQualityExpansionPanel = true;
	}

	submitDataQuality(model) {
		// console.log(model);
		this.dataQualityExpansionPanel = false;
		this.discussionExpansionPanel = true;
	}

	submitDiscussion(model) {
		// console.log(model);
		this.discussionExpansionPanel = false;
    }

    beforeSessionExpires(event) {
        // if (event) {
        //     this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        // }
    }

    onExpireSession(event) {
        // if (event) {
        //     this.formSubmitted = true;
        //     this.authService.logout().subscribe(d => {
        //         this.stateService.storeUrl(null);
        //         this.principal.authenticate(null);
        //         this.router.navigate(['../../']);
		// 		setTimeout(() => {
		// 			this.signinModalService.openDialog('Your session has expired due to inactivity. Your edits in the form have been automatically saved and submitted and Amass will review your edits over the coming days before they appear within your profile. Please login again.');
		// 		}, 2000);
		// 	    });
        // }
        if (event && this.forceFormSubmit) {
            // console.log('After session',event);
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
                setTimeout(() => {
                    this.signinModalService.openDialog('Your session has expired due to inactivity. Your edits in the form have been automatically saved and submitted and Amass will review your edits over the coming days before they appear within your profile. Please login again.');
                }, 2000);
            });
        }
    }

    settings() {
		this.router.navigate(['/settings']);
	}

    @HostListener('document:keyup', [])
    @HostListener('document:click', [])
    @HostListener('document:wheel', [])
    @HostListener('window:scroll', [])
    resetTimer() {
        this.authService.notifyUserAction();
    }

    @HostListener('window:popstate', [])
    onPopState() {
        this.cookieService.put('logoutClicked', '0');

	}

    @HostListener('window:beforeunload', ['$event'])
    preventUser($event) {
        $event.preventDefault();
        return $event.returnValue = "Are you sure you want to exit?";
    }
}
