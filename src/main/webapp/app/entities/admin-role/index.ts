export * from './admin-role.model';
export * from './admin-role-popup.service';
export * from './admin-role.service';
export * from './admin-role-dialog.component';
export * from './admin-role-delete-dialog.component';
export * from './admin-role-detail.component';
export * from './admin-role.component';
export * from './admin-role.route';
