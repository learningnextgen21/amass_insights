import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { AdminRole } from './admin-role.model';
import { AdminRolePopupService } from './admin-role-popup.service';
import { AdminRoleService } from './admin-role.service';

@Component({
    selector: 'jhi-admin-role-dialog',
    templateUrl: './admin-role-dialog.component.html'
})
export class AdminRoleDialogComponent implements OnInit {

    adminRole: AdminRole;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private adminRoleService: AdminRoleService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.adminRole.id !== undefined) {
            this.subscribeToSaveResponse(
                this.adminRoleService.update(this.adminRole));
        } else {
            this.subscribeToSaveResponse(
                this.adminRoleService.create(this.adminRole));
        }
    }

    private subscribeToSaveResponse(result: Observable<AdminRole>) {
        result.subscribe((res: AdminRole) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: AdminRole) {
        this.eventManager.broadcast({ name: 'adminRoleListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-admin-role-popup',
    template: ''
})
export class AdminRolePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private adminRolePopupService: AdminRolePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.adminRolePopupService
                    .open(AdminRoleDialogComponent as Component, params['id']);
            } else {
                this.adminRolePopupService
                    .open(AdminRoleDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
