import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    AdminRoleService,
    AdminRolePopupService,
    AdminRoleComponent,
    AdminRoleDetailComponent,
    AdminRoleDialogComponent,
    AdminRolePopupComponent,
    AdminRoleDeletePopupComponent,
    AdminRoleDeleteDialogComponent,
    adminRoleRoute,
    adminRolePopupRoute,
} from './';

const ENTITY_STATES = [
    ...adminRoleRoute,
    ...adminRolePopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AdminRoleComponent,
        AdminRoleDetailComponent,
        AdminRoleDialogComponent,
        AdminRoleDeleteDialogComponent,
        AdminRolePopupComponent,
        AdminRoleDeletePopupComponent,
    ],
    entryComponents: [
        AdminRoleComponent,
        AdminRoleDialogComponent,
        AdminRolePopupComponent,
        AdminRoleDeleteDialogComponent,
        AdminRoleDeletePopupComponent,
    ],
    providers: [
        AdminRoleService,
        AdminRolePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceAdminRoleModule {}
