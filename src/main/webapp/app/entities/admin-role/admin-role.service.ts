
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AdminRole } from './admin-role.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class AdminRoleService {

    private resourceUrl = 'api/admin-roles';
    private resourceSearchUrl = 'api/_search/admin-roles';

    constructor(private http: HttpClient) { }

    create(adminRole: AdminRole): Observable<any> {
        const copy = this.convert(adminRole);
        return this.http.post(this.resourceUrl, copy, { observe: 'response' });
    }

    update(adminRole: AdminRole): Observable<any> {
        const copy = this.convert(adminRole);
        return this.http.put(this.resourceUrl, copy, { observe: 'response' });
    }

    find(id: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    private convert(adminRole: AdminRole): AdminRole {
        const copy: AdminRole = Object.assign({}, adminRole);
        return copy;
    }
}
