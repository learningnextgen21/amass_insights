import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { AdminRole } from './admin-role.model';
import { AdminRoleService } from './admin-role.service';
import { WINDOW } from '../../layouts';

@Component({
    selector: 'jhi-admin-role-detail',
    templateUrl: './admin-role-detail.component.html'
})
export class AdminRoleDetailComponent implements OnInit, OnDestroy {

    adminRole: AdminRole;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private adminRoleService: AdminRoleService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAdminRoles();
    }

    load(id) {
        this.adminRoleService.find(id).subscribe((adminRole) => {
            this.adminRole = adminRole;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAdminRoles() {
        this.eventSubscriber = this.eventManager.subscribe(
            'adminRoleListModification',
            (response) => this.load(this.adminRole.id)
        );
    }
}
