import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AdminRoleComponent } from './admin-role.component';
import { AdminRoleDetailComponent } from './admin-role-detail.component';
import { AdminRolePopupComponent } from './admin-role-dialog.component';
import { AdminRoleDeletePopupComponent } from './admin-role-delete-dialog.component';

export const adminRoleRoute: Routes = [
    {
        path: 'admin-role',
        component: AdminRoleComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.adminRole.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'admin-role/:id',
        component: AdminRoleDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.adminRole.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const adminRolePopupRoute: Routes = [
    {
        path: 'admin-role-new',
        component: AdminRolePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.adminRole.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'admin-role/:id/edit',
        component: AdminRolePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.adminRole.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'admin-role/:id/delete',
        component: AdminRoleDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.adminRole.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
