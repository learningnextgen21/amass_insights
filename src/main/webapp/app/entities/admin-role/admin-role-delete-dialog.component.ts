import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AdminRole } from './admin-role.model';
import { AdminRolePopupService } from './admin-role-popup.service';
import { AdminRoleService } from './admin-role.service';

@Component({
    selector: 'jhi-admin-role-delete-dialog',
    templateUrl: './admin-role-delete-dialog.component.html'
})
export class AdminRoleDeleteDialogComponent {

    adminRole: AdminRole;

    constructor(
        private adminRoleService: AdminRoleService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.adminRoleService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'adminRoleListModification',
                content: 'Deleted an adminRole'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-admin-role-delete-popup',
    template: ''
})
export class AdminRoleDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private adminRolePopupService: AdminRolePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.adminRolePopupService
                .open(AdminRoleDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
