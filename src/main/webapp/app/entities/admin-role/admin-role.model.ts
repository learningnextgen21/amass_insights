import { BaseEntity } from './../../shared';

export class AdminRole implements BaseEntity {
    constructor(
        public id?: number,
        public userID?: number,
        public adminRole?: string,
        public description?: any,
    ) {
    }
}
