import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DataProviderTag } from './data-provider-tag.model';
import { DataProviderTagService } from './data-provider-tag.service';

@Injectable()
export class DataProviderTagPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private dataProviderTagService: DataProviderTagService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dataProviderTagService.find(id).subscribe((dataProviderTag) => {
                    if (dataProviderTag.createdDate) {
                        dataProviderTag.createdDate = {
                            year: dataProviderTag.createdDate.getFullYear(),
                            month: dataProviderTag.createdDate.getMonth() + 1,
                            day: dataProviderTag.createdDate.getDate()
                        };
                    }
                    dataProviderTag.updatedDate = this.datePipe
                        .transform(dataProviderTag.updatedDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.dataProviderTagModalRef(component, dataProviderTag);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dataProviderTagModalRef(component, new DataProviderTag());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dataProviderTagModalRef(component: Component, dataProviderTag: DataProviderTag): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dataProviderTag = dataProviderTag;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
