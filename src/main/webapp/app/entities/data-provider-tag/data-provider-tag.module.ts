import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    DataProviderTagService,
    DataProviderTagPopupService,
    DataProviderTagComponent,
    DataProviderTagDetailComponent,
    DataProviderTagDialogComponent,
    DataProviderTagPopupComponent,
    DataProviderTagDeletePopupComponent,
    DataProviderTagDeleteDialogComponent,
    dataProviderTagRoute,
    dataProviderTagPopupRoute,
} from './';

const ENTITY_STATES = [
    ...dataProviderTagRoute,
    ...dataProviderTagPopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DataProviderTagComponent,
        DataProviderTagDetailComponent,
        DataProviderTagDialogComponent,
        DataProviderTagDeleteDialogComponent,
        DataProviderTagPopupComponent,
        DataProviderTagDeletePopupComponent,
    ],
    entryComponents: [
        DataProviderTagComponent,
        DataProviderTagDialogComponent,
        DataProviderTagPopupComponent,
        DataProviderTagDeleteDialogComponent,
        DataProviderTagDeletePopupComponent,
    ],
    providers: [
        DataProviderTagService,
        DataProviderTagPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceDataProviderTagModule {}
