export * from './data-provider-tag.model';
export * from './data-provider-tag-popup.service';
export * from './data-provider-tag.service';
export * from './data-provider-tag-dialog.component';
export * from './data-provider-tag-delete-dialog.component';
export * from './data-provider-tag-detail.component';
export * from './data-provider-tag.component';
export * from './data-provider-tag.route';
