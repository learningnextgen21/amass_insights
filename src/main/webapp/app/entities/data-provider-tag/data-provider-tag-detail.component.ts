import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { DataProviderTag } from './data-provider-tag.model';
import { DataProviderTagService } from './data-provider-tag.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-data-provider-tag-detail',
    templateUrl: './data-provider-tag-detail.component.html'
})
export class DataProviderTagDetailComponent implements OnInit, OnDestroy {

    dataProviderTag: DataProviderTag;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private dataProviderTagService: DataProviderTagService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDataProviderTags();
    }

    load(id) {
        this.dataProviderTagService.find(id).subscribe((dataProviderTag) => {
            this.dataProviderTag = dataProviderTag;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDataProviderTags() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dataProviderTagListModification',
            (response) => this.load(this.dataProviderTag.id)
        );
    }
}
