import { BaseEntity } from './../../shared';

export class DataProviderTag implements BaseEntity {
    constructor(
        public id?: number,
        public recordID?: string,
        public providerTag?: string,
        public explanation?: any,
        public inactive?: number,
        public createdDate?: any,
        public updatedDate?: any,
        public providerTags?: BaseEntity[],
    ) {
    }
}
