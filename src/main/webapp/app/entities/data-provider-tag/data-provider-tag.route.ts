import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DataProviderTagComponent } from './data-provider-tag.component';
import { DataProviderTagDetailComponent } from './data-provider-tag-detail.component';
import { DataProviderTagPopupComponent } from './data-provider-tag-dialog.component';
import { DataProviderTagDeletePopupComponent } from './data-provider-tag-delete-dialog.component';

export const dataProviderTagRoute: Routes = [
    {
        path: 'data-provider-tag',
        component: DataProviderTagComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataProviderTag.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'data-provider-tag/:id',
        component: DataProviderTagDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataProviderTag.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dataProviderTagPopupRoute: Routes = [
    {
        path: 'data-provider-tag-new',
        component: DataProviderTagPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataProviderTag.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-provider-tag/:id/edit',
        component: DataProviderTagPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataProviderTag.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-provider-tag/:id/delete',
        component: DataProviderTagDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataProviderTag.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
