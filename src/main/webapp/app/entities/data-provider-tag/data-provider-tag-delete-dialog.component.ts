import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DataProviderTag } from './data-provider-tag.model';
import { DataProviderTagPopupService } from './data-provider-tag-popup.service';
import { DataProviderTagService } from './data-provider-tag.service';

@Component({
    selector: 'jhi-data-provider-tag-delete-dialog',
    templateUrl: './data-provider-tag-delete-dialog.component.html'
})
export class DataProviderTagDeleteDialogComponent {

    dataProviderTag: DataProviderTag;

    constructor(
        private dataProviderTagService: DataProviderTagService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dataProviderTagService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dataProviderTagListModification',
                content: 'Deleted an dataProviderTag'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-data-provider-tag-delete-popup',
    template: ''
})
export class DataProviderTagDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataProviderTagPopupService: DataProviderTagPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dataProviderTagPopupService
                .open(DataProviderTagDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
