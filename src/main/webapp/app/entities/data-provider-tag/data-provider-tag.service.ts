

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { DataProviderTag } from './data-provider-tag.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class DataProviderTagService {

    private resourceUrl = 'api/data-provider-tags';
    private resourceSearchUrl = 'api/_search/data-provider-tags';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(dataProviderTag: DataProviderTag): Observable<any> {
        const copy = this.convert(dataProviderTag);
        return this.http.post(this.resourceUrl, copy, { observe: 'response' });
    }

    update(dataProviderTag: DataProviderTag): Observable<any> {
        const copy = this.convert(dataProviderTag);
        return this.http.put(this.resourceUrl, copy, { observe: 'response' });
    }

    find(id: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    private convert(dataProviderTag: DataProviderTag): DataProviderTag {
        const copy: DataProviderTag = Object.assign({}, dataProviderTag);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(dataProviderTag.createdDate);

        copy.updatedDate = this.dateUtils.toDate(dataProviderTag.updatedDate);
        return copy;
    }
}
