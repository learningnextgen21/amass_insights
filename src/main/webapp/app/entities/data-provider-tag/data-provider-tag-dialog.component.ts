import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { DataProviderTag } from './data-provider-tag.model';
import { DataProviderTagPopupService } from './data-provider-tag-popup.service';
import { DataProviderTagService } from './data-provider-tag.service';
import { DataProvider, DataProviderService } from '../data-provider';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-data-provider-tag-dialog',
    templateUrl: './data-provider-tag-dialog.component.html'
})
export class DataProviderTagDialogComponent implements OnInit {

    dataProviderTag: DataProviderTag;
    isSaving: boolean;

    dataproviders: DataProvider[];
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private dataProviderTagService: DataProviderTagService,
        private dataProviderService: DataProviderService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.dataProviderService.query()
            .subscribe((res) => { this.dataproviders = res.body; }, (res) => this.onError(res));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dataProviderTag.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dataProviderTagService.update(this.dataProviderTag));
        } else {
            this.subscribeToSaveResponse(
                this.dataProviderTagService.create(this.dataProviderTag));
        }
    }

    private subscribeToSaveResponse(result: Observable<DataProviderTag>) {
        result.subscribe((res: DataProviderTag) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DataProviderTag) {
        this.eventManager.broadcast({ name: 'dataProviderTagListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackDataProviderById(index: number, item: DataProvider) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-data-provider-tag-popup',
    template: ''
})
export class DataProviderTagPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataProviderTagPopupService: DataProviderTagPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dataProviderTagPopupService
                    .open(DataProviderTagDialogComponent as Component, params['id']);
            } else {
                this.dataProviderTagPopupService
                    .open(DataProviderTagDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
