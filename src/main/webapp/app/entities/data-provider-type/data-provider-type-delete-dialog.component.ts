import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DataProviderType } from './data-provider-type.model';
import { DataProviderTypePopupService } from './data-provider-type-popup.service';
import { DataProviderTypeService } from './data-provider-type.service';

@Component({
    selector: 'jhi-data-provider-type-delete-dialog',
    templateUrl: './data-provider-type-delete-dialog.component.html'
})
export class DataProviderTypeDeleteDialogComponent {

    dataProviderType: DataProviderType;

    constructor(
        private dataProviderTypeService: DataProviderTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dataProviderTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dataProviderTypeListModification',
                content: 'Deleted an dataProviderType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-data-provider-type-delete-popup',
    template: ''
})
export class DataProviderTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataProviderTypePopupService: DataProviderTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dataProviderTypePopupService
                .open(DataProviderTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
