import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { DataProviderType } from './data-provider-type.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class DataProviderTypeService {

    private resourceUrl = 'api/data-provider-types';
    private resourceSearchUrl = 'api/_search/data-provider-types';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(dataProviderType: DataProviderType): Observable<any> {
        const copy = this.convert(dataProviderType);
        return this.http.post(this.resourceUrl, copy, { observe: 'response' });
    }

    update(dataProviderType: DataProviderType): Observable<any> {
        const copy = this.convert(dataProviderType);
        return this.http.put(this.resourceUrl, copy, { observe: 'response' });
    }

    find(id: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    private convert(dataProviderType: DataProviderType): DataProviderType {
        const copy: DataProviderType = Object.assign({}, dataProviderType);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(dataProviderType.createdDate);

        copy.updatedDate = this.dateUtils.toDate(dataProviderType.updatedDate);
        return copy;
    }
}
