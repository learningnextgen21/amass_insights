import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { DataProviderType } from './data-provider-type.model';
import { DataProviderTypePopupService } from './data-provider-type-popup.service';
import { DataProviderTypeService } from './data-provider-type.service';

@Component({
    selector: 'jhi-data-provider-type-dialog',
    templateUrl: './data-provider-type-dialog.component.html'
})
export class DataProviderTypeDialogComponent implements OnInit {

    dataProviderType: DataProviderType;
    isSaving: boolean;
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private dataProviderTypeService: DataProviderTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dataProviderType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dataProviderTypeService.update(this.dataProviderType));
        } else {
            this.subscribeToSaveResponse(
                this.dataProviderTypeService.create(this.dataProviderType));
        }
    }

    private subscribeToSaveResponse(result: Observable<DataProviderType>) {
        result.subscribe((res: DataProviderType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DataProviderType) {
        this.eventManager.broadcast({ name: 'dataProviderTypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-data-provider-type-popup',
    template: ''
})
export class DataProviderTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataProviderTypePopupService: DataProviderTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dataProviderTypePopupService
                    .open(DataProviderTypeDialogComponent as Component, params['id']);
            } else {
                this.dataProviderTypePopupService
                    .open(DataProviderTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
