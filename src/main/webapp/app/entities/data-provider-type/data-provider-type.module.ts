import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    DataProviderTypeService,
    DataProviderTypePopupService,
    DataProviderTypeComponent,
    DataProviderTypeDetailComponent,
    DataProviderTypeDialogComponent,
    DataProviderTypePopupComponent,
    DataProviderTypeDeletePopupComponent,
    DataProviderTypeDeleteDialogComponent,
    dataProviderTypeRoute,
    dataProviderTypePopupRoute,
} from './';

const ENTITY_STATES = [
    ...dataProviderTypeRoute,
    ...dataProviderTypePopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DataProviderTypeComponent,
        DataProviderTypeDetailComponent,
        DataProviderTypeDialogComponent,
        DataProviderTypeDeleteDialogComponent,
        DataProviderTypePopupComponent,
        DataProviderTypeDeletePopupComponent,
    ],
    entryComponents: [
        DataProviderTypeComponent,
        DataProviderTypeDialogComponent,
        DataProviderTypePopupComponent,
        DataProviderTypeDeleteDialogComponent,
        DataProviderTypeDeletePopupComponent,
    ],
    providers: [
        DataProviderTypeService,
        DataProviderTypePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceDataProviderTypeModule {}
