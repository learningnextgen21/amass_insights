import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DataProviderTypeComponent } from './data-provider-type.component';
import { DataProviderTypeDetailComponent } from './data-provider-type-detail.component';
import { DataProviderTypePopupComponent } from './data-provider-type-dialog.component';
import { DataProviderTypeDeletePopupComponent } from './data-provider-type-delete-dialog.component';

export const dataProviderTypeRoute: Routes = [
    {
        path: 'data-provider-type',
        component: DataProviderTypeComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataProviderType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'data-provider-type/:id',
        component: DataProviderTypeDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataProviderType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dataProviderTypePopupRoute: Routes = [
    {
        path: 'data-provider-type-new',
        component: DataProviderTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataProviderType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-provider-type/:id/edit',
        component: DataProviderTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataProviderType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-provider-type/:id/delete',
        component: DataProviderTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataProviderType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
