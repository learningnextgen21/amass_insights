import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { DataProviderType } from './data-provider-type.model';
import { DataProviderTypeService } from './data-provider-type.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-data-provider-type-detail',
    templateUrl: './data-provider-type-detail.component.html'
})
export class DataProviderTypeDetailComponent implements OnInit, OnDestroy {

    dataProviderType: DataProviderType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private dataProviderTypeService: DataProviderTypeService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDataProviderTypes();
    }

    load(id) {
        this.dataProviderTypeService.find(id).subscribe((dataProviderType) => {
            this.dataProviderType = dataProviderType;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDataProviderTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dataProviderTypeListModification',
            (response) => this.load(this.dataProviderType.id)
        );
    }
}
