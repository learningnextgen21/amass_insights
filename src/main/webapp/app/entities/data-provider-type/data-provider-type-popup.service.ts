import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DataProviderType } from './data-provider-type.model';
import { DataProviderTypeService } from './data-provider-type.service';

@Injectable()
export class DataProviderTypePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private dataProviderTypeService: DataProviderTypeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dataProviderTypeService.find(id).subscribe((dataProviderType) => {
                    if (dataProviderType.createdDate) {
                        dataProviderType.createdDate = {
                            year: dataProviderType.createdDate.getFullYear(),
                            month: dataProviderType.createdDate.getMonth() + 1,
                            day: dataProviderType.createdDate.getDate()
                        };
                    }
                    dataProviderType.updatedDate = this.datePipe
                        .transform(dataProviderType.updatedDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.dataProviderTypeModalRef(component, dataProviderType);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dataProviderTypeModalRef(component, new DataProviderType());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dataProviderTypeModalRef(component: Component, dataProviderType: DataProviderType): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dataProviderType = dataProviderType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
