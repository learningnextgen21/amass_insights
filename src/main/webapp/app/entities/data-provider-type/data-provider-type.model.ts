import { BaseEntity } from './../../shared';

export class DataProviderType implements BaseEntity {
    constructor(
        public id?: number,
        public recordID?: string,
        public explanation?: any,
        public providerType?: string,
        public inactive?: number,
        public createdDate?: any,
        public updatedDate?: any,
        public dataProviders?: BaseEntity[],
    ) {
    }
}
