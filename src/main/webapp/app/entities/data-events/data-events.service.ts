

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, Principal } from '../../shared';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataEventsService {

	private resourceElasticSearchUrl = 'api/es/eventsearch';

	constructor(private http: HttpClient, private dateUtils: JhiDateUtils) {
    }

	search(req?: any): Observable<any> {
		return this.http.post(this.resourceElasticSearchUrl, req);
    }

    unrestrictedSearch(): Observable<any> {
        return this.http.post('api/ur/eventsearch?type=dataevent', {});
    }

    queryWithSearchFilter(req?: any): Observable<any> {
		const newReq = Object.assign({
            '_source': {
                'include': [
                    'id',
                    'recordID',
                    'eventEditionNames',
                    'categories',
                    'link',
                    'description',
                    'details',
                    'agenda',
                    'startTime',
                    'endTime',
                    'lenthInDays',
                    'pricingModels',
                    'price',
                    'venue',
                    'address',
                    'city',
                    'state',
                    'country',
                    'zipCode',
                    'userPermission',
                    'lastUpdated',
                    'createdTime',
                    'authoredArticles',
                    'releventArticles',
                    'completeness',
                    'industries',
                    'topics',
                    'eventTypes',
                    'completeness'
                ]
            }
        }, req);

		return this.http.post(this.resourceElasticSearchUrl, newReq);
    }

    findByID(recordID: string) {
        const req = {
			'_source': {

            },
			'filter': {
				'query': {
					'match': {
						'recordID': recordID
					}
				}
            }
        };
        return this.http.post(this.resourceElasticSearchUrl + '?detail=true', req);
    }
}
