import { BaseEntity } from '../../shared';
import { DataOrganization } from '../data-organization';
import { DataProvider } from '../data-provider';

export class DataEvents implements BaseEntity {
    constructor(
		public id?: number,
        public recordID?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public eventEditionNames?: string,
        public startTime?: any,
        public endTime?: any,
        public industries?: any,
        public categories?: any,
        public topics?: any,
        public description?: string,
        public completeness?: any,
        public link?: string,
        public locked?: boolean,
        public createdTime?: any,
        public lastUpdated?: any,
        public lenthInDays?: any,
        public eventTypes?: any[],
        public details?: any,
        public agenda?: any,
        public venue?: any,
        public address?: any,
        public city?: string,
        public state?: string,
        public country?: string,
        public zipCode?: string,
        public price?: any,
        public pricingModels?: any,
        public organizationOrganizer?: DataOrganization[],
        public providerOrganizer?: DataProvider[],
        public sponsorOrganization?: DataOrganization[],
        public sponsorProvider?: DataProvider[]
    ) {

    }
}
