import { Routes } from '@angular/router';
import { UserRouteAccessService, UserRouteAccessUnrestrictedService } from 'app/shared';
import { DataEventsComponent } from './data-events.component';
import { DataEventsDetailComponent } from './data-events-detail.component';

export const dataEventsRoute: Routes = [
	{
        path: 'events',
		data: {
			authorities: ['ROLE_USER'],
			pageTitle: 'datamarketplaceApp.dataEvents.home.title',
			breadcrumb: 'Events',
			type: 'dynamic'
        },
        canActivate: [UserRouteAccessUnrestrictedService],
        children: [
			{
                path: ':recordID',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'datamarketplaceApp.dataEvents.home.title',
                    breadcrumb: 'Data Events',
                    type: 'dynamic'
                },
                canActivate: [UserRouteAccessService],
                component: DataEventsDetailComponent
            }, {
                path: '',
                component: DataEventsComponent,
                data: {
					breadcrumb: '',
					type: 'dynamic'
				}
            }
        ]
	}
];
