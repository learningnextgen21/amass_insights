import { Component, OnInit } from '@angular/core';
import { DataEvents } from './data-events.model';
import { ActivatedRoute } from '@angular/router';
import { DataEventsService } from './data-events.service';
import { BehaviorSubject } from 'rxjs';
import { GenericProfileTabsModel, TabContentType, GenericSubTabListItem } from 'app/shared/generic-profile';
import { DatePipe } from '@angular/common';
import { GoogleAnalyticsEventsService } from 'app/shared/googleanalytics/google-analytics-events.service';

@Component({
    selector: 'jhi-data-events-detail',
    templateUrl: 'data-events-detail.component.html',
    styleUrls: [
        '../data-provider/data-provider-detail.component.css',
        './data-events.component.scss'
    ]
})
export class DataEventsDetailComponent implements OnInit {
    dataEvent: DataEvents;
    recordID: string;
    isLoading: boolean;

    tabsArray: BehaviorSubject<GenericProfileTabsModel[]> = new BehaviorSubject<GenericProfileTabsModel[]>([]);
    tabs: GenericProfileTabsModel[];
    overviewTabTopTree: any;
    urlLinks: any;
    constructor(
        private activatedRoute: ActivatedRoute,
        private dataEventsService: DataEventsService,
        private datePipe: DatePipe,
        public googleAnalyticsService: GoogleAnalyticsEventsService
    ) { }

    ngOnInit() {
        this.isLoading = true;
        this.overviewTabTopTree = [];
        this.activatedRoute.params.subscribe(params => {
            if (params['recordID']) {
                this.recordID = params['recordID'];
                this.dataEventsService.findByID(this.recordID).subscribe(data => {
                    this.dataEvent = data['hits'].total ? data['hits'].hits[0]._source : null;
                    if (this.dataEvent) {
                        this.loadAllTree(this.dataEvent);
                    }
                    this.isLoading = false;
                });
            }
        });
    }
    websiteLink() {
        this.googleAnalyticsService.emitEvent('Event', 'External Link Clicked', 'Event Details - About - Website');
    }
    loadTabs(dataEvent: DataEvents) {
        const eventLocation = [dataEvent.city, dataEvent.state, dataEvent.country];
        const organizerOrganizations: GenericSubTabListItem[] = [];
        const eventOrganizer = [];
        if (dataEvent.organizationOrganizer) {
            dataEvent.organizationOrganizer.forEach(org => {
                organizerOrganizations.push({
                    id: org.id,
                    title: org.name,
                    subTitle: org.description
                });
                eventOrganizer.push({
                    desc: org.name
                });
            });
        }
        const sponsorOrganizations: GenericSubTabListItem[] = [];
        if (dataEvent.sponsorOrganization) {
            dataEvent.sponsorOrganization.forEach(org => {
                sponsorOrganizations.push({
                    id: org.id,
                    title: org.name,
                    subTitle: org.description
                });
            });
        }
        const organizerDataProviders: GenericSubTabListItem[] = [];
        if (dataEvent.providerOrganizer) {
            dataEvent.providerOrganizer.forEach(org => {
                if (org.providerName !== 'RECORD_NULL') {
                    organizerDataProviders.push({
                        id: org.id,
                        title: org.providerName,
                        mainContent: org.shortDescription,
                        url: '../#/providers/' + org.recordID,
                        locked: org.locked ? org.locked : false,
                        base64Image: org.locked ? null : (org.logo && org.logo.logo) ? org.logo.logo : null,
                        completenessBar: org.completeness,
                        overAllScore: org.scoreOverall && org.scoreOverall.description ? org.scoreOverall : '',
                        addedDate: org.createdDate != null ? this.datePipe.transform(org.createdDate, 'mediumDate', 'EST') : '',
                        updatedDate: org.updatedDate != null ? this.datePipe.transform(org.updatedDate, 'mediumDate', 'EST') : '',
                        providerStatus: org.userProviderStatus,
                        providersTabs: 'Providers'
                    });
                    eventOrganizer.push({
                        id: org.recordID,
                        desc: org.providerName
                    });
                }
            });
        }
        const sponsorDataProviders: GenericSubTabListItem[] = [];
        if (dataEvent.sponsorProvider) {
            dataEvent.sponsorProvider.forEach(org => {
                if (org.providerName !== 'RECORD_NULL') {
                    sponsorDataProviders.push({
                        id: org.id,
                        title: org.providerName,
                        mainContent: org.shortDescription,
                        url: '../#/providers/' + org.recordID,
                        locked: org.locked ? org.locked : false,
                        base64Image: org.locked ? null : (org.logo && org.logo.logo) ? org.logo.logo : null,
                        completenessBar: org.completeness,
                        overAllScore: org.scoreOverall && org.scoreOverall.description ? org.scoreOverall : '',
                        addedDate: org.createdDate != null ? this.datePipe.transform(org.createdDate, 'mediumDate', 'EST') : '',
                        updatedDate: org.updatedDate != null ? this.datePipe.transform(org.updatedDate, 'mediumDate', 'EST') : '',
                        providerStatus: org.userProviderStatus,
                        providersTabs: 'Providers'
                    });
                }
            });
        }
        this.tabs = [{
            tabLabelName: 'Overview',
            tabLabelTooltip: 'High-level overview of the event\'s characteristics.',
            tabContent: [{
                isTreeNodesWithPanel: false,
                treeNodes: [this.overviewTabTopTree],
            }, {
                isTreeNodesWithPanel: true,
                treeNodes: [
                    [{
                        desc: 'Event Type:',
                        children: dataEvent.eventTypes,
                        tooltip: 'What is the main format type of the event?',
                        params: {
                            filter: 'eventTypes.id',
                            type: 'checkbox'
                        },
                        routing: '/events'
                    }],
                    [{
                        desc: 'Topics:',
                        children: dataEvent.topics,
                        tooltip: 'The topics that are relevant to the event.',
                        params: {
                            filter: 'topics.id',
                            type: 'checkbox'
                        },
                        routing: '/events'
                    }],
                    [{
                        desc: 'Data Industries:',
                        children: dataEvent.industries,
                        tooltip: 'The data industries that are relevant to the event.',
                        params: {
                            filter: 'industries.id',
                            type: 'checkbox'
                        },
                        routing: '/events'
                    }],
                    [{
                        desc: 'Data Categories:',
                        children: dataEvent.categories,
                        tooltip: 'The data categories that are relevant to the event.',
                        params: {
                            filter: 'categories.id',
                            type: 'checkbox'
                        },
                        routing: '/events'
                    }]
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (dataEvent.eventTypes || dataEvent.eventTypes || dataEvent.industries || dataEvent.categories) ? 'Event Classifications' : '',
                    tooltip: 'High-level classifications of the event\'s content.'
                }
            }, {
                isTreeNodesWithPanel: true,
                treeNodes: [
                    [{
                        desc: 'Organizer:',
                        children: eventOrganizer,
                        tooltip: 'Organization that organizes the event.',
                        routing: '/providers'
                    }]
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (dataEvent.organizationOrganizer) ? 'Organizer Details' : '',
                    tooltip: 'Information about the organization that organized the event.'
                }
            }]
        }, {
            tabLabelName: 'Details',
            tabLabelTooltip: 'Important details of the event.',
            tabContent: [{
                isTreeNodesWithPanel: true,
                treeNodes: [
                    [{
                        desc: 'Details:',
                        children: dataEvent.details ? [{ desc: dataEvent.details }] : null,
                        tooltip: 'Further details about the event.'
                    }],
                    [{
                        desc: 'Agenda:',
                        children: dataEvent.agenda ? [{ desc: dataEvent.agenda }] : null,
                        tooltip: 'Details about the agenda for the event.'
                    }]
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (dataEvent.details || dataEvent.agenda) ? 'Content Details' : ''
                }
            }, {
                isTreeNodesWithPanel: true,
                treeNodes: [
                    [{
                        desc: 'Venue:',
                        children: dataEvent.venue ? [{ desc: dataEvent.venue }] : null,
                        tooltip: 'The venue the event is taking place in.'
                    }],
                    [{
                        desc: 'Address:',
                        children: dataEvent.address ? [{ desc: dataEvent.address }] : null,
                        tooltip: 'The address of the venue the event is taking place in.'
                    }],
                    [{
                        desc: 'Location:',
                        children: eventLocation.filter(Boolean).join(', ') ? [{ desc: eventLocation.filter(Boolean).join(', ') }] : null,
                        tooltip: 'The city, state, and country of the venue the event is taking place in.'
                    }],
                    [{
                        desc: 'Zip:',
                        children: dataEvent.zipCode ? [{ desc: dataEvent.zipCode }] : null,
                        tooltip: 'The zip code of the venue the event is taking place in.'
                    }],
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: (dataEvent.venue || dataEvent.address || eventLocation.filter(Boolean).length || dataEvent.zipCode) ? 'Location' : '',
                    tooltip: 'Details about the location for the event.'
                }
            }, {
                isTreeNodesWithPanel: true,
                treeNodes: [
                    [{
                        desc: dataEvent.pricingModels && dataEvent.pricingModels.length ? 'Pricing Model:' : '',
                        children: dataEvent.pricingModels && dataEvent.pricingModels.length ? dataEvent.pricingModels : '',
                        params: {
                            filter: 'pricingModels.id',
                            type: 'checkbox'
                        },
                        routing: '/events',
                        tooltip: 'The model for how the event is being priced.'
                    }],
                    [{
                        desc: 'Price:',
                        children: dataEvent.price ? [{ desc: dataEvent.price }] : null,
                        tooltip: 'More specific details about how the event is being priced.'
                    }]
                ],
                expansionPanel: {
                    expanded: true,
                    panelHeading: ((dataEvent.pricingModels && dataEvent.pricingModels.length) || (dataEvent.price && dataEvent.price.length)) ? 'Pricing' : '',
                    tooltip: 'Details related to the pricing of tickets for the event.'
                }
            }],
            subTabs: [{
                tabLabelName: 'Organizer Organizations',
                tabLabelTooltip: 'Organization that is organizing the event.',
                tabContent: [{
                    contentType: TabContentType.LIST,
                    list: {
                        data: organizerOrganizations
                    },
                    noDataFoundMsg: 'No organizations found.'
                }]
            }, {
                tabLabelName: 'Sponsor Organizations',
                tabLabelTooltip: 'All organizations that are sponsoring the event.',
                tabContent: [{
                    contentType: TabContentType.LIST,
                    list: {
                        data: sponsorOrganizations
                    },
                    noDataFoundMsg: 'No organizations found.'
                }]
            }, {
                tabLabelName: 'Organizer Data Providers',
                tabLabelTooltip: 'Data provider that is organizing the event, if applicable.',
                tabContent: [{
                    contentType: TabContentType.LIST,
                    list: {
                        data: organizerDataProviders
                    },
                    noDataFoundMsg: 'No providers found.'
                }]
            }, {
                tabLabelName: 'Sponsor Data Providers',
                tabLabelTooltip: 'Data providers that are sponsoring the event, if applicable.',
                tabContent: [{
                    contentType: TabContentType.LIST,
                    list: {
                        data: sponsorDataProviders
                    },
                    noDataFoundMsg: 'No providers found.'
                }]
            }]
        }];
    }

    loadAllTree(data: DataEvents) {
        this.overviewTabTopTree = [];
        if (this.dataEvent.startTime) {
            this.overviewTabTopTree.push({
                desc: 'Start Time (EST):',
                children: [{desc: this.datePipe.transform(data.startTime, 'medium', 'EST')}],
                tooltip: 'Start date and time of the event.'
            });
        }
        if (this.dataEvent.endTime) {
            this.overviewTabTopTree.push({
                desc: 'End Time (EST):',
                children: [{desc: this.datePipe.transform(data.endTime, 'medium', 'EST')}],
                tooltip: 'End date and time of the event.'
            });
        }
        if (this.dataEvent.lenthInDays) {
            this.overviewTabTopTree.push({
                desc: 'Length:',
                children: [{desc: data.lenthInDays + ' Days'}],
                tooltip: 'The length of the event, expressed in the number of days.'
            });
        }
        if (this.dataEvent.description) {
            this.overviewTabTopTree.push({
                desc: 'In Brief:',
                children: [{desc: data.description}],
                tooltip: 'Short description of the event.'
            });
        }

        this.loadTabs(data);
    }
    getLinkUrl(url) {
        if (url) {
            this.urlLinks = (url.indexOf('http://') !== -1 || url.indexOf('https://') !== -1) ? url : 'http://' + url;
           // window.open(this.urlLinks, '_blank');
        }
        // return this.urlLinks;
    }

}
