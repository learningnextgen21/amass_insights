import { Component, OnInit, ViewChild } from '@angular/core';
import { DataEvents } from './data-events.model';
import { DataEventsService } from './data-events.service';
import { ResponseWrapper, ITEMS_PER_PAGE, GenericFilterModel, AmassFilterService, Principal, GenericFilterComponent,AuthServerProvider,StateStorageService,SigninModalService } from 'app/shared';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { GoogleAnalyticsEventsService } from 'app/shared/googleanalytics/google-analytics-events.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from 'app/loader/loaders.service';
import { ApplyAccountComponent } from 'app/account';
import { SigninDialogComponent } from 'app/dialog/signindialog.component';
import { GenericProfileCardModel } from 'app/shared/generic-profile-card';
import { DatePipe } from '@angular/common';

const bob = require('elastic-builder');
@Component({
    selector: 'jhi-data-events',
    templateUrl: './data-events.component.html',
    styleUrls: [
        './data-events.component.scss'
    ]
})
export class DataEventsComponent implements OnInit {
    currentDate: any;
    minDate: any;
    maxDate: any;
    minUpdatedDate: any;
    dataEvents: DataEvents[];

    itemsPerPage: number;
	links: any;
	page: any;
    predicate: any;
    reverse: any;
    totalItems: number;
	itemsFrom: number;
	currentSearch: any;
	currentSearchQuery: string;
	currentFilter: string;
    currentSorting: any;

    sortingArray = [];
    sortedIndex: any;

    loader: any;
	loading: any;
    loaderInfinite: any;
    limitedEvents: any;

    subscription: any;

    reSubmitValues: any;
    completenessTooltip: any[];
    filters: GenericFilterModel[];
    statusFilter: any;
    paramFilter: string;
	paramID: string;
	paramType: string;
	paramCreatedDate: any;
    paramUpdatedDate: any;
    isAdmin: boolean;
    mdDialogRef: MatDialogRef<any>;
    limitedLogoutUsersEvent: boolean;
    @ViewChild(GenericFilterComponent) genericFilter: GenericFilterComponent;
    totalItemsTooltip: string;
    sortByTooltip: string;
    params: any;
    sortCategory: any;
    filterCheckBox: any;
    genericProfileCard: GenericProfileCardModel[] = [];
    unregisterUserMsg: boolean;
    pageName = 'list';

    constructor(
		public dialog: MatDialog,
		public _DomSanitizer: DomSanitizer,
		public googleAnalyticsService: GoogleAnalyticsEventsService,
        private dataEventsService: DataEventsService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private loaderService: LoaderService,
        private filterService: AmassFilterService,
        private principal: Principal,
        private datePipe: DatePipe,
        private stateService: StateStorageService,
        private authService: AuthServerProvider,
        private signinModalService: SigninModalService,

    ) {
        this.dataEvents = [];
		this.itemsPerPage = ITEMS_PER_PAGE;
		this.page = 0;
		this.itemsFrom = 0;
		this.links = {
			last: 0
		};
		this.predicate = 'id';
		this.reverse = true;
		this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
		this.currentFilter = '';
		this.currentSorting = '';
    }

    ngOnInit() {
        this.dialog.closeAll();
       /* this.currentDate = new Date();
       this.minDate = new Date(this.currentDate.getFullYear() - 2, this.currentDate.getMonth(), this.currentDate.getDate());
       this.maxDate = new Date(this.currentDate.getFullYear() + 5, this.currentDate.getMonth(), this.currentDate.getDate());
       this.minUpdatedDate = new Date(1970, 10, 1); */
        this.dataEvents = [];
        this.loaderService.display(true);
        this.loader = true;
        this.completenessTooltip = [];
        this.sortedIndex = '';
        this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
        this.activatedRoute.queryParams.subscribe(params => {
            this.params = params;
            if (params && params['filter'] && params['id'] && !this.isAuthenticated()) {
                this.router.navigate(['events']);
            }
        });
        if(!this.isAuthenticated()) {
            this.unregisterUserMsg = true; // dont remove this variable because of it is related to notification
        }
        if (this.isAuthenticated()) {
            this.filterService.setAuthorisedUrl();
            this.sortBy({ // default sort option for event component
                    key: 'startTime',
                    label: 'Start',
                    order: 'asc',
                    userLevel:'unrestricted',
                    mouseoverExplanation: ''
                });
        } else {
            this.filterService.setUnAuthorisedUrl();
            this.subscription = this.dataEventsService.unrestrictedSearch().subscribe(
                (res) => this.onSuccessFilter(res, res.headers),
                (res) => this.onError(res)
            );
            // this.openSignUp();
        }

        this.totalItemsTooltip = 'Number of events matching active search and filter criteria.';
        this.sortByTooltip = 'Sort list of events by clicking any of the sort fields.';
        this.sortingArray = [
			{
				key: 'eventEditionNames',
				label: 'Name',
                order: '',
                userLevel:'unrestricted',
                DropDown:'Name',
				mouseoverExplanation: 'Name of the Event edition.'
			},
			{
				key: 'topics.desc',
				label: 'Topic',
                order: '',
                userLevel:'unrestricted',
                DropDown:'Topic',
				mouseoverExplanation: 'Topic related to the event.'
            },
            {
                key: 'industries.desc',
                label: 'Industry',
                order: '',
                userLevel:'unrestricted',
                DropDown:'Industry',
                mouseoverExplanation: 'Data industry related to the event.'
            },
			/* {
				key: 'dataCategory.sortorder',
				label: 'Category',
                order: '',
                userLevel:'unrestricted',
				mouseoverExplanation: 'Data category related to the event.'
            }, */
            {
				key: 'startTime',
				label: 'Start',
                order: 'asc',
                userLevel:'unrestricted',
                DropDown:'Start',
				mouseoverExplanation: ''
            },
            {
				key: 'endTime',
				label: 'End',
                order: '',
                userLevel:'unrestricted',
                DropDown:'End',
				mouseoverExplanation: ''
			},
			{
				key: 'lastUpdated',
				label: 'Updated',
                order: '',
                userLevel:'unrestricted',
                DropDown:'Updated',
				mouseoverExplanation: 'Date the event\'s details were last updated.'
            },
            {
				key: 'createdTime',
				label: 'Added',
                order: '',
                userLevel:'unrestricted',
                DropDown:'Added',
				mouseoverExplanation: 'Date the event was discovered and added.'
			},
			{
				key: 'completeness',
				label: 'Completeness',
                order: '',
                userLevel:'unrestricted',
                DropDown:'Completeness',
				mouseoverExplanation: 'Percent of the event\'s details that have been filled in.'
            },
            {
				key: 'recommended',
                label: 'Recommended',
                type: 'recommended',
                DropDown:'Recommended',
                mouseoverExplanation: 'Sort by a combination of fields recommended by Amass: Start Date & Completeness',
                recommendedSorting: [
                    {
                        key: 'startTime',
                        label: 'Start Date',
                        order: 'asc',
                        DropDown:'Start Date',
                        type: 'recommended'
                    },
                    {
                        key: 'completeness',
                        label: 'Completeness',
                        order: 'desc',
                        DropDown:'Completeness',
                        type: 'recommended'
                    }
                ],
                order: '',
                userLevel:'unrestricted',
            }
        ];
        this.sortCategory = 'Events';
		this.filterCheckBox = 'Events';
        this.filters = [];
        this.constructFilters();
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    logoutUsers(events: any) {
        if (!this.isAuthenticated()) {
            this.openSignUp();
            this.router.navigate(['/events']);
            this.googleAnalyticsService.emitEvent('Events', 'Restricted Internal Link Clicked', events.label);
        } else {
            this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
        }
    }

    constructFilters() {
        const currentDate = new Date();
        const minDate = new Date(currentDate.getFullYear() - 2, currentDate.getMonth(), currentDate.getDate());
        const maxDate = new Date(currentDate.getFullYear() + 5, currentDate.getMonth(), currentDate.getDate());
        const minUpdatedDate = new Date(1970, 10, 1);
        const dataTopicFilterArray = [];
        const relatedDataIndustryFilterArray = [];
        const relatedDataCategoryFilterArray = [];
        const eventTypesFilterArray = [];
        if (this.isAuthenticated()) {
            this.filterService.getLookupCodes('EVENT_TYPE').subscribe(data => { // responce return the value based on the params
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                    eventTypesFilterArray.push({
                        id: item._source.id,
                        count: item._source.recordCount,
                        value: item._source.lookupcode,
                        checked: false
                    });
                }
            });
            this.filterService.getDataTopic('eventCount').subscribe(data => {
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                        dataTopicFilterArray.push({
                        id: item._source.id,
                        count: item._source.eventCount,
                        value: item._source.topicName,
                        checked: (!this.isAuthenticated() && item._source.id === 2) ? true : false,
                        locked: item.locked,
                        explanation: null
                    });
                }
            });
            this.filterService.getESFilters('dataindustry', 'eventIndustryCount', 'dataIndustry').subscribe(data => {
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                    if (this.isAdmin) {
                        relatedDataIndustryFilterArray.push({
                            id: item._source.id,
                            count: item._source.eventIndustryCount,
                            value: item._source.dataIndustry,
                            checked: false,
                            explanation: item._source.explanation
                        });
                    } else if (item._source.dataIndustry !== 'DATA_INDUSTRY_NULL') {
                        relatedDataIndustryFilterArray.push({
                            id: item._source.id,
                            count: item._source.eventIndustryCount,
                            value: item._source.dataIndustry,
                            checked: false,
                            explanation: item._source.explanation
                        });
                    }
                }
            });
            this.filterService.getESFilters('datacategory', 'eventCategoryCount', 'datacategory').subscribe(data => {
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                    if (this.isAdmin) {
                        relatedDataCategoryFilterArray.push({
                            id: item._source.id,
                            count: item._source.eventCategoryCount,
                            value: item._source.datacategory,
                            checked: false,
                            locked: item._source.locked,
                            explanation: item._source.explanation
                        });
                    } else if (item._source.datacategory !== 'DATA_CATEGORY_NULL') {
                        relatedDataCategoryFilterArray.push({
                            id: item._source.id,
                            count: item._source.eventCategoryCount,
                            value: item._source.datacategory,
                            checked: false,
                            locked: item._source.locked,
                            explanation: item._source.explanation
                        });
                    }
                }
            });
        } else {
            this.filterService.unRestrictedQsearch('lookupcode', 'EVENT_TYPE').subscribe(data => {
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                    eventTypesFilterArray.push({
                        id: item._source.id,
                        count: item._source.recordCount,
                        value: item._source.lookupcode,
                        checked: false
                    });
                }
            });
            this.filterService.unRestrictedQsearch('datatopic', '', 'eventCount').subscribe(data => {
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                        dataTopicFilterArray.push({
                        id: item._source.id,
                        count: item._source.eventCount,
                        value: item._source.topicName,
                        checked: (!this.isAuthenticated() && item._source.id === 2) ? true : false,
                        locked: item.locked,
                        explanation: null
                    });
                }
            });
            this.filterService.unRestrictedQsearch('dataindustry', '', 'eventIndustryCount').subscribe(data => {
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                    if (this.isAdmin) {
                        relatedDataIndustryFilterArray.push({
                            id: item._source.id,
                            count: item._source.eventIndustryCount,
                            value: item._source.dataIndustry,
                            checked: false,
                            explanation: item._source.explanation
                        });
                    } else if (item._source.dataIndustry !== 'DATA_INDUSTRY_NULL') {
                        relatedDataIndustryFilterArray.push({
                            id: item._source.id,
                            count: item._source.eventIndustryCount,
                            value: item._source.dataIndustry,
                            checked: false,
                            explanation: item._source.explanation
                        });
                    }
                }
            });
            this.filterService.unRestrictedQsearch('datacategory', '', 'eventCategoryCount').subscribe(data => {
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                    if (this.isAdmin) {
                        relatedDataCategoryFilterArray.push({
                            id: item._source.id,
                            count: item._source.eventCategoryCount,
                            value: item._source.datacategory,
                            checked: false,
                            locked: item._source.locked,
                            explanation: item._source.explanation
                        });
                    } else if (item._source.datacategory !== 'DATA_CATEGORY_NULL') {
                        relatedDataCategoryFilterArray.push({
                            id: item._source.id,
                            count: item._source.eventCategoryCount,
                            value: item._source.datacategory,
                            checked: false,
                            locked: item._source.locked,
                            explanation: item._source.explanation
                        });
                    }
                }
            });
        }

        const pricingModelFilterArray = [];
        if (this.isAuthenticated()) {
            this.filterService.getLookupCodes('EVENT_PRICING_MODEL').subscribe(data => {
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                    pricingModelFilterArray.push({
                        id: item._source.id,
                        count: item._source.recordCount,
                        value: item._source.description,
                        checked: false
                    });
                }
            });
        } else {
            this.filterService.unRestrictedQsearch('lookupcode', 'EVENT_PRICING_MODEL').subscribe(data => {
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                    pricingModelFilterArray.push({
                        id: item._source.id,
                        count: item._source.recordCount,
                        value: item._source.description,
                        checked: false
                    });
                }
            });
        }
        this.filters.push({
            filterName: 'Start Date',
			filterTerm: 'startTime',
			filterType: 'datepicker',
			filterNgModel: [minDate, maxDate],
            filterID: 'startTime',
            expanded: true,
            'minDate': minDate,
            'maxDate': maxDate,
			minRange: '',
            maxRange: '',
            minRangeInit: '',
            maxRangeInit: '',
			order: 1,
			mouseoverExplanation: ''
        });
        this.filters.push({
            filterName: 'End Date',
			filterTerm: 'endTime',
			filterType: 'datepicker',
			filterNgModel: [minDate, maxDate],
			filterID: 'endTime',
            'minDate': minDate,
            'maxDate': maxDate,
			minRange: '',
            maxRange: '',
            minRangeInit: '',
            maxRangeInit: '',
            expanded: false,
			order: 2,
			mouseoverExplanation: ''
        });
        this.filters.push({
            filterName: 'Related Topic',
			filterList: dataTopicFilterArray,
			filterType: 'checkbox',
			filterTerm: 'topics.id',
            expanded: false,
            order: 3,
            mouseoverExplanation: 'All of the topics related to the event.'
        });
        this.filters.push({
			filterName: 'Completeness',
			filterTerm: 'completeness',
			filterType: 'rangeFromTo',
			filterNgModel: [0, 100],
			filterID: 'completeness',
			minRange: 0,
			maxRange: 100,
			minRangeInit: 0,
			maxRangeInit: 100,
			rangeStep: 0.1,
			expanded: false,
			order: 4,
			mouseoverExplanation: 'Percent of the event\'s details that have been filled in.'
        });
        this.filters.push({
			filterName: 'Related Data Industry',
			filterList: relatedDataIndustryFilterArray,
			filterTerm: 'industries.id',
			filterType: 'checkbox',
			expanded: false,
			order: 5,
			mouseoverExplanation: 'All the industries related to the event.'
        });
        this.filters.push({
			filterName: 'Related Data Category',
			filterList: relatedDataCategoryFilterArray,
			filterType: 'checkbox',
			filterTerm: 'categories.id',
			expanded: false,
			order: 6,
			mouseoverExplanation: 'All the types of data or information related to the event.'
        });
        this.filters.push({
			filterName: 'Event Type',
			filterList: eventTypesFilterArray,
			filterType: 'checkbox',
			filterTerm: 'eventTypes.id',
            expanded: false,
			order: 7,
			mouseoverExplanation: ''
        });
        this.filters.push({
			filterName: 'Length (in Days)',
			filterTerm: 'lenthInDays',
			filterType: 'rangeFromTo',
			filterNgModel: [0, 14],
			filterID: 'lenthInDays',
			minRange: 0,
			maxRange: 14,
			minRangeInit: 0,
			maxRangeInit: 14,
			rangeStep: 1,
            expanded: false,
            hidden: true,
			order: 8,
			mouseoverExplanation: ''
        });
        this.filters.push({
			filterName: 'Pricing Model',
			filterList: pricingModelFilterArray,
			filterTerm: 'pricingModels.id',
			filterType: 'checkbox',
			hidden: true,
			order: 9,
			mouseoverExplanation: 'How the event is being priced.'
        });
        this.filters.push({
            filterName: 'Last Updated Date',
			filterTerm: 'lastUpdated',
			filterType: 'datepicker',
            filterNgModel: [minUpdatedDate, currentDate],
			filterID: 'lastUpdated',
            hidden: true,
            minDate: minUpdatedDate,
            maxDate: currentDate,
			minRange: '',
            maxRange: '',
            minRangeInit: '',
            maxRangeInit: '',
			order: 10,
			mouseoverExplanation: 'Date the event\'s details were last updated.'
        });
        this.filters.push({
			filterName: 'Date Added',
			filterTerm: 'createdTime',
			filterType: 'datepicker',
			filterNgModel: [minDate, currentDate],
			filterID: 'createdTime',
            'minDate': minDate,
            maxDate: currentDate,
			minRange: '',
            maxRange: '',
            minRangeInit: '',
            maxRangeInit: '',
            hidden: true,
			order: 11,
			mouseoverExplanation: 'Date the event was discovered and added.'
        });
        const _that = this;
        this.loader = true;
        if (this.isAuthenticated()) {
            setTimeout(() => {
                if (!_that.params['filter1']) {
                    _that.defaultFilterChanges(true);
                }
                if (_that.params['createdPastDays']) {
                    const date = new Date();
                    const pastDate = date.setDate(date.getDate() - _that.params['createdPastDays']);
                    _that.genericFilter.datepickerValueChanged([minDate, new Date(pastDate)], 'createdTime', 'datepicker', minDate, new Date(pastDate), 10);
                }
                if (_that.params['updatedPastDays']) {
                    const date = new Date();
                    const pastDate = date.setDate(date.getDate() - _that.params['updatedPastDays']);
                    _that.genericFilter.datepickerValueChanged([minDate, new Date(pastDate)], 'lastUpdated', 'datepicker', minDate, new Date(pastDate), 10);
                }
                _that.loader = false;
            }, 1000);
        }
    }

    sortBy(column: any) {
        if (this.isAuthenticated()) {
            this.dataEvents = [];
            this.links = {
                last: 0
            };
            this.page = 0;
            this.itemsFrom = 0;
            if (!column) {
                return this.clear();
            }
            const index = column === 'none' ? -1 : this.sortingArray.indexOf(column);
            this.sortedIndex = index;
            for (let i = 0; i < this.sortingArray.length; i++) {
                if (i === index) {
                    this.sortingArray[index].order = (column.order === 'desc') ? 'asc' : 'desc';
                } else {
                    this.sortingArray[i].order = '';
                }
            }
            if (column.key === 'recommended') {
                for (let i = 0; i < column.recommendedSorting.length; i++) {
                    column.recommendedSorting[i].order = column.order;
                }
            }

            this.currentSorting = column !== 'none' ? column : '';
            if (column === 'none') {
                this.currentSorting = '';
                return;
            }
            this.loadAll();
        } else {
            this.openSignUp();
        }
    }
    openSigninDialog() {
        this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
            width: '400px'
        });
    }

    openApplyDialog() {
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        });
    }

    openSignUp() {
        if (this.dialog) {
            this.dialog.closeAll();
        }
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        });
        return;
    }

    loadAll() {
        this.limitedEvents = '';
		this.loader = true;
        let queryBody = {};
        if (this.isAuthenticated()) {
        if (this.currentSorting && this.currentFilter && this.currentSearch) {
            const requestBody = bob.requestBodySearch();
        if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
            requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
        } else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
            for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
                requestBody.sort(bob.sort(this.currentSorting.recommendedSorting[i]['key']).order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')).size(20);
            }
        }
        const queryJSON = requestBody.toJSON();
            queryJSON['query'] = {
                query_string: this.currentSearch
            };
            queryJSON['filter'] = this.currentFilter;
            queryBody = queryJSON;
        } else if (this.currentSorting && this.currentFilter) {
            const requestBody = bob.requestBodySearch();
        if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
            requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
        } else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
            for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
                requestBody.sort(bob.sort(this.currentSorting.recommendedSorting[i]['key']).order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')).size(20);
            }
        }
        const queryJSON = requestBody.toJSON();
            queryJSON['filter'] = this.currentFilter;
            queryBody = queryJSON;
        } else if (this.currentFilter && this.currentSearch) {
            queryBody = {
                size: 20,
                query: {
                    query_string: this.currentSearch
                },
                filter: this.currentFilter['query']
            };
        } else if (this.currentSorting && this.currentSearch) {
            const requestBody = bob.requestBodySearch();
        if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
            requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
        } else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
            for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
                requestBody.sort(bob.sort(this.currentSorting.recommendedSorting[i]['key']).order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')).size(20);
            }
        }
        const queryJSON = requestBody.toJSON();
            queryJSON['query'] = {
                query_string: this.currentSearch
            };
            queryBody = queryJSON;
        } else {
            if (this.currentSearch) {
                queryBody = {
                    size: 20,
                    query: {
                        query_string: this.currentSearch
                    }
                };
            }
            if (this.currentFilter) {
                queryBody = {
                    size: 20,
                    filter: this.currentFilter['query']
                };
            }
            if (this.currentSorting) {
                const requestBody = bob.requestBodySearch();
                if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
                    requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
                } else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
                    for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
                        requestBody.sort(bob.sort(this.currentSorting.recommendedSorting[i]['key']).order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')).size(20);
                    }
                }
                const queryJSON = requestBody.toJSON();
                queryBody = queryJSON;
            }
        }
        } else {
            queryBody = {};
            this.dialog.closeAll();
        }
        if (this.page) {
            queryBody['from'] = this.itemsFrom;
        }
        if ( this.subscription) {
            this.subscription.unsubscribe();
        }
		this.subscription = this.dataEventsService.queryWithSearchFilter(queryBody).subscribe(
			(res) => this.onSuccessFilter(res, res.headers),
			(res) => this.onError(res)
		);
		return;
	}

    loadPage(page) {
        this.limitedEvents = '';
		this.page = page;
        this.itemsFrom = (this.page === 0) ? 0 : (this.page * 20);
        this.loader = false;
		if (this.itemsFrom <= this.totalItems) {
            this.limitedEvents = '';
			if (this.page <= 4 && this.isAuthenticated()) {
				this.loadAll();
                this.limitedEvents = '';
                this.loaderInfinite = true;
                this.loader = false;
			} else if (this.page <= -1 && !this.isAuthenticated()) {
                this.loadAll();
                this.loaderInfinite = true;
                this.loader = false;
            } else {
                if (!this.isAuthenticated()) {
                    this.limitedLogoutUsersEvent = true;
                    this.loaderInfinite = false;
                    this.loader = false;
                } else {
                    this.limitedLogoutUsersEvent = false;
                    this.loaderInfinite = false;
                    this.limitedEvents = 'Maximum number of entries listed. Please refine your search/filtering criteria to display the most appropriate entries.';
                }
			}

		}
    }

    search(query) {
        if (this.isAuthenticated()) {
            this.googleAnalyticsService.emitEvent('Events', 'Search Submitted', 'Events List - Search - Search Button - ' + "'" + query + "'");
            if (!query) {
                return this.clear();
            }
            this.dataEvents = [];
            this.links = {
                last: 0
            };
            this.page = 0;
            this.predicate = '_score';
            this.reverse = false;
            this.itemsFrom = 0;
            this.currentSearchQuery = query;
            this.currentSearch = {
                'query': query + '*',
                'fields': ['_all', 'eventEditionNames^2']
            };
            this.loadAll();
        } else {
             this.openSignUp();
             this.googleAnalyticsService.emitEvent('Events', 'Restricted Internal Link Clicked', 'Events List - Search - Search Button' + '-' + query);
        }
    }
    searchClear(events: any) {
        if (this.isAuthenticated()) {
		    this.loader = true;
            this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
        } else {
            this.googleAnalyticsService.emitEvent('Events', 'Restricted Internal Link Clicked', events.label);
        }
    }
    eventProfile(events: any, recordID) {
        if (this.isAuthenticated()) {
            this.loader = true;
            this.router.navigate(['events', recordID]);
            this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
        } else {
            this.openSignUp();
            this.googleAnalyticsService.emitEvent('Events', 'Restricted Internal Link Clicked', events.label);
        }
	}

    onFilterChanges(event) {
        if (this.isAuthenticated()) {
            if (event === 'clearFilter') {
                this.onFilter({
                    query: 'clearFilter'
                });
                return;
            }
            if (event && event.selectedFilterData && event.selectedFilterData.length) {
                const requestBody = bob.requestBodySearch();
                const allFilterQuery = bob.boolQuery();
                const filters = event.selectedFilterData;
                const filtersLength = filters.length;
                for (let i = 0; i < filtersLength; i++) {
                    const term = filters[i].queryTerm;
                    const filter = filters[i].filters;
                    // console.log(filter);
                    if (filters[i].filters && filter.length) {
                        if ((filters[i].queryType === 'must' || filters[i].queryType === 'should') && term === 'marketplaceStatus') {
                            this.statusFilter = '';
                            if (filter === 'Followed' || filter === 'Unlocked') {
                                this.statusFilter = filter;
                            } else if (filter === 'Complete') {
                                allFilterQuery.should(bob.matchQuery(term, 'Expanded'));
                                allFilterQuery.should(bob.matchQuery(term, 'Detailed'));
                                allFilterQuery.should(bob.matchQuery(term, 'Followed'));
                                allFilterQuery.should(bob.matchQuery(term, 'Unlocked'));
                                this.statusFilter = '';
                            } else if (filter !== 'Followed' && filter !== 'Unlocked') {
                                allFilterQuery.should(bob.matchQuery(term, filter));
                            }
                        } else if (filters[i].queryType === 'range' || filters[i].queryType === 'rangeFromTo' || filters[i].queryType === 'datepicker' || filters[i].queryType === 'datepickerSingle') {
                            if (typeof filter === 'string') {
                                if (filters[i].queryType === 'datepicker') {
                                    allFilterQuery.must(bob.rangeQuery(term).from(this.datePipe.transform(filter,'yyyy-MM-dd', 'EST')));
                                } else {
                                    allFilterQuery.must(bob.rangeQuery(term).from(filter));
                                }
                            } else {
                                allFilterQuery.must(bob.rangeQuery(term).from(filter[0][0]).to(filter[0][1]));
                            }
                        } else if (filters[i].queryType === 'rangeBetween') {
                            allFilterQuery.must(bob.rangeQuery(term).gte(filter[0][0]).lte(filter[0][1]));
                        } else {
                            allFilterQuery.must(bob.termsQuery(term, filter));
                        }
                    }
                }

                requestBody.query(allFilterQuery);

                this.onFilter({
                    query: requestBody.toJSON(),
                    'statusFilter': this.statusFilter
                });
            }
            return;
        } else {
            this.activatedRoute.queryParams.subscribe(params => {
                if (params && params['utm_medium'] !== 'email' && !this.isAuthenticated()) {
                    this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
                        width: '400px'
                    });

                } else if (params && params['utm_medium'] === 'email' && !this.isAuthenticated()) {
                    this.stateService.storeUrl(this.router.url);
                    this.router.navigate(['accessdenied']);
                    this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
                        width: '400px'
                    });
                }
            });
        }
    }

    onFilter(options: any) {
		if (!options.query || options.query === 'clearFilter') {
			this.currentFilter = '';
			this.onClearFilter();
		} else {
			this.currentFilter = options.query ? options.query : '';
		}
		this.statusFilter = options.statusFilter ? options.statusFilter : '';
		this.dataEvents = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.itemsFrom = 0;
		this.reverse = false;
		this.loadAll();
	}

	onClearFilter() {
		this.paramFilter = '';
		this.paramID = '';
		this.currentFilter = '';
		this.statusFilter = '';
		this.dataEvents = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.itemsFrom = 0;
		this.reverse = false;
		this.loadAll();
        this.router.navigate(['/events']);
		return;
	}

    clear() {
            this.dataEvents = [];
            this.links = {
                last: 0
            };
            this.page = 0;
            this.predicate = 'id';
            this.reverse = true;
            this.currentSearch = '';
            this.currentSearchQuery = '';
            this.loadAll();
	}
    onKeydown(event: any) {
        if (!this.principal.isAuthenticated()) {
           this.openSignUp();
        } else {
            this.googleAnalyticsService.emitEvent('Events', 'Search Submitted', 'Events List - Search - Textbox');
        }
    }
    private onSuccessFilter(data, headers) {
		this.loader = false;
        this.loaderInfinite = false;
        this.loaderService.display(false);
		this.totalItems = data['hits'] ? data['hits'].total : 0;
        this.links.last = Math.ceil(this.totalItems / 20);
        this.genericProfileCard = [];
		if (!this.page) {
			this.dataEvents = [];
		}
		if (data['hits'] && data['hits'].hits) {
			for (let i = 0; i < data['hits'].hits.length; i++) {
                const eventsObj = data['hits'].hits[i]._source;
                this.dataEvents.push(eventsObj);
                this.completenessTooltip.push(data['hits'].hits[i]._source.completeness + '% ' + 'percent of this event\'s detail that has been filled in');
            }
            this.dataEvents.forEach((item, index) => {
               this.genericProfileCard.push({
                content: [{
                    columns: 1,
                    columnSize: [12],
                    columnContent: [
                        {
                            title: item.eventEditionNames,
                            url: '/#/events/'+ item.recordID,
                            userPermission: false
                        }]
                   },{
                    columns: 3,
                    columnSize: [4 ,4 ,4],
                    columnContent: [
                        {
                            label: item.topics ? 'Topics:' : null,
                            arrayText: item.topics,
                            filterKey: 'topics.id',
                            url: '/#/events',
                            filterType: 'checkbox'
                       },
                       {
                            label: item.industries ? 'Industries:' : null,
                            arrayText: item.industries,
                            filterKey: 'industries.id',
                            url: '/#/events',
                            filterType: 'checkbox'
                       },
                       {
                            label: item.categories ? 'Categories:' : null,
                            arrayText: item.categories,
                            filterKey: 'categories.id',
                            url: '/#/events',
                            filterType: 'checkbox'
                       }
                    ]
                },{
                    columns: 1,
                    columnSize: [12],
                    columnContent:[
                    {
                        plainText: item.description,
                    }]
                },
                {
                    columns: 3,
                    columnSize: [4, 4, 4],
                    columnContent:[
                    {
                        label: 'Start:',
                        plainText: this.datePipe.transform(item.startTime, 'mediumDate', 'EST')
                    },
                    {
                        label: 'End:',
                        plainText: this.datePipe.transform(item.endTime, 'mediumDate', 'EST')
                    },
                    {
                        completeness: item.completeness
                    }]
                }],
            })
        });
		}
		if (this.totalItems === 0 && this.currentSearchQuery && !this.reSubmitValues) {
			this.reSubmitValues = true;
			setTimeout(() => {
				this.search(this.currentSearchQuery);
			}, 3000);
		} else {
			this.reSubmitValues = false;
		}
    }

    private onError(error) {
		this.loader = false;
		this.loaderInfinite = false;
    }

    trackByRecordID(index, dataEvent) {
		return dataEvent.recordID;
    }

    defaultFilterChanges(event) {
        if (event === true) {
            this.currentDate = new Date();
            this.minDate = new Date(this.currentDate.getFullYear() - 2, this.currentDate.getMonth(), this.currentDate.getDate());
            this.maxDate = new Date(this.currentDate.getFullYear() + 5, this.currentDate.getMonth(), this.currentDate.getDate());
            this.genericFilter.datepickerValueChanged([this.currentDate, null], 'startTime', 'datepicker', this.minDate, this.maxDate, 0);

        }
    }

    onExpireSession(event) {
		//console.log('eventcheck',event);
        if (event) {
			//console.log('eventcheck',event);
           // this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
				setTimeout(() => {
				this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
			}, 2000);
			}); 
        }
    }
	  beforeSessionExpires(event) {
        if (event) {
           // this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }
}
