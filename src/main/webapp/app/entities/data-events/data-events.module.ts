import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    DataEventsService,
    DataEventsComponent,
    dataEventsRoute,
} from './';
import { ShareButtonModule } from '@ngx-share/button';
import { DataEventsDetailComponent } from './data-events-detail.component';
import { GenericProfileModule } from 'app/shared/generic-profile';
import { GenericProfileCardModule } from 'app/shared/generic-profile-card/generic-profile-card.module';

const ENTITY_STATES = [
    ...dataEventsRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        ShareButtonModule,
        GenericProfileModule,
        GenericProfileCardModule
    ],
    declarations: [
        DataEventsComponent,
        DataEventsDetailComponent
    ],
    entryComponents: [
        DataEventsComponent,
    ],
    providers: [
        DataEventsService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceDataEventsModule {}
