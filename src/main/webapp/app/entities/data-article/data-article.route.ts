import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService,UserRouteAccessUnrestrictedService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DataArticleComponent } from './data-article.component';
import { DataArticleDetailComponent } from './data-article-detail.component';
import { DataArticlePopupComponent } from './data-article-dialog.component';
import { DataArticleDeletePopupComponent } from './data-article-delete-dialog.component';
import { DataNewArticleComponent } from './data-new-article.component';

export const dataArticleRoute: Routes = [
	{
		path: 'news',
		data: {
			authorities: ['ROLE_USER'],
			pageTitle: 'datamarketplaceApp.dataArticle.home.title',
			breadcrumb: 'News',
			type: 'dynamic'
		},
		canActivate: [UserRouteAccessUnrestrictedService],
		children: [
			{
			  path: ':recordID',
			  data: {
				authorities: ['ROLE_USER'],
				pageTitle: 'datamarketplaceApp.dataArticle.home.blogTitle',
				breadcrumb: 'Article',
				type: 'dynamic'
			  },
			  canActivate: [UserRouteAccessService],
			  component: DataNewArticleComponent
			}, {
				path: '',
				component: DataArticleComponent,
				data: {
					breadcrumb: '',
					type: 'dynamic'
				}
			}
		]
    }
];

export const dataArticlePopupRoute: Routes = [
	{
		path: 'data-article-new',
		component: DataArticlePopupComponent,
		data: {
			authorities: ['ROLE_USER'],
			pageTitle: 'datamarketplaceApp.dataArticle.home.title'
		},
		canActivate: [UserRouteAccessService],
		outlet: 'popup'
	},
	/* {
		path: 'data-article/:id/edit',
		component: DataArticlePopupComponent,
		data: {
			authorities: ['ROLE_USER'],
			pageTitle: 'datamarketplaceApp.dataArticle.home.title'
		},
		canActivate: [UserRouteAccessService],
		outlet: 'popup'
	},
	{
		path: 'data-article/:id/delete',
		component: DataArticleDeletePopupComponent,
		data: {
			authorities: ['ROLE_USER'],
			pageTitle: 'datamarketplaceApp.dataArticle.home.title'
		},
		canActivate: [UserRouteAccessService],
		outlet: 'popup'
	} */
];
