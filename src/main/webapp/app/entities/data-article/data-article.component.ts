import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { DomSanitizer } from '@angular/platform-browser';

import { DataArticle } from './data-article.model';
import { DataArticleService } from './data-article.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, GenericFilterListModel, GenericFilterModel, ShareButtonsModel, StateStorageService, GenericFilterComponent ,AuthServerProvider,SigninModalService} from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { AmassFilterService } from '../../shared/genericfilter/filters.service';
import { LoaderService } from '../../loader/loaders.service';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
const bob = require('elastic-builder');
import { ApplyAccountComponent } from '../../account/apply/apply.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SigninDialogComponent } from 'app/dialog/signindialog.component';
import { DatePipe } from '@angular/common';
import { GenericProfileCardModel } from 'app/shared/generic-profile-card';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';
import { CookieService } from 'ngx-cookie';

@Component({
	selector: 'jhi-data-article',
	templateUrl: './data-article.component.html',
	styleUrls: ['./data-article.component.css', '../../shared/share-buttons/share-buttons.scss']
})
export class DataArticleComponent implements OnInit, OnDestroy {

	dataArticles: DataArticle[];
	currentAccount: any;
	eventSubscriber: Subscription;
	itemsPerPage: number;
	links: any;
	page: any;
	predicate: any;
	queryCount: any;
	subscription: any;
	reverse: any;
	totalItems: number;
	itemsFrom: number;
	currentFilter: string;
	currentSearch: any;
	currentSearchQuery: string;
	currentSorting: any;
	sortingArray = [];
    sortedIndex: any;
    params: any;
	paramFilter: string;
    paramID: string;
    loader: any;
    loaderInfinite: any;
    filters: GenericFilterModel[];
    statusFilter: any;
    isAdmin: boolean;
    sortCategory: string;
    filterCheckBox: any;
    limitedProviders: any;
    articlePurpose: any;
    articleTopics: any;
    articleRelevantProviders: any;
    logos: any;
    providerName: any;
    relevantProviderRecordId: any
    amassFilterId: any;
    cookiejson: any;
    pageName = 'list';
    isSortByBarCollapsed: boolean;
    currentUrl: any;
    shareButtons: ShareButtonsModel[];
    emailText: string;
    logoutUsers: boolean;
    logoutUserClick: boolean;
    mdDialogRef: MatDialogRef<any>;
    sortLabel: any;
    relevantProviderLogo: any;
    unregisterUserMsg: boolean;
    @ViewChild(GenericFilterComponent) genericFilter;
    genericProfileCard: GenericProfileCardModel[] = [];

	constructor(
		private dataArticleService: DataArticleService,
		private alertService: JhiAlertService,
		private dataUtils: JhiDataUtils,
		private eventManager: JhiEventManager,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private principal: Principal,
        public _DomSanitizer: DomSanitizer,
        private filterService: AmassFilterService,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private loaderService: LoaderService,
        public dialog: MatDialog,
        private datePipe: DatePipe,
        private stateService: StateStorageService,
        private cookieService: CookieService,
        private authService: AuthServerProvider,
	    private signinModalService: SigninModalService,
	) {
		this.dataArticles = [];
		this.itemsPerPage = ITEMS_PER_PAGE;
		this.itemsFrom = 0;
		this.page = 0;
		this.links = {
			last: 0
		};
		this.predicate = 'id';
		this.reverse = true;
		this.currentSearchQuery = '';
		this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
		this.currentSorting = '';
        this.isSortByBarCollapsed = true;
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
	}

	loadAll() {
        let queryBody = {};
        if (this.isAuthenticated()) {
            if (this.currentSorting && this.currentFilter && this.currentSearch) {
                const requestBody = bob.requestBodySearch();
                requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
                const queryJSON = requestBody.toJSON();
                queryJSON['query'] = {
                    query_string: this.currentSearch
                };
                queryJSON['filter'] = this.currentFilter;
                queryBody = queryJSON;
            } else if (this.currentSorting && this.currentFilter) {
            // this.loader = true;
                const requestBody = bob.requestBodySearch();
                requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
                const queryJSON = requestBody.toJSON();
                queryJSON['filter'] = this.currentFilter;
                queryBody = queryJSON;
            } else if (this.currentFilter && this.currentSearch) {
                queryBody = {
                    size: 20,
                    query: {
                        query_string: this.currentSearch
                    },
                    filter: this.currentFilter['query']
                };
            } else if (this.currentSorting && this.currentSearch) {
                const requestBody = bob.requestBodySearch();
                requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
                const queryJSON = requestBody.toJSON();
                queryJSON['query'] = {
                    query_string: this.currentSearch
                };
                queryBody = queryJSON;
            } else {
                if (this.currentSearch) {
                    queryBody = {
                        size: 20,
                        query: {
                            query_string: this.currentSearch
                        }
                    };
                }
                if (this.currentFilter) {
                    queryBody = {
                        size: 20,
                        filter: this.currentFilter['query']
                    };
                }
                if (this.currentSorting) {
                    const requestBody = bob.requestBodySearch();
                    requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
                    const queryJSON = requestBody.toJSON();
                    queryBody = queryJSON;
                }
            }
        } else {
            queryBody = {};
            this.dialog.closeAll();
        }
		if (this.page) {
			queryBody['from'] = this.itemsFrom;
		}
		if ( this.subscription ) {
            this.subscription.unsubscribe();
        }
        if (this.isAuthenticated()) {
            this.subscription = this.dataArticleService.elasticSearchAuthenticated(queryBody).subscribe(
                (res) => this.onSuccess(res, res.headers),
                (res) => this.onError(res)
            );
            this.unregisterUserMsg = true;
        } else {
            this.subscription = this.dataArticleService.elasticSearch({}).subscribe(
                (res) => this.onSuccess(res, res.headers),
                (res) => this.onError(res)
            );
        }

		return;
	}

	reset() {
		this.page = 0;
		this.dataArticles = [];
        this.loadAll();
	}
    isAuthenticated() {
        return this.principal.isAuthenticated();
    }
	loadPage(page) {
		this.page = page;
		this.itemsFrom = (this.page === 0) ? 0 : (this.page * 20);
		if (this.itemsFrom <= this.totalItems) {
            if (this.page <= 4 && this.isAuthenticated()) {
                this.loadAll();
                this.loaderInfinite = true;
                this.loader = false;
            } else if (this.page <= -1 && !this.isAuthenticated()) {
                this.loadAll();
                this.loaderInfinite = true;
                this.loader = false;
                this.logoutUsers = true;
            } else {
                if (!this.isAuthenticated()) {
                    this.logoutUsers = true;
                } else {
                    this.logoutUsers = false;
                    this.limitedProviders = 'Maximum number of entries listed. Please refine your search/filtering criteria to display the most appropriate entries.';
                }
            }
		}
	}

	clear() {
            this.loader = true;
            this.dataArticles = [];
            this.links = {
                last: 0
            };
            this.page = 0;
            this.predicate = 'id';
            this.reverse = true;
            this.currentSearch = '';
            this.currentSearchQuery = '';
            this.loadAll();
	}

	search(query) {
        if (this.isAuthenticated()) {
            this.googleAnalyticsService.emitEvent('Articles', 'Search Submitted', 'Articles List - Search - Search Button' + ' - ' + '\'' + query + '\'');
        this.loader = true;
		if (!query) {
			return this.clear();
		}
		this.dataArticles = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.predicate = '_score';
		this.reverse = false;
		this.itemsFrom = 0;
		this.sortBy({key: 'title', order: 'desc'});
		this.currentSearchQuery = query;
		this.currentSearch = {
			'query': query,
			'fields': ['title^2']
		};
        this.loadAll();
       } else {
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '570px'
        });
       }
    }
    searchClear(events: any) {
        if (this.isAuthenticated()) {
            this.loader = true;
            this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
        }
    }
    articleTitle(events: any) {
      this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
        if (!this.principal.isAuthenticated()) {
            this.router.navigate(['/news']);
            this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            });
        }
    }
    /* articleLink(url) {
        if (this.isAuthenticated()) {
            window.open(url, '_blank');
        } else {
            this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            });
        }
    } */
    purposeLink(purpose, type) {
        if (!this.principal.isAuthenticated()) {
            this.router.navigate(['/news']);
            this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            });
            if(type === 'Purposes') {
                this.googleAnalyticsService.emitEvent('Purpose', 'Restricted Internal Link Clicked', 'Articles List - Article - Purposes' + ' - ' + purpose);
            } else if(type === 'Topics') {
                this.googleAnalyticsService.emitEvent('Topic', 'Restricted Internal Link Clicked', 'Articles List - Article - Topic' + ' - ' + purpose);
            }
        }
        if(type === 'Purposes') {
            this.googleAnalyticsService.emitEvent('Purpose', 'Internal Link Clicked', 'Articles List - Article - Purposes' + ' - ' + purpose);
        } else if(type === 'Topics') {
            this.googleAnalyticsService.emitEvent('Topic', 'Internal Link Clicked', 'Articles List - Article - Topic' + ' - ' + purpose);
        }
    }
    onKeydown(events: any) {
        if (this.isAuthenticated()) {
            this.googleAnalyticsService.emitEvents(events.keyword, this.currentUrl);
            this.googleAnalyticsService.emitEvent('Articles', 'Search Submitted', 'Articles List - Search - Textbox');
        } else {
            this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            });
        }
    }
	ngOnInit() {
        //this.filterService.setAuthorisedUrl();
        this.dialog.closeAll();
        //this.isAuthenticated();
        this.currentUrl = this.router.url;
        this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
        this.loaderService.display(true);
        this.loader = false;
        this.currentSorting = {key: 'publishedDate', order: 'desc', userLevel:'unrestricted'};
		this.activatedRoute.queryParams.subscribe(params => {
            this.params = params;
            if (params && params['filter'] && params['id'] && !this.isAuthenticated()) {
                this.router.navigate(['news']);
            }
			if (params['filter'] && params['id']) {
				this.paramFilter = params['filter'];
				this.paramID = params['id'];
            }

            if (!params || !params['utm_medium']) {
                this.loadAll();
                this.registerChangeInDataArticles();
            }
        });

		this.sortingArray = [
			{
				key: 'title',
				label: 'Title',
                order: '',
                DropDown:'Title',
                userLevel:'unrestricted',
			},
			{
				key: 'author',
				label: 'Writer',
                order: '',
                DropDown:'Writer',
                userLevel:'unrestricted',
			},
			{
				key: 'publishedDate',
				label: 'Published',
                order: 'desc',
                DropDown:'Published',
                userLevel:'unrestricted',
			},
			{
				key: 'createdDate',
				label: 'Added',
                order: '',
                DropDown:'Added',
                userLevel:'unrestricted',
			},
			{
				key: 'updatedDate',
				label: 'Updated',
                order: '',
                DropDown:'Updated',
                userLevel:'unrestricted',
			}
        ];
        this.sortCategory = 'Articles';
        this.filterCheckBox = 'Articles';
		this.sortedIndex = '';

        this.filters = [];
        const dataTopicFilterArray = [];
        const dataPurposeFilterArray = [];
        const dataPurposeTypeFilterArray = [];
        const amassFilterArray = [];
        if (this.isAuthenticated()) {
            this.filterService.getDataTopic().subscribe(data => {
                this.loader = true;
                this.loaderService.display(false);
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                        dataTopicFilterArray.push({
                        id: item._source.id,
                        count: item._source.articleCount,
                        value: item._source.topicName,
                        checked: !this.isAuthenticated() && item._source.id === 2 ? true : false,
                        locked: item.locked,
                        explanation: null
                    });
                }
            });
            this.filterService.getDataSourcePurpose().subscribe(data => {
                this.loader = true;
                this.loaderService.display(false);
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                    dataPurposeFilterArray.push({
                        id: item._source.id,
                        count: item._source.articleCount,
                        value: item._source.purpose,
                        checked: false,
                        locked: item.locked,
                        explanation: item._source.explanation
                    });
                }
            });
            this.filterService.getDataSourcePurposeType().subscribe(data => {
                this.loader = true;
                this.loaderService.display(false);
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                    if(item._source.purposeType !== '') {
                        dataPurposeTypeFilterArray.push({
                            id: item._source.id,
                            count: item._source.articleCount,
                            value: item._source.purposeType,
                            checked: false,
                            locked: item.locked,
                            explanation: item._source.explanation
                        });
                    }

                }
            });
        } else {
            this.filterService.unRestrictedQsearch('datatopic', '', 'articleCount').subscribe(data => {
                this.loader = true;
                this.loaderService.display(false);
                if(data['hits'] && data['hits'].hits) {
                    const listItems = data['hits'].hits;
                    for (let i = 0; i < listItems.length; ++i) {
                        const item = listItems[i];
                            dataTopicFilterArray.push({
                            id: item._source.id,
                            count: item._source.articleCount,
                            value: item._source.topicName,
                            checked: !this.isAuthenticated() && item._source.id === 2 ? true : false,
                            locked: item.locked,
                            explanation: null
                        });
                    }
                }
            });
            this.filterService.unRestrictedQsearch('dataresourcepurpose', '', 'articleCount').subscribe(data => {
                this.loader = true;
                this.loaderService.display(false);
                if(data['hits'] && data['hits'].hits) {
                    const listItems = data['hits'].hits;
                    for (let i = 0; i < listItems.length; ++i) {
                        const item = listItems[i];
                        dataPurposeFilterArray.push({
                            id: item._source.id,
                            count: item._source.articleCount,
                            value: item._source.purpose,
                            checked: false,
                            locked: item.locked,
                            explanation: item._source.explanation
                        });
                    }
                }
            });
            this.filterService.unRestrictedQsearch('dataresourcepurposetype', '', 'articleCount').subscribe(data => {
                this.loader = true;
                this.loaderService.display(false);
                const listItems = data['hits'].hits;
                for (let i = 0; i < listItems.length; ++i) {
                    const item = listItems[i];
                    dataPurposeTypeFilterArray.push({
                        id: item._source.id,
                        count: item._source.articleCount,
                        value: item._source.purposeType,
                        checked: false,
                        locked: item.locked,
                        explanation: item._source.explanation
                    });
                }
            });
        }
        this.dataArticleService.amassFilter().subscribe(data => {
            const relevantArticles = data['hits'].hits;
            for(let i = 0; i < relevantArticles.length; i++) {
                const item = relevantArticles[i];
                this.amassFilterId = item._source.id;
                amassFilterArray.push({
                    id: item._source.id,
                });
            }
        });
        this.filters.push({
            filterName: '',
            filterList: [{
                id: 211,
                label: 'Amass Insights in the News',
                value: 211,
                checked: false
            }],
            filterTerm: 'relevantProviders.id',
            filterType: 'checkbox',
            order: 1,
        });
        this.filters.push({
            filterName: 'Topic',
			filterList: dataTopicFilterArray,
			filterType: 'checkbox',
			filterTerm: 'articleTopics.id',
            expanded: true,
            order: 2,
            mouseoverExplanation: null
        })
        this.filters.push({
            filterName: 'Purpose',
			filterList: dataPurposeFilterArray,
			filterType: 'checkbox',
			filterTerm: 'purpose.id',
			expanded: false,
            order: 3,
            mouseoverExplanation: null
        });
        this.filters.push({
            filterName: 'Purpose Type',
			filterList: dataPurposeTypeFilterArray,
			filterType: 'checkbox',
			filterTerm: 'purposeType.id',
			expanded: false,
            order: 4,
            mouseoverExplanation: null
        });
        const minDates = new Date(1970, 10, 1);
        const currentDate = new Date();
        this.filters.push({
			filterName: 'Published Date',
			filterTerm: 'publishedDate',
			filterType: 'datepicker',
			filterNgModel: [minDates, currentDate],
			filterID: 'publishedDate',
            minDate: minDates,
            maxDate: currentDate,
			minRange: '',
            maxRange: '',
            minRangeInit: '',
            maxRangeInit: '',
			order: 5
        });
        this.filters.push({
			filterName: 'Date Added',
			filterTerm: 'createdDate',
			filterType: 'datepicker',
			filterNgModel: [minDates, currentDate],
			filterID: 'createdDate',
            minDate: minDates,
            maxDate: currentDate,
			minRange: '',
            maxRange: '',
            minRangeInit: '',
            maxRangeInit: '',
			order: 6
        });
        this.filters.push({
			filterName: 'Last Updated Date',
			filterTerm: 'updatedDate',
			filterType: 'datepicker',
			filterNgModel: [minDates, currentDate],
			filterID: 'updatedDate',
            minDate: minDates,
            maxDate: currentDate,
			minRange: '',
            maxRange: '',
            minRangeInit: '',
            maxRangeInit: '',
			order: 7
        });
        this.filters.push({
            filterName: '',
            filterList: [{
                label: 'Recommended by Amass',
                value: 'checked',
                checked: false
            }],
            filterTerm: 'includeInNewsletter',
            filterType: 'checkbox',
            order: 8,
        });

        this.shareButtons = [{
            type: 'email',
            description: 'Hi, <br>I\'d like to share this interesting'
        }, {
            type: 'linkedin',
            description: ''
        }, {
            type: 'twitter',
            description: ''
        }, {
            type: 'copy',
            description: ''
        }];
    /* recommended by amass filter  */
        this.cookiejson = this.cookieService.get('filterValues_news') ? JSON.parse(this.cookieService.get('filterValues_news')) : null;
        if(this.cookiejson && this.cookiejson[0].queryTerm) {
            this.filters[7].filterList[0].checked = true;
        }


    }
    openSigninDialog() {
        this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
            width: '400px'
        });
    }

    openApplyDialog() {
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        });
    }
    unrestrictedUsers() {
        if (!this.isAuthenticated()) {
            this.router.navigate(['/news']);
            this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            });
        }
    }
    onFilterChanges(event) {
        if (this.isAuthenticated()) {
            if (event === 'clearFilter') {
                this.onFilter({
                    query: 'clearFilter'
                });
                return;
            }
            if (event && event.selectedFilterData && event.selectedFilterData.length) {
                const requestBody = bob.requestBodySearch();
                const allFilterQuery = bob.boolQuery();
                const filters = event.selectedFilterData;
                const filtersLength = filters.length;
                for (let i = 0; i < filtersLength; i++) {
                    const term = filters[i].queryTerm;
                    const filter = filters[i].filters;
                    if (filters[i].filters && filter.length) {
                        if (filters[i].queryType === 'should') {
                            allFilterQuery.should(bob.matchQuery(term, filter));
                        } else if (filters[i].queryType === 'range' || filters[i].queryType === 'rangeFromTo' || filters[i].queryType === 'datepicker') {
                            //console.log(filters[i].queryType, typeof filter);
                            if (typeof filter === 'string') {
                                if (filters[i].queryType === 'datepicker') {
                                    allFilterQuery.must(bob.rangeQuery(term).from(this.datePipe.transform(filter,'yyyy-MM-dd', 'EST')));
                                } else {
                                allFilterQuery.must(bob.rangeQuery(term).from(filter));
                                }
                            } else {
                                allFilterQuery.must(bob.rangeQuery(term).from(filter[0][0]).to(filter[0][1]));
                            }
                        } else if (filters[i].queryType === 'rangeBetween') {
                            allFilterQuery.must(bob.rangeQuery(term).gte(filter[0][0]).lte(filter[0][1]));
                        } else {
                            allFilterQuery.must(bob.termsQuery(term, filter));
                        }
                    }
                }

                if (this.params) {
                    if (this.params && this.params.dt) {
                        const _that = this;
                        _that.filters.filter((item, index) => {
                            if (item.filterTerm === 'articleTopics.id') {
                                setTimeout(function() {
                                    item.filterList.filter(itm => {
                                        if (itm.id === parseInt(_that.params.dt, 10)) {
                                            itm.checked = true;
                                        }
                                    });
                                }, 2000);
                            }
                        });
                        allFilterQuery.must(bob.termsQuery('articleTopics.id', this.params['dt']));
                    }
                }

                requestBody.query(allFilterQuery);

                this.onFilter({
                    query: requestBody.toJSON()
                });
            }
            return;
        } else {
            this.activatedRoute.queryParams.subscribe(params => {
                this.principal.identity().then(account => {
                    if (params && params['utm_medium'] !== 'email' && !account) {
                        this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
                            width: '400px'
                        });
                    } else if (params && params['utm_medium'] === 'email' && !account) {
                        this.stateService.storeUrl(this.router.url);
                        this.router.navigate(['accessdenied']);
                        this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
                            width: '400px'
                        });
                    } else if (params && params['utm_medium'] === 'email' && account) {
                        setTimeout(() => {
                            this.onFilterChanges({
                                selectedFilterData: this.genericFilter.selectedFilterData
                            });
                            this.loaderService.display(false);
                        }, 500);
                    }
                });
            });
        }
	}

	ngOnDestroy() {
        if (this.eventSubscriber) {
            this.eventSubscriber.unsubscribe();
        }
	}

	onFilter(options: any) {
        this.loader = true;
		if (!options.query) {
			this.currentFilter = '';
		}
        this.currentFilter = options.query ? options.query : '';
		if (options.query === 'clearFilter') {
            this.currentFilter = '';
			this.onClearFilter();
			return true;
		}
		this.dataArticles = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.reverse = false;
		this.loadAll();
	}

	onClearFilter() {
        this.loader = true;
        this.currentFilter = '';
		this.dataArticles = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.reverse = false;
        this.loadAll();
        this.router.navigate(['/news']);
		return;
	}

	trackId(index: number, item: DataArticle) {
		return item.id;
	}

	byteSize(field) {
		return this.dataUtils.byteSize(field);
	}

	openFile(contentType, field) {
		return this.dataUtils.openFile(contentType, field);
	}
	registerChangeInDataArticles() {
		this.eventSubscriber = this.eventManager.subscribe('dataArticleListModification', response => this.reset());
	}

	sort() {
		const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
		if (this.predicate !== 'id') {
			result.push('id');
		}
		return result;
	}

	sortBy(column: any) {
        if (!this.isAuthenticated()) {
            if (this.sortCategory === 'Providers') {
                this.sortLabel = 'Providers List - Sort - ' + column.label;
            } else if (this.sortCategory === 'Categories') {
                this.sortLabel = 'Data Categories - Sort - ' + column.label;
            } else {
                this.sortLabel = 'Articles List - Sort - ' + column.label;
            }
            this.googleAnalyticsService.emitEvent('Articles', 'Restricted Internal Link Clicked', this.sortLabel);
        }
        if (this.isAuthenticated()) {
            this.dataArticles = [];
            if (!column) {
                return this.clear();
            }
            const index = this.sortingArray.indexOf(column);
            this.sortedIndex = index;
            for (let i = 0; i < this.sortingArray.length; i++) {
                if (i === index) {
                    this.loader = true;
                    this.sortingArray[index].order = (column.order === 'desc') ? 'asc' : 'desc';
                } else {
                    this.sortingArray[i].order = '';
                }
            }

            this.currentSorting = column;
            this.loadAll();
       } else {
           this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '570px'
        });
       }
    }

	private onSuccess(data, headers) {
        this.loader = false;
        this.loaderInfinite = false;
		this.totalItems = data['hits'] ? data['hits'].total : 0;
		this.links.last = Math.ceil(this.totalItems / 20);
		if (!this.page) {
			this.dataArticles = [];
		}
		if (data['hits'] && data['hits'].hits) {
            this.relevantProviderLogo = false;
			for (let i = 0; i < data['hits'].hits.length; i++) {
                const articleObj = data['hits'].hits[i]._source;
                if ( this.currentAccount || !this.currentAccount) {
                    const articleNewObj = Object.assign({
                        emailText: this.currentAccount?'Hi,\n I\'d like to share this interesting ' + data['hits'].hits[i]._source.title + ' ' + data['hits'].hits[i]._source.url + ` I came across on the Amass Insights Platform.\n\n` + 'Regards,\n ' +  this.currentAccount.firstName + ' ' + this.currentAccount.lastName +'\n\n\n P.S. If you liked this article and you\'re not already an Insights user, https://amassinsights.com?signup=true, and you\'ll receive instant access to Alt-Data news, events, though-leadership, and new, interesting data providers entering our ecosystem.' : 'Hi,\n I\'d like to share this interesting ' + data['hits'].hits[i]._source.title + ' ' + data['hits'].hits[i]._source.url + ` I came across on the Amass Insights Platform.\n` + '\n P.S. If you liked this article and you\'re not already an Insights user, https://amassinsights.com?signup=true, and you\'ll receive instant access to Alt-Data news, events, though-leadership, and new, interesting data providers entering our ecosystem.',
                        linkedInText: 'Check out this interesting ' + data['hits'].hits[i]._source.title + ' article I came across on the Amass Insights Platform: ' + data['hits'].hits[i]._source.url + ' #' + data['hits'].hits[i]._source.title + ' #AmassInsights',
                        twitterText: 'Check out this interesting ' + data['hits'].hits[i]._source.title + ' article I came across on the Amass Insights Platform: ' + data['hits'].hits[i]._source.url + ' #' + data['hits'].hits[i]._source.title + ' #AmassInsights'
                    }, articleObj);
                    this.dataArticles.push(articleNewObj);
                   /*  for (i = 0; i < this.dataArticles.length; i++) {
                        const articleLists = this.dataArticles[i].relevantProviders[i];
                        if (articleLists && articleLists.logo) {
                            this.relevantProviderLogo = true;
                        }
                    } */
                } else {
                    this.dataArticles.push(articleObj);
                }
            }
            /*this.dataArticles.forEach((item, index) => {
                console.log('ITEM',item);
                console.log('ITEMPURPOSE',item.articleTopics);
                console.log('ITEMPURPOSE2',item.relevantProviders);
                console.log('VisualUrl',item.visualUrl);
                for(let i = 0; i < item.purpose.length; i++) {
                    this.articlePurpose = item.purpose[i];
                    //console.log('list....', this.articlePurpose);
                }
                for(let i = 0; i < item.articleTopics.length; i++) {
                    this.articleTopics = item.articleTopics[i];
                    console.log('Topics....', this.articleTopics);
                }
                if(item.relevantProviders && item.relevantProviders.length){
                    for(let i = 0; i < item.relevantProviders.length; i++) {
                        this.articleRelevantProviders = item.relevantProviders[i];
                        console.log('RELEVANT',this.articleRelevantProviders)
                        console.log('Imagelogo1....', this.articleRelevantProviders.logo);
                        console.log('Imagelogo2....', this.articleRelevantProviders.logo.logo);
                        this.logos = this.articleRelevantProviders.logo.logo;
                        this.providerName = this.articleRelevantProviders.providerName;
                        this.relevantProviderRecordId = this.articleRelevantProviders.recordID
                    }
                }
                console.log(this.relevantProviderRecordId);
                let sharebuttonsArray = []
                sharebuttonsArray = [{
                    type: 'email',
                    description: item.emailText,
                    url: item.url
                }, {
                    type: 'linkedin',
                    description: item.linkedInText,
                    url: item.url
                }, {
                    type: 'twitter',
                    description: item.twitterText,
                    url: item.url
                }, {
                    type: 'copy',
                    url: item.url
                }];
                this.genericProfileCard.push({
                    content: [
                        {
                            columns: 2,
                            columnSize: [ item.visualUrl ? 2 : 0, item.visualUrl ? 10 : 12],
                            columnContent:[{
                                image: item.visualUrl ? this._DomSanitizer.bypassSecurityTrustUrl(item.visualUrl) : ''
                            },{
                                title: item.title,
                                url: '/#/news/'+ item.recordID,
                                style: (item.authorDataProvider && item.authorDataProvider.locked) ? 'blurText' : ''
                            }]
                        },
                        {
                            columns: 2,
                            columnSize: [5,7],
                            columnContent: [{
                                label: (item.authorDataProvider && item.authorDataProvider.providerName && item.authorDataProvider.providerName !== 'RECORD_NULL') || (item.authorOrganization && item.authorOrganization.name && item.authorOrganization.name !== 'NAME_NULL') || item.stream || (item.author && item.author != 'PROVIDER_NULL') ? 'by' : '',
                                plainText: item.authorOrganization && item.authorOrganization.name && item.authorOrganization.name !== 'NAME_NULL'
                                      ? item.authorOrganization.name : item.authorDataProvider && item.authorDataProvider.providerName && item.authorDataProvider.providerName !== 'RECORD_NULL' && item.authorDataProvider.providerName !== 'PROVIDER_NULL' ? item.authorDataProvider.providerName : item.stream ? item.stream : item.author ? item.author : '',
                                      plainTextUrl : item.authorDataProvider && item.authorDataProvider.recordID && item.authorDataProvider.recordID !== 'PROVIDER_NULL' ? '/#/providers/'+ item.authorDataProvider.recordID : ''
                            }, {
                                label: 'Published:',
                                plainText: this.datePipe.transform(item.publishedDate, 'mediumDate', 'EST')
                            }]
                        }, {
                            columns: 2,
                            columnSize: [5,7],
                            columnContent: [{
                                label: item.purpose && item.purpose.length ? 'Purposes:' : '',
                                plainText: (item.purpose && item.purpose.length) ?  this.articlePurpose.purpose : '',
                                plainTextUrl: this.articlePurpose ? '/#/news?filter1=purpose.id&value1='+this.articlePurpose.id+'&type1=checkbox' : ''
                            },{
                                label: item.articleTopics && item.articleTopics.length ? 'Topics:' : '',
                                plainText: item.articleTopics && item.articleTopics.length ? this.articleTopics.topicName : '',
                                plainTextUrl: this.articleTopics ? '/#/news?filter1=articleTopics.id&value1='+this.articleTopics.id+'&type1=checkbox' : ''
                            }]
                        }, {
                            columns: 2,
                            columnSize: [9,3],
                            columnContent: [{
                                plainText: item.summary
                            },{
                                shareButtons: sharebuttonsArray
                            }]
                        }, {
                            columns: 2,
                            columnSize: [12],
                            columnContent: [{
                                title: (item.relevantProviders && item.relevantProviders.length) ? 'Related Providers' : '',
                            }]
                        }, {
                            columns: 2,
                            columnSize: [1,11],
                            columnContent: [{
                                image: (item.relevantProviders && item.relevantProviders.length) && this.logos && !this.articleRelevantProviders.locked ? this._DomSanitizer.bypassSecurityTrustUrl('data:image/png;base64,' + this.logos) : '',
                                imageStyle: 'relatedProviders-image'
                            }, {
                                plainText: (item.relevantProviders && item.relevantProviders.length) && this.providerName ? this.providerName : '',
                                plainTextUrl: (item.relevantProviders && item.relevantProviders.length) && this.relevantProviderRecordId ? '/#/providers/'+ this.relevantProviderRecordId : ''
                            }]
                        }
                    ]
                })
            });*/
		}
	}

	private onError(error) {
        this.loader = false;
        this.loaderInfinite = false;
		this.alertService.error(error.message, null, null);
	}

    onExpireSession(event) {
		//console.log('eventcheck',event);
        if (event) {
			//console.log('eventcheck',event);
            //this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
				setTimeout(() => {
				this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
			}, 2000);
			}); 
        }
    }
	  beforeSessionExpires(event) {
        if (event) {
           // this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }
}
