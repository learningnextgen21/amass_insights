import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { DataArticle } from './data-article.model';
import { createRequestOption, Principal } from '../../shared';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataArticleService {

	private resourceUrl = 'api/articles';
	private resourceSearchUrl = 'api/_search/articles';
    private resourceElasticSearchUrl = 'api/es/qsearch?type=dataarticle';
    private blogUnrestrictedUrl = 'api/ur/qsearch?type=blog';
    private resourceElasticSearchUnrestrictedUrl = 'api/ur/qsearch?type=dataarticle&lookupcode=&sort=';
    private resourcePressUnrestrictedUrl = 'api/ur/pressNews';
	constructor(
        private http: HttpClient,
        private dateUtils: JhiDateUtils,
        public principal: Principal
        ) { }
    isAuthenticated() {
        return this.principal.isAuthenticated();
    }
	create(dataArticle: DataArticle): Observable<DataArticle> {
		const copy = this.convert(dataArticle);
		return this.http.post(this.resourceUrl, copy);
    }

	update(dataArticle: DataArticle): Observable<DataArticle> {
		const copy = this.convert(dataArticle);
		return this.http.put(this.resourceUrl, copy);
	}

	find(id: number): Observable<DataArticle> {
		return this.http.get(`${this.resourceUrl}/${id}`);
	}
    findById(recordId: string): Observable<DataArticle> {
        const req = {
            "filter": {
                "query": {
                    "match": {
                        "recordID": recordId
                    }
                }
            }
        };
        return this.http.post(this.resourceElasticSearchUrl, req);
    }
    amassFilter(): Observable<DataArticle> {
        const req = {
            "sort": [
               {
                  "publishedDate":"desc"
               }
            ],
            "size": 20,
            "filter": {
               "query": {
                  "bool": {
                     "must": {
                        "terms": {
                           "relevantProviders.id": [211]
                        }
                     }
                  }
               }
            }
         };
        return this.http.post(this.resourceElasticSearchUrl, req);
    }
    pressArticles(): Observable<DataArticle> {
        return this.http.post(this.resourcePressUnrestrictedUrl + '?press=true', '');
    }
	query(req?: any): Observable<any> {
		const options = createRequestOption(req);
		return this.http.get(this.resourceUrl, {params: options});
	}

	delete(id: number): Observable<any> {
		return this.http.delete(`${this.resourceUrl}/${id}`);
	}

	search(req?: any): Observable<any> {
		const options = createRequestOption(req);
		return this.http.get(this.resourceSearchUrl, {params: options});
	}

	elasticSearch(req: any): Observable<any> {
		return this.http.post(this.resourceElasticSearchUnrestrictedUrl, req);
    }

    blogUnrestricted(req: any): Observable<any> {
		return this.http.post(this.blogUnrestrictedUrl, req);
    }

    elasticSearchAuthenticated(req: any): Observable<any> {
		return this.http.post(this.resourceElasticSearchUrl, req);
	}

	private convert(dataArticle: DataArticle): DataArticle {
		const copy: DataArticle = Object.assign({}, dataArticle);
		copy.publishedDate = this.dateUtils
			.convertLocalDateToServer(dataArticle.publishedDate);
		copy.createdDate = this.dateUtils
			.convertLocalDateToServer(dataArticle.createdDate);

		copy.updatedDate = this.dateUtils.toDate(dataArticle.updatedDate);
		return copy;
	}
}
