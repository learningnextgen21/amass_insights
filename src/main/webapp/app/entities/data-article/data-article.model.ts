import { BaseEntity } from './../../shared';

export class DataArticle implements BaseEntity {
    constructor(
        public id?: number,
        public articleuniquekey?: string,
        public articleTopics?: string,
        public recordID?: string,
        public title?: string,
        public url?: string,
        public publishedDate?: any,
        public content?: any,
        public locked?: boolean,
        public contentHTML?: any,
        public summary?: any,
        public author?: string,
        public publisher?: string,
        public summaryHTML?: any,
        public relevancy?: number,
        public purpose?: any,
        public visualUrl?: any,
        public purposeType?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public authorOrganization?: BaseEntity,
        public authorDataProvider?: BaseEntity,
        public relevantProviders?: BaseEntity[],
        public emailText?: string,
        public linkedInText?: string,
        public twitterText?: string,
        public stream?: any
    ) {
    }
}