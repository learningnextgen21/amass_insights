import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DataArticle } from './data-article.model';
import { DataArticleService } from './data-article.service';

@Injectable()
export class DataArticlePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private dataArticleService: DataArticleService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dataArticleService.find(id).subscribe((dataArticle) => {
                    if (dataArticle.publishedDate) {
                        dataArticle.publishedDate = {
                            year: dataArticle.publishedDate.getFullYear(),
                            month: dataArticle.publishedDate.getMonth() + 1,
                            day: dataArticle.publishedDate.getDate()
                        };
                    }
                    if (dataArticle.createdDate) {
                        dataArticle.createdDate = {
                            year: dataArticle.createdDate.getFullYear(),
                            month: dataArticle.createdDate.getMonth() + 1,
                            day: dataArticle.createdDate.getDate()
                        };
                    }
                    dataArticle.updatedDate = this.datePipe
                        .transform(dataArticle.updatedDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.dataArticleModalRef(component, dataArticle);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dataArticleModalRef(component, new DataArticle());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dataArticleModalRef(component: Component, dataArticle: DataArticle): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dataArticle = dataArticle;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
