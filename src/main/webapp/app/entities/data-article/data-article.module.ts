import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    DataArticleService,
    DataArticlePopupService,
    DataArticleComponent,
    DataArticleDetailComponent,
    DataArticleDialogComponent,
    DataArticlePopupComponent,
    DataArticleDeletePopupComponent,
    DataArticleDeleteDialogComponent,
    dataArticleRoute,
    dataArticlePopupRoute,
    DataNewArticleComponent
} from './';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { GenericProfileCardModule } from 'app/shared/generic-profile-card/generic-profile-card.module';

const ENTITY_STATES = [
    ...dataArticleRoute,
    ...dataArticlePopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        GenericProfileCardModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true }),
        ShareButtonModule,
        ShareButtonsModule.withConfig({
            debug: true
        })
    ],
    declarations: [
        DataArticleComponent,
        DataArticleDetailComponent,
        DataArticleDialogComponent,
        DataArticleDeleteDialogComponent,
        DataArticlePopupComponent,
        DataArticleDeletePopupComponent,
        DataNewArticleComponent
    ],
    entryComponents: [
        DataArticleComponent,
        DataArticleDialogComponent,
        DataArticlePopupComponent,
        DataArticleDeleteDialogComponent,
        DataArticleDeletePopupComponent,
    ],
    providers: [
        DataArticleService,
        DataArticlePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceDataArticleModule {}