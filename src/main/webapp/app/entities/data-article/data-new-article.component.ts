import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { DomSanitizer } from '@angular/platform-browser';

import { DataArticle } from './data-article.model';
import { DataArticleService } from './data-article.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, GenericFilterListModel, GenericFilterModel, ShareButtonsModel, StateStorageService, GenericFilterComponent } from '../../shared';
import { LoaderService } from '../../loader/loaders.service';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
const bob = require('elastic-builder');
import { ApplyAccountComponent } from '../../account/apply/apply.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SigninDialogComponent } from '../../dialog/signindialog.component';

@Component({
	selector: 'jhi-data-new-article',
	templateUrl: './data-new-article.component.html',
	styleUrls: ['./data-article.component.css', './data-new-article.component.css', '../../shared/share-buttons/share-buttons.scss']
})
export class DataNewArticleComponent implements OnInit, OnDestroy {

	dataArticles: DataArticle[];
	currentAccount: any;
	eventSubscriber: Subscription;
	itemsPerPage: number;
	links: any;
	page: any;
	predicate: any;
	subscription: any;
	reverse: any;
	totalItems: number;
    params: any;
	paramFilter: string;
    paramID: string;
    loader: any;
    loaderInfinite: any;
    filters: GenericFilterModel[];
    isAdmin: boolean;
    recordID: string;

    isSortByBarCollapsed: boolean;
    currentUrl: any;
    shareButtons: ShareButtonsModel[];
    emailText: string;
    logoutUsers: boolean;
    logoutUserClick: boolean;
    mdDialogRef: MatDialogRef<any>;
    sortLabel: any;
    relevantProviderLogo: any;
    dataArticle: any;
    @ViewChild(GenericFilterComponent) genericFilter;

	constructor(
		private dataArticleService: DataArticleService,
		private alertService: JhiAlertService,
		private dataUtils: JhiDataUtils,
		private eventManager: JhiEventManager,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private principal: Principal,
        public _DomSanitizer: DomSanitizer,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private loaderService: LoaderService,
        public dialog: MatDialog,
	) {
		this.dataArticles = [];
		this.itemsPerPage = ITEMS_PER_PAGE;
		this.page = 0;
		this.links = {
			last: 0
		};
		this.predicate = 'id';
		this.reverse = true;
        this.isSortByBarCollapsed = true;
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
	}

	load(recordID) {
        this.dataArticleService.findById(recordID).subscribe(dataArticle => {
            this.dataArticle = dataArticle['hits'].total ? dataArticle['hits'].hits[0]._source : null;
            this.loaderService.display(false);
        });
        const queryBody = {};
		if ( this.subscription ) {
            this.subscription.unsubscribe();
        }
	}

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }
    articlePage(events: any) {
      this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    topicLink(topic) {
        this.googleAnalyticsService.emitEvent('Article', 'Internal Link Clicked', 'Article - Purposes ' + '- ' + topic);
    }
    purposeLink(purpose) {
        this.googleAnalyticsService.emitEvent('Article', 'Internal Link Clicked', 'Article - Topics ' + '- ' + purpose);
    }
    relatedProviderLink(provider) {
        this.googleAnalyticsService.emitEvent('Article', 'Internal Link Clicked', 'Article - Related Provider ' + '- ' + provider);
    }
    relatedProviderLogo(logo) {
        this.googleAnalyticsService.emitEvent('Article', 'Internal Link Clicked', 'Article - Related Provider - Logo ' + '- ' + logo);
    }

	ngOnInit() {
       // this.filterService.setAuthorisedUrl();
        this.dialog.closeAll();
        this.currentUrl = this.router.url;
        this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
        /* this.loaderService.display(true); */
        this.loader = false;
		this.activatedRoute.params.subscribe(params => {
            this.params = params;
            if (params['recordID'] && this.isAuthenticated()) {
                this.load(params['recordID']);
            } else {
                this.router.navigate(['accessdenied']);
                this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
                    width: '400px'
                });
            }
        });

       /*  this.shareButtons = [{
            type: 'email',
            description: 'Hi, <br>I\'d like to share this interesting'
        }, {
            type: 'linkedin',
            description: ''
        }, {
            type: 'twitter',
            description: ''
        }, {
            type: 'copy',
            description: ''
        }]; */
    }
    unrestrictedUsers() {
        if (!this.isAuthenticated()) {
            this.router.navigate(['/news']);
            this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            });
        }
    }

	ngOnDestroy() {
        if (this.eventSubscriber) {
            this.eventSubscriber.unsubscribe();
        }
	}
}
