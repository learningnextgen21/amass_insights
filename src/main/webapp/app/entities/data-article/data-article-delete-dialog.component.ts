import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DataArticle } from './data-article.model';
import { DataArticlePopupService } from './data-article-popup.service';
import { DataArticleService } from './data-article.service';

@Component({
    selector: 'jhi-data-article-delete-dialog',
    templateUrl: './data-article-delete-dialog.component.html'
})
export class DataArticleDeleteDialogComponent {

    dataArticle: DataArticle;

    constructor(
        private dataArticleService: DataArticleService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dataArticleService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dataArticleListModification',
                content: 'Deleted an dataArticle'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-data-article-delete-popup',
    template: ''
})
export class DataArticleDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataArticlePopupService: DataArticlePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dataArticlePopupService
                .open(DataArticleDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
