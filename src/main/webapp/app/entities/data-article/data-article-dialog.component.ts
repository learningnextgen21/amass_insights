import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { DataArticle } from './data-article.model';
import { DataArticlePopupService } from './data-article-popup.service';
import { DataArticleService } from './data-article.service';
import { DataOrganization, DataOrganizationService } from '../data-organization';
import { DataProvider, DataProviderService } from '../data-provider';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-data-article-dialog',
    templateUrl: './data-article-dialog.component.html'
})
export class DataArticleDialogComponent implements OnInit {

    dataArticle: DataArticle;
    isSaving: boolean;

    dataorganizations: DataOrganization[];

    dataproviders: DataProvider[];
    publishedDateDp: any;
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private dataArticleService: DataArticleService,
        private dataOrganizationService: DataOrganizationService,
        private dataProviderService: DataProviderService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.dataOrganizationService.query()
            .subscribe((res) => { this.dataorganizations = res; }, (res) => this.onError(res));
        this.dataProviderService.query()
            .subscribe((res) => { this.dataproviders = res.body; }, (res) => this.onError(res));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dataArticle.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dataArticleService.update(this.dataArticle));
        } else {
            this.subscribeToSaveResponse(
                this.dataArticleService.create(this.dataArticle));
        }
    }

    private subscribeToSaveResponse(result: Observable<DataArticle>) {
        result.subscribe((res: DataArticle) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DataArticle) {
        this.eventManager.broadcast({ name: 'dataArticleListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackDataOrganizationById(index: number, item: DataOrganization) {
        return item.id;
    }

    trackDataProviderById(index: number, item: DataProvider) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-data-article-popup',
    template: ''
})
export class DataArticlePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataArticlePopupService: DataArticlePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dataArticlePopupService
                    .open(DataArticleDialogComponent as Component, params['id']);
            } else {
                this.dataArticlePopupService
                    .open(DataArticleDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
