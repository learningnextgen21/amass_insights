import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { DataArticle } from './data-article.model';
import { DataArticleService } from './data-article.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-data-article-detail',
    templateUrl: './data-article-detail.component.html'
})
export class DataArticleDetailComponent implements OnInit, OnDestroy {

    dataArticle: DataArticle;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private dataArticleService: DataArticleService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDataArticles();
    }

    load(id) {
        this.dataArticleService.find(id).subscribe((dataArticle) => {
            this.dataArticle = dataArticle;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDataArticles() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dataArticleListModification',
            (response) => this.load(this.dataArticle.id)
        );
    }
}
