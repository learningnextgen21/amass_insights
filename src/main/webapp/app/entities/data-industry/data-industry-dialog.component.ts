import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { DataIndustry } from './data-industry.model';
import { DataIndustryPopupService } from './data-industry-popup.service';
import { DataIndustryService } from './data-industry.service';
import { DataProvider, DataProviderService } from '../data-provider';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-data-industry-dialog',
    templateUrl: './data-industry-dialog.component.html'
})
export class DataIndustryDialogComponent implements OnInit {

    dataIndustry: DataIndustry;
    isSaving: boolean;

    dataproviders: DataProvider[];
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private dataIndustryService: DataIndustryService,
        private dataProviderService: DataProviderService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.dataProviderService.query()
            .subscribe((res) => { this.dataproviders = res.body; }, (res) => this.onError(res));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dataIndustry.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dataIndustryService.update(this.dataIndustry));
        } else {
            this.subscribeToSaveResponse(
                this.dataIndustryService.create(this.dataIndustry));
        }
    }

    private subscribeToSaveResponse(result: Observable<DataIndustry>) {
        result.subscribe((res: DataIndustry) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DataIndustry) {
        this.eventManager.broadcast({ name: 'dataIndustryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackDataProviderById(index: number, item: DataProvider) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-data-industry-popup',
    template: ''
})
export class DataIndustryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataIndustryPopupService: DataIndustryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dataIndustryPopupService
                    .open(DataIndustryDialogComponent as Component, params['id']);
            } else {
                this.dataIndustryPopupService
                    .open(DataIndustryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
