import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    DataIndustryService,
    DataIndustryPopupService,
    DataIndustryComponent,
    DataIndustryDetailComponent,
    DataIndustryDialogComponent,
    DataIndustryPopupComponent,
    DataIndustryDeletePopupComponent,
    DataIndustryDeleteDialogComponent,
    dataIndustryRoute,
    dataIndustryPopupRoute,
} from './';

const ENTITY_STATES = [
    ...dataIndustryRoute,
    ...dataIndustryPopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DataIndustryComponent,
        DataIndustryDetailComponent,
        DataIndustryDialogComponent,
        DataIndustryDeleteDialogComponent,
        DataIndustryPopupComponent,
        DataIndustryDeletePopupComponent,
    ],
    entryComponents: [
        DataIndustryComponent,
        DataIndustryDialogComponent,
        DataIndustryPopupComponent,
        DataIndustryDeleteDialogComponent,
        DataIndustryDeletePopupComponent,
    ],
    providers: [
        DataIndustryService,
        DataIndustryPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceDataIndustryModule {}
