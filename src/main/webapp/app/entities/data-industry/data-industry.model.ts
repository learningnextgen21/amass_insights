import { BaseEntity } from './../../shared';

export class DataIndustry implements BaseEntity {
    constructor(
        public id?: number,
        public recordID?: string,
        public dataIndustry?: string,
        public explanation?: any,
        public inactive?: number,
        public createdDate?: any,
        public updatedDate?: any,
        public hashtag?: string,
        public dataProviders?: BaseEntity[],
        public associatedProviders?: BaseEntity[],
        public locked?: boolean
    ) {
    }
}
