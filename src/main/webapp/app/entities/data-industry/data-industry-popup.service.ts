import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DataIndustry } from './data-industry.model';
import { DataIndustryService } from './data-industry.service';

@Injectable()
export class DataIndustryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private dataIndustryService: DataIndustryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dataIndustryService.find(id).subscribe((dataIndustry) => {
                    if (dataIndustry.createdDate) {
                        dataIndustry.createdDate = {
                            year: dataIndustry.createdDate.getFullYear(),
                            month: dataIndustry.createdDate.getMonth() + 1,
                            day: dataIndustry.createdDate.getDate()
                        };
                    }
                    dataIndustry.updatedDate = this.datePipe
                        .transform(dataIndustry.updatedDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.dataIndustryModalRef(component, dataIndustry);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dataIndustryModalRef(component, new DataIndustry());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dataIndustryModalRef(component: Component, dataIndustry: DataIndustry): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dataIndustry = dataIndustry;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
