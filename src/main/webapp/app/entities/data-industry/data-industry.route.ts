import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DataIndustryComponent } from './data-industry.component';
import { DataIndustryDetailComponent } from './data-industry-detail.component';
import { DataIndustryPopupComponent } from './data-industry-dialog.component';
import { DataIndustryDeletePopupComponent } from './data-industry-delete-dialog.component';

export const dataIndustryRoute: Routes = [
    {
        path: 'data-industry',
        component: DataIndustryComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataIndustry.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'data-industry/:id',
        component: DataIndustryDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataIndustry.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dataIndustryPopupRoute: Routes = [
    {
        path: 'data-industry-new',
        component: DataIndustryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataIndustry.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-industry/:id/edit',
        component: DataIndustryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataIndustry.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-industry/:id/delete',
        component: DataIndustryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataIndustry.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
