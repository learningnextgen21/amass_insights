import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DataIndustry } from './data-industry.model';
import { DataIndustryPopupService } from './data-industry-popup.service';
import { DataIndustryService } from './data-industry.service';

@Component({
    selector: 'jhi-data-industry-delete-dialog',
    templateUrl: './data-industry-delete-dialog.component.html'
})
export class DataIndustryDeleteDialogComponent {

    dataIndustry: DataIndustry;

    constructor(
        private dataIndustryService: DataIndustryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dataIndustryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dataIndustryListModification',
                content: 'Deleted an dataIndustry'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-data-industry-delete-popup',
    template: ''
})
export class DataIndustryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataIndustryPopupService: DataIndustryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dataIndustryPopupService
                .open(DataIndustryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
