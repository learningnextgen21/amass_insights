import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { DataIndustry } from './data-industry.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataIndustryService {

    private resourceUrl = 'api/data-industries';
    private resourceSearchUrl = 'api/_search/data-industries';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(dataIndustry: DataIndustry): Observable<DataIndustry> {
        const copy = this.convert(dataIndustry);
        return this.http.post(this.resourceUrl, copy);
    }

    update(dataIndustry: DataIndustry): Observable<DataIndustry> {
        const copy = this.convert(dataIndustry);
        return this.http.put(this.resourceUrl, copy);
    }

    find(id: number): Observable<DataIndustry> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, {params: options});
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, {params: options});
    }

    private convert(dataIndustry: DataIndustry): DataIndustry {
        const copy: DataIndustry = Object.assign({}, dataIndustry);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(dataIndustry.createdDate);

        copy.updatedDate = this.dateUtils.toDate(dataIndustry.updatedDate);
        return copy;
    }
}
