import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { DataIndustry } from './data-industry.model';
import { DataIndustryService } from './data-industry.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-data-industry-detail',
    templateUrl: './data-industry-detail.component.html'
})
export class DataIndustryDetailComponent implements OnInit, OnDestroy {

    dataIndustry: DataIndustry;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private dataIndustryService: DataIndustryService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDataIndustries();
    }

    load(id) {
        this.dataIndustryService.find(id).subscribe((dataIndustry) => {
            this.dataIndustry = dataIndustry;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDataIndustries() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dataIndustryListModification',
            (response) => this.load(this.dataIndustry.id)
        );
    }
}
