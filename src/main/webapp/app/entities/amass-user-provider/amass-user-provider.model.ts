import { BaseEntity } from './../../shared';

export class AmassUserProvider implements BaseEntity {
    constructor(
        public id?: number,
        public userID?: number,
        public providerID?: number,
        public getNotifications?: number,
        public notificationType?: number,
        public interestType?: number,
        public inactive?: number,
        public createdDate?: any,
        public updatedDate?: any,
    ) {
    }
}
