import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AmassUserProvider } from './amass-user-provider.model';
import { AmassUserProviderPopupService } from './amass-user-provider-popup.service';
import { AmassUserProviderService } from './amass-user-provider.service';

@Component({
    selector: 'jhi-amass-user-provider-dialog',
    templateUrl: './amass-user-provider-dialog.component.html'
})
export class AmassUserProviderDialogComponent implements OnInit {

    amassUserProvider: AmassUserProvider;
    isSaving: boolean;
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private amassUserProviderService: AmassUserProviderService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.amassUserProvider.id !== undefined) {
            this.subscribeToSaveResponse(
                this.amassUserProviderService.update(this.amassUserProvider));
        } else {
            this.subscribeToSaveResponse(
                this.amassUserProviderService.create(this.amassUserProvider));
        }
    }

    private subscribeToSaveResponse(result: Observable<AmassUserProvider>) {
        result.subscribe((res: AmassUserProvider) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: AmassUserProvider) {
        this.eventManager.broadcast({ name: 'amassUserProviderListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-amass-user-provider-popup',
    template: ''
})
export class AmassUserProviderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private amassUserProviderPopupService: AmassUserProviderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.amassUserProviderPopupService
                    .open(AmassUserProviderDialogComponent as Component, params['id']);
            } else {
                this.amassUserProviderPopupService
                    .open(AmassUserProviderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
