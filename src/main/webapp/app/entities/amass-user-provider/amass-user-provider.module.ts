import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    AmassUserProviderService,
    AmassUserProviderPopupService,
    AmassUserProviderComponent,
    AmassUserProviderDetailComponent,
    AmassUserProviderDialogComponent,
    AmassUserProviderPopupComponent,
    AmassUserProviderDeletePopupComponent,
    AmassUserProviderDeleteDialogComponent,
    amassUserProviderRoute,
    amassUserProviderPopupRoute,
} from './';

const ENTITY_STATES = [
    ...amassUserProviderRoute,
    ...amassUserProviderPopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AmassUserProviderComponent,
        AmassUserProviderDetailComponent,
        AmassUserProviderDialogComponent,
        AmassUserProviderDeleteDialogComponent,
        AmassUserProviderPopupComponent,
        AmassUserProviderDeletePopupComponent,
    ],
    entryComponents: [
        AmassUserProviderComponent,
        AmassUserProviderDialogComponent,
        AmassUserProviderPopupComponent,
        AmassUserProviderDeleteDialogComponent,
        AmassUserProviderDeletePopupComponent,
    ],
    providers: [
        AmassUserProviderService,
        AmassUserProviderPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceAmassUserProviderModule {}
