
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';
import { AmassUserProvider } from './amass-user-provider.model';
import { createRequestOption } from '../../shared';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class AmassUserProviderService {

    private resourceUrl = 'api/amass-user-providers';
    private resourceSearchUrl = 'api/_search/amass-user-providers';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(amassUserProvider: AmassUserProvider): Observable<any> {
        const copy = this.convert(amassUserProvider);
        return this.http.post(this.resourceUrl, copy, { observe: 'response' });
    }

    update(amassUserProvider: AmassUserProvider): Observable<any> {
        const copy = this.convert(amassUserProvider);
        return this.http.put(this.resourceUrl, copy, { observe: 'response' });
    }

    find(id: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    private convert(amassUserProvider: AmassUserProvider): AmassUserProvider {
        const copy: AmassUserProvider = Object.assign({}, amassUserProvider);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(amassUserProvider.createdDate);

        copy.updatedDate = this.dateUtils.toDate(amassUserProvider.updatedDate);
        return copy;
    }
}
