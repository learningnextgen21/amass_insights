import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AmassUserProvider } from './amass-user-provider.model';
import { AmassUserProviderPopupService } from './amass-user-provider-popup.service';
import { AmassUserProviderService } from './amass-user-provider.service';

@Component({
    selector: 'jhi-amass-user-provider-delete-dialog',
    templateUrl: './amass-user-provider-delete-dialog.component.html'
})
export class AmassUserProviderDeleteDialogComponent {

    amassUserProvider: AmassUserProvider;

    constructor(
        private amassUserProviderService: AmassUserProviderService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.amassUserProviderService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'amassUserProviderListModification',
                content: 'Deleted an amassUserProvider'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-amass-user-provider-delete-popup',
    template: ''
})
export class AmassUserProviderDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private amassUserProviderPopupService: AmassUserProviderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.amassUserProviderPopupService
                .open(AmassUserProviderDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
