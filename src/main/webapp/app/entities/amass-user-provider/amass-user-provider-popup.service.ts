import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { AmassUserProvider } from './amass-user-provider.model';
import { AmassUserProviderService } from './amass-user-provider.service';

@Injectable()
export class AmassUserProviderPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private amassUserProviderService: AmassUserProviderService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.amassUserProviderService.find(id).subscribe((amassUserProvider) => {
                    if (amassUserProvider.createdDate) {
                        amassUserProvider.createdDate = {
                            year: amassUserProvider.createdDate.getFullYear(),
                            month: amassUserProvider.createdDate.getMonth() + 1,
                            day: amassUserProvider.createdDate.getDate()
                        };
                    }
                    amassUserProvider.updatedDate = this.datePipe
                        .transform(amassUserProvider.updatedDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.amassUserProviderModalRef(component, amassUserProvider);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.amassUserProviderModalRef(component, new AmassUserProvider());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    amassUserProviderModalRef(component: Component, amassUserProvider: AmassUserProvider): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.amassUserProvider = amassUserProvider;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
