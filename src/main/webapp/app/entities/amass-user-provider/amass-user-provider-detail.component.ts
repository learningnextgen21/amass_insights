import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AmassUserProvider } from './amass-user-provider.model';
import { AmassUserProviderService } from './amass-user-provider.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-amass-user-provider-detail',
    templateUrl: './amass-user-provider-detail.component.html'
})
export class AmassUserProviderDetailComponent implements OnInit, OnDestroy {

    amassUserProvider: AmassUserProvider;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private amassUserProviderService: AmassUserProviderService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAmassUserProviders();
    }

    load(id) {
        this.amassUserProviderService.find(id).subscribe((amassUserProvider) => {
            this.amassUserProvider = amassUserProvider;
        });
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAmassUserProviders() {
        this.eventSubscriber = this.eventManager.subscribe(
            'amassUserProviderListModification',
            (response) => this.load(this.amassUserProvider.id)
        );
    }
}
