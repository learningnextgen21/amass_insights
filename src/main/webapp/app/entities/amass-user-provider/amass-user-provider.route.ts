import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AmassUserProviderComponent } from './amass-user-provider.component';
import { AmassUserProviderDetailComponent } from './amass-user-provider-detail.component';
import { AmassUserProviderPopupComponent } from './amass-user-provider-dialog.component';
import { AmassUserProviderDeletePopupComponent } from './amass-user-provider-delete-dialog.component';

export const amassUserProviderRoute: Routes = [
    {
        path: 'amass-user-provider',
        component: AmassUserProviderComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.amassUserProvider.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'amass-user-provider/:id',
        component: AmassUserProviderDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.amassUserProvider.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const amassUserProviderPopupRoute: Routes = [
    {
        path: 'amass-user-provider-new',
        component: AmassUserProviderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.amassUserProvider.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'amass-user-provider/:id/edit',
        component: AmassUserProviderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.amassUserProvider.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'amass-user-provider/:id/delete',
        component: AmassUserProviderDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.amassUserProvider.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
