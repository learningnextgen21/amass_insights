
import { Injectable } from '@angular/core';

import { Observable, Subject } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, createRequestOption,Principal } from '../../shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
@Injectable()
export class InvestorService {

    private investorElasticSearchUrl = 'api/es/investorsearch';
	constructor(private http: HttpClient, private dateUtils: JhiDateUtils,private principal: Principal) { }

queryWithSearchFilterInvestor(req?: any, statusFilter?: any): Observable<any> {
    const newReq = Object.assign({
        '_source': {
            'include': [
                'id',
                'createdDate',
                'lastUpdatedDate',
                'name',
                'primaryType',
                'primarySubtype',
                'mainResearchStyle',
                'headQuartersLocation',
                'totalAumRange'
            ]
        }
    }, req);
    const options = createRequestOption(newReq);
    let elasticSearchUrl = '';
    if(this.principal.isAuthenticated())
    {
        switch (statusFilter) {
            case 'Followed':
                elasticSearchUrl = this.investorElasticSearchUrl + '?followedProviders=true';
                break;
            case 'Unlocked':
                elasticSearchUrl = this.investorElasticSearchUrl + '?unlockedProviders=true';
                break;
            default:
                elasticSearchUrl = this.investorElasticSearchUrl;
                break;
        }
    }
    return this.http.post(elasticSearchUrl, newReq);
}
}