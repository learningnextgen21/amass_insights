import { Component, OnInit, OnDestroy, Inject, HostListener, ViewChild, AfterViewInit } from '@angular/core';
import { AmassFilterService } from '../../shared/genericfilter/filters.service';
import {
    ITEMS_PER_PAGE,
    Principal,
    CSRFService,
    GenericFilterListModel,
    GenericFilterModel,
    GenericFilterComponent,
    StateStorageService,
    AuthServerProvider,
    SigninModalService
} from '../../shared';
import { LoaderService } from '../../loader/loaders.service';
import { ActivatedRoute, Router } from '@angular/router';
import { InvestorService } from './investor.service';
import { DataProviderSearchService } from '../data-provider/data-provider-search.service';
import { DataProviderFilterService } from '../data-provider/data-provider-filter.service';

const bob = require('elastic-builder');

@Component({
    selector: 'jhi-investor',
    templateUrl: './investor.component.html',
    styleUrls: ['./investor.component.scss', '../../shared/share-buttons/share-buttons.scss', '../../../content/scss/amass-form.scss']
})
export class InvestorComponent implements OnInit, OnDestroy {
    expandStatus: boolean;
    sortingArray = [];
    sortCategory: string;
    totalItemsTooltip: string;
    sortByTooltip: string;
    filterCheckBox: any;
    filters: GenericFilterModel[];
    isProvider: boolean;
    csrf: string;
    loaderInfinite: any;
    page: any;
    links: any;
    investors: any[];
    itemsFrom: number;
    sortedIndex: any;
    currentSorting: any;
    limitedProviders: any;
    loader: any;
    subscription: any;
    statusFilter: any;
    totalItems: number;
    isList: boolean;
    currentFilter: string;
    limitedLogoutUsersEvent: boolean;
    public selectFilterData: any;
    public selectFilters: any;
    currentSearchQuery: string;
    clearFilter: boolean;
    currentSearch: any;
    searchCommonValue: boolean;

    constructor(
        private filtersService: AmassFilterService,
        private loaderService: LoaderService,
        private principal: Principal,
        private csrfService: CSRFService,
        private authService: AuthServerProvider,
        private stateService: StateStorageService,
        private router: Router,
        private signinModalService: SigninModalService,
        private investorService: InvestorService,
        private dataProviderSearchService: DataProviderSearchService,
        private dataProviderfilterService: DataProviderFilterService
    ) {
        this.links = {
            last: 0
        };
        this.investors = [];
        this.currentSorting = '';
        this.isList = true;
        this.currentFilter = '';
        this.selectFilters = [];
        this.selectFilterData = [];
    }

    onResize(event) {
        // onresize methods used for window dynamic resize depend upon the screen
        const view = event.target.innerWidth;
        if (view > 700) {
            this.expandStatus = true;
            // this.ngOnInit();
        } else if (view < 700) {
            this.expandStatus = false;
            // this.ngOnInit();
        }
    }
    ngOnInit() {
        this.csrf = this.csrfService.getCSRF();
        this.filtersService.setAuthorisedUrl();
        this.isList = true;
        this.dataProviderSearchService.investorSearch$.subscribe(event => {
            console.log('event', event);
            if (event === 'clear') {
                this.clear();
            }
            if (event !== 'clear') {
                if (
                    event &&
                    event.option &&
                    event.option.value ===
                        '<strong class="warn-text">At least one provider matches this query but is locked for you. Please contact Amass for access.</strong>'
                ) {
                    this.currentSearchQuery = '';
                } else {
                    //console.log(event.option.value.providerTag);
                    this.currentSearchQuery = event.option.value.providerTag ? event.option.value.providerTag : event.option.value;
                    this.search(this.currentSearchQuery);
                }
            }
        });
        this.dataProviderSearchService.investormessageSearch$.subscribe(event => {
            console.log('event', event);
            if (event === 'clear') {
                this.clear();
            }
        });

        this.isProvider = this.principal.hasAnyAuthorityDirect(['ROLE_PROVIDER']);
        const typesArray = [];
        this.filtersService.getTypes().subscribe(data => {
            this.loaderService.display(false);
            const listItems = data['aggregations'] && data['aggregations'].dataTypes ? data['aggregations'].dataTypes.buckets : [];
            for (let i = 0; i < listItems.length; ++i) {
                const types = listItems[i];
                const typesId = parseInt(types.key, 10);
                const typesValue = types.dataTypes.hits.hits[0]._source.dataTypes;
                const typesDescription = typesValue.filter(item => item.id === types.key);
                if (this.isProvider) {
                    typesArray.push({
                        id: typesId,
                        count: types.doc_count,
                        value:
                            typesDescription && typesDescription[0] && typesDescription[0].description
                                ? typesDescription[0].description
                                : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
                        checked: false
                    });
                    //  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
                } else if (types.dataTypes.hits.hits[0]._source.dataTypes !== 'DEFAULT_NULL') {
                    typesArray.push({
                        id: typesId,
                        count: types.doc_count,
                        value:
                            typesDescription && typesDescription[0] && typesDescription[0].description
                                ? typesDescription[0].description
                                : '',
                        checked: false
                    });
                }
            }
            // console.log('Dformat ',deliveryFormatsArrayForFilter);
        });
        const strategiesArray = [];
        this.filtersService.getStrategies().subscribe(data => {
            this.loaderService.display(false);
            const listItems =
                data['aggregations'] && data['aggregations'].investmentStrategies ? data['aggregations'].investmentStrategies.buckets : [];
            for (let i = 0; i < listItems.length; ++i) {
                const strategies = listItems[i];
                const strategiesId = parseInt(strategies.key, 10);
                const strategiesValue = strategies.investmentStrategies.hits.hits[0]._source.investmentStrategies;
                //console.log('Ranjith testing aggregation ', strategiesValue);
                const strategiesDescription = strategiesValue.filter(item => item.id === strategies.key);
                //console.log('Dformat 1',typesDescription);
                if (this.isProvider) {
                    strategiesArray.push({
                        id: strategiesId,
                        count: strategies.doc_count,
                        value:
                            strategiesDescription && strategiesDescription[0] && strategiesDescription[0].description
                                ? strategiesDescription[0].description
                                : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
                        checked: false
                    });
                    //  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
                } else if (strategies.investmentStrategies.hits.hits[0]._source.investmentStrategies !== 'DEFAULT_NULL') {
                    strategiesArray.push({
                        id: strategiesId,
                        count: strategies.doc_count,
                        value:
                            strategiesDescription && strategiesDescription[0] && strategiesDescription[0].description
                                ? strategiesDescription[0].description
                                : '',
                        checked: false
                    });
                }
            }
            // console.log('Dformat ',deliveryFormatsArrayForFilter);
        });


        const assetClassArray = [];
        this.filtersService.getAssetClass().subscribe(data => {
            this.loaderService.display(false);
            const listItems = data['aggregations'] && data['aggregations'].assetClass ? data['aggregations'].assetClass.buckets : [];
            for (let i = 0; i < listItems.length; ++i) {
                const assetClass = listItems[i];
                const assetClassId = parseInt(assetClass.key, 10);
                const assetClassValue = assetClass.assetClass.hits.hits[0]._source.assetClass;
                //console.log('Ranjith testing aggregation ', strategiesValue);
                const assetClassDescription = assetClassValue.filter(item => item.id === assetClass.key);
                //console.log('Dformat 1',typesDescription);
                if (this.isProvider) {
                    assetClassArray.push({
                        id: assetClassId,
                        count: assetClass.doc_count,
                        value:
                            assetClassDescription && assetClassDescription[0] && assetClassDescription[0].description
                                ? assetClassDescription[0].description
                                : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
                        checked: false
                    });
                    //  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
                } else if (assetClass.assetClass.hits.hits[0]._source.assetClass !== 'DEFAULT_NULL') {
                    assetClassArray.push({
                        id: assetClassId,
                        count: assetClass.doc_count,
                        value:
                            assetClassDescription && assetClassDescription[0] && assetClassDescription[0].description
                                ? assetClassDescription[0].description
                                : '',
                        checked: false
                    });
                }
            }
            console.log('asset ', assetClassArray);
        });
        const sectorArray = [];
        this.filtersService.getSector().subscribe(data => {
            this.loaderService.display(false);
            const listItems = data['aggregations'] && data['aggregations'].sector ? data['aggregations'].sector.buckets : [];
            for (let i = 0; i < listItems.length; ++i) {
                const sectors = listItems[i];
                const sectorsId = parseInt(sectors.key, 10);
                const sectorsValue = sectors.sector.hits.hits[0]._source.sector;
                const sectorsDescription = sectorsValue.filter(item => item.id === sectors.key);
                if (this.isProvider) {
                    sectorArray.push({
                        id: sectorsId,
                        count: sectors.doc_count,
                        value:
                            sectorsDescription && sectorsDescription[0] && sectorsDescription[0].description
                                ? sectorsDescription[0].description
                                : '', // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
                        checked: false
                    });
                    //  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
                } else if (sectors.sector.hits.hits[0]._source.sector !== 'DEFAULT_NULL') {
                    sectorArray.push({
                        id: sectorsId,
                        count: sectors.doc_count,
                        value:
                            sectorsDescription && sectorsDescription[0] && sectorsDescription[0].description
                                ? sectorsDescription[0].description
                                : '',
                        checked: false
                    });
                }
            }
            console.log('sectorArray ', sectorArray);
        });

        const primaryTypeArray = [];
        this.filtersService.getPrimaryType().subscribe(data => {
            this.loaderService.display(false);
            const listItems = data['aggregations'] && data['aggregations'].primaryType ? data['aggregations'].primaryType.buckets : [];
            for (let i = 0; i < listItems.length; ++i) {
                const primaryTypes = listItems[i];
                const primaryTypesValue = primaryTypes.primaryType.hits.hits[0]._source.primaryType;
                //    console.log('scoreInvestorsWilling',scoreInvestorsWilling);
                if (this.isProvider) {
                    if (primaryTypesValue.description !== '') {
                        primaryTypeArray.push({
                            id: primaryTypes.key,
                            count: primaryTypes.doc_count,
                            value: primaryTypesValue.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
                            checked: false
                        });
                    }
                    //  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
                } else if (primaryTypes.primaryType.hits.hits[0]._source.primaryType !== 'DEFAULT_NULL') {
                    if (primaryTypesValue.description !== '') {
                        primaryTypeArray.push({
                            id: primaryTypes.key,
                            count: primaryTypes.doc_count,
                            value: primaryTypesValue.description,
                            checked: false
                        });
                    }
                }
            }
        });
        const primarySubTypeArray = [];
        this.filtersService.getPrimarySubType().subscribe(data => {
            this.loaderService.display(false);
            const listItems =
                data['aggregations'] && data['aggregations'].primarySubtype ? data['aggregations'].primarySubtype.buckets : [];
            for (let i = 0; i < listItems.length; ++i) {
                const primarysubTypes = listItems[i];
                const primarySubTypesValue = primarysubTypes.primarySubtype.hits.hits[0]._source.primarySubtype;
                //    console.log('scoreInvestorsWilling',scoreInvestorsWilling);
                if (this.isProvider) {
                    if (primarySubTypesValue.description !== '') {
                        primarySubTypeArray.push({
                            id: primarysubTypes.key,
                            count: primarysubTypes.doc_count,
                            value: primarySubTypesValue.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
                            checked: false
                        });
                    }
                    //  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
                } else if (primarysubTypes.primarySubtype.hits.hits[0]._source.primarySubtype !== 'DEFAULT_NULL') {
                    if (primarySubTypesValue.description !== '') {
                        primarySubTypeArray.push({
                            id: primarysubTypes.key,
                            count: primarysubTypes.doc_count,
                            value: primarySubTypesValue.description,
                            checked: false
                        });
                    }
                }
            }
        });
        const totalAumArray = [];
        this.filtersService.getTotalAum().subscribe(data => {
            this.loaderService.display(false);
            const listItems = data['aggregations'] && data['aggregations'].totalAumRange ? data['aggregations'].totalAumRange.buckets : [];
            for (let i = 0; i < listItems.length; ++i) {
                const totalAum = listItems[i];
                const totalAumValue = totalAum.totalAumRange.hits.hits[0]._source.totalAumRange;
                //    console.log('scoreInvestorsWilling',scoreInvestorsWilling);
                if (this.isProvider) {
                    if (totalAumValue.description !== '') {
                        totalAumArray.push({
                            id: totalAum.key,
                            count: totalAum.doc_count,
                            value: totalAumValue.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
                            checked: false
                        });
                    }
                    //  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
                } else if (totalAum.totalAumRange.hits.hits[0]._source.totalAumRange !== 'DEFAULT_NULL') {
                    if (totalAumValue.description !== '') {
                        totalAumArray.push({
                            id: totalAum.key,
                            count: totalAum.doc_count,
                            value: totalAumValue.description,
                            checked: false
                        });
                    }
                }
            }
        });
        const researchStyleArray = [];
        this.filtersService.getResearchStyle().subscribe(data => {
            this.loaderService.display(false);
            const listItems = data['aggregations'] && data['aggregations'].mainResearchStyle ? data['aggregations'].mainResearchStyle.buckets : [];
            for (let i = 0; i < listItems.length; ++i) {
                const mainResearchStyles = listItems[i];
                const mainResearchStyleValue = mainResearchStyles.mainResearchStyle.hits.hits[0]._source.mainResearchStyle;
                 // console.log('mainResearchStyleValue',mainResearchStyleValue);
                if (this.isProvider) {
                    if (mainResearchStyleValue.description !== '') {
                        researchStyleArray.push({
                            id: mainResearchStyles.key,
                            count: mainResearchStyles.doc_count,
                            value: mainResearchStyleValue.description, // === 'DEFAULT_NULL' ? 'No Asset Class' : asset,
                            checked: false
                        });
                    }
                    //  	console.log('pricingModelArrayForFilter '+pricingModelArrayForFilter);
                } else if (mainResearchStyles.mainResearchStyle.hits.hits[0]._source.mainResearchStyle !== 'DEFAULT_NULL') {
                    if (mainResearchStyleValue.description !== '') {
                        researchStyleArray.push({
                            id: mainResearchStyles.key,
                            count: mainResearchStyles.doc_count,
                            value: mainResearchStyleValue.description,
                            checked: false
                        });
                    }
                }
            }
        });
        this.sortingArrayMethod();
        this.sortCategory = 'investors';
        this.totalItemsTooltip = 'Number of asset managers matching active search and filter criteria.';
        this.sortByTooltip = 'Sort list of asset managers by clicking any of the sort fields.';
        this.filterCheckBox = 'Providers';
        this.filters = [];

        this.filters.push({
            filterName: 'Type',
            filterList: typesArray,
            filterType: 'checkbox',
            filterTerm: 'dataTypes.id',
            expanded: true,
            order: 1,
            mouseoverExplanation: ''
        });
        this.filters.push({
            filterName: 'Strategy',
            filterList: strategiesArray,
            filterType: 'checkbox',
            filterTerm: 'investmentStrategies.id',
            expanded: false,
            order: 2,
            mouseoverExplanation: ''
        });
        this.filters.push({
            filterName: 'Primary Type',
            filterList: primaryTypeArray,
            filterType: 'checkbox',
            filterTerm: 'primaryType.id',
            expanded: false,
            order: 3,
            mouseoverExplanation: ''
        });
        this.filters.push({
            filterName: 'Primary Subtype',
            filterList: primarySubTypeArray,
            filterType: 'checkbox',
            filterTerm: 'primarySubtype.id',
            expanded: false,
            order: 4,
            mouseoverExplanation: ''
        });
        this.filters.push({
            filterName: 'Main Research Style',
            filterList: researchStyleArray,
            filterType: 'checkbox',
            filterTerm: 'mainResearchStyle.id',
            expanded: false,
            order: 5,
            mouseoverExplanation: ''
        });
        this.filters.push({
            filterName: 'Total AUM Range',
            filterList: totalAumArray,
            filterType: 'checkbox',
            filterTerm: 'totalAumRange.id',
            expanded: false,
            order: 6,
            mouseoverExplanation: ''
        });
        this.filters.push({
            filterName: 'Asset Class',
            filterList: assetClassArray,
            filterType: 'checkbox',
            filterTerm: 'assetClass.id',
            expanded: false,
            order: 7,
            mouseoverExplanation: 'The financial security asset class that the asset manager invests in or researches.'
        });
        this.filters.push({
            filterName: 'Sector',
            filterList: sectorArray,
            filterType: 'checkbox',
            filterTerm: 'sector.id',
            expanded: false,
            order: 8,
            mouseoverExplanation: 'The sectors the asset manager invests in or researches.'
        });
        this.sortedIndex = '';
        this.loadAll();
    }
    ngOnDestroy() {}
    sortingArrayMethod() {
        if (this.isProvider) {
            this.sortBy({
                key: 'lastUpdatedDate',
                label: 'Updated',
                order: 'desc',
                DropDown: 'Updated',
                userLevel: 'unrestricted'
            });
        }
        this.sortingArray = [
            {
                key: 'name',
                label: 'Name',
                order: '',
                userLevel: 'unrestricted',
                DropDown: 'Name',
                mouseoverExplanation: 'Name of the asset manager.'
            },
            {
                key: 'createdDate',
                label: 'Added',
                order: '',
                userLevel: 'unrestricted',
                DropDown: 'Added',
                mouseoverExplanation: 'Date the asset manager was discovered and added.'
            },
            {
                key: 'lastUpdatedDate',
                label: 'Updated',
                order: 'desc',
                userLevel: 'unrestricted',
                DropDown: 'Updated',
                mouseoverExplanation: "Date the asset manager's profile was last updated."
            },
            {
                key: 'totalAumInMillions',
                label: 'AUM',
                order: '',
                userLevel: 'unrestricted',
                DropDown: 'totalAumInMillions',
                mouseoverExplanation:''
            }
            /* 	{
		key: 'Scores',
		label: 'Scores',
		order: '',
		DropDown: 'Scores',
		userLevel: 'unrestricted',
	} */
        ];

    }
    sortBy(column: any) {
        console.log('col', column);
        this.investors = [];
        this.page = 0;
        this.itemsFrom = 0;
        if (!column) {
            return this.clear();
        }
        const index = column === 'none' ? -1 : this.sortingArray.indexOf(column);
        this.sortedIndex = index;
        for (let i = 0; i < this.sortingArray.length; i++) {
            if (i === index) {
                this.sortingArray[index].order = column.order === 'desc' ? 'asc' : 'desc';
            } else {
                this.sortingArray[i].order = '';
            }
        }
        this.currentSorting = column !== 'none' ? column : '';
        if (column === 'none') {
            this.currentSorting = '';
            return;
        }
        this.loadAll();

        this.links = {
            last: 0
        };
    }
    clear() {
        this.investors = [];
        this.page = 0;
        /* 	this.predicate = 'id';
	this.reverse = true; */
        this.currentSearch = '';
        this.currentSearchQuery = '';
        this.searchCommonValue = false;
        this.sortBy({
            key: 'lastUpdatedDate',
            label: 'Updated',
            order: 'desc',
            DropDown: 'Updated',
            userLevel: 'unrestricted'
        });
        this.sortingArrayMethod();
        this.loadAll();
        this.links = {
            last: 0
        };
    }
    searchCommon(query) {
        if (query) {
            this.searchCommonValue = true;
            this.investors = [];
            this.page = 0;
            this.itemsFrom = 0;
            this.sortBy('none');
            this.currentSearch = {
                query: query + '*', // this.reSubmitValues ? '*' + query + '*' : query,
                fields: ['_all', 'name^2']
            };
            this.loadAll();
        }
    }

    search(query) {
        this.links = {
            last: 0
        };
        this.page = 0;
        /* this.predicate = '_score';
	this.reverse = false; */
        this.itemsFrom = 0;
        this.sortBy('none');
        this.currentSearchQuery = query;
        if (this.currentFilter) {
            const event = 'clear Filter';
            this.dataProviderfilterService.clearFilterRemoveSearch(event);
            this.clearFilter = true;
            this.onClearFilter();
        }
        this.currentSearch = {
            query: {
                multi_match: {
                    fields: ['name'],
                    query: query,
                    type: 'phrase_prefix'
                }
            }
        };
        this.loadAll();
    }
    onFilter(options: any) {
        if (!options.query || options.query === 'clearFilter') {
            this.currentFilter = '';
            this.onClearFilter();
        } else {
            this.currentFilter = options.query ? options.query : '';
        }
        this.statusFilter = options.statusFilter ? options.statusFilter : '';
        this.investors = [];
        this.page = 0;
        this.itemsFrom = 0;
        this.links = {
            last: 0
        };
        this.loadAll();
    }
    onClearFilter() {
        /* this.paramFilter = '';
	this.paramID = ''; */
        this.currentFilter = '';
        this.statusFilter = '';
        this.investors = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.itemsFrom = 0;
        //this.reverse = false;
        /* this.corona=false;*/
        this.selectFilters = [];
        this.selectFilterData = [];
        this.loadAll();
        this.router.navigate(['/investors']);
        return;
    }

    onFilterCheck(event) {}
    onFilterChanges(event) {
        if (event === 'clearFilter') {
            this.onFilter({
                query: 'clearFilter'
            });
            return;
        }
        if (event && event.selectedFilterData && event.selectedFilterData.length) {
            const requestBody = bob.requestBodySearch();
            const allFilterQuery = bob.boolQuery();
            const filters = event.selectedFilterData;
            const filtersLength = filters.length;
            for (let i = 0; i < filtersLength; i++) {
                const term = filters[i].queryTerm;
                const filter = filters[i].filters;
                // console.log(filter);
                if (filters[i].filters && filter.length) {
                    if ((filters[i].queryType === 'must' || filters[i].queryType === 'should') && term === 'marketplaceStatus') {
                        this.statusFilter = '';
                        if (filter === 'Followed' || filter === 'Unlocked') {
                            this.statusFilter = filter;
                        } else if (filter === 'Complete') {
                            allFilterQuery.should(bob.matchQuery(term, 'Expanded'));
                            allFilterQuery.should(bob.matchQuery(term, 'Detailed'));
                            allFilterQuery.should(bob.matchQuery(term, 'Followed'));
                            allFilterQuery.should(bob.matchQuery(term, 'Unlocked'));
                            this.statusFilter = '';
                        } else if (filter !== 'Followed' && filter !== 'Unlocked') {
                            allFilterQuery.should(bob.matchQuery(term, filter));
                        }
                    } else if (filters[i].queryType === 'rangeBetween') {
                        allFilterQuery.must(
                            bob
                                .rangeQuery(term)
                                .gte(filter[0][0])
                                .lte(filter[0][1])
                        );
                    } else {
                        allFilterQuery.must(bob.termsQuery(term, filter));
                    }
                }
            }

            requestBody.query(allFilterQuery);

            this.onFilter({
                query: requestBody.toJSON(),
                statusFilter: this.statusFilter
            });
            return;
        }
    }
    loadPage(page) {
        console.log('page', page);
        this.limitedProviders = '';
        this.page = page;
        this.itemsFrom = this.page === 0 ? 0 : this.page * 20;
        console.log('itemsFrom', this.itemsFrom);
        console.log('total', this.totalItems);
        console.log('current', this.currentFilter);
        if (this.itemsFrom <= this.totalItems) {
            this.limitedProviders = '';
            if (this.page <= 4 && this.currentFilter === '') {
                this.loadAll();
                this.loaderInfinite = true;
                this.loader = false;
                this.limitedProviders = '';
            } else if (this.page >= 5) {
                this.limitedProviders =
                    'Maximum number of entries listed. Please refine your search/filtering criteria to display the most appropriate entries.';
                this.limitedLogoutUsersEvent = false;
                this.loaderInfinite = false;
            } else if (this.page > 0 && this.currentFilter && this.page <= 4) {
                this.loadAll();
                this.loaderInfinite = true;
                this.loader = false;
                this.limitedProviders = '';
            }
        }
    }
    loadAll() {
        this.limitedProviders = '';
        this.loader = true;
        let queryBody = {};
        console.log('sorting', this.currentSorting);
        if (this.currentSorting && this.currentFilter) {
            const requestBody = bob.requestBodySearch();
            // requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
            if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
                requestBody
                    .sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc'))
                    .size(20);
            } else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
                for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
                    requestBody
                        .sort(
                            bob
                                .sort(this.currentSorting.recommendedSorting[i]['key'])
                                .order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')
                        )
                        .size(20);
                }
            }
            const queryJSON = requestBody.toJSON();
            queryJSON['filter'] = this.currentFilter;
            queryBody = queryJSON;
        } else {
            if (this.currentSearch && !this.searchCommonValue) {
                console.log('2');
                const requestBody = bob.requestBodySearch();
                if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
                    requestBody
                        .sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc'))
                        .size(20);
                } else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
                    for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
                        requestBody
                            .sort(
                                bob
                                    .sort(this.currentSorting.recommendedSorting[i]['key'])
                                    .order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')
                            )
                            .size(20);
                    }
                }
                const queryJSON = requestBody.toJSON();
                queryBody = queryJSON;
                //console.log('query',queryJSON);
                queryBody = this.currentSearch;
                let newObj = Object.assign(queryBody,this.currentSearch);
                queryBody =  Object.assign(newObj,queryJSON);
                //console.log('query',this.currentSearch);

                //queryBody['size'] = 20;
                //queryBody =this.currentSearch;
            } else if (this.currentSearch && this.searchCommonValue) {
                console.log('3');
                queryBody = {
                    size: 20,
                    query: {
                        query_string: this.currentSearch
                    }
                };
                const requestBody = bob.requestBodySearch();
                if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
                    requestBody
                        .sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc'))
                        .size(20);
                } else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
                    for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
                        requestBody
                            .sort(
                                bob
                                    .sort(this.currentSorting.recommendedSorting[i]['key'])
                                    .order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')
                            )
                            .size(20);
                    }
                }
                const queryJSON = requestBody.toJSON();
                //console.log('query',queryJSON);
                let queryBodyObj =  Object.assign(queryBody,queryJSON);
                queryBody=Object.assign(queryBodyObj);
                //console.log('query',this.currentSearch);
            }
            if (this.currentSorting && !this.currentSearch) {
                console.log('4');
                const requestBody = bob.requestBodySearch();
                if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
                    requestBody
                        .sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc'))
                        .size(20);
                } else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
                    for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
                        requestBody
                            .sort(
                                bob
                                    .sort(this.currentSorting.recommendedSorting[i]['key'])
                                    .order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')
                            )
                            .size(20);
                    }
                }
                const queryJSON = requestBody.toJSON();
                queryBody = queryJSON;
            }
        }
        if (this.page) {
            console.log('5');
            if (this.currentSearch && this.searchCommonValue === false) {
                // Check for provider list button filter query.
                // queryBody = {};
                // queryBody = {
                // 	size: 20,
                // 	query: {
                // 		query_string: this.currentSearch
                // 	}
                // };
                queryBody = this.currentSearch;
                queryBody['size'] = 20;
                console.log(this.currentSearch, queryBody);
            }
            queryBody['from'] = this.itemsFrom;
        }
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        console.log('qurybody', queryBody);
        if(Object.keys(queryBody).length === 0)
        {
            queryBody['size'] = 20;
            console.log('qurybodysize', queryBody);
        }
        this.subscription = this.investorService
            .queryWithSearchFilterInvestor(queryBody, this.statusFilter)
            .subscribe(res => this.onSuccessFilter(res, res.headers), res => this.onError(res));
    }
    private onError(error) {
        this.loader = false;
        this.loaderInfinite = false;
    }
    private onSuccessFilter(data, headers) {
        this.loader = false;
        this.loaderInfinite = false;
        console.log('loader', this.loader);
        this.totalItems = data['hits'] ? data['hits'].total : 0;
        this.links.last = Math.ceil(this.totalItems / 20);
        if (!this.page) {
            this.investors = [];
        }
        if (data['hits'] && data['hits'].hits) {
            for (let i = 0; i < data['hits'].hits.length; i++) {
                const providerObj = data['hits'].hits[i]._source;
                this.investors.push(providerObj);
            }
            console.log('inv', this.investors);
            /* if (this.totalItems === 0 && this.currentSearchQuery && !this.reSubmitValues) {
			this.reSubmitValues = true;
			setTimeout(() => {
				this.search(this.currentSearchQuery);
			}, 3000);
		} else {
			this.reSubmitValues = false;
		} */
        }
    }
    reset() {
        this.page = 0;
        this.investors = [];
        this.loadAll();
    }

    onExpireSession(event) {
        //console.log('eventcheck',event);
        if (event) {
            //console.log('eventcheck',event);
            // this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
                setTimeout(() => {
                    this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
                }, 2000);
            });
        }
    }
    beforeSessionExpires(event) {
        if (event) {
            // this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }
}
