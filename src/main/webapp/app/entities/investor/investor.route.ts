import { Routes } from '@angular/router';
import { UserRouteAccessService } from '../../shared';
import { InvestorComponent } from './investor.component';




export const investorRoute: Routes = [
    {
        path: 'investors',
        component: InvestorComponent,
        data: {
            authorities: ['ROLE_PROVIDER','ROLE_ADMIN'],
            pageTitle: 'Asset Managers',
            breadcrumb: 'Investors'
        },
        canActivate: [UserRouteAccessService]
    }
];
