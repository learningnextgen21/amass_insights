import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
    DatamarketplaceSharedModule,
    FormlyEditorFieldComponent,
    FormlyAutoCompleteFieldComponent,
    FormlyMultiSelectFieldComponent,
    FormlyDropdownFieldComponent,
    FormlyMaskFieldComponent,
    FormlySliderFieldComponent,
    FormlyRadioButtonFieldComponent,
    FormlyTextAreaFieldComponent,
} from '../../shared';
import { FormlyFieldButton } from '../../shared/custom-form-fields/button-field.component';
import { FormlyModule } from '@ngx-formly/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyHorizontalWrapperComponent } from 'app/entities/data-provider/horizontal-wrapper';
import { UppyModule } from 'app/uppy/uppy.module';
import { UppyService } from 'app/uppy';
import { GenericProfileModule } from 'app/shared/generic-profile';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { MatMenuModule } from '@angular/material/menu';
import { CommentsModule } from '../../comments';
import { InactivityTimerComponent, GenericConfirmDialogComponent } from 'app/shared/auth/inactivity-timer.component';
import { from } from 'rxjs';
import { FormlyDatePickerFieldComponent } from 'app/shared/custom-form-fields/datepicker-field.component';
import { FormlyFileUploadFieldComponent } from 'app/shared/custom-form-fields/fileupload-form-field.component';
import { UppyDialogComponent } from 'app/uppy/uppy/uppy-dialog.component';
import { FormlyFieldCustomInput } from 'app/shared/custom-form-fields/input-form-field.component';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import { InvestorComponent } from './investor.component';
import { investorRoute } from './investor.route';
import { InvestorService } from './investor.service';

export function maxlengthValidationMessage(err, field) {
    return `This field has a maximum length of  ${field.templateOptions.maxLength}  characters. Please shorten this text to fit under this length.`;
  }
@NgModule({
    imports: [
        //CommentsModule,
        DatamarketplaceSharedModule,
        // HttpClientModule,
        //ReactiveFormsModule,
        //MatMenuModule,
        RouterModule.forRoot(investorRoute, { useHash: true }),
       /*  FormlyModule.forRoot({
            wrappers: [{ name: 'form-field-horizontal', component: FormlyHorizontalWrapperComponent  }],
            validationMessages: [
                { name: 'required', message: 'This field is required' },
                { name: 'maxlength', message: maxlengthValidationMessage },
            ],
            types: [{
                name: 'inputEditor', component: FormlyEditorFieldComponent
            }, {
                name: 'autocomplete', component: FormlyAutoCompleteFieldComponent
            }, {
                name: 'multiselect', component: FormlyMultiSelectFieldComponent
            }, {
                name: 'dropdown', component: FormlyDropdownFieldComponent
            }, {
                name: 'mask', component: FormlyMaskFieldComponent
            }, {
                name: 'slider', component: FormlySliderFieldComponent
            }, {
                name: 'radioButton', component: FormlyRadioButtonFieldComponent
            }, {
                name: 'textarea', component: FormlyTextAreaFieldComponent
            }, {
                name: 'datepicker', component: FormlyDatePickerFieldComponent
            }, {
                name: 'fileUpload', component: FormlyFileUploadFieldComponent
            },{
                name: 'button', component: FormlyFieldButton
            },{
                name: 'input', component: FormlyFieldCustomInput
            }]
        }),
        FormlyBootstrapModule,
        UppyModule,
        GenericProfileModule,
        ShareButtonModule,
        ShareButtonsModule.withConfig({
            debug: true
        }),
        ProgressSpinnerModule */
    ],
    declarations: [
        InvestorComponent
    ],
    entryComponents: [
    ],
    providers: [
        // { provide: HTTP_INTERCEPTORS, useClass: MyHttpLogInterceptor, multi: true }
        InvestorService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceInvestorModule {}