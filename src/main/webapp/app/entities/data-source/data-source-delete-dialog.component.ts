import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DataSource } from './data-source.model';
import { DataSourcePopupService } from './data-source-popup.service';
import { DataSourceService } from './data-source.service';

@Component({
    selector: 'jhi-data-source-delete-dialog',
    templateUrl: './data-source-delete-dialog.component.html'
})
export class DataSourceDeleteDialogComponent {

    dataSource: DataSource;

    constructor(
        private dataSourceService: DataSourceService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dataSourceService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dataSourceListModification',
                content: 'Deleted an dataSource'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-data-source-delete-popup',
    template: ''
})
export class DataSourceDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataSourcePopupService: DataSourcePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dataSourcePopupService
                .open(DataSourceDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
