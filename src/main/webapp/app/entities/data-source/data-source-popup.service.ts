import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DataSource } from './data-source.model';
import { DataSourceService } from './data-source.service';

@Injectable()
export class DataSourcePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private dataSourceService: DataSourceService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dataSourceService.find(id).subscribe((dataSource) => {
                    if (dataSource.createdDate) {
                        dataSource.createdDate = {
                            year: dataSource.createdDate.getFullYear(),
                            month: dataSource.createdDate.getMonth() + 1,
                            day: dataSource.createdDate.getDate()
                        };
                    }
                    dataSource.updatedDate = this.datePipe
                        .transform(dataSource.updatedDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.dataSourceModalRef(component, dataSource);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dataSourceModalRef(component, new DataSource());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dataSourceModalRef(component: Component, dataSource: DataSource): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dataSource = dataSource;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
