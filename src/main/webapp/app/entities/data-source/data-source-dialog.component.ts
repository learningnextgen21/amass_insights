import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { DataSource } from './data-source.model';
import { DataSourcePopupService } from './data-source-popup.service';
import { DataSourceService } from './data-source.service';
import { DataProvider, DataProviderService } from '../data-provider';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-data-source-dialog',
    templateUrl: './data-source-dialog.component.html'
})
export class DataSourceDialogComponent implements OnInit {

    dataSource: DataSource;
    isSaving: boolean;

    dataproviders: DataProvider[];
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private dataSourceService: DataSourceService,
        private dataProviderService: DataProviderService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.dataProviderService.query()
            .subscribe((res) => { this.dataproviders = res.body; }, (res) => this.onError(res));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dataSource.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dataSourceService.update(this.dataSource));
        } else {
            this.subscribeToSaveResponse(
                this.dataSourceService.create(this.dataSource));
        }
    }

    private subscribeToSaveResponse(result: Observable<DataSource>) {
        result.subscribe((res: DataSource) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DataSource) {
        this.eventManager.broadcast({ name: 'dataSourceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackDataProviderById(index: number, item: DataProvider) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-data-source-popup',
    template: ''
})
export class DataSourcePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataSourcePopupService: DataSourcePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dataSourcePopupService
                    .open(DataSourceDialogComponent as Component, params['id']);
            } else {
                this.dataSourcePopupService
                    .open(DataSourceDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
