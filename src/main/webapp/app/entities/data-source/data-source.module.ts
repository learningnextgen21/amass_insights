import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    DataSourceService,
    DataSourcePopupService,
    DataSourceComponent,
    DataSourceDetailComponent,
    DataSourceDialogComponent,
    DataSourcePopupComponent,
    DataSourceDeletePopupComponent,
    DataSourceDeleteDialogComponent,
    dataSourceRoute,
    dataSourcePopupRoute,
} from './';

const ENTITY_STATES = [
    ...dataSourceRoute,
    ...dataSourcePopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DataSourceComponent,
        DataSourceDetailComponent,
        DataSourceDialogComponent,
        DataSourceDeleteDialogComponent,
        DataSourcePopupComponent,
        DataSourceDeletePopupComponent,
    ],
    entryComponents: [
        DataSourceComponent,
        DataSourceDialogComponent,
        DataSourcePopupComponent,
        DataSourceDeleteDialogComponent,
        DataSourceDeletePopupComponent,
    ],
    providers: [
        DataSourceService,
        DataSourcePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceDataSourceModule {}
