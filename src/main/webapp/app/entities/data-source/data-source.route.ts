import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DataSourceComponent } from './data-source.component';
import { DataSourceDetailComponent } from './data-source-detail.component';
import { DataSourcePopupComponent } from './data-source-dialog.component';
import { DataSourceDeletePopupComponent } from './data-source-delete-dialog.component';

export const dataSourceRoute: Routes = [
    {
        path: 'data-source',
        component: DataSourceComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataSource.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'data-source/:id',
        component: DataSourceDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataSource.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dataSourcePopupRoute: Routes = [
    {
        path: 'data-source-new',
        component: DataSourcePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataSource.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-source/:id/edit',
        component: DataSourcePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataSource.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-source/:id/delete',
        component: DataSourceDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataSource.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
