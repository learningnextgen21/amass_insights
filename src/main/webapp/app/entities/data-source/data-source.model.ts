import { BaseEntity } from './../../shared';

export class DataSource implements BaseEntity {
    constructor(
        public id?: number,
        public recordID?: string,
        public dataSource?: string,
        public explanation?: any,
        public inactive?: number,
        public createdDate?: any,
        public updatedDate?: any,
        public providerSources?: BaseEntity[],
    ) {
    }
}
