

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { DataSource } from './data-source.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class DataSourceService {

    private resourceUrl = 'api/data-sources';
    private resourceSearchUrl = 'api/_search/data-sources';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(dataSource: DataSource): Observable<any> {
        const copy = this.convert(dataSource);
        return this.http.post(this.resourceUrl, copy, { observe: 'response' });
    }

    update(dataSource: DataSource): Observable<any> {
        const copy = this.convert(dataSource);
        return this.http.put(this.resourceUrl, copy, { observe: 'response' });
    }

    find(id: number): Observable<any> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, { params: options, observe: 'response' });
    }

    private convert(dataSource: DataSource): DataSource {
        const copy: DataSource = Object.assign({}, dataSource);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(dataSource.createdDate);

        copy.updatedDate = this.dateUtils.toDate(dataSource.updatedDate);
        return copy;
    }
}
