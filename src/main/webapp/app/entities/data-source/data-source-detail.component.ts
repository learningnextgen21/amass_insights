import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { DataSource } from './data-source.model';
import { DataSourceService } from './data-source.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-data-source-detail',
    templateUrl: './data-source-detail.component.html'
})
export class DataSourceDetailComponent implements OnInit, OnDestroy {

    dataSource: DataSource;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private dataSourceService: DataSourceService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDataSources();
    }

    load(id) {
        this.dataSourceService.find(id).subscribe((dataSource) => {
            this.dataSource = dataSource;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDataSources() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dataSourceListModification',
            (response) => this.load(this.dataSource.id)
        );
    }
}
