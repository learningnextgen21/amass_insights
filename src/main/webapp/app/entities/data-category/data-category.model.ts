import { BaseEntity } from './../../shared';

export class DataCategory implements BaseEntity {
    constructor(
        public id?: number,
        public logo?: any,
        public recordID?: string,
        public datacategory?: any,
        public relevantDataCategoryProviderCount?: number,
        public mainDataCategoryProviderCount?:number,
        public hashtag?: string,
        public followed?: boolean,
        public explanation?: any,
        public inactive?: number,
        public createdDate?: any,
        public newRelevantProviderDate?: any,
        public newMainProviderDate?: any,
        public updatedDate?: any,
        public dataProviders?: BaseEntity[],
        public categoryFeatures?: BaseEntity[],
        public categoryAuthorities?: BaseEntity[],
        public categoryProviders?: BaseEntity[],
        public locked?: boolean
    ) {
    }
}
