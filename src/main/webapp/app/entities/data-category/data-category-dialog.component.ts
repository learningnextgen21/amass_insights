import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { DataCategory } from './data-category.model';
import { DataCategoryPopupService } from './data-category-popup.service';
import { DataCategoryService } from './data-category.service';
import { DataProvider, DataProviderService } from '../data-provider';
import { DataFeature, DataFeatureService } from '../data-feature';
// import { Authority, AuthorityService } from '../authority';
import { ResponseWrapper } from '../../shared';
import {BaseEntity} from '../../shared/model/base-entity';

@Component({
    selector: 'jhi-data-category-dialog',
    templateUrl: './data-category-dialog.component.html'
})
export class DataCategoryDialogComponent implements OnInit {

    dataCategory: DataCategory;
    isSaving: boolean;

    datafeatures: DataFeature[];

   //  authorities: BaseEntity[];

    dataproviders: DataProvider[];
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private dataCategoryService: DataCategoryService,
        private dataProviderService: DataProviderService,
        private dataFeatureService: DataFeatureService,
//         private authorityService: AuthorityService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.dataFeatureService.query()
            .subscribe((res) => { this.datafeatures = res; }, (res) => this.onError(res));
        //  this.authorityService.query()
        //      .subscribe((res) => { this.authorities = res; }, (res) => this.onError(res));
        this.dataProviderService.query()
            .subscribe((res) => { this.dataproviders = res.body; }, (res) => this.onError(res));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dataCategory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dataCategoryService.update(this.dataCategory));
        } else {
            this.subscribeToSaveResponse(
                this.dataCategoryService.create(this.dataCategory));
        }
    }

    private subscribeToSaveResponse(result: Observable<DataCategory>) {
        result.subscribe((res: DataCategory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DataCategory) {
        this.eventManager.broadcast({ name: 'dataCategoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackDataFeatureById(index: number, item: DataFeature) {
        return item.id;
    }

    trackAuthorityById(index: number, item: BaseEntity) {
        return item.id;
    }

    trackDataProviderById(index: number, item: DataProvider) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-data-category-popup',
    template: ''
})
export class DataCategoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataCategoryPopupService: DataCategoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dataCategoryPopupService
                    .open(DataCategoryDialogComponent as Component, params['id']);
            } else {
                this.dataCategoryPopupService
                    .open(DataCategoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
