import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { DataCategory } from './data-category.model';
import { DataCategoryService } from './data-category.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-data-category-detail',
    templateUrl: './data-category-detail.component.html'
})
export class DataCategoryDetailComponent implements OnInit, OnDestroy {

    dataCategory: DataCategory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private dataCategoryService: DataCategoryService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDataCategories();
    }

    load(id) {
        this.dataCategoryService.find(id).subscribe((dataCategory) => {
            this.dataCategory = dataCategory;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDataCategories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dataCategoryListModification',
            (response) => this.load(this.dataCategory.id)
        );
    }
}
