import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { DataCategory } from './data-category.model';
import { createRequestOption } from '../../shared';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataCategoryService {

    private resourceUrl = 'api/data-categories';
    private resourceSearchUrl = 'api/_search/data-categories';
    private resourceElasticSearchUrl = 'api/es/datacategorysearch';
    private resourceFollowUrl = 'api/follow-category?dataCategoryID';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(dataCategory: DataCategory): Observable<DataCategory> {
        const copy = this.convert(dataCategory);
        return this.http.post(this.resourceUrl, copy);
    }

    update(dataCategory: DataCategory): Observable<DataCategory> {
        const copy = this.convert(dataCategory);
        return this.http.put(this.resourceUrl, copy);
    }

    find(id: number): Observable<DataCategory> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, {params: options});
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, {params: options});
    }

    elasticSearch(req?: any, statusFilter?: any): Observable<any> {
        let esUrl = '';
        switch (statusFilter) {
            case 'Followed':
            esUrl = this.resourceElasticSearchUrl + '?followedDatacategory=true';
            break;
            default:
            esUrl = this.resourceElasticSearchUrl;
            break;
        }
        return this.http.post(esUrl, req);
    }

    followAction(id?: number): Observable<any> {
		return this.http.get(`${this.resourceFollowUrl}=${id}`);
    }

    private convert(dataCategory: DataCategory): DataCategory {
        const copy: DataCategory = Object.assign({}, dataCategory);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(dataCategory.createdDate);

        copy.updatedDate = this.dateUtils.toDate(dataCategory.updatedDate);
        return copy;
    }
}
