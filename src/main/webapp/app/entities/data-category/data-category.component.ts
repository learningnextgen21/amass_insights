import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { DataCategory } from './data-category.model';
import { DataCategoryService } from './data-category.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, GenericFilterListModel, GenericFilterModel } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { DomSanitizer } from '@angular/platform-browser';
import { LoaderService } from '../../loader/loaders.service';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { GenericProfileCardModel, GenericProfileCardButton } from 'app/shared/generic-profile-card';
//import { GenericProfileCardButtonModel } from 'app/shared/generic-profile-card/generic-profile-card-buttons.model'
import { DatePipe } from '@angular/common';
import { AuthServerProvider,StateStorageService,SigninModalService } from '../../shared';

const bob = require('elastic-builder');

@Component({
    selector: 'jhi-data-category',
    templateUrl: './data-category.component.html',
    styleUrls: [
        'data-category.component.css',
        '../data-article/data-article.component.css',
        '../data-provider/data-provider.component.scss',
        '../../../content/scss/amass-form.scss'
    ]
})
export class DataCategoryComponent implements OnInit, OnDestroy {

    dataCategories: DataCategory[];
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    currentSearchQuery: string;
    currentSearch: any;
    currentFilter: string;
    itemsFrom: number;
    sortedIndex: any;
    currentSorting: string;
    sortingCategoryArray = [];
    sortingArray = [];
    loader: any;
	loading: any;
    loaderInfinite: any;
    isAdmin: boolean;
    filters: GenericFilterModel[];
    statusFilter: string;
    paramID: any;
    paramFilter: any;
    sortCategory: string;
    filterCheckBox: any;
    currentUrl: any;
    pageName = 'list';
    genericProfileCard: GenericProfileCardModel[] = [];

    constructor(
        private dataCategoryService: DataCategoryService,
        private alertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private principal: Principal,
        public _DomSanitizer: DomSanitizer,
        private datePipe: DatePipe,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private loaderService: LoaderService,
        private stateService: StateStorageService,
        private authService: AuthServerProvider,
	    private signinModalService: SigninModalService,
    ) {
        this.dataCategories = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;
        this.currentSorting = '';
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        this.loader = true;
        let queryBody = {};
        if (this.currentSorting && this.currentFilter && this.currentSearch) {
			const requestBody = bob.requestBodySearch();
			requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
			const queryJSON = requestBody.toJSON();
			queryJSON['query'] = {
				query_string: this.currentSearch
			};
			queryJSON['filter'] = this.currentFilter;
			queryBody = queryJSON;
		} else if (this.currentSorting && this.currentFilter) {
			const requestBody = bob.requestBodySearch();
			requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
			const queryJSON = requestBody.toJSON();
			queryJSON['filter'] = this.currentFilter;
			queryBody = queryJSON;
		} else if (this.currentFilter && this.currentSearch) {
			queryBody = {
				size: 20,
				query: {
					query_string: this.currentSearch
				},
				filter: this.currentFilter['query']
			};
		} else if (this.currentSorting && this.currentSearch) {
			const requestBody = bob.requestBodySearch();
			requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
			const queryJSON = requestBody.toJSON();
			queryJSON['query'] = {
				query_string: this.currentSearch
			};
			queryBody = queryJSON;
		} else {
			if (this.currentSearch) {
				queryBody = {
					size: 20,
					query: {
						query_string: this.currentSearch
					}
				};
			}
			if (this.currentFilter) {
				queryBody = {
					size: 20,
					filter: this.currentFilter['query']
				};
			}
			if (this.currentSorting) {
				const requestBody = bob.requestBodySearch();
				requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
				const queryJSON = requestBody.toJSON();
				queryBody = queryJSON;
			}
		}
        if (this.page) {
			queryBody['from'] = this.itemsFrom;
		}
        this.dataCategoryService.elasticSearch(queryBody, this.statusFilter).subscribe(
            (res) => this.onSuccess(res, res.headers),
            (res) => this.onError(res)
        );
    }
    provider(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    searchClear(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
    categoryFollow(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    categoryNav(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    reset() {
        this.page = 0;
        this.dataCategories = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.itemsFrom = (this.page === 0) ? 0 : (this.page * 20);
		if (this.itemsFrom <= this.totalItems) {
            this.loadAll();
            this.loaderInfinite = true;
            this.loader = false;
		}
    }

    clear() {
        this.loader = true;
        this.dataCategories = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.predicate = 'id';
        this.reverse = true;
        this.currentSearch = '';
        this.currentSearchQuery = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.dataCategories = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.reverse = false;
		this.itemsFrom = 0;
        this.sortBy('none');
        this.currentSearchQuery = query;
		this.currentSearch = {
			'query': query + '*',
			'fields': ['_all', 'datacategory^2']
		};
        this.loadAll();
    }
    onKeydown(event: any) {
        this.googleAnalyticsService.emitEvents(event.keyword, this.currentUrl);
    }
    ngOnInit() {
        this.currentUrl = this.router.url;
        this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
        this.loaderService.display(true);
        this.loader = true;
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.filters = [];
        this.filters.push({
			filterName: 'Status',
			filterTerm: 'status',
			filterType: 'radio',
			filterList: [{
				value: 'Followed',
                checked: false,
                show: true
            }],
            expanded: true,
            order: 1
        });
        this.sortBy({key: 'newMainProviderDate', order: 'desc', userLevel: 'unrestricted',});
        this.registerChangeInDataCategories();
        this.sortingArray = [
			{
				key: 'sortorder',
				label: 'Category',
                order: '',
                userLevel: 'unrestricted',
                DropDown:'Category',
                mouseoverExplanation: 'Name of the data category.'
			},
			{
				key: 'createdDate',
				label: 'Added',
                order: '',
                userLevel: 'unrestricted',
                DropDown:'Added',
                mouseoverExplanation: 'Date the data catgory was created.'
			},
			{
				key: 'newRelevantProviderDate',
				label: 'Provider Last Added',
                order: '',
                userLevel: 'unrestricted',
                DropDown:'Provider Last Added',
                mouseoverExplanation: 'Date a data provider was most recently discovered and added to the corresponding data category.'
			},
			{
				key: 'newMainProviderDate',
				label: 'Main Provider Last Added',
                order: 'desc',
                userLevel: 'unrestricted',
                DropDown:'Main Provider Last Added',
                mouseoverExplanation: 'Date a main data provider was most recently discovered and added to the corresponding data category.'
			},
			{
				key: 'relevantDataCategoryProviderCount',
				label: 'Providers',
                order: '',
                userLevel: 'unrestricted',
                DropDown:'Providers',
                mouseoverExplanation: 'Total count of data providers linked to the corresponding data category.'
			},
			{
				key: 'mainDataCategoryProviderCount',
				label: 'Main Providers',
                order: '',
                userLevel: 'unrestricted',
                DropDown:'Main Providers',
                mouseoverExplanation: 'Count of data providers with the corresponding data category as their Main Data Category.'
			}
        ];
        this.sortCategory = 'Categories';
        this.filterCheckBox = 'Categories';
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: DataCategory) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    registerChangeInDataCategories() {
        this.eventSubscriber = this.eventManager.subscribe('dataCategoryListModification', (response) => this.reset());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }
    sortBy(column: any) {
		console.log('sortBy...', column);
        this.dataCategories = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.itemsFrom = 0;
		if (!column) {
			return this.clear();
		}
		const index = column === 'none' ? -1 : this.sortingArray.indexOf(column);
		this.sortedIndex = index;
		for (let i = 0; i < this.sortingArray.length; i++) {
			if (i === index) {
             //   this.loader = true;
				this.sortingArray[index].order = (column.order === 'desc') ? 'asc' : 'desc';
			} else {
				this.sortingArray[i].order = '';
			}
		}

		this.currentSorting = column !== 'none' ? column : '';
		if (column === 'none') {
			this.currentSorting = '';
			return;
		}
		this.loadAll();
    }

    onFilter(options: any) {
		// console.log('onFilter...', options);
		if (!options.query || options.query === 'clearFilter') {
			this.currentFilter = '';
			this.onClearFilter();
		} else {
			this.currentFilter = options.query ? options.query : '';
		}
		this.statusFilter = options.statusFilter ? options.statusFilter : '';
		this.dataCategories = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.itemsFrom = 0;
		this.reverse = false;
		this.loadAll();
    }

    onClearFilter() {
		this.paramFilter = '';
		this.paramID = '';
		this.currentFilter = '';
		this.statusFilter = '';
		this.dataCategories = [];
		this.links = {
			last: 0
		};
		this.page = 0;
		this.itemsFrom = 0;
		this.reverse = false;
		this.loadAll();
		this.router.navigate(['/categories']);
		return;
	}

    onFilterChanges(event) {
        if (event === 'clearFilter') {
			this.onFilter({
				query: 'clearFilter'
			});
			return;
		}
		if (event && event.selectedFilterData && event.selectedFilterData.length) {
			const requestBody = bob.requestBodySearch();
			const allFilterQuery = bob.boolQuery();
			const filters = event.selectedFilterData;
			const filtersLength = filters.length;
			for (let i = 0; i < filtersLength; i++) {
				const term = filters[i].queryTerm;
				const filter = filters[i].filters;
				if (filters[i].filters && filter.length) {
					if ((filters[i].queryType === 'must' || filters[i].queryType === 'should') && term === 'status') {
						this.statusFilter = '';
						if (filter === 'Followed') {
							this.statusFilter = filter;
						} else {
							allFilterQuery.should(bob.matchQuery(term, filter));
						}
					} else if (filters[i].queryType === 'range' || filters[i].queryType === 'rangeFromTo') {
						allFilterQuery.must(bob.rangeQuery(term).from(filter[0][0]).to(filter[0][1]));
					} else if (filters[i].queryType === 'rangeBetween') {
						allFilterQuery.must(bob.rangeQuery(term).gte(filter[0][0]).lte(filter[0][1]));
					} else {
						allFilterQuery.must(bob.termsQuery(term, filter));
					}
				}
			}

			requestBody.query(allFilterQuery);

			this.onFilter({
				query: requestBody.toJSON(),
				'statusFilter': this.statusFilter
			});
		}
		return;
    }

    private onSuccess(data, headers) {
        const buttonList: GenericProfileCardButton[] = [];
        this.loader = false;
        this.loaderInfinite = false;
        this.totalItems = data['hits'] ? data['hits'].total : 0;
        this.links.last = Math.ceil(this.totalItems / 20);
        if (!this.page) {
            this.loaderService.display(false);
			this.dataCategories = [];
		}
		if (data['hits'] && data['hits'].hits) {
			for (let i = 0; i < data['hits'].hits.length; i++) {
                this.dataCategories.push(data['hits'].hits[i]._source);
            }
            /*this.dataCategories.forEach((item, index) => {
                let buttonArray = [];
                if (item.followed) {
                    buttonArray = [{
                        label: 'Unfollow',
                        clickAction: 'followAction',
                        btnClass: 'cat-request-btn',
                        icon: 'fa fa-minus-circle',
                        gaLabel: 'Data Categories - Category - Unfollow',
                        gaCategory: 'Category Follow',
                        gaEvent: 'Unfollowed',
                        tooltip: 'Unfollow Category Updates'
                    }];
                } else {
                    buttonArray = [{
                        label: 'Follow',
                        clickAction: 'followAction',
                        btnClass: 'cat-btn-lock',
                        icon: 'fa fa-plus-circle',
                        gaLabel: 'Data Categories - Category - Follow',
                        gaCategory: 'Category Follow',
                        gaEvent: 'Followed',
                        tooltip: 'Follow Category Updates'
                    }];
                }
                this.genericProfileCard.push({
                    id: item.id,
                    content:[
                        {
                       columns:1,
                       columnSize: [2],
                       columnContent:[{
                            image: item.locked ? null : (item.logo && item.logo.logo) ? this._DomSanitizer.bypassSecurityTrustUrl('data:image/png;base64,' + item.logo.logo) : null,
                            url: '/#/providers?filter1=categories.id&value1='+item.id+'&type1=checkbox'
                       }]
                   }, 
                   {
                       columns: 4,
                       columnSize: [2, 6, 2, 2],
                       columnContent: [{
                           image: !item.locked && (item.logo && item.logo.logo) ? this._DomSanitizer.bypassSecurityTrustUrl('data:image/png;base64,' + item.logo.logo) : null,
                           url: '/#/providers?filter1=categories.id&value1='+item.id+'&type1=checkbox'
                       },{
                           title: item.datacategory,
                           url: !item.locked ? '/#/providers?filter1=categories.id&value1='+item.id+'&type1=checkbox' : '',
                           toolTip: !item.locked ? item.datacategory : 'Your account does not have permission to view this.',
                           style: item.locked ? 'blurText' : ''
                       },{
                           label: item.relevantDataCategoryProviderCount ? 'Providers' : '',
                           text: item.relevantDataCategoryProviderCount ? item.relevantDataCategoryProviderCount : '',
                           toolTip: item.relevantDataCategoryProviderCount ? 'Number of providers with this related data category' : ''
                       },{
                           label: item.mainDataCategoryProviderCount ? 'Main Providers' : '',
                           plainText: item.mainDataCategoryProviderCount ? item.mainDataCategoryProviderCount : '',
                           toolTip: item.mainDataCategoryProviderCount ? 'Number of providers with this as their main data category' : ''
                       }],
                    },{
                        columns: 3,
                        columnSize: [2, 8, 2],
                        columnContent:[{},{
                            plainText: item.explanation ? item.explanation : '',
                            style: item.locked ? 'blurText' : '',
                            toolTip: !item.locked ? item.explanation : 'Your account does not have permission to view this.'
                        },{
                            buttons: !item.locked ? buttonArray : []
                        }]
                    },{
                        columns:3,
                        columnSize: [2, 3, 3],
                        columnContent:[{
                            label: item.createdDate ? 'Added:' : '',
                            text: item.createdDate ? this.datePipe.transform(item.createdDate, 'mediumDate', 'EST') : '',
                            toolTip: item.createdDate ? 'Date this data category was created' : ''
                        },{
                            label: item.newRelevantProviderDate ? 'Provider Last Added:' : '',
                            text: item.newRelevantProviderDate ? this.datePipe.transform(item.newRelevantProviderDate, 'mediumDate', 'EST') : '',
                            toolTip: 'Date a provider was most recently added to this data category'
                        },{
                            label: item.newMainProviderDate ? 'Main Provider Last Added:' :'',
                            text: item.newMainProviderDate ? this.datePipe.transform(item.newMainProviderDate, 'mediumDate', 'EST') : '',
                            toolTip: item.newMainProviderDate ?'Date a main provider was most recently added to this data category' : ''
                        }]
                    }]
                })
            });*/
        }
    }

    private onError(error) {
        this.loader = false;
        this.loaderInfinite = false;
        this.alertService.error(error.message, null, null);
    }

    mouseEnter(p) { // This event used for open and close the popup while mouseover the locked providers
        setTimeout(() => {
            p.close();
        }, 5000);
    }

    upgrade() {
        this.router.navigate(['/upgrade']);
    }

    /*onButtonClick(event, id, index) {
        console.log(event);
        if (event && event.clickAction) {
            this[event.clickAction](id,event);
            if (event.clickAction === 'followAction') {
                this.genericProfileCard[index].content[1].columnContent[2].buttons[0] = {
                    label: 'Unfollow',
                    clickAction: 'followAction',
                    btnClass: 'cat-request-btn',
                    icon: 'fa fa-minus-circle',
                    gaLabel: 'Data Categories - Category - Unfollow',
                    gaCategory: 'Category Follow',
                    gaEvent: 'Unfollowed',
                    tooltip: 'Unfollow Category Updates'
                }
            }

            if (event.clickAction === 'followAction') {
                this.genericProfileCard[index].content[1].columnContent[2].buttons[0] = {
                    label: 'Follow',
                    clickAction: 'followAction',
                    btnClass: 'cat-btn-lock',
                    icon: 'fa fa-plus-circle',
                    gaLabel: 'Data Categories - Category - Follow',
                    gaCategory: 'Category Follow',
                    gaEvent: 'Followed',
                    tooltip: 'Follow Category Updates'
                }
            }
        }
    }*/

    followAction(categoryID: number) {
        this.dataCategoryService.followAction(categoryID).subscribe();
        //this.googleAnalyticsService.emitEvent(event.gaCategory, event.gaEvent, event.gaLabel);
	}
    onExpireSession(event) {
		//console.log('eventcheck',event);
        if (event) {
			//console.log('eventcheck',event);
            //this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
				setTimeout(() => {
				this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
			}, 2000);
			}); 
        }
    }
	  beforeSessionExpires(event) {
        if (event) {
            //this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }

}
