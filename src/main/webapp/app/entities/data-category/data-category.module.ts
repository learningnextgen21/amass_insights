import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    DataCategoryService,
    DataCategoryPopupService,
    DataCategoryComponent,
    DataCategoryDetailComponent,
    DataCategoryDialogComponent,
    DataCategoryPopupComponent,
    DataCategoryDeletePopupComponent,
    DataCategoryDeleteDialogComponent,
    dataCategoryRoute,
    dataCategoryPopupRoute,
} from './';
import { GenericProfileCardModule } from 'app/shared/generic-profile-card/generic-profile-card.module';

const ENTITY_STATES = [
    ...dataCategoryRoute,
    ...dataCategoryPopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        GenericProfileCardModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DataCategoryComponent,
        DataCategoryDetailComponent,
        DataCategoryDialogComponent,
        DataCategoryDeleteDialogComponent,
        DataCategoryPopupComponent,
        DataCategoryDeletePopupComponent,
    ],
    entryComponents: [
        DataCategoryComponent,
        DataCategoryDialogComponent,
        DataCategoryPopupComponent,
        DataCategoryDeleteDialogComponent,
        DataCategoryDeletePopupComponent,
    ],
    providers: [
        DataCategoryService,
        DataCategoryPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceDataCategoryModule {}
