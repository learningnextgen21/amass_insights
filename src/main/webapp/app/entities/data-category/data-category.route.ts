import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DataCategoryComponent } from './data-category.component';
import { DataCategoryDetailComponent } from './data-category-detail.component';
import { DataCategoryPopupComponent } from './data-category-dialog.component';
import { DataCategoryDeletePopupComponent } from './data-category-delete-dialog.component';

export const dataCategoryRoute: Routes = [
    {
        path: 'categories',
        component: DataCategoryComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataCategory.home.title',
            breadcrumb: 'Categories'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'data-category/:id',
        component: DataCategoryDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.dataCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dataCategoryPopupRoute: Routes = [
    {
        path: 'data-category-new',
        component: DataCategoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-category/:id/edit',
        component: DataCategoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-category/:id/delete',
        component: DataCategoryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.dataCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
