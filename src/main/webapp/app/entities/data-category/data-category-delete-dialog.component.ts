import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DataCategory } from './data-category.model';
import { DataCategoryPopupService } from './data-category-popup.service';
import { DataCategoryService } from './data-category.service';

@Component({
    selector: 'jhi-data-category-delete-dialog',
    templateUrl: './data-category-delete-dialog.component.html'
})
export class DataCategoryDeleteDialogComponent {

    dataCategory: DataCategory;

    constructor(
        private dataCategoryService: DataCategoryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dataCategoryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dataCategoryListModification',
                content: 'Deleted an dataCategory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-data-category-delete-popup',
    template: ''
})
export class DataCategoryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataCategoryPopupService: DataCategoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dataCategoryPopupService
                .open(DataCategoryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
