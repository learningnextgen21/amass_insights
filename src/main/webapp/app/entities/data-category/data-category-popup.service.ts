import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DataCategory } from './data-category.model';
import { DataCategoryService } from './data-category.service';

@Injectable()
export class DataCategoryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private dataCategoryService: DataCategoryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dataCategoryService.find(id).subscribe((dataCategory) => {
                    if (dataCategory.createdDate) {
                        dataCategory.createdDate = {
                            year: dataCategory.createdDate.getFullYear(),
                            month: dataCategory.createdDate.getMonth() + 1,
                            day: dataCategory.createdDate.getDate()
                        };
                    }
                    dataCategory.updatedDate = this.datePipe
                        .transform(dataCategory.updatedDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.dataCategoryModalRef(component, dataCategory);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dataCategoryModalRef(component, new DataCategory());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dataCategoryModalRef(component: Component, dataCategory: DataCategory): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dataCategory = dataCategory;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
