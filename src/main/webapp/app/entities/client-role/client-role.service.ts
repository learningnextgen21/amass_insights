

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ClientRole } from './client-role.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ClientRoleService {

    private resourceUrl = 'api/client-roles';
    private resourceSearchUrl = 'api/_search/client-roles';

    constructor(private http: HttpClient) { }

    create(clientRole: ClientRole): Observable<ClientRole> {
        const copy = this.convert(clientRole);
        return this.http.post(this.resourceUrl, copy);
    }

    update(clientRole: ClientRole): Observable<ClientRole> {
        const copy = this.convert(clientRole);
        return this.http.put(this.resourceUrl, copy);
    }

    find(id: number): Observable<ClientRole> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, {params: options});
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, {params: options});
    }

    private convert(clientRole: ClientRole): ClientRole {
        const copy: ClientRole = Object.assign({}, clientRole);
        return copy;
    }
}
