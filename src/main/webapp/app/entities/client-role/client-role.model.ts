import { BaseEntity } from './../../shared';

export class ClientRole implements BaseEntity {
    constructor(
        public id?: number,
        public investorType?: number,
        public assets?: number,
        public assetClasses?: string,
        public equities?: string,
        public description?: any,
        public paid?: string,
        public approvalStatus?: number,
        public subscriptionType?: number,
    ) {
    }
}
