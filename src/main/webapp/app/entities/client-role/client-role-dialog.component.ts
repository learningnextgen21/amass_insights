import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { ClientRole } from './client-role.model';
import { ClientRolePopupService } from './client-role-popup.service';
import { ClientRoleService } from './client-role.service';

@Component({
    selector: 'jhi-client-role-dialog',
    templateUrl: './client-role-dialog.component.html'
})
export class ClientRoleDialogComponent implements OnInit {

    clientRole: ClientRole;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private alertService: JhiAlertService,
        private clientRoleService: ClientRoleService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clientRole.id !== undefined) {
            this.subscribeToSaveResponse(
                this.clientRoleService.update(this.clientRole));
        } else {
            this.subscribeToSaveResponse(
                this.clientRoleService.create(this.clientRole));
        }
    }

    private subscribeToSaveResponse(result: Observable<ClientRole>) {
        result.subscribe((res: ClientRole) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ClientRole) {
        this.eventManager.broadcast({ name: 'clientRoleListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-client-role-popup',
    template: ''
})
export class ClientRolePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientRolePopupService: ClientRolePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.clientRolePopupService
                    .open(ClientRoleDialogComponent as Component, params['id']);
            } else {
                this.clientRolePopupService
                    .open(ClientRoleDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
