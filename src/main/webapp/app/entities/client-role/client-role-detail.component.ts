import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { ClientRole } from './client-role.model';
import { ClientRoleService } from './client-role.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-client-role-detail',
    templateUrl: './client-role-detail.component.html'
})
export class ClientRoleDetailComponent implements OnInit, OnDestroy {

    clientRole: ClientRole;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private clientRoleService: ClientRoleService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientRoles();
    }

    load(id) {
        this.clientRoleService.find(id).subscribe((clientRole) => {
            this.clientRole = clientRole;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientRoles() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientRoleListModification',
            (response) => this.load(this.clientRole.id)
        );
    }
}
