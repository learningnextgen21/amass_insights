export * from './client-role.model';
export * from './client-role-popup.service';
export * from './client-role.service';
export * from './client-role-dialog.component';
export * from './client-role-delete-dialog.component';
export * from './client-role-detail.component';
export * from './client-role.component';
export * from './client-role.route';
