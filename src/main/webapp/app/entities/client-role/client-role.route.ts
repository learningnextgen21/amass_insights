import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ClientRoleComponent } from './client-role.component';
import { ClientRoleDetailComponent } from './client-role-detail.component';
import { ClientRolePopupComponent } from './client-role-dialog.component';
import { ClientRoleDeletePopupComponent } from './client-role-delete-dialog.component';

export const clientRoleRoute: Routes = [
    {
        path: 'client-role',
        component: ClientRoleComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.clientRole.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-role/:id',
        component: ClientRoleDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.clientRole.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientRolePopupRoute: Routes = [
    {
        path: 'client-role-new',
        component: ClientRolePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.clientRole.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-role/:id/edit',
        component: ClientRolePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.clientRole.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-role/:id/delete',
        component: ClientRoleDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.clientRole.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
