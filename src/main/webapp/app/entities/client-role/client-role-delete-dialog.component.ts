import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ClientRole } from './client-role.model';
import { ClientRolePopupService } from './client-role-popup.service';
import { ClientRoleService } from './client-role.service';

@Component({
    selector: 'jhi-client-role-delete-dialog',
    templateUrl: './client-role-delete-dialog.component.html'
})
export class ClientRoleDeleteDialogComponent {

    clientRole: ClientRole;

    constructor(
        private clientRoleService: ClientRoleService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.clientRoleService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'clientRoleListModification',
                content: 'Deleted an clientRole'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-client-role-delete-popup',
    template: ''
})
export class ClientRoleDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private clientRolePopupService: ClientRolePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.clientRolePopupService
                .open(ClientRoleDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
