import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    ClientRoleService,
    ClientRolePopupService,
    ClientRoleComponent,
    ClientRoleDetailComponent,
    ClientRoleDialogComponent,
    ClientRolePopupComponent,
    ClientRoleDeletePopupComponent,
    ClientRoleDeleteDialogComponent,
    clientRoleRoute,
    clientRolePopupRoute,
} from './';

const ENTITY_STATES = [
    ...clientRoleRoute,
    ...clientRolePopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ClientRoleComponent,
        ClientRoleDetailComponent,
        ClientRoleDialogComponent,
        ClientRoleDeleteDialogComponent,
        ClientRolePopupComponent,
        ClientRoleDeletePopupComponent,
    ],
    entryComponents: [
        ClientRoleComponent,
        ClientRoleDialogComponent,
        ClientRolePopupComponent,
        ClientRoleDeleteDialogComponent,
        ClientRoleDeletePopupComponent,
    ],
    providers: [
        ClientRoleService,
        ClientRolePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceClientRoleModule {}
