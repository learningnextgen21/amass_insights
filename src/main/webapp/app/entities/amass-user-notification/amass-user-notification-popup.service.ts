import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AmassUserNotification } from './amass-user-notification.model';
import { AmassUserNotificationService } from './amass-user-notification.service';

@Injectable()
export class AmassUserNotificationPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private amassUserNotificationService: AmassUserNotificationService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.amassUserNotificationService.find(id).subscribe((amassUserNotification) => {
                    if (amassUserNotification.createdDate) {
                        amassUserNotification.createdDate = {
                            year: amassUserNotification.createdDate.getFullYear(),
                            month: amassUserNotification.createdDate.getMonth() + 1,
                            day: amassUserNotification.createdDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.amassUserNotificationModalRef(component, amassUserNotification);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.amassUserNotificationModalRef(component, new AmassUserNotification());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    amassUserNotificationModalRef(component: Component, amassUserNotification: AmassUserNotification): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.amassUserNotification = amassUserNotification;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
