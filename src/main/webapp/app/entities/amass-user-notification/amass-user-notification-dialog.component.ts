import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AmassUserNotification } from './amass-user-notification.model';
import { AmassUserNotificationPopupService } from './amass-user-notification-popup.service';
import { AmassUserNotificationService } from './amass-user-notification.service';

@Component({
    selector: 'jhi-amass-user-notification-dialog',
    templateUrl: './amass-user-notification-dialog.component.html'
})
export class AmassUserNotificationDialogComponent implements OnInit {

    amassUserNotification: AmassUserNotification;
    isSaving: boolean;
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private amassUserNotificationService: AmassUserNotificationService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.amassUserNotification.id !== undefined) {
            this.subscribeToSaveResponse(
                this.amassUserNotificationService.update(this.amassUserNotification));
        } else {
            this.subscribeToSaveResponse(
                this.amassUserNotificationService.create(this.amassUserNotification));
        }
    }

    private subscribeToSaveResponse(result: Observable<AmassUserNotification>) {
        result.subscribe((res: AmassUserNotification) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: AmassUserNotification) {
        this.eventManager.broadcast({ name: 'amassUserNotificationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-amass-user-notification-popup',
    template: ''
})
export class AmassUserNotificationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private amassUserNotificationPopupService: AmassUserNotificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.amassUserNotificationPopupService
                    .open(AmassUserNotificationDialogComponent as Component, params['id']);
            } else {
                this.amassUserNotificationPopupService
                    .open(AmassUserNotificationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
