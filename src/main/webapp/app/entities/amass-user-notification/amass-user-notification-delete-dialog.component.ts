import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AmassUserNotification } from './amass-user-notification.model';
import { AmassUserNotificationPopupService } from './amass-user-notification-popup.service';
import { AmassUserNotificationService } from './amass-user-notification.service';

@Component({
    selector: 'jhi-amass-user-notification-delete-dialog',
    templateUrl: './amass-user-notification-delete-dialog.component.html'
})
export class AmassUserNotificationDeleteDialogComponent {

    amassUserNotification: AmassUserNotification;

    constructor(
        private amassUserNotificationService: AmassUserNotificationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.amassUserNotificationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'amassUserNotificationListModification',
                content: 'Deleted an amassUserNotification'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-amass-user-notification-delete-popup',
    template: ''
})
export class AmassUserNotificationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private amassUserNotificationPopupService: AmassUserNotificationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.amassUserNotificationPopupService
                .open(AmassUserNotificationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
