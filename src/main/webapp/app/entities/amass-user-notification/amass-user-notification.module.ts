import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    AmassUserNotificationService,
    AmassUserNotificationPopupService,
    AmassUserNotificationComponent,
    AmassUserNotificationDetailComponent,
    AmassUserNotificationDialogComponent,
    AmassUserNotificationPopupComponent,
    AmassUserNotificationDeletePopupComponent,
    AmassUserNotificationDeleteDialogComponent,
    amassUserNotificationRoute,
    amassUserNotificationPopupRoute,
} from './';

const ENTITY_STATES = [
    ...amassUserNotificationRoute,
    ...amassUserNotificationPopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AmassUserNotificationComponent,
        AmassUserNotificationDetailComponent,
        AmassUserNotificationDialogComponent,
        AmassUserNotificationDeleteDialogComponent,
        AmassUserNotificationPopupComponent,
        AmassUserNotificationDeletePopupComponent,
    ],
    entryComponents: [
        AmassUserNotificationComponent,
        AmassUserNotificationDialogComponent,
        AmassUserNotificationPopupComponent,
        AmassUserNotificationDeleteDialogComponent,
        AmassUserNotificationDeletePopupComponent,
    ],
    providers: [
        AmassUserNotificationService,
        AmassUserNotificationPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceAmassUserNotificationModule {}
