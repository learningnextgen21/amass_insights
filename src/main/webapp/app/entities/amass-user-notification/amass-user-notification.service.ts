
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { AmassUserNotification } from './amass-user-notification.model';
import { createRequestOption } from '../../shared';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AmassUserNotificationService {

    private resourceUrl = 'api/amass-user-notifications';
    private resourceSearchUrl = 'api/_search/amass-user-notifications';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(amassUserNotification: AmassUserNotification): Observable<AmassUserNotification> {
        const copy = this.convert(amassUserNotification);
        return this.http.post(this.resourceUrl, copy);
    }

    update(amassUserNotification: AmassUserNotification): Observable<AmassUserNotification> {
        const copy = this.convert(amassUserNotification);
        return this.http.put(this.resourceUrl, copy);
    }

    find(id: number): Observable<AmassUserNotification> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, {params: options});
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, {params: options});
    }

    private convert(amassUserNotification: AmassUserNotification): AmassUserNotification {
        const copy: AmassUserNotification = Object.assign({}, amassUserNotification);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(amassUserNotification.createdDate);
        return copy;
    }
}
