import { BaseEntity } from './../../shared';

export class AmassUserNotification implements BaseEntity {
    constructor(
        public id?: number,
        public notificationID?: number,
        public userID?: number,
        public userProviderID?: number,
        public userDataCategoryID?: number,
        public userProductID?: number,
        public userCommentID?: number,
        public createdDate?: any,
        public status?: string,
        public notificationType?: string,
    ) {
    }
}
