import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AmassUserNotificationComponent } from './amass-user-notification.component';
import { AmassUserNotificationDetailComponent } from './amass-user-notification-detail.component';
import { AmassUserNotificationPopupComponent } from './amass-user-notification-dialog.component';
import { AmassUserNotificationDeletePopupComponent } from './amass-user-notification-delete-dialog.component';

export const amassUserNotificationRoute: Routes = [
    {
        path: 'amass-user-notification',
        component: AmassUserNotificationComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.amassUserNotification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'amass-user-notification/:id',
        component: AmassUserNotificationDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.amassUserNotification.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const amassUserNotificationPopupRoute: Routes = [
    {
        path: 'amass-user-notification-new',
        component: AmassUserNotificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.amassUserNotification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'amass-user-notification/:id/edit',
        component: AmassUserNotificationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.amassUserNotification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'amass-user-notification/:id/delete',
        component: AmassUserNotificationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.amassUserNotification.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
