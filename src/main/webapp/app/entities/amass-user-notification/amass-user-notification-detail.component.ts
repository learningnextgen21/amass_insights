import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AmassUserNotification } from './amass-user-notification.model';
import { AmassUserNotificationService } from './amass-user-notification.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-amass-user-notification-detail',
    templateUrl: './amass-user-notification-detail.component.html'
})
export class AmassUserNotificationDetailComponent implements OnInit, OnDestroy {

    amassUserNotification: AmassUserNotification;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private amassUserNotificationService: AmassUserNotificationService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAmassUserNotifications();
    }

    load(id) {
        this.amassUserNotificationService.find(id).subscribe((amassUserNotification) => {
            this.amassUserNotification = amassUserNotification;
        });
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAmassUserNotifications() {
        this.eventSubscriber = this.eventManager.subscribe(
            'amassUserNotificationListModification',
            (response) => this.load(this.amassUserNotification.id)
        );
    }
}
