import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { DatamarketplaceDataArticleModule } from './data-article/data-article.module';
import { DatamarketplaceDataProviderTagModule } from './data-provider-tag/data-provider-tag.module';
import { DatamarketplaceDataProviderTypeModule } from './data-provider-type/data-provider-type.module';
import { DatamarketplaceDataProviderModule } from './data-provider/data-provider.module';
import { DatamarketplaceDataOrganizationModule } from './data-organization/data-organization.module';
import { DatamarketplaceDataCategoryModule } from './data-category/data-category.module';
import { DatamarketplaceDataFeatureModule } from './data-feature/data-feature.module';
import { DatamarketplaceDataIndustryModule } from './data-industry/data-industry.module';
import { DatamarketplaceDataSourceModule } from './data-source/data-source.module';
import { DatamarketplaceAdminRoleModule } from './admin-role/admin-role.module';
import { DatamarketplaceClientRoleModule } from './client-role/client-role.module';
import { DatamarketplaceLookupCodeModule } from './lookup-code/lookup-code.module';
import { DatamarketplaceAmassUserModule } from './amass-user/amass-user.module';
import { DatamarketplaceAmassUserDataCategoryModule } from './amass-user-data-category/amass-user-data-category.module';
import { DatamarketplaceAmassUserNotificationModule } from './amass-user-notification/amass-user-notification.module';
import { DatamarketplaceAmassUserProviderModule } from './amass-user-provider/amass-user-provider.module';
import { DatamarketplaceDataEventsModule } from './data-events/data-events.module';

@NgModule({
    imports: [
        DatamarketplaceDataEventsModule,
        DatamarketplaceDataArticleModule,
        DatamarketplaceDataCategoryModule,
        DatamarketplaceDataProviderTagModule,
        DatamarketplaceDataProviderTypeModule,
        DatamarketplaceDataOrganizationModule,
        DatamarketplaceDataFeatureModule,
        DatamarketplaceDataIndustryModule,
        DatamarketplaceDataSourceModule,
        DatamarketplaceAdminRoleModule,
        DatamarketplaceClientRoleModule,
        DatamarketplaceLookupCodeModule,
        DatamarketplaceAmassUserModule,
        DatamarketplaceAmassUserDataCategoryModule,
        DatamarketplaceAmassUserNotificationModule,
        DatamarketplaceAmassUserProviderModule,
        DatamarketplaceDataProviderModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceEntityModule {}
