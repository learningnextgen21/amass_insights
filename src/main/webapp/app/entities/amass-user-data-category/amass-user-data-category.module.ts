import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../../shared';
import {
    AmassUserDataCategoryService,
    AmassUserDataCategoryPopupService,
    AmassUserDataCategoryComponent,
    AmassUserDataCategoryDetailComponent,
    AmassUserDataCategoryDialogComponent,
    AmassUserDataCategoryPopupComponent,
    AmassUserDataCategoryDeletePopupComponent,
    AmassUserDataCategoryDeleteDialogComponent,
    amassUserDataCategoryRoute,
    amassUserDataCategoryPopupRoute,
} from './';

const ENTITY_STATES = [
    ...amassUserDataCategoryRoute,
    ...amassUserDataCategoryPopupRoute,
];

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AmassUserDataCategoryComponent,
        AmassUserDataCategoryDetailComponent,
        AmassUserDataCategoryDialogComponent,
        AmassUserDataCategoryDeleteDialogComponent,
        AmassUserDataCategoryPopupComponent,
        AmassUserDataCategoryDeletePopupComponent,
    ],
    entryComponents: [
        AmassUserDataCategoryComponent,
        AmassUserDataCategoryDialogComponent,
        AmassUserDataCategoryPopupComponent,
        AmassUserDataCategoryDeleteDialogComponent,
        AmassUserDataCategoryDeletePopupComponent,
    ],
    providers: [
        AmassUserDataCategoryService,
        AmassUserDataCategoryPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceAmassUserDataCategoryModule {}
