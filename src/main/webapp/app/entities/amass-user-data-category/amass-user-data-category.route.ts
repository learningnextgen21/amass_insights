import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AmassUserDataCategoryComponent } from './amass-user-data-category.component';
import { AmassUserDataCategoryDetailComponent } from './amass-user-data-category-detail.component';
import { AmassUserDataCategoryPopupComponent } from './amass-user-data-category-dialog.component';
import { AmassUserDataCategoryDeletePopupComponent } from './amass-user-data-category-delete-dialog.component';

export const amassUserDataCategoryRoute: Routes = [
    {
        path: 'amass-user-data-category',
        component: AmassUserDataCategoryComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.amassUserDataCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'amass-user-data-category/:id',
        component: AmassUserDataCategoryDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'datamarketplaceApp.amassUserDataCategory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const amassUserDataCategoryPopupRoute: Routes = [
    {
        path: 'amass-user-data-category-new',
        component: AmassUserDataCategoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.amassUserDataCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'amass-user-data-category/:id/edit',
        component: AmassUserDataCategoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.amassUserDataCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'amass-user-data-category/:id/delete',
        component: AmassUserDataCategoryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'datamarketplaceApp.amassUserDataCategory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
