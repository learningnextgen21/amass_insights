import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AmassUserDataCategory } from './amass-user-data-category.model';
import { AmassUserDataCategoryPopupService } from './amass-user-data-category-popup.service';
import { AmassUserDataCategoryService } from './amass-user-data-category.service';

@Component({
    selector: 'jhi-amass-user-data-category-dialog',
    templateUrl: './amass-user-data-category-dialog.component.html'
})
export class AmassUserDataCategoryDialogComponent implements OnInit {

    amassUserDataCategory: AmassUserDataCategory;
    isSaving: boolean;
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private amassUserDataCategoryService: AmassUserDataCategoryService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.amassUserDataCategory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.amassUserDataCategoryService.update(this.amassUserDataCategory));
        } else {
            this.subscribeToSaveResponse(
                this.amassUserDataCategoryService.create(this.amassUserDataCategory));
        }
    }

    private subscribeToSaveResponse(result: Observable<AmassUserDataCategory>) {
        result.subscribe((res: AmassUserDataCategory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: AmassUserDataCategory) {
        this.eventManager.broadcast({ name: 'amassUserDataCategoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error;
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-amass-user-data-category-popup',
    template: ''
})
export class AmassUserDataCategoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private amassUserDataCategoryPopupService: AmassUserDataCategoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.amassUserDataCategoryPopupService
                    .open(AmassUserDataCategoryDialogComponent as Component, params['id']);
            } else {
                this.amassUserDataCategoryPopupService
                    .open(AmassUserDataCategoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
