import { BaseEntity } from './../../shared';

export class AmassUserDataCategory implements BaseEntity {
    constructor(
        public id?: number,
        public userID?: number,
        public dataCategoryID?: number,
        public getNotifications?: number,
        public notificationType?: string,
        public interestType?: string,
        public inactive?: number,
        public createdDate?: any,
        public updatedDate?: any,
    ) {
    }
}
