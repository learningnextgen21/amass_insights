
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { AmassUserDataCategory } from './amass-user-data-category.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AmassUserDataCategoryService {

    private resourceUrl = 'api/amass-user-data-categories';
    private resourceSearchUrl = 'api/_search/amass-user-data-categories';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(amassUserDataCategory: AmassUserDataCategory): Observable<AmassUserDataCategory> {
        const copy = this.convert(amassUserDataCategory);
        return this.http.post(this.resourceUrl, copy);
    }

    update(amassUserDataCategory: AmassUserDataCategory): Observable<AmassUserDataCategory> {
        const copy = this.convert(amassUserDataCategory);
        return this.http.put(this.resourceUrl, copy);
    }

    find(id: number): Observable<AmassUserDataCategory> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, {params: options});
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<any> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, {params: options});
    }

    private convert(amassUserDataCategory: AmassUserDataCategory): AmassUserDataCategory {
        const copy: AmassUserDataCategory = Object.assign({}, amassUserDataCategory);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(amassUserDataCategory.createdDate);

        copy.updatedDate = this.dateUtils.toDate(amassUserDataCategory.updatedDate);
        return copy;
    }
}
