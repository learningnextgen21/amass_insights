import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AmassUserDataCategory } from './amass-user-data-category.model';
import { AmassUserDataCategoryPopupService } from './amass-user-data-category-popup.service';
import { AmassUserDataCategoryService } from './amass-user-data-category.service';

@Component({
    selector: 'jhi-amass-user-data-category-delete-dialog',
    templateUrl: './amass-user-data-category-delete-dialog.component.html'
})
export class AmassUserDataCategoryDeleteDialogComponent {

    amassUserDataCategory: AmassUserDataCategory;

    constructor(
        private amassUserDataCategoryService: AmassUserDataCategoryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.amassUserDataCategoryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'amassUserDataCategoryListModification',
                content: 'Deleted an amassUserDataCategory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-amass-user-data-category-delete-popup',
    template: ''
})
export class AmassUserDataCategoryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private amassUserDataCategoryPopupService: AmassUserDataCategoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.amassUserDataCategoryPopupService
                .open(AmassUserDataCategoryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
