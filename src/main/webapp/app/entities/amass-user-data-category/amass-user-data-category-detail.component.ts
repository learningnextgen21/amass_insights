import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AmassUserDataCategory } from './amass-user-data-category.model';
import { AmassUserDataCategoryService } from './amass-user-data-category.service';
import {WINDOW} from '../../layouts';

@Component({
    selector: 'jhi-amass-user-data-category-detail',
    templateUrl: './amass-user-data-category-detail.component.html'
})
export class AmassUserDataCategoryDetailComponent implements OnInit, OnDestroy {

    amassUserDataCategory: AmassUserDataCategory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private amassUserDataCategoryService: AmassUserDataCategoryService,
        private route: ActivatedRoute,
        @Inject(WINDOW) private window: Window,
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAmassUserDataCategories();
    }

    load(id) {
        this.amassUserDataCategoryService.find(id).subscribe((amassUserDataCategory) => {
            this.amassUserDataCategory = amassUserDataCategory;
        });
    }
    previousState() {
        this.window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAmassUserDataCategories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'amassUserDataCategoryListModification',
            (response) => this.load(this.amassUserDataCategory.id)
        );
    }
}
