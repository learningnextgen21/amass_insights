import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { AmassUserDataCategory } from './amass-user-data-category.model';
import { AmassUserDataCategoryService } from './amass-user-data-category.service';

@Injectable()
export class AmassUserDataCategoryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private amassUserDataCategoryService: AmassUserDataCategoryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.amassUserDataCategoryService.find(id).subscribe((amassUserDataCategory) => {
                    if (amassUserDataCategory.createdDate) {
                        amassUserDataCategory.createdDate = {
                            year: amassUserDataCategory.createdDate.getFullYear(),
                            month: amassUserDataCategory.createdDate.getMonth() + 1,
                            day: amassUserDataCategory.createdDate.getDate()
                        };
                    }
                    amassUserDataCategory.updatedDate = this.datePipe
                        .transform(amassUserDataCategory.updatedDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.amassUserDataCategoryModalRef(component, amassUserDataCategory);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.amassUserDataCategoryModalRef(component, new AmassUserDataCategory());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    amassUserDataCategoryModalRef(component: Component, amassUserDataCategory: AmassUserDataCategory): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.amassUserDataCategory = amassUserDataCategory;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
