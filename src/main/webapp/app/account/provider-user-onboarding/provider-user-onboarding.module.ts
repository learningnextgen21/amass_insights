import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormlyModule } from '@ngx-formly/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';

import {
    DatamarketplaceSharedModule,
    FormlyEditorFieldComponent,
    FormlyAutoCompleteFieldComponent,
    FormlyMultiSelectFieldComponent,
    FormlyDropdownFieldComponent,
    FormlyMaskFieldComponent,
    FormlySliderFieldComponent,
    FormlyRadioButtonFieldComponent,
    FormlyTextAreaFieldComponent,
    FormlyDatePickerFieldComponent,
    FormlyFileUploadFieldComponent
} from '../../shared';
import { FormlyHorizontalWrapperComponent } from 'app/entities/data-provider/horizontal-wrapper';
import { ProviderUserOnboardingComponent } from './provider-user-onboarding-form.component';
import { providerUserOnboardingRoute } from './provider-user-onboarding.route';
import { ProviderUserOnboardingService } from './provider-user-onboarding.service';
import { FormlyFieldCheckboxComponent } from 'app/shared/custom-form-fields/checkbox-form-field.component';

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        // HttpClientModule,
        ReactiveFormsModule,
        RouterModule.forRoot([providerUserOnboardingRoute], { useHash: true }),
        FormlyModule.forRoot({
            wrappers: [{ name: 'form-field-horizontal', component: FormlyHorizontalWrapperComponent  }],
            validationMessages: [
                { name: 'required', message: 'This field is required' },
            ],
            types: [{
                name: 'inputEditor', component: FormlyEditorFieldComponent
            }, {
                name: 'autocomplete', component: FormlyAutoCompleteFieldComponent
            }, {
                name: 'multiselect', component: FormlyMultiSelectFieldComponent
            }, {
                name: 'dropdown', component: FormlyDropdownFieldComponent
            }, {
                name: 'mask', component: FormlyMaskFieldComponent
            }, {
                name: 'slider', component: FormlySliderFieldComponent
            }, {
                name: 'radioButton', component: FormlyRadioButtonFieldComponent
            }, {
                name: 'textarea', component: FormlyTextAreaFieldComponent
            }, {
                name: 'datepicker', component: FormlyDatePickerFieldComponent
            }, {
                name: 'fileUpload', component: FormlyFileUploadFieldComponent
            }, {
                name: 'checkbox', component: FormlyFieldCheckboxComponent
            }]
        }),
        FormlyBootstrapModule,
    ],
    declarations: [
        ProviderUserOnboardingComponent
    ],
    entryComponents: [

    ],
    providers: [
        ProviderUserOnboardingService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProviderUserOnbardingFormModule {}
