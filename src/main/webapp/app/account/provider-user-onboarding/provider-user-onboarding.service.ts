import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { ResponseWrapper, createRequestOption } from 'app/shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class ProviderUserOnboardingService {
    private providerUserOnboardingUrl = 'api/providerOnboarding';
    public relevantProviders: any;
    public  resourcesObj: any;
    private resourceElasticSearchUrl = 'api/es/organizationsearch';
    private resourceAdminUrl = 'api/resources';
    private updateAllResourceAdminUrl = 'api/resourcesList';
    private userOnboardingFileUploadUrl = '/api/ur/user-resource-file-upload';

	constructor(private http: HttpClient) { }

    findByIdForOrganizationForm(recordID: string): Observable<any> {
		const req = {
			'_source': {
				'exclude': [

				],
				'include': [
                    'name',
                    'file',
                    'link',
                    'userPermission',
                    'workflows',
                    'marketplaceStatus',
                    'researchMethodsCompleted',
                    'ieiStatus',
                    'priority',
                    'topics',
                    'purposes',
                    'purposeTypes',
                    'relavantOrganizations',
                    'relavantDataProviders'
				]
			},
			'filter': {
				'query': {
					'match': {
						'recordID': recordID
					}
				}
			}
		};
		return this.http.post(this.resourceElasticSearchUrl + '?detail=true', req);
	}

    addResourceAdmin(request?: any): Observable<any> {
        const model = request;
        //console.log('ID 1', request.ieiStatus.id);
        if(request && request.ieiStatus && request.ieiStatus.id === null) {
            model.ieiStatus = request.ieiStatus.id;
            //console.log('ID', request.ieiStatus.id);
        }
        if(request && request.priority && request.priority.id === null) {
            model.priority = request.priority.id;
        }
        return this.http.post(this.resourceAdminUrl, model);
    }

    providerUserOnboarding(request?: any, check?:string): Observable<any> {
        const model = request;
        console.log(model);
        return this.http.put(this.providerUserOnboardingUrl, model,{
            params: {
                checkBox: check
            }
        });
    }

    // For the user who don't have the operator permission.
    userOnboardingFileUpload(id? : any, fileUploads?: any): Observable<any> {
        console.log('hello');
        const formData = new FormData();
        formData.append('fileUploads', fileUploads);
        return this.http.post(this.userOnboardingFileUploadUrl, formData, {
            params: {
                userId: id
            }
        });
    }

    checkListProviderUserOnboarding(request?: any): Observable<any> {
        const model = request;
        console.log(model);
        return this.http.put(this.providerUserOnboardingUrl, model);
    }

    updateResourceAdmin(request?: any, action?: string): Observable<any> {
        const model = request;
        if(request && request.ieiStatus && request.ieiStatus.id === null) {
            model.ieiStatus = request.ieiStatus.id;
            //console.log('ID', request.ieiStatus.id);
        }
        if(request && request.priority && request.priority.id === null) {
            model.priority = request.priority.id;
        }
        return this.http.put(this.resourceAdminUrl, model);
    }

    createResourceLink(request?: any): Observable<any> {
        const model = request;
        return this.http.post(this.resourceAdminUrl, model);
    }

    updateResourceLink(request?: any): Observable<any> {
        const model = request;
        return this.http.put(this.resourceAdminUrl, model);
    }

    deleteResourceLink(request?: any, deleteLinkObj?: any, dataProviderId?:any, resources?: any): Observable<any> {
        const model = request;
        for(let i=0; i < resources.length; i++) {
            if(deleteLinkObj === resources[i].id) {
                this.relevantProviders = resources[i].relavantDataProviders;
                for(let i=0; i < this.relevantProviders.length; i++) {
                    if(this.relevantProviders[i].id === dataProviderId) {
                         this.resourcesObj= resources[i];
                      this.relevantProviders.splice(i, 1);
                      this.resourcesObj.relavantDataProviders = this.relevantProviders;
                    }
                }
               // console.log('Delete',this.deleteLinkObj,this.resources[i].id, this.resources[i].relavantDataProviders, this.itemRelevantProviders);
            }
        }
        return this.http.delete(this.resourceAdminUrl + '?providerId='+dataProviderId +'&resourceId='+deleteLinkObj);

    }

    updateAllResources(request?: any, action?: string): Observable<any> {
        const model = request;
        return this.http.post(this.updateAllResourceAdminUrl, model);
    }
}
