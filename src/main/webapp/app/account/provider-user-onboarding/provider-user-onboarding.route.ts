import { Route } from '@angular/router';
import { CanDeactivateGuard, UserRouteAccessService } from 'app/shared';
import { ProviderUserOnboardingComponent } from './provider-user-onboarding-form.component';

export const providerUserOnboardingRoute: Route = {
    path: 'provider-onboarding',
    data: {
        authorities: ['ROLE_PROVIDER'],
        pageTitle: 'Data Provider Onboarding',
        breadcrumb: ' Data Provider Onboarding',
        //type: 'dynamic'
    },
    component: ProviderUserOnboardingComponent,
    canActivate: [UserRouteAccessService],
    // canDeactivate: [CanDeactivateGuard]
};
