import {
    Component,
    OnInit,
    OnDestroy,
    ViewChild,
    AfterViewInit,
    Inject,
    Injectable,
    Input,
    Output,
    EventEmitter,
    HostListener,
    ElementRef
} from '@angular/core';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { DomSanitizer } from '@angular/platform-browser';
import { DataProviderService } from 'app/entities/data-provider/data-provider.service';
import { ContactDialogComponent } from 'app/shared/contactdialog/contact-dialog.component';
import { Principal } from 'app/shared/auth/principal.service';
import { DataSource } from '@angular/cdk/table';
import { GoogleAnalyticsEventsService } from 'app/shared/googleanalytics/google-analytics-events.service';
import { NestedTreeControl, FlatTreeControl } from '@angular/cdk/tree';
import { WINDOW } from 'app/layouts';
import { SigninModalService, AmassFilterService, CSRFService, AuthServerProvider, StateStorageService } from 'app/shared';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig, FormlyTemplateOptions } from '@ngx-formly/core';
import { ConfirmDialogComponent } from 'app/shared/dialogs/confirm-dialog.component';
import { UppyService, UppyComponent } from 'app/uppy';
import { contains } from 'ramda';
let _that = null;
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie';
import { DataProviderLinkAdminFormComponent } from 'app/entities/data-provider/data-provider-link-admin-form.component';
import { environmentDev } from 'app/environments/environment.dev';
import { AddResourceAdminService } from 'app/add-resource-admin/add-resource-admin.service';
import { AmassSettingService } from '../settings/settings.service';
import { DataResource } from 'app/add-resource-admin/add-resource-admin-form.model';
import { ProviderUserOnboardingService } from './provider-user-onboarding.service';
import { UserOnboardingService } from '../user-onboarding/user-onboarding.service';
import { ApplyAccountComponent } from '../apply/apply.component';

@Component({
    selector: 'jhi-provider-user-onboarding-form',
    templateUrl: './provider-user-onboarding-form.component.html',
    styleUrls: ['../../../content/css/material-tab.css', './provider-user-onboarding.css']
})
export class ProviderUserOnboardingComponent implements OnInit {
    dialogRef: MatDialogRef<ConfirmDialogComponent>;
    //adminDialogRef: MatDialogRef<AddResourceAdminFormComponent>
    resDialogRef: MatDialogRef<any>;
    mdDialogRef: MatDialogRef<any>;
    formSubmitted: boolean;
    dataResourceName: any;
    dataResourceId: any;
    dataResourceModel: any;
    dataResourceRecordId: any;
    dataResourceLinkID: any;
    routerOutletRedirect: string;
    valueChanged: boolean;

    resourceID: number;
    basicProvider: any;
    submissionMessage: boolean;
    dataName: any;
    basicModelID: any;
    resourceRecordID: any;
    linkID: any;
    emptyField: boolean;
    dataLinksList: any[];
    linkMapId: any;
    editLinkMapId: any;
    editRelevantFile: any;
    fileId: any;
    fileName: any;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    isLoading: boolean;
    isAdmin: boolean;
    isPendingUnlock: boolean;
    isPendingExpand: boolean;
    domSanitizer: any;
    authoredArticles: any;
    relevantArticles: any;
    tabIndex: number;
    relevantArticlePage: number;
    relevantArticleLinks: any;
    relevantArticleFrom: number;
    totalRelevantArticles: number;
    authoredArticlePage: number;
    authoredArticleLinks: any;
    authoredArticleFrom: number;
    totalAuthoredArticles: number;
    recordID: string;
    profileTabLabel: any;
    ProfileCategory: any;
    equitesDelete: any;
    relevantOrganizations: any[] = [];
    relevantDataProviders: any[] = [];
    trackLoad: any;
    closePopup: boolean;
    redirect: any;

    progressBarColor = 'primary';
    progressBarMode = 'determinate';
    mdTooltipDelay: number;
    trackname: any;
    tracklink: any;
    trackfile: any;
    trackownerOrganizationCity: any;
    trackownerOrganizationState: any;
    trackownerOrganizationCountry: any;
    trackownerOrganizationYearfound: number;
    trackownerOrganizationHeadcount: any;
    trackownerOrganizationHeadcountNumber: any;
    trackownerOrganizationHeadcountBucket: any;
    tracknumberOfAnalystEmployees: any;
    trackInvestor: any;
    trackNumberOfInvestor: any;
    topicsCount: number = 0;

    dataLinks: any[] = [];
    dataFiles: any[] = [];
    resourceMarketplaceStatus: any[] = [];
    resourceWorkFlows: any[] = [];
    resourceResearchMethods: any[] = [];
    resourceIEIStatus: any[] = [];
    resourcePriority: any[] = [];
    resourceTopics: any[] = [];
    resourcePurposes: any[] = [];
    resourcePurposeType: any[] = [];
    selectedValue: any[] = [];

    basicExpansionPanel: boolean;
    adminExpansionPanel: boolean;
    categorizationsExpansionPanel: boolean;
    relevantEntitiesPanel: boolean;
    panelName: string;
    overAll: string;
    progressSpinner: boolean;

    basicForm = new FormGroup({});
    basicLinkForm = new FormGroup({});
    basicFileForm = new FormGroup({});
    basicResourceForm = new FormGroup({});

    basicModel: any = {};

    basicOptions: FormlyFormOptions = {};
    basicLinkOptions: FormlyFormOptions = {};
    basicFileOptions: FormlyFormOptions = {};
    basicResourceOptions: FormlyFormOptions = {};

    adminForm = new FormGroup({});
    adminModel: any = {};
    adminOptions: FormlyFormOptions = {};

    categorizationsDetailsForm = new FormGroup({});
    categorizationsDetailsModel: any = {};
    categorizationsDetailsOptions: FormlyFormOptions = {};

    relevantEntitiesDetailsForm = new FormGroup({});
    relevantEntitiesDetailsModel: any = {};
    relevantEntitiesDetailsOptions: FormlyFormOptions = {};

    providerID: any;
    providerRecordID: string = '';
    userID: any;
    partnershipValue: any;
    providerUserModelID: any;
    providerUserModelRecordID: string = '';
    providerUserOnboardingObj: any;
    hideMarketingSection: boolean = false;
    hideDocumentSection: boolean = false;
	hideSamplesSection: boolean = false;
	hideReportSection: boolean = false;
    dataCategories: any[] = [];
    geographies: any[] = [];
    oldRegionsValue: any;
    assetClasses: any[] = [];
    sectors: any[] = [];

    marketingLiteratureList: DataResource[] = [];
    dataDictionaryList: DataResource[] = [];
    samplesAndExamplesList: DataResource[] = [];
    reportsAnalysesList: DataResource[] = [];
    marketingLiteratureResource: DataResource[] = [];

    providerUserModel: any = {};

    dataProductsForm = new FormGroup({});
    dataProductsModel: any = {};
    dataProductsOptions: FormlyFormOptions = {};

    marketingForm = new FormGroup({});
    documentMarketingModel: any = {};

    documentForm = new FormGroup({});
    documentDataDictionaryModel: any = {};

	samplesForm = new FormGroup({});
    documentSampleModel: any = {};

	reportForm = new FormGroup({});
    documentReportModel: any = {};
    documentOptions: FormlyFormOptions = {};

	documentModel: any = {};

    dataChecklistPanel: boolean = false;
    dataProductsPanel: boolean = false;
    documentExpansionPanel: boolean = false;

    dataProductsFields: FormlyFieldConfig[] = [
        {
            key: 'categories',
            type: 'multiselect',
            className: 'custom-multiselect',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Data Categories:',
                description:
                    'What are all the data categories your data offerings fit into, aside from your main data category? Which classifications describe all of the types of data/information/research you collect/provide/analyze?',
                required: true,
                options: this.dataCategories,
                attributes: {
                    // field: 'desc',
                    placeholderText: '',
                    labelTooltip: '',
                    descriptionTooltip: '',
                    placeholderTooltip: ''
                },
                change: (event: any) => {}
            }
        },
        {
            key: 'geographies',
            type: 'checkbox',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Regions:',
                description: 'Which geographical regions  do your data offerings focus on?',
                required: true,
                options: this.geographies,
                attributes: {
                    // field: 'id',
                    multiple: 'true',
                    placeholderText: '',
                    labelTooltip: '',
                    descriptionTooltip: '',
                    placeholderTooltip: ''
                },
                change: (event) => {
                    console.log(event, event['modelKey']);
                    const newArr = event['modelKey'];
                    const finalArr = [];
                    newArr.map(item => {
                        finalArr.push({
                            id: item
                        });
                        this.providerUserModel.geographies = finalArr;
                    });
                    console.log(this.providerUserModel.geographies, this.providerUserModel);
                }
            }
        },
        {
            key: 'dataRequirements',
            type: 'input',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Data Requirements:',
                description: 'Describe any requirements for a data buyer to have an interest in your data products.',
                attributes: {
                    placeholderText:
                        'An example: "Buyers of our data products are typically large quantitative hedge funds that have their own data science teams and prefer datasets covering hundreds of US equities."',
                    labelTooltip: '',
                    descriptionTooltip: '',
                    placeholderTooltip: ''
                },
                keypress: event => {}
            }
        },
        {
            key: 'assetClasses',
            type: 'multiselect',
            className: 'custom-multiselect',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Asset Classes:',
                description: 'What are all the security asset classes your data offerings can be used to research?',
                required: true,
                options: this.assetClasses,
                attributes: {
                    placeholderText: '',
                    labelTooltip: '',
                    descriptionTooltip: '',
                    placeholderTooltip: ''
                },
                change: (event: any) => {}
            }
        },
        {
            key: 'sectors',
            type: 'multiselect',
            className: 'custom-multiselect',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Sectors:',
                description: 'What are all the sectors your data offerings could be used to research?',
                required: true,
                options: this.sectors,
                attributes: {
                    // field: 'id',
                    placeholderText: '',
                    labelTooltip: '',
                    descriptionTooltip: '',
                    placeholderTooltip: ''
                },
                change: (event: any) => {}
            }
        },
        {
            key: '',
            type: 'fileUpload',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Provider Logo:',
                description:'',
                attributes: {
                    'logoType' : 'Logo',
					'accept': 'image/*',
					'role': 'admin',
                    labelTooltip: '',
                    descriptionTooltip: ''
                },
                change: (event: any) => {
                    if (event && event[0].name) {
                        this.basicInput(event);
                    }
                    this.dataProviderService.onUpload(this.providerRecordID, event[0]).subscribe(data=> {
						// console.log('Logo', data);
						if (data.logo) {
							this.providerUserModel = {
								...this.providerUserModel,
								logo: {logo: data.logo}
							}
							this.dataProviderService.sendChangeSignal(data.logo);
						}
						// console.log('Basic Model', this.providerUserModel);
					});
                }
            }
        },
    ];

    documentMarketingLiterature: FormlyFieldConfig[] = [
        {
            key: '',
            type: 'fileUpload',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Marketing Literature:',
                description:
                    'Overviews of your company, data provider brand and data products, including marketing decks/presentations, data profile/briefs/one-pagers, pre-filled questionnaires, and your About/Products websites.',
                attributes: {
                    fileType: 'File',
                    placeholderText: '',
                    labelTooltip: 'Upload file attachments and/or submit the URLs of websites.',
                    descriptionTooltip: 'Upload file attachments and/or submit the URLs of websites.'
                },
                change: (event: any) => {
                    if (event && event[0].name) {
                        this.basicInput(event);
                    }
                    if (this.providerRecordID) {
                        this.dataProviderService.onResourceFileUpload(this.providerRecordID, event[0]).subscribe(data => {
                            console.log('Data 1', data);
                            const marketingFileData = data;
                            const marketingFileObj = {};
                            marketingFileObj['file'] = { id: data.id };
                            marketingFileObj['name'] = data.fileName;
                            marketingFileObj['purposes'] = [{id: 70}];
                            marketingFileObj['relavantDataProviders'] = [{id: this.providerID}];
                            console.log('Data 2', marketingFileObj);
                            setTimeout(() => {
                                this.dataResourceService.addResourceAdmin(marketingFileObj).subscribe(result => {
                                    console.log('Data 3', result);
                                    const dataResourceObj = result;
                                    const createdResource = {};
                                    createdResource['name'] = dataResourceObj.name;
                                    createdResource['id'] = dataResourceObj.id;
                                    createdResource['fileType'] = marketingFileData.fileID;
                                    createdResource['resourceID'] = dataResourceObj.id;
                                    console.log('Data 4', createdResource);
                                    this.marketingLiteratureList.push(createdResource);
                                    console.log('Data 5', this.marketingLiteratureList);
                                });
                            }, 500);
                        });
                    } else {
                        this.providerUserOnboardingService.userOnboardingFileUpload(this.userID, event[0]).subscribe(data => {
                            console.log('Data 1', data);
                            const marketingFileData = data;
                            const marketingFileObj = {};
                            marketingFileObj['file'] = { id: data.id };
                            marketingFileObj['name'] = data.fileName;
                            marketingFileObj['purposes'] = [{id: 70}];
                            console.log('Data 2', marketingFileObj);
                            setTimeout(() => {
                                this.dataResourceService.addResourceAdmin(marketingFileObj).subscribe(result => {
                                    console.log('Data 3', result);
                                    const dataResourceObj = result;
                                    const createdResource = {};
                                    createdResource['name'] = dataResourceObj.name;
                                    createdResource['id'] = dataResourceObj.id;
                                    createdResource['fileType'] = marketingFileData.fileID;
                                    createdResource['resourceID'] = dataResourceObj.id;
                                    console.log('Data 4', createdResource);
                                    this.marketingLiteratureList.push(createdResource);
                                    console.log('Data 5', this.marketingLiteratureList);
                                });
                            }, 500);
                        });
                    }
                }
            }
        },
        {
            key: 'link',
            type: 'input',
            className: 'customInputDoc',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: '',
                description: '',
                attributes: {
                    placeholders: 'https://example.com/about',
                    labelTooltip: '',
                    descriptionTooltip: ''
                },
                keypress: event => {
                    if (event) {
                        this.hideMarketingSection = false;
                    }
                }
            }
        },
        {
            key: '',
            type: 'button',
            className: 'addCustomButton',
            templateOptions: {
                label: '+ Add Link',
                attributes: {
                    btnType: 'button'
                },
                click: (event: any) => {
                    this.googleAnalyticsService.emitEvent(
                        'Provider Profile Form - Resource',
                        'Added',
                        'Provider Profile Form - Marketing Literature - Button - Add Link'
                    );
                    if (event && this.providerRecordID) {
                        this.dataProviderService.addLinkAdmin(this.documentMarketingModel).subscribe(data => {
                            const marketingLinkObj = {};
                            marketingLinkObj['link'] = {id:data.id};
                            marketingLinkObj['name'] = data.link;
                            marketingLinkObj['purposes'] = [{id: 70}];
                            marketingLinkObj['relavantDataProviders'] = [{id: this.providerID}];
                            if(marketingLinkObj) {
                                this.dataResourceService.addResourceAdmin(marketingLinkObj).subscribe(data => {
                                    const  dataObj = data;
                                    const createdResource = {};
                                    createdResource['name'] = dataObj.name;
                                    createdResource['id'] = dataObj.id;
                                    createdResource['linkType'] = dataObj.name;
                                    createdResource['resourceID'] = dataObj.id;
                                    this.marketingLiteratureList.push(createdResource);
                                });
                            }
                        });
                    } else {
                        this.dataProviderService.addLinkAdmin(this.documentMarketingModel).subscribe(data => {
                            const marketingLinkObj = {};
                            marketingLinkObj['link'] = {id:data.id};
                            marketingLinkObj['name'] = data.link;
                            marketingLinkObj['purposes'] = [{id: 70}];
                            if(marketingLinkObj) {
                                this.dataResourceService.addResourceAdmin(marketingLinkObj).subscribe(data => {
                                    const  dataObj = data;
                                    const createdResource = {};
                                    createdResource['name'] = dataObj.name;
                                    createdResource['id'] = dataObj.id;
                                    createdResource['linkType'] = dataObj.name;
                                    createdResource['resourceID'] = dataObj.id;
                                    this.marketingLiteratureList.push(createdResource);
                                });
                            }
                        });
                    }
                    this.marketingForm.get('link').setValue('');
                }
            },
            hideExpression: ((model: any, formState: any) => this.hideMarketingSection)
        }
        /* {
			type: 'button',
			className: 'customButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: '',
				text: '+ Add Link',
        		btnType: 'info',
				description: '',
                attributes: {
					'placeholderText' : '',
					'labelTooltip': '',
                    'descriptionTooltip': '',
                }
			},
		} */
    ];

    documentDataDictionary: FormlyFieldConfig[] = [
        {
            key: '',
            type: 'fileUpload',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Documentation & Data Dictionary:',
                description:
                    'Detailed documentation on your data products and datasets, including data dictionaries, API documentation, and coverage lists (companies/tickers).',
                attributes: {
                    fileType: 'File',
                    placeholderText: '',
                    labelTooltip: 'Upload file attachments and/or submit the URLs of websites.',
                    descriptionTooltip: 'Upload file attachments and/or submit the URLs of websites.'
                },
                change: (event: any) => {
                    if (event && event[0].name) {
                        this.basicInput(event);
                    }
                    if (this.providerRecordID) {
                        this.dataProviderService.onResourceFileUpload(this.providerRecordID, event[0]).subscribe(data => {
                            const documentFileData = data;
                            const datadictionaryObj = {};
                            datadictionaryObj['file'] = { id: data.id };
                            datadictionaryObj['name'] = data.fileName;
                            datadictionaryObj['purposes'] = [{ id: 72 }];
                            datadictionaryObj['relavantDataProviders'] = [{ id: this.providerID }];
                            setTimeout(() => {
                                this.dataResourceService.addResourceAdmin(datadictionaryObj).subscribe(result => {
                                    const dataObj = result;
                                    const createdResource = {};
                                    createdResource['name'] = dataObj.name;
                                    createdResource['id'] = dataObj.id;
                                    createdResource['fileType'] = documentFileData.fileID;
                                    createdResource['resourceID'] = dataObj.id;
                                    this.dataDictionaryList.push(createdResource);
                                });
                            }, 500);
                        });
                    } else {
                        this.providerUserOnboardingService.userOnboardingFileUpload(this.userID, event[0]).subscribe(data => {
                            const documentFileData = data;
                            const datadictionaryObj = {};
                            datadictionaryObj['file'] = { id: data.id };
                            datadictionaryObj['name'] = data.fileName;
                            datadictionaryObj['purposes'] = [{ id: 72 }];
                            setTimeout(() => {
                                this.dataResourceService.addResourceAdmin(datadictionaryObj).subscribe(result => {
                                    const dataObj = result;
                                    const createdResource = {};
                                    createdResource['name'] = dataObj.name;
                                    createdResource['id'] = dataObj.id;
                                    createdResource['fileType'] = documentFileData.fileID;
                                    createdResource['resourceID'] = dataObj.id;
                                    this.dataDictionaryList.push(createdResource);
                                });
                            }, 500);
                        });
                    }
                }
            }
        },
        {
            key: 'link',
            type: 'input',
            className: 'customInputDoc',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: '',
                description: '',
                attributes: {
                    placeholders: ' https://example.com/documentation',
                    labelTooltip: '',
                    descriptionTooltip: ''
                },
                // change: this.inputChanges,
                keypress: event => {
                    if (event) {
                        this.hideDocumentSection = false;
                    }
                }
            }
        },
        {
            key: '',
            type: 'button',
            className: 'addCustomButton',
            templateOptions: {
                label: '+ Add Link',
                attributes: {
                    btnType: 'button'
                },
                click: (event: any) => {
                    this.googleAnalyticsService.emitEvent(
                        'Provider Profile Form - Resource',
                        'Added',
                        'Provider Profile Form - Documentation & Data Dictionary - Button - Add Link'
                    );
                    if (event && this.providerRecordID) {
                        this.dataProviderService.addLinkAdmin(this.documentDataDictionaryModel).subscribe(data => {
                            const resourceObj = {};
                            resourceObj['link'] = { id: data.id };
                            resourceObj['name'] = data.link;
                            resourceObj['purposes'] = [{ id: 72 }];
                            resourceObj['relavantDataProviders'] = [{ id: this.providerID }];
                            if (resourceObj) {
                                this.dataResourceService.addResourceAdmin(resourceObj).subscribe(result => {
                                    const dataObj = result;
                                    const createdResource = {};
                                    createdResource['name'] = dataObj.name;
                                    createdResource['id'] = dataObj.id;
                                    createdResource['linkType'] = dataObj.name;
                                    createdResource['resourceID'] = dataObj.id;
                                    this.dataDictionaryList.push(createdResource);
                                });
                            }
                        });
                    } else {
                        this.dataProviderService.addLinkAdmin(this.documentDataDictionaryModel).subscribe(data => {
                            const resourceObj = {};
                            resourceObj['link'] = { id: data.id };
                            resourceObj['name'] = data.link;
                            resourceObj['purposes'] = [{ id: 72 }];
                            if (resourceObj) {
                                this.dataResourceService.addResourceAdmin(resourceObj).subscribe(result => {
                                    const dataObj = result;
                                    const createdResource = {};
                                    createdResource['name'] = dataObj.name;
                                    createdResource['id'] = dataObj.id;
                                    createdResource['linkType'] = dataObj.name;
                                    createdResource['resourceID'] = dataObj.id;
                                    this.dataDictionaryList.push(createdResource);
                                });
                            }
                        });
                    }
                    this.documentForm.get('link').setValue('');
                }
            },
            hideExpression: (model: any, formState: any) => this.hideDocumentSection
        }
        /* {
			type: 'button',
			className: 'customButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: '',
				text: '+ Add Link',
        		btnType: 'info',
				description: '',
                attributes: {
					'placeholderText' : '',
					'labelTooltip': '',
                    'descriptionTooltip': '',
                }
			},
		} */
    ];

    documentSamples: FormlyFieldConfig[] = [
        {
            key: '',
            type: 'fileUpload',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Samples & Examples:',
                description:
                    'Examples of your data products and datasets, including data snapshots/samples, product screenshots, demo videos/presentations, sample reports, and trial websites.',
                attributes: {
                    fileType: 'File',
                    placeholderText: '',
                    labelTooltip: 'Upload file attachments and/or submit the URLs of websites.',
                    descriptionTooltip: 'Upload file attachments and/or submit the URLs of websites.'
                },
                change: (event: any) => {
                    if (event && event[0].name) {
                        this.basicInput(event);
                    }
                    if (this.providerRecordID) {
                        this.dataProviderService.onResourceFileUpload(this.providerRecordID, event[0]).subscribe(data => {
                            const samplesFileData = data;
                            const samplesObj = {};
                            samplesObj['file'] = { id: data.id };
                            samplesObj['name'] = data.fileName;
                            samplesObj['purposes'] = [{ id: 93 }];
                            samplesObj['relavantDataProviders'] = [{ id: this.providerID }];
                            setTimeout(() => {
                                this.dataResourceService.addResourceAdmin(samplesObj).subscribe(result => {
                                    const dataObj = result;
                                    const createdResource = {};
                                    createdResource['name'] = dataObj.name;
                                    createdResource['id'] = dataObj.id;
                                    createdResource['fileType'] = samplesFileData.fileID;
                                    createdResource['resourceID'] = dataObj.id;
                                    this.samplesAndExamplesList.push(createdResource);
                                });
                            }, 500);
                        });
                    } else {
                        this.providerUserOnboardingService.userOnboardingFileUpload(this.userID, event[0]).subscribe(data => {
                            const samplesFileData = data;
                            const samplesObj = {};
                            samplesObj['file'] = { id: data.id };
                            samplesObj['name'] = data.fileName;
                            samplesObj['purposes'] = [{ id: 93 }];
                            setTimeout(() => {
                                this.dataResourceService.addResourceAdmin(samplesObj).subscribe(result => {
                                    const dataObj = result;
                                    const createdResource = {};
                                    createdResource['name'] = dataObj.name;
                                    createdResource['id'] = dataObj.id;
                                    createdResource['fileType'] = samplesFileData.fileID;
                                    createdResource['resourceID'] = dataObj.id;
                                    this.samplesAndExamplesList.push(createdResource);
                                });
                            }, 500);
                        });
                    }
                }
            }
        },
        {
            key: 'link',
            type: 'input',
            className: 'customInputDoc',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: '',
                description: '',
                attributes: {
                    placeholders: ' https://example.com/data-sample',
                    labelTooltip: '',
                    descriptionTooltip: ''
                },
                // change: this.inputChanges,
                keypress: event => {
                    if (event) {
                        this.hideSamplesSection = false;
                    }
                }
            }
        },
        {
            key: '',
            type: 'button',
            className: 'addCustomButton',
            templateOptions: {
                label: '+ Add Link',
                attributes: {
                    btnType: 'button'
                },
                click: (event: any) => {
                    this.googleAnalyticsService.emitEvent(
                        'Provider Profile Form - Resource',
                        'Added',
                        'Provider Profile Form - Samples & Examples - Button - Add Link'
                    );
                    if (event && this.providerRecordID) {
                        this.dataProviderService.addLinkAdmin(this.documentSampleModel).subscribe(data => {
                            const resourceObj = {};
                            resourceObj['link'] = { id: data.id };
                            resourceObj['name'] = data.link;
                            resourceObj['purposes'] = [{ id: 93 }];
                            resourceObj['relavantDataProviders'] = [{ id: this.providerID }];
                            if (resourceObj) {
                                this.dataResourceService.addResourceAdmin(resourceObj).subscribe(result => {
                                    const dataObj = result;
                                    const createdResource = {};
                                    createdResource['name'] = dataObj.name;
                                    createdResource['id'] = dataObj.id;
                                    createdResource['linkType'] = dataObj.name;
                                    createdResource['resourceID'] = dataObj.id;
                                    this.samplesAndExamplesList.push(createdResource);
                                });
                            }
                        });
                    } else {
                        this.dataProviderService.addLinkAdmin(this.documentSampleModel).subscribe(data => {
                            const resourceObj = {};
                            resourceObj['link'] = { id: data.id };
                            resourceObj['name'] = data.link;
                            resourceObj['purposes'] = [{ id: 93 }];
                            if (resourceObj) {
                                this.dataResourceService.addResourceAdmin(resourceObj).subscribe(result => {
                                    const dataObj = result;
                                    const createdResource = {};
                                    createdResource['name'] = dataObj.name;
                                    createdResource['id'] = dataObj.id;
                                    createdResource['linkType'] = dataObj.name;
                                    createdResource['resourceID'] = dataObj.id;
                                    this.samplesAndExamplesList.push(createdResource);
                                });
                            }
                        });
                    }
                    this.samplesForm.get('link').setValue('');
                }
            },
            hideExpression: ((model: any, formState: any) => this.hideSamplesSection)
        }
        /* {
			type: 'button',
			className: 'customButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: '',
				text: '+ Add Link',
        		btnType: 'info',
				description: '',
                attributes: {
					'placeholderText' : '',
					'labelTooltip': '',
                    'descriptionTooltip': '',
                }
			},
		} */
    ];

    documentReportsAnalyses: FormlyFieldConfig[] = [
        {
            key: '',
            type: 'fileUpload',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Reports & Analyses:',
                description:
                    'Demonstrations of the value or utility of your data products and datasets, including backtests, case studies, white papers, research reports & articles, and analytics dashboards.',
                attributes: {
                    fileType: 'File',
                    placeholderText: '',
                    labelTooltip: 'Upload file attachments and/or submit the URLs of websites.',
                    descriptionTooltip: 'Upload file attachments and/or submit the URLs of websites.'
                },
                change: (event: any) => {
                    if (event && event[0].name) {
                        this.basicInput(event);
                    }
                    if (this.providerRecordID) {
                        this.dataProviderService.onResourceFileUpload(this.providerRecordID, event[0]).subscribe(data=> {
                            const reportFileData = data;
                            const reportsFileObj = {};
                            reportsFileObj['file'] = {id:data.id};
                            reportsFileObj['name'] = data.fileName;
                            reportsFileObj['purposes'] = [{id: 91}];
                            reportsFileObj['relavantDataProviders'] = [{ id: this.providerID }];
                            setTimeout(() => {
                                this.dataResourceService.addResourceAdmin(reportsFileObj).subscribe(data => {
                                    const  dataObj = data;
                                    const createdResource = {};
                                    createdResource['name'] = dataObj.name;
                                    createdResource['id'] = dataObj.id;
                                    createdResource['fileType'] = reportFileData.fileID;
                                    createdResource['resourceID']=dataObj.id;
                                    this.reportsAnalysesList.push(createdResource);
                                });
                            }, 500);
                        });
                    } else {
                        this.providerUserOnboardingService.userOnboardingFileUpload(this.userID, event[0]).subscribe(data => {
                            const reportFileData = data;
                            const reportsFileObj = {};
                            reportsFileObj['file'] = {id:data.id};
                            reportsFileObj['name'] = data.fileName;
                            reportsFileObj['purposes'] = [{id: 91}];
                            setTimeout(() => {
                                this.dataResourceService.addResourceAdmin(reportsFileObj).subscribe(data => {
                                    const  dataObj = data;
                                    const createdResource = {};
                                    createdResource['name'] = dataObj.name;
                                    createdResource['id'] = dataObj.id;
                                    createdResource['fileType'] = reportFileData.fileID;
                                    createdResource['resourceID']=dataObj.id;
                                    this.reportsAnalysesList.push(createdResource);
                                });
                            }, 500);
                        });
                    }
                }
            }
        },
        {
            key: 'link',
            type: 'input',
            className: 'customInputDoc',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: '',
                description: '',
                attributes: {
                    placeholders: ' https://example.com/report',
                    labelTooltip: '',
                    descriptionTooltip: ''
                },
                // change: this.inputChanges,
                keypress: event => {
                    if(event) {
                        this.hideReportSection = false;
                    }
                }
            }
        },
        {
            key: '',
            type: 'button',
            className: 'addCustomButton',
            templateOptions: {
                label: '+ Add Link',
                attributes: {
                    btnType: 'button'
                },
                click: (event: any) => {
                    this.googleAnalyticsService.emitEvent('Provider Profile Form - Resource', 'Added', 'Provider Profile Form - Reports & Analyses - Button - Add Link');
                        if(event && this.providerRecordID) {
                            this.dataProviderService.addLinkAdmin(this.documentReportModel).subscribe(data => {
                                const resourceObj = {};
                                resourceObj['link'] = {id:data.id};
                                resourceObj['name'] = data.link;
                                resourceObj['purposes'] = [{id: 91}];
                                resourceObj['relavantDataProviders'] = [{id: this.providerID}];
                                if(resourceObj) {
                                    this.dataResourceService.addResourceAdmin(resourceObj).subscribe(data => {
                                        const  dataObj = data;
                                        const createdResource = {};
                                        createdResource['name'] = dataObj.name;
                                        createdResource['id'] = dataObj.id;
                                        createdResource['linkType'] = dataObj.name;
                                        createdResource['resourceID'] = dataObj.id;
                                        this.reportsAnalysesList.push(createdResource);
                                    });
                                }
                            });
                        } else {
                            this.dataProviderService.addLinkAdmin(this.documentReportModel).subscribe(data => {
                                const resourceObj = {};
                                resourceObj['link'] = {id:data.id};
                                resourceObj['name'] = data.link;
                                resourceObj['purposes'] = [{id: 91}];
                                if(resourceObj) {
                                    this.dataResourceService.addResourceAdmin(resourceObj).subscribe(data => {
                                        const  dataObj = data;
                                        const createdResource = {};
                                        createdResource['name'] = dataObj.name;
                                        createdResource['id'] = dataObj.id;
                                        createdResource['linkType'] = dataObj.name;
                                        createdResource['resourceID'] = dataObj.id;
                                        this.reportsAnalysesList.push(createdResource);
                                    });
                                }
                            });
                        }
                    this.reportForm.get('link').setValue('');
                }
            },
            hideExpression: ((model: any, formState: any) => this.hideReportSection)
        }
        /* {
			type: 'button',
			className: 'customButton',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: '',
				text: '+ Add Link',
        		btnType: 'info',
				description: '',
                attributes: {
					'placeholderText' : '',
					'labelTooltip': '',
                    'descriptionTooltip': '',
                }
			},
		} */
    ];

    basicLinkFields: FormlyFieldConfig[] = [
        {
            key: 'link',
            type: 'autocomplete',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Link:',
                placeholder: '',
                required: false,
                options: this.dataLinks,
                attributes: {
                    field: 'link',
                    trackNames: 'Link',
                    trackCategory: 'Resource Admin - Input',
                    trackLabel: 'Resource Admin - Dropdown - Link',
                    minLength: '1'
                },
                change: (event: any) => {
                    //console.log('default value....', event);
                    if (event) {
                        this.valueChanged = true;
                    }
                    const queryObj = event;
                    this.dataLinks = [];
                    this.basicInput(event);
                    if (queryObj.query) {
                        this.filterService.getDataLinks(queryObj.query).subscribe(d => {
                            const listItems = d ? d['hits']['hits'] : [];
                            //console.log('...search...', listItems);
                            for (let i = 0; i < listItems.length; i++) {
                                //console.log('...search 1...', listItems);
                                this.dataLinks.push({
                                    id: listItems[i]._source.id,
                                    link: listItems[i]._source.link
                                });
                                //console.log('...search...', this.dataOrganizations);
                            }
                            this.basicLinkFields[0].templateOptions.options = this.dataLinks;
                        });
                    }
                },
                blur: (event: any) => {
                    // if (event && event.target && !event['selectedVal']) {
                    //     event.target.value = '';
                    // }
                    if (event && event.target && event.target.value === '') {
                        this.emptyField = true;
                        this.basicModel.link = null;
                        console.log('link blur', event.target.value, this.emptyField);
                    }
                },
                click: (event: any) => {
                    this.linkID = event ? event.id : '';
                    if (this.linkID != '' && this.linkID != undefined && this.linkID != null) {
                        this.emptyField = false;
                        this.basicModel = {
                            ...this.basicModel,
                            name: event.link
                        };
                    } else {
                        this.emptyField = true;
                    }
                    //console.log(this.organizationButton, '------', this.emptyField);
                }
            }
        }
    ];
    basicFileFields: FormlyFieldConfig[] = [
        {
            key: 'file',
            type: 'autocomplete',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'File:',
                placeholder: '',
                required: false,
                options: this.dataFiles,
                attributes: {
                    field: 'fileName',
                    trackNames: 'File',
                    trackCategory: 'Resource Admin - Input',
                    trackLabel: 'Resource Admin - Attachment - File',
                    logoType: 'Logo',
                    minLength: '1'
                },
                change: (event: any) => {
                    //console.log('default value....', event);
                    const queryObj = event;
                    this.dataFiles = [];
                    if (event) {
                        this.valueChanged = true;
                    }
                    if (queryObj.query) {
                        this.filterService.getAllFiles(queryObj.query).subscribe(d => {
                            const listItems = d ? d['hits']['hits'] : [];
                            //console.log('...search...', listItems);
                            for (let i = 0; i < listItems.length; i++) {
                                //console.log('...search 1...', listItems);
                                this.dataFiles.push({
                                    id: listItems[i]._source.id,
                                    fileName: listItems[i]._source.fileName
                                });
                                //console.log('...search...', this.dataOrganizations);
                            }
                            this.basicFileFields[0].templateOptions.options = this.dataFiles;
                        });
                    }
                },
                blur: (event: any) => {
                    //console.log('Blur event', event);
                    // if (event && event.target && !event['selectedVal']) {
                    // 	// event.target.value = '';
                    // }
                    if (event && event.target && event.target.value === '') {
                        this.basicModel.file = null;
                    }
                },
                click: (event: any) => {
                    //console.log('click event', event);
                    //console.log('Basic Form', this.basicFileForm);
                    this.basicModel = {
                        ...this.basicModel,
                        name: event.fileName
                    };
                }
            }
        },
        {
            key: '',
            type: 'fileUpload',
            className: 'Width',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: '',
                change: (event: any) => {
                    this.dataProviderService.onResourceFileUpload(this.providerRecordID, event[0]).subscribe(data => {
                        this.fileName = data.fileName;
                        this.fileId = data.id;
                        if (this.fileName) {
                            this.basicModel = {
                                ...this.basicModel,
                                file: { id: this.fileId, fileName: this.fileName },
                                name: this.fileName
                            };
                        }
                    });
                }
            },
            hideExpression: 'model.file'
        }
    ];
    basicResourceFields: FormlyFieldConfig[] = [
        {
            key: 'name',
            type: 'input',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Resource Name:',
                placeholder: '',
                required: true,
                attributes: {
                    trackNames: 'Resource Name',
                    trackCategory: 'Resource Admin - Input',
                    trackLabel: 'Resource Admin - Textbox - Name'
                },
                change: (event: any) => {
                    // console.log('Drop', event);
                    this.basicInput;
                    if (event) {
                        // console.log('Input', event);
                        this.valueChanged = true;
                    }
                },
                keypress: event => {
                    // console.log('Event', event);
                    if (event) {
                        this.valueChanged = true;
                        // console.log('Event 1', this.valueChanged);
                    }
                }
            }
        }
    ];

    adminFields: FormlyFieldConfig[] = [
        // {
        //     key: 'userPermission',
        //     type: 'radioButton',
        //     defaultValue: 'ROLE_SILVER',
        //     wrappers: ['form-field-horizontal'],
        //     templateOptions: {
        //         label: 'User Permissions:',
        //         required: true,
        //         description: 'Who should be able to see this resource?',
        //         attributes: {
        //             trackNames: 'User Permissions',
        //             trackCategory: 'Resource Admin - Input',
        //             trackLabel: 'Resource Admin - Dropdown - User Permissions'
        //         },
        //         options: [
        //             {
        //                 value: 'ROLE_ADMIN',
        //                 label: 'Admin'
        //             },
        //             {
        //                 value: 'ROLE_SILVER',
        //                 label: 'Silver'
        //             },
        //             {
        //                 value: 'ROLE_BRONZE',
        //                 label: 'Bronze'
        //             },
        //             {
        //                 value: 'ROLE_USER',
        //                 label: 'Registered'
        //             }
        //         ],
        //         change: (event: any) => {
        //             // console.log('Drop', event);
        //             if (event) {
        //                 this.valueChanged = true;
        //             }
        //         }
        //     }
        // },
        // {
        //     key: 'marketplaceStatus.id',
        //     type: 'dropdown',
        //     className: 'customMediumWidth-dropdown',
        //     defaultValue: environmentDev.dev ? 2043 : 1985,
        //     wrappers: ['form-field-horizontal'],
        //     templateOptions: {
        //         label: 'Marketplace Status:',
        //         description: 'What should an external client understand about the status of our research on this resource?',
        //         required: true,
        //         options: this.resourceMarketplaceStatus,
        //         attributes: {
        //             placeholderText: '',
        //             field: 'label',
        //             labelTooltip:
        //                 'The only statuses IEI will be using are ""Minimal"" and ""Complete"". Ignore all other statuses.Explanation of choices:""Minimal"": When you\'ve only done the minimal research""Complete"": When the full research has been completed	',
        //             descriptionTooltip:
        //                 'The only statuses IEI will be using are ""Minimal"" and ""Complete"". Ignore all other statuses. Explanation of choices:""Minimal"": When you\'ve only done the minimal research""Complete"": When the full research has been completed',
        //             trackNames: 'Marketplace Status',
        //             trackCategory: 'Resource Admin - Input',
        //             trackLabel: 'Resource Admin - Dropdown - Marketplace Status'
        //         },
        //         change: (event: any) => {
        //             // console.log('Drop', event);
        //             if (event) {
        //                 this.valueChanged = true;
        //             }
        //         }
        //     }
        // },
        // {
        //     key: 'workflows',
        //     type: 'multiselect',
        //     //className: 'custom-multiselect',
        //     wrappers: ['form-field-horizontal'],
        //     templateOptions: {
        //         label: 'Workflows:',
        //         description: 'What are the workflows that need to be completed to complete the research on this resource?',
        //         required: false,
        //         options: this.resourceWorkFlows,
        //         attributes: {
        //             placeholderText: 'An indication of the process that should be taken when reviewing the resource.',
        //             field: 'desc',
        //             labelTooltip:
        //                 "If the resource mentions more than 10 potentially relevant companies, designate it Numerous Companies, then label the resource 'Amass Review' and move on.If each tracked company in the resource has been categorized as a Data Tool, designate it Data Tool and move on.If a tracked Data Tool is mentioned, you may add the Data Tool workflow.",
        //             descriptionTooltip:
        //                 "If the resource mentions more than 10 potentially relevant companies, designate it Numerous Companies, then label the resource 'Amass Review' and move on.If each tracked company in the resource has been categorized as a Data Tool, designate it Data Tool and move on.If a tracked Data Tool is mentioned, you may add the Data Tool workflow.",
        //             placeholderTooltip:
        //                 "If the resource mentions more than 10 potentially relevant companies, designate it Numerous Companies, then label the resource 'Amass Review' and move on.If each tracked company in the resource has been categorized as a Data Tool, designate it Data Tool and move on.If a tracked Data Tool is mentioned, you may add the Data Tool workflow.",
        //             trackNames: 'Workflows',
        //             trackCategory: 'Resource Admin - Input',
        //             trackLabel: 'Resource Admin - Dropdown - Workflow'
        //         },
        //         change: (event: any) => {
        //             if (event) {
        //                 this.valueChanged = true;
        //             }
        //         }
        //     }
        // },
        // {
        //     key: 'researchMethodsCompleted',
        //     type: 'multiselect',
        //     className: 'custom-multiselect',
        //     wrappers: ['form-field-horizontal'],
        //     templateOptions: {
        //         label: 'Research Methods Completed:',
        //         description: 'What types/methods of researching this resource have been completed?',
        //         required: true,
        //         options: this.resourceResearchMethods,
        //         attributes: {
        //             placeholderText: '',
        //             field: 'desc',
        //             labelTooltip: "Fill these in as you research a resource, and especially after you're done researching a resource.",
        //             descriptionTooltip:
        //                 "Fill these in as you research a resource, and especially after you're done researching a resource.",
        //             trackNames: 'Research Methods Completed',
        //             trackCategory: 'Resource Admin - Input',
        //             trackLabel: 'Resource Admin - Dropdown - Research Methods Completed'
        //         },
        //         change: (event: any) => {
        //             if (event) {
        //                 this.valueChanged = true;
        //             }
        //         }
        //     }
        // },
        // {
        //     key: 'ieiStatus.id',
        //     type: 'dropdown',
        //     className: 'mediumWidthSticky-dropdown',
        //     wrappers: ['form-field-horizontal'],
        //     templateOptions: {
        //         label: 'IEI Status:',
        //         description: "What's the status of this resource's research?",
        //         required: false,
        //         options: this.resourceIEIStatus,
        //         attributes: {
        //             placeholderText: '',
        //             field: 'label',
        //             labelTooltip: '',
        //             descriptionTooltip: '',
        //             trackNames: 'IEI Status',
        //             trackCategory: 'Resource Admin - Input',
        //             trackLabel: 'Resource Admin - Dropdown - IEI Status'
        //         },
        //         change: (event: any) => {
        //             // console.log('Drop', event);
        //             if (event) {
        //                 this.valueChanged = true;
        //             }
        //         }
        //     }
        // },
        // {
        //     key: 'priority.id',
        //     type: 'dropdown',
        //     className: 'widthSticky-dropdown',
        //     wrappers: ['form-field-horizontal'],
        //     templateOptions: {
        //         label: 'Priority:',
        //         description: '',
        //         required: false,
        //         options: this.resourcePriority,
        //         attributes: {
        //             placeholderText: 'If filled in, this denotes how a researcher should prioritize their work.',
        //             field: 'label',
        //             labelTooltip: '',
        //             descriptionTooltip: '',
        //             trackNames: 'Priority',
        //             trackCategory: 'Resource Admin - Input',
        //             trackLabel: 'Resource Admin - Dropdown - Priority'
        //         },
        //         change: (event: any) => {
        //             // console.log('Drop', event);
        //             if (event) {
        //                 this.valueChanged = true;
        //             }
        //         }
        //     }
        // }
    ];
    categorizationsFields: FormlyFieldConfig[] = [
        {
            key: 'topics',
            type: 'multiselect',
            className: 'custom-multiselect',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Topics (' + this.topicsCount + '):',
                description: 'What topics does this resource relate to or touch upon?',
                required: false,
                options: this.resourceTopics,
                attributes: {
                    placeholderText: '',
                    field: 'id',
                    labelTooltip: '',
                    descriptionTooltip: '',
                    trackNames: 'Topics',
                    trackCategory: 'Resource Admin - Input',
                    trackLabel: 'Resource Admin - Dropdown - Topic'
                },
                change: (event: any) => {
                    if (event) {
                        this.valueChanged = true;
                    }
                }
            }
        },
        {
            key: 'purposes',
            type: 'multiselect',
            className: 'custom-multiselect',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Purposes:',
                description: '',
                required: false,
                options: this.resourcePurposes,
                attributes: {
                    placeholderText: 'A quick categorization of the focus of the resource.',
                    field: 'id',
                    labelTooltip:
                        "There may be inaccurate purposes already recorded, these can be taken out.There is no limit on the number of purposes a resource can have.Resources regarding new organizations, events or products should be labelled 'New'.For Employee Movement resources, do not link all of the former Organizations and Data Providers the employees mentioned in the resource worked at. Only link the most immediate Organizations and Data Providers that the employee left and went to. So if an employee went from Organization A to Organization B but used to work at Organization C, only link Organizations A and B.",
                    descriptionTooltip: '',
                    placeholderTooltip:
                        'There may be inaccurate purposes already recorded, these can be taken out.There is no limit on the number of purposes a resource can have.Resources regarding new organizations, events or products should be labelled New.For Employee Movement resources, do not link all of the former Organizations and Data Providers the employees mentioned in the resource worked at. Only link the most immediate Organizations and Data Providers that the employee left and went to. So if an employee went from Organization A to Organization B but used to work at Organization C, only link Organizations A and B.',
                    trackNames: 'Purposes',
                    trackCategory: 'Resource Admin - Input',
                    trackLabel: 'Resource Admin - Dropdown - Purpose'
                },
                change: (event: any) => {
                    if (event) {
                        this.valueChanged = true;
                    }
                }
            }
        },
        {
            key: 'purposeTypes',
            type: 'multiselect',
            className: 'custom-multiselect',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Purpose Types:',
                description: '',
                required: false,
                options: this.resourcePurposeType,
                attributes: {
                    placeholderText: '',
                    field: 'id',
                    labelTooltip:
                        'If an organization is only one product, link it as Organization, not Product. Most resources will be either Organization or Product types.',
                    descriptionTooltip: '',
                    placeholderTooltip: '',
                    trackNames: 'Purpose Types',
                    trackCategory: 'Resource Admin - Input',
                    trackLabel: 'Resource Admin - Dropdown - Purpose Type'
                },
                change: (event: any) => {
                    if (event) {
                        this.valueChanged = true;
                    }
                }
            }
        }
    ];
    relevantEntitiesFields: FormlyFieldConfig[] = [
        {
            key: 'relavantOrganizations',
            type: 'autocomplete',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Relevant Organizations:*',
                type: 'autocomplete',
                //placeholder: '',
                required: false,
                options: this.relevantOrganizations,
                attributes: {
                    field: 'name',
                    minLength: '1',
                    multiple: 'true',
                    placeholderText: 'All of the Organizations mentioned in or relevant to the resource or the content in the resource.',
                    labelTooltip:
                        "Any organization mentioned and already in the database should be included.Only link Organizations that the resource really has any relevance towards. (As an example for Organizations not to link, see Purpose notes below).It's not necessary to link Organizations that are being mentioned in a way that isn't related to them being a data provider, data tool, or potential customer. For example, if the resource is an analysis of other companies, the companies beiung analyzed don't need to be linked here.",
                    placeholderTooltip:
                        "Any organization mentioned and already in the database should be included.Only link Organizations that the resource really has any relevance towards. (As an example for Organizations not to link, see Purpose notes below).It's not necessary to link Organizations that are being mentioned in a way that isn't related to them being a data provider, data tool, or potential customer. For example, if the resource is an analysis of other companies, the companies beiung analyzed don't need to be linked here.",
                    trackNames: 'Relevant Organizations',
                    trackCategory: 'Resource Admin - Input',
                    trackLabel: 'Resource Admin - Dropdown - Relevant Organizations'
                },
                change: (event: any) => {
                    //console.log('default value....', event);
                    const queryObj = event;
                    this.relevantOrganizations = [];
                    if (event) {
                        this.valueChanged = true;
                    }
                    const selected = this.basicModel['relavantOrganizations'];
                    ///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
                    if (selected && selected.length) {
                        this.selectedValue = selected.map(item => item.id);
                    }
                    if (queryObj.query) {
                        this.filterService.getAllOrganizations(queryObj.query).subscribe(d => {
                            const listItems = d && d['hits'] ? d['hits']['hits'] : [];
                            //console.log('...search...', listItems);
                            for (let i = 0; i < listItems.length; i++) {
                                if (!this.selectedValue.includes(listItems[i]._source.id)) {
                                    //console.log('...search...', listItems);
                                    this.relevantOrganizations.push({
                                        id: listItems[i]._source.id,
                                        name: listItems[i]._source.name
                                    });
                                }
                                //console.log('...search...', this.dataOrganizations);
                            }
                            this.relevantEntitiesFields[0].templateOptions.options = this.relevantOrganizations;
                        });
                    }
                }
            }
        },
        {
            key: 'relavantDataProviders',
            type: 'autocomplete',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Relevant Data Providers:',
                placeholder: '',
                required: false,
                attributes: {
                    field: 'providerName',
                    minLength: '1',
                    multiple: 'true',
                    placeholderText: 'All of the Data Providers mentioned in or relevant to the resource or the content in the resource.',
                    labelTooltip:
                        "Each Data Provider should first be included as a Relevant Organization. Be sure new Data Providers have an Owner Organization listed and the entries are connected. It's not necessary to link Data Providers that are being mentioned in a way that isn't related to them being a data provider. For example, if the resource is an analysis of a company's retail sales, even if that company is a data provider themselves, since the resource analyzes that company and is not using that company's own data, it should not included.",
                    placeholderTooltip:
                        "Each Data Provider should first be included as a Relevant Organization. Be sure new Data Providers have an Owner Organization listed and the entries are connected. It's not necessary to link Data Providers that are being mentioned in a way that isn't related to them being a data provider. For example, if the resource is an analysis of a company's retail sales, even if that company is a data provider themselves, since the resource analyzes that company and is not using that company's own data, it should not included.",
                    trackNames: 'Relevant Data Providers',
                    trackCategory: 'Resource Admin - Input',
                    trackLabel: 'Resource Admin - Dropdown - Relevant Data Providers'
                },
                change: (event: any) => {
                    const queryObj = event;
                    this.relevantDataProviders = [];
                    if (event) {
                        this.valueChanged = true;
                    }
                    const selected = this.basicModel['relavantOrganizations'];
                    ///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
                    if (selected && selected.length) {
                        this.selectedValue = selected.map(item => item.id);
                    }
                    if (queryObj.query) {
                        this.filterService.getAllProviders(queryObj.query).subscribe(d => {
                            const listItems = d && d['hits'] ? d['hits']['hits'] : [];
                            for (let i = 0; i < listItems.length; ++i) {
                                const item = listItems[i]._source;
                                if (
                                    listItems[i]._source.providerName !== '' &&
                                    listItems[i]._source.providerName !== 'RECORD_NULL' &&
                                    listItems[i]._source.providerName !== 'PROVIDER_NULL'
                                ) {
                                    if (!this.selectedValue.includes(item.id)) {
                                        this.relevantDataProviders.push({
                                            id: item.id,
                                            providerName: item.providerName,
                                            img: item.logo ? item.logo.logo : null
                                        });
                                    }
                                }
                            }
                            this.relevantEntitiesFields[1].templateOptions.options = this.relevantDataProviders;
                        });
                    }
                }
            }
        }
    ];

    csrf: string;
    organisationRecordID: any;

    constructor(
        private dataResourceService: AddResourceAdminService,
        private amassSettingService: AmassSettingService,
        private providerUserOnboardingService: ProviderUserOnboardingService,
        private userOnboardingService: UserOnboardingService,
        private dataProviderService: DataProviderService,
        private filterService: AmassFilterService,
        private route: ActivatedRoute,
        private router: Router,
        public dialog: MatDialog,
        private principal: Principal,
        private authService: AuthServerProvider,
        public _DomSanitizer: DomSanitizer,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private signinModalService: SigninModalService,
        @Inject(WINDOW) private window: Window,
        public snackBar: MatSnackBar,
        private uppyService: UppyService,
        private csrfService: CSRFService,
        private el: ElementRef,
        private stateService: StateStorageService,
        private cookieService: CookieService
    ) {
        this.routerOutletRedirect = '';
        this.domSanitizer = DomSanitizer;
        this.formSubmitted = false;
        this.hideMarketingSection = true;
        this.hideDocumentSection = true;
		this.hideSamplesSection = true;
		this.hideReportSection = true;
    }
    openUppyDialog(events: any) {
        //this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        const dialogRef = this.dialog.open(UppyComponent, {
            width: '500px',
            height: '3000px'
        });
        // dialogRef.afterClosed().subscribe(result => {
        // 	console.log('The dialog was closed');
        //   });
    }
    ngOnInit() {
        _that = this;
        this.csrf = this.csrfService.getCSRF();
        this.filterService.setAuthorisedUrl();
        this.trackownerOrganizationCity = 0;
        this.trackownerOrganizationState = 0;
        this.trackownerOrganizationCountry = 0;
        this.trackownerOrganizationYearfound = 0;
        this.trackownerOrganizationHeadcount = 0;
        this.trackownerOrganizationHeadcountBucket = 0;
        this.trackownerOrganizationHeadcountNumber = 0;
        this.topicsCount = 0;
        this.isLoading = true;
        this.isPendingUnlock = false;
        this.isPendingExpand = false;
        this.valueChanged = false;
        this.tabIndex = 0;
        this.basicExpansionPanel = true;
        this.adminExpansionPanel = false;
        this.categorizationsExpansionPanel = false;
        this.dataChecklistPanel = true;
        this.dataProductsPanel = true;
        this.documentExpansionPanel = true;
        this.relevantEntitiesPanel = true;
        if (this.dataResourceLinkID) {
            this.emptyField = false;
        } else {
            this.emptyField = true;
        }
        this.route.queryParams.subscribe(params => {
            if (params['pending-unlock']) {
                this.isPendingUnlock = true;
            }

            if (params['pending-expand']) {
                this.isPendingExpand = true;
            }
        });
        this.subscription = this.route.params.subscribe(params => {
            this.recordID = params['recordID'];
            //this.load(params['recordID']);
            this.trackLoad = this.recordID;
        });
        this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
        this.relevantArticlePage = 0;
        this.relevantArticleLinks = {
            last: 0
        };
        this.authoredArticlePage = 0;
        this.authoredArticleLinks = {
            last: 0
        };
        this.mdTooltipDelay = 500;
        this.amassSettingService.setAuthorisedUrl().subscribe(data => {
            const newObj = data;
            console.log(newObj);
            this.providerRecordID = newObj && newObj[0] && newObj[0]['providerRecordID'] ? newObj[0]['providerRecordID'] : '';
            this.providerID = newObj && newObj[0] && newObj[0]['providerID'] ? newObj[0]['providerID'] : '';
            console.log('ID', this.providerID);
            this.userID = newObj && newObj[0] && newObj[0]['userID'] ? newObj[0]['userID'] : '';
            console.log(this.providerRecordID, this.providerID, data,this.userID);
            this.userOnboardingService.providerLogo(this.providerID).subscribe(data => {
                this.providerUserModel['logo'] = data?.logo;
                console.log('Logo', this.providerUserModel);
            });
            this.load();
        });

        this.dataCategories = [];
		this.filterService.getAllDataCategories().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.datacategory !== '' && listItems[i]._source.datacategory !== 'DATA_CATEGORY_NULL') {
					this.dataCategories.push({
						value: {
                            id: listItems[i]._source.id,
                            desc: listItems[i]._source.datacategory,
                            //locked: listItems[i]._source.locked
                        },
                        label: listItems[i]._source.datacategory
						// img: listItems[i]._source.logo ? listItems[i]._source.logo.logo : null
					});
				}
			}
            this.dataProductsFields[0].templateOptions.options = this.dataCategories;
		});

        this.geographies = [];
		this.filterService.getLookupCodes('GEOGRAPHICAL_REGION').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.geographies.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataProductsFields[1].templateOptions.options = this.geographies;
            console.log(this.geographies);
		});

        this.assetClasses = [];
		this.filterService.getRelatedAssetClass().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.assetClasses.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.description
					},
					label: listItems[i]._source.description
				});
			}
			this.dataProductsFields[3].templateOptions.options = this.assetClasses;
            console.log(this.assetClasses);
		});

        this.sectors = [];
		this.filterService.getLookupCodes('RLVNT_SECTOR').subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.sectors.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode,
						// locked: listItems[i]._source.locked ? listItems[i]._source : false
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataProductsFields[4].templateOptions.options = this.sectors;
            console.log(this.sectors);
		});

        this.userOnboardingService.checkUser().subscribe(data => {
            // console.log(data, data.geographies);
            this.oldRegionsValue = data.geographies;
            // console.log(this.oldRegionsValue);
            this.providerUserOnboardingObj = data;
            this.providerUserModel = data;
            this.partnershipValue = data.partnership;
            this.userID = data.id;
            console.log('PARTNERS', this.providerUserModel, this.partnershipValue);
            if(this.partnershipValue && this.partnershipValue.id) {
                this.providerUserModel.partnersCheckList = true;
            } else {
                this.providerUserModel.partnersCheckList = false;
            }
            if (this.providerUserModel.categories) {
                const dataCategories = [];
                for (const obj in this.providerUserModel.categories) {
                    if (obj) {
                        dataCategories.push({
                            id: this.providerUserModel.categories[obj].id,
                            desc: this.providerUserModel.categories[obj].datacategory
                        });
                    }
                }
                delete this.providerUserModel.categories;
                this.providerUserModel.categories = dataCategories;
            }
            if (this.providerUserModel.geographies) {
                const geographicalFocus = [];
                for (const obj in this.providerUserModel.geographies) {
                    if (obj) {
                        geographicalFocus.push(this.providerUserModel.geographies[obj].id)
                    }
                    console.log('Pushed', geographicalFocus);
                }
                delete this.providerUserModel.geographies;
                this.providerUserModel.geographies = geographicalFocus;
            }
            if (this.providerUserModel.assetClasses) {
                const providerRelevantAssetClass = [];
                for (const obj in this.providerUserModel.assetClasses) {
                    if (obj) {
                        providerRelevantAssetClass.push({
                            id: this.providerUserModel.assetClasses[obj].id,
                            desc: this.providerUserModel.assetClasses[obj].description
                        });
                    }
                }
                delete this.providerUserModel.assetClasses;
                this.providerUserModel.assetClasses = providerRelevantAssetClass;
            }
            if (this.providerUserModel.sectors) {
                const relevantSectors = [];
                for (const obj in this.providerUserModel.sectors) {
                    if (obj) {
                        relevantSectors.push({
                            id: this.providerUserModel.sectors[obj].id,
                            desc: this.providerUserModel.sectors[obj].description
                        });
                    }
                }
                delete this.providerUserModel.sectors;
                this.providerUserModel.sectors = relevantSectors;
            }
        });

        // if (this.basicModel.providerPartners) {
        //     this.providerPartnersCount = this.dataProvider.providerPartners.length;
        //     this.relatedProvidersFields[0].templateOptions.label = 'Provider Data Partners ('+this.providerPartnersCount+'):';
        //     const providerPartners = [];
        //     for (const obj in this.dataProvider.providerPartners) {
        //         if (obj) {
        //             const partnerName = !this.dataProvider.providerPartners[obj].locked ? this.dataProvider.providerPartners[obj].providerName : 'Provider Locked';
        //             providerPartners.push({
        //                 id: this.dataProvider.providerPartners[obj].id,
        //                 providerName: partnerName
        //             });
        //         }
        //     }
        //     delete this.dataProvider.providerPartners;
        //     this.dataProvider.providerPartners = providerPartners;
        // }

        this.dataLinks = [];
        this.filterService.getDataLinks().subscribe(data => {
            const listItems = data ? data['hits']['hits'] : [];
            for (let i = 0; i < listItems.length; i++) {
                this.dataLinks.push({
                    value: {
                        id: listItems[i]._source.id,
                        link: listItems[i]._source.link
                    },
                    label: listItems[i]._source.link
                });
            }
            this.basicLinkFields[0].templateOptions.options = this.dataLinks;
        });

        this.dataFiles = [];
        this.filterService.getAllFiles().subscribe(data => {
            const listItems = data ? data['hits']['hits'] : [];
            for (let i = 0; i < listItems.length; i++) {
                this.dataFiles.push({
                    value: {
                        id: listItems[i]._source.id,
                        fileName: listItems[i]._source.fileName
                    },
                    label: listItems[i]._source.fileName
                });
            }
            this.basicFileFields[0].templateOptions.options = this.dataFiles;
        });

        // this.resourceMarketplaceStatus = [];
        // this.filterService.getLookupCodes('MARKETPLACE_STATUS').subscribe(data => {
        //     const listItems = data ? data['hits']['hits'] : [];
        //     this.resourceMarketplaceStatus.push({ label: 'Select', value: null });
        //     for (let i = 0; i < listItems.length; i++) {
        //         this.resourceMarketplaceStatus.push({
        //             value: listItems[i]._source.id,
        //             label: listItems[i]._source.lookupcode
        //         });
        //     }
        //     this.adminFields[1].templateOptions.options = this.resourceMarketplaceStatus;
        // });

        // this.resourceWorkFlows = [];
        // this.filterService.getLookupCodes('WORKFLOW').subscribe(data => {
        //     const listItems = data ? data['hits']['hits'] : [];
        //     for (let i = 0; i < listItems.length; i++) {
        //         this.resourceWorkFlows.push({
        //             value: {
        //                 id: listItems[i]._source.id,
        //                 desc: listItems[i]._source.lookupcode
        //             },
        //             label: listItems[i]._source.lookupcode
        //         });
        //     }
        //     this.adminFields[2].templateOptions.options = this.resourceWorkFlows;
        // });

        // this.resourceResearchMethods = [];
        // this.filterService.getLookupCodes('RESEARCH_METHOD').subscribe(data => {
        //     const listItems = data ? data['hits']['hits'] : [];
        //     for (let i = 0; i < listItems.length; i++) {
        //         this.resourceResearchMethods.push({
        //             value: {
        //                 id: listItems[i]._source.id,
        //                 desc: listItems[i]._source.lookupcode
        //             },
        //             label: listItems[i]._source.lookupcode
        //         });
        //     }
        //     this.adminFields[3].templateOptions.options = this.resourceResearchMethods;
        // });

        // this.resourceIEIStatus = [];
        // this.filterService.getLookupCodes('IEI_STATUS').subscribe(data => {
        //     const listItems = data ? data['hits']['hits'] : [];
        //     this.resourceIEIStatus.push({ label: 'Select', value: null });
        //     for (let i = 0; i < listItems.length; i++) {
        //         this.resourceIEIStatus.push({
        //             value: listItems[i]._source.id,
        //             label: listItems[i]._source.lookupcode
        //         });
        //     }
        //     this.adminFields[4].templateOptions.options = this.resourceIEIStatus;
        // });

        // this.resourcePriority = [];
        // this.filterService.getLookupCodes('PRIORITY').subscribe(data => {
        //     const listItems = data ? data['hits']['hits'] : [];
        //     this.resourcePriority.push({ label: 'Select', value: null });
        //     for (let i = 0; i < listItems.length; i++) {
        //         this.resourcePriority.push({
        //             value: listItems[i]._source.id,
        //             label: listItems[i]._source.lookupcode
        //         });
        //     }
        //     this.adminFields[5].templateOptions.options = this.resourcePriority;
        // });

        this.resourceTopics = [];
        this.filterService.getDataTopics().subscribe(data => {
            const listItems = data ? data['hits']['hits'] : [];
            for (let i = 0; i < listItems.length; i++) {
                this.resourceTopics.push({
                    value: {
                        id: listItems[i]._source.id,
                        desc: listItems[i]._source.topicName
                    },
                    label: listItems[i]._source.topicName
                });
            }
            this.categorizationsFields[0].templateOptions.options = this.resourceTopics;
        });

        this.resourcePurposes = [];
        this.filterService.getDataResourcePurpose().subscribe(data => {
            const listItems = data ? data['hits']['hits'] : [];
            for (let i = 0; i < listItems.length; i++) {
                this.resourcePurposes.push({
                    value: {
                        id: listItems[i]._source.id,
                        desc: listItems[i]._source.purpose
                    },
                    label: listItems[i]._source.purpose
                });
            }
            this.categorizationsFields[1].templateOptions.options = this.resourcePurposes;
        });

        this.resourcePurposeType = [];
        this.filterService.getDataResourcePurposeType().subscribe(data => {
            const listItems = data ? data['hits']['hits'] : [];
            for (let i = 0; i < listItems.length; i++) {
                if (listItems[i]._source.purposeType !== '') {
                    this.resourcePurposeType.push({
                        value: {
                            id: listItems[i]._source.id,
                            desc: listItems[i]._source.purposeType
                        },
                        label: listItems[i]._source.purposeType
                    });
                }
            }
            this.categorizationsFields[2].templateOptions.options = this.resourcePurposeType;
        });

        this.relevantOrganizations = [];
        this.filterService.getAllOrganizations().subscribe(d => {
            const listItems = d && d['hits'] ? d['hits']['hits'] : [];
            for (let i = 0; i < listItems.length; i++) {
                this.relevantOrganizations.push({
                    id: listItems[i]._source.id,
                    name: listItems[i]._source.name
                });
            }
            this.relevantEntitiesFields[0].templateOptions.options = this.relevantOrganizations;
        });
    }

    updateChecklist(event: any, item: any) {
        // console.log(event, item, this.oldRegionsValue);
        this.providerUserOnboardingObj.geographies = this.oldRegionsValue;
        if (event.checked === false && item === 'partnersCheckList') {
            this.providerUserOnboardingObj.partnership = null;
        } else if(event.checked === true && item === 'partnersCheckList') {
            this.providerUserOnboardingObj.partnership = { id: environmentDev.dev ? 2151 : 2093 };
        }
        // console.log(this.providerUserOnboardingObj);
        this.providerUserOnboardingService.providerUserOnboarding(this.providerUserOnboardingObj,'checkList').subscribe(data => {
            console.log('Updated Data', data);
        });
    }
    updateLinkChecklist(event: any, item: any) {
        // console.log(event);
        this.providerUserOnboardingObj.geographies = this.oldRegionsValue;
        if (item === 'providersPage') {
            this.providerUserOnboardingObj.providersPage = true;
            this.providerUserOnboardingService.providerUserOnboarding(this.providerUserOnboardingObj,'checkList').subscribe(data => {
                console.log('Updated Data', data);
            });
        } else if (item === 'investorsPage') {
            this.providerUserOnboardingObj.investorsPage = true;
            this.providerUserOnboardingService.providerUserOnboarding(this.providerUserOnboardingObj,'checkList').subscribe(data => {
                console.log('Updated Data', data);
            });
        } else if (item === 'userOnboarding') {
            this.providerUserOnboardingObj.userOnboarding = true;
            this.providerUserOnboardingService.providerUserOnboarding(this.providerUserOnboardingObj,'checkList').subscribe(data => {
                console.log('Updated Data', data);
            });
        } else if (item === 'userRoleOnboarding') {
            this.providerUserOnboardingObj.userRoleOnboarding = true;
            this.providerUserOnboardingService.providerUserOnboarding(this.providerUserOnboardingObj,'checkList').subscribe(data => {
                console.log('Updated Data', data);
            });
            const element = document.getElementById("providerUserOnboardingForm");
            element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
        } else if (item === 'dataProfile') {
            this.providerUserOnboardingObj.dataProfile = true;
            this.providerUserOnboardingService.providerUserOnboarding(this.providerUserOnboardingObj,'checkList').subscribe(data => {
                console.log('Updated Data', data);
            });
        }


    }

    signup(events: any) {
        console.log(events);
        if (!this.isAuthenticated() && this.providerUserOnboardingObj.confirmed === false) {
            this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            });
            this.mdDialogRef.componentInstance.companyName = 'ROLE_PROVIDER';
            this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        }
    }

    isAuthenticated() {
		return this.principal.isAuthenticated();
	}

    deleteMarketingLiterature(obj, index) {
		const deleteLinkObj = obj.resourceID;
		const resourcesId=obj.id;
		this.marketingLiteratureList.map(item => {
            if(item.id === obj.id) {
                const itemObject = item;
                this.dataResourceService.deleteResourceLink(itemObject, obj.resourceID, this.providerID, this.marketingLiteratureResource).subscribe(data => {
                    if(data) {
                        this.marketingLiteratureList.splice(index, 1);
                    }
                });
            }
		});
		// this.dataDictionaryList.splice(index, 1);
		// this.samplesAndExamplesList.splice(index, 1);
		// this.reportsAnalysesList.splice(index, 1);
	}

	deleteDocumentationDataDictionary(obj, index) {
		const deleteLinkObj = obj.resourceID;
		const resourcesId=obj.id;
		this.dataDictionaryList.map(item => {

            if(item.id === obj.id) {
                const itemObject = item;
                this.dataResourceService.deleteResourceLink(itemObject, obj.resourceID, this.providerID, this.marketingLiteratureResource).subscribe(data => {
                    if(data) {
                        this.dataDictionaryList.splice(index, 1);
                    }
                });
            }
        });
	}
	deleteSamplesExamples(obj,index) {
		const deleteLinkObj = obj.resourceID;
		const resourcesId=obj.id;
		this.samplesAndExamplesList.map(item => {
            if(item.id === obj.id) {
                const itemObject = item;
                this.dataResourceService.deleteResourceLink(itemObject, obj.resourceID, this.providerID, this.marketingLiteratureResource).subscribe(data => {
                    if(data) {
                        this.samplesAndExamplesList.splice(index, 1);
                    }
                });
            }
		});
	}
	deleteReportAnalyses(obj,index) {
		const deleteLinkObj = obj.resourceID;
		const resourcesId=obj.id;
		this.reportsAnalysesList.map(item => {
            if(item.id === obj.id) {
                const itemObject = item;
                this.dataResourceService.deleteResourceLink(itemObject, obj.resourceID, this.providerID, this.marketingLiteratureResource).subscribe(data => {
                    if(data) {
                        this.reportsAnalysesList.splice(index, 1);
                    }
                });
            }
		});
	}

    load() {
		this.dataProviderService.findByIdEditForm(this.providerRecordID).subscribe(dataProvider => {
			// this.loaderService.display(false);
			// this.dataProvider = dataProvider['hits'].total ? dataProvider['hits'].hits[0]._source : null;
			// this.dataProviderID = this.dataProvider ? this.dataProvider.id : null;
            // this.completeness = this.dataProvider.completeness;
            // this.isLoading = false;
			// this.constructFormModel(this.dataProvider);
			// this.companyDetailsModel = this.dataProvider;
            if(this.providerID){
                this.filterService.findByResourceId(this.providerID).subscribe(data => {
                    if(data['hits'] && data['hits'].hits) {
                        for (let i = 0; i < data['hits'].hits.length; i++) {
                            const marketingResourceObj = data['hits'].hits[i]._source;
                            this.marketingLiteratureResource.push(marketingResourceObj);
                            // if (this.resourceObj.link !== null) {
                            // 	this.resourceLink.push(this.resourceObj.link);
                            // }
                            // console.log('Link', this.resourceLink);
                        }
                    }
                        this.marketingLiteratureResource.forEach((item,index) => {
                        //	this.itemID = item.id;
                        //	console.log('Link', item.link);
                            const obj = item;
                            if(obj && obj.purposes[0] && obj.purposes[0].purpose === 'Description & Details') {
                                if(obj.link) {
                                    obj.link['resourceID'] = obj.id;
                                    obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
                                    this.marketingLiteratureList.push(obj.link);
                                }
                                if(obj.file) {
                                    obj.file['resourceID'] = obj.id;
                                    obj.file['filesName'] = obj.file && obj.file.fileName ? obj.file.fileName : null;
                                    this.marketingLiteratureList.push(obj.file);
                                }
                            }
                            if(obj && obj.purposes[0] && obj.purposes[0].purpose === 'Documentation or Data Dictionary') {
                                if(obj.link) {
                                    obj.link['resourceID'] = obj.id;
                                    obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
                                    this.dataDictionaryList.push(obj.link);
                                }
                                if(obj.file) {
                                    obj.file['resourceID'] = obj.id;
                                    obj.file['filesName'] = obj.file && obj.file.fileName ? obj.file.fileName : null;
                                    this.dataDictionaryList.push(obj.file);
                                }
                            }
                            if(obj && obj.purposes[0] && obj.purposes[0].purpose === 'Snapshot') {
                                if(obj.link) {
                                    obj.link['resourceID'] = obj.id;
                                    obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
                                    this.samplesAndExamplesList.push(obj.link);
                                }
                                if(obj.file) {
                                    obj.file['resourceID'] = obj.id;
                                    obj.file['filesName'] = obj.file && obj.file.fileName ? obj.file.fileName : null;
                                    this.samplesAndExamplesList.push(obj.file);
                                }
                            }
                            if(obj && obj.purposes[0] && obj.purposes[0].purpose === 'Research & Analysis') {
                                if(obj.link) {
                                    obj.link['resourceID'] = obj.id;
                                    obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
                                    this.reportsAnalysesList.push(obj.link);
                                }
                                if(obj.file) {
                                    obj.file['resourceID'] = obj.id;
                                    obj.file['filesName'] = obj.file && obj.file.fileName ? obj.file.fileName : null;
                                    this.reportsAnalysesList.push(obj.file);
                                }
                            }
                        });
                });
             }
             else if(this.userID && !this.providerID){
        this.filterService.findByOperatorResourceId(this.userID).subscribe(data => {
            if(data['hits'] && data['hits'].hits) {
                for (let i = 0; i < data['hits'].hits.length; i++) {
                    const marketingResourceObj = data['hits'].hits[i]._source;
                    this.marketingLiteratureResource.push(marketingResourceObj);
                    // if (this.resourceObj.link !== null) {
                    // 	this.resourceLink.push(this.resourceObj.link);
                    // }
                    // console.log('Link', this.resourceLink);
                }
            }
                this.marketingLiteratureResource.forEach((item,index) => {
                //	this.itemID = item.id;
                //	console.log('Link', item.link);
                    const obj = item;
                    if(obj && obj.purposes[0] && obj.purposes[0].purpose === 'Description & Details') {
                        if(obj.link) {
                            obj.link['resourceID'] = obj.id;
                            obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
                            this.marketingLiteratureList.push(obj.link);
                        }
                        if(obj.file) {
                            obj.file['resourceID'] = obj.id;
                            obj.file['filesName'] = obj.file && obj.file.fileName ? obj.file.fileName : null;
                            this.marketingLiteratureList.push(obj.file);
                        }
                    }
                    if(obj && obj.purposes[0] && obj.purposes[0].purpose === 'Documentation or Data Dictionary') {
                        if(obj.link) {
                            obj.link['resourceID'] = obj.id;
                            obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
                            this.dataDictionaryList.push(obj.link);
                        }
                        if(obj.file) {
                            obj.file['resourceID'] = obj.id;
                            obj.file['filesName'] = obj.file && obj.file.fileName ? obj.file.fileName : null;
                            this.dataDictionaryList.push(obj.file);
                        }
                    }
                    if(obj && obj.purposes[0] && obj.purposes[0].purpose === 'Snapshot') {
                        if(obj.link) {
                            obj.link['resourceID'] = obj.id;
                            obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
                            this.samplesAndExamplesList.push(obj.link);
                        }
                        if(obj.file) {
                            obj.file['resourceID'] = obj.id;
                            obj.file['filesName'] = obj.file && obj.file.fileName ? obj.file.fileName : null;
                            this.samplesAndExamplesList.push(obj.file);
                        }
                    }
                    if(obj && obj.purposes[0] && obj.purposes[0].purpose === 'Research & Analysis') {
                        if(obj.link) {
                            obj.link['resourceID'] = obj.id;
                            obj.link['linkName'] = obj.link && obj.link.link ? obj.link.link : null;
                            this.reportsAnalysesList.push(obj.link);
                        }
                        if(obj.file) {
                            obj.file['resourceID'] = obj.id;
                            obj.file['filesName'] = obj.file && obj.file.fileName ? obj.file.fileName : null;
                            this.reportsAnalysesList.push(obj.file);
                        }
                    }
                });
                // console.log('Data Relevant Links', this.dataRelevantLinks);
                // console.log('Data Relevant Files', this.dataRelevantFiles);
            // 	this.resourceModel = this.dataResources;
        });
    }
        });
    }

    // openLinkDialog(events: any) {
    // 	if(events === 'editLink') {
    // 		if(this.linkID && !this.linkMapId && !this.editLinkMapId) {
    // 			this.filterService.findByLinkId(this.linkID).subscribe(linkData => {
    // 				this.loaderService.display(false);
    // 				this.dataLinksList = linkData['hits'] ? linkData['hits'].hits[0]._source : null;
    //             });
    //         } else if(this.linkMapId && !this.editLinkMapId && !this.linkID) {
    // 			this.filterService.findByLinkId(this.linkMapId).subscribe(linkData => {
    // 				this.loaderService.display(false);
    // 				this.dataLinksList = linkData['hits'] ? linkData['hits'].hits[0]._source : null;
    //             });
    // 		} else if(this.editLinkMapId && !this.linkID && !this.linkMapId) {
    // 			this.filterService.findByLinkId(this.editLinkMapId).subscribe(linkData => {
    // 				this.loaderService.display(false);
    // 				this.dataLinksList = linkData['hits'] ? linkData['hits'].hits[0]._source : null;
    //             });
    // 		} else if(this.dataResourceLinkID) {
    // 			this.filterService.findByLinkId(this.dataResourceLinkID).subscribe(linkData => {
    // 				this.loaderService.display(false);
    // 				this.dataLinksList = linkData['hits'] && linkData['hits'].hits[0] ?  linkData['hits'].hits[0]._source : null;
    //             });
    // 		}
    // 		setTimeout(() => {
    // 			this.resDialogRef = this.dialog.open(DataProviderLinkAdminFormComponent, {
    // 				width: '1000px',
    // 				disableClose: true
    //             });
    //             this.resDialogRef.componentInstance.defaultValue = 'editLink';
    //             this.resDialogRef.componentInstance.basicModel = this.dataLinksList;
    //             this.resDialogRef.afterClosed().subscribe(result => {
    // 				if(result) {
    //                     this.emptyField = false;
    //                     this.valueChanged = true;
    //                     // if((this.dataResourceModel.link && this.dataResourceModel.link.link) === this.dataResourceModel.name) {
    //                     //     this.basicModel = {
    //                     //         ...this.basicModel,
    //                     //         link: {id:result.id, link: result.link},
    //                     //         name: result.link
    //                     //     }
    //                     // }
    // 					this.basicModel = {
    // 						...this.basicModel,
    //                         link: {id:result.id, link: result.link},
    //                         name: result.link
    //                     }
    //                     this.editLinkMapId = result.id;
    // 			    }
    // 			});
    // 		}, 500);
    // 	} else if(events === 'addLink') {
    // 		setTimeout(() => {
    // 			this.resDialogRef = this.dialog.open(DataProviderLinkAdminFormComponent, {
    // 				width: '1000px',
    // 				disableClose: true
    //             });
    //             this.resDialogRef.afterClosed().subscribe(result=> {
    // 				if(result) {
    // 					this.emptyField = false;
    // 					this.basicModel = {
    // 						...this.basicModel,
    //                         link: {id:result.id, link: result.link},
    //                         name: result.link
    //                     }
    //                     this.linkMapId = result.id;
    // 			    }
    // 			});
    // 			//this.orgDialogRef.componentInstance.basicProvider = this.dataProviderId;
    // 		}, 500);
    // 	}
    // }

    getLinkImgUrl(desc) {
        if (desc) {
            const fileName = desc.toLowerCase().replace(' ', '_');
            const link = 'content/images/' + fileName + '.png';
            return link;
        }
    }

    providerProfileTab(event: MatTabChangeEvent) {
        /*  this.profileTabLabel = 'Provider Profile - Tab ' + event.tab.textLabel;
		this.ProfileCategory = 'Provider Profile ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent(this.ProfileCategory, 'Tab Link Clicked', this.profileTabLabel); */
    }

    providerFollow(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    unlockProviders(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    requestMoreInfo(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    providerPartnerTab(event: MatTabChangeEvent) {
        this.profileTabLabel = 'Provider Profile -  ' + event.tab.textLabel;
        this.googleAnalyticsService.emitEvent('Partner Provider', 'Internal Link Clicked', this.profileTabLabel);
    }

    dataFieldLink(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    profileWebsite(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    profileArticle(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    samplesTab(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    addLinks(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    // load(recordID) {
    // 	this.dataResourceService.findByIdForOrganizationForm(recordID).subscribe(dataOrganization => {
    // 		this.loaderService.display(false);
    //         this.dataOrganization = dataOrganization['hits'].total ? dataOrganization['hits'].hits[0]._source : null;
    // 		this.isLoading = false;
    // 		this.constructFormModel(this.dataOrganization);
    //         this.basicModel = this.dataOrganization;
    //         this.providerID = this.dataOrganization ? this.dataOrganization.id : null;
    // 	}, error => {
    // 		if (error.status === 401) {
    //             this.stateService.storeUrl(null);
    //             this.router.navigate(['']);
    // 			this.signinModalService.openDialog('Session expired! Please re-login.');
    // 		}
    // 		this.dataOrganization = null;
    // 		this.isLoading = false;
    // 	});
    //    // this.loaderService.display(false);
    // }

    constructFormModel(resourceTrack) {
        console.log(resourceTrack);
        this.trackname = resourceTrack.name ? resourceTrack.name.length : 0;
    }

    previousState() {
        this.window.history.back();
    }

    // openConfirmationDialog(nextState?: RouterStateSnapshot) {
    //     console.log(nextState);
    //     //const redirect = nextState.url;
    //     //this.dialog.closeAll();
    //     this.dialogRef = this.dialog.open(ConfirmDialogComponent, {
    //         disableClose: false
    //     });
    //     this.dialogRef.componentInstance.confirmMessage = 'You have some unsaved edits in this form.';
    //     this.dialogRef.componentInstance.button = {
    //         leave: true,
    //         confirm: true,
    //         leaveText: 'Leave and Don\'t Save',
    //         confirmText: 'Save and Submit'
    //     };
    //     this.dialogRef.afterClosed().subscribe(result => {
    //         if (result) {
    //             // do confirmation actions
    //             if (result === 'leave') {
    //                 this.dialog.closeAll();
    //             }

    //             if (result === 'save') {
    //                 //console.log('Popup', this.basicProvider, this.action);
    //                 this.submitResourceAdmin(this.basicModel);
    //                 this.dialog.closeAll();
    //             }
    //             this.dialogRef = null;
    //         }
    //     });
    // 	return false;
    // }

    openConfirmationDialog(nextState?: RouterStateSnapshot) {
        if (nextState) {
            this.redirect = nextState.url;
        }
        //this.dialog.closeAll();
        this.dialogRef = this.dialog.open(ConfirmDialogComponent, {
            disableClose: false
        });
        this.dialogRef.componentInstance.confirmMessage = 'You have some unsaved edits in this form.';
        this.dialogRef.componentInstance.button = {
            leave: true,
            confirm: true,
            leaveText: "Leave and Don't Save",
            confirmText: 'Save and Submit'
        };
        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // do confirmation actions
                if (result === 'leave') {
                    if (this.redirect === '/') {
                        this.authService.logout().subscribe();
                        this.formSubmitted = true;
                        this.principal.authenticate(null);
                        this.stateService.storeUrl(null);
                        this.router.navigate(['../../']);
                    } else {
                        this.formSubmitted = true;
                        if (this.redirect) {
                            this.router.navigate([this.redirect]);
                            this.dialog.closeAll();
                        } else if (this.principal.isAuthenticated()) {
                            this.stateService.storeUrl(null);
                            if (this.redirect) {
                                this.router.navigate([this.redirect]);
                                this.dialog.closeAll();
                            }
                            this.dialog.closeAll();
                        } else {
                            this.stateService.storeUrl(null);
                            this.router.navigate(['']);
                            this.dialog.closeAll();
                        }
                    }
                }
                if (result === 'save') {
                    //console.log('Popup', this.basicProvider, this.action);
                    //this.submitResourceAdmin(this.basicModel);
                    this.dialog.closeAll();
                }
                this.dialogRef = null;
            }
        });
        return false;
    }

    trackByIndex(index) {
        return index;
    }
    closeSignUpDialog() {
        if (this.valueChanged === false) {
            // this.adminDialogRef.close();
        }
        if (this.valueChanged === true) {
            if (this.formSubmitted) {
                this.openConfirmationDialog();
            } else {
                if (this.formSubmitted === true) {
                    this.openConfirmationDialog();
                } else if (this.formSubmitted === false) {
                    this.openConfirmationDialog();
                }
            }
        }
    }

    submitProviderUser(model, panelName?: string, currentPanel?: string, nextPanel?: string, tabIndex?: number, form?: string, skipRedirect?: boolean) {
		const formName = panelName ? panelName + '' : 'Form';
        const submitMessage = 'Thank you for your submission. We will be in touch soon. In the meantime, please have a look around.';
        const filterArr = Array.prototype.filter;
        console.log(model, this.providerUserOnboardingObj);
        if (form && this[form].valid) {
			const autoCompleteFields = this.el.nativeElement.querySelectorAll('#' + form + ' .ui-autocomplete-input');
			let invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
            const filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                return node.value === '';
            });
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
			} else {
                // delete model['id'];
				// model['id'] = this.providerID;
                this.providerUserOnboardingObj.assetClasses = model.assetClasses;
                this.providerUserOnboardingObj.categories = model.categories;
                this.providerUserOnboardingObj.dataRequirements = model.dataRequirements;
                this.providerUserOnboardingObj.geographies = model.geographies;
                this.providerUserOnboardingObj.sectors = model.sectors;
                this.providerUserOnboardingObj.userRoleOnboarding = true;
				this.progressSpinner = true;
                this.providerUserOnboardingService.providerUserOnboarding(this.providerUserOnboardingObj,'providerUserOnboardingForm').subscribe(data => {
                    if (data) {
						this.progressSpinner = false;
                        this.formSubmitted = true;
                        this.providerUserModelID = data.id;
                        this.providerUserModelRecordID = data.recordID;
                        this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                        console.log(this.providerUserModelID, this.providerUserModelRecordID);
                        if (currentPanel && currentPanel !== '') {
                            this[currentPanel] = false;
                            if (nextPanel && nextPanel !== '') {
                                this[nextPanel] = true;
                            }
                            if (tabIndex) {
                                this.tabIndex = tabIndex;
                            }
                        } else {
                            if (skipRedirect) {
                                return true;
                            }
                            this.stateService.storeUrl(null);
                            if (data.userRoleOnboarding === true) {
                                this.router.navigate(['/investors'], { relativeTo: this.route });
                            }
                        }
                    } else {
                        // console.log(data);
						this.formSubmitted = false;
						this.progressSpinner = false;
                        this.openSnackBar('' + formName + ' failed to save!', 'Close');
                    }
                }, error => {
					if (error.status === 401) {
						this.formSubmitted = false;
						this.progressSpinner=false;
						this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
					} else {
						this.formSubmitted = false;
						this.progressSpinner=false;
						this.openSnackBar('' + formName + ' failed to save!', 'Close');
					}
                });
            }
        } else {
            let invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
            let autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
            let filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                return node.required && node.value === '';
            });
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else if (invalidElements.length > 0) {
                if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                    invalidElements[0].querySelectorAll('input')[0].focus();
                } else {
                    setTimeout(() => {
                        invalidElements[0].focus();
                    }, 1000);
                }
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else {
                this.tabIndex = this.tabIndex === 0 ? 1 : 0;
                setTimeout(() => {
                    invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
                    autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
                    filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                        return node.required && node.value === '';
                    });
                    if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                        setTimeout(() => {
                            filteredAutoCompleteFields[0].focus();
                        }, 1000);
                        this.openSnackBar('Please fill all the required fields!', 'Close');
                        this.expandFormPanels();
                    } else if (invalidElements.length > 0) {
                        if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                            invalidElements[0].querySelectorAll('input')[0].focus();
                        } else {
                            setTimeout(() => {
                                invalidElements[0].focus();
                            }, 1000);
                        }
                        this.openSnackBar('Please fill all the required fields!', 'Close');
                        this.expandFormPanels();
                    } else {
						// delete model['id'];
						// model['id'] = this.providerID;
                        this.providerUserOnboardingObj.assetClasses = model.assetClasses;
                        this.providerUserOnboardingObj.categories = model.categories;
                        this.providerUserOnboardingObj.dataRequirements = model.dataRequirements;
                        this.providerUserOnboardingObj.geographies = model.geographies;
                        this.providerUserOnboardingObj.sectors = model.sectors;
                        this.providerUserOnboardingObj.userRoleOnboarding = true;
						this.progressSpinner = true;
                        this.providerUserOnboardingService.providerUserOnboarding(this.providerUserOnboardingObj,'providerUserOnboardingForm').subscribe(data => {
                            if (data) {
								this.progressSpinner=false;
                                this.formSubmitted = true;
                                this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                                if (currentPanel && currentPanel !== '') {
                                    this[currentPanel] = false;
                                    if (nextPanel && nextPanel !== '') {
                                        this[nextPanel] = true;
                                    }
                                    if (tabIndex) {
                                        this.tabIndex = tabIndex;
                                    }
                                } else {
                                    if (skipRedirect) {
                                        return true;
                                    }
                                    this.stateService.storeUrl(null);
                                    if (data.userRoleOnboarding === true) {
                                        this.router.navigate(['/investors'], { relativeTo: this.route });
                                    }
                                }
                            } else {
								this.formSubmitted = false;
								this.progressSpinner = false;
                                this.openSnackBar('' + formName + ' failed to save!', 'Close');
                            }
                        }, error => {
							if (error.status === 401) {
								this.formSubmitted = false;
								this.progressSpinner = false;
								this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
							} else {
								this.formSubmitted = false;
								this.progressSpinner = false;
								this.openSnackBar('' + formName + ' failed to save!', 'Close');
							}
                        });
                    }
                }, 2000);
            }
        }
	}

    expandFormPanels() {
        this.basicExpansionPanel = true;
        this.adminExpansionPanel = true;
        this.categorizationsExpansionPanel = true;
        this.relevantEntitiesPanel = true;
    }

    formSubmit(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    formSave(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    basicInput($event) {
        const inputKey = $event;
        /* delete entry */
        if (inputKey.key === 'name') {
            if (inputKey.model.name.length < _that.trackname) {
                _that.googleAnalyticsService.emitEvent(
                    inputKey.templateOptions.attributes.trackCategory,
                    'Deleted',
                    inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames
                );
            } else if (inputKey.model.name.length > _that.trackname) {
                _that.googleAnalyticsService.emitEvent(
                    inputKey.templateOptions.attributes.trackCategory,
                    'Added',
                    inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames
                );
            }
        }
    }

    openSnackBar(message: string, action: string, delay?: number) {
        this.snackBar.open(message, action, {
            duration: delay ? delay : 10000,
            panelClass: ['blue-snackbar']
        });
    }

    submitBasic(model) {
        // console.log(model);
        this.basicExpansionPanel = false;
        this.adminExpansionPanel = true;
    }

    submitAdmin(model) {
        // console.log(model);
        this.adminExpansionPanel = false;
        this.categorizationsExpansionPanel = true;
    }

    submitCategorization(model) {
        // console.log(model);
        this.categorizationsExpansionPanel = false;
        this.tabIndex = 1;
    }

    submitRelatedEntities(model) {
        // console.log(model);
        this.relevantEntitiesPanel = true;
    }

    beforeSessionExpires(event) {
        if (event) {
            // this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }

    onExpireSession(event) {
        if (event) {
            this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
                this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
            });
        }
    }

    @HostListener('document:keyup', [])
    @HostListener('document:click', [])
    @HostListener('document:wheel', [])
    @HostListener('window:scroll', [])
    resetTimer() {
        this.authService.notifyUserAction();
    }

    @HostListener('window:popstate', [])
    onPopState() {
        this.cookieService.put('logoutClicked', '0');
    }

    @HostListener('window:beforeunload', ['$event'])
    preventUser($event) {
        $event.preventDefault();
        return $event.returnValue = "Are you sure you want to exit?";
    }
}
