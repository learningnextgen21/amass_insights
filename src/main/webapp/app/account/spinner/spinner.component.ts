import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent {
  @Input() show: boolean;
}
