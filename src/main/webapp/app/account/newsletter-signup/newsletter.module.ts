import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatamarketplaceSharedModule } from 'app/shared';
import { NewsLetterFooterComponent } from './newsletter-footer.component';
import { NewsLetterSignUpComponent } from './newsletter-signup.component';
import { NewsLetterSignUpService } from './newsletter-signup.service';

@NgModule({
	imports: [
        CommonModule,
		FormsModule,
        ReactiveFormsModule,
        DatamarketplaceSharedModule,
	],
	declarations: [
        NewsLetterSignUpComponent,
        NewsLetterFooterComponent
	],
	entryComponents: [
        NewsLetterSignUpComponent,
        NewsLetterFooterComponent
	],
    exports: [
        NewsLetterSignUpComponent,
        NewsLetterFooterComponent
    ],
	providers: [
		NewsLetterSignUpService
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceNewsLetterModule {}