import { Injectable, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { NewsLetterSignUpComponent } from './newsletter-signup.component';

@Injectable()
export class NewsLetterSignUpDialogService {
	private isOpen = false;
	constructor(
		public mdDialog: MatDialog
	) {}

	open(): void {
		const dialogRef = this.mdDialog.open(NewsLetterSignUpComponent, {
			width: '570px'
		});
	}
}
