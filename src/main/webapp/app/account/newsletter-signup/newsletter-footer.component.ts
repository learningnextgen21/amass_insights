import { Component, OnInit, AfterViewInit, Renderer2, ElementRef, Inject, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { Router } from '@angular/router';
import { JhiEventManager, JhiLanguageService } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';
import { SigninDialogComponent } from '../../dialog/signindialog.component';
import { NgForm } from '@angular/forms';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { CookieService } from 'ngx-cookie';
import { NewsLetterSignUpService } from './newsletter-signup.service';

@Component({
    selector: 'jhi-newsletter-footer',
    templateUrl: './newsletter-footer.component.html',
    styleUrls: ['./newsletter-signup.component.css']
})
export class NewsLetterFooterComponent implements OnInit{
    //@Input() footer: NewsLetterFooterComponent;
    error: string;
    errorEmailExists: string;
    errorUserExists: string;
    registerAccount: any;
    success: boolean;
    validationMessage: string;
    errorMsgValidation: any;
    @ViewChild(MatInput) input;
    modelValue: any;
    csrf: string;

    constructor(
        private languageService: JhiLanguageService,
        private eventManager: JhiEventManager,
        private elementRef: ElementRef,
        private newsLetterFooterForm: NewsLetterSignUpService,
        private renderer: Renderer2,
        private router: Router,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private cookieService: CookieService
	) {

	}

    ngOnInit() {
        this.success = false;
        this.registerAccount = {};
        this.validationMessage = null;
        this.errorMsgValidation = '';
        this.registerAccount.emailAddress = '';
    }

    change(message: any) {
        this.errorMsgValidation = message;
    }

    register(registerForm: NgForm) {
        if (registerForm.valid) {
            this.validationMessage = null;
            this.error = null;
            this.errorEmailExists = null;
                this.newsLetterFooterForm.save(this.registerAccount).subscribe(
                    () => {
                        this.success = true;
                    },
                    response => this.processError(response)
                );
        }
    }
    newsLetter(event) {
        if (event) {
            this.errorEmailExists = null;
            this.success = null;
        }
    }
    private processError(response) {
        // console.log('response2',response);
        this.success = null;
        if (response.status === 400 && response.error === 'email address already in use') {
            this.errorEmailExists = 'ERROR';
            this.renderer.selectRootElement('#applyEmail').focus();
        } else {
            this.error = 'ERROR';
            this.renderer.selectRootElement('#applyEmail').focus();
        }
    }

    signUp(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
}
