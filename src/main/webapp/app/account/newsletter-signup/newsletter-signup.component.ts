import { Component, OnInit, AfterViewInit, Renderer2, ElementRef, Inject, ViewChild, EventEmitter, Output } from '@angular/core';

import { Router } from '@angular/router';
import { JhiEventManager, JhiLanguageService } from 'ng-jhipster';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';

import { NewsLetterSignUpService } from './newsletter-signup.service';
import { SigninDialogComponent } from '../../dialog/signindialog.component';
import { NgForm } from '@angular/forms';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { CookieService } from 'ngx-cookie';

@Component({
    selector: 'jhi-newsletter-signup',
    templateUrl: './newsletter-signup.component.html',
    styleUrls: ['./newsletter-signup.component.css']
})
export class NewsLetterSignUpComponent implements OnInit {
    error: string;
    errorEmailExists: string;
    errorUserExists: string;
    registerAccount: any;
    success: boolean;
    validationMessage: string;
    errorMsgValidation: any;
    cookieStatus: any;
    mdDialogRef: MatDialogRef<any>;
    dialogRef: MatDialogRef<any>;
    @ViewChild(MatInput) input;
    modelValue: any;
    csrf: string;

    constructor(
        private languageService: JhiLanguageService,
        private eventManager: JhiEventManager,
        private elementRef: ElementRef,
        private newsLetterSignupForm: NewsLetterSignUpService,
        private renderer: Renderer2,
        private router: Router,
        public mdDialog: MatDialog,
        public dialog: MatDialog,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private cookieService: CookieService
	) {

    }

    ngOnInit() {
        this.success = false;
        this.registerAccount = {};
        this.validationMessage = null;
        this.errorMsgValidation = '';
        this.registerAccount.emailAddress = '';
        this.cookieStatus = this.cookieService.get('cookieconsent_status');
    }

    change(message: any) {
        this.errorMsgValidation = message;
    }

    register(registerForm: NgForm) {
        if (registerForm.valid) {
            this.validationMessage = null;
                this.error = null;
                this.errorEmailExists = null;
                    this.newsLetterSignupForm.save(this.registerAccount).subscribe(
                        () => {
                            this.success = true;
                        },
                        response => this.processError(response)
                    );
        } 
    }

    private processError(response) {
        this.success = null;
        if (response.status === 400 && response.error === 'email address already in use') {
            this.errorEmailExists = 'ERROR';
            this.renderer.selectRootElement('#applyEmail').focus();
        } else {
            this.error = 'ERROR';
            this.renderer.selectRootElement('#applyEmail').focus();
        }
    }
    
    signUp(events: any) {
        if(this.cookieStatus !== 'deny') {
            this.cookieService.put('Cancel', 'True');
            this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
        }
    }

    closeSignUpDialog(events: any) {
        this.mdDialog.closeAll();
        if(this.cookieStatus !== 'deny') {
            this.cookieService.put('Cancel', 'True');
            this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        }
    }
}