import { Injectable } from '@angular/core';
// import { Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class NewsLetterSignUpService {
    headers: any;

    constructor(private http: HttpClient) {
        this.headers = new HttpHeaders({
            'Content-Type': 'multipart/form-data',
            'cache-control': 'no-cache'
        });
    }

    save(account: any): Observable<any> {
        return this.http.post('api/ur/email-address', account);
    }

    signupForNewsletter(input: any): Observable<any> {
        const formData = new FormData();
        formData.append('w-field-field-21041-111950-630168-email', input.email);
        return this.http.post('https://app.mailjet.com/widget/iframe/2DW0/5tn', formData, { headers: this.headers });
    }
}