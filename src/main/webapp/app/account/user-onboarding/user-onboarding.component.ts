import { Component, OnInit, ElementRef, Inject, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig, FormlyTemplateOptions } from '@ngx-formly/core';
import { AmassFilterService, CSRFService, Principal, AuthServerProvider, StateStorageService, SigninModalService } from 'app/shared';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserOnboardingService } from './user-onboarding.service';
import { Router } from '@angular/router';
import { WINDOW } from '../../layouts/main/window.service';
import { NavbarDataService } from 'app/shared/auth/navbardata.service';
import { AmassSettingService } from '../settings/settings.service';

@Component({
    selector: 'jhi-user-onboarding',
    templateUrl: './user-onboarding.component.html',
    styleUrls: ['./user-onboarding.component.scss']
})
export class UserOnboardingComponent implements OnInit {
    basicForm = new FormGroup({});
    basicModel: any = {};
    basicOptions: FormlyFormOptions = {};
    adminForm = new FormGroup({});
    adminModel: any = {};
    adminOptions: FormlyFormOptions = {};
    providerForm = new FormGroup({});
    providerModel: any = {};
    providerOptions: FormlyFormOptions = {};
    privacySecurityForm = new FormGroup({});
    privacySecurityOptions: FormlyFormOptions = {};
    privacyModel: any = {};
    dataCategories: any[];
    selectedValue: any[] = [];
    adminFields: FormlyFieldConfig[];
    geographicalFocus: any[];
    descriptionsForm = new FormGroup({});
    descriptionsModel: any = {};
    descriptionsOptions: FormlyFormOptions = {};
    providerRelevantAssetClass: any[];
    relevantSectors: any[];
    basicExpansionPanel: boolean;
    companyExpansionPanel: boolean;
    descriptionsExpansionPanel: boolean;
    investor: any;
    isInvestor: boolean;
    isProvider: boolean;
    basic: boolean;
    privacy: boolean;
    investorFields: boolean;
    assetField: boolean;
    providerDetails: boolean;
    //providerPartnershipModel: any[];
    partnershipAmass: any[] = [];
    agreeValidationMessage: any;
    referral: string = `<a href> Referral Agreement </a>`;
    reseller: string = `<a href='/#/reseller-agreement'> Reseller Agreement </a>`;
    mndaTrial: boolean;
    consent: boolean;
    panelName: string;
    progressSpinner: boolean;
    basicFields: FormlyFieldConfig[];

    privacySecurityFields: FormlyFieldConfig[];
    providerFields: FormlyFieldConfig[];
    descriptionsFields: FormlyFieldConfig[];

    csrf: string;

    constructor(
        private filterService: AmassFilterService,
        public snackBar: MatSnackBar,
        private el: ElementRef,
        private userOnboarding: UserOnboardingService,
        private router: Router,
        @Inject(WINDOW) private window: Window,
        private navbardata: NavbarDataService,
        private csrfService: CSRFService,
        private principal: Principal,
        private stateService: StateStorageService,
        private authService: AuthServerProvider,
        private signinModalService: SigninModalService,
        private amassSettingService: AmassSettingService
    ) {}

    ngOnInit() {
        this.csrf = this.csrfService.getCSRF();
        this.filterService.setAuthorisedUrl();
        this.agreeValidationMessage = null;

        this.filterService.getRelatedAssetClass().subscribe(data => {});
        this.isInvestor = this.principal.hasAnyAuthorityDirect(['ROLE_INVESTOR']);
        this.isProvider = this.principal.hasAnyAuthorityDirect(['ROLE_PROVIDER']);
        this.userOnboarding.checkUser().subscribe(data => {
            if (data) {
                this.basicModel.firstName = data.firstName;
                this.basicModel.lastName = data.lastName;
                this.basicModel.company = data.company;
                this.basicModel = data;
                this.investor = data;
                this.investor = data ? data.id : null;
                if (this.basicModel.mndatrial) {
                    this.mndaTrial = true;
                }
                if (this.basicModel.consent) {
                    this.consent = true;
                }
                if (this.basicModel.firstName && this.basicModel.lastName && this.basicModel.company) {
                    this.basic = true;
                }
                if (this.basicModel.orgvisibility && this.basicModel.visibility) {
                    this.privacy = true;
                }
                if (
                    this.basicModel.categories.length >= 1 &&
                    this.basicModel.geographies.length >= 1 &&
                    this.basicModel.dataRequirements &&
                    this.basicModel.dataRequests
                ) {
                    this.investorFields = true;
                }
                if (this.basicModel.assetClasses.length >= 1 && this.basicModel.sectors.length >= 1) {
                    this.assetField = true;
                }
                if (
                    this.basicModel.consent &&
                    this.basicModel.providerName &&
                    this.basicModel.providerWebsite &&
                    this.basicModel.partnership &&
                    this.basicModel.introductions
                ) {
                    this.providerDetails = true;
                }

                this.partnershipAmass = [];
                this.filterService.getLookupCodes('PARTNERSHIP_TYPE').subscribe(data => {
                    const listItems = data && data['hits'] ? data['hits']['hits'] : [];
                    for (let i = 0; i < listItems.length; i++) {
                        if (listItems[i]._source.lookupcode !== 'Referral')
                            this.partnershipAmass.push({
                                value: listItems[i]._source.id,
                                label: listItems[i]._source.description
                            });
                    }
                    this.providerFields[2].templateOptions.options = this.partnershipAmass;
                });

                if (this.basicModel.assetClasses) {
                    const providerRelevantAssetClass = [];
                    for (const obj in this.basicModel.assetClasses) {
                        if (obj) {
                            providerRelevantAssetClass.push({
                                id: this.basicModel.assetClasses[obj].id,
                                desc: this.basicModel.assetClasses[obj].description
                            });
                        }
                    }
                    delete this.basicModel.assetClasses;
                    this.basicModel.assetClasses = providerRelevantAssetClass;
                }
                if (this.basicModel.sectors) {
                    const relevantSectors = [];
                    for (const obj in this.basicModel.sectors) {
                        if (obj) {
                            relevantSectors.push({
                                id: this.basicModel.sectors[obj].id,
                                desc: this.basicModel.sectors[obj].description
                            });
                        }
                    }
                    delete this.basicModel.sectors;
                    this.basicModel.sectors = relevantSectors;
                }
                if (this.basicModel.geographies) {
                    const geographicalFocus = [];
                    for (const obj in this.basicModel.geographies) {
                        if (obj) {
                            geographicalFocus.push({
                                id: this.basicModel.geographies[obj].id,
                                desc: this.basicModel.geographies[obj].description
                            });
                        }
                    }
                    delete this.basicModel.geographies;
                    this.basicModel.geographies = geographicalFocus;
                }
                if (this.basicModel.categories) {
                    const dataCategories = [];
                    for (const obj in this.basicModel.categories) {
                        if (obj) {
                            dataCategories.push({
                                id: this.basicModel.categories[obj].id,
                                desc: this.basicModel.categories[obj].datacategory
                            });
                        }
                    }
                    delete this.basicModel.categories;
                    this.basicModel.categories = dataCategories;
                }
            }
            //this.hideModel = this.basicModel;
            //console.log('hidemodel',this.hideModel);

            // this.checkModel = this.basicModel;
            this.adminFields = [
                {
                    key: 'categories',
                    type: 'autocomplete',
                    className: 'custom-multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Data Categories:',
                        placeholder: '',
                        required: true,
                        description: 'Which categories of data providers are you most interested in discovering? Input at least two.',
                        options: this.dataCategories,
                        attributes: {
                            field: 'desc',
                            minLength: '1',
                            // 'placeholderText' : 'All the data categories this data provider collects/provides/analyzes.',
                            // 'labelTooltip': 'See "Data Categories" table for a definition of each data category. Always make sure the Main Data Category is included in here. Try to get up to 7 most appropriate data categories for each company. Some companies may have less than this and some other may have many more. In such unusual cases, seek assistance.',
                            multiple: 'true',
                            template: 'true',
                            dropdown: 'true'
                            //'placeholderTooltip': 'See "Data Categories" table for a definition of each data category. Always make sure the Main Data Category is included in here. Try to get up to 7 most appropriate data categories for each company. Some companies may have less than this and some other may have many more. In such unusual cases, seek assistance.'
                        },
                        change: (event: any) => {
                            const queryObj = event;
                            this.dataCategories = [];

                            const selected = this.basicModel['categories'];
                            ///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
                            if (selected && selected.length) {
                                this.selectedValue = selected.map(item => parseInt(item.id, 10));
                            }
                            if (queryObj.query) {
                                this.filterService.getAllDataCategories(queryObj.query).subscribe(d => {
                                    const listItems = d ? d['hits']['hits'] : [];
                                    for (let i = 0; i < listItems.length; i++) {
                                        const item = listItems[i]._source;
                                        if (
                                            listItems[i]._source.datacategory !== '' &&
                                            listItems[i]._source.datacategory !== 'DATA_CATEGORY_NULL'
                                        ) {
                                            if (!this.selectedValue.includes(item.id)) {
                                                this.dataCategories.push({
                                                    id: item.id,
                                                    desc: item.datacategory,
                                                    img: item.logo ? item.logo.logo : null
                                                });
                                            }
                                        }
                                    }
                                    setTimeout(() => {
                                        this.adminFields[0].templateOptions.options = this.dataCategories;
                                    }, 2500);
                                });
                            } else {
                                this.filterService.getCategoriesForFilter().subscribe(data => {
                                    //const listItems = d ? d['hits']['hits'] : [];
                                    const listItems =
                                        data['aggregations'] && data['aggregations'].categories
                                            ? data['aggregations'].categories.buckets
                                            : [];
                                    for (let i = 0; i < listItems.length; i++) {
                                        //const item = listItems[i]._source;
                                        const category = listItems[i];
                                        const categoryId = parseInt(category.key, 10);
                                        const categoryList = category.categories.hits.hits[0]._source.categories;
                                        const filteredCategoryDescription = categoryList.filter(item => item.id === category.key);
                                        if (category.categories.hits.hits[0]._source.categories !== 'DEFAULT_NULL') {
                                            if (!this.selectedValue.includes(categoryId)) {
                                                this.dataCategories.push({
                                                    id: categoryId,
                                                    desc:
                                                        filteredCategoryDescription &&
                                                        filteredCategoryDescription[0] &&
                                                        filteredCategoryDescription[0].desc
                                                            ? filteredCategoryDescription[0].desc
                                                            : '',
                                                    img: categoryList.logo ? categoryList.logo.logo : null
                                                });
                                            }
                                        }
                                    }
                                    setTimeout(() => {
                                        this.adminFields[0].templateOptions.options = this.dataCategories;
                                    }, 2500);
                                });
                            }
                        },
                        blur: (event: any) => {
                            if (event && event.target && !event['selectedVal']) {
                                event.target.value = '';
                            }
                        }
                    },
                    hideExpression: this.formCheck('categories')
                },
                {
                    key: 'geographies',
                    type: 'multiselect',
                    className: 'custom-multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Regions:',
                        placeholder: '',
                        description:
                            'Data providers covering which of the following geographical regions are you most interested in discovering? Input at least one.',
                        required: true,
                        options: this.geographicalFocus,
                        attributes: {
                            /*  'placeholderText' : 'The geographies that the data provider focuses on collecting/providing/analyzing data about.',
                        'labelTooltip': 'If a data provider covers numerous countries across continents, then it is multinational. If a data provider only covers one continent, certain countries or a specific region, then select each one. This is not the location of the company itself.',
                        'placeholderTooltip': 'If a data provider covers numerous countries across continents, then it is multinational. If a data provider only covers one continent, certain countries or a specific region, then select each one. This is not the location of the company itself.',
                        'lookupmodule':'GEOGRAPHICAL_FOCUS' */
                        }
                    },
                    hideExpression: this.formCheck('geographies')
                },
                {
                    key: 'dataRequirements',
                    type: 'input',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Data Requirements: ',
                        type: 'input',
                        placeholder:
                            'An example: "We prefer large, raw datasets that already have tickers mapped and have at least 3 years of historical data."',
                        description: 'Describe any requirements for a data provider to be of interest to you.',
                        //change: this.basicInput
                        keypress: event => {},
                        maxLength: 256
                    },
                    hideExpression: this.formCheck('dataRequirements')
                },
                {
                    key: 'dataRequests',
                    type: 'input',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Data Requests: ',
                        type: 'input',
                        placeholder:
                            'An example: "We are currently seeking any data that can provide us additional insight into how coronavirus has affected the restaurant industry and their suppliers."',
                        description: 'Describe any requests for data you are looking for that are top-of-mind.',
                        //change: this.basicInput
                        keypress: event => {},
                        maxLength: 256
                    },
                    hideExpression: this.formCheck('dataRequests')
                }
            ];
            this.geographicalFocus = [];
            this.filterService.getGeographicRegion().subscribe(data => {
                const listItems = data ? data['hits']['hits'] : [];
                for (let i = 0; i < listItems.length; i++) {
                    this.geographicalFocus.push({
                        value: {
                            id: listItems[i]._source.id,
                            desc: listItems[i]._source.lookupcode
                        },
                        label: listItems[i]._source.lookupcode
                    });
                }
                this.adminFields[1].templateOptions.options = this.geographicalFocus;
            });
            this.providerRelevantAssetClass = [];
            this.filterService.getRelatedAssetClass().subscribe(data => {
                const listItems = data ? data['hits']['hits'] : [];
                //console.log('List Items',listItems);
                //listItems.shift();
                //console.log('List Items 1',listItems);
                for (let i = 0; i < listItems.length; i++) {
                    //console.log('4',this.providerRelevantAssetClass[0]);
                    //this.providerRelevantAssetClass.shift();
                    if (listItems[i]._source.description !== 'DEFAULT_NULL') {
                        this.providerRelevantAssetClass.push({
                            value: {
                                id: listItems[i]._source.id,
                                desc: listItems[i]._source.description
                            },
                            label: listItems[i]._source.description
                        });
                    }
                }
                this.descriptionsFields[0].templateOptions.options = this.providerRelevantAssetClass;
            });

            this.relevantSectors = [];
            this.filterService.getLookupCodes('RLVNT_SECTOR').subscribe(data => {
                const listItems = data ? data['hits']['hits'] : [];
                for (let i = 0; i < listItems.length; i++) {
                    this.relevantSectors.push({
                        value: {
                            id: listItems[i]._source.id,
                            desc: listItems[i]._source.lookupcode
                            //desc: listItems[i]._source.description,
                            //locked: listItems[i]._source.locked ? listItems[i]._source : false
                        },
                        label: listItems[i]._source.lookupcode
                    });
                }
                this.descriptionsFields[1].templateOptions.options = this.relevantSectors;
            });
            //this.providerPartnershipModel = [];
            /*  this.filterService.getLookupCodes('PARTNERSHIP_TYPE').subscribe(data =>{
		   const listItems = data && data['hits'] ? data['hits']['hits'] : [];
		   for(let i = 0; i< listItems.length; i++){
			   this.providerPartnershipModel.push({
				   value: listItems[i]._source.id,
				   label: listItems[i]._source.description
			   });
		   }
		   this.providerFields[2].templateOptions.options = this.providerPartnershipModel;
	   }); */
            this.basicFields = [
                {
                    key: 'firstName',
                    type: 'input',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'First Name',
                        placeholder: '',
                        //description: 'What is the name of the brand which acts as a data provider? This is often the name of the company.',
                        required: true,
                        maxLength: 50,
                        // change: this.basicInput
                        keypress: event => {}
                    },
                    hideExpression: this.formCheck('firstName')
                },
                {
                    key: 'lastName',
                    type: 'input',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Last Name:',
                        placeholder: '',
                        //description: 'What is the name of the brand which acts as a data provider? This is often the name of the company.',
                        required: true,
                        maxLength: 50,
                        // change: this.basicInput
                        keypress: event => {}
                    },
                    hideExpression: this.formCheck('lastName')
                },
                {
                    key: 'company',
                    type: 'input',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Company Name:',
                        placeholder: '',
                        //description: 'What is the name of the brand which acts as a data provider? This is often the name of the company.',
                        required: true,
                        maxLength: 36,
                        // change: this.basicInput
                        keypress: event => {}
                    },
                    hideExpression: this.formCheck('company')
                }
            ];

            this.privacySecurityFields = [
                {
                    key: 'orgvisibility',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: "Visibility of my Data Provider's Profile:",
                        description: "Who would you like to be able to see your organization's data profile and resources?",
                        options: [
                            {
                                value: 'Anyone (Public)',
                                label: 'Anyone (Public)'
                            },
                            {
                                value: 'Verified Users Only',
                                label: 'Verified Users Only'
                            },
                            {
                                value: 'Asset Managers Only',
                                label: 'Asset Managers Only'
                            },
                            {
                                value: 'Data Providers Only',
                                label: 'Data Providers Only'
                            },
                            {
                                value: 'People that I Directly Give Access to ',
                                label: 'People that I Directly Give Access to '
                            },
                            {
                                value: 'Just Me (Private & Anonymous)',
                                label: 'Just Me (Private & Anonymous)'
                            }
                        ],
                        click: event => {
                            console.log('...visibility...');
                            //this.googleAnalyticsService.emitEvent('Provider Profile Form - Visibility', 'Changed', 'Provider Profile Form - Dropdown - User Permissions');
                        }
                    },
                    hideExpression: this.formCheck('orgvisibility')
                },
                // {
                //     key: 'visibility',
                //     type: 'radioButton',
                //     wrappers: ['form-field-horizontal'],
                //     templateOptions: {
                //         label: 'Visibility of Me & My Comments:',
                //         description: 'Who would you like to be able to see your personal name, profile and comments?',
                //         options: [
                //             {
                //                 value: 'Anyone (Public)',
                //                 label: 'Anyone (Public)'
                //             },
                //             {
                //                 value: 'Verified Users Only',
                //                 label: 'Verified Users Only'
                //             },
                //             {
                //                 value: 'Data Providers Only',
                //                 label: 'Data Providers Only'
                //             },
                //             {
                //                 value: 'Investment Managers Only',
                //                 label: 'Investment Managers Only'
                //             },
                //             {
                //                 value: 'People that I Directly Give Access to',
                //                 label: 'People that I Directly Give Access to'
                //             },
                //             {
                //                 value: 'Just Me (Private & Anonymous)',
                //                 label: 'Just Me (Private & Anonymous)'
                //             }
                //         ],
                //         click: event => {
                //             console.log('...visibility...');
                //             //this.googleAnalyticsService.emitEvent('Provider Profile Form - Visibility', 'Changed', 'Provider Profile Form - Dropdown - User Permissions');
                //         }
                //     },
                //     hideExpression: this.formCheck('visibility')
                // }
            ];

            this.providerFields = [
                {
                    key: 'providerName',
                    type: 'input',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Provider Name:',
                        placeholder: '',
                        description:
                            'What is the official name of your brand which acts as a data provider? This is often the name of your company.',
                        required: true,
                        attributes: {
                            placeholderText: 'Please use correct spelling and casing.',
                            labelTooltip:
                                'The name of the brand or division of your organization that collects/provides/analyzes data and/or research. It is not always the same name as your organization (but it usually is).',
                            descriptionTooltip:
                                'The name of the brand or division of your organization that collects/provides/analyzes data and/or research. It is not always the same name as your organization (but it usually is).',
                            placeholderTooltip:
                                'The name of the brand or division of your organization that collects/provides/analyzes data and/or research. It is not always the same name as your organization (but it usually is).',
                            trackNames: 'Provider Name',
                            trackCategory: 'Provider Profile Form - Textbox',
                            trackLabel: 'Provider Profile Form - Input'
                        },
                        maxLength: 100,
                        keypress: event => {}
                        //change: this.basicInput
                    },
                    hideExpression: this.formCheck('providerName')
                },
                {
                    key: 'providerWebsite',
                    type: 'input',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: "Provider's Home Website:",
                        type: 'input',
                        placeholder: '',
                        description: 'What is the link to the home webpage of your brand that acts as a data provider?',
                        required: true,
                        //pattern: /^(?:https?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/,
                        attributes: {
                            //'placeholderText' : 'The URL of the website of your data provider business in form of "www.domain.com"',
                            labelTooltip: '',
                            descriptionTooltip: '',
                            placeholderTooltip: '',
                            trackNames: 'Home Website',
                            trackCategory: 'Provider Profile Form - Textbox',
                            trackLabel: 'Provider Profile Form - Input'
                        },
                        maxLength: 45,
                        //change: this.basicInput
                        keypress: event => {}
                    },
                    hideExpression: this.formCheck('providerWebsite')
                },
                {
                    key: 'partnership',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Partnership with Amass:',
                        placeholder: '',
                        description: 'Would you like to partner with Amass to enhance your ability to sell your data products?',
                        options: this.partnershipAmass,
                        attributes: {
                            // 'placeholderText1' : 'In brief:',
                            placeholderText:
                                'In brief, Amass takes 30% of the revenue on sales of your data products to our prospects and takes the lead on every step of the sales, delivery and support process while keeping you in the loop, as laid out in the ' +
                                ' ' +
                                this.reseller +
                                '.'
                            //'placeholderText3' : 'For reselling, Amass takes 30% of the revenue and takes the lead on every step of the sales process, as laid out in the'+' '+this.reseller+'.'
                        }
                        //change: this.inputChanges
                    },
                    hideExpression: this.formCheck('partnership')
                },
                {
                    key: 'introductions',
                    type: 'radioButton',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Introductions to Data Buyers:',
                        placeholder: '',
                        description:
                            'Would you like Amass executives to set up meetings for you with qualified data buyers and if so, how many?',
                        options: [
                            {
                                value: 'No, Thanks.',
                                label: 'No, Thanks.'
                            },
                            {
                                value: "I'm not sure, so let's chat about the options.",
                                label: "I'm not sure, so let's chat about the options."
                            },
                            {
                                value: 'Yes, set up one (1) meeting for us and we will pay you $500.',
                                label: 'Yes, set up one (1) meeting for us and we will pay you $500.'
                            },
                            {
                                value: 'Yes, set up three (3) meetings for us and we will pay you $1,000.',
                                label: 'Yes, set up three (3) meetings for us and we will pay you $1,000.'
                            },
                            {
                                value: 'Yes, set up thirty (30) meetings for us (within 5 months) and we will pay you $15,000.',
                                label: 'Yes, set up thirty (30) meetings for us (within 5 months) and we will pay you $15,000.'
                            }
                        ],
                        attributes: {
                            placeholderText:
                                'Once you update your profile on Insights, executives at Amass will proactively reach out to prospective data buyers with the appropriate budget and data requirements matching your data products. If the data buyer reviews your profile and expresses interest in setting up a meeting, we will organize a meeting with both parties. '
                        }
                        //change: this.inputChanges
                    },
                    hideExpression: this.formCheck('introductions')
                }
            ];
            this.descriptionsFields = [
                {
                    key: 'assetClasses',
                    type: 'multiselect',
                    className: 'custom-multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Asset Classes:',
                        required: true,
                        placeholder: '',
                        description: 'Which asset classes are you interested in acquiring data about?',
                        //description: 'What are all the asset classes this provider\'s data offerings can be used to research?',
                        options: this.providerRelevantAssetClass,
                        attributes: {
                            /* 'placeholderText' : 'All of the financial security asset classes that the provider\'s data can be used for or has any relevance towards.',
                'labelTooltip': 'At minimum, the Main Asset Class should be listed here. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.',
                'descriptionTooltip': 'At minimum, the Main Asset Class should be listed here. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.',
                'placeholderTooltip': 'At minimum, the Main Asset Class should be listed here. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.' */
                        }
                    },
                    hideExpression: this.formCheck('assetClasses')
                },
                {
                    key: 'sectors',
                    type: 'multiselect',
                    className: 'custom-multiselect',
                    wrappers: ['form-field-horizontal'],
                    templateOptions: {
                        label: 'Sectors:',
                        required: true,
                        placeholder: '',
                        description: 'Which sectors are you interested in acquiring data about?',
                        options: this.relevantSectors
                    },
                    hideExpression: this.formCheck('sectors')
                }
            ];
        });
    }

    openSnackBar(message: string, action: string, delay?: number) {
        this.snackBar.open(message, action, {
            duration: delay ? delay : 10000,
            panelClass: ['blue-snackbar']
        });
    }
    expandFormPanels() {
        this.basicExpansionPanel = true;
        this.companyExpansionPanel = true;
        this.descriptionsExpansionPanel = true;
    }
    formSave() {}
    formCheck(message: string) {
        if (message === 'firstName') {
            if (this.basicModel.firstName) {
                return true;
            }
        } else if (message === 'lastName') {
            if (this.basicModel.lastName) {
                return true;
            }
        } else if (message === 'company') {
            if (this.basicModel.company) {
                return true;
            }
        } else if (message === 'orgvisibility' && !this.isProvider) {
            return true;
        } else if (message === 'orgvisibility' && this.isProvider) {
            return this.basicModel.orgvisibility ? true : false;
        } else if (message === 'visibility') {
            if (this.basicModel.visibility) {
                return true;
            }
        } else if (message === 'providerName') {
            if (this.basicModel.providerName) {
                return true;
            }
        } else if (message === 'providerWebsite') {
            if (this.basicModel.providerWebsite) {
                return true;
            }
        } else if (message === 'partnership') {
            if (this.basicModel.partnership) {
                return true;
            }
        } else if (message === 'introductions') {
            if (this.basicModel.introductions) {
                return true;
            }
        } else if (message === 'categories') {
            if (this.basicModel.categories.length >= 1) {
                return true;
            }
        } else if (message === 'geographies') {
            if (this.basicModel.geographies.length >= 1) {
                return true;
            }
        } else if (message === 'dataRequirements') {
            if (this.basicModel.dataRequirements) {
                return true;
            }
        } else if (message === 'dataRequests') {
            if (this.basicModel.dataRequests) {
                return true;
            }
        } else if (message === 'assetClasses') {
            if (this.basicModel.assetClasses.length >= 1) {
                return true;
            }
        } else if (message === 'sectors') {
            if (this.basicModel.sectors.length >= 1) {
                return true;
            }
        }
    }
    submitProviderEdit(
        model,
        panelName?: string,
        currentPanel?: string,
        nextPanel?: string,
        tabIndex?: number,
        form?: string,
        skipRedirect?: boolean
    ) {
        this.panelName = panelName;

        const formName = 'Investor';
        const submitMessage =
            'Your edits have been successfully submitted. Amass will review these edits over the coming days before they appear within your profile.';
        const filterArr = Array.prototype.filter;
        if (form && this[form].valid) {
            const autoCompleteFields = this.el.nativeElement.querySelectorAll('#' + form + ' .ui-autocomplete-input');
            let invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
            const filteredAutoCompleteFields = filterArr.call(autoCompleteFields, function(node) {
                return node.value === '';
            });
            if (form === 'providerForm' && !model.consent) {
                this.agreeValidationMessage = '* This field is required';
                this.openSnackBar('Please fill all the required fields!', 'Close');
            }
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else {
                delete model['id'];
                model['id'] = this.investor;
                model.userOnboarding = true;
                this.progressSpinner = true;
                this.userOnboarding.saveData(model).subscribe(
                    data => {
                        if (data) {
                            // this.formSubmitted = true;
                            //this.openSnackBar('Investor Details Saved Successfully', 'Close');
                            this.progressSpinner = false;
                            this.openSnackBar(
                                'Thank you for your submission. We will be in touch soon. In the meantime, please have a look around.',
                                'Close'
                            );

                           /*  if (this.basicModel.userRoleOnboarding === false) {
                                this.router.navigateByUrl('/how-it-works-providers');
                            } else if(this.basicModel.userRoleOnboarding === true) {
                                this.router.navigateByUrl('/provider-onboarding');
                            } */

                            if (this.isProvider && this.basicModel.userRoleOnboarding === false) {
                                this.router.navigateByUrl('/how-it-works-providers');
                            } else if(this.isProvider && this.basicModel.userRoleOnboarding === true) {
                                this.router.navigate(['/investors']);
                            } else if(this.isInvestor) {
                                this.router.navigate(['/providers']);
                            }

                            this.navbardata.sendData(true);
                            if (currentPanel && currentPanel !== '') {
                                this[currentPanel] = false;
                                if (nextPanel && nextPanel !== '') {
                                    this[nextPanel] = true;
                                }
                                if (tabIndex) {
                                    //this.tabIndex = tabIndex;
                                }
                            } else {
                                if (skipRedirect) {
                                    return true;
                                }
                                /*  this.stateService.storeUrl(null);
                            this.router.navigate(['../../', model.recordID], { relativeTo: this.route }); */
                            }
                        } else {
                            // console.log(data);
                            //this.formSubmitted = false;
                            this.progressSpinner = false;
                            this.openSnackBar('' + formName + ' failed to save!', 'Close');
                        }
                    },
                    error => {
                        if (error.status === 401) {
                            //this.formSubmitted = false;
                            this.progressSpinner = false;
                            this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
                        } else {
                            this.progressSpinner = false;
                            this.openSnackBar('' + formName + ' failed to save!', 'Close');
                        }
                    }
                );
            }
        } else {
            let invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
            let autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
            let filteredAutoCompleteFields = filterArr.call(autoCompleteFields, function(node) {
                return node.required && node.value === '';
            });
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else if (invalidElements.length > 0) {
                if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                    invalidElements[0].querySelectorAll('input')[0].focus();
                } else {
                    setTimeout(() => {
                        invalidElements[0].focus();
                    }, 1000);
                }
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else if (!model.consent && this.isProvider) {
                this.agreeValidationMessage = '* This field is required';
                this.openSnackBar('Please fill all the required fields!', 'Close');
            } else {
                delete model['id'];
                model['id'] = this.investor;
                model.userOnboarding = true;
                this.progressSpinner = true;
                this.userOnboarding.saveData(model).subscribe(
                    data => {
                        if (data) {
                            // this.formSubmitted = true;
                            this.progressSpinner = false;
                            this.openSnackBar(
                                'Thank you for your submission. We will be in touch soon. In the meantime, please have a look around.',
                                'Close'
                            );
                          /*   if (this.basicModel.userRoleOnboarding === false) {
                                this.router.navigateByUrl('/how-it-works-providers');
                            } else if(this.basicModel.userRoleOnboarding === true) {
                                this.router.navigateByUrl('/provider-onboarding');
                            } */

                            if (this.isProvider && this.basicModel.userRoleOnboarding === false) {
                                this.router.navigateByUrl('/how-it-works-providers');
                            } else if(this.isProvider && this.basicModel.userRoleOnboarding === true) {
                                this.router.navigate(['/investors']);
                            } else if(this.isInvestor) {
                                this.router.navigate(['/providers']);
                            }

                            this.navbardata.sendData(true);
                            if (currentPanel && currentPanel !== '') {
                                this[currentPanel] = false;
                                if (nextPanel && nextPanel !== '') {
                                    this[nextPanel] = true;
                                }
                                if (tabIndex) {
                                    //this.tabIndex = tabIndex;
                                }
                            } else {
                                if (skipRedirect) {
                                    return true;
                                }
                                /*  this.stateService.storeUrl(null);
                            this.router.navigate(['../../', model.recordID], { relativeTo: this.route }); */
                            }
                        } else {
                            // console.log(data);
                            //this.formSubmitted = false;
                            this.progressSpinner = false;
                            this.openSnackBar('' + formName + ' failed to save!', 'Close');
                        }
                    },
                    error => {
                        if (error.status === 401) {
                            //this.formSubmitted = false;
                            this.progressSpinner = false;
                            this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
                        } else {
                            this.progressSpinner = false;
                            this.openSnackBar('' + formName + ' failed to save!', 'Close');
                        }
                    }
                );
            }
        }
    }
    onExpireSession(event) {
        //console.log('eventcheck',event);
        if (event) {
            //console.log('eventcheck',event);
            //this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
                setTimeout(() => {
                    this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
                }, 2000);
            });
        }
    }
    beforeSessionExpires(event) {
        if (event) {
            this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }

    @HostListener('window:beforeunload', ['$event'])
    preventUser($event) {
        $event.preventDefault();
        return $event.returnValue = "Are you sure you want to exit?";
    }
}
