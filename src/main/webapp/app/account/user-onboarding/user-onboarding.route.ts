import { Route } from '@angular/router';
import { UserOnboardingComponent } from './user-onboarding.component';
import { UserRouteAccessService } from '../../shared';



export const userOnboardingRoute: Route = {
    path: 'get-started',
    component: UserOnboardingComponent,
    data: {
        authorities: ['ROLE_INVESTOR','ROLE_PROVIDER'],
        pageTitle: 'Get Started with Insights ',
        breadcrumb: ' Get Started'
    },
    canActivate: [UserRouteAccessService]
};
