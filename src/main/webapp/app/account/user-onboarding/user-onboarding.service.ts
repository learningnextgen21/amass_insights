import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserOnboardingService {
    private userUrl = 'api/userOnboarding';
    public partnership: any;
    constructor(
        private http: HttpClient
    )
    {

    }

    saveData(request?: any): Observable<any> {
        const model = request;
        // console.log(this.partnership);
        if (model && model.partnership && !model.partnership.id) {
            model.partnership = {
                id: request.partnership
            };
            this.partnership = model.partnership;
        } else if (model && model.partnership && typeof(model.partnership) === 'object') {
            delete model.partnership;
            // console.log(model, model.partnership, request);
            model['partnership'] = this.partnership;
        } else if (model && model.partnership && model.partnership.id === null) {
            model.partnership = null;
        }
        return this.http.put(this.userUrl, model);
    }

    checkUser():Observable<any> {
        return this.http.get('api/userRetrieve');
    }

    providerLogo(providerID: any):Observable<any> {
        return this.http.get('api/providerLogo/' + `${providerID}`);
    }
}
