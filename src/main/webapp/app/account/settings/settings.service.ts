import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AmassSettingService {
    private accountLinkUrl = 'api/operator-permission';
    private countUrl = 'api/ur/providerscount';
    private tagUrl = 'api/tagProvider';
    private providerPermission = 'api/user-provider';
    user: any;
    constructor(private http: HttpClient) {
        this.user = {};
    }

    setAuthorisedUrl() {
        return this.http.get(this.accountLinkUrl);
    }
    checkProviderCount(entityType: String): Observable<any> {
        return this.http.get(`${this.countUrl}/${entityType}`);
    }
    providerTagCount(tagName: String): Observable<any> {
        return this.http.get(`${this.tagUrl}/${tagName}`);
    }
    activateMymatches(): Observable<any> {
        return this.http.put('api/activateMymatches', {});
    }
    myMatches(changes: string, order: string, itemsFrom: number) {
        let queryString = 'key=' + changes;
        if (order) {
            queryString += '&order=' + order;
        }
        queryString += '&itemsFrom=' + itemsFrom;

        return this.http.post('api/es/matchessearch?' + queryString, {});
    }
    checkSubscribe(): Observable<any> {
        return this.http.get('api/checkout');
    }
    checkUser(): Observable<any> {
        return this.http.get('api/userRetrieve');
    }
    checkProviderPermission(providerId: Number): Observable<any> {
        return this.http.get('api/providerpermission/' + providerId);
    }
    checkUserValue(): Observable<any> {
        return this.http.get('api/checkUser');
    }
    cardDetails(): Observable<any> {
        return this.http.get('api/cardDetais');
    }
    savecard(card: any): Observable<any> {
        return this.http.post('api/updateCardDetails', card);
    }
    savenewcard(card: any): Observable<any> {
        return this.http.post('api/saveCardDetails', card);
    }
    userauthorities(): Observable<any> {
        return this.http.get('api/userAuthority');
    }
    restrictedAuthority(): Observable<any> {
        return this.http.get('api/restrictedAuthority');
    }
    saveData(request?: any): Observable<any> {
        this.user.dataRequests = request.dataRequests;
        this.user.dataRequirements = request.dataRequirements;
        this.user.categories = request.categories;
        this.user.geographies = request.geographies;

        return this.http.put('api/saveData', this.user);
    }
    saveInvestor(request?: any): Observable<any> {
        this.user.fundName = request.fundName;
        this.user.assetClasses = request.assetClasses;
        this.user.geographies = request.geographies;
        this.user.sectors = request.sectors;
        return this.http.put('api/saveData', this.user);
    }
    save(request?: any): Observable<any> {
        return this.http.put('api/privacy', request);
    }
}
