import { Component, OnInit, Inject, EventEmitter, Output, AfterViewInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { JhiLanguageService } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Principal, AccountService, JhiLanguageHelper, AuthServerProvider, StateStorageService, SigninModalService } from '../../shared';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { AmassSettingService } from './settings.service';
// import { StripeService, Elements, Element as StripeElement, ElementsOptions } from 'ngx-stripe';
import { StripeService } from 'ngx-stripe';
import { StripeCardElement, StripeElementsOptions } from '@stripe/stripe-js';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig, FormlyTemplateOptions } from '@ngx-formly/core';
import { AmassFilterService } from 'app/shared';
import { Router } from '@angular/router';
import { DataProviderService } from 'app/entities/data-provider';

export interface DialogData {
    notify_news_update: boolean;
}
@Component({
    selector: 'jhi-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss', './settings.component.css']
})
export class SettingsComponent implements OnInit, AfterViewInit, OnDestroy {
    error: string;
    success: string;
    settingsAccount: any;
    paymentAccount: any;
    languages: any[];
    mdDialogRef: MatDialogRef<any>;
    confirmAccountSettings: any;
    dataConfirm: any;
    successMessage: any;
    previousMail: any;
    emailTracking: boolean;
    providerId: any;
    errorUserExists: any;
    cardErrorExists: any;
    unsubscribeValue: boolean;
    updatedMail: any;
    subscription: any;
    cardDetails: any;
    cardList: any;
    elements: any;
    card: StripeCardElement;
    elementsOptions: StripeElementsOptions = {
        locale: 'en'
    };
    check: any;
    stripeTest: FormGroup;
    tokenId: any;
    dataCategories: any[];
    selectedValue: any[] = [];
    providerRelevantAssetClass: any[] = [];
    relevantSectors: any[] = [];
    geographicalFocus: any[];
    investor: boolean;
    provider: boolean;
    admin: boolean;
    recordId: string;
    userRole: boolean;
    //referral: string = `<a href=''> Referral </a>` ;
    reseller: string = `<a href='/#/reseller-agreement'> Reseller </a>`;
    /*  @ViewChild('resource') resource: ElementRef;
    @ViewChild('resources') resources: ElementRef; */
    basicForm = new FormGroup({});
    basicModel: any = {};
    checkModel: any = {};
    userAuthoritiesValue: any;
    basicOptions: FormlyFormOptions = {};
    basicFields: FormlyFieldConfig[];
    @ViewChild('cardInfo') cardInfo: ElementRef;
    investigationForm = new FormGroup({});
    investigationOptions: FormlyFormOptions = {};
    privacySecurityForm = new FormGroup({});
    privacySecurityOptions: FormlyFormOptions = {};
    privacyModel: any = {};
    investorSecurityForm = new FormGroup({});
    investorSecurityOptions: FormlyFormOptions = {};
    investorModel: any = {};
    providerSecurityForm = new FormGroup({});
    providerSecurityOptions: FormlyFormOptions = {};
    providerForm = new FormGroup({});
    providerModel: any = {};
    providerOptions: FormlyFormOptions = {};
    investigationFields: FormlyFieldConfig[] = [
        {
            key: 'fundName',
            type: 'input',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Fund Name:',
                type: 'input',
                placeholder: '',
                //description: 'What is the link to the home webpage of your brand that acts as a data provider?',
                required: true,
                attributes: {
                    /* 	'placeholderText' : 'Link to the website of the data provider or the division within the company/organization that provides/analyzes data.',
					'labelTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                    'descriptionTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                    'placeholderTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                    'trackNames': 'Provider Website',
                    'trackCategory': 'Provider Profile Admin - Textbox',
                    'trackLabel': 'Provider Profile Admin - Input' */
                },
                keypress: event => {}
            }
        },
        {
            key: 'assetClasses',
            type: 'multiselect',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Asset Classes:',
                //required: true,
                placeholder: '',
                //description: 'What are all the asset classes this provider\'s data offerings can be used to research?',
                options: this.providerRelevantAssetClass,
                attributes: {
                    /* 'placeholderText' : 'All of the financial security asset classes that the provider\'s data can be used for or has any relevance towards.',
                    'labelTooltip': 'At minimum, the Main Asset Class should be listed here. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.',
                    'descriptionTooltip': 'At minimum, the Main Asset Class should be listed here. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.',
                    'placeholderTooltip': 'At minimum, the Main Asset Class should be listed here. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.' */
                }
            }
        },

        {
            key: 'sectors',
            type: 'multiselect',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Sectors:',
                //required: true,
                placeholder: '',
                //description: 'What sectors are the companies/securities in that this data could be used for?',
                options: this.relevantSectors,
                attributes: {
                    /* field: 'id',
					'placeholderText' : 'All of the sectors the provider\'s data could be used for or has any relevance towards.' */
                }
            }
        }
    ];
    privacySecurityFields: FormlyFieldConfig[] = [];
    investorSecurityFields: FormlyFieldConfig[] = [
        {
            key: 'visibility',
            type: 'radioButton',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Visibility of Me & My Comments:',
                required: true,
                //description: 'Who would you like to be able to see your personal name, profile and comments?',
                options: [
                    {
                        value: 'Anyone (Public)',
                        label: 'Anyone (Public)'
                    },
                    {
                        value: 'Verified Users Only',
                        label: 'Verified Users Only'
                    },
                    {
                        value: 'Data Providers Only',
                        label: 'Data Providers Only'
                    },
                    {
                        value: 'Investment Managers Only',
                        label: 'Investment Managers Only'
                    },
                    {
                        value: 'People that I Directly Give Access to',
                        label: 'People that I Directly Give Access to'
                    },

                    {
                        value: 'Just Me (Private & Anonymous)',
                        label: 'Just Me (Private & Anonymous)'
                    }
                ],
                click: event => {
                    //this.googleAnalyticsService.emitEvent('Provider Profile Form - Visibility', 'Changed', 'Provider Profile Form - Dropdown - User Permissions');
                }
            }
        }
    ];
    providerSecurityFields: FormlyFieldConfig[] = [
        {
            key: 'visibility',
            type: 'radioButton',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Visibility of Me & My Comments:',
                required: true,
                //description: 'Who would you like to be able to see your personal name, profile and comments?',
                options: [
                    {
                        value: 'Anyone (Public)',
                        label: 'Anyone (Public)'
                    },
                    {
                        value: 'Verified Users Only',
                        label: 'Verified Users Only'
                    },
                    {
                        value: 'Data Providers Only',
                        label: 'Data Providers Only'
                    },
                    {
                        value: 'Investment Managers Only',
                        label: 'Investment Managers Only'
                    },
                    {
                        value: 'People that I Directly Give Access to',
                        label: 'People that I Directly Give Access to'
                    },

                    {
                        value: 'Just Me (Private & Anonymous)',
                        label: 'Just Me (Private & Anonymous)'
                    }
                ],
                click: event => {
                    //this.googleAnalyticsService.emitEvent('Provider Profile Form - Visibility', 'Changed', 'Provider Profile Form - Dropdown - User Permissions');
                }
            }
        }
    ];

    providerFields: FormlyFieldConfig[] = [
        {
            key: 'providerName',
            type: 'input',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Provider Name:',
                placeholder: '',
                //description: 'What is the official name of your brand which acts as a data provider? This is often the name of your company.',
                required: true,
                attributes: {
                    /*   'placeholderText' : 'Please use correct spelling and casing.',
                    'labelTooltip': 'The name of the brand or division of your organization that collects/provides/analyzes data and/or research. It is not always the same name as your organization (but it usually is).',
                    'descriptionTooltip': 'The name of the brand or division of your organization that collects/provides/analyzes data and/or research. It is not always the same name as your organization (but it usually is).',
                    'placeholderTooltip': 'The name of the brand or division of your organization that collects/provides/analyzes data and/or research. It is not always the same name as your organization (but it usually is).',
                    'trackNames': 'Provider Name',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input' */
                },
                keypress: event => {}
                //change: this.basicInput
            }
        },
        {
            key: 'providerWebsite',
            type: 'input',
            wrappers: ['form-field-horizontal'],
            templateOptions: {
                label: 'Provider Home Website:',
                type: 'input',
                placeholder: '',
                // description: 'What is the link to the home webpage of your brand that acts as a data provider?',
                required: true,
                pattern: /^(?:https?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/,
                attributes: {
                    //'placeholderText' : 'The URL of the website of your data provider business in form of "www.domain.com"',
                    /* 'labelTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                    'descriptionTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                    'placeholderTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                    'trackNames': 'Home Website',
                    'trackCategory': 'Provider Profile Form - Textbox',
                    'trackLabel': 'Provider Profile Form - Input' */
                },
                //change: this.basicInput
                keypress: event => {}
            },

            validation: {
                messages: {
                    required: 'Website is required'
                }
            }
        }
        /*  {
			key: '',
			type: 'fileUpload',
			className: 'customWidth',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Provider Logo:',
				attributes: {
				 	'logoType' : 'Logo',
					'accept': 'image/*',
					'trackNames': 'Provider Logo',
                    'trackCategory': 'Provider Profile Form - File',
                    'trackLabel': 'Provider Profile Form - File - Provider Logo'
				},
				change: ( event : any) => {
					if(event && event[0].name) {
						//this.basicInput(event);
					}
					this.dataProviderService.onUpload(this.recordId, event[0]).subscribe(data=>{
					});
				}
			}
		} */
    ];
    constructor(
        private account: AccountService,
        private principal: Principal,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        public dialog: MatDialog,
        public mdDialog: MatDialog,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private amassSettingService: AmassSettingService,
        private stripeService: StripeService,
        private fb: FormBuilder,
        public snackBar: MatSnackBar,
        private filterService: AmassFilterService,
        private router: Router,
        private el: ElementRef,
        private dataProviderService: DataProviderService,
        private stateService: StateStorageService,
        private authService: AuthServerProvider,
        private signinModalService: SigninModalService
    ) {
        this.unsubscribeValue = true;
        this.cardList = {};
        this.settingsAccount = {};
    }
    ngOnInit() {
        this.filterService.setAuthorisedUrl();
        this.amassSettingService.userauthorities().subscribe(authorities => {
            this.userRole = authorities;
        });
        this.investor = this.principal.hasAnyAuthorityDirect(['ROLE_INVESTOR']);
        this.provider = this.principal.hasAnyAuthorityDirect(['ROLE_PROVIDER']);
        this.admin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
        this.previousMail = null;
        this.principal.identity().then(account => {
            //this.settingsAccount = this.copyAccount(account);
            this.settingsAccount.email = account.login;
            /* if (account.login !== account.email) {
				this.previousMail = account.login;
			} */
        });

        this.languageHelper.getAll().then(languages => {
            this.languages = languages;
        });

        const partnershipAmass = [];

        this.amassSettingService.setAuthorisedUrl().subscribe(data => {
            this.providerId = data;
            //console.log('Ranjith', this.providerId);
            //console.log(this.providerId[0].providerRecordID);
            if (this.providerId) {
                this.recordId = this.providerId[0].providerRecordID;
            }

            this.filterService.getLookupCodes('PARTNERSHIP_TYPE').subscribe(data => {
                const listItems = data && data['hits'] ? data['hits']['hits'] : [];
                listItems.map((val, index) => {
                    // console.log('......partnership mapping', val);
                    if (val._source.lookupcode !== 'Referral') {
                        partnershipAmass.push({
                            value: val._source.id,
                            label: val._source.lookupcode === 'Reseller' ? this.reseller : val._source.lookupcode
                        });
                    }
                });

                this.privacySecurityFields = [
                    {
                        key: 'orgvisibility',
                        type: 'radioButton',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Visibility of my Organizations:',
                            required: true,
                            //description: 'Who would you like to be able to see your organization\'s profile and resources?',
                            options: [
                                {
                                    value: 'Anyone (Public)',
                                    label: 'Anyone (Public)'
                                },
                                {
                                    value: 'Verified Users Only',
                                    label: 'Verified Users Only'
                                },
                                {
                                    value: 'Investment Managers Only',
                                    label: 'Investment Managers Only'
                                },
                                {
                                    value: 'Data Providers Only',
                                    label: 'Data Providers Only'
                                },
                                {
                                    value: 'People that I Directly Give Access to ',
                                    label: 'People that I Directly Give Access to '
                                },
                                {
                                    value: 'Just Me (Private & Anonymous)',
                                    label: 'Just Me (Private & Anonymous)'
                                }
                            ],
                            click: event => {
                                //this.googleAnalyticsService.emitEvent('Provider Profile Form - Visibility', 'Changed', 'Provider Profile Form - Dropdown - User Permissions');
                            }
                        },
                        hideExpression: this.formCheck('orgvisibility')
                    },
                    {
                        key: 'visibility',
                        type: 'radioButton',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Visibility of Me & My Comments:',
                            required: true,
                            //description: 'Who would you like to be able to see your personal name, profile and comments?',
                            options: [
                                {
                                    value: 'Anyone (Public)',
                                    label: 'Anyone (Public)'
                                },
                                {
                                    value: 'Verified Users Only',
                                    label: 'Verified Users Only'
                                },
                                {
                                    value: 'Data Providers Only',
                                    label: 'Data Providers Only'
                                },
                                {
                                    value: 'Investment Managers Only',
                                    label: 'Investment Managers Only'
                                },
                                {
                                    value: 'People that I Directly Give Access to',
                                    label: 'People that I Directly Give Access to'
                                },

                                {
                                    value: 'Just Me (Private & Anonymous)',
                                    label: 'Just Me (Private & Anonymous)'
                                }
                            ],
                            click: event => {
                                //this.googleAnalyticsService.emitEvent('Provider Profile Form - Visibility', 'Changed', 'Provider Profile Form - Dropdown - User Permissions');
                            }
                        },
                        hideExpression: this.formCheck('visibility')
                    },
                    {
                        key: 'partnership.id',
                        type: 'radioButton',
                        wrappers: ['form-field-horizontal'],
                        templateOptions: {
                            label: 'Type of Data Partnership with Amass:',
                            required: true,
                            options: partnershipAmass,
                            click: event => {
                                //this.googleAnalyticsService.emitEvent('Provider Profile Form - Visibility', 'Changed', 'Provider Profile Form - Dropdown - User Permissions');
                            }
                        },
                        hideExpression: this.formCheck('partnership')
                    }
                ];
            });
        });
        this.amassSettingService.checkUserValue().subscribe(data => {
            this.settingsAccount = data;
        });

        this.amassSettingService.checkUser().subscribe(data => {
            if (data) {
                this.basicModel = data;
                this.checkModel = data;
                this.checkModel = data ? data.id : null;
                this.userAuthoritiesValue = data;
                this.basicModel.dataRequirements = data.dataRequirements;
                this.basicModel.dataRequests = data.dataRequests;
                this.settingsAccount.notify_news_update = data.notify_news_update;
                //this.settingsAccount.authorities = null;
                //this.basicModel.categories = data.categories;
                if (data.categories) {
                    const investorTypes = [];
                    for (const obj in data.categories) {
                        if (obj) {
                            investorTypes.push({
                                id: data.categories[obj].id,
                                desc: data.categories[obj].datacategory
                            });
                        }
                    }
                    data.categories = investorTypes;
                }
                this.basicModel.categories = data.categories;
                this.basicModel.fundName = data.fundName;
                if (data.assetClasses) {
                    const assestTypes = [];
                    for (const obj in data.assetClasses) {
                        if (obj) {
                            assestTypes.push({
                                id: data.assetClasses[obj].id,
                                desc: data.assetClasses[obj].lookupcode
                            });
                        }
                    }
                    data.assetClasses = assestTypes;
                }
                this.basicModel.assetClasses = data.assetClasses;
                if (data.sectors) {
                    const sectorTypes = [];
                    for (const obj in data.sectors) {
                        if (obj) {
                            sectorTypes.push({
                                id: data.sectors[obj].id,
                                desc: data.sectors[obj].lookupcode
                            });
                        }
                    }
                    data.sectors = sectorTypes;
                }
                this.basicModel.sectors = data.sectors;
                if (data.geographies) {
                    const geographiesTypes = [];
                    for (const obj in data.geographies) {
                        if (obj) {
                            geographiesTypes.push({
                                id: data.geographies[obj].id,
                                desc: data.geographies[obj].lookupcode
                            });
                        }
                    }
                    data.geographies = geographiesTypes;
                }
                this.basicModel.geographies = data.geographies;
            }
        });
        this.amassSettingService.checkSubscribe().subscribe(data => {
            this.subscription = data;
        });
        this.amassSettingService.cardDetails().subscribe(data => {
            this.cardDetails = data;
            this.cardList = data;
        });
        this.stripeTest = this.fb.group({
            name: ['', [Validators.required]]
            //card: ['', [Validators.required]],
        });

        this.basicFields = [
            {
                key: 'geographies',
                type: 'multiselect',
                wrappers: ['form-field-horizontal'],
                templateOptions: {
                    label: 'Regions Interested In:',
                    placeholder: '',
                    //required: true,
                    options: this.geographicalFocus,
                    attributes: {
                        /*  'placeholderText' : 'The geographies that the data provider focuses on collecting/providing/analyzing data about.',
                        'labelTooltip': 'If a data provider covers numerous countries across continents, then it is multinational. If a data provider only covers one continent, certain countries or a specific region, then select each one. This is not the location of the company itself.',
                        'placeholderTooltip': 'If a data provider covers numerous countries across continents, then it is multinational. If a data provider only covers one continent, certain countries or a specific region, then select each one. This is not the location of the company itself.',
                        'lookupmodule':'GEOGRAPHICAL_FOCUS' */
                    }
                }
            },
            {
                key: 'categories',
                type: 'autocomplete',
                wrappers: ['form-field-horizontal'],
                templateOptions: {
                    label: 'Data Categories Interested In:',
                    placeholder: '',
                    options: this.dataCategories,
                    attributes: {
                        field: 'desc',
                        minLength: '1',
                        // 'placeholderText' : 'All the data categories this data provider collects/provides/analyzes.',
                        // 'labelTooltip': 'See "Data Categories" table for a definition of each data category. Always make sure the Main Data Category is included in here. Try to get up to 7 most appropriate data categories for each company. Some companies may have less than this and some other may have many more. In such unusual cases, seek assistance.',
                        multiple: 'true',
                        template: 'true',
                        dropdown: 'true'
                        //'placeholderTooltip': 'See "Data Categories" table for a definition of each data category. Always make sure the Main Data Category is included in here. Try to get up to 7 most appropriate data categories for each company. Some companies may have less than this and some other may have many more. In such unusual cases, seek assistance.'
                    },
                    change: (event: any) => {
                        console.log('event', event);
                        const queryObj = event;
                        console.log('queryobj', queryObj);
                        this.dataCategories = [];

                        const selected = this.basicModel['categories'];
                        ///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
                        if (selected && selected.length) {
                            this.selectedValue = selected.map(item => parseInt(item.id, 10));
                        }
                        console.log('queryobjquery', queryObj.query);
                        if (queryObj.query) {
                            console.log('queryobjquery1', queryObj.query);
                            this.dataCategories = [];
                            this.filterService.getAllDataCategories(queryObj.query).subscribe(d => {
                                const listItems = d ? d['hits']['hits'] : [];
                                for (let i = 0; i < listItems.length; i++) {
                                    const item = listItems[i]._source;
                                    if (
                                        listItems[i]._source.datacategory !== '' &&
                                        listItems[i]._source.datacategory !== 'DATA_CATEGORY_NULL'
                                    ) {
                                        if (!this.selectedValue.includes(item.id)) {
                                            this.dataCategories.push({
                                                id: item.id,
                                                desc: item.datacategory,
                                                img: item.logo ? item.logo.logo : null
                                            });
                                        }
                                    }
                                }

                                setTimeout(() => {
                                    this.basicFields[1].templateOptions.options = this.dataCategories;
                                }, 3000);
                            });
                        } else {
                            console.log('called');
                            this.dataCategories = [];
                            this.filterService.getCategoriesForFilter().subscribe(data => {
                                //const listItems = d ? d['hits']['hits'] : [];
                                const listItems =
                                    data['aggregations'] && data['aggregations'].categories ? data['aggregations'].categories.buckets : [];
                                for (let i = 0; i < listItems.length; i++) {
                                    //const item = listItems[i]._source;
                                    const category = listItems[i];
                                    const categoryId = parseInt(category.key, 10);
                                    const categoryList = category.categories.hits.hits[0]._source.categories;
                                    const filteredCategoryDescription = categoryList.filter(item => item.id === category.key);
                                    if (category.categories.hits.hits[0]._source.categories !== 'DEFAULT_NULL') {
                                        if (!this.selectedValue.includes(categoryId)) {
                                            this.dataCategories.push({
                                                id: categoryId,
                                                desc:
                                                    filteredCategoryDescription &&
                                                    filteredCategoryDescription[0] &&
                                                    filteredCategoryDescription[0].desc
                                                        ? filteredCategoryDescription[0].desc
                                                        : '',
                                                img: categoryList.logo ? categoryList.logo.logo : null
                                            });
                                        }
                                    }
                                }
                                setTimeout(() => {
                                    this.basicFields[1].templateOptions.options = this.dataCategories;
                                }, 3000);
                            });
                        }
                    },
                    blur: (event: any) => {
                        if (event && event.target && !event['selectedVal']) {
                            event.target.value = '';
                        }
                    }
                }
            },
            {
                key: 'dataRequirements',
                type: 'input',
                wrappers: ['form-field-horizontal'],
                templateOptions: {
                    label: 'Additional Data Requirements: ',
                    type: 'input',
                    placeholder: '',
                    //description: 'What is the link to the home webpage of your brand that acts as a data provider?',
                    attributes: {
                        //'placeholderText' : 'Link to the website of the data provider or the division within the company/organization that provides/analyzes data.',
                        //	'labelTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                        // 'descriptionTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                        // 'placeholderTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                        //   'trackNames': 'Provider Website',
                        // 'trackCategory': 'Provider Profile Admin - Textbox',
                        // 'trackLabel': 'Provider Profile Admin - Input'
                    },
                    //change: this.basicInput
                    keypress: event => {}
                }
            },
            {
                key: 'dataRequests',
                type: 'input',
                wrappers: ['form-field-horizontal'],
                templateOptions: {
                    label: 'Additional Data Requests: ',
                    type: 'input',
                    placeholder: '',
                    //description: 'What is the link to the home webpage of your brand that acts as a data provider?',
                    attributes: {
                        //'placeholderText' : 'Link to the website of the data provider or the division within the company/organization that provides/analyzes data.',
                        //	'labelTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                        // 'descriptionTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                        // 'placeholderTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                        //   'trackNames': 'Provider Website',
                        // 'trackCategory': 'Provider Profile Admin - Textbox',
                        // 'trackLabel': 'Provider Profile Admin - Input'
                    },
                    //change: this.basicInput
                    keypress: event => {}
                }
            }
        ];
        this.providerRelevantAssetClass = [];
        this.filterService.getRelatedAssetClass().subscribe(data => {
            const listItems = data ? data['hits']['hits'] : [];
            //console.log('List Items',listItems);
            //listItems.shift();
            //console.log('List Items 1',listItems);
            for (let i = 0; i < listItems.length; i++) {
                //console.log('4',this.providerRelevantAssetClass[0]);
                //this.providerRelevantAssetClass.shift();
                if (listItems[i]._source.description !== 'DEFAULT_NULL') {
                    this.providerRelevantAssetClass.push({
                        value: {
                            id: listItems[i]._source.id,
                            desc: listItems[i]._source.description
                        },
                        label: listItems[i]._source.description
                    });
                }
            }
            this.investigationFields[1].templateOptions.options = this.providerRelevantAssetClass;
        });
        this.geographicalFocus = [];
        this.filterService.getGeographicRegion().subscribe(data => {
            const listItems = data ? data['hits']['hits'] : [];
            for (let i = 0; i < listItems.length; i++) {
                this.geographicalFocus.push({
                    value: {
                        id: listItems[i]._source.id,
                        desc: listItems[i]._source.lookupcode
                    },
                    label: listItems[i]._source.lookupcode
                });
            }
            this.basicFields[0].templateOptions.options = this.geographicalFocus;
            //this.investigationFields[2].templateOptions.options = this.geographicalFocus;
            //this.categorizationsFields[10].templateOptions.options = this.geographicalFocus;
        });
        this.relevantSectors = [];
        this.filterService.getLookupCodes('RLVNT_SECTOR').subscribe(data => {
            const listItems = data ? data['hits']['hits'] : [];
            for (let i = 0; i < listItems.length; i++) {
                this.relevantSectors.push({
                    value: {
                        id: listItems[i]._source.id,
                        desc: listItems[i]._source.lookupcode
                        //desc: listItems[i]._source.description,
                        //locked: listItems[i]._source.locked ? listItems[i]._source : false
                    },
                    label: listItems[i]._source.lookupcode
                });
            }
            this.investigationFields[2].templateOptions.options = this.relevantSectors;
        });
    }
    formCheck(message: string) {
        if (message === 'orgvisibility') {
            if (this.providerId === 'null' || !this.providerId) {
                return true;
            }
        }

        if (message === 'visibility') {
            if (this.admin) {
                return true;
            }
        }
        if (message === 'partnership') {
            if (this.providerId === 'null' || !this.providerId) {
                return true;
            }
        }
    }
    ngAfterViewInit() {
        this.stripeService
            .elements({
                locale: 'en'
            })
            .subscribe(elements => {
                this.elements = elements;
                // Only mount the element the first time
                if (!this.card) {
                    this.card = this.elements.create('card', {
                        style: {
                            base: {
                                iconColor: '#666EE8',
                                color: '#31325F',
                                lineHeight: '40px',
                                fontWeight: 300,
                                //   fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                                fontSize: '18px',
                                '::placeholder': {
                                    color: '#CFD7E0'
                                }
                            }
                            /* invalid: {
                            color: '#fa755a',
                            iconColor: '#fa755a'
                          } */
                        }
                    });
                    if (this.cardInfo && this.cardInfo.nativeElement) {
                        this.card.mount(this.cardInfo.nativeElement);
                    }
                    this.check = this.card;
                    this.check.addEventListener('change', function(event) {
                        const displayError = document.getElementById('card-errors');
                        if (event.error) {
                            displayError.textContent = event.error.message;
                        } else {
                            displayError.textContent = '';
                        }
                    });
                }
            });
    }
    ngOnDestroy() {
        // this.check.destroy();
    }
    doSomething() {
        this.router.navigate(['/providers', this.providerId[0].providerRecordID, 'edit']);
    }
    doSomethingProfile() {
        this.router.navigate(['/providers', this.providerId[0].providerRecordID]);
    }
    doPassword() {
        this.router.navigate(['/password']);
    }

    formSave() {}
    submitProviderEdit(model) {
        //this.save();
        console.log('notification', this.settingsAccount.notify_news_update, model);
        model.notify_news_update = this.settingsAccount.notify_news_update;
        this.amassSettingService.save(model).subscribe(data => {
            if (data) {
                this.openSnackBar('User Details Saved Successfully', 'Close');
            }
        });
    }
    submitInvestor(model) {
        this.amassSettingService.saveInvestor(model).subscribe(data => {
            if (data) {
                this.openSnackBar('Investor Details saved successfully', 'Close');
            }
        });
    }

    mailUpdate() {
        this.principal.identity().then(account => {
            this.settingsAccount = this.copyAccount(account);
            this.settingsAccount.email = account.login;
        });
    }
    recordIdLink(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    onChange(event) {
        // can't event.preventDefault();
        // console.log('onChange event.checked '+ event.checked);
        this.unsubscribeValue = event.checked;
        console.log('onChange' + this.unsubscribeValue, this.settingsAccount.notify_news_update);
        if (this.unsubscribeValue === false) {
            this.mdDialogRef = this.dialog.open(SettingsComponentComponent, {
                width: '400px',
                data: { name: this.settingsAccount }
            });
            this.mdDialogRef.componentInstance.accounts = this.settingsAccount;
            this.mdDialogRef.afterClosed().subscribe(() => {
                this.success = this.mdDialogRef.componentInstance.success;
            });
        }
    }
    save() {
        if (
            (this.settingsAccount.notify_news_update === false && this.unsubscribeValue === true) ||
            (this.settingsAccount.notify_news_update === true && this.unsubscribeValue === true)
        ) {
            // console.log('First', this.settingsAccount.notify_news_update === false && this.unsubscribeValue === true);
            // console.log('Second', this.settingsAccount.notify_news_update === true && this.unsubscribeValue === true);
            // console.log('onChange 1' + this.unsubscribeValue);
            // console.log('onChange 2' + this.settingsAccount.notify_news_update);
            this.account.save(this.settingsAccount).subscribe(
                () => {
                    this.error = null;
                    this.success = 'OK';
                    this.principal.identity(true).then(account => {
                        this.settingsAccount = this.copyAccount(account);
                        this.updatedMail = this.settingsAccount.email;
                        //  console.log(account.login, account.email, account.login !== account.email);
                        if (account.login !== account.email) {
                            this.previousMail = account.login;
                            this.mailUpdate();
                            this.googleAnalyticsService.emitEvent('Account Settings', 'Confirm', 'Account Settings - Textbox - Email');
                        } else {
                            this.previousMail = null;
                        }
                    });
                    this.languageService.getCurrent().then(current => {
                        if (this.settingsAccount.langKey !== current) {
                            this.languageService.changeLanguage(this.settingsAccount.langKey);
                        }
                    });
                },
                response => this.processError(response)
                // () => {
                //     this.success = null;
                //     this.error = 'ERROR';
                // }
            );
        } else if (this.unsubscribeValue === false) {
            this.mdDialogRef = this.dialog.open(SettingsComponentComponent, {
                width: '400px',
                data: { name: this.settingsAccount }
            });
            this.mdDialogRef.componentInstance.accounts = this.settingsAccount;
            this.mdDialogRef.afterClosed().subscribe(() => {
                this.success = this.mdDialogRef.componentInstance.success;
            });
        }
    }
    savecard() {
        this.amassSettingService.savecard(this.cardList).subscribe(data => {
            response => this.processError(response);
        });
    }
    private processError(response) {
        this.success = null;
        this.error = 'ERROR';
        if (response.status === 400) {
            //console.log('Inside');
            this.errorUserExists = 'ERROR';
        }
        if (response.status === 500) {
            //console.log('Inside');
            this.cardErrorExists = 'ERROR';
        }
    }
    saveSetting(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    copyAccount(account) {
        return {
            activated: account.activated,
            email: account.email,
            firstName: account.firstName,
            langKey: account.langKey,
            lastName: account.lastName,
            login: account.login,
            imageUrl: account.imageUrl,
            company: account.company,
            notify_news_update: account.notify_news_update
        };
    }
    openSnackBar(message: string, action: string, delay?: number) {
        this.snackBar.open(message, action, {
            duration: delay ? delay : 10000,
            panelClass: ['blue-snackbar']
        });
    }
    buy(event: any) {
        const name = this.stripeTest.get('name').value;
        this.stripeService.createToken(this.card, { name }).subscribe(result => {
            if (result.token) {
                this.tokenId = result.token.id;
                this.amassSettingService.savenewcard(this.tokenId).subscribe(data => {
                    if (data) {
                        this.openSnackBar('Card Details Saved Successfully', 'Close');
                    }
                    response => this.processError(response);
                });
            } else if (result.error) {
                // Error creating the token
                this.error = result.error.message;
            }
        });
    }
    submit(model, form?: string, skipRedirect?: boolean) {
        // const formName ='Investor';
        const submitMessage = 'Your edits have been successfully submitted.';
        const filterArr = Array.prototype.filter;
        if (form && this[form].valid) {
            const autoCompleteFields = this.el.nativeElement.querySelectorAll('#' + form + ' .ui-autocomplete-input');
            let invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
            const filteredAutoCompleteFields = filterArr.call(autoCompleteFields, function(node) {
                return node.value === '';
            });
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
            } else {
                delete model['id'];
                model['id'] = this.checkModel;
                //model = this.checkModel;
                this.amassSettingService.save(model).subscribe(
                    data => {
                        if (data) {
                            this.openSnackBar(submitMessage, 'close');
                        } else {
                            this.openSnackBar('form' + ' failed to save!', 'Close');
                        }
                    },
                    error => {
                        if (error.status === 401) {
                            this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
                        } else {
                            this.openSnackBar('form' + ' failed to save!', 'Close');
                        }
                    }
                );
            }
        } else {
            let invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
            let autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
            let filteredAutoCompleteFields = filterArr.call(autoCompleteFields, function(node) {
                return node.required && node.value === '';
            });
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
            } else if (invalidElements.length > 0) {
                if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                    invalidElements[0].querySelectorAll('input')[0].focus();
                } else {
                    setTimeout(() => {
                        invalidElements[0].focus();
                    }, 1000);
                }
                this.openSnackBar('Please fill all the required fields!', 'Close');
            } else {
                delete model['id'];
                model['id'] = this.checkModel;
                this.amassSettingService.save(model).subscribe(
                    data => {
                        if (data) {
                            this.openSnackBar(submitMessage, 'Close');
                        } else {
                            // console.log(data);
                            //this.formSubmitted = false;
                            this.openSnackBar('form' + ' failed to save!', 'Close');
                        }
                    },
                    error => {
                        if (error.status === 401) {
                            //this.formSubmitted = false;
                            this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
                        } else {
                            this.openSnackBar('form' + ' failed to save!', 'Close');
                        }
                    }
                );
            }
        }
    }
    onExpireSession(event) {
        //console.log('eventcheck',event);
        if (event) {
            //console.log('eventcheck',event);
            //this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
                setTimeout(() => {
                    this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
                }, 2000);
            });
        }
    }
    beforeSessionExpires(event) {
        if (event) {
            //this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }
}

@Component({
    selector: 'jhi-setting-confirm',
    templateUrl: '../../dialog/confirmation.component.html',
    styleUrls: ['../../dialog/dialog.component.css']
})
export class SettingsComponentComponent implements OnInit {
    settingsAccount: any;
    error: string;
    success: string;
    public accounts: any;
    settingValidation: any;
    previousMail: any;
    currentMail: any;

    constructor(
        public dialogRef: MatDialogRef<SettingsComponentComponent>,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private accountService: AccountService,
        private principal: Principal,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}
    ngOnInit() {
        this.settingValidation = false;
        this.previousMail = null;
    }

    closeLoginDialog(events: any) {
        this.dialogRef.close();
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    confirm(data: any) {
        this.accountService.save(this.accounts).subscribe(
            () => {
                this.error = null;
                this.success = 'OK';
                this.settingValidation = true;
                this.principal.identity(true).then(account => {
                    this.accounts = this.copyAccount(account);
                    if (account.login !== account.email) {
                        this.previousMail = account.login;
                        this.currentMail = account.email;
                        this.googleAnalyticsService.emitEvent('Account Settings', 'Confirm', 'Account Settings - Textbox - Email');
                    } else {
                        this.previousMail = null;
                    }
                });
                this.close();
            },
            () => {
                this.success = null;
                this.error = 'ERROR';
            }
        );
    }
    confirmUnsubscribe(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    close() {
        this.dialogRef.close();
    }
    cancel(events: any) {
        this.googleAnalyticsService.emitEvent(events.label, events.category, events.event);
    }

    copyAccount(account) {
        return {
            activated: account.activated,
            email: account.email,
            firstName: account.firstName,
            langKey: account.langKey,
            lastName: account.lastName,
            login: account.login,
            imageUrl: account.imageUrl,
            company: account.company,
            notify_news_update: account.notify_news_update
        };
    }
}
