import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SessionsService {

    private resourceUrl = 'api/account/sessions/';
    constructor(private http: HttpClient) { }

    findAll(): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(series: string): Observable<any> {
        return this.http.delete(`${this.resourceUrl}${series}`);
    }
}
