import { Route } from '@angular/router';
import { AccountActivationComponent } from './account-activation.component';

export const accountActivationRoute: Route = {
    path: 'activationUser',
    component: AccountActivationComponent,
    data: {
        authorities: [],
        pageTitle: 'Account Activation Page'
    }
};