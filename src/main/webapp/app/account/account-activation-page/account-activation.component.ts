import { Component, OnInit, AfterViewInit, Renderer2, ElementRef, Inject, ViewChild, EventEmitter, Output } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { JhiEventManager, JhiLanguageService } from 'ng-jhipster';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';

import { SigninDialogComponent } from '../../dialog/signindialog.component';
import { NgForm } from '@angular/forms';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { CookieService } from 'ngx-cookie';
import { AccountActivationService } from './account-activation.service';
import { LoginService, StateStorageService, CSRFService,Principal } from 'app/shared';

@Component({
    selector: 'jhi-app-account-activation',
    templateUrl: './account-activation.component.html',
    styleUrls: ['./account-activation.component.css']
})
export class AccountActivationComponent implements OnInit, AfterViewInit {
    confirmPassword: string;
    doNotMatch: string;
    error: string;
    errorEmailExists: string;
    errorUserExists: string;
    accountActivation: any;
    success: boolean;
    validationMessage: string;
    strengthBarValidationMessage: any;
    agreeValidationMessage: any;
    errorMsgValidation: any;
    mdDialogRef: MatDialogRef<any>;
    dialogRef: MatDialogRef<any>;
    @ViewChild(MatInput) input;
    modelValue: any;
    csrf: string;
    agreeTerms: boolean;
    companyName: string;
    passwordStrengthBar: boolean;
    userErrorMessage: any;
    providerUserPermission: any;

    constructor(
        private languageService: JhiLanguageService,
        private loginService: LoginService,
        private eventManager: JhiEventManager,
        private accountActivationService: AccountActivationService,
        private stateStorageService: StateStorageService,
        private elementRef: ElementRef,
        private renderer: Renderer2,
        private router: Router,
        private route: ActivatedRoute,
        public mdDialog: MatDialog,
        public dialog: MatDialog,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private cookieService: CookieService,
        private csrfService: CSRFService,
        private principal: Principal,
	) {

	}

    ngOnInit() {
        this.accountActivation = {};
        this.providerUserPermission = [];
        this.route.queryParams.subscribe((params) => {
			this.accountActivationService.get(params['key']).subscribe((data) => {
                this.accountActivation = data;
				this.error = null;
				//this.success = 'OK';
			}, () => {
				this.success = null;
                this.error = 'ERROR';
			});
		});
        this.success = false;
        this.accountActivation.notify_news_update = true;
        this.validationMessage = null;
        this.strengthBarValidationMessage = null;
        this.agreeValidationMessage = null;
        this.errorMsgValidation = '';
        this.agreeTerms = false;
        this.accountActivation.email = '';
        this.accountActivation.password = '';
         if (this.companyName === 'ROLE_INVESTOR') {
            this.accountActivation.authorities = 'ROLE_INVESTOR';
         } else if (this.companyName === 'ROLE_PROVIDER') {
            this.accountActivation.authorities = 'ROLE_PROVIDER';
         } else {
            this.accountActivation.authorities = '';
         }
	}

    ngAfterViewInit() {
        // this.input.focus();
        // setTimeout(() => {
        //     this.input.focus();
        // }, 1000);
         this.renderer.selectRootElement('#password').focus();
    }
/* linkedin component terms validation*/
    agreeCheck(event) {
        this.agreeTerms = event.checked;
    }
    change(message: any) {
        this.errorMsgValidation = message;
    }
    onKeydown(password) {
        const values = password;
        if (values) {
            this.passwordStrengthBar = true;
        }
    }

    register(registerForm: NgForm) {
        this.accountActivationService.setAuthorised(this.accountActivation.id).subscribe(data => {
            this.providerUserPermission = data;
        });
        if (registerForm.valid) {
            this.validationMessage = null;
            this.strengthBarValidationMessage = null;
            this.agreeValidationMessage = null;
            if (!this.accountActivation.agreeTerms) {
                this.agreeValidationMessage = 'Please agree to our Privacy Policy and Terms of Service to activate your account.';
                this.renderer.selectRootElement('#password').focus();
            } else if (this.accountActivation.password !== this.confirmPassword) {
                this.doNotMatch = 'ERROR';
                this.error = null;
                this.errorUserExists = null;
                this.errorEmailExists = null;
                this.renderer.selectRootElement('#password').focus();
            } else if (this.errorMsgValidation !== '') {
                this.strengthBarValidationMessage = this.errorMsgValidation;
            } else {
                this.doNotMatch = null;
                this.error = null;
                this.errorUserExists = null;
                this.errorEmailExists = null;
                this.languageService.getCurrent().then(key => {
                    this.accountActivation.langKey = key;
                    this.accountActivation.login = this.accountActivation.email;
                    this.accountActivation.authorities = [this.accountActivation.authorities];
                    this.accountActivationService.save(this.accountActivation).subscribe(
                        () => {
                            //this.success = true;
                            if (this.accountActivation.notify_news_update) {
                            }
                            if (this.accountActivation.email && this.accountActivation.password) {
                                this.loginService.login({
                                   // username: this.username,
                                    username: this.accountActivation.email,
                                    password: this.accountActivation.password,
                                   // rememberMe: this.rememberMe
                                }).then(() => {
                                   // this.authenticationError = false;
                                   // this.dialogRef.close();
                                    if (this.router.url === '/register' || (/^\/activate\//.test(this.router.url)) ||
                                        (/^\/reset\//.test(this.router.url))) {
                                        this.router.navigate(['']);
                                    }

                                    this.eventManager.broadcast({
                                        name: 'authenticationSuccess',
                                        content: 'Sending Authentication Success'
                                    });

                                    // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
                                    // // since login is succesful, go to stored previousState and clear previousState
                                    const redirect = this.stateStorageService.getUrl();
                                    if (redirect) {
                                        if (redirect.indexOf('?') !== -1) {
                                            const redirectParams = this.parseQueryString(redirect);
                                            this.stateStorageService.storeUrl(null);
                                            this.router.navigate([redirect.split('?')[0]], {queryParams: redirectParams});
                                        } else {
                                            this.stateStorageService.storeUrl(null);
                                            this.router.navigate([redirect]);
                                        }
                                    } else {
                                        const isInvestor = this.principal.hasAnyAuthorityDirect(['ROLE_INVESTOR']);
					const isProvider = this.principal.hasAnyAuthorityDirect(['ROLE_PROVIDER']);
                                               if(this.providerUserPermission!=null)
                                                 {
                                          this.router.navigate(['/providers', this.providerUserPermission[0].providerRecordID,'edit'])
                                                 }
                                                 else if(isInvestor || isProvider)
                                                 {
                                                    this.router.navigate(['/get-started']);
                                                 }
                                                 else{
                                            this.router.navigate(['/providers']);
                                                 }

                                    }
                                }, error => {
                                    // setTimeout(() => {
                                    //     this.csrf = this.csrfService.getCSRF();
                                    // }, 1000);
                                    // this.inactiveUser = null;
                                    // this.authenticationError = true;
                                    // console.log('error');
                                    // this.http.get('api/ur/user-status?loginID=' + this.username, {observe: 'response', responseType: 'text'}).subscribe(res => {
                                    //     console.log(res);
                                    //     if (res.headers.get('x-datamarketplaceapp-params') !== 'Authentication failed' && error.status !== 401 && error.status !== 409) {
                                    //         this.inactiveUser = null;
                                    //         this.authenticationError = false;
                                    //         return;
                                    //     } else if (error.status === 409) {
                                    //         this.existingUserAuthentication = true;
                                    //         this.authenticationError = false;
                                    //     }
                                    //      else {
                                    //         this.inactiveUser = null;
                                    //         this.authenticationError = true;
                                    //         this.existingUserAuthentication = false;
                                    //         this.authenticationErrorMessage = 'Incorrect login email and/or password. Please try again.';
                                    //         return;
                                    //     }
                                    // }, err => {
                                    //     this.inactiveUser = err;
                                    //     this.authenticationError = false;
                                    //     return;
                                    // });
                                    // this.authenticationErrorMessage = 'Incorrect login email and/or password. Please try again.';
                                }).catch(error => {
                    
                    
                                });
                            }
                        },
                        response => this.processError(response)
                    );
                });
            }
        } else {
            this.validationMessage = 'Please fill in all required fields to activate your account.';
            this.renderer.selectRootElement('#password').focus();
        }
    }
    valuechange(newValue) {
        this.modelValue = newValue;
        if (this.modelValue.length < 1) {
            this.doNotMatch = null;
        }
    }
    parseQueryString(queryString: string): {} {
		let myquery;
		const params = {};
		// if the query string is NULL
		if (queryString === null || !queryString) {
			queryString = window.location.search.substring(1);
			return params;
		}
		myquery = queryString.split('?');
		const queries = myquery[1].split('&');
		queries.forEach((indexQuery: string) => {
			const indexPair = indexQuery.split('=');
			const queryKey = decodeURIComponent(indexPair[0]);
			const queryValue = decodeURIComponent(indexPair.length > 1 ? indexPair[1] : '');
			params[queryKey] = queryValue;
		});

		return params;
	}
    private processError(response) {
        this.success = null;
        if (response.status === 400 && response.error === 'login already in use') {
            this.errorUserExists = 'ERROR';
            this.renderer.selectRootElement('#firstName').focus();
        } else if (response.status === 400 && response.error === 'email address already in use') {
            this.errorEmailExists = 'ERROR';
            this.renderer.selectRootElement('#firstName').focus();
        } else if(response.status === 500){
            this.userErrorMessage = 'ERROR';
        } else {
            this.error = 'ERROR';
            this.renderer.selectRootElement('#firstName').focus();
        }
    }

    // apply() {
    //     this.mdDialog.closeAll();

    //     this.dialogRef = this.mdDialog.open(ApplyAccountComponent, {
    //         width: '400px'
    //     });
    // }

    openSigninDialog(events: any) {
        this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
            width: '400px'
        });
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    newsletter(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    signUp(events) {
         this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    termsOfService(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    closeSignUpDialog(events: any) {
        this.mdDialog.closeAll();
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }

    roleValueChange(event) {
        this.cookieService.put('ROLE', event.value);
    }
    terms(){
      //  this.router.navigate(['/terms']);
        window.open('/#/terms', '_blank');
    }
    privacyPolicy(){
       // this.router.navigate(['/privacy-policy']);
        window.open('/#/privacy-policy', '_blank');
    }
}
