import { Injectable } from '@angular/core';
// import { Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable()
export class AccountActivationService {
    headers: any;
    private accountUrl='api/ur/useroperator-permission';

    constructor(private http: HttpClient) {
        this.headers = new HttpHeaders({
            'Content-Type': 'multipart/form-data',
            'cache-control': 'no-cache'
        });
    }

    get(key: string): Observable<any> {
        const params: HttpParams = new HttpParams();
        params.set('key', key);

        return this.http.get('api/ur/activationUser', {
            params: {
                'key': key
            }
        });
    }

    save(account: any): Observable<any> {
        return this.http.put('api/ur/userAccounts', account);
    }

    setAuthorised(id:any):Observable<any>
    {
        return this.http.get(`${this.accountUrl}/${id}`);

    }
}
