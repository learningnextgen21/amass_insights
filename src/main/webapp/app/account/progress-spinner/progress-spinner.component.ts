import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-progress-spinner',
  templateUrl: './progress-spinner.component.html',
  styleUrls: ['./progress-spinner.component.scss']
})
export class ProgressSpinnerComponent implements OnInit {
  @Input() panelName: string;

  constructor() { }

  ngOnInit() {
    console.log('panelprogress',this.panelName);
  }
}