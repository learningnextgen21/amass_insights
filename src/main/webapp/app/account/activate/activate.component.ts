import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { ActivateService } from './activate.service';
import { LoginModalService } from '../../shared';
import { SigninDialogComponent } from '../../dialog/signindialog.component';

@Component({
	selector: 'jhi-activate',
	templateUrl: './activate.component.html'
})
export class ActivateComponent implements OnInit {
	error: string;
	success: string;

	constructor(
		private activateService: ActivateService,
		private loginModalService: LoginModalService,
		private route: ActivatedRoute,
		public mdDialog: MatDialog
	) {
	}

	ngOnInit() {
		this.route.queryParams.subscribe((params) => {
			this.activateService.get(params['key']).subscribe(() => {
				this.error = null;
				this.success = 'OK';
			}, () => {
				this.success = null;
				this.error = 'ERROR';
			});
		});
	}

	login() {
		this.mdDialog.closeAll();

		this.mdDialog.open(SigninDialogComponent, {
			width: '400px'
		});
	}
}
