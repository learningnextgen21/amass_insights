import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule, AccountService } from '../shared';
import { AmassSettingService } from './settings/settings.service';
import { ReactiveFormsModule } from '@angular/forms';
import { UserOnboardingService } from './user-onboarding/user-onboarding.service';

import {
	ApplyAccountService,
	ActivateService,
	ConfirmService,
	EmailActionService,
	PasswordService,
	PasswordResetInitService,
	PasswordResetFinishService,
	SessionsService,
	SessionsComponent,
	ActivateComponent,
	ConfirmComponent,
	EmailActionComponent,
	// PasswordComponent,
	PasswordResetInitComponent,
	// PasswordResetFinishComponent,
	SettingsComponent,
	SocialRegisterComponent,
	accountState,


} from './';
//import { AccountActivationComponent } from './account-activation-page/account-activation.component';
import { AccountActivationService } from './account-activation-page/account-activation.service';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyHorizontalWrapperComponent } from 'app/entities/data-provider/horizontal-wrapper';
import {
    FormlyEditorFieldComponent,
    FormlyAutoCompleteFieldComponent,
    FormlyMultiSelectFieldComponent,
    FormlyDropdownFieldComponent,
    FormlyMaskFieldComponent,
    FormlySliderFieldComponent,
    FormlyRadioButtonFieldComponent,
    FormlyTextAreaFieldComponent,
} from './../shared';
import { FormlyFieldCustomInput } from 'app/shared/custom-form-fields/input-form-field.component';
import { UserOnboardingComponent } from './user-onboarding/user-onboarding.component';
import { FormlyFieldMultiCheckbox } from '@ngx-formly/bootstrap';
import { ProviderUserOnbardingFormModule } from './provider-user-onboarding/provider-user-onboarding.module';
export function maxlengthValidationMessage(err, field) {
    return `This field has a maximum length of  ${field.templateOptions.maxLength}  characters. Please shorten this text to fit under this length.`;
  }
@NgModule({
	imports: [
		DatamarketplaceSharedModule,
		ReactiveFormsModule,
		RouterModule.forRoot(accountState, { useHash: true }),
		FormlyModule.forRoot({
            wrappers: [{ name: 'form-field-horizontal', component: FormlyHorizontalWrapperComponent  }],
            validationMessages: [
				{ name: 'required', message: 'This field is required' },
				{ name: 'maxlength', message: maxlengthValidationMessage },

            ],
            types: [{
					name: 'autocomplete', component: FormlyAutoCompleteFieldComponent
				},
				{
					name: 'input', component: FormlyFieldCustomInput
				},
				{
					name:'radioButton', component: FormlyRadioButtonFieldComponent
				}
			]
		}),
        ProviderUserOnbardingFormModule
		],
	declarations: [
		SocialRegisterComponent,
		ActivateComponent,
	//	AccountActivationComponent,
		ConfirmComponent,
		EmailActionComponent,
	    // PasswordComponent,
		PasswordResetInitComponent,
		// PasswordResetFinishComponent,
		SessionsComponent,
		SettingsComponent,
		UserOnboardingComponent
	],
	providers: [
		SessionsService,
		ApplyAccountService,
		AccountActivationService,
		ActivateService,
		ConfirmService,
		EmailActionService,
		AccountService,
		PasswordService,
		PasswordResetInitService,
        PasswordResetFinishService,
		AmassSettingService,
		UserOnboardingService
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceAccountModule {}
