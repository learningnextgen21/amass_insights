import { Routes } from '@angular/router';

import {
	activateRoute,
	confirmRoute,
	emailActionRoute,
	passwordRoute,
	passwordResetFinishRoute,
	passwordResetInitRoute,
	sessionsRoute,
	socialRegisterRoute,
	settingsRoute,
} from './';
import { accountActivationRoute } from './account-activation-page/account-activation.route';
import { providerUserOnboardingRoute } from './provider-user-onboarding/provider-user-onboarding.route';
import { userOnboardingRoute } from './user-onboarding/user-onboarding.route';

const ACCOUNT_ROUTES = [
	activateRoute,
	accountActivationRoute,
	confirmRoute,
	emailActionRoute,
	passwordRoute,
	passwordResetFinishRoute,
	passwordResetInitRoute,
	sessionsRoute,
	socialRegisterRoute,
	settingsRoute,
	userOnboardingRoute,
    providerUserOnboardingRoute

];

export const accountState: Routes = [{
	path: '',
	children: ACCOUNT_ROUTES
}];
