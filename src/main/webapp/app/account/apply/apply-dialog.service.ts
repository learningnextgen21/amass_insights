import { Injectable, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { ApplyAccountComponent } from './apply.component';

@Injectable()
export class ApplyDialogService {
	private isOpen = false;
	constructor(
		public mdDialog: MatDialog
	) {}

	open(): void {
		const dialogRef = this.mdDialog.open(ApplyAccountComponent, {
			width: '570px'
		});
	}
}
