import { Component, OnInit, AfterViewInit, Renderer2, ElementRef, Inject, ViewChild, EventEmitter, Output } from '@angular/core';

import { Router } from '@angular/router';
import { JhiEventManager, JhiLanguageService } from 'ng-jhipster';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';

import { ApplyAccountService } from './apply.service';
import { SigninDialogComponent } from '../../dialog/signindialog.component';
import { FormControl, NgForm, Validators } from '@angular/forms';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { CookieService } from 'ngx-cookie';
import { MatSelect } from '@angular/material/select';

@Component({
    selector: 'jhi-app-apply-dialog',
    templateUrl: './apply.component.html',
    styleUrls: ['./apply.component.css']
})
export class ApplyAccountComponent implements OnInit, AfterViewInit {
    confirmPassword: string;
    doNotMatch: string;
    error: string;
    errorEmailExists: string;
    errorUserExists: string;
    registerAccount: any;
    success: boolean;
    validationMessage: string;
    strengthBarValidationMessage: any;
    agreeValidationMessage: any;
    errorMsgValidation: any;
    mdDialogRef: MatDialogRef<any>;
    dialogRef: MatDialogRef<any>;
    @ViewChild(MatInput) input;
    @ViewChild(MatSelect) userRole: MatSelect; 
    modelValue: any;
    csrf: string;
    agreeTerms: boolean;
    companyName: string;
    passwordStrengthBar: boolean;
    isClickedOnce: boolean = false;
    selectedRole = new FormControl('', Validators.required);

    constructor(
        private languageService: JhiLanguageService,
        private eventManager: JhiEventManager,
        private applyService: ApplyAccountService,
        private elementRef: ElementRef,
        private renderer: Renderer2,
        private router: Router,
        public mdDialog: MatDialog,
        public dialog: MatDialog,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private cookieService: CookieService
	) {

	}

    ngOnInit() {
        this.success = false;
        this.registerAccount = {};
        this.registerAccount.notify_news_update = true;
        this.validationMessage = null;
        this.strengthBarValidationMessage = null;
        this.agreeValidationMessage = null;
        this.errorMsgValidation = '';
        this.agreeTerms = false;
        this.registerAccount.email = '';
        this.registerAccount.password = '';
        //  if (this.companyName === 'ROLE_INVESTOR') {
        //     this.registerAccount.authorities = 'ROLE_INVESTOR';
        //  } else if (this.companyName === 'ROLE_PROVIDER') {
        //     this.registerAccount.authorities = 'ROLE_PROVIDER';
        //  } else {
        //     this.registerAccount.authorities = '';
        //  }
	}

    ngAfterViewInit() {
        this.input.focus();
        setTimeout(() => {
            this.input.focus();
             if (this.companyName === 'ROLE_INVESTOR') {
                this.userRole.writeValue('ROLE_INVESTOR');
                this.selectedRole.setValue('ROLE_INVESTOR');
            } else if (this.companyName === 'ROLE_PROVIDER') {
                this.userRole.writeValue('ROLE_PROVIDER');
                this.selectedRole.setValue('ROLE_PROVIDER');
            } else {
                this.userRole.writeValue('');
            }
        }, 1000);
        /* this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#firstName'), 'focus', []); */
    }
/* linkedin component terms validation*/
    agreeCheck(event) {
        this.agreeTerms = event.checked;
    }
    change(message: any) {
        this.errorMsgValidation = message;
    }
    onKeydown(password) {
        const values = password;
        if (values) {
            this.passwordStrengthBar = true;
        }
    }

    register(registerForm: NgForm) {
        if (registerForm.valid) {
            this.validationMessage = null;
            this.strengthBarValidationMessage = null;
            this.agreeValidationMessage = null;
            if (!this.registerAccount.agreeTerms) {
                this.agreeValidationMessage = 'Please agree to our Privacy Policy and Terms of Service to sign up.';
                this.renderer.selectRootElement('#firstName').focus();
            } else if (this.registerAccount.password !== this.confirmPassword) {
                this.doNotMatch = 'ERROR';
                this.error = null;
                this.errorUserExists = null;
                this.errorEmailExists = null;
                this.renderer.selectRootElement('#firstName').focus();
            } else if (this.errorMsgValidation !== '') {
                this.strengthBarValidationMessage = this.errorMsgValidation;
            } else if (this.selectedRole.value === '' || this.selectedRole.value === undefined || this.selectedRole.value === null) {
                this.validationMessage = 'Please fill in all required fields to sign up.';
            } else {
                this.doNotMatch = null;
                this.error = null;
                this.errorUserExists = null;
                this.errorEmailExists = null;
                this.isClickedOnce = true;
                this.languageService.getCurrent().then(key => {
                    this.registerAccount.langKey = key;
                    this.registerAccount.login = this.registerAccount.email;
                    this.registerAccount.authorities = [];
                    this.registerAccount.authorities = [this.selectedRole.value];
                    this.applyService.save(this.registerAccount).subscribe(
                        () => {
                            this.success = true;
                            if (this.registerAccount.notify_news_update) {
                            }
                        },
                        response => this.processError(response)
                    );
                });
            }
        } else {
            this.validationMessage = 'Please fill in all required fields to sign up.';
            this.renderer.selectRootElement('#firstName').focus();
        }
    }
    valuechange(newValue) {
        this.modelValue = newValue;
        if (this.modelValue.length < 1) {
            this.doNotMatch = null;
        }
    }
    private processError(response) {
        this.success = null;
        this.isClickedOnce = false;
        if (response.status === 400 && response.error === 'login already in use') {
            this.errorUserExists = 'ERROR';
            this.renderer.selectRootElement('#firstName').focus();
        } else if (response.status === 400 && response.error === 'email address already in use') {
            this.errorEmailExists = 'ERROR';
            this.renderer.selectRootElement('#firstName').focus();
        } else {
            this.error = 'ERROR';
            this.renderer.selectRootElement('#firstName').focus();
        }
    }

    apply() {
        this.mdDialog.closeAll();

        this.dialogRef = this.mdDialog.open(ApplyAccountComponent, {
            width: '400px'
        });
    }

    openSigninDialog(events: any) {
        this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
            width: '400px'
        });
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    newsletter(events: any) {
    if(this.registerAccount.notify_news_update)
    {
        this.cookieService.put('NEWS','false');
    }
    else{
        this.cookieService.put('NEWS','true');
    }
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    signUp(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    termsOfService(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    closeSignUpDialog(events: any) {
        this.mdDialog.closeAll();
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }

    roleValueChange(event) {
        this.registerAccount.authorities = event.value;
        this.cookieService.put('ROLE', event.value);
    }
    newsValueChange(event)
    {
        this.cookieService.put('NEWS',event.value)
    }
}
