import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PasswordStrengthBarComponent } from './password-strength-bar.component';
import { PasswordComponent } from './password.component';
import { PasswordService } from './password.service';

@NgModule({
	imports: [
        CommonModule,
		FormsModule,
        ReactiveFormsModule
	],
	declarations: [
        PasswordComponent,
        PasswordStrengthBarComponent
	],
	entryComponents: [
        PasswordComponent,
        PasswordStrengthBarComponent
	],
    exports: [
        PasswordComponent,
        PasswordStrengthBarComponent
    ],
	providers: [
        PasswordService
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplacePasswordModule {}