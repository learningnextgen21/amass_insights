import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PasswordService {

    constructor(private http: HttpClient) {}

    save(newPassword: string, currentPassword: string): Observable<any> {
        return this.http.post('api/account/change_password', newPassword, {
            params: {
                currentPassword: currentPassword
            }
        });
    }
}
