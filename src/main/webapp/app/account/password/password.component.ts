import { Component, OnInit } from '@angular/core';

import { Principal,AuthServerProvider,StateStorageService,SigninModalService } from '../../shared';
import { PasswordService } from './password.service';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { Router} from '@angular/router';

@Component({
    selector: 'jhi-password',
    templateUrl: './password.component.html',
    styleUrls: [
        '../../../content/scss/amass-form.scss',
        './password.component.css',
        '../../home/home.scss'
    ]
})
export class PasswordComponent implements OnInit {
    doNotMatch: string;
    error: string;
    success: string;
    account: any;
    password: string;
    confirmPassword: string;
    currentPassword: string;
    currentPasswordError: string;
    errorMsgValidation: any;
    strengthBarValidationMessage: any;

    constructor(
        private passwordService: PasswordService,
        private principal: Principal,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private stateService: StateStorageService,
        private authService: AuthServerProvider,
		private signinModalService: SigninModalService,
		private router: Router,
    ) {
    }

    ngOnInit() {
        this.errorMsgValidation = '';
        this.principal.identity().then((account) => {
            this.account = account;
        });
    }
    change(message: any) {
        this.errorMsgValidation = message;
    }

    changePassword() {
        if (this.password !== this.confirmPassword) {
            this.error = null;
            this.success = null;
            this.doNotMatch = 'ERROR';
        }else if (this.errorMsgValidation !== '') {
            this.strengthBarValidationMessage = 'Password needs to be at least 6 characters and include at least two of the following: character, number, special character.';
            this.doNotMatch = null;

        } else {
            this.doNotMatch = null;
            this.passwordService.save(this.password, this.currentPassword).subscribe(() => {
                this.error = null;
                this.success = 'Your password has been successfully changed.';
                this.strengthBarValidationMessage = null;
                this.currentPasswordError = null;
            }, (error) => {
                this.success = null;
                if(error.status === 400) {
                    this.currentPasswordError = 'Incorrect password. Please try again.';
                    this.currentPassword = '';
                }
            });
        }
    }
    passwordChange(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    onExpireSession(event) {
		//console.log('eventcheck',event);
        if (event) {
			//console.log('eventcheck',event);
          //  this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
				setTimeout(() => {
				this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
			}, 2000);
			}); 
        }
    }
	  beforeSessionExpires(event) {
        if (event) {
           // this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }
}
