

import { Injectable } from '@angular/core';
// import { Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class ConfirmService {

    constructor(private http: HttpClient) {}

    get(key: string): Observable<any> {
        const params: HttpParams = new HttpParams();
        params.set('key', key);

        return this.http.get('api/confirm', {
            params: {
                'key': key
            }
        });
    }
}
