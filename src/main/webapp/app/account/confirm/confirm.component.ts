import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { ConfirmService } from './confirm.service';
import { LoginModalService } from '../../shared';
import { SigninDialogComponent } from '../../dialog/signindialog.component';

@Component({
	selector: 'jhi-confirm',
    templateUrl: './confirm.component.html',
    styleUrls: ['../social/social-register.component.css']
})
export class ConfirmComponent implements OnInit {
	error: string;
	success: string;

	constructor(
		private confirmService: ConfirmService,
		private loginModalService: LoginModalService,
		private route: ActivatedRoute,
		public mdDialog: MatDialog
	) {
	}

	ngOnInit() {
		this.route.queryParams.subscribe((params) => {
			this.confirmService.get(params['key']).subscribe(() => {
				this.error = null;
				this.success = 'OK';
			}, () => {
				this.success = null;
				this.error = 'ERROR';
			});
		});
	}

	login() {
		this.mdDialog.closeAll();

		this.mdDialog.open(SigninDialogComponent, {
			width: '400px'
		});
	}
}
