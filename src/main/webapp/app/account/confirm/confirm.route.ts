import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ConfirmComponent } from './confirm.component';

export const confirmRoute: Route = {
    path: 'confirm',
    component: ConfirmComponent,
    data: {
        authorities: [],
        pageTitle: 'confirm.title'
    }
};
