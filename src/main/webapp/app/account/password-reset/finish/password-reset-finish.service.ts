import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PasswordResetFinishService {

    constructor(private http: HttpClient) {}

    save(keyAndPassword: any): Observable<any> {
        return this.http.post('api/account/reset_password/finish', keyAndPassword);
    }

    keyExpiryCheck(key: string): Observable<any> {
        return this.http.get('api/ur/reset_key?resetKey=' + key, {observe: 'response', responseType: 'text'});
    }
}
