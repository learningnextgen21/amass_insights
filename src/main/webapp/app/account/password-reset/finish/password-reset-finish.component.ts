import { Component, OnInit, AfterViewInit, Renderer2, ElementRef, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';

import { PasswordResetFinishService } from './password-reset-finish.service';
import { SigninDialogComponent } from '../../../dialog/signindialog.component';
import { GoogleAnalyticsEventsService } from '../../../shared/googleanalytics/google-analytics-events.service';

@Component({
    selector: 'jhi-password-reset-finish',
    templateUrl: './password-reset-finish.component.html',
    styleUrls: ['./password-reset-finish.component.css']
})
export class PasswordResetFinishComponent implements OnInit, AfterViewInit {
    confirmPassword: string;
    doNotMatch: string;
    error: string;
    keyMissing: boolean;
    resetAccount: any;
    success: string;
    key: string;
    errorMsgValidation: any;
    strengthBarValidationMessage: any;
    unmatchedErrorMsg: any;
    errorMessage: string;

    constructor(
        private passwordResetFinishService: PasswordResetFinishService,
        private route: ActivatedRoute,
        private elementRef: ElementRef, private renderer: Renderer2,
        public mdDialog: MatDialog,
        public googleAnalyticsService: GoogleAnalyticsEventsService
    ) {
    }

    ngOnInit() {
        this.errorMessage = '';
        this.errorMsgValidation = '';
        this.route.queryParams.subscribe((params) => {
            this.key = params['key'];
        });
        this.resetAccount = {};
        this.keyMissing = !this.key;
        if (this.key) {
            this.passwordResetFinishService.keyExpiryCheck(this.key).subscribe(response => {
                if (response.status === 200 && response._body === 'Valid Key') {
                    this.keyMissing = false;
                }
            }, error => {
                if (error.status === 400 && (error.error === 'Invalid Key' || error.error === 'Please provide a valid Reset Key')) {
                    this.error = 'ERROR';
                    this.errorMessage = 'Password reset key either expired or invalid key!';
                    this.keyMissing = true;
                }
            });
        } else {
            this.error = 'ERROR';
            this.errorMessage = 'Please provide valid key!';
            this.keyMissing = true;
        }
    }
    change(message: any) {
        this.errorMsgValidation = message;
    }

    ngAfterViewInit() {
        if (this.elementRef.nativeElement.querySelector('#password') != null) {
            this.renderer.selectRootElement('#password').focus();
        }
    }

    finishReset() {
        this.doNotMatch = null;
        this.error = null;
        if (this.resetAccount.password !== this.confirmPassword) {
            this.doNotMatch = 'ERROR';
            this.unmatchedErrorMsg = 'The password and its confirmation do not match!';
        }else if (this.errorMsgValidation !== '') {
            this.strengthBarValidationMessage = 'Password needs to be at least 6 characters and include at least two of the following: character, number, special character.';
        } else {
            this.passwordResetFinishService.save({key: this.key, newPassword: this.resetAccount.password}).subscribe(() => {
                this.success = 'OK';
            }, () => {
                this.success = null;
                this.error = 'ERROR';
                this.errorMessage = 'Your password couldn\'t be reset. Remember a password request is only valid for 24 hours.';
            });
        }
    }
    passwordReset(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    login() {
        this.mdDialog.closeAll();

		this.mdDialog.open(SigninDialogComponent, {
			width: '400px'
		});
    }
}
