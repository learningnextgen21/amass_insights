

import { Injectable } from '@angular/core';
// import { Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class EmailActionService {

	constructor(private http: HttpClient) {}

	get(api: string, key: string): Observable<any> {
		const params: HttpParams = new HttpParams();
		params.set('key', key);

		return this.http.get('api/ur/' + api, {
			params: {
                'key': key
            }
		});
    }
    unsubscription(key: string): Observable<any> {
        const params: HttpParams = new HttpParams();
		params.set('key', key);
        return this.http.get('api/ur/unsubscription', {
			params: {
                'key': key
            }
		});
    }
}
