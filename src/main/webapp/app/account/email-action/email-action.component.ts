import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';

import { EmailActionService } from './email-action.service';
import { LoginModalService, Principal } from '../../shared';
import { SigninDialogComponent } from '../../dialog/signindialog.component';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';

@Component({
	selector: 'jhi-email-action',
	templateUrl: './email-action.component.html',
	styleUrls: ['./email-action.component.css']
})
export class EmailActionComponent implements OnInit {
	error: string;
	success: string;
	title: string;
    message: string;
    mdDialogRef: MatDialogRef<any>;
    actionPopup: boolean;
    account: any;

	constructor(
		private emailActionService: EmailActionService,
		private loginModalService: LoginModalService,
		private route: ActivatedRoute,
        public mdDialog: MatDialog,
        public dialog: MatDialog,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        public principal: Principal
      //  public dialogRef: MatDialogRef<EmailActionComponent>,
	) {
	}

	ngOnInit() {
        this.actionPopup = false;
        if (this.actionPopup === false) {
		this.route.queryParams.subscribe(params => {
			this.emailActionService.get(params['api'], params['key']).subscribe(resp => {
                const response = resp;
				this.error = null;
				this.success = 'OK';
				this.title = response.title ? response.title : 'Email Action';
                this.message = response.message ? response.message : '';
			}, () => {
				this.success = null;
				this.error = 'ERROR';
				this.title = 'Error';
			});
        });
       }

        this.principal.identity().then(account => {
            if (account) {
                this.account = account;
            }
        });
	}

	login() {
		this.mdDialog.closeAll();

		this.mdDialog.open(SigninDialogComponent, {
			width: '400px'
		});
    }
    unsubscription(events: any) {
        this.actionPopup = true;
        this.mdDialogRef = this.dialog.open(EmailActionComponentComponent, {
            width: '400px'
           // data: { name: this.actionPopup }
        });
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    closeLoginDialog(events: any) {
        // this.dialogRef.close();
    }
}

@Component({
    selector: 'jhi-email-action-unsubscription',
    templateUrl: '../../account/email-action/unsubscription-popup.component.html',
    styleUrls: ['../../dialog/dialog.component.css']
})
export class EmailActionComponentComponent implements OnInit {
    error: string;
	success: string;
	title: string;
    message: string;
    // mdDialogRef: MatDialogRef<any>;
    actionPopup: boolean;
    confirmBtn: any;
    constructor(
        public dialogRef: MatDialogRef<EmailActionComponentComponent>,
        private emailActionService: EmailActionService,
		private loginModalService: LoginModalService,
		private route: ActivatedRoute,
        public mdDialog: MatDialog,
        public dialog: MatDialog,
    ) {}
    ngOnInit() {
        this.confirmBtn = true;
        if (!this.confirmBtn) {
        this.route.queryParams.subscribe(params => {
			this.emailActionService.get(params['api'], params['key']).subscribe(resp => {
                const response = resp;
				this.error = null;
				this.success = 'OK';
				this.title = response.title ? response.title : 'Email Action';
                this.message = response.message ? response.message : '';
			}, () => {
				this.success = null;
				this.error = 'ERROR';
				this.title = 'Error';
			});
        });
      } else {
        this.route.queryParams.subscribe(params => {
            this.emailActionService.unsubscription(params['key']).subscribe(resp => {
                const response = resp;
                this.error = null;
                this.success = 'OK';
                this.title = response.title ? response.title : 'Email Action';
                this.message = response.message ? 'Your email address has been unsubscribed from the newsletter. We\'re sorry to see you go! Please let us know if there\'s anything we could have improved to make your newsletter experience better!' : '';
                this.confirmBtn = true;
            }, () => {
                this.success = null;
                this.error = 'ERROR';
                this.title = 'Error';
            });
        });
      }
    }
    confirm() {
        this.route.queryParams.subscribe(params => {
           this.emailActionService.unsubscription(params['key']).subscribe(resp => {
               const response = resp;
               this.error = null;
               this.success = 'OK';
               this.title = response.title ? response.title : 'Email Action';
               this.message = response.message ? 'Your email address has been unsubscribed from the newsletter. We\'re sorry to see you go! Please let us know if there\'s anything we could have improved to make your newsletter experience better!' : '';
               this.confirmBtn = false;
           }, () => {
               this.success = null;
               this.error = 'ERROR';
               this.title = 'Error';
           });
       });
       this.actionPopup = false;
       this.dialogRef.close();
   }
   closeLoginDialog(events: any) {
    this.dialogRef.close();
  //  this.googleAnalyticsService.emitEvent(events.label, events.category, events.event);
}

}
