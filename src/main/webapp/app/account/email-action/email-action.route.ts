import { Route } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { EmailActionComponent } from './email-action.component';

export const emailActionRoute: Route = {
	path: 'email-action',
	component: EmailActionComponent,
	data: {
		authorities: [],
		pageTitle: 'emailAction.title'
	}
};
