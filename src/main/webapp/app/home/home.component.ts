import { Component, OnInit, Inject, Output, EventEmitter, OnChanges, AfterViewInit} from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Account, Principal } from '../shared';

import { SigninDialogComponent } from '../dialog/signindialog.component';

import { ApplyAccountComponent } from '../account/apply/apply.component';
import {WINDOW} from '../layouts/main/window.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';
import { StateStorageService } from '../shared/auth/state-storage.service';
import { NgbCarouselConfig, NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { NewsLetterSignUpComponent } from '../account/newsletter-signup/newsletter-signup.component';
import { DataProviderService } from '../entities/data-provider/data-provider.service';
import { LoaderService } from '../loader/loaders.service';
import { DataProvider } from '../entities/data-provider/data-provider.model';
import { DataEventsService } from '../entities/data-events/data-events.service';
import { HomeEvents } from '../shared/genric-home/genric-home-event.model';
import { DataArticleService } from '../entities/data-article/data-article.service';
import { DataHomeArticle } from '../shared/genric-home/genric-home-article-model';
@Component({
	selector: 'jhi-home',
	templateUrl: './home.component.html',
	styleUrls: [
		'home.scss',
		'home.css'
    ],
    providers: [NgbCarouselConfig]

})
export class HomeComponent implements OnInit, OnChanges, AfterViewInit {
	account: Account;
	modalRef: NgbModalRef;
	mdDialogRef: MatDialogRef<any>;
	hovering1: boolean;
	hovering2: boolean;
	hovering3: boolean;
   // @Output() sliderValue = new EventEmitter<any>();
    @Output() sliderValue: EventEmitter<any> = new EventEmitter();
    sliderCount: any;
    eventDataListStatus: boolean;
    articleDataListStatus: boolean;
    providerDataListStatus: boolean;
    activeSliderChanges: boolean;
    dataFeatureProvider: DataProvider[];
    dataRecentProvider: DataProvider[];
    dataEvents: HomeEvents[];
    dataArticles: DataHomeArticle[];
    currentAccount: any;
    sliderInterval: any;
	constructor(
		private principal: Principal,
		private eventManager: JhiEventManager,
		public dialog: MatDialog,
		public googleAnalyticsService: GoogleAnalyticsEventsService,
		@Inject(WINDOW) private window: Window,
        public router: Router,
        private activatedRoute: ActivatedRoute,
        private stateStorageService: StateStorageService,
        config: NgbCarouselConfig,
        private dataProviderService: DataProviderService,
        private loaderService: LoaderService,
        private dataEventsService: DataEventsService,
        private dataArticleService: DataArticleService
	) {
        this.sliderInterval = 4000;
        config.interval = this.sliderInterval;
        config.wrap = true;
        config.keyboard = false;
        config.pauseOnHover = false;
        config.showNavigationArrows = true;
        this.dataFeatureProvider = [];
        this.dataRecentProvider = [];
        this.dataEvents = [];
        this.dataArticles = [];
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
	}

	ngOnInit() {
       // this.config.interval = 4000;
		this.hovering1 = false;
		this.hovering2 = false;
        this.hovering3 = false;
        this.loaderService.display(true);
		this.principal.identity().then(account => {
			this.account = account;
			if (this.account && this.account.activated) {
				this.router.navigate(['providers']);
			}
        });
        this.activatedRoute.queryParams.subscribe(params => {
            const redirect = this.stateStorageService.getUrl();
            console.log('...params', params);
            setTimeout(() => {
                if (params['newsletter'] && params['newsletter'] === 'true') {
                    this.mdDialogRef = this.dialog.open(NewsLetterSignUpComponent, {
                        width: '500px'
                    });
                 }
                if (params['signup'] && params['signup'] === 'true' && !this.isAuthenticated() ||
                    (params['signup'] && params['signup'] === 'true' && params['role'] && (params['role'] === 'investor' || params['role'] === 'provider') && !this.isAuthenticated())) {
                    this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                        width: '570px'
                    });
                if (params['signup'] && params['signup'] === 'true' && params['role'] && (params['role'] === 'investor') && !this.isAuthenticated()) {
                    this.mdDialogRef.componentInstance.companyName = 'ROLE_INVESTOR';
                 } else if (params['signup'] && params['signup'] === 'true' && params['role'] && (params['role'] === 'provider') && !this.isAuthenticated()) {
                    this.mdDialogRef.componentInstance.companyName = 'ROLE_PROVIDER';
                 } else {
                    this.mdDialogRef.componentInstance.companyName = '';
                 }

            } else if (this.isAuthenticated()) {
                this.router.navigate([redirect]);
              }
            }, 1000);

            setTimeout(() => {
                if (params['login'] && params['login'] === 'true' && !this.isAuthenticated()) {
                    this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
                        width: '400px'
                    });
                }
              }, 1000);
        });
        this.dataProviderService.featureProviderData('featured').subscribe(data => {
            console.log(data);
           // this.dataFeatureHome = data['hits'].total ? data['hits'].hits[0]._source : null;
            if (data['hits'] && data['hits'].hits) {
                for (let i = 0; i < data['hits'].hits.length; i++) {
                    const featureProvider = data['hits'].hits[i]._source;
                    this.dataFeatureProvider.push(featureProvider);
                }
            }
            this.loaderService.display(false);
        });
        this.dataProviderService.featureProviderData('recent').subscribe(data => {
            console.log(data);
           // this.dataFeatureHome = data['hits'].total ? data['hits'].hits[0]._source : null;
            if (data['hits'] && data['hits'].hits) {
                for (let i = 0; i < data['hits'].hits.length; i++) {
                    const recentProvider = data['hits'].hits[i]._source;
                    this.dataRecentProvider.push(recentProvider);
                }
            }
            this.loaderService.display(false);
        });
        this.dataEventsService.unrestrictedSearch().subscribe(data => {
            if (data['hits'] && data['hits'].hits) {
                for (let i = 0; i < 3; i++) {
                    const eventsObj = data['hits'].hits[i]._source;
                    this.dataEvents.push(eventsObj);
                }
            }
            this.loaderService.display(false);
        });
        let queryBody = {};
        this.dataArticleService.elasticSearch(queryBody).subscribe(data => {
            if (data['hits'] && data['hits'].hits) {
                for (let i = 0; i < 3; i++) {
                    const articleObj = data['hits'].hits[i]._source;
                    if ( this.currentAccount || !this.currentAccount) {
                        const articleNewObj = Object.assign({
                            emailText: this.currentAccount?'Hi,\n I\'d like to share this interesting ' + data['hits'].hits[i]._source.title + ' ' + data['hits'].hits[i]._source.url + ` I came across on the Amass Insights Platform.\n\n` + 'Regards,\n ' +  this.currentAccount.firstName + ' ' + this.currentAccount.lastName +'\n\n\n P.S. If you liked this article and you\'re not already an Insights user, https://amassinsights.com?signup=true, and you\'ll receive instant access to Alt-Data news, events, though-leadership, and new, interesting data providers entering our ecosystem.' : 'Hi,\n I\'d like to share this interesting ' + data['hits'].hits[i]._source.title + ' ' + data['hits'].hits[i]._source.url + ` I came across on the Amass Insights Platform.\n` + '\n P.S. If you liked this article and you\'re not already an Insights user, https://amassinsights.com?signup=true, and you\'ll receive instant access to Alt-Data news, events, though-leadership, and new, interesting data providers entering our ecosystem.',
                            linkedInText: 'Check out this interesting ' + data['hits'].hits[i]._source.title + ' article I came across on the Amass Insights Platform: ' + data['hits'].hits[i]._source.url + ' #' + data['hits'].hits[i]._source.title + ' #AmassInsights',
                            twitterText: 'Check out this interesting ' + data['hits'].hits[i]._source.title + ' article I came across on the Amass Insights Platform: ' + data['hits'].hits[i]._source.url + ' #' + data['hits'].hits[i]._source.title + ' #AmassInsights'
                        }, articleObj);
                        this.dataArticles.push(articleNewObj);
                    } else {
                        this.dataArticles.push(articleObj);
                    }

                }
                this.loaderService.display(false);
            }
        });

    }
    onSlider(value) {
        if (value.current === '101') {
            this.sliderInterval = 12000;
          //  console.log('time....', this.sliderInterval);
           // this.config.interval = this.sliderInterval;
        } else if (value.current === '100' || value.current === '102') {
            this.sliderInterval = 4000;
           // this.config.interval = this.sliderInterval;
          //  console.log('time....', this.sliderInterval);
        }
    }
    ngOnChanges() {
        console.log('..test..');
    }
    ngAfterViewInit() {
        console.log('test..');
    }
    onSlide(event) {
      //  console.log(event);
        if (event.current === '121') {
            this.sliderCount = '1';
        } else if (event.current === '221') {
            this.sliderCount = '2';
        } else if(event.current === '222') {
            this.sliderCount = '3';
        }

    }
    activeDataStatus(status) {
        const valueList = status;
        if (valueList === 'event') {
            this.eventDataListStatus = true;
            this.articleDataListStatus = false;
            this.providerDataListStatus = false;
        } else if (valueList === 'article') {
            this.articleDataListStatus = true;
            this.eventDataListStatus = false;
            this.providerDataListStatus = false;
        } else  if (valueList === 'eventLeft') {
            this.eventDataListStatus = false;
        } else  if (valueList === 'articleLeft') {
            this.articleDataListStatus = false;
        } else if (valueList === 'provider') {
            this.providerDataListStatus = true;
            this.articleDataListStatus = false;
            this.eventDataListStatus = false;
        } else if (valueList === 'providerLeft') {
            this.providerDataListStatus = false;
        }
    }
    /* recentNews(events) {
        console.log(events);
        this.router.navigate(['/news']);
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    upcomingEvents(events) {
        console.log(events);
        this.router.navigate(['/events']);
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    } */
    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

	openSigninDialog(events) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
		this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
			width: '400px'
		});
	}
	openApplyDialog(value) {
		this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '570px'
        });
        this.mdDialogRef.componentInstance.companyName = value;
    }
    investorsSignUp(label: any) {
        this.googleAnalyticsService.emitEvent('Sign Up', 'Popup Link Clicked', label);
    }
	openApplyContact() {
		this.googleAnalyticsService.emitEvent('Contact Us', 'Internal Link Clicked', 'Home - Providers - Contact Us');
		this.router.navigate(['./contact-us']);
	}
    featureSec() {
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '570px'
        });
    }
    insightsSec() {
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '570px'
        });
    }
	scrollTo(selector) {
		this.googleAnalyticsService.emitEvent('Home', 'Internal Link Clicked', 'Home - Top Image - Learn More');
		const element = document.getElementById(selector);

		element.scrollIntoView(true);
		// account for fixed header
		const scrolledY = this.window.scrollY;
        // reduce fixed height header
        window.scrollTo({ left: 0, top: 800, behavior: 'smooth' });
		/* if (scrolledY) {
			this.window.scroll(0, scrolledY - 63);
		} */
    }
    pressPage() {
        this.router.navigate(['/press']);
    }

    unrestrictedUser(message:string)
    {
        console.log('message',message);
        localStorage.setItem('SearchData',message);
        this.router.navigateByUrl('unproviders');
    }
}
