import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DatamarketplaceSharedModule } from '../shared';

import { HOME_ROUTE, HomeComponent } from './';
// import { AnimateOnScrollModule } from 'ng2-animate-on-scroll';
import { HomeEventsComponent } from '../shared/genric-home/genric-home-event.component';
import { HomeArticleComponent } from '../shared/genric-home/genric-home-article.component';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { feature_ROUTE } from './home.route';
import { DataFeatureProviderComponent } from 'app/entities/data-provider/data-feature-provider.component';

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot([ HOME_ROUTE, feature_ROUTE ], { useHash: true }),
       // AnimateOnScrollModule.forRoot(),
        ShareButtonModule,
        ShareButtonsModule.withConfig({
            debug: true
        })
    ],
    declarations: [
        HomeComponent,
        HomeArticleComponent,
        HomeEventsComponent,
        DataFeatureProviderComponent
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceHomeModule {}
