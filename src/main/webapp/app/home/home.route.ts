import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { HomeComponent } from './';
import { DataFeatureComponent } from 'app/entities/data-feature';
import { DataFeatureProviderComponent } from 'app/entities/data-provider/data-feature-provider.component';

export const HOME_ROUTE: Route = {
	path: '',
	component: HomeComponent,
	data: {
		authorities: [],
		pageTitle: 'home.title',
		type: 'static'
    }

};
export const feature_ROUTE: Route = {
	path: 'home-feature',
	component: DataFeatureProviderComponent,
	data: {
		authorities: [],
		pageTitle: 'home.title',
		type: 'static'
    }

};

