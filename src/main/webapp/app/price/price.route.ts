import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { PriceComponent } from '.';

export const PRICE_ROUTE: Route = {
	path: 'upgrade',
	component: PriceComponent,
	data: {
		authorities: ['ROLE_USER'],
		pageTitle: 'home.title',
		breadcrumb: 'Plans & Pricing',
		type: 'static'
    },
    canActivate: [UserRouteAccessService]
};
