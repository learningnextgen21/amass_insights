import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class PriceService {
    private priceUrl = 'api/usercheckout';

	constructor(private http: HttpClient) {

    }

    upgradePrice(result:any): Observable<any> {
		console.log(result);
        return this.http.post(this.priceUrl, {},{

            params: {
                name:result
            }
        });
    }

   
}
