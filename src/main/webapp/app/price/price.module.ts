import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DatamarketplaceSharedModule } from '../shared';
import { PRICE_ROUTE, PriceComponent } from '.';
import { AccordionModule } from 'primeng/accordion';
import { NgxStripeModule } from 'ngx-stripe';
import { PriceService } from './price.service';
import { environmentDev } from '../environments/environment.dev';
import { environmentProd } from './../environments/environment.prod';

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        AccordionModule,
        NgxStripeModule.forRoot(environmentDev.dev ? environmentDev.APIEndpoint : environmentProd.APIEndpoint),
        RouterModule.forRoot([ PRICE_ROUTE ], { useHash: true })
    ],
    declarations: [ PriceComponent],
    providers:[PriceService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap:    [ PriceComponent ]
})
export class DataMarketplacePriceModule { 

}
