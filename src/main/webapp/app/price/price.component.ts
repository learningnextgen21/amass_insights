import { Component, OnInit, Inject } from '@angular/core';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';
import {WINDOW} from '../layouts/main/window.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environmentDev } from './../environments/environment.dev';
import {AccordionModule} from 'primeng/accordion';
import { PriceService } from './price.service';
import { UserService } from './../shared';
import { Principal,AuthServerProvider,StateStorageService,SigninModalService } from './../shared';
import { environmentProd } from 'app/environments/environment.prod';

@Component({
    selector: 'jhi-price',
    templateUrl: 'price.component.html',
    styleUrls: [
        './price.scss',
        '../home/home.css'
    ]
})
export class PriceComponent implements OnInit {
public price : boolean;
public features: string;
public plan: boolean;
pageName = 'list';
    constructor(
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        public priceService: PriceService,
        public router: Router,
        private userService: UserService,
        private principal: Principal,
        private stateService: StateStorageService,
        private authService: AuthServerProvider,
		private signinModalService: SigninModalService,
        @Inject(WINDOW) private window: Window,
        ) {

        }

    ngOnInit() {
        this.userService.userauthorities().subscribe((authorities) => {
            console.log('authorities',authorities);
            this.price = authorities;
            console.log('price',this.price);
            if(this.price)
            {
                this.router.navigateByUrl('/settings');
            }
        });
        const APIEndpoint = environmentDev.dev ? environmentDev.APIEndpoint : environmentProd.APIEndpoint;
        console.log('const', APIEndpoint);
    }
    eventlinkedin(events) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    contactUs(events) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    scrollTo(selector) {
		const element = document.getElementById(selector);

		element.scrollIntoView(true);
		// account for fixed header
		const scrolledY = this.window.scrollY;
        // reduce fixed height header
        if (scrolledY) {
			this.window.scroll(0, scrolledY - 63);
		}
    }
    teamEvent(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        console.log(events.category);
    }
    pressPage() {
        this.router.navigate(['/press']);
    }
    amassView(events) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }

    btnClick = function() {
        this.router.navigateByUrl('/contact-us');
}
submitPrice(events: any)
{
console.log(events);
    this.priceService.upgradePrice(events).subscribe(data => {
        console.log(data);
        if (data === null)
        {
           /*  this.window.location.href = '/#/checkout';
            this.window.location.reload(true);
     */
    this.router.navigateByUrl('/checkout');

    
        }  
    });

}
submitFeatures(events: string)
{
this.features = events;
this.plan = true;
}
onExpireSession(event) {
    //console.log('eventcheck',event);
    if (event) {
        //console.log('eventcheck',event);
        //this.formSubmitted = true;
        this.authService.logout().subscribe(d => {
            this.stateService.storeUrl(null);
            this.principal.authenticate(null);
            this.router.navigate(['../../']);
            setTimeout(() => {
            this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
        }, 2000);
        }); 
    }
}
  beforeSessionExpires(event) {
    if (event) {
       // this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
    }
}

}
