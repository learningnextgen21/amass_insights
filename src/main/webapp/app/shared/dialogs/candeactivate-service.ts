import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';

import { DataProviderEditComponent } from 'app/entities/data-provider/data-provider-edit.component';
import { DataProviderAdminFormComponent } from 'app/entities/data-provider/data-provider-admin-form.component';
import { AccountService } from '../auth/account.service';
import { AddProviderAdminComponent } from 'app/add-provider-admin';

@Injectable()
export class CanDeactivateGuard implements CanDeactivate<DataProviderEditComponent> {
    providerService: AccountService;
    canDeactivate(
        component: DataProviderEditComponent,
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
        nextState: RouterStateSnapshot
    ): Observable<boolean> | boolean {
        // you can just return true or false synchronously
        // if (component.formSubmitted) {
        //     // console.log('form submitted...');
        //     return true;
        // }
        // console.log(route);
        // console.log(route.params);
        // console.log(component.pendingEdits);
        // console.log(component.formSubmitted);
        // console.log(component.navigateNextPage);

        if (component.pendingEdits) {
            return true;
        }
        // console.log('form not submitted....');
        // or, you can also handle the guard asynchronously, e.g.
        // asking the user for confirmation.

        if (component.formSubmitted && !component.navigateNextPage) {
            console.log('1', component.navigateNextPage);
            return component.openConfirmationDialog(nextState);
        }

        // if (!component.formSubmitted) {
        //     return true;
        // }

        if (component.unsavedEdits  && !component.navigateNextPage) {
            console.log('2');
            return component.openConfirmationDialog(nextState);
        }

        if(component.formSubmitted && component.navigateNextPage) {
            console.log('3');
            return component.submitWhileNavigation();
        }

        return true;
    }
}

@Injectable()
export class CanDeactivateAdminFormGuard implements CanDeactivate<DataProviderAdminFormComponent> {

    canDeactivate(
        component: DataProviderAdminFormComponent,
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
        nextState: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | boolean {
        // you can just return true or false synchronously
        if (component.formSubmitted) {
            return true;
        }
        // or, you can also handle the guard asynchronously, e.g.
        // asking the user for confirmation.
        return component.openConfirmationDialog(nextState);
    }
}

@Injectable()
export class CanDeactivateAddProviderAdminFormGuard implements CanDeactivate<AddProviderAdminComponent> {

    canDeactivate(
        component: AddProviderAdminComponent,
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
        nextState: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | boolean {
        // you can just return true or false synchronously
        if (component.formSubmitted) {
            return true;
        }
        // or, you can also handle the guard asynchronously, e.g.
        // asking the user for confirmation.
        return component.openConfirmationDialog(nextState);
    }
}
