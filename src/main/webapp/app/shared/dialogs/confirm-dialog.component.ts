import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'jhi-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
})
export class ConfirmDialogComponent {
    public confirmMessage: string;
    public button: ButtonModel;
    constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>) {
        this.button = {
            leave: false,
            confirm: true,
            leaveText: 'Leave',
            confirmText: ' Confirm'
        };
    }

    onLeave() {
        this.dialogRef.close('leave');
    }

    onSave() {
        this.dialogRef.close('save');
    }
}

export class ButtonModel {
    leave?: boolean;
    leaveText?: string;
    confirm?: boolean;
    confirmText?: string;
}
