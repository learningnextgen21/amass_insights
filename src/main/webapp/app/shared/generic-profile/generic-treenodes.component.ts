import { Component, OnInit, ViewChild, Input, AfterViewInit, Injectable } from '@angular/core';
import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { BehaviorSubject, Observable, of as observableOf } from 'rxjs';
import { Router } from '@angular/router';
import { GoogleAnalyticsEventsService } from '../googleanalytics/google-analytics-events.service';
import { Principal } from '../../shared/auth/principal.service';
import { ApplyAccountComponent } from 'app/account';

/**
 * File database, it can build a tree structured Json object from string.
 * Each node in Json object represents a file or a directory. For a file, it has filename and type.
 * For a directory, it has filename and children (a list of files or directories).
 * The input will be a json object string, and the output is a list of `FileNode` with nested
 * structure.
 */
@Injectable()
export class FileDatabase {
    dataChange: BehaviorSubject<AmassTreeNode[]> = new BehaviorSubject<AmassTreeNode[]>([]);
    treeData: any = {};

    get data(): AmassTreeNode[] { return this.dataChange.value; }

    constructor() {
        this.initialize();
    }

    initialize(treeData?: any) {
        if (treeData) {
            TREE_DATA = treeData;
        }
        // Parse the string to json object.
        const dataObject = TREE_DATA;
        /* const dataTooltip = TREE_DATA['tooltip'];
        const dataLinks = TREE_DATA['dataLinks']; */

        // Build the tree nodes from Json object. The result is a list of `FileNode` with nested
        //     file node as children.
        const data = this.buildAmassTree(dataObject, 0);

        // Notify the change.
        this.dataChange.next(data);
    }

    /**
     * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
     * The return value is the list of `FileNode`.
     */
    buildFileTree(value: any, level: number, tooltip?: any, dataLinks?: any): AmassTreeNode[] {
        const data: any[] = [];
        for (const k in value) {
            if (k) {
                const v = value[k];
                const node = new AmassTreeNode();
                node.label = `${k}`;
                node.tooltip = tooltip;
                node.locked = true;
                if (v === null || v === undefined) {
                    // no action
                } else if (typeof v === 'object') {
                    if (v.hasOwnProperty('length')) {
                        const obj = v.reduce(function(n, object) {
                            n[object.id] = object.desc;
                            return n;
                        }, {});
                        node.children = this.buildFileTree(obj, level + 1, null, dataLinks);
                        if (dataLinks) {
                            node.dataLinks = dataLinks.filter(i => {
                                return node.label === i.node;
                            })[0];
                        }
                    } else {
                        node.children = this.buildFileTree(v, level + 1);
                        if (dataLinks) {
                            node.dataLinks = dataLinks.filter(i => {
                                return node.label === i.node;
                            })[0];
                        }
                    }
                } else {
                    node.label = v;
                }
                data.push(node);
            }
        }
        return data;
    }

    buildAmassTree(array: any, level: number): AmassTreeNode[] {
        const data: any[] = [];
        // console.log(array);
        for (let obj of Object.keys(array)) {
            const node = new AmassTreeNode();
            var newObj = array[obj];
            if (newObj) {
                // console.log(newObj);
                node.label = newObj.label || newObj.label || newObj.desc || newObj.description;
                node.logo = newObj.logo ? newObj.logo : null;
                node.tooltip = newObj.tooltip ? newObj.tooltip : null;
                node.params = newObj.params ? newObj.params : null;
                node.routing = newObj.routing ? newObj.routing : null;
                node.ga = newObj.ga ? newObj.ga : null;
                node.font = newObj.font ? newObj.font : null;
                if (newObj.children) {
                    node.children = this.buildAmassTree(newObj.children, level + 1);
                } else {
                    for (const k in newObj) {
                        if (k) {
                            node[k] = newObj[k];
                        }
                    }
                }
            }
            data.push(node);
        }
        return data;
    }
}

@Component({
    selector: 'jhi-generic-treenodes',
    templateUrl: './generic-treenodes.component.html',
    styleUrls: [
        './generic-treenodes.component.css'
    ],
    providers: [GoogleAnalyticsEventsService, FileDatabase]
})
export class GenericTreenodesComponent implements OnInit, AfterViewInit {

    treeControl: FlatTreeControl<AmassTreeFlatNode>;
    treeFlattener: MatTreeFlattener<AmassTreeNode, AmassTreeFlatNode>;
    dataSource: MatTreeFlatDataSource<AmassTreeNode, AmassTreeFlatNode>;
    @ViewChild('tree') tree;
    @Input() treeData;
    @Input() treeNodeData: boolean;
    parentLabel: any;
    updatedTreeData: BehaviorSubject<TreeNodes> = new BehaviorSubject<TreeNodes>({});
    mdDialogRef: MatDialogRef<any>;
    editorFieldWidth: boolean = false;
    constructor(database: FileDatabase, private router: Router, private googleAnalyticsService: GoogleAnalyticsEventsService ,private principal: Principal, public dialog: MatDialog,) {
        this.treeFlattener = new MatTreeFlattener(this.transformer, this._getLevel,
            this._isExpandable, this._getChildren);
        this.treeControl = new FlatTreeControl<AmassTreeFlatNode>(this._getLevel, this._isExpandable);
        this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

        database.dataChange.subscribe(data => {
            this.dataSource.data = data;
            // console.log(this.dataSource.data);
            if(this.dataSource && this.dataSource.data && this.dataSource.data[0] && this.dataSource.data[0].label === 'Key Data Fields:') {
                this.editorFieldWidth = true;
            } else if (this.dataSource && this.dataSource.data && this.dataSource.data[0] && this.dataSource.data[0].label === 'Use Cases:') {
                this.editorFieldWidth = true;
            } else if (this.dataSource && this.dataSource.data && this.dataSource.data[0] && this.dataSource.data[0].label === 'Product Details:') {
                this.editorFieldWidth = true;
            } else if (this.dataSource && this.dataSource.data && this.dataSource.data[0] && this.dataSource.data[0].label === 'In-Depth:') {
                this.editorFieldWidth = true;
            }
        });
        this.updatedTreeData.subscribe(data => {
            database.treeData = data;
            database.initialize(data);
        });
    }

    transformer = (node: AmassTreeNode, level: number) => {
        const flatNode = new AmassTreeFlatNode();
        flatNode.label = node.label;
        flatNode.id = node.id;
        flatNode.tooltip = node.tooltip;
        flatNode.params = node.params;
        flatNode.ga = node.ga;
        flatNode.font = node.font;
        flatNode.level = level;
        flatNode.expandable = !!node.children;
        flatNode.locked = node.locked;
        flatNode.routing = node.routing;
        flatNode.logo = node.logo;
        return flatNode;
    }

    private _getLevel = (node: AmassTreeFlatNode) => node.level;
    private _isExpandable = (node: AmassTreeFlatNode) => node.expandable;
    private _getChildren = (node: AmassTreeNode): Observable<AmassTreeNode[]> => {
        return observableOf(node.children);
    }

    hasChild = (_: number, _nodeData: AmassTreeFlatNode) => {
        return _nodeData.expandable;
    };

    ngOnInit() {
        this.updatedTreeData.next(this.treeData);
    }

    mouseEnter(p) { // This event used for open and close the popup while mouseover the locked providers
        setTimeout(() => {
            p.close();
        }, 5000);
    }

    upgrade() {
        this.router.navigate(['/upgrade']);
    }

    ngAfterViewInit() {
        this.tree.treeControl.expandAll();
    }
    isAuthenticated() {
        return this.principal.isAuthenticated();
    }
    anchorLink(node) {
        console.log(node);
        if(this.isAuthenticated())
        {
        const parentNode = this.getParent(node);
        console.log(parentNode);
        this.parentLabel = parentNode.label;
        if (parentNode.ga) {
            this.gaEvent({
                label: parentNode.ga.labelPrefix + node.id + node[parentNode.ga.labelSuffix],
                category: parentNode.ga.category,
                event: parentNode.ga.event
            });
        }
        if (parentNode.params) {
            this.router.navigate([parentNode.routing], {
                queryParams: {
                    filter1: parentNode.params.filter,
                    value1: node.id,
                    type1: parentNode.params.type
                }
            });
        }
        if (node.id && !parentNode.params && parentNode.routing) {
         this.router.navigate([parentNode.routing, node.id]);
         }
        }
        else
        {
            this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            });
        }
        this.googleAnalyticsService.emitEvent(node.label, 'Internal Link Clicked', 'Event Details - Overview - ' + this.parentLabel + ' - ' + node.label);
        console.log(node.label);
        console.log(this.parentLabel);
    }

    /**
     * Iterate over each node in reverse order and return the first node that has a lower level than the passed node.
     */
    getParent(node: AmassTreeFlatNode) {
        const { treeControl } = this;
        const currentLevel = treeControl.getLevel(node);

        if (currentLevel < 1) {
            return null;
        }

        const startIndex = treeControl.dataNodes.indexOf(node) - 1;

        for (let i = startIndex; i >= 0; i--) {
            const currentNode = treeControl.dataNodes[i];

            if (treeControl.getLevel(currentNode) < currentLevel) {
                return currentNode;
            }
        }
    }

    gaEvent(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

}

export interface TreeDataModel {
    id?: number;
    name: string;
    level?: number;
    children?: TreeDataModel[];
    tooltip?: string;
    desc?: string;
    tree: any;
}

const GetChildren = (node: TreeDataModel) => observableOf(node.children);

/**
 * File node data with nested structure.
 * Each node has a filename, and a type or a list of children.
 */
export class FileNode {
    children: FileNode[];
    filename: string;
    type: any;
    tooltip: any;
    dataLinks: any;
    locked: boolean;
}

/**
 * Tree node data with nested structure.
 * Each node has a label, and a tooltip or a list of children.
 */
export class AmassTreeNode {
    children: AmassTreeNode[];
    label: string;
    desc: string;
    id: any;
    logo: any;
    tooltip: any;
    dataLinks: any;
    locked: boolean;
    params: any;
    ga: any;
    font: string;
    routing: string;
}

/** Flat node with expandable and level information */
export class AmassTreeFlatNode {
    label: string;
    id: any;
    tooltip: any;
    params: any;
    ga: any;
    locked: boolean;
    level: number;
    expandable: boolean;
    font: string;
    routing: string;
    logo: any;
}

/** Flat node with expandable and level information */
export class FileFlatNode {
    filename: string;
    type: any;
    tooltip: any;
    dataLinks: any;
    locked: boolean;
    level: number;
    expandable: boolean;
}

/**
 * The file structure tree data in string. The data could be parsed into a Json object
 */
let TREE_DATA = {};
export class TreeNodes {
    data?: any;
    dataLinks?: any;
    tooltip?: string;
}
