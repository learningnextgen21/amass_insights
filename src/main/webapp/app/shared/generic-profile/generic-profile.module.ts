import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { GenericProfileComponent } from './generic-profile.component';
import { GenericTabsComponent } from './generic-tabs.component';
import { CommonModule } from '@angular/common';
import { DatamarketplaceSharedLibsModule } from '../shared-libs.module';
import { GenericTreenodesComponent } from './generic-treenodes.component';
import { GenericProfileDatatableComponent } from './generic-profile-datatable.component';
import { GenericProfileSubTabsComponent } from './generic-profile-subtabs.component';
import { GenericProfileButtonsComponent } from './generic-profile-buttons.component';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { GenericProfileCardModule } from '../generic-profile-card/generic-profile-card.module';

@NgModule({
    imports: [
        CommonModule,
        DatamarketplaceSharedLibsModule,
        ShareButtonModule,
        ShareButtonsModule.withConfig({
            debug: true
        }),
        GenericProfileCardModule
    ],
    declarations: [
        GenericProfileComponent,
        GenericTabsComponent,
        GenericProfileSubTabsComponent,
        GenericTreenodesComponent,
        GenericProfileDatatableComponent,
        GenericProfileButtonsComponent
    ],
    exports: [
        GenericProfileComponent,
        GenericTabsComponent,
        GenericProfileSubTabsComponent,
        GenericTreenodesComponent,
        GenericProfileDatatableComponent,
        GenericProfileButtonsComponent
    ],
    bootstrap: [GenericProfileComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenericProfileModule { }
