import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GenericProfileTabsModel } from './generic-profile-tabs.model';
import { GoogleAnalyticsEventsService } from 'app/shared/googleanalytics/google-analytics-events.service';
import { Principal } from '../auth/principal.service';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoaderService } from '../../loader/loaders.service';
import { DataResourceTabService } from 'app/entities/data-provider/data-resourcetab.service';

@Component({
    selector: 'jhi-generic-tab',
    templateUrl: './generic-tabs.component.html',
    styleUrls: [
        './generic-tabs.component.scss'
    ]
})
export class GenericTabsComponent implements OnInit {
    tabBackgroundColor = 'primary';
    @Input() tabs: GenericProfileTabsModel[];

    @Output() subTabLoadData = new EventEmitter();
    @Output() buttonActions = new EventEmitter();
    @Input() dataResourceValue: any;
    @Input() dataResourceName: any;
    @Input() dataResourceRecId: any;
    @Input() operatorPermission: any;
    @Input() userRole: boolean;
    @Input() isLocked: boolean;
    @Input() resourceTab: number;
    resource: number;
    page: number;
    itemFrom: number;
    totalItems: number;
    pageLinks: any;
    providerId: any;
    providerName:any;
    providerRecID: any;
    userLevel: boolean;
    accessPermission: boolean;
    locked: boolean;
    loader: boolean;
    loaderValue: any;

    constructor(
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private principal: Principal,
        private http: HttpClient,
        public snackBar: MatSnackBar,
        private loaderService: LoaderService,
         private dataresourceTabService: DataResourceTabService

        ) { }

    ngOnInit() {
        //console.log('Data Resource Value', this.dataResourceValue);
        //console.log('Data Resource Name', this.dataResourceName);
        this.loaderService.display(false);
        this.loader = false;
        this.accessPermission = this.operatorPermission;
        console.log('Access Permission', this.accessPermission);
        console.log(this.userRole);
        this.providerId = this.dataResourceValue;
        this.providerName = this.dataResourceName;
        this.providerRecID = this.dataResourceRecId;
        this.pageLinks = {
            last: 0
        };
        this.page = 0;
        this.itemFrom = 0;
        this.totalItems = 0;
        this.userLevel = this.principal.hasAnyPermissionAboveDirect(200);
        console.log('this.userLevel',this.userLevel);
        this.locked= this.isLocked;
        console.log('locked',this.locked);
      this.dataresourceTabService.messageResource$.subscribe(message =>{
          console.log('message',message);
          if(message === 'resources')
          {
            this.resource=2;
          }
          else if(message === 'partners')
          {
              this.resource = 1;
            //  window.scroll(0,document.body.scrollHeight);
          }
          else 
          {
              this.resource = 0;
          }
      })
      if(localStorage.getItem('partners'))
      {
        setTimeout(() => {
            window.scroll(0,document.body.scrollHeight);       
        }, 1500);
         this.resource=1;
        localStorage.removeItem('partners');
      }
      if(localStorage.getItem('resources'))
      {
        this.resource=2;
        localStorage.removeItem('resources');
      }
     
    }
    tabChanges(event: any) {
        console.log(event.tab.textLabel);
        if(event.tab.textLabel === 'Overview')
        {
            this.resource=0;
        }
        if(event.tab.textLabel === 'Details')
        {
            this.resource=1;
        }
        if(event.tab.textLabel === 'Samples & Resources ( 0 )')
        {
            this.resource=2;
        }
        this.googleAnalyticsService.emitEvent('Provider Profile - ' + event.tab.textLabel, 'Tab Link Clicked', 'Provider Profile - Tab - ' + event.tab.textLabel);
        console.log(event.tab.textLabel);
    }
    expansionPanel(event: any, eve: any) {
        console.log('expansion panel......', event);
        console.log('panel......', eve);
        this.googleAnalyticsService.emitEvent('Provider Profile - ' + eve + ' - ' + event, 'Expand Panel Link Clicked', 'Provider Profile - Tab - ' + eve + ' - Panel ' + ' - ' + event);
    }
    loadSubTabPage(page) {
        this.page = page;
        this.itemFrom = (this.page === 0) ? 0 : (this.page * 10);
        if (this.itemFrom <= this.totalItems) {
            this.subTabLoadData.emit(this.itemFrom);
        }
    }

    trackByIndex(index) {
        return index;
    }

    buttonClick(button) {
        this.buttonActions.emit(button);
    }

    onclick(event) {
        console.log('called',event);
        //this.loaderService.display(true);
        this.loader=true;
        this.fileDownload(event.clickableObj.fileID);
        this.loaderValue = event;
    }

    fileDownload(fileID) {
        this.http.get('api/filedownload/' + fileID, {responseType: 'text'}).subscribe(data => {
            if (data && data !== 'file not found') {
                window.location.href = 'api/filedownload/' + fileID;
               // this.loaderService.display(false);
        this.loader=false;
            } else {
                this.snackBar.open('File not found!', 'Close', {
                    duration: 5000
                });
                //this.loaderService.display(false);
                this.loader=false;
            }
        }, error => {
            console.log(error);
            this.snackBar.open('File not found!', 'Close', {
                duration: 5000
            });
            //this.loaderService.display(false);
            this.loader=false;
        });
    }

}
