import { Observable, of as observableOf } from 'rxjs';
import { DataSource } from '@angular/cdk/table';
import { Component, OnInit, Input } from '@angular/core';
import { GoogleAnalyticsEventsService } from '../googleanalytics/google-analytics-events.service';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Principal } from '../auth/principal.service';
import { AddResourceAdminFormComponent } from '../../add-resource-admin/add-resource-admin-form.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'app/shared/dialogs/confirm-dialog.component';
import { DataResourceTabService } from 'app/entities/data-provider/data-resourcetab.service';
import { Router } from '@angular/router';

export interface DocumentsData {
    fileName: string;
    dataResourcePurpose: any;
    dataResourcePurposeType: any;
    dateCreated: string;
    fileID: string;
}

export class DocumentsDataSource extends DataSource<DocumentsData> {

    constructor(private data: DocumentsData[]) {
        super();
    }

    connect(): Observable<DocumentsData[]> {
        return observableOf(this.data);
    }

    disconnect() { }

}

@Component({
    selector: 'jhi-generic-profile-datatable',
    templateUrl: './generic-profile-datatable.component.html',
    styleUrls: [
        './generic-profile.component.css'
    ]
})
export class GenericProfileDatatableComponent implements OnInit {
    @Input() displayedColumns = ['fileName', 'purpose', 'purposeType', 'dateCreated'];
    exampleDatabase = null;
    @Input() dataSource: DocumentsDataSource;
    @Input() blockContent: boolean;
    @Input() dataResource: any;
    @Input() dataProviderName: any;
    @Input() dataRecordId: any;
    @Input() editResourcePermission: any;
    dataProviderId: any;
    providerName: any;
    providerRecID: any;
    userLevel: boolean;
    dialogRef: MatDialogRef<ConfirmDialogComponent>;
    resDialogRef: MatDialogRef<any>;
    isAdmin: any;
    editAccessPermissions: boolean;
    constructor(
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private http: HttpClient,
        public snackBar: MatSnackBar,
        private principal: Principal,
        public dialog: MatDialog,
        private router: Router,
        private dataresourceTabService: DataResourceTabService,
    ) {
        this.dataSource = new DocumentsDataSource(this.exampleDatabase);
    }

    ngOnInit() {
        //console.log('Data Resource', this.dataResource);
        this.dataProviderId = this.dataResource;
        this.providerName = this.dataProviderName;
        this.providerRecID = this.dataRecordId;
        this.editAccessPermissions = this.editResourcePermission;
        console.log('Add Resource', this.editAccessPermissions);
        this.userLevel = this.principal.hasAnyPermissionAboveDirect(200);
        this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
    }

    fileDownload(fileID) {
        this.http.get('api/filedownload/' + fileID, {responseType: 'text'}).subscribe(data => {
            if (data && data !== 'file not found') {
                window.location.href = 'api/filedownload/' + fileID;
            } else {
                this.snackBar.open('File not found!', 'Close', {
                    duration: 5000
                });
            }
        }, error => {
            console.log(error);
            this.snackBar.open('File not found!', 'Close', {
                duration: 5000
            });
        });
    }

    addResource() {
        //console.log('add resource.....');
        this.resDialogRef = this.dialog.open(AddResourceAdminFormComponent, { // To open up the Resource Admin Form in Provider Profile Page - Sample & Resources Tab.
        width: '1200px',
        disableClose: true
        });
        this.resDialogRef.afterClosed().subscribe(result=> {
			console.log('result',result);
            this.dataresourceTabService.sendResourceTab('resources');
        });
        this.resDialogRef.componentInstance.basicProvider = this.dataProviderId;
        this.resDialogRef.componentInstance.dataName = this.providerName;
        this.resDialogRef.componentInstance.providerRecordID = this.providerRecID;
        this.resDialogRef.componentInstance.submissionMessage = true;
    }

    upgrade() {
        this.router.navigate(['/upgrade']);
    }

    formSave(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
}
