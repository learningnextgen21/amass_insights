import { Component, OnInit, Input } from '@angular/core';
import { GenericProfileTabsModel } from './generic-profile-tabs.model';

@Component({
    selector: 'jhi-generic-profile',
    templateUrl: './generic-profile.component.html',
    styleUrls: [
        './generic-profile.component.css'
    ]
})
export class GenericProfileComponent implements OnInit {
    @Input() tabs: GenericProfileTabsModel[];
    constructor() { }

    ngOnInit() {

    }

}
