import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GenericProfileTabsModel } from './generic-profile-tabs.model';
import { DomSanitizer } from '@angular/platform-browser';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { Principal } from '../../shared/auth/principal.service';

@Component({
    selector: 'jhi-generic-profile-subtab',
    templateUrl: './generic-profile-subtabs.component.html',
    styleUrls: [
        './generic-profile-subtabs.component.scss',
        '../../shared/share-buttons/share-buttons.scss',
        '../../entities/data-article/data-article.component.css',
        '../../entities/data-provider/data-provider.component.scss'
    ]
})
export class GenericProfileSubTabsComponent implements OnInit {
    tabBackgroundColor = 'primary';
    @Input() subTabs: GenericProfileTabsModel[];
    @Output() subTabLoadData = new EventEmitter();
    @Output() buttonActions = new EventEmitter();
    page: number;
    itemFrom: number;
    totalItems: number;
    pageLinks: any;
    userLevel: boolean;
    progressBarColor = 'primary';
	progressBarMode = 'determinate';
    constructor(public _DomSanitizer: DomSanitizer, public googleAnalyticsService: GoogleAnalyticsEventsService, private principal: Principal) { }

    ngOnInit() {
        this.userLevel = this.principal.hasAnyPermissionAboveDirect(200);
        this.pageLinks = {
            last: 0
        };
        this.page = 0;
        this.itemFrom = 0;
        this.totalItems = 0;
    }

    loadSubTabPage(page) {
        this.page = page;
        this.itemFrom = (this.page === 0) ? 0 : (this.page * 10);
        if (this.itemFrom <= this.totalItems) {
            this.subTabLoadData.emit(this.itemFrom);
        }
    }

    trackByIndex(index) {
        return index;
    }

    buttonClick(button) {
        this.buttonActions.emit(button);
    }
    subTabTitle(events) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

}
