import { GenericProfileButtonsModel, GenericProfileShareButtonsModel } from './generic-profile-buttons.model';

export class GenericProfileTabsModel {
	constructor(
        public tabLabelName?: string,
        public tabLabelRight?: any,
        public tabLabelRightTooltip?: string,
        public tabLabelTooltip?: string,
        public tabContent?: GenericProfileTabContent[],
        public subTabs?: GenericProfileTabsModel[],
        public locked?: boolean
	) {

	}
}

export class GenericProfileTabContent {
    constructor(
        public contentType?: TabContentType,
        public isTreeNodesWithPanel?: boolean,
        public expansionPanel?: GenericProfileExpansionPanel,
        public treeNodes?: any[],
        public list?: GenericSubTabList,
        public noDataFoundMsg?: string,
        public dataTable?: any,
        public dataTableDisplayColumns?: any[],
        public blockContent?: boolean,
        public plainText?: any,
        public buttons?: GenericProfileButtonsModel[],
        public shareButtons?: GenericProfileShareButtonsModel[]
    ) {

    }
}

export enum TabContentType {
    PANEL_WITH_TREE,
    PANEL_WITH_TEXT,
    LIST,
    PLAIN_TEXT
}

export class GenericSubTabList {
    constructor(
        public data?: GenericSubTabListItem[],
        public loadPage?: (page: number) => void,
        public page?: number,
        public itemFrom?: number,
        public pageLinks?: any,
        public totalItems?: number
    ) {

    }
}

export class GenericSubTabListItem {
    constructor(
        public id?: number,
        public title?: string,
        public subTitle?: string,
        public image?: any,
        public base64Image?: any,
        public url?: string,
        public mainContent?: any,
        public mainContentHTML?: any,
        public topRight?: any,
        public topLeft?: any,
        public topLeftLabel?: any,
        public topLeftLabelTooltip?: string,
        public topLeftContent?: any[],
        public topRightLabel?: any,
        public topRightLabelTooltip?: string,
        public topRightContent?: any[],
        public bottomRightLabel?: any,
        public bottomRightLabelTooltip?: string,
        public bottomRightContent?: any[],
        public bottomLeft?: any,
        public bottomRight?: any,
        public locked?: boolean,
        public completenessBar?: any,
        public overAllScore?: any,
        public addedDate?: any,
        public updatedDate?: any,
        public providerStatus?: any,
        public providersTabs?: any,
        public articlesTabs?: any
    ) {

    }
}

export class GenericProfileExpansionPanel {
    constructor(
        public panelHeading?: string,
        public expanded?: boolean,
        public tooltip?: string,
        public editAccess?: boolean
    ) {

    }
}
