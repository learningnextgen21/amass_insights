import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GenericProfileButtonsModel } from './generic-profile-buttons.model';

@Component({
    selector: 'jhi-generic-profile-buttons',
    templateUrl: './generic-profile-buttons.component.html'
})
export class GenericProfileButtonsComponent implements OnInit {
    @Input() button: GenericProfileButtonsModel;
    @Output() action = new EventEmitter();
    constructor() { }

    ngOnInit() {

    }

    buttonAction() {
        this.action.emit(this.button); // To emit the button values in a generic way.
    }

}
