import { Subject } from 'rxjs';

export class GenericProfileButtonsModel {
	constructor(
		public label?: string,
        public buttonTooltip?: string,
        public action?: string,
        public actionArguments?: any[],
        public status?: any
	) {

    }
}

export class GenericProfileShareButtonsModel {
	constructor(
        public buttonTooltip?: string,
        public url?: string,
        public type?: string
	) {

    }
}
