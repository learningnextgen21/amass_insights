import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';

import {
	DatamarketplaceSharedLibsModule,
	DatamarketplaceSharedCommonModule,
	CSRFService,
	AuthServerProvider,
	AccountService,
	UserService,
	StateStorageService,
	LoginService,
	LoginModalService,
	SigninModalService,
	ApplyDialogService,
	Principal,
	JhiTrackerService,
	HasAnyAuthorityDirective,
	HasNotAuthorityDirective,
	GenericFilterComponent,
	GenericFilterListComponent,
	AmassFilterService,
	JhiSocialComponent,
	SocialService,
	JhiLoginModalComponent,
    JhiSortbarComponent,
    ConfirmDialogComponent
} from './';
import { InvestorDialogComponent } from'./dialogs/investor-dialog.component';
import { GenericSearchComponent } from'./generic-search/generic-search.component';
import { SpinnerComponent } from '../account/spinner/spinner.component';
import { ProgressSpinnerComponent } from '../account/progress-spinner/progress-spinner.component';
import { AmassShareButtonsComponent } from './share-buttons/share-buttons.component';
import { ShareButtonModule } from '@ngx-share/button';
import { FormlyHorizontalWrapperComponent } from 'app/entities/data-provider';
import { FormlyEditorFieldComponent } from './custom-form-fields/editor-form-field.component';
import { FormlyAutoCompleteFieldComponent } from './custom-form-fields/autocomplete-form-field.component';
import { FormlyMultiSelectFieldComponent } from './custom-form-fields/multiselect-form-field.component';
import { FormlyDropdownFieldComponent } from './custom-form-fields/dropdown-form-field.component';
import { FormlyMaskFieldComponent } from './custom-form-fields/mask-form-field.component';
import { FormlySliderFieldComponent } from './custom-form-fields/slider-form-field.component';
import { FormlyRadioButtonFieldComponent } from './custom-form-fields/radio-button-field.component';
import { FormlyTextAreaFieldComponent } from './custom-form-fields/textarea-field.component';
import { FormlyFieldButton } from './custom-form-fields/button-field.component';
import { InactivityTimerComponent, GenericConfirmDialogComponent } from './auth/inactivity-timer.component';
import {InputTextModule} from 'primeng/inputtext';
import { EditorModule } from 'primeng/editor';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import {InputMaskModule} from 'primeng/inputmask';
import {SliderModule} from 'primeng/slider';
import {RadioButtonModule} from 'primeng/radiobutton';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {SidebarModule} from 'primeng/sidebar';
import {CalendarModule} from 'primeng/calendar';
import {FileUploadModule} from 'primeng/fileupload';
import {ButtonModule} from 'primeng/button';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyDatePickerFieldComponent } from './custom-form-fields/datepicker-field.component';
import { FormlyFileUploadFieldComponent } from './custom-form-fields/fileupload-form-field.component';
import { FormlyFieldCustomInput } from './custom-form-fields/input-form-field.component';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {CheckboxModule} from 'primeng/checkbox';
import { FormlyFieldCheckboxComponent } from './custom-form-fields/checkbox-form-field.component';
import { FieldsetModule } from 'primeng/fieldset';
import { FormlyFieldsetFormComponent } from './custom-form-fields/fieldset-form-field.component';

@NgModule({
	imports: [
		DatamarketplaceSharedLibsModule,
        DatamarketplaceSharedCommonModule,
        ReactiveFormsModule,
        ShareButtonModule,
        InputTextModule,
        EditorModule,
        AutoCompleteModule,
        DropdownModule,
        MultiSelectModule,
        InputMaskModule,
        SliderModule,
        RadioButtonModule,
        InputTextareaModule,
		SidebarModule,
		CalendarModule,
		FileUploadModule,
		ButtonModule,
		ProgressSpinnerModule,
        CheckboxModule,
        FieldsetModule
	],
	declarations: [
		JhiSocialComponent,
		JhiLoginModalComponent,
		HasAnyAuthorityDirective,
        HasNotAuthorityDirective,
        JhiSortbarComponent,
		GenericFilterComponent,
		GenericFilterListComponent,
		GenericSearchComponent,
        SpinnerComponent,
		ProgressSpinnerComponent,
        ConfirmDialogComponent,
		InvestorDialogComponent,
        AmassShareButtonsComponent,
        FormlyHorizontalWrapperComponent,
        FormlyEditorFieldComponent,
        FormlyAutoCompleteFieldComponent,
        FormlyMultiSelectFieldComponent,
        FormlyDropdownFieldComponent,
        FormlyMaskFieldComponent,
        FormlySliderFieldComponent,
        FormlyRadioButtonFieldComponent,
        FormlyFieldCheckboxComponent,
		FormlyTextAreaFieldComponent,
		FormlyDatePickerFieldComponent,
		FormlyFileUploadFieldComponent,
		FormlyFieldButton,
		FormlyFieldCustomInput,
        FormlyFieldsetFormComponent
        // InactivityTimerComponent,
        // GenericConfirmDialogComponent
	],
	providers: [
		LoginService,
		LoginModalService,
		SigninModalService,
		ApplyDialogService,
		AccountService,
		StateStorageService,
		Principal,
		CSRFService,
		JhiTrackerService,
		AuthServerProvider,
		SocialService,
		UserService,
		DatePipe,
		AmassFilterService
	],
	entryComponents: [JhiLoginModalComponent, ConfirmDialogComponent, InvestorDialogComponent],
	exports: [
		DatamarketplaceSharedCommonModule,
		JhiSocialComponent,
		JhiLoginModalComponent,
		HasAnyAuthorityDirective,
        HasNotAuthorityDirective,
        JhiSortbarComponent,
		GenericFilterComponent,
		GenericFilterListComponent,
		GenericSearchComponent,
        DatePipe,
        SpinnerComponent,
		ProgressSpinnerComponent,
        ConfirmDialogComponent,
		InvestorDialogComponent,
        AmassShareButtonsComponent
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class DatamarketplaceSharedModule {}
