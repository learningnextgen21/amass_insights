import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SocialService } from './social.service';
import { CSRFService } from '../auth/csrf.service';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { CookieService } from 'ngx-cookie';

@Component({
    selector: 'jhi-social',
    templateUrl: './social.component.html',
    styleUrls: ['./social.component.css']
})
export class JhiSocialComponent implements OnInit, OnChanges {
    @Input() provider: string;
    @Input() dialogType: string;
    label: string;
    dialogLabel: string;
    providerSetting: string;
    providerURL: string;
    @Input() csrf: string;
    isAccountActive: boolean;
    @Input() agreeTerms: any;
    @Input() authorities: any;
    error: string;

    constructor(
        private csrfService: CSRFService,
        private socialService: SocialService,
        private cookieService: CookieService,
        private activatedRoute: ActivatedRoute,
        public googleAnalyticsService: GoogleAnalyticsEventsService
    ) {}

	ngOnInit() {
        // console.log('Type', this.dialogType, this.dialogLabel);
		this.isAccountActive = false;
		this.label = this.provider === 'linkedin' ? 'LinkedIn' : this.provider.charAt(0).toUpperCase() + this.provider.slice(1);
        this.dialogLabel = this.dialogType.charAt(0).toUpperCase() + this.dialogType.slice(1);
		this.providerSetting = this.socialService.getProviderSetting(this.provider);
		this.providerURL = this.socialService.getProviderURL(this.provider);
		this.csrf = this.csrfService.getCSRF();
		this.activatedRoute.queryParams.subscribe(params => {
			if (params['active'] === '0') {
				this.isAccountActive = true;
			}
		});
	}

	fire(event) {
        this.error = null;
		if (!this.agreeTerms && this.dialogType.replace(' ', '').toLowerCase() === 'signup') {
			this.error = 'Please agree to our Privacy Policy and Terms of Service to sign up.';
			event.preventDefault();
        }
        if (this.authorities === '' && this.dialogType.replace(' ', '').toLowerCase() === 'signup') {
            this.error = 'Please select User Role to sign up.';
			event.preventDefault();
		}
    }
    /* linkedin component terms validation*/
    linkdinSubmit(event) {
        //console.log('Type', this.dialogType, this.dialogLabel);
        if(this.dialogType === 'Activate My Account') {
            this.cookieService.put('LINKEDIN', 'True');
        }
        if (this.agreeTerms !== true && this.dialogLabel === 'Sign Up') {
            this.error = 'Please agree to our Privacy Policy and Terms of Service to sign up.';
            event.preventDefault();
        } else if (this.authorities === '' && this.dialogLabel === 'Sign Up') {
            this.error = 'Please select User Role to sign up.';
            event.preventDefault();
        }
        if (this.dialogLabel === 'Sign Up') {
            this.googleAnalyticsService.emitEvent('Sign Up', 'LinkedIn Form Submitted', 'Sign Up - Button - Sign Up with LinkedIn');
        } else {
            this.googleAnalyticsService.emitEvent('Login', 'Logged In with LinkedIn', 'Login - Button - Login with LinkedIn');
        }
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        const log: string[] = [];

        for (const p in changes) {
            if (changes.hasOwnProperty(p)) {
                const c = changes[p];
                const from = JSON.stringify(c.previousValue);
                const to = JSON.stringify(c.currentValue);
                if (p === 'agreeTerms' && to) {
                    this.error = null;
                }
            }
        }
    }
}
