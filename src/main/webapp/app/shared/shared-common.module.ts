import { NgModule, LOCALE_ID, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { WindowRef } from './tracker/window.service';
import {
	DatamarketplaceSharedLibsModule,
	JhiLanguageHelper,
	FindLanguageFromKeyPipe,
	JhiAlertComponent,
	JhiAlertErrorComponent,
	StatisticsComponent,
} from './';
import { TranslateModule } from '@ngx-translate/core';
// import { HomeEventsComponent } from './genric-home/genric-home-event.component';
// import { HomeArticleComponent } from './genric-home/genric-home-article.component';
import { from } from 'rxjs';

@NgModule({
	imports: [
        DatamarketplaceSharedLibsModule
	],
	declarations: [
		FindLanguageFromKeyPipe,
		JhiAlertComponent,
		JhiAlertErrorComponent,
        StatisticsComponent,
       // HomeEventsComponent,
      //  HomeArticleComponent
	],
	providers: [
		WindowRef,
		Title,
		{
			provide: LOCALE_ID,
			useValue: 'en'
        },
        JhiLanguageHelper
	],
	exports: [
        DatamarketplaceSharedLibsModule,
        TranslateModule,
		FindLanguageFromKeyPipe,
		JhiAlertComponent,
		JhiAlertErrorComponent,
        StatisticsComponent,
       // HomeEventsComponent,
      //  HomeArticleComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceSharedCommonModule {}
