import { Injectable } from '@angular/core';
import { DataCategory } from 'app/entities/data-category';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Principal } from '../auth/principal.service';
import { HttpClient } from '@angular/common/http';
import { DataOrganization } from 'app/entities/data-organization';

@Injectable()
export class AmassFilterService {
	private providerTypesUrl = 'api/data-provider-types';
    private mainDataCategoriesUrl = 'api/es/maincategory';
	private esSearchUrl = 'api/ur/qsearch';
    private esResourceUrl = 'api/es/qsearch';
	private dataProviderTagUrl = 'api/data-provider-tags';
	private dataProviderTag = 'api/data-provider-tagsValue';
    private frequency = 'api/es/qdeliveryferquency';
    private dataCategoriesUrl = 'api/es/datacategorysearch';
    private categorySearchUrl = 'api/es/categorysearch';
    private dataProviderPublicTag = 'api/public-tags';
    private newLookupCode = 'api/lookup-code-creation';
	constructor(
        private http: HttpClient,
        private principal: Principal
    ) {

    }

    setAuthorisedUrl() {
        if (this.principal.isAuthenticated()) {
            this.esSearchUrl = 'api/es/qsearch';
        }
    }

    setUnAuthorisedUrl() {
        if (!this.principal.isAuthenticated()) {
            this.esSearchUrl = 'api/ur/qsearch';
        }
    }

    unRestrictedQsearch(type: string, lookupmodule?: string, sort?: string) {
        let queryString = 'type=' + type;
        if (lookupmodule) {
            queryString += '&lookupmodule=' + lookupmodule;
        }
        if (sort) {
            queryString += '&sort=' + sort;
        } else {
            queryString += '&sort=';
        }
        return this.http.post('api/ur/qsearch?' + queryString, {});
    }

    getLookupCodes(lookupmodule?: string) {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source': {
				'include': [
					'id',
                    'lookupcode',
                    'description',
					'recordCount',
					'sortorder'
				]
			},
			'sort': [
				{
					'sortorder': 'asc'
				}
			],
			'size': 200,
			'filter': {
				'query': {
					'match': {
						'lookupmodule': lookupmodule
					}
				}
			}
		});
    }

	getProvidersTypes() {
		return this.http.get<any[]>(this.providerTypesUrl);
	}

	getMainDataCategories(query?: string) {
        const queryJSON = {
			'size': 0,
			'aggs': {
				'providerfilter': {
					'terms':
					{
						'field': 'mainDataCategory.id',
						size: 100
					},
					aggs: {
						'filterelement': {
							top_hits: {
								size: 1,
								_source: {
									include: ['mainDataCategory.datacategory', 'mainDataCategory.explanation']
								}
							}
						}
					}
				}
			}
        };
        if (query) {
            queryJSON['query'] = {
                'prefix': {
                    'mainDataCategory.datacategory': query
                }
            }
        }
		return this.http.post(this.mainDataCategoriesUrl, queryJSON);
    }
    getRelatedDataCategory() {
        return this.http.post(this.esSearchUrl + '?type=datacategory', {
			size: 150,
			'sort': [
				{
					'relevantDataCategoryProviderCount': 'desc'
				}
			],
			'_source': {
				'include': [
					'id',
					'datacategory',
					'relevantDataCategoryProviderCount',
					'locked',
                    'explanation',
                    'userPermission',
                    'recordID'
				]
			},
			query: {
				match_all: {}
			}
		});
    }

    getDtaCategoryImage(id): Observable<DataCategory> {
        return this.http.post(this.categorySearchUrl + '?type=datacategory' , {
                'filter': {
                    'query': {
                    'match': {
                        'id': id
                    }
                    }
                }
            });
    }

    getDataCategoriesWithImg(req?: any) {
        let options = {};
        if (req) {
            options = req;
        }
        return this.http.post(this.esSearchUrl + '?type=datacategory', options);
    }

    getESFilters(type?: string, countType?: string, include?: string) {
        type = type ? type : 'dataIndustry';
        countType = countType ? countType : 'relevantDataIndustryProviderCount';
        const sorting: any = {};
        sorting[countType] = 'desc';
        const queryJSON = {
			size: 100,
			'_source': {
				'include': [
					'id',
					include,
					countType,
					'explanation'
				]
			},
			'sort': [
				sorting
			]
        };
        return this.http.post(this.esSearchUrl + '?type=' + type, queryJSON);
    }

    getRelatedDataIndustry(query?: string) {
        const queryJSON = {
			size: 100,
			'_source': {
				'include': [
					'id',
					'dataIndustry',
					'relevantDataIndustryProviderCount',
					'explanation'
				]
			},
			'sort': [
				{
					'relevantDataIndustryProviderCount': 'desc'
				}
			]
        };

        if (query) {
            queryJSON['query'] = {
                'prefix': {
                   'dataIndustry': query
                }
            };
        } else {
            queryJSON['query'] = {
                'match_all': {}
            };
        }
        return this.http.post(this.esSearchUrl + '?type=dataindustry', queryJSON);
    }
    getMainDataIndustry(query?: string) {
        const queryJSON = {
            'size': 0,
            'aggs': {
                'dataindustries': {
                    'terms':
                    {
                        'field': 'mainDataIndustry.id',
                        size: 100
                    },
                    aggs: {
                        'mainDataIndustry': {
                            top_hits: {
                                size: 1,
                                _source: {
                                    include: [
                                        'mainDataIndustry.id',
                                        'mainDataIndustry.dataIndustry',
                                        'mainDataIndustry.explanation',
                                        'mainDataIndustry.relevantDataIndustryProviderCount'
                                    ]
                                }
                            }
                        }
                    }
                }
            }
		};
			if (query) {
				queryJSON['query'] = {
					'prefix': {
					'mainDataIndustry.dataIndustry': query
					}
				};
			}
        return this.http.post(this.esSearchUrl + '?type=dataprovider', queryJSON);
    }
    getDataFeatures(query?: string) {
        const queryJSON = {
            size: 500,
            'sort': [
                {
                    'providerCount': 'desc'
                }
            ],
            '_source': {
                'include': [
                    'id',
                    'dataFeature',
                    'providerCount',
                    'explanation'
                ]
            }
        };
        if (query) {
            queryJSON['query'] = {
                'prefix': {
                   'dataFeature': query
                }
            };
        } else {
            queryJSON['query'] = {
                'match_all': {}
            };
        }
        return this.http.post(this.esSearchUrl + '?type=datafeature', queryJSON);
    }
    getGeographicFocus() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source': {
				'include': [
					'id',
					'lookupcode',
					'recordCount',
					'sortorder'
				]
			},
			'sort': [
				{
					'sortorder': 'asc'
				}
			],
			'size': 200,
			'filter': {
				'query': {
					'match': {
						'lookupmodule': 'GEOGRAPHICAL_FOCUS'
					}
				}
			}
		});
	}
	getGeographicRegion() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source': {
				'include': [
					'id',
					'lookupcode',
					'recordCount',
					'sortorder'
				]
			},
			'sort': [
				{
					'sortorder': 'asc'
				}
			],
			'size': 200,
			'filter': {
				'query': {
					'match': {
						'lookupmodule': 'GEOGRAPHICAL_REGION'
					}
				}
			}
		});
    }
    getMainAssetClass() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
			'size': 0,
			'aggs': {
				'mainAssetclass': {
					'terms':
					{ 'field': 'mainAssetClass', size: 100 },
					aggs: {
						'mainAssetClass': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['mainAssetClass'] }
							}
						}
					}
				}
			}
		});
    }
 /*------------- Ranjith added query for filter  Start----------*/
 getCategoriesForFilter() {
    return this.http.post(this.esSearchUrl + '?type=dataprovider', {
        'size': 0,
        'aggs': {
            'categories': {
                'terms':
                { 'field': 'categories.id', size: 100 },
                aggs: {
                    'categories': {
                        top_hits: {
                            size: 1,
                            _source:
                            { include: ['categories.id', 'categories.desc', ] }
                        }
                    }
                }
            }
        }
    });
}

 getDeliveryFormatsForFilter() {
    return this.http.post(this.esSearchUrl + '?type=dataprovider', {
        'size': 0,
        'aggs': {
            'deliveryFormats': {
                'terms':
                { 'field': 'deliveryFormats.id', size: 100 },
                aggs: {
                    'deliveryFormats': {
                        top_hits: {
                            size: 1,
                            _source:
                            { include: ['deliveryFormats.id', 'deliveryFormats.desc', ] }
                        }
                    }
                }
            }
        }
    });
}

    getPricingModelForFilter() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
			'size': 0,
			'aggs': {
				'dataProvidersPricingModels': {
					'terms':
					{ 'field': 'dataProvidersPricingModels.id', size: 100 },
					aggs: {
						'dataProvidersPricingModels': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['dataProvidersPricingModels.id', 'dataProvidersPricingModels.description', ] }
							}
						}
					}
				}
			}
		});
    }

    /*getWillingnessToProvideToInvestors() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
			'size': 0,
			'aggs': {
				'scoreInvestorsWillingness': {
					'terms':
					{ 'field': 'scoreInvestorsWillingness.id', size: 100 },
					aggs: {
						'scoreInvestorsWillingness': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['scoreInvestorsWillingness.id', 'scoreInvestorsWillingness.description', ] }
							}
						}
					}
				}
			}
		});
    }

    getPriorityForFilter() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
			'size': 0,
			'aggs': {
				'priority': {
					'terms':
					{ 'field': 'priority', size: 100 },
					aggs: {
						'priority': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['priority'] }
							}
						}
					}
				}
			}
		});
    } */

    getResearchStatus() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
			'size': 0,
			'aggs': {
				'ieiStatus': {
					'terms':
					{ 'field': 'ieiStatus.id', size: 100 },
					aggs: {
						'ieiStatus': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['ieiStatus.id', 'ieiStatus.description', ] }
							}
						}
					}
				}
			}
		});
    }

    getProfileStatus() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
			'size': 0,
			'aggs': {
				'marketplaceStatus': {
					'terms':
					{ 'field': 'marketplaceStatus.id', size: 100 },
					aggs: {
						'marketplaceStatus': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['marketplaceStatus.id', 'marketplaceStatus.description', ] }
							}
						}
					}
				}
			}
		});
    }


    getGeographicalFocus() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
            'size': 0,
            'aggs': {
                'geographicalFoci': {
                    'terms':
                    { 'field': 'geographicalFoci.id', size: 100 },
                    aggs: {
                        'geographicalFoci': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['geographicalFoci.id', 'geographicalFoci.description', ] }
                            }
                        }
                    }
                }
            }
        });
    }

    getPriorityFilter() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
			'size': 0,
			'aggs': {
				'priority': {
					'terms':
					{ 'field': 'priority', size: 100 },
					aggs: {
						'priority': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['priority'] }
							}
						}
					}
				}
			}
		});
    }

    getIndustriesForFilter() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
            'size': 0,
            'aggs': {
                'industries': {
                    'terms':
                    { 'field': 'industries.id', size: 100 },
                    aggs: {
                        'industries': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['industries.id', 'industries.desc', ] }
                            }
                        }
                    }
                }
            }
        });
    }

    getFeaturesForFilter() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
            'size': 0,
            'aggs': {
                'features': {
                    'terms':
                    { 'field': 'features.id', size: 100 },
                    aggs: {
                        'features': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['features.id', 'features.desc', ] }
                            }
                        }
                    }
                }
            }
        });
    }

    getAssetClassesForFilter() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
            'size': 0,
            'aggs': {
                'assetClasses': {
                    'terms':
                    { 'field': 'assetClasses.id', size: 100 },
                    aggs: {
                        'assetClasses': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['assetClasses.id', 'assetClasses.desc', ] }
                            }
                        }
                    }
                }
            }
        });
    }

    getSecurityTypesForFilter() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
            'size': 0,
            'aggs': {
                'securityTypes': {
                    'terms':
                    { 'field': 'securityTypes.id', size: 100 },
                    aggs: {
                        'securityTypes': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['securityTypes.id', 'securityTypes.desc', ] }
                            }
                        }
                    }
                }
            }
        });
    }

    getSourcesForFilter() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
            'size': 0,
            'aggs': {
                'sources': {
                    'terms':
                    { 'field': 'sources.id', size: 100 },
                    aggs: {
                        'sources': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['sources.id', 'sources.desc', ] }
                            }
                        }
                    }
                }
            }
        });
    }

    getInvestorTypes() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
            'size': 0,
            'aggs': {
                'investorTypes': {
                    'terms':
                    { 'field': 'investorTypes.id', size: 100 },
                    aggs: {
                        'investorTypes': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['investorTypes.id', 'investorTypes.description', ] }
                            }
                        }
                    }
                }
            }
        });
    }

    getRelevantSectorsForFilter() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
            'size': 0,
            'aggs': {
                'relevantSectors': {
                    'terms':
                    { 'field': 'relevantSectors.id', size: 100 },
                    aggs: {
                        'relevantSectors': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['relevantSectors.id', 'relevantSectors.desc', ] }
                            }
                        }
                    }
                }
            }
        });
    }

    getScoreInvestorsWillingness() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
			'size': 0,
			'aggs': {
				'scoreInvestorsWillingness': {
					'terms':
					{ 'field': 'scoreInvestorsWillingness.id', size: 100 },
					aggs: {
						'scoreInvestorsWillingness': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['scoreInvestorsWillingness.id', 'scoreInvestorsWillingness.description', ] }
							}
						}
					}
				}
			}
		});
    }

    getUniquenessFilterArray() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
			'size': 0,
			'aggs': {
				'scoreUniqueness': {
					'terms':
					{ 'field': 'scoreUniqueness.id', size: 100 },
					aggs: {
						'scoreUniqueness': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['scoreUniqueness.id', 'scoreUniqueness.description', ] }
							}
						}
					}
				}
			}
		});
    }

    getValueFilterArray() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
			'size': 0,
			'aggs': {
				'scoreValue': {
					'terms':
					{ 'field': 'scoreValue.id', size: 100 },
					aggs: {
						'scoreValue': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['scoreValue.id', 'scoreValue.description', ] }
							}
						}
					}
				}
			}
		});
    }

    getScoreReadinessFilterArray() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
			'size': 0,
			'aggs': {
				'scoreReadiness': {
					'terms':
					{ 'field': 'scoreReadiness.id', size: 100 },
					aggs: {
						'scoreReadiness': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['scoreReadiness.id', 'scoreReadiness.description', ] }
							}
						}
					}
				}
			}
		});
    }
    getScoreOverallFilterArray() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
			'size': 0,
			'aggs': {
				'scoreOverall': {
					'terms':
					{ 'field': 'scoreOverall.id', size: 100 },
					aggs: {
						'scoreOverall': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['scoreOverall.id', 'scoreOverall.description', ] }
							}
						}
					}
				}
			}
		});
    }

    getDeliveryMethodsForFilter() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
            'size': 0,
            'aggs': {
                'deliveryMethods': {
                    'terms':
                    { 'field': 'deliveryMethods.id', size: 100 },
                    aggs: {
                        'deliveryMethods': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['deliveryMethods.id', 'deliveryMethods.desc', ] }
                            }
                        }
                    }
                }
            }
        });
    }

    getDeliveryFrequenciesForFilter() {
        return this.http.post(this.esSearchUrl + '?type=dataprovider', {
            'size': 0,
            'aggs': {
                'deliveryFrequencies': {
                    'terms':
                    { 'field': 'deliveryFrequencies.id', size: 100 },
                    aggs: {
                        'deliveryFrequencies': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['deliveryFrequencies.id', 'deliveryFrequencies.description', ] }
                            }
                        }
                    }
                }
            }
        });
    }
    /*------------- Ranjith added query for filter  END----------*/
    getRelatedAssetClass() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source': {
				'include': [
					'id',
					'description',
					'recordCount',
					'sortorder'
				]
			},
			'sort': [
				{
					'sortorder': 'asc'
				}
			],
			'size': 200,
			'filter': {
				'query': {
					'match': {
						'lookupmodule': 'ASSET_CLASS'
					}
				}
			}
        });
    }
   /* getResearchStatus() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source': {
				'include': [
					'id',
					'description',
					'recordCount',
					'sortorder'
				]
			},
			'sort': [
				{
					'recordCount': 'desc'
				}
			],
			'size': 200,
			'filter': {
				'query': {
					'match': {
						'lookupmodule': 'IEI_STATUS'
					}
				}
			}
        });
    }*/
    getPriority() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source': {
				'include': [
					'id',
					'description',
					'recordCount',
					'sortorder'
				]
			},
			'sort': [
				{
					'recordCount': 'desc'
				}
			],
			'size': 200,
			'filter': {
				'query': {
					'match': {
						'lookupmodule': 'PRIORITY'
					}
				}
			}
        });
    }

    /*getProfileStatus() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source': {
				'include': [
					'id',
					'description',
                    'recordCount',
                    'lookupcode',
					'sortorder'
				]
			},
			'sort': [
				{
					'recordCount': 'desc'
				}
			],
			'size': 200,
			'filter': {
				'query': {
					'match': {
						'lookupmodule': 'MARKETPLACE_STATUS'
					}
				}
			}
        });
    }*/
    getSecurityType() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source': {
				'include': [
					'id',
					'lookupcode',
					'recordCount',
					'sortorder'
				]
			},
			'sort': [
				{
					'recordCount': 'desc'
				}
			],
			'size': 50,
			'filter': {
				'query': {
					'match': {
						'lookupmodule': 'RLVNT_SECURITY_TYPE'
					}
				}
			}
        });
    }
    getRelatedSectors() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source': {
				'include': [
					'id',
					'lookupcode',
					'recordCount',
					'sortorder'
				]
			},
			'sort': [
				{
					'recordCount': 'desc'
				}
			],
			'size': 50,
			'filter': {
				'query': {
					'match': {
						'lookupmodule': 'RLVNT_SECTOR'
					}
				}
			}
        });
    }
    getInvestorType() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source': {
				'include': [
					'id',
					'lookupcode',
					'recordCount',
					'sortorder'
				]
			},
			'sort': [
				{
					'sortorder': 'asc'
				}
			],
			'filter': {
				'query': {
					'match': {
						'lookupmodule': 'RLVNT_INVESTOR_TYPE'
					}
				}
			}
		});
    }
    getDataSource() {
        return this.http.post(this.esSearchUrl + '?type=datasource', {
			size: 500,
			'sort': [
				{
					'providerCount': 'desc'
				}
			],
			'_source': {
				'include': [
					'id',
					'dataSource',
					'providerCount',
					'explanation'
				]
			},
			query: {
				match_all: {}
			}
        });
	}

	findByResourceId(id: number): Observable<any> {
		console.log('Relavant Id', id);
        const req = {
            _source: {
                include: [
                    'id',
                    'recordID',
					'name',
					'description',
					'link',
					'file',
					'userPermission',
					'marketplaceStatus',
					'workflows',
					'researchMethodsCompleted',
					'ieiStatus',
					'priority',
					'topics',
					'purposes',
					'purposes.id',
					'purposes.purpose',
					'purposes.recordID',
					'purposes.sortorder',
					'purposeTypes',
					'relavantOrganizations',
					'relavantDataProviders.recordID',
					'relavantDataProviders.id',
					'webCreatedDate',
					'createdTimeFormula',
				]
			},
			'size': 500,
			'filter': {
				'query': {
					'match': {
						'relavantDataProviders.id': id
					}
				}
			}
		};
		return this.http.post(this.esResourceUrl + '?type=dataresources', req);
	}
	findByOperatorResourceId(id: number): Observable<any> {
		console.log('Relavant Id', id);
        const req = {
            _source: {
                include: [
                    'id',
                    'recordID',
					'name',
					'description',
					'link',
					'file',
					'userPermission',
					'marketplaceStatus',
					'workflows',
					'researchMethodsCompleted',
					'creatorUserId',
					'ieiStatus',
					'priority',
					'topics',
					'purposes',
					'purposes.id',
					'purposes.purpose',
					'purposes.recordID',
					'purposes.sortorder',
					'purposeTypes',
					'relavantOrganizations',
					'relavantDataProviders.recordID',
					'relavantDataProviders.id',
					'webCreatedDate',
					'createdTimeFormula',
				]
			},
			'size': 500,
			'filter': {
				'query': {
					'match': {
						'creatorUserId': id
					}
				}
			}
		};
		return this.http.post(this.esResourceUrl + '?type=dataresources', req);
	}
	findByRelevantLinkId(id: number): Observable<any> {
		console.log('Relavant Id', id);
        const req = {
            _source: {
                include: [
                    'id',
                    'recordID',
					'name',
					'description',
					'link',
					'file',
					'userPermission',
					'marketplaceStatus',
					'workflows',
					'researchMethodsCompleted',
					'ieiStatus',
					'priority',
					'topics',
					'purposes',
					'purposes.id',
					'purposes.purpose',
					'purposes.recordID',
					'purposes.sortorder',
					'purposeTypes',
					'relavantOrganizations',
					'relavantDataProviders',
					'relavantDataProviders.id',
					'webCreatedDate',
					'createdTimeFormula'
				]
			},
			'size': 500,
			'filter': {
				'query': {
					'match': {
						'link.id': id
					}
				}
			}
		};
		return this.http.post(this.esResourceUrl + '?type=dataresources', req);
	}

	findResourceByResourceId(id: number): Observable<any> {
		console.log('Relavant Id', id);
        const req = {
			'filter': {
				'query': {
					'match': {
						'id': id
					}
				}
			}
		};
		return this.http.post(this.esResourceUrl + '?type=dataresources', req);
	}

	findByLinkId(id: number): Observable<any> {
        const req = {
            /* _source: {
                exclude: [],
                include: [
                    'id',
                    'recordID',
                    'link',
					'linkTitle',
					'dataLinksCategory'
				]
			}, */
			'filter': {
				'query': {
					'match': {
						'id': id
					}
				}
			}
		};
		return this.http.post(this.esSearchUrl + '?type=datalinks', req);
	}

	getDataLinks(query?: string) {
		const queryJSON = {
            '_source': {
                'include': [
                    'id',
					'link'
                ]
            }
        };
		if (query) {
            queryJSON['query'] = {
                'prefix': {
                   'link': query
                }
            };
        } else {
            queryJSON['query'] = {
                'match_all': {}
            };
        }
        return this.http.post(this.esSearchUrl + '?type=datalinks', queryJSON);
	}

	getAllFiles(query?: string) {
		const queryJSON = {
            '_source': {
                'include': [
                    'id',
                    'fileName'
                ]
            }
        };
		if (query) {
            queryJSON['query'] = {
                'prefix': {
                   'fileName': query
                }
            };
        } else {
            queryJSON['query'] = {
                'match_all': {}
            };
        }
        return this.http.post(this.esSearchUrl + '?type=documentsmetadata', queryJSON);
        // return this.http.post(this.esSearchUrl + '?type=documentsmetadata', {
		// 	'_source': {
		// 		'include': [
		// 			'id',
		// 			'fileName',
		// 		]
		// 	}
        // });
	}

	getDataTopics() {
        return this.http.post(this.esSearchUrl + '?type=datatopic', {
			size: 500,
			'sort': [
				{
					'sortOrder': 'asc'
				}
			],
			'_source': {
				'include': [
					'id',
					'topicName',
					'sortOrder'
				]
			},
			query: {
				match_all: {}
			}
        });
	}
	getDataResourcePurpose() {
        return this.http.post(this.esSearchUrl + '?type=dataresourcepurpose', {
			size: 500,
			'sort': [
				{
					'sortorder': 'asc'
				}
			],
			'_source': {
				'include': [
					'id',
					'purpose',
					'sortorder'
				]
			},
			query: {
				match_all: {}
			}
        });
	}
	getDataResourcePurposeType() {
        return this.http.post(this.esSearchUrl + '?type=dataresourcepurposetype', {
			size: 500,
			'sort': [
				{
					'sortOrder': 'asc'
				}
			],
			'_source': {
				'include': [
					'id',
					'purposeType',
					'sortOrder'
				]
			},
			query: {
				match_all: {}
			}
        });
    }
    getDataProviderTagUrl(): Observable<any[]> {
        return this.http.get<any[]>(this.dataProviderTagUrl);
	}
	getDataProviderTagUser(value:String):Observable<any>{
        return this.http.get(`${this.dataProviderTag}/${value}`);
	}

    getDataProviderPublicTagUrl(): Observable<any[]> {
        return this.http.get<any[]>(this.dataProviderPublicTag);
    }


     getPricingModel(query?: string) {
        const queryJSON = {
			'_source': {
				'include': [
					'id',
					'description',
					'recordCount'
				]
			},
			'sort': [
				{
					'recordCount': 'desc'
				}
			],
			'size': 200,
			'filter': {
				'query': {
					'match': {
						'lookupmodule': 'PRICING_MODEL'
					}
				}
			}
        };
        if (query) {
            queryJSON['query'] = {
                'prefix': {
                   'description': query
                }
            };
        } else {
            queryJSON['query'] = {
                'match_all': {}
            };
        }
         return this.http.post(this.esSearchUrl + '?type=lookupcode', queryJSON);
    }

    getWillingnessToProvideToInvestors() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source':
			{ 'include': [ 'id', 'description', 'recordCount', 'sortorder' ] }
			,
			'sort': [
			{ 'sortorder': 'asc' }
			],
			'size': 200,
			'filter': {
			'query': {
			'match':
			{ 'lookupmodule': 'INVESTOR_WILLINGNESS_SCORE' }
			}
			}
        });
    }

    getRelevantSectors() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source': {
				'include': [
					'id',
					'description',
					'recordCount',
					'sortorder'
				]
			},
			'sort': [
				{
					'sortorder': 'asc'
				}
			],
			'size': 200,
			'filter': {
				'query': {
					'match': {
						'lookupmodule': 'RLVNT_SECTOR'
					}
				}
			}
		});
    }
    getUniqueness() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source':
			{ 'include': [ 'id', 'description', 'recordCount', 'sortorder' ] }
			,
			'sort': [
			{ 'sortorder': 'asc' }
			],
			'size': 200,
			'filter': {
			'query': {
			'match':
			{ 'lookupmodule': 'UNIQUENESS_SCORE' }
			}
			}
		});
    }
    getValue() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source':
			{ 'include': [ 'id', 'description', 'recordCount', 'sortorder'] }
			,
			'sort': [
			{ 'sortorder': 'asc' }
			],
			'size': 200,
			'filter': {
			'query': {
			'match':
			{ 'lookupmodule': 'VALUE_SCORE' }
			}
			}
		});
    }
    getReadiness() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source':
			{ 'include': [ 'id', 'description', 'recordCount', 'sortorder' ] }
			,
			'sort': [
			{ 'sortorder': 'asc' }
			],
			'size': 200,
			'filter': {
			'query': {
			'match':
			{ 'lookupmodule': 'READINESS_SCORE' }
			}
			}
		});
    }
    getOverall() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
			'_source':
			{ 'include': [ 'id', 'description', 'recordCount', 'sortorder' ] }
			,
			'sort': [
			{ 'sortorder': 'asc' }
			],
			'size': 200,
			'filter': {
			'query': {
			'match':
			{ 'lookupmodule': 'OVERALL_SCORE' }
			}
			}
		});
    }
    getDataTopic(countType?: string) {
        countType = countType ? countType : 'articleCount';
        const sorting: any = {};
        sorting[countType] = 'desc';
        return this.http.post(this.esSearchUrl + '?type=datatopic', {
			size: 100,
			'_source': {
				'include': [
					'id', 'topicName', 'explanation', countType
				]
			},
			'sort': [
				sorting
			],
			query: {
				match_all: {}
			}
		});
    }
    getDataSourcePurpose() {
        return this.http.post(this.esSearchUrl + '?type=dataresourcepurpose', {
			size: 100,
			'_source': {
				'include': [
					'id', 'purpose', 'explanation', 'articleCount'
				]
			},
			'sort': [
				{
					'articleCount': 'desc'
				}
			],
			query: {
				match_all: {}
			}
		});
    }
    getDataSourcePurposeType() {
        return this.http.post(this.esSearchUrl + '?type=dataresourcepurposetype', {
			size: 100,
			'_source': {
				'include': [
					'id', 'purposeType', 'explanation', 'articleCount'
				]
			},
			'sort': [
				{
					'articleCount': 'desc'
				}
			],
			query: {
				match_all: {}
			}
		});
    }
    getDeliveryMethods() {
        return this.http.post(this.esSearchUrl + '?type=lookupcode', {
                           '_source': {
                             'include': [
                               'id',
                               'lookupcode',
							  'recordCount',
							  'sortorder'
                            ]
                           },
                          'sort': [
                             {
                               'sortorder': 'asc'
                             }
                           ],
                           'size': 200,
                           'filter': {
                             'query': {
                               'match': {
                                 'lookupmodule': 'DELIVERY_METHOD'
                              }
                             }
                           }
                         });
                   }
                   getDeliveryFormat() {
                       return this.http.post(this.esSearchUrl + '?type=lookupcode', {
                           '_source': {
                             'include': [
                               'id',
                               'lookupcode',
							   'recordCount',
							   'sortorder'
                             ]
                           },
                           'sort': [
                             {
                               'sortorder': 'asc'
                             }
                           ],
                           'size': 200,
                           'filter': {
                             'query': {
                               'match': {
                                 'lookupmodule': 'DELIVERY_FORMAT'
                               }
                             }
                           }
                         });
                   }
                   deliveryFrequency() {
                    return this.http.post(this.esSearchUrl + '?type=lookupcode', {
                        '_source': {
                          'include': [
                            'id',
                            'lookupcode',
							'recordCount',
							'sortorder'
                          ]
                        },
                        'sort': [
                          {
                            'sortorder': 'asc'
                          }
                        ],
                        'size': 200,
                        'filter': {
                          'query': {
                            'match': {
                              'lookupmodule': 'DELIVERY_FREQUENCY'
                            }
                          }
                        }
                      });
                }

    getDataUpdateFrequency(query?: string) {
        const queryJSON = {
			'_source': {
				'include': [
					'id',
					'lookupcode',
					'recordCount',
					'sortorder'
				]
			},
			'sort': [
				{
					'sortorder': 'asc'
				}
			],
			'size': 200,
			'filter': {
				'query': {
					'match': {
						'lookupmodule': 'DATA_UPDATE_FREQUENCY'
					}
				}
			}
		};
        if (query) {
            queryJSON['query'] = {
                'prefix': {
                    'lookupcode': query
                }
            }
        }
        return this.http.post(this.esSearchUrl + '?type=lookupcode', queryJSON);
    }

    getAllDataCategories(query?: string) {
         const queryJSON = {
            'size': 500
        };
        if (query) {
            queryJSON['query'] = {
                'prefix': {
                    'datacategory': query
                }
            }
        }
		return this.http.post('/api/es/categorysearch?type=datacategory', queryJSON);
	}
	getAllDataCategorie(query?: string) {
		 const queryJSON = {
            'size': 0,
            'aggs': {
                'datacategories': {
                    'terms':
                    {
                        'field': 'mainDataCategory.id',
                        size: 100
                    },
                    aggs: {
                        'mainDataCategory': {
                            top_hits: {
                                size: 1,
                                _source: {
                                    include: [
                                        'mainDataCategory.id',
                                        'mainDataCategory.datacategory',
                                        'mainDataCategory.explanation',
                                        'mainDataCategory.logo'
                                    ]
                                }
                            }
                        }
                    }
                }
            }
		};
		if (query) {
            queryJSON['query'] = {
                'prefix': {
                    'mainDataCategory.datacategory': query
                }
            }
        }
        return this.http.post(this.esSearchUrl + '?type=dataprovider', queryJSON);
	}

	getAllProviders(query?: string) {
        const queryJSON = {
			'_source': {
				'exclude': [
				]
			},
            'size': 50
        };
        if (query) {
            queryJSON['query'] = {
                'prefix': {
                    'providerName': query
                }
            }
		}
		return this.http.post('/api/es/providersearch', queryJSON);
	}

    getAllEmailAddres(query?: string) {
        const queryJSON = {
			'_source': {
				'exclude': [
				]
			},
            'size': 50
        };
        if (query) {
            queryJSON['query'] = {
                'prefix': {
                    'emailAddress': query
                }
            }
		}
		return this.http.post('/api/es/emailSearch', queryJSON);
	}

    getAllUsers(query?: string) {
        const queryJSON = {
			'_source': {
				'exclude': [
				]
			},
            'size': 50
        };
        if (query) {
            queryJSON['query'] = {
                'prefix': {
                    'email': query
                }
            }
		}
		return this.http.post('/api/es/userSearch', queryJSON);
	}

	getAllOrganizations(query?: string) {
        const queryJSON = {
			'_source': {
				'include': [
					'id',
					'name'
				]
			},
            'size': 50
        };
        if (query) {
            queryJSON['query'] = {
                'prefix': {
                    'name': query
                }
            }
		}
		return this.http.post('/api/es/organizationsearch', queryJSON);
    }
    getGeographicMultiselect(event, lookupmodule) {
        return this.http.post(this.newLookupCode + '?type=lookupcode', {
                'lookupmodule': lookupmodule,
                'lookupcode': event,
                'description': event

		});
    }

	getTypes() {
        return this.http.post(this.esSearchUrl + '?type=datainvestmentmanager', {
            'size': 0,
            'aggs': {
                'dataTypes': {
                    'terms':
                    { 'field': 'dataTypes.id', size: 100 },
                    aggs: {
                        'dataTypes': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['dataTypes.id', 'dataTypes.description', ] }
                            }
                        }
                    }
                }
            }
        });
    }
	getStrategies() {
        return this.http.post(this.esSearchUrl + '?type=datainvestmentmanager', {
            'size': 0,
            'aggs': {
                'investmentStrategies': {
                    'terms':
                    { 'field': 'investmentStrategies.id', size: 100 },
                    aggs: {
                        'investmentStrategies': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['investmentStrategies.id', 'investmentStrategies.description', ] }
                            }
                        }
                    }
                }
            }
        });
    }
	getResearchStyle() {
        return this.http.post(this.esSearchUrl + '?type=datainvestmentmanager', {
            'size': 0,
            'aggs': {
                'mainResearchStyle': {
                    'terms':
                    { 'field': 'mainResearchStyle.id', size: 100 },
                    aggs: {
                        'mainResearchStyle': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['mainResearchStyle.id', 'mainResearchStyle.description', ] }
                            }
                        }
                    }
                }
            }
        });
    }
	getAssetClass() {
        return this.http.post(this.esSearchUrl + '?type=datainvestmentmanager', {
            'size': 0,
            'aggs': {
                'assetClass': {
                    'terms':
                    { 'field': 'assetClass.id', size: 100 },
                    aggs: {
                        'assetClass': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['assetClass.id', 'assetClass.description', ] }
                            }
                        }
                    }
                }
            }
        });
    }
	getSector() {
        return this.http.post(this.esSearchUrl + '?type=datainvestmentmanager', {
            'size': 0,
            'aggs': {
                'sector': {
                    'terms':
                    { 'field': 'sector.id', size: 100 },
                    aggs: {
                        'sector': {
                            top_hits: {
                                size: 1,
                                _source:
                                { include: ['sector.id', 'sector.description', ] }
                            }
                        }
                    }
                }
            }
        });
    }

	getPrimaryType() {
        return this.http.post(this.esSearchUrl + '?type=datainvestmentmanager', {
			'size': 0,
			'aggs': {
				'primaryType': {
					'terms':
					{ 'field': 'primaryType.id', size: 100 },
					aggs: {
						'primaryType': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['primaryType.id', 'primaryType.description', ] }
							}
						}
					}
				}
			}
		});
    }
	getPrimarySubType() {
        return this.http.post(this.esSearchUrl + '?type=datainvestmentmanager', {
			'size': 0,
			'aggs': {
				'primarySubtype': {
					'terms':
					{ 'field': 'primarySubtype.id', size: 100 },
					aggs: {
						'primarySubtype': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['primarySubtype.id', 'primarySubtype.description', ] }
							}
						}
					}
				}
			}
		});
    }
	getTotalAum() {
        return this.http.post(this.esSearchUrl + '?type=datainvestmentmanager', {
			'size': 0,
			'aggs': {
				'totalAumRange': {
					'terms':
					{ 'field': 'totalAumRange.id', size: 100 },
					aggs: {
						'totalAumRange': {
							top_hits: {
								size: 1,
								_source:
								{ include: ['totalAumRange.id', 'totalAumRange.description', ] }
							}
						}
					}
				}
			}
		});
    }
}
