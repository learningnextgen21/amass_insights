import { Component, OnInit, Input, OnChanges, Output, EventEmitter, ViewChild, ViewChildren, ElementRef, SimpleChanges, AfterViewInit, Inject } from '@angular/core';
import { GenericFilterModel } from '.';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import * as _ from 'lodash';
import { CookieService } from 'ngx-cookie';
import { Principal } from '../auth/principal.service';
import { DatePipe } from '@angular/common';
import { retry, retryWhen } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { AmassSettingService } from '../../account/settings/settings.service';
import { DataProviderFilterService } from '../../entities/data-provider/data-provider-filter.service';
@Component({
	selector: 'jhi-generic-filter',
	templateUrl: 'generic-filter.component.html',
	styleUrls: [
		'./generic-filter.component.scss'
    ]
})
export class GenericFilterComponent implements OnInit, OnChanges, AfterViewInit {
	@Input()
	filterType: any;
	@Input()
	filterList: GenericFilterModel[];
    @Output() onFilterChanges = new EventEmitter();
    @Output() defaultFilter = new EventEmitter();
    @Output() onFilterCheck = new EventEmitter();
	@Input() filterCheckBox: any;
	@Input()
    isAdmin: boolean;
    disableMoreBtn: boolean = false;
    filterCheck: boolean;
    /* @Input()
    corona: boolean; */
	public selectedFilters: any;
	public selectedRadio: any = {};
    public selectedFilterData: any;
    private filterSelected = new Subject();
	public orderProp: string;
	public reverse: boolean;
	public selectedRangeSlider: any;
	public showMore: boolean;
	public action: any;
	public lesMore: any;
	public lesMoreAction: any;

	private queryParams = {};
	public paramFilter: string;
	public paramID: string;
	public paramType: string;
	public paramCreatedDate: any;
	public paramPublishedDate: any;
	public paramUpdatedDate: any;
	public paramFollow: boolean;
	public sliderInit: boolean;
    public hiddenFilterCount: number;
    cookieStatus: any;
	genericFilterLabel: any;
    filterLabel: any;
    cookiejson: any;
    currentPath: string;
    checkedStatus: boolean;
    checkedCategory: any;
    defaultItem: boolean;
    enableDefaultItem: boolean;
    public subject = new Subject();
    isSortByBarCollapsed: boolean;
    userLevel: boolean;

	@ViewChildren('rangeSliderRef') rangeSliderRef;

    public matTooltipShowDelay: number;
	constructor(
		public googleAnalyticsService: GoogleAnalyticsEventsService,
		private router: Router,
        private activatedRoute: ActivatedRoute,
        private cookieService: CookieService,
        private principal: Principal,
        private amassSettingService: AmassSettingService,
        private datePipe: DatePipe,
		private dataProviderfilterService:DataProviderFilterService,
	) {
		this.orderProp = 'order';
		this.reverse = true;
		this.matTooltipShowDelay = 500;
		router.events.subscribe(event => {
            const url = event['url'];
            this.currentPath = url ? url.replace('/', '').split('?')[0] : '';
			if (event instanceof NavigationStart) {
                this.currentPath = event.url.replace('/', '').split('?')[0];
				this.getCheckedFilters();
			}
        });
        this.isSortByBarCollapsed = true;
    }

	ngOnInit() {
        this.dataProviderfilterService.clearFilterSearch$.subscribe(event =>{
			console.log('event',event);
			if(event)
			{
                this.clearFilters();
			}
        });

        this.cookieStatus =  this.cookieService.get('cookieconsent_status');
        this.filterSelected.subscribe(data => {
            console.log('filter initi...', data);
        });

        this.router.events.subscribe(event => {
            const url = event['url'];
            this.currentPath = url ? url.replace('/', '').split('?')[0] : '';
			if (event instanceof NavigationStart) {
                this.currentPath = event.url.replace('/', '').split('?')[0];
				// this.getCheckedFilters();
			}
        });
        this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
        //this.userLevel = this.principal.hasAnyPermissionAboveDirect(200);
        this.amassSettingService.userauthorities().subscribe(data => {
			//this.providerUserPermission = data;
			console.log('dataauthoritiesgenric',data);
            this.userLevel = data;
        });
        this.defaultItem = true;
        this.selectedRadio = {};
		this.selectedFilters = [];
		this.selectedFilterData = [];
		this.showMore = false;
		this.sliderInit = true;
		// console.log(this.selectedFilterData);
		this.paramCreatedDate = null;
		this.paramUpdatedDate = null;
        this.paramFollow = false;
        if(this.router.url === '/investors') {
            this.disableMoreBtn = true;
        }
        this.activatedRoute.queryParams.subscribe(params => {
            this.queryParams = params;
            const url = this.router.url;
            this.currentPath = url ? url.replace('/', '').split('?')[0] : '';
            // console.log('current path...', this.currentPath);
            if (params['utm_medium'] === 'email' || params['filter1']) {
                this.cookieService.remove('filterValues_' + this.currentPath);
            }
            let i = 1;
            for (const obj in params) {
                console.log(obj);
                if (obj) {
                    if (obj.indexOf('filter') !== -1) {
                        this.onQueryParamsFilter(params['value' + i], params['filter' + i], params['type' + i], i);
                        this.getCheckedFiltersPromise(i);
                        this.expandFilters(params['filter' + i]);
                        this.filterURLParams();
                        i++;
                    }
                }
            }

			if (params['filter'] && params['id'] && params['type']) {
				this.reset();
				this.paramID = params['id'];
				this.paramFilter = params['filter'];
				this.paramType = params['type'];
				this.expandFilters();
				this.onFilter(this.paramID, this.paramFilter, null, this.paramType);
			}
			if (params['cd']) {
				this.reset();
                let filterIndex = -1;
                this.paramCreatedDate = new Date(parseInt(params['cd'], 10));
                const minDate = new Date(parseInt(params['cd'], 10));
                console.log(params['cd']);
                console.log(minDate);
				this.paramUpdatedDate = null;
				this.selectedFilterData.filter((obj, pos, arr) => {
					if (obj.queryTerm === 'updatedDate') {
						filterIndex = pos;
					}
                });
                this.filterList.filter((obj, pos, arr) => {

                    if (obj.filterTerm === 'createdDate') {
                        console.log(obj);
                        obj.filterNgModel = [minDate, new Date()];
                        obj.minRange = minDate;
                        obj.maxRange = new Date();
                    }
                });
				if (filterIndex > -1) {
					this.selectedFilterData.splice(filterIndex, 1);
				}

				this.constructRangeFilter('datepicker', 'createdDate', [this.paramCreatedDate, null]);
			}

			if (params['pd']) {
				this.reset();
				let filterIndex = -1;
				this.selectedFilterData.filter((obj, pos, arr) => {
					if (obj.queryTerm === 'updatedDate') {
						filterIndex = pos;
					}
				});
				if (filterIndex > -1) {
					this.selectedFilterData.splice(filterIndex, 1);
				}
				this.paramPublishedDate = params['pd'];
				this.paramCreatedDate = null;
				this.paramUpdatedDate = null;
				this.constructRangeFilter('datepicker', 'publishedDate', [this.paramPublishedDate, null]);
			}

			if (params['ud']) {
				this.reset();
				if (params['f']) {
					this.paramFollow = true;
					this.paramFilter = 'marketplaceStatus';
					this.paramType = 'radio';
					this.paramID = 'Followed';
				}
				let filterIndex = -1;
				this.selectedFilterData.filter((obj, pos, arr) => {
					if (obj.queryTerm === 'createdDate') {
						filterIndex = pos;
					}
				});
				if (filterIndex > -1) {
					this.selectedFilterData.splice(filterIndex, 1);
				}
                this.paramUpdatedDate = new Date(parseInt(params['ud'], 10));
                const minDate = new Date(parseInt(params['ud'], 10));
                this.paramCreatedDate = null;
                this.filterList.filter((obj, pos, arr) => {

                    if (obj.filterTerm === 'updatedDate') {
                        console.log(obj);
                        obj.filterNgModel = [minDate, new Date()];
                        obj.minRange = minDate;
                        obj.maxRange = new Date();
                    }
                });
				this.constructRangeFilter('datepicker', 'updatedDate', [this.paramUpdatedDate, null]);
			}
		});
        this.cookiejson = this.cookieService.get('filterValues_' + this.currentPath) ? JSON.parse(this.cookieService.get('filterValues_' + this.currentPath)) : null;
		this.hiddenFilterCount = this.filterList.reduce(function(n, obj, index) {
			if (obj.hidden) {
				n = n + 1;
			}
			return n;
        }, 0);
        if (this.cookiejson && this.cookiejson.length && this.isAuthenticated()) {
            this.selectedFilterData = this.cookiejson;
            this.onFilterChanges.emit({
				selectedFilterData: this.cookiejson
            });
            this.setFiltersState(this.cookiejson);
        }
        if (this.isAuthenticated()) {
            this.enableDefaultItem = true;
        }
    }

	ngOnChanges(changes: SimpleChanges) {
		console.log(changes);
		// console.log('changes...', this.filterList);
		console.log(this.selectedFilterData);
		this.sort('order');
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

	sort(property) {
		this.orderProp = property;
		const direction = this.reverse ? 1 : -1;

		this.filterList.sort(function(a, b) {
			if (a[property] < b[property]) {
				return -1 * direction;
			} else if ( a[property] > b[property]) {
				return 1 * direction;
			} else {
				return 0;
			}
		});
	}

	constructRangeFilter(type, term, value) {
		this.selectedFilters[term] = [];
		this.selectedFilters[term].push(value);
		const filterDataIndex = this.selectedFilterData.filter(item => {
			return item.queryTerm === term;
		});
		// console.log('index...', filterDataIndex);
		if (filterDataIndex) {
			this.selectedFilterData.splice(filterDataIndex, 1);
		}

		if (this.paramFollow) {
			let filterIndex = -1;
			this.selectedFilterData.filter((obj, pos, arr) => {
				if (obj.queryTerm === 'marketplaceStatus') {
					filterIndex = pos;
				}
			});
			if (filterIndex > -1) {
				this.selectedFilterData.splice(filterIndex, 1);
			}
			this.selectedFilterData.push({
				queryType: 'should',
				queryTerm: 'marketplaceStatus',
				filters: 'Followed'
			});
		}

		this.selectedFilterData.push({
			queryType: type,
			queryTerm: term,
			filters: this.selectedFilters[term]
		});
		// console.log(this.selectedFilterData);
		this.getCheckedFilters();

		this.emitFilterEvent();
    }

    datepickerValueChanged(values, term, type, min,  max, filterIndex) {
        if (!this.isAuthenticated()) {
            this.clearFilters();
            return false;
        } else {
            if ((values[0] === null && values[1] === null) || (values[0] === null && values[1] === '') || (values[1] === null && values[0] === '')) {
                this.clearFilters();
            } else if (values.length) {
                    values[0] = this.datePipe.transform(values[0], 'yyyy-MM-dd');
                    values[1] = this.datePipe.transform(values[1], 'yyyy-MM-dd');
                    this.selectedFilters[term] = [];
                    this.selectedFilters[term].push(values);
                    let filterDataIndex  = -1;
                    this.selectedFilterData.filter((item, pos) => {
                        if (item.queryTerm === term) {
                            filterDataIndex = pos;
                        }
                    });
                    // console.log('index...', filterDataIndex);
                    if (filterDataIndex > -1) {
                        this.selectedFilterData.splice(filterDataIndex, 1);
                    }

                    if (!(values[0] === min && values[1] === max)) {
                        this.selectedFilterData.push({
                            queryType: type,
                            queryTerm: term,
                            filters: this.selectedFilters[term]
                        });
                    } else {
                        this.selectedFilterData.splice(filterDataIndex, 1);
                    }

                    this.emitFilterEvent();

                    this.filterList[filterIndex].minRange = values[0];
                    this.filterList[filterIndex].maxRange = values[1];
                }
           }
	    }

	sliderValueChanged(event, term, handle, type, min,  max, elementID, filterIndex) {
		// console.log('sliderValueChanged...', event, handle);
        // console.log('sd', this.selectedFilterData);
        if (!this.isAuthenticated()) {
            // this.applyDialogService.open();
            this.clearFilters();
            return false;
        } else {
            if (event.length) {
                this.selectedFilters[term] = [];
                this.selectedFilters[term].push(event);
                let filterDataIndex  = -1;
                this.selectedFilterData.filter((item, pos) => {
                    if (item.queryTerm === term) {
                        filterDataIndex = pos;
                    }
                });
                // console.log('index...', filterDataIndex);
                if (filterDataIndex > -1) {
                    this.selectedFilterData.splice(filterDataIndex, 1);
                }

                if (!(event[0] === min && event[1] === max)) {
                    this.selectedFilterData.push({
                        queryType: type,
                        queryTerm: term,
                        filters: this.selectedFilters[term]
                    });
                } else {
                    this.selectedFilterData.splice(filterDataIndex, 1);
                }

                this.emitFilterEvent();

                this.filterList[filterIndex].minRange = event[0];
                this.filterList[filterIndex].maxRange = event[1];

            }

            if (event.type === 'change') {
                this.rangeSliderRef.forEach(rangeSliderRef => {
                    if (elementID === rangeSliderRef.el.nativeElement.id) {
                        const values = rangeSliderRef.slider.get();
                        const value = Number(values[handle]);
                        this.setSliderHandle(handle, event.target.value, elementID, term, type);
                    }
                });

            }
       }
	}

	setSliderHandle(i, value, elementID, term, type) {
		const r = [null, null];
		r[i] = value;
		const _that = this;
		this.rangeSliderRef.filter(item => {
			if (item.el.nativeElement.id === elementID) {
				console.log(item.el.nativeElement.id);
				item.slider.set(r);
				console.log(item.slider);
				setTimeout(function() {
					_that.constructSliderRequest(item, term, type);
				}, 500);
			}
		});
	}

	constructSliderRequest(slider, term, type) {
		// console.log(slider);
		// console.log(this.selectedFilterData);
		this.selectedFilters[term] = [];
		this.selectedFilters[term].push(slider.ngModel);
		let filterDataIndex  = -1;
		this.selectedFilterData.filter((item, pos) => {
			if (item.queryTerm === term) {
				filterDataIndex = pos;
			}
		});
		// console.log('index...', filterDataIndex);
		if (filterDataIndex > -1) {
			this.selectedFilterData.splice(filterDataIndex, 1);
		}

		// console.log(slider.ngModel[0], slider.min, slider.ngModel[1], slider.max, !(slider.ngModel[0] === slider.min && slider.ngModel[1] === slider.max));

		if (!(slider.ngModel[0] === slider.min && slider.ngModel[1] === slider.max)) {
			this.selectedFilterData.push({
				queryType: type,
				queryTerm: term,
				filters: this.selectedFilters[term]
			});
		} else if (filterDataIndex > -1) {
			this.selectedFilterData.splice(filterDataIndex, 1);
		}

		this.emitFilterEvent();
    }

    onQueryParamsFilter(value, term, filterType, paramIndex) {
        console.log('Counting filter params...', paramIndex, term, filterType, paramIndex);
        let queryType = '';
        switch (filterType) {
            case 'checkbox':
            case 'horizontal-checkbox':
            this.selectedFilters[term] = this.selectedFilters[term] ? this.selectedFilters[term] : [];
            queryType = 'must';
            break;
            case 'radio':
            this.selectedFilters[term] = this.selectedFilters[term] ? this.selectedFilters[term] : '';
            queryType = 'should';
            break;
            case 'range':
            case 'rangeFromTo':
            case 'slide-toggle':
            queryType = 'range';
            break;
            case 'datepicker':
            case 'datepickerSingle':
            queryType = 'datepicker';
            break;
            case 'rangeBetween':
            queryType = 'rangeBetween';
            break;
        }

        let filterDataIndex = -1;
        if (!this.selectedFilterData) {
            this.selectedFilterData = [];
        }
        this.selectedFilterData.filter((obj, pos, arr) => {
            if (obj.queryTerm === term) {
                filterDataIndex = pos;
            }
        });
        if (this.queryParams['filter' + paramIndex] && this.queryParams['value' + paramIndex] && this.queryParams['type' + paramIndex]) {
            if (typeof(this.selectedFilters[term]) === 'object' && typeof(this.selectedFilters[term]) !== 'string') {
                this.selectedFilters[term].push(value);
            } else {
                this.selectedFilters[term] = value;
            }
            this.selectedFilterData.push({
                'queryType': queryType,
                queryTerm: term,
                filters: this.selectedFilters[term]
            });
        }

        const filtered =  _.uniqWith(this.selectedFilterData, _.isEqual);
        this.selectedFilterData = filtered;
    }

    onFilterPromise(id, term, event?, filterType?, paramIndex?: number): Observable<any> {
        return Observable.create(observer => {
            if (!id || !term) {
                // observer.error('argument missing');
                throw new Error('argument missing');
            }
            if (!this.isAuthenticated) {
                if (event) { event.checked = false; }
                if (event && event.source) { event.source._checked = false; }
                this.clearFilters();
                // observer.error('unrestricted');
                throw new Error('unrestricted');
            } else {
                let queryType = '';
                switch (filterType) {
                    case 'checkbox':
                    case 'horizontal-checkbox':
                    this.selectedFilters[term] = this.selectedFilters[term] ? this.selectedFilters[term] : [];
                    queryType = 'must';
                    break;
                    case 'radio':
                    this.selectedFilters[term] = this.selectedFilters[term] ? this.selectedFilters[term] : '';
                    queryType = 'should';
                    break;
                    case 'range':
                    case 'rangeFromTo':
                    case 'slide-toggle':
                    queryType = 'range';
                    break;
                    case 'datepicker':
                    case 'datepickerSingle':
                    queryType = 'datepicker';
                    break;
                    case 'rangeBetween':
                    queryType = 'rangeBetween';
                    break;
                }

                const index = (event && event.source && event.source.value) ? this.selectedFilters[term].indexOf(parseInt(event.source.value, 10)) : -1;
                let filterDataIndex = -1;
                if (!this.selectedFilterData) {
                    this.selectedFilterData = [];
                }
                this.selectedFilterData.filter((obj, pos, arr) => {
                    if (obj.queryTerm === term) {
                        filterDataIndex = pos;
                    }
                });

                if (index > -1 || filterDataIndex > -1 || (event && event.source && !event.source.checked)) {
                    if (index > -1) {
                        if (this.paramFilter) {
                            this.selectedFilters[term] = [];
                            this.reset();
                        } else {
                            this.selectedFilters[term].splice(index, 1);
                        }
                    }
                    if (!this.selectedFilters[term].length) {
                        this.selectedFilterData.splice(filterDataIndex, 1);
                    }
                }
                if ((this.queryParams['filter' + paramIndex] && this.queryParams['value' + paramIndex] && this.queryParams['type' + paramIndex])
                || this.paramCreatedDate || this.paramUpdatedDate || (event && event.source && event.source.checked)) {
                    if (typeof(this.selectedFilters[term]) === 'object' && typeof(this.selectedFilters[term]) !== 'string') {
                        this.selectedFilters[term].push(id);
                    } else {
                        this.selectedFilters[term] = id;
                    }
                    this.selectedFilterData.push({
                        'queryType': queryType,
                        queryTerm: term,
                        filters: this.selectedFilters[term]
                    });
                }

                const filtered =  _.uniqWith(this.selectedFilterData, _.isEqual);
                this.selectedFilterData = filtered;
                this.getCheckedFiltersPromise(paramIndex);
                observer.next(this.selectedFilterData);
            }
            observer.complete();
        })/* .retryWhen(errors => {
            console.log('filter retrying....', errors);
            errors.delay(3000).take(2);
        }) */;
    }

	onFilter(id, term, event?, filterType?) {
        if (!this.isAuthenticated()) {
            // this.applyDialogService.open();
           // console.log(event.source._checked);
            event.checked = false;
            event.source._checked = false;
            this.clearFilters();
            return false;
        } else {
            if (event && event.checked === false) {
                event.checked = false;
                event.source._checked = false;
               // this.clearFilters();
               }
            // console.log(type, id, term, event, filterType);
            let queryType = '';
            switch (filterType) {
                case 'checkbox':
                case 'horizontal-checkbox':
                this.selectedFilters[term] = this.selectedFilters[term] ? this.selectedFilters[term] : [];
                queryType = 'must';
                break;
                case 'radio':
                this.selectedFilters[term] = this.selectedFilters[term] ? this.selectedFilters[term] : '';
                queryType = 'should';
                break;
                case 'range':
                case 'rangeFromTo':
                case 'slide-toggle':
                queryType = 'range';
                break;
                case 'datepicker':
                case 'datepickerSingle':
                queryType = 'datepicker';
                break;
                case 'rangeBetween':
                queryType = 'rangeBetween';
                break;
            }
            // console.log(typeof(this.selectedFilters[term]));
            // this.selectedFilters[term] = this.selectedFilters[term] ? this.selectedFilters[term] : [];
            const index = (event && event.source && event.source.value) ? this.selectedFilters[term].indexOf(parseInt(event.source.value, 10)) : -1;
            let filterDataIndex = -1;
            if (!this.selectedFilterData) {
                this.selectedFilterData = [];
            }
            this.selectedFilterData.filter((obj, pos, arr) => {
                // console.log(pos, obj, arr);
                if (obj.queryTerm === term) {
                    filterDataIndex = pos;
                }
            });

            if (index > -1 || filterDataIndex > -1 || (event && event.source && !event.source.checked)) {
                if (index > -1) {
                    if (this.paramFilter) {
                        this.selectedFilters[term] = [];
                        this.reset();
                    } else {
                        this.selectedFilters[term].splice(index, 1);
                    }
                }
                if (!this.selectedFilters[term].length) {
                    this.selectedFilterData.splice(filterDataIndex, 1);
                }
            }
            if ((this.paramID && this.paramFilter && this.paramType) || this.paramCreatedDate || this.paramUpdatedDate || (event && event.source && event.source.checked)) {
                if (typeof(this.selectedFilters[term]) === 'object' && typeof(this.selectedFilters[term]) !== 'string') {
                    this.selectedFilters[term].push(id);
                } else {
                    this.selectedFilters[term] = id;
                }
                this.selectedFilterData.push({
                    'queryType': queryType,
                    queryTerm: term,
                    filters: this.selectedFilters[term]
                });
            }
            // console.log(this.selectedFilterData);
            // console.log(term, index, filterDataIndex);

            const filtered =  _.uniqWith(this.selectedFilterData, _.isEqual);
            this.selectedFilterData = filtered;

            // console.log(filtered);
            this.getCheckedFilters();
            // console.log(this.selectedFilterData);
            this.emitFilterEvent();
        }
    }
    defaultFilterChange(event) {
        console.log(event);
        event.source._checked = this.defaultItem;
        const eventsValue = event.source._checked;
            this.defaultFilter.emit(eventsValue);
    }
	filterChecked(events: any) {
        console.log(events);
        if (this.principal.isAuthenticated()) {
            if (this.filterCheckBox === 'Providers') {
                this.checkedCategory = 'Providers';
                this.genericFilterLabel = 'Providers List - Filter' + ' - ' + events.label;
            } else if (this.filterCheckBox === 'Articles') {
                this.checkedCategory = 'Articles';
                this.genericFilterLabel = 'Articles List - Filter' + ' - ' + events.label;
            } else if (this.filterCheckBox === 'Categories') {
                this.checkedCategory = 'Categories';
                this.genericFilterLabel = 'Data Categories - Filter' + ' - ' + events.label;
             } else {
                this.checkedCategory = 'Events';
                this.genericFilterLabel = 'Events List  - Filter' + ' - ' + events.label;
             }
            if (events.event.source._checked === true) {
            this.action = 'Filter Checked';
            } else {
            this.action = 'Filter Unchecked';
            this.cookieService.remove('filterValues_' + this.currentPath);
                if (events.event.source._checked === false) {
                   // this.clearFilters();
                    events.event.source._checked = false;
                    events.event.checked = false;
                }
            }
            this.googleAnalyticsService.emitEvent(this.checkedCategory, this.action, this.genericFilterLabel);
            console.log(this.checkedCategory);
        }
        if (!this.principal.isAuthenticated()) {
            if (this.filterCheckBox === 'Articles') {
                this.googleAnalyticsService.emitEvent('Articles', 'Restricted Internal Link Clicked', 'Articles List - Filter ' + '-' + events.label);
                console.log(events.label);
            }

        }
    }
    datePicker(value) {
        console.log(value);
    }
	filterRange(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    filterSlider(events: any) {
        console.log(events);
        if (this.filterCheckBox === 'Providers') {
            this.genericFilterLabel = 'Providers List - Filter' + ' - ' + events.label;
            this.action = 'Slider Changed';
            this.googleAnalyticsService.emitEvent(this.filterCheckBox, this.action, this.genericFilterLabel);
        } else if (this.filterCheckBox === 'Articles') {
           this.genericFilterLabel = 'Articles List - Article' + ' - ' + events.label;
           this.action = 'Slider Changed';
           this.googleAnalyticsService.emitEvent(this.filterCheckBox, this.action, this.genericFilterLabel);
        } else if (this.filterCheckBox === 'Categories') {
           this.genericFilterLabel = 'Data Categories - Filter' + ' - ' + events.label;
           this.action = 'Slider Changed';
           this.googleAnalyticsService.emitEvent(this.filterCheckBox, this.action, this.genericFilterLabel);
        } else {
           this.genericFilterLabel = 'Events List  - Filter' + ' - ' + events.label;
           this.action = 'Slider Changed';
           this.filterCheckBox = 'Events';
           this.googleAnalyticsService.emitEvent(this.filterCheckBox, this.action, this.genericFilterLabel);
           console.log(this.filterCheckBox);
        }
    }
    filterGAEvents(event, state) {
            if (this.filterCheckBox === 'Providers') {
                this.genericFilterLabel = event.filterName + ' - ' + state;
                this.action = 'Filter' + ' ' +  state;
                this.googleAnalyticsService.emitEvent(this.filterCheckBox, this.action, this.genericFilterLabel);
            } else if (this.filterCheckBox === 'Articles') {
               this.genericFilterLabel = 'Articles List - Filter' + ' - ' + event.filterName;
               this.action = 'Filter' + ' ' + state;
               this.googleAnalyticsService.emitEvent(this.filterCheckBox, this.action, this.genericFilterLabel);
            } else if (this.filterCheckBox === 'Categories') {
               this.genericFilterLabel = event.filterName + ' - ' + state;
               this.action = 'Filter' + ' ' +  state;
               this.googleAnalyticsService.emitEvent(this.filterCheckBox, this.action, this.genericFilterLabel);
            } else {
               this.genericFilterLabel = event.filterName + ' - ' + state;
               this.action = 'Filter' + ' ' +  state;
               this.filterCheckBox = 'Events';
               this.googleAnalyticsService.emitEvent(this.filterCheckBox, this.action, this.genericFilterLabel);
            }
    }
    onCollapse(filters) {
        this.filterGAEvents(filters, 'Contracted');
    }

    onExpand(filters) {
        this.filterGAEvents(filters, 'Expanded');
    }
    filterAuthenticate()
    {
        this.filterCheck = true;
        this.onFilterCheck.emit(this.filterCheck);
    }
	emitFilterEvent() {
        this.filterSelected.subscribe(data => {
            if (data) {
                this.onFilterChanges.emit({
                    selectedFilterData: data
                });
                // if (this.cookieStatus !== 'deny') {
                    this.cookieService.put('filterValues_' + this.currentPath, JSON.stringify(this.selectedFilterData));
                    this.cookiejson = this.cookieService.get('filterValues_' + this.currentPath) ? JSON.parse(this.cookieService.get('filterValues_' + this.currentPath)) : null;
                // }
            }
        });

		if (this.selectedFilterData.length) {
            this.filterList.forEach(item => {
                item.expanded = false;
               // item.hidden = false;
            });
            this.onFilterChanges.emit({
                selectedFilterData: this.selectedFilterData
            });
            console.log(this.selectedFilterData);
            console.log(this.currentPath);
            // if (this.cookieStatus !== 'deny') {
                this.cookieService.put('filterValues_' + this.currentPath, JSON.stringify(this.selectedFilterData));
                this.cookiejson = this.cookieService.get('filterValues_' + this.currentPath) ? JSON.parse(this.cookieService.get('filterValues_' + this.currentPath)) : null;
                this.selectedFilterData.forEach(item => {
                    this.expandFilters(item.queryTerm);
                });
            // }
		} else {
			this.onFilterChanges.emit('clearFilter');
        }
	}

	clearFilters() {
        this.defaultItem = false;
		if (this.filterCheckBox === 'Providers') {
            this.checkedCategory = 'Providers';
			 this.filterLabel = 'Providers List - Filter - Clear';
		} else if (this.filterCheckBox === 'Articles') {
            this.checkedCategory = 'Articles';
			this.filterLabel = 'Articles List - Filter - Clear';
		} else if (this.filterCheckBox === 'Categories') {
            this.checkedCategory = 'Categories';
			this.filterLabel = 'Data Categories - Filter - Clear';
		} else {
            this.checkedCategory = 'Events';
            this.filterLabel = 'Events List - Filter - Clear';
        }
		this.googleAnalyticsService.emitEvent(this.checkedCategory, 'All Filters Cleared', this.filterLabel);
		this.selectedFilterData = [];
        this.selectedFilters = [];
        this.cookieService.remove('filterValues_' + this.currentPath);
		this.reset();
		let filterType = null;
		// console.log(this.filterList);
		this.filterList.map((value, index) => {
			filterType = this.filterList[index].filterType;
			if (filterType !== 'range' && filterType !== 'rangeBetween' && filterType !== 'rangeFromTo' && filterType !== 'datepicker' && filterType !== 'datepickerSingle') {
				this.filterList[index].filterList.filter((item, idx) => {
					this.filterList[index].filterList[idx].checked = false;
				});
				if (filterType === 'radio') {
					this.selectedRadio[this.filterList[index].filterName.toLocaleLowerCase()] = '';
				}
			}

			if (filterType === 'range' || filterType === 'rangeBetween' || filterType === 'rangeFromTo' || filterType === 'datepicker' || filterType !== 'datepickerSingle') {
				const min = this.filterList[index].minRangeInit;
				const max = this.filterList[index].maxRangeInit;
				this.filterList[index].filterNgModel = [min, max];
				this.filterList[index].minRange = min;
				this.filterList[index].maxRange = max;
			}
		});
		// console.log('clear', this.filterList);
		this.emitFilterEvent();
	}

	showMoreFilters() {
		this.filterList.map((item, index) => {
			if (item.hidden !== undefined) {
                item.hidden = this.showMore;
                console.log('....more filters...', item.hidden)
			}
		});
		this.showMore = !this.showMore;
		if (this.showMore === true) {
			// this.lesMore = 'Providers List - Filter - More';
			if (this.filterCheckBox === 'Providers') {
                this.checkedCategory = 'Providers';
				this.filterLabel = 'Providers List - Filter - More';
		   } else if (this.filterCheckBox === 'Articles') {
            this.checkedCategory = 'Articles';
			   this.filterLabel = 'Articles List - Filter - More';
		   } else if (this.filterCheckBox === 'Categories')  {
            this.checkedCategory = 'Categories';
			   this.filterLabel = 'Data Categories - Filter - More';
		   } else {
            this.checkedCategory = 'Events';
            this.filterLabel = 'Events List - Filter - More';
           }
			this.lesMoreAction = 'Less Filters Viewed';
		} else {
		// this.lesMore = 'Providers List - Filter - Less';
			if (this.filterCheckBox === 'Providers') {
                this.checkedCategory = 'Providers';
				this.filterLabel = 'Providers List - Filter - Less';
			} else if (this.filterCheckBox === 'Articles') {
                this.checkedCategory = 'Articles';
				this.filterLabel = 'Articles List - Filter - Less';
			} else if (this.filterCheckBox === 'Categories')  {
                this.checkedCategory = 'Categories';
				this.filterLabel = 'Data Categories - Filter - Less';
			} else {
                this.checkedCategory = 'Events';
                this.filterLabel = 'Events List - Filter - Less';
               }
			this.lesMoreAction = 'More Filters Viewed';
		}
        this.googleAnalyticsService.emitEvent(this.checkedCategory, this.lesMoreAction, this.filterLabel);
        console.log(this.checkedCategory);
	}

	reset() {
		this.paramCreatedDate = null;
		this.paramPublishedDate = null;
		this.paramUpdatedDate = null;
		this.paramFilter = null;
		this.paramFollow = false;
		this.paramID = null;
		this.paramType = null;
    }

    getCheckedFiltersPromise(paramIndex: number) {
		const _that = this;
		const elementID = _that.queryParams['filter' + paramIndex] ? _that.queryParams['filter' + paramIndex].replace('.', '_') : null;
		this.filterList.filter((item, index) => {
			if (item.filterTerm === this.queryParams['filter' + paramIndex]) {
				const paramID = this.queryParams['value' + paramIndex];
                setTimeout(function() {
					if (item.filterType === 'slide-toggle') {
						item.filterList.filter((itm, idx) => {
							item.filterList[0].checked = paramID === 'checked' ? true : false;
						});
					}
				}, 3000);
				setTimeout(function() {
					if (item.filterType === 'checkbox' || item.filterType === 'horizontal-checkbox') {
						item.filterList.filter((itm, idx) => {
							if (itm && itm.id === parseInt(paramID, 10)) {
								item.filterList[idx].checked = true;
							}
						});
					}
				}, 5000);
				setTimeout(function() {
					if (item.filterType === 'radio') {
						item.filterList.filter((itm, idx) => {
							if (itm && itm.value === paramID) {
								item.filterList[idx].checked = true;
							}
						});
					}
				}, 5000);
				setTimeout(function() {
					if (item.filterType === 'range' || item.filterType === 'rangeFromTo' || item.filterType === 'rangeBetween' ) {
						item.filterNgModel[0] = parseInt(paramID, 10);
						item.minRange = parseInt(paramID, 10);
						_that.rangeSliderRef._results.forEach(itm => {
							if (itm.el.nativeElement.id === elementID) {
								itm.slider.set([item.minRange, item.maxRange]);
							}
						});
					}
                }, 3000);
                setTimeout(function() {
					if (item.filterType === 'datepicker') {
                        item.filterNgModel = [new Date(parseInt(paramID)), null];
                        item.minRange = new Date(parseInt(paramID));
                        // item.maxRange = paramID[1];
                    }
				}, 3000);
			}
		});
	}

	getCheckedFilters() {
		const _that = this;
		const elementID = _that.paramFilter ? _that.paramFilter.replace('.', '_') : null;
		this.filterList.filter((item, index) => {
			if (item.filterTerm === this.paramFilter) {
				const paramID = this.paramID;
                // console.log(paramID);
                setTimeout(function() {
					if (item.filterType === 'slide-toggle') {
						item.filterList.filter((itm, idx) => {
							item.filterList[0].checked = paramID === 'checked' ? true : false;
						});
					}
				}, 3000);
				setTimeout(function() {
					if (item.filterType === 'checkbox' || item.filterType === 'horizontal-checkbox') {
						item.filterList.filter((itm, idx) => {
							if (itm && itm.id === parseInt(paramID, 10)) {
								item.filterList[idx].checked = true;
							}
						});
					}
				}, 3000);
				setTimeout(function() {
					if (item.filterType === 'radio') {
						item.filterList.filter((itm, idx) => {
							// console.log(itm, itm.value, itm.value === paramID);
							if (itm && itm.value === paramID) {
								item.filterList[idx].checked = true;
							}
						});
					}
				}, 5000);
				setTimeout(function() {
					if (item.filterType === 'range' || item.filterType === 'rangeFromTo' || item.filterType === 'rangeBetween' ) {
						item.filterNgModel[0] = parseInt(paramID, 10);
						item.minRange = parseInt(paramID, 10);
						console.log('inside loop....', _that.rangeSliderRef);
						_that.rangeSliderRef._results.forEach(itm => {
							if (itm.el.nativeElement.id === elementID) {
								itm.slider.set([item.minRange, item.maxRange]);
							}
						});
					}
                }, 3000);
                setTimeout(function() {
					if (item.filterType === 'datepicker') {
                        console.log('date,,,,,,,,filter.........');
                        item.filterNgModel = paramID;
                        item.minRange = paramID[0];
                         item.maxRange = paramID[1];
						/* item.minRange = parseInt(paramID, 10);
						console.log('inside loop....', _that.rangeSliderRef);
						_that.rangeSliderRef._results.forEach(itm => {
							if (itm.el.nativeElement.id === elementID) {
								itm.slider.set([item.minRange, item.maxRange]);
							}
						}); */
                    }
				}, 3000);
			}
		});
	}

	expandFilters(filterTerm?: string) {
        let paramFilter = this.paramFilter ? this.paramFilter : null;
        if (filterTerm) {
            paramFilter = filterTerm;
        }

		this.filterList.forEach((item, index) => {
			if (item.filterTerm === paramFilter) {
				item.expanded = true;
			}
		});
	}

	trackByIndex(index) {
		return index;
    }

    setFiltersState(filters) {
        const _that = this;
        for (let i = 0; i < filters.length; i++) {
            const filterList = this.filterList;
            _that.filterList.filter((item, index) => {
                if (item.filterTerm === filters[i].queryTerm) {
                    const elementID = filters[i].queryTerm.replace('.', '_');
                    const paramID = filters[i].filters ? filters[i].filters[0] : 0;
                    setTimeout(() => {
                        if (item.filterType === 'checkbox' || item.filterType === 'horizontal-checkbox') {
                            item.filterList.forEach(itm => {
                                if (filters[i].filters.indexOf(itm.id) > -1) {
                                    itm.checked = true;
                                }
                            });
                        }
                        if (item.filterType === 'radio') {
                            item.filterList.forEach(itm => {
                                if (filters[i].filters.indexOf(itm.value) > -1) {
                                    itm.checked = true;
                                    _that.selectedRadio[item.filterName.toLocaleLowerCase()] = itm.value;
                                }
                            });
                        }
                        if (item.filterType === 'range' || item.filterType === 'rangeFromTo' || item.filterType === 'rangeBetween') {
                            item.filterNgModel[0] = parseFloat(paramID);
                            item.minRange = parseFloat(paramID);
                            _that.rangeSliderRef._results.forEach(itm => {
                                if (itm.el.nativeElement.id === elementID) {
                                    itm.slider.set([item.minRange, item.maxRange]);
                                }
                            });
                        }
                        if (item.filterType === 'datepicker') {
                            item.filterNgModel = paramID;
                            item.minRange = paramID[0];
                            item.maxRange = paramID[1];
                        }
                    }, 3000);

                }
            });
        }
    }

    filterURLParams() {
        // Commented to display More button by clicking the filterURLParams.

        // this.filterList.forEach(item => {
        //     item.hidden = false;
        // });
        this.hiddenFilterCount = this.filterList.reduce((n, obj, index) => {
            if (obj.hidden) {
                n = n + 1;
            }
            return n;
        }, 0);
        this.onFilterChanges.emit({
            selectedFilterData: this.selectedFilterData
        });
    }

    ngAfterViewInit() {
        if (this.queryParams && this.queryParams['utm_medium'] === 'email') {
            this.filterList.forEach(item => {
                item.hidden = false;
            });
            this.hiddenFilterCount = this.filterList.reduce((n, obj, index) => {
                if (obj.hidden) {
                    n = n + 1;
                }
                return n;
            }, 0);
            this.onFilterChanges.emit({
                selectedFilterData: this.selectedFilterData
            });
        }
        /* if (this.queryParams && this.queryParams['utm_medium'] === 'email') {
            this.cookiejson = this.cookieService.get('filterValues_accessdenied') ? JSON.parse(this.cookieService.get('filterValues_accessdenied')) : null;
        }
        if (this.cookiejson && this.cookiejson.length && this.isAuthenticated()) {
            this.filterList.forEach(item => {
                item.expanded = false;
                item.hidden = false;
            });
            this.selectedFilterData = this.cookiejson;
            this.onFilterChanges.emit({
                selectedFilterData: this.selectedFilterData
            });
            this.cookieService.put('filterValues_' + this.currentPath, JSON.stringify(this.selectedFilterData));
            this.cookieService.remove('filterValues_accessdenied');
            this.selectedFilterData.forEach(item => {
                this.expandFilters(item.queryTerm);
            });
        } */
    }

    mouseEnter(p) { // This event used for open and close the popup while mouseover the locked providers
        setTimeout(() => {
            p.close();
        }, 2000);
    }

    upgrade() {
        this.router.navigate(['/upgrade']);
    }

    collapseSortByBar() {
		this.isSortByBarCollapsed = true;
	}

	toggleSortByBar() {
		this.isSortByBarCollapsed = !this.isSortByBarCollapsed;
	}

}
