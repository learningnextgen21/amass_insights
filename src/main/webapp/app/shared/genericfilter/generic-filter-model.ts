import { GenericFilterListModel } from './generic-filter-list.model';

export class GenericFilterModel {
	constructor(
		public filterName?: string,
		public filterList?: GenericFilterListModel[],
		public filterTerm?: string,
		public filterType?: string,
		public filterRange?: any,
        public filterNgModel?: any,
        public filterID?: any,
        public hidden?: boolean,
        public expanded?: boolean,
		public minRange?: any,
		public maxRange?: any,
		public minRangeInit?: any,
		public maxRangeInit?: any,
        public rangeStep?: any,
        public order?: number,
        public mouseoverExplanation?: string,
        public minDate?: any,
        public maxDate?: any
	) {

	}
}
