import { BaseEntity } from '../../shared';

export class GenericFilterListModel implements BaseEntity {
	constructor(
		public id?: number,
        public value?: any,
        public label?: string,
		public checked?: boolean,
		public count?: number,
        public filterTerm?: string,
        public locked?: boolean,
        public explanation?: string,
        public show?: boolean
	) {

	}
}
