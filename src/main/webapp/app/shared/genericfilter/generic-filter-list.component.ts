import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GenericFilterModel } from './generic-filter-model';

const bob = require('elastic-builder');
const requestQueryBody = bob.requestBodySearch();

@Component({
	selector: 'jhi-generic-filter-list',
	templateUrl: 'generic-filter-list.component.html',
	styleUrls: [
		'./generic-filter.component.scss'
	]
})
export class GenericFilterListComponent implements OnInit {
	@Input() filters: GenericFilterModel;
	private activeFilters: any = [];
	selectedFilters: any;
	@Output() onFilterChanges = new EventEmitter();
	selectedRadio: any = {};
	constructor() {

	}

	ngOnInit() {
		console.log(this.filters);
		this.selectedFilters = {};
		if (this.filters.filterType === 'radio') {
			this.selectedRadio[this.filters.filterName.toLowerCase()] = '';
		}
	}

	onFilter(type, value, filterTerm) {
		console.log(type, value, filterTerm);
		if (!this.selectedFilters[filterTerm]) {
			this.selectedFilters[filterTerm] = [];
		}
		this.selectedFilters[filterTerm].push(value);
		this.onFilterChanges.emit({
			queryType: type,
			queryTerm: filterTerm,
			selectedFilters: this.selectedFilters
		});
	}

}
