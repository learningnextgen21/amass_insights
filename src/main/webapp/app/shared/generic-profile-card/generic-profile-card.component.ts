import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { GenericProfileCardModel, GenericProfileCardButton } from './generic-profile-card.model';
import { GenericProfileButtonsModel } from '../generic-profile/generic-profile-buttons.model';
import { Principal } from '../../shared/auth/principal.service';

@Component({
    selector: 'jhi-generic-profile-card',
    templateUrl: './generic-profile-card.component.html',
    styleUrls: [
        './generic-profile-card.component.scss',
        '../../entities/data-events/data-events.component.scss',
        '../../shared/share-buttons/share-buttons.scss'
    ]
})
export class GenericProfileCardComponent implements OnInit {
    @Input() genericProfileCard: GenericProfileCardModel;
    @Input() buttons: GenericProfileButtonsModel;
    @Output() onClick: EventEmitter<any> = new EventEmitter();
    @Output() onLinkClickEmitter: EventEmitter<any> = new EventEmitter();
    userLevel: any;
    editPermission: any;
    sampleResource: boolean;
    lockedProvider: boolean;
    @Input() editAccessPermission: any;
    @Input() locked: boolean;
    @Input() loader: boolean;
    @Input() loaderValue: any;
    @ViewChild('resource') resource: ElementRef;
    constructor(
        private principal: Principal
    ) { }

    ngOnInit() {
        console.log(this.genericProfileCard);
        this.editPermission = this.editAccessPermission;
        console.log('editpermission',this.editPermission);
        console.log('BlockUi', this.editPermission);
        this.userLevel = this.principal.hasAnyPermissionAboveDirect(200);
        //this.userLevel=false;
        console.log('userLevel',this.userLevel);
        this.lockedProvider= this.locked;
        console.log('lockedProvider',this.lockedProvider);
        if (this.genericProfileCard.accessPage === 'sample') {
            this.sampleResource = true;
        }
    }

    onButtonClick(event: GenericProfileCardButton) {
        this.onClick.emit(event);
    }

    onLinkClick(event) {
        this.onLinkClickEmitter.emit(event);
    }

}
