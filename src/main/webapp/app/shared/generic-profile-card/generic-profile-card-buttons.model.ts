export class GenericProfileCardButtonModel{
    constructor(
         public label?: string,
         public type?: string,
         public icon?: string,
         public gaCategory?: string,
         public gaType?: string,
         public clickAction?: string,
         public tooltip?: string
    ) {}
}