import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { GenericProfileCardComponent } from './generic-profile-card.component';
import { DatamarketplaceSharedLibsModule } from '../shared-libs.module';
import { CommonModule } from '@angular/common';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import {ProgressSpinnerModule} from 'primeng/progressspinner';

@NgModule({
    imports: [ BrowserModule, CommonModule, DatamarketplaceSharedLibsModule, ProgressSpinnerModule, ShareButtonModule, ShareButtonsModule.withConfig({
        debug: true
    })],
    declarations: [ GenericProfileCardComponent ],
    exports: [GenericProfileCardComponent],
    bootstrap:    [ GenericProfileCardComponent ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GenericProfileCardModule { }