import { EventEmitter, Output } from '@angular/core';

export class GenericProfileCardModel {
    constructor(
        public subTitle?: string,
        public content?: GenericProfileCardContentModel[],
        public id?: number,
        public locked?: boolean,
        public accessPage?: any
    ) {

    }
}

export class GenericProfileCardContentModel {
    constructor(
        public columnContent?: GenericProfileCardContentColumnText[],
        public columns?: number,
        public columnSize?: number[]
    ) {

    }
}

export class GenericProfileCardContentColumnText {
    constructor(
        public title?: string,
        public image?: any,
        public imageStyle?: any,
        public label?: string,
        public labelToolTip?: string,
        public labelStyle?: any,
        public text?: any,
        public titleRight?: any,
        public style?: any,
        public plainText?: any,
        public userPermission?: any,
        public locked?: any,
        public plainTextUrl?: any,
        public toolTip?: any,
        public buttons?: GenericProfileCardButton[],
        public shareButtons?: GenericProfileCardShareButtons[],
        public arrayText?: GenericProfileCardArrayText[],
        public url?: any,
        public filterType?: any,
        public filterKey?: any,
        public completeness?: any,
        public clickable?: boolean,
        public clickableObj?: any
    ) {

    }
}

export class GenericProfileCardArrayText {
    constructor(
        public id?: any,
        public desc?: any
    ) {}
}

export class GenericProfileCardButton {
    constructor(
         public label?: string,
         public icon?: any,
         public url?: any,
         public btnClass?: any,
         public gaAction?: string,
         public gaLabel?: string,
         public gaCategory?: string,
         public gaEvent?: string,
         public clickAction?: string,
         public tooltip?: string
    ) {}

}

export class GenericProfileCardShareButtons {
	constructor(
        public buttonTooltip?: string,
        public url?: string,
        public type?: string,
        public description?: string
	) {}
}
