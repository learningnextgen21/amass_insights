import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { startWith, map, switchMap, tap } from 'rxjs/operators';
import { forkJoin, Observable, zip } from 'rxjs';
import { DataProviderService } from '../../entities/data-provider/data-provider.service';
import { DataProviderSearchService } from '../../entities/data-provider/data-provider-search.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Principal } from '../../shared';

@Component({
	selector: 'jhi-generic-search',
	templateUrl: 'generic-search.component.html',
	styleUrls: [
		'../../shared/share-buttons/share-buttons.scss',
		'../../entities/data-provider/data-provider.component.scss'
	]
})
export class GenericSearchComponent implements OnInit {
	providerNameOptions: any;
	providerNameOptionsTag: any[] = [];
	providerSearchControl: FormControl = new FormControl();
	providerSearchControlTag: FormControl = new FormControl();
	filteredData: any;
	nameOptionsData: any;
	uniqueData: any;
	dataProviderObj: any;
	dataProviderTagObj: any;
	dataProviderList: any[] = [];
	dataObj: any;
	@Output() searchCommon = new EventEmitter();
	@Output() clear = new EventEmitter();
	@Output() blockAutocomplete = new EventEmitter();
	@Input() currentSearchQuery: string;
	@Input() userSearch: string;
    @ViewChild('inputFieldHeader') clearInput: ElementRef;
	header: boolean;
	searchButtons: string;
	currentSearch: any;
	@Input() entityType: string;
	constructor(
		private dataProviderService: DataProviderService,
		private dataProviderSearchService: DataProviderSearchService,
		private router: Router,
		private principal: Principal,
	) {

	}
	isAuthenticated() {
		return this.principal.isAuthenticated();
	}

	ngOnInit() {
		console.log('userSearch', this.userSearch);
        this.dataProviderService.clearHeaderSearch$.subscribe(data => {
            if (data && this.clearInput && this.clearInput.nativeElement && this.clearInput.nativeElement.value) {
                this.clearInput.nativeElement.value = '';
            }
        });

		if (this.userSearch && this.userSearch === 'header') {
			this.header = true;
		}
		else {
			this.header = false;
		}
		if (this.isAuthenticated() && this.entityType !=='investors') {
			this.providerNameOptions = this.providerSearchControl.valueChanges
				.pipe(
					startWith(''),
					switchMap(val => {
					//   console.log('val',val);
					//   console.log('vallength',val.length);
					//   console.log('providername',this.providerNameOptions);  
						/*   if(val.length <= 2)
						{
							console.log('called');
							return [];
						}   */
						const data = this.dataProviderService.query({
							query: {
								prefix: {
									'providerName': val
								}
							},
							size: 5
						});
						const tag = this.dataProviderService.queryTag({
							query: {
								prefix: {
									'providerTag': val
								}
							},
							size: 5
						});

						return forkJoin([data, tag])
							.pipe(
								map(dataValue => {
									const result = dataValue[0].body;
									const providerWarningText = '<strong class="warn-text">At least one provider matches this query but is locked for you. Please contact Amass for access.</strong>';
									for (let i = 0; i < dataValue.length; i++) {
										const resultProviderName = dataValue[0].body;
										const resultProviderTag = dataValue[1].body;
										this.dataProviderTagObj = [];
										this.dataProviderObj = [];
										if (resultProviderName['hits']['total'] && resultProviderName['hits']['hits'] && val.length > 2) {
											this.dataProviderObj = [];
											this.dataProviderObj = resultProviderName['hits']['hits'].map(item => {
												if (!item._source.locked) {
													//const providerValue = 'providerName:'+item._source.providerName
													this.dataProviderObj.push({
														providerName: item._source.providerName,
														recordID: item._source.recordID
													});
                                                    this.dataProviderList = this.dataProviderObj;
												    console.log('dataProviderList', this.dataProviderList);
												}
												else if (item._source.locked && this.dataProviderObj.length === 0) {
													this.dataProviderObj = [providerWarningText];
                                                    this.dataProviderList = this.dataProviderObj;
												    console.log('dataProviderList', this.dataProviderList);
												}
											});
										}
										else {
											return [];
										}

										if (resultProviderTag['hits']['total'] && resultProviderTag['hits']['hits'] && val.length > 2) {
											this.dataProviderTagObj = [];
											this.dataProviderTagObj = resultProviderTag['hits']['hits'].map(item => {
												if (item._source.providerTag && (item._source.privacy === 'Public' || 'Private') && item._source.type !== null) {
                                                    console.log(item);
													/* const tagResult=item._source.type+':'+item._source.providerTag
													console.log('tagResult',tagResult); */
													return item._source;

												}
												/* else {
													return providerWarningText;
												} */
											});
										}
									}
									if (this.dataProviderObj && this.dataProviderObj.length || this.dataProviderTagObj && this.dataProviderTagObj.length) {
										//console.log('called');
										//console.log('dataProviderList 123', this.dataProviderTagObj);
										// const finalArr = this.dataProviderObj.concat(this.dataProviderTagObj);
										// console.log('finalArr',this.dataProviderObj);
										// const filtered = finalArr.filter(x => x !== undefined);
										//    console.log(filtered);
                                        console.log(this.dataProviderTagObj);
										this.dataProviderList = this.dataProviderList.concat(this.dataProviderTagObj);								
										this.uniqueData = this.dataProviderList.filter((elem, index, self) => {
                                            console.log(elem, index, self);
											return index === self.indexOf(elem) && elem !== undefined;
										});

										console.log('uniqueData', this.uniqueData);
										this.filteredData = this.uniqueData;
										this.filteredData.push(this.filteredData.splice(this.filteredData.indexOf(providerWarningText), 1)[0]);
										return this.filteredData;
									}


								}),
								tap(data => {
									// console.log(data);
								}),
							);
					}),
				)
				.pipe(
					startWith([]),
				);
		}
		else if(this.isAuthenticated && this.entityType === 'investors')
		{
			this.providerNameOptions = this.providerSearchControl.valueChanges
			.pipe(
			  startWith(''),
			  switchMap(val => {
				  
					  return this.dataProviderService.queryinvestor({
						  query: {
							  prefix: {
								  'name': val
							  }
						  },
						  size: 5
					  }).pipe(
						  map(data => {
							  const result = data.body;
							  const providerWarningText = '<strong class="warn-text">At least one provider matches this query but is locked for you. Please contact Amass for access.</strong>';
							  if (result['hits']['total'] && result['hits']['hits'] && val.length > 2) {
								  const dataObj = result['hits']['hits'].map(item => {
									  if (item._source.name) {
										  return item._source.name;
									  } else {
										  return providerWarningText;
									  }
								  });
								  var uniqueData = dataObj.filter((elem, index, self) => {
									  return index === self.indexOf(elem);
								  });
								  const filteredData = uniqueData;
								  filteredData.push(filteredData.splice(filteredData.indexOf(providerWarningText), 1)[0]);
								 console.log(filteredData);
								  return filteredData;
							  } else {
								  return [];
							  }
						  }),
						  tap(data => {
							  // console.log(data);
						  }),
					  );
			  }),
			)
			.pipe(
				startWith([]),
			);
		}
		else if (!this.isAuthenticated()) {
			console.log('called');
			this.currentSearchQuery = '';
			this.providerNameOptionsTag.push('B2C Transactional Data', 'Hiring, Jobs & Employment Data', 'Satellite, Supply Chain & Shipping Data', 'Consumer Location Data');
			console.log(this.providerNameOptionsTag);
		}
		if (!this.isAuthenticated()) {
			this.providerNameOptions = this.providerSearchControl.valueChanges
				.pipe(
					startWith(''),
					switchMap(val => {
						console.log(val.length);
						return this.dataProviderService.query({
							'_source': {
								'include': [
									'providerName',
									'recordID'
								]
							},
							query: {
								prefix: {
									'providerName': val
								}
							},
							size: 5
						}).pipe(
							map(data => {
								const result = data.body;
								this.dataObj = [];
								const providerWarningText = '<strong class="warn-text">At least one provider matches this query but is locked for you. Please contact Amass for access.</strong>';
								if (result['hits']['total'] && result['hits']['hits'] && val.length > 2) {
									this.dataObj = [];
									this.dataObj = result['hits']['hits'].map(item => {
										if (item._source.providerName) {
											this.dataObj.push({
												providerName: item._source.providerName,
												recordID: item._source.recordID
											});
										}
										this.dataProviderList = this.dataObj;
									});
									console.log(' UR dataProviderList 123', this.dataProviderList);
									var uniqueData = this.dataProviderList.filter((elem, index, self) => {
										return index === self.indexOf(elem);
									});
									const filteredData = uniqueData;
									console.log(filteredData);
									filteredData.push(filteredData.splice(filteredData.indexOf(providerWarningText), 1)[0]);
									//  console.log(filteredData);
									return filteredData;
								} else {
									return [];
								}
							}),
							tap(data => {
								// console.log(data);
							}),
						);
						/*  } else {
							 console.log('backspace');	
							 return [];
						 } */
					}),
				)
				.pipe(
					startWith([]),
				);
		}
	}
	searchName(query) {
		console.log('query', query);
		if (query) {
			this.searchCommon.emit(query);
		}
		//this.dataProviderSearchService.sendMessage(query);
		if (this.isAuthenticated() && this.header) {
			this.currentSearchQuery = '';
			this.dataProviderSearchService.headersendSearch(query);
			localStorage.setItem('searchheader', query);
			this.router.navigate(['../providers'])
		}
		if(this.isAuthenticated())
		{
			this.currentSearch = {
				'query': query + '*', // this.reSubmitValues ? '*' + query + '*' : query,
				'fields': ['_all', 'providerName^2']
			};
			const search={};
			search['searchTerms']=query;
			console.log('json',JSON.stringify(this.currentSearch));
			search['elasticSearch']=JSON.stringify(this.currentSearch);
			this.dataProviderService.searchAdded(search).subscribe(data =>{
							 console.log('datasearch',data);
						 }
						 );
		}
		if (query && !this.isAuthenticated()) {
			if (!this.isAuthenticated() && this.header) {
				//this.currentSearchQuery = '';
				this.dataProviderSearchService.headerUnrestrictedCommonSendSearch(query);
				localStorage.setItem('searchheader', query);
				this.router.navigateByUrl('unproviders');

			}
		}

	}
	searchInvestorName(query) {
		console.log('query', query);
		if (query) {
			this.searchCommon.emit(query);
		}
	}
	/* onKeydown($event){
		console.log($event.key); // This will log key which is pressed.
	} */
	clearSearch() {
		console.log('clera');
		this.currentSearchQuery = '';
		//this.providerNameOptions = '';
		if(this.entityType!=='investors')
		{
			this.dataProviderSearchService.sendSearch('clear');
		}
		if(this.entityType =='investors')
		{
			this.dataProviderSearchService.sendInvestorSearch('clear');
		}
		//localStorage.setItem('search','clear');

	}
	blockAutocompleteSearch(event, header) {
		console.log('header  ', header);
		console.log('event', event);
		//this.blockAutocomplete.emit(event);
		if (this.header) {
			this.currentSearchQuery = '';
		}
		if (this.isAuthenticated()) {
			// this.dataProviderSearchService.sendSearch(event);
			const value = event.option.value.providerTag ? event.option.value.providerTag : event.option.value.recordID;
			if (event && event.option && event.option.value && !event.option.value.providerTag) {
				localStorage.setItem('search', event.option.value.providerName);
				this.router.navigate([`../providers/${value}`]);
			} else if (event && event.option && event.option.value && event.option.value.providerTag) {
				this.dataProviderSearchService.sendSearch(event);
				localStorage.setItem('search', event.option.value.providerTag);
				this.router.navigate(['../providers']);
			}
		}
		else if (!this.isAuthenticated()) {
			this.currentSearchQuery = '';
			console.log('cureent', this.currentSearchQuery);
			console.log(event.option.value);
			if (event.option.value === 'B2C Transactional Data') {
				this.searchButtons = 'Transactional';
			}
			else if (event.option.value === 'Hiring, Jobs & Employment Data') {
				this.searchButtons = 'Jobs';
			}
			else if (event.option.value === 'Satellite, Supply Chain & Shipping Data') {
				this.searchButtons = 'Shipping';
			}
			else if (event.option.value === 'Consumer Location Data') {
				this.searchButtons = 'Location';
			}
			else {
				this.searchButtons = event.option.value.providerName;
				this.currentSearchQuery = this.searchButtons;

			}
			console.log('searchbutton', this.searchButtons);
			if (this.searchButtons) {
				console.log('calledheader');
				if (this.header && !this.isAuthenticated()) {
					console.log('unrestricted');
					if (event.option.value.recordID) {
						this.dataProviderSearchService.headerUnrestrictedSendSearch(this.searchButtons);
						localStorage.setItem('SearchData', this.searchButtons);
						this.router.navigate([`../unproviders/${event.option.value.recordID}`]);
					} else if (!event.option.value.recordID) {
						this.dataProviderSearchService.headerUnrestrictedSendSearch(this.searchButtons);
						localStorage.setItem('SearchData', this.searchButtons);
						this.router.navigateByUrl('unproviders');
					}
				}
			}
			if (header === 'nonHeader' && !this.isAuthenticated()) {
				console.log('unrestrictedheader');
				this.router.navigate([`../unproviders/${event.option.value.recordID}`]);
				/* const value= event.option.value;
				console.log('value',value); */
			}
			// this.router.navigateByUrl('unproviders');
		}

	}
	blockAutocompleteinvestorSearch(event)
	{
		console.log('event',event);
		this.dataProviderSearchService.sendInvestorSearch(event);
	}
}