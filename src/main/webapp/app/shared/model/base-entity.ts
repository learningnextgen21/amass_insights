export interface BaseEntity {
    // using type any to avoid methods complaining of invalid type
    id?: any;
    name?: any;
    providerName?: any;
    explanation?: any;
    providerType?: any;
    recordID?: any;
    logo?: any;
    locked?: boolean;
}