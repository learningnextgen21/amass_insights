import { Injectable, Output, EventEmitter, Directive } from '@angular/core';
import { JhiLanguageService } from 'ng-jhipster';

import { Principal } from '../auth/principal.service';
import { AuthServerProvider } from '../auth/auth-session.service';
import { JhiTrackerService } from '../tracker/tracker.service';
import { INTERCOM_ID } from '../../app.constants';
import { Intercom } from 'ng-intercom';
import { HttpClient } from '@angular/common/http';

@Directive()
@Injectable()
export class LoginService {

    @Output()
    getLoggedInName: EventEmitter<any> = new EventEmitter();

	constructor(
		private languageService: JhiLanguageService,
		private principal: Principal,
		private trackerService: JhiTrackerService,
		private authServerProvider: AuthServerProvider,
        private intercom: Intercom,
        private http: HttpClient
	) {}

	login(credentials, callback?) {
		const cb = callback || function() {};

		return new Promise((resolve, reject) => {
			this.authServerProvider.login(credentials).subscribe((data) => {
				this.principal.identity(true).then((account) => {
					// After the login the language will be changed to
					// the language selected by the user during his registration
					if (account !== null) {
						this.languageService.changeLanguage(account.langKey);
						this.intercom.boot({
							app_id: INTERCOM_ID,
							name: account.firstName + ' ' + account.lastName, // Full name = First Name & Last Name
							email: account.email, // Email address
							user_id: account.id
                        });
                        this.getLoggedInName.emit(account.login);
                        ga('set', 'userId', account.id);
					}
					this.trackerService.sendActivity();
					resolve(data);
				});
				return cb();
			}, (err) => {
				this.logout();
				reject(err);
				return cb(err);
			});
		});
	}

	logout() {
		this.authServerProvider.logout().subscribe(data => {
            this.http.get('api/account').subscribe(() => {}, () => {});
        });
        this.principal.authenticate(null);
        this.getLoggedInName.emit('User');
	}
}
