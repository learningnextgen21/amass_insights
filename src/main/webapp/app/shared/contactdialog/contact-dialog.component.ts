import { Component, OnInit, Input, EventEmitter, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { NgForm } from '@angular/forms';
import { JhiLanguageHelper, Principal, LoginModalService, LoginService, AccountService } from '../../shared';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { HttpClient } from '@angular/common/http';

@Component({
	selector: 'jhi-contact-dialog',
	templateUrl: './contact-dialog.component.html',
	styleUrls: [
		'./contact-dialog.component.scss',
		'../../../content/scss/amass-form.scss'
	]
})
export class ContactDialogComponent implements OnInit, AfterViewChecked {
	contactFields: any;
	public providerID: number;
    public categoryID: number;
    public requestType: number;
	validationError: string;
	responseMessage: string;
	public dialogTitle: string;
	public dialogSubTitle: string;
    public dialogType: string;
    public userName: string;
    public userEmail: any;
    public requestAccess: boolean;
	onButtonAction = new EventEmitter();

	constructor(
		private http: HttpClient,
        public dialogRef: MatDialogRef<any>,
        private cdRef: ChangeDetectorRef,
        public principal: Principal,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private router: Router

	) { }

	ngOnInit() {
		this.contactFields = {};
		this.validationError = null;
		this.responseMessage = null;
		switch (this.dialogType) {
			case 'moreInformation':
			this.contactFields.providerInformation = 'I\'d like more information about this data provider';
			this.contactFields.accessToProvider = null;
			this.contactFields.accessToCategory = null;
			break;
			case 'unlockProvider':
            this.requestAccess = true;
			this.contactFields.accessToProvider = 'I\'d like access to this data provider';
			this.contactFields.providerInformation = null;
			this.contactFields.accessToCategory = null;
			break;
			case 'unlockCategory':
			this.contactFields.accessToCategory = 'I\'d like access to this data category';
			this.contactFields.contactRequestEngineeringData = null;
			this.contactFields.accessToProvider = null;
            break;
            case 'requestFindingData':
			this.contactFields.contactRequestFindingData = 'I am looking for the best alternative data sources that will add alpha to my portfolio.';
			this.contactFields.contactRequestEngineeringData = null;
            this.contactFields.contactRequestSellingData = null;
            this.contactFields.contactDataSample = null;
            this.contactFields.contactDirectContact = null;
            this.contactFields.suggestProviderType = null;
            this.contactFields.suggestArticleType = null;
            this.userName = '';
            this.userEmail = '';
            break;
            case 'requestEngineeringData':
			this.contactFields.contactRequestEngineeringData = 'I\'d like help with cleaning, analyzing and interpreting this dataset.';
			this.contactFields.contactRequestFindingData = null;
            this.contactFields.contactRequestSellingData = null;
            this.contactFields.contactDataSample = null;
            this.contactFields.contactDirectContact = null;
            this.contactFields.suggestProviderType = null;
            this.contactFields.suggestArticleType = null;
            this.userName = '';
            this.userEmail = '';
            break;
            case 'requestSellingData':
			this.contactFields.contactRequestSellingData = 'I\'d like help identifying and selling to new prospects for my data products.';
			this.contactFields.contactRequestFindingData = null;
            this.contactFields.contactRequestEngineeringData = null;
            this.contactFields.contactDataSample = null;
            this.contactFields.contactDirectContact = null;
            this.contactFields.suggestProviderType = null;
            this.contactFields.suggestArticleType = null;
            this.userName = '';
            this.userEmail = '';
            break;
            case 'requestDirectContact':
            this.contactFields.contactDirectContact = 'I\'d like direct contact with this company to continue my information-gathering process.';
            this.contactFields.contactRequestSellingData = null;
			this.contactFields.contactRequestFindingData = null;
            this.contactFields.contactRequestEngineeringData = null;
            this.contactFields.contactDataSample = null;
            this.contactFields.suggestProviderType = null;
            this.contactFields.suggestArticleType = null;
            break;
            case 'requestDataSample':
            this.contactFields.contactDataSample = 'I\'d like to receive sample data from this provider that I can use to backtest their dataset to determine its value.';
            this.contactFields.contactRequestSellingData = null;
			this.contactFields.contactRequestFindingData = null;
            this.contactFields.contactRequestEngineeringData = null;
            this.contactFields.contactDirectContact = null;
            this.contactFields.suggestProviderType = null;
            this.contactFields.suggestArticleType = null;
            break;
            case 'suggestProvider':
            this.contactFields.contactDataSample = null;
            this.contactFields.contactRequestSellingData = null;
			this.contactFields.contactRequestFindingData = null;
            this.contactFields.contactRequestEngineeringData = null;
            this.contactFields.contactDirectContact = null;
            this.contactFields.suggestArticleType = null;
            this.contactFields.suggestProviderType = 'I\'d like to suggest that Provider is added to your platform. You can find more information about Provider at www.example.com.';
            break;
            case 'suggestArticle':
            this.contactFields.contactDataSample = null;
            this.contactFields.contactRequestSellingData = null;
			this.contactFields.contactRequestFindingData = null;
            this.contactFields.contactRequestEngineeringData = null;
            this.contactFields.contactDirectContact = null;
            this.contactFields.suggestProviderType = null;
            this.contactFields.suggestArticleType = 'I\'d like to suggest that Provider is added to your platform. You can find more information about Provider at www.example.com.';
			break;
			default:
			this.contactFields.providerInformation = null;
			this.contactFields.accessToProvider = null;
			this.contactFields.accessToCategory = null;
			this.dialogType = 'contactUs';
			break;
		}
    }
    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

	submitContactMessage(contactPopupForm: NgForm) {
		if (contactPopupForm.valid && this.contactFields.contactRequestFindingData !== '' && this.contactFields.contactRequestEngineeringData !== '' && this.contactFields.contactRequestSellingData !== '' && this.contactFields.providerInformation !== '' && this.contactFields.accessToProvider !== '' && this.contactFields.accessToCategory !== '' && this.contactFields.contactDirectContact !== '' && this.contactFields.contactDataSample !== '') {
			this.validationError = null;
           let apiResource = this.isAuthenticated() ? 'api/contact-us' : 'api/ur/contact-us';
			let action = '';
			const inputData = {};

			switch (this.dialogType) {
				case 'unlockProvider':
				action = '-provider-unlock';
				inputData['message'] = this.contactFields['accessToProvider'];
				inputData['providerID'] = this.providerID;
				break;
				case 'moreInformation':
                action = '-provider-more-info';
                inputData['requestType'] = this.requestType;
				inputData['message'] = this.contactFields['providerInformation'];
				inputData['providerID'] = this.providerID;
				break;
				case 'unlockCategory':
				action = '-category-unlock';
				inputData['categoryID'] = this.categoryID;
				inputData['providerID'] = this.providerID;
				inputData['message'] = this.contactFields['accessToCategory'];
                break;
                case 'requestFindingData':
				action = this.isAuthenticated() ? '-request-header-info' : '-header-request-logout';
				inputData['requestType'] = this.requestType;
                inputData['message'] = this.contactFields['contactRequestFindingData'];
                inputData['userName'] = !this.isAuthenticated() ? this.userName : '';
                inputData['userEmail'] = !this.isAuthenticated() ? this.userEmail : '';
                break;
                case 'requestEngineeringData':
				action = this.isAuthenticated() ? '-request-header-info' : '-header-request-logout';
				inputData['requestType'] = this.requestType;
                inputData['message'] = this.contactFields['contactRequestEngineeringData'];
                inputData['userName'] = !this.isAuthenticated() ? this.userName : '';
                inputData['userEmail'] = !this.isAuthenticated() ? this.userEmail : '';
                break;
                case 'requestSellingData':
				action = this.isAuthenticated() ? '-request-header-info' : '-header-request-logout';
				inputData['requestType'] = this.requestType;
                inputData['message'] = this.contactFields['contactRequestSellingData'];
                inputData['userName'] = !this.isAuthenticated() ? this.userName : '';
                inputData['userEmail'] = !this.isAuthenticated() ? this.userEmail : '';
                break;
                case 'requestDirectContact':
				action = '-provider-more-info';
				inputData['requestType'] = this.requestType;
                inputData['message'] = this.contactFields['contactDirectContact'];
                inputData['providerID'] = this.providerID;
                break;
                case 'requestDataSample':
				action = '-provider-more-info';
				inputData['requestType'] = this.requestType;
                inputData['message'] = this.contactFields['contactDataSample'];
                inputData['providerID'] = this.providerID;
                break;
                case 'suggestProvider':
				action = '-request-header-info';
				inputData['requestType'] = this.requestType;
                inputData['message'] = this.contactFields['suggestProviderType'];
                inputData['providerID'] = this.providerID;
                break;
                case 'suggestArticle':
				action = '-request-header-info';
				inputData['requestType'] = this.requestType;
                inputData['message'] = this.contactFields['suggestArticleType'];
                inputData['providerID'] = this.providerID;
				break;
				case 'contactUs':
				action = '';
				inputData['message'] = this.contactFields['message'];
				break;
			}

            apiResource = apiResource + action;

            if (inputData['message'].length <= 255) {
                this.http.post(apiResource, inputData, { observe: 'response' }).subscribe(resp => {
                    // console.log('resp', resp);
                    this.responseMessage = 'Thank you for contacting Amass Insights. We will be in touch with you soon.';
                    this.onButtonAction.emit('success');
                }, (response) => {
                    // console.log(response);
                    this.responseMessage = null;
                    this.validationError = 'Something went wrong while submitting. Please try again.';
                    this.onButtonAction.emit('error');
                });
            }

           /*  if (inputData['message'] === 'I\'d like more information about this data provider') {
                this.googleAnalyticsService.emitEvent('Provider More Info', 'Form Submitted', 'More Information Button was clicked');
            } else if (inputData['message'] === 'I\'d like access to this data provider') {
                this.googleAnalyticsService.emitEvent('Provider Unlock', 'Form Submitted', 'Unlock Provider Button was clicked');
            } */
            if (inputData['message'] === 'I\'d like more information about this data provider') {
                this.googleAnalyticsService.emitEvent('Provider More Info', 'Form Submitted', 'More Information Button was clicked');
            } else if (inputData['message'] === 'I\'d like access to this data provider') {
                this.googleAnalyticsService.emitEvent('Provider Unlock', 'Form Submitted', 'Unlock Provider Button was clicked');
            }
            if (this.dialogType === 'Suggest') {
                this.googleAnalyticsService.emitEvent('Suggest Provider', 'Form Submitted', 'Contact Us Popup - Button - Submit');
            }

		} else {
			this.validationError = 'Please fill the required fields and resend.';
			this.responseMessage = null;
        }

	}

	closeDialog() {
		this.onButtonAction.emit('cancelled');
        this.dialogRef.close();
       /*  if (this.contactFields.providerInformation === 'I\'d like more information about this data provider') {
            this.googleAnalyticsService.emitEvent('Provider More Info', 'Exit Popup Link Clicked', 'Contact Us Popup - Button - Cancel');
        } else if (this.contactFields.accessToProvider === 'I\'d like access to this data provider') {
            this.googleAnalyticsService.emitEvent('Provider Unlock', 'Exit Popup Link Clicked', 'Contact Us Popup - Button - Cancel');
        } */
        if (this.contactFields.providerInformation === 'I\'d like more information about this data provider') {
            this.googleAnalyticsService.emitEvent('Provider More Info', 'Exit Popup Link Clicked', 'Contact Us Popup - Button - Cancel');
        } else if (this.contactFields.accessToProvider === 'I\'d like access to this data provider') {
            this.googleAnalyticsService.emitEvent('Provider Unlock', 'Exit Popup Link Clicked', 'Contact Us Popup - Button - Cancel');
        }
    }
    upgrade()
    {
        this.dialogRef.close();
        this.router.navigate(['/upgrade']);
    }
    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }
}
