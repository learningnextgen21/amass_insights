export class SortingModel {
	constructor(
		public key: string,
		public label: string,
        public order: string,
        public mouseoverExplanation: string,
        public type?: string,
        public recommendedSorting?: SortingModel[],
        public userLevel?: any
	) {

	}
}
