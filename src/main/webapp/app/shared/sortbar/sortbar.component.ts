import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { SortingModel } from './sorting.model';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { CookieService } from 'ngx-cookie';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { Principal } from '../auth/principal.service';
import { AmassSettingService } from '../../account/settings/settings.service';

@Component({
	selector: 'jhi-sortbar',
	templateUrl: 'sortbar.component.html',
	styleUrls: [
		'./sortbar.component.scss'
	],
	encapsulation: ViewEncapsulation.None
})
export class JhiSortbarComponent implements OnInit {
	@Input() sortingArray: SortingModel[];
	@Input() totalItems: number;
    @Input() entityType: string;
    @Input() explanation: string;
    @Input() sortCategory: string;
    @Input() totalItemsTooltip: string;
    @Input() sortByTooltip: string;
    providerCount: any;
    @ViewChild('sortbarblock') sortbarblock: ElementRef;
    @ViewChild('sortbarblockscore') sortbarblockscore: ElementRef;
    orders: any;
    sortLabel: any;
    cookiesortjson: any;
    currentPath: string;
    isNavbarCollapsed: boolean;
    provider: boolean;
	@Output() sortBy = new EventEmitter();
    isSortByBarCollapsed: boolean;
    name:string;
    activated: boolean;
    constructor(
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        public cookieService: CookieService,
        private router: Router,
        private principal: Principal,
        private amassSettingService: AmassSettingService
    ) {
        router.events.subscribe(event => {
            const url = event['url'];
            this.currentPath = url ? url.replace('/', '').split('?')[0] : '';
			if (event instanceof NavigationStart) {
                this.currentPath = event.url.replace('/', '').split('?')[0];
			}
        });
        this.isSortByBarCollapsed = true;
    }

	ngOnInit() {
        console.log('name',this.name);
        this.name = '';
        this.cookiesortjson = this.cookieService.get('newSorting_' + this.currentPath) ? JSON.parse(this.cookieService.get('newSorting_' + this.currentPath)) : null;
        if (this.cookiesortjson && this.principal.isAuthenticated()) {
            const sorting = this.cookiesortjson;
            this.sortBy.emit(this.cookiesortjson);
            this.sortingArray.filter(item => {
                if (item.key === sorting.key) {
                    item.order = sorting.order;
                }
            });
        }
       // this.userLevel = this.principal.hasAnyPermissionAboveDirect(200);
       //console.log('totalItems',this.totalItems);
        this.amassSettingService.checkProviderCount(this.entityType).subscribe(data => {
            this.providerCount = data;
        });
    }
	sort(column,name?: string) {
        if(name)
        {
            column.key = name;
            this.name = name;
            this.activated = true;
        }
        else
        {
            this.name = column.key;
            this.activated=false;
        }
        this.sortBy.emit(column);
        this.cookieService.put('newSorting_' + this.currentPath, JSON.stringify(column));
        this.cookiesortjson = this.cookieService.get('newSorting_' + this.currentPath) ? JSON.parse(this.cookieService.get('newSorting_' + this.currentPath)) : null;
        if (this.sortCategory === 'Providers') {
            this.sortLabel = 'Providers List - Sort - ' + column.label;
        } else if (this.sortCategory === 'Categories') {
            this.sortLabel = 'Data Categories - Sort - ' + column.label;
        } else if (this.sortCategory = 'Articles') {
            this.sortLabel = 'Articles List - Sort - ' + column.label;
            console.log('articles...', this.sortLabel);
        } else {
            this.sortLabel = 'Events List - Sort - ' + column.label;
        }
        if (column.order === 'asc') {
            this.orders = 'Sort Inverted';
        } else {
            this.orders = 'Sort Activated';
        }
        this.googleAnalyticsService.emitEvent(this.sortCategory, this.orders, this.sortLabel);
	}

    upgrade() {
        this.router.navigate(['/upgrade']);
    }

	collapseSortByBar() {
		this.isSortByBarCollapsed = true;
	}

	toggleSortByBar() {
		this.isSortByBarCollapsed = !this.isSortByBarCollapsed;
	}

}
