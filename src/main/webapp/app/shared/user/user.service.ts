

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { User } from './user.model';
import { createRequestOption } from '../model/request-util';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class UserService {
    private resourceUrl = 'api/users';
    private resourceUserProviderUrl = 'api/userProviderOperated';
    private checkUser = 'api/checkUserById';
    private providerPermissionUrl = 'api/providerPermissionAccess';

    constructor(private http: HttpClient) { }

    create(user: User): Observable<HttpResponse<any>> {
        const model=user;
        if(model.rolePermission){
            model.rolePermission={id:model.rolePermission};
        }
        return this.http.post(this.resourceUrl, model, { observe: 'response' });
    }

    update(user: User): Observable<HttpResponse<any>> {
        const model=user;
        if(model.rolePermission){
            model.rolePermission={id:model.rolePermission};
        }
        return this.http.put(this.resourceUrl, model, { observe: 'response' });
    }

    createProvider(user: User): Observable<HttpResponse<any>> {
        return this.http.post(this.providerPermissionUrl, user, { observe: 'response' });
    }

    find(login: string): Observable<User> {
        return this.http.get(`${this.resourceUrl}/${login}`);
    }

    findUserProvider(id:Number):Observable<any>{
        return this.http.get(`${this.resourceUserProviderUrl}/${id}`, { observe: 'response' });
    }

    findUserValueById(id:Number):Observable<any>{
        return this.http.get(`${this.checkUser}/${id}`, { observe: 'response' });
    }
    query(req?: any): Observable<HttpResponse<any>> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(login: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${login}`, { observe: 'response' });
    }

    authorities(): Observable<any> {
        return this.http.get('api/users/authorities', { observe: 'response' });
    }
    userauthorities(): Observable<any> {
        return this.http.get('api/userAuthority');
    }
}
