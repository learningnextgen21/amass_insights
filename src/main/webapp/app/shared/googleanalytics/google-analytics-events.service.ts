import { Injectable } from '@angular/core';

@Injectable()
export class GoogleAnalyticsEventsService {
	public emitEvent(
		eventCategory: string,
		eventAction: string,
		eventLabel: string = null,
		eventValue: number = null
	) {
		ga('send', 'event', {
			'eventCategory': eventCategory,
			'eventAction': eventAction,
			'eventLabel': eventLabel,
			'eventValue': eventValue
		});
    }
    public emitEvents(
		eventKeyword: string,
		eventPageview: string
	) {
		ga('send', 'event', {
			'eventKeyword': eventKeyword,
			'eventPageview': eventPageview
		});
	}
}
