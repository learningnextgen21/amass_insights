import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { HomeEvents } from './genric-home-event.model';
import { DataEventsService } from '../../entities/data-events/data-events.service';
import { ResponseWrapper, ITEMS_PER_PAGE, GenericFilterModel, AmassFilterService, Principal, GenericFilterComponent } from 'app/shared';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { GoogleAnalyticsEventsService } from 'app/shared/googleanalytics/google-analytics-events.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from 'app/loader/loaders.service';
import { ApplyAccountComponent } from 'app/account';

const bob = require('elastic-builder');
@Component({
    selector: 'jhi-home-data-events',
    templateUrl: './genric-home-event.component.html',
    styleUrls: [
        './genric-home-event.component.scss'
    ]
})
export class HomeEventsComponent implements OnInit {
    currentDate: any;
    minDate: any;
    maxDate: any;
    minUpdatedDate: any;
    /* dataEvents: HomeEvents[]; */

    itemsPerPage: number;
	links: any;
	page: any;
    predicate: any;
    reverse: any;
    totalItems: number;
	itemsFrom: number;
	currentSearch: any;
	currentSearchQuery: string;
	currentFilter: string;
    currentSorting: any;

    sortingArray = [];
    sortedIndex: any;

    loader: any;
	loading: any;
    loaderInfinite: any;
    limitedEvents: any;

    subscription: any;

    reSubmitValues: any;
    completenessTooltip: any[];
    filters: GenericFilterModel[];
    statusFilter: any;
    paramFilter: string;
	paramID: string;
	paramType: string;
	paramCreatedDate: any;
    paramUpdatedDate: any;
    isAdmin: boolean;
    mdDialogRef: MatDialogRef<any>;
    limitedLogoutUsersEvent: boolean;
    @ViewChild(GenericFilterComponent) genericFilter: GenericFilterComponent;
    totalItemsTooltip: string;
    sortByTooltip: string;
    params: any;
    sortCategory: string;
    filterCheckBox: any;
    @Input() dataEvents: any;

    constructor(
		public dialog: MatDialog,
		public _DomSanitizer: DomSanitizer,
		public googleAnalyticsService: GoogleAnalyticsEventsService,
        private dataEventsService: DataEventsService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private loaderService: LoaderService,
        private filterService: AmassFilterService,
        private principal: Principal
    ) {
       /*  this.dataEvents = []; */
		this.itemsPerPage = ITEMS_PER_PAGE;
		this.page = 0;
		this.itemsFrom = 0;
		this.links = {
			last: 0
		};
		this.predicate = 'id';
		this.reverse = true;
		this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
		this.currentFilter = '';
		this.currentSorting = '';
    }

    ngOnInit() {
        this.dialog.closeAll();
       console.log(this.dataEvents);
       // this.loaderService.display(true);
        this.loader = true;
        this.completenessTooltip = [];
        this.sortedIndex = '';
        this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
        this.activatedRoute.queryParams.subscribe(params => {
            this.params = params;
           /*  if (params && !this.isAuthenticated()) {
                this.router.navigate(['events']);
            } */
        });

    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    logoutUsers(events: any) {
        console.log(events);
            this.openSignUp();
            this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    upcomingEvents(events) {
        console.log(events);
        this.router.navigate(['/events']);
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    upcomingEventsHeader() {
        this.router.navigate(['/events']);
    }

    openSignUp() {
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        });
        return;
    }

    loadAll() {
        this.limitedEvents = '';
		this.loader = true;
        let queryBody = {};
        if (this.isAuthenticated()) {
        if (this.currentSorting && this.currentFilter && this.currentSearch) {
            const requestBody = bob.requestBodySearch();
        if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
            requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
        } else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
            for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
                requestBody.sort(bob.sort(this.currentSorting.recommendedSorting[i]['key']).order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')).size(20);
            }
        }
        const queryJSON = requestBody.toJSON();
            queryJSON['query'] = {
                query_string: this.currentSearch
            };
            queryJSON['filter'] = this.currentFilter;
            queryBody = queryJSON;
        } else if (this.currentSorting && this.currentFilter) {
            const requestBody = bob.requestBodySearch();
        if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
            requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
        } else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
            for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
                requestBody.sort(bob.sort(this.currentSorting.recommendedSorting[i]['key']).order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')).size(20);
            }
        }
        const queryJSON = requestBody.toJSON();
            queryJSON['filter'] = this.currentFilter;
            queryBody = queryJSON;
        } else if (this.currentFilter && this.currentSearch) {
            queryBody = {
                size: 20,
                query: {
                    query_string: this.currentSearch
                },
                filter: this.currentFilter['query']
            };
        } else if (this.currentSorting && this.currentSearch) {
            const requestBody = bob.requestBodySearch();
        if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
            requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
        } else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
            for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
                requestBody.sort(bob.sort(this.currentSorting.recommendedSorting[i]['key']).order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')).size(20);
            }
        }
        const queryJSON = requestBody.toJSON();
            queryJSON['query'] = {
                query_string: this.currentSearch
            };
            queryBody = queryJSON;
        } else {
            if (this.currentSearch) {
                queryBody = {
                    size: 20,
                    query: {
                        query_string: this.currentSearch
                    }
                };
            }
            if (this.currentFilter) {
                queryBody = {
                    size: 20,
                    filter: this.currentFilter['query']
                };
            }
            if (this.currentSorting) {
                const requestBody = bob.requestBodySearch();
                if (typeof this.currentSorting === 'object' && this.currentSorting['key'] !== 'recommended') {
                    requestBody.sort(bob.sort(this.currentSorting['key']).order(this.currentSorting['order'] === 'desc' ? 'desc' : 'asc')).size(20);
                } else if (this.currentSorting['type'] && this.currentSorting['type'] === 'recommended') {
                    for (let i = 0; i < this.currentSorting.recommendedSorting.length; i++) {
                        requestBody.sort(bob.sort(this.currentSorting.recommendedSorting[i]['key']).order(this.currentSorting.recommendedSorting[i]['order'] === 'desc' ? 'desc' : 'asc')).size(20);
                    }
                }
                const queryJSON = requestBody.toJSON();
                queryBody = queryJSON;
            }
        }
        } else {
            queryBody = {};
            this.dialog.closeAll();
        }
        if (this.page) {
            queryBody['from'] = this.itemsFrom;
        }
        if ( this.subscription) {
            this.subscription.unsubscribe();
        }
		/* this.subscription = this.dataEventsService.queryWithSearchFilter(queryBody).subscribe(
			(res: ResponseWrapper) => this.onSuccessFilter(res, res.headers),
			(res: ResponseWrapper) => this.onError(res)
		); */
		return;
	}

    loadPage(page) {
        this.limitedEvents = '';
		this.page = page;
        this.itemsFrom = (this.page === 0) ? 0 : (this.page * 20);
        this.loader = false;
		if (this.itemsFrom <= this.totalItems) {
            this.limitedEvents = '';
			if (this.page <= 4 && this.isAuthenticated()) {
				this.loadAll();
                this.limitedEvents = '';
                this.loaderInfinite = true;
                this.loader = false;
			} else if (this.page <= -1 && !this.isAuthenticated()) {
                this.loadAll();
                this.loaderInfinite = true;
                this.loader = false;
            } else {
                if (!this.isAuthenticated()) {
                    this.limitedLogoutUsersEvent = true;
                    this.loaderInfinite = false;
                    this.loader = false;
                } else {
                    this.limitedLogoutUsersEvent = false;
                    this.loaderInfinite = false;
                    this.limitedEvents = 'Maximum number of entries listed. Please refine your search/filtering criteria to display the most appropriate entries.';
                }
			}

		}
    }

    eventProfile(events: any) {
        console.log(events);
        this.openSignUp();
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
    onKeydown(event: any) {
        if (!this.principal.isAuthenticated()) {
           this.openSignUp();
        } else {
            this.googleAnalyticsService.emitEvent('Events', 'Search Submitted', 'Events List - Search - Textbox');
        }
    }
    /* private onSuccessFilter(data, headers) {
		this.loader = false;
        this.loaderInfinite = false;
        this.loaderService.display(false);
		this.totalItems = data['hits'] ? data['hits'].total : 0;
		this.links.last = Math.ceil(this.totalItems / 20);
		if (!this.page) {
			this.dataEvents = [];
		}
		if (data['hits'] && data['hits'].hits) {
			for (let i = 0; i < 3; i++) {
                const eventsObj = data['hits'].hits[i]._source;
                this.dataEvents.push(eventsObj);
				this.completenessTooltip.push(data['hits'].hits[i]._source.completeness + '% ' + 'percent of this event\'s detail that has been filled in');
            }
		}
		if (this.totalItems === 0 && this.currentSearchQuery && !this.reSubmitValues) {
			this.reSubmitValues = true;
			setTimeout(() => {
				this.search(this.currentSearchQuery);
			}, 3000);
		} else {
			this.reSubmitValues = false;
		}
    } */

   /*  private onError(error) {
		this.loader = false;
		this.loaderInfinite = false;
    } */

    trackByRecordID(index, dataEvent) {
		return dataEvent.recordID;
    }

    defaultFilterChanges(event) {
        console.log(event);
        if (event === true) {
            console.log(event);
            this.genericFilter.datepickerValueChanged([this.currentDate, this.maxDate], 'startTime', 'datepicker', this.minDate, this.maxDate, 0);
            this.currentDate = new Date();
             this.minDate = new Date(this.currentDate.getFullYear() - 2, this.currentDate.getMonth(), this.currentDate.getDate());
            this.maxDate = new Date(this.currentDate.getFullYear() + 5, this.currentDate.getMonth(), this.currentDate.getDate());
            this.minUpdatedDate = new Date(1970, 10, 1);

        }
    }

}
