import { BaseEntity } from './../../shared';

export class DataHomeArticle implements BaseEntity {
    constructor(
        public id?: number,
        public articleuniquekey?: string,
        public recordID?: string,
        public title?: string,
        public url?: string,
        public publishedDate?: any,
        public content?: any,
        public contentHTML?: any,
        public summary?: any,
        public author?: string,
        public publisher?: string,
        public summaryHTML?: any,
        public relevancy?: number,
        public purpose?: string,
        public purposeType?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public authorOrganization?: BaseEntity,
        public authorDataProvider?: BaseEntity,
        public relevantProviders?: BaseEntity[],
        public emailText?: string,
        public linkedInText?: string,
        public twitterText?: string
    ) {
    }
}
