import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { DomSanitizer } from '@angular/platform-browser';

import { DataHomeArticle } from './genric-home-article-model';
import { DataArticleService } from '../../entities/data-article/data-article.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper, GenericFilterListModel, GenericFilterModel, ShareButtonsModel, StateStorageService } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { AmassFilterService } from '../../shared/genericfilter/filters.service';
import { LoaderService } from '../../loader/loaders.service';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
const bob = require('elastic-builder');
import { ApplyAccountComponent } from '../../account/apply/apply.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SigninDialogComponent } from '../../dialog/signindialog.component';

@Component({
	selector: 'jhi-home-data-article',
	templateUrl: './genric-home-article.component.html',
	styleUrls: ['./genric-home-article.component.css', '../../shared/share-buttons/share-buttons.scss']
})
export class HomeArticleComponent implements OnInit, OnDestroy {

	/* dataArticles: DataHomeArticle[];
	currentAccount: any; */
	eventSubscriber: Subscription;
	itemsPerPage: number;
	links: any;
	page: any;
	predicate: any;
	queryCount: any;
	subscription: any;
	reverse: any;
	totalItems: number;
	itemsFrom: number;
	currentFilter: string;
	currentSearch: any;
	currentSearchQuery: string;
	currentSorting: any;
	sortingArray = [];
    sortedIndex: any;
    params: any;
	paramFilter: string;
    paramID: string;
    loader: any;
    loaderInfinite: any;
    filters: GenericFilterModel[];
    statusFilter: any;
    isAdmin: boolean;
    sortCategory: string;
    filterCheckBox: any;
    limitedProviders: any;

    isSortByBarCollapsed: boolean;
    currentUrl: any;
    shareButtons: ShareButtonsModel[];
    emailText: string;
    logoutUsers: boolean;
    logoutUserClick: boolean;
    mdDialogRef: MatDialogRef<any>;
    @Input() dataArticles: any;

	constructor(
		private dataArticleService: DataArticleService,
		private alertService: JhiAlertService,
		private dataUtils: JhiDataUtils,
		private eventManager: JhiEventManager,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private principal: Principal,
        public _DomSanitizer: DomSanitizer,
        private filterService: AmassFilterService,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private loaderService: LoaderService,
        public dialog: MatDialog,
        private stateService: StateStorageService
	) {
		/* this.dataArticles = []; */
		this.itemsPerPage = ITEMS_PER_PAGE;
		this.itemsFrom = 0;
		this.page = 0;
		this.links = {
			last: 0
		};
		this.predicate = 'id';
		this.reverse = true;
		this.currentSearchQuery = '';
		this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
		this.currentSorting = '';
        this.isSortByBarCollapsed = true;
        /* this.principal.identity().then(account => {
            this.currentAccount = account;
        }); */
	}

	loadAll() {
        let queryBody = {};
		if (this.page) {
			queryBody['from'] = this.itemsFrom;
		}
		if ( this.subscription ) {
            this.subscription.unsubscribe();
        }
		/* this.subscription = this.dataArticleService.elasticSearch(queryBody).subscribe(
			(res: ResponseWrapper) => this.onSuccess(res, res.headers),
            (res: ResponseWrapper) => this.onError(res)
		); */
		return;
	}

	reset() {
		this.page = 0;
		/* this.dataArticles = []; */
        this.loadAll();
	}
    isAuthenticated() {
        return this.principal.isAuthenticated();
    }
    searchClear(events: any) {
        if (this.isAuthenticated()) {
            this.loader = true;
            this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
            console.log(events);
        }
    }
    articleTitle(events: any) {
      console.log(events);
      this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    articleLink(url) {
            window.open(url, '_blank');
    }
    unrestricted() {
            this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
                width: '570px'
            });
    }

    logoutUsersArticle(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        });
    }
    shareArticle(events) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
	ngOnInit() {
        console.log(this.dataArticles);
        if (this.isAuthenticated()) {
            this.filterService.setAuthorisedUrl();
        }
       // this.filterService.setAuthorisedUrl();
        this.dialog.closeAll();
        this.currentUrl = this.router.url;
        this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
       // this.loaderService.display(true);
        this.loader = false;
        this.currentSorting = {key: 'publishedDate', order: 'desc'};
		this.activatedRoute.queryParams.subscribe(params => {
            this.params = params;
            /* if (params && params['filter'] && params['id'] && !this.isAuthenticated()) {
                console.log(params);
                this.router.navigate(['news']);
            } */
			if (params['filter'] && params['id']) {
				this.paramFilter = params['filter'];
				this.paramID = params['id'];
            }
        });

        this.sortCategory = 'Articles';
        this.filterCheckBox = 'Articles';
		this.sortedIndex = '';
		this.loadAll();
        this.registerChangeInDataArticles();

        this.filters = [];
        const dataTopicFilterArray = [];
        const dataPurposeFilterArray = [];
        const dataPurposeTypeFilterArray = [];
    }

	ngOnDestroy() {
		this.eventSubscriber.unsubscribe();
	}
	trackId(index: number, item: DataHomeArticle) {
		return item.id;
	}

	byteSize(field) {
		return this.dataUtils.byteSize(field);
	}

	openFile(contentType, field) {
		return this.dataUtils.openFile(contentType, field);
	}
	registerChangeInDataArticles() {
		this.eventSubscriber = this.eventManager.subscribe('dataArticleListModification', response => this.reset());
	}
    recentNews(events) {
        console.log(events);
        this.router.navigate(['/news']);
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
}
