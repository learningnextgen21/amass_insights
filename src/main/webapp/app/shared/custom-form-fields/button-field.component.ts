import { Component, OnInit } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
  selector: 'jhi-formly-button',
  template: `
  <div class="ui-fluid">
    <p-button [label]="to.label" [type]="to.attributes && to.attributes['btnType']" [styleClass]="field.className" (onClick)="handleClick($event)"></p-button>
   </div>
  `,
  styleUrls: ['./custom-fields.css']
})
export class FormlyFieldButton extends FieldType implements OnInit {
  handleClick($event) {
    if(this.to['click']) {
      this.to.click($event); // To pass the event, using that event can do some operations after clicking the button.
    }
  }
  ngOnInit() {

  }
}
