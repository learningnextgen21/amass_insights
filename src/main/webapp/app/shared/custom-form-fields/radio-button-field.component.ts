import { FieldType } from '@ngx-formly/core';
import { Component, OnInit } from '@angular/core';
import { Principal } from '../auth/principal.service';

@Component({
    selector: 'jhi-formly-radio',
    template: `
    <div class="ui-g">
    <div class="ui-g-12" *ngFor="let option of to.options">
    <p-radioButton [name]="key" [formControl]="formControl" [value]="option.value" [(ngModel)]="modelKey" (onClick)="onClick($event)"></p-radioButton>
   <span [innerHTML]="option.label"></span>
    </div>
    </div>
    `
})
export class FormlyRadioButtonFieldComponent extends FieldType implements OnInit {
    public modelKey: any = {};
    constructor(private principal: Principal) {
        super();
    }

    ngOnInit() {
        this.modelKey = this.nestedObjectByString(this.model, this.key);
        this.setNestedObjectValue(this.key, this.modelKey);
    }

    onClick(event) {
        this.setNestedObjectValue(this.key, this.modelKey);
        this.principal.unsavedPopup$.next(true);
        if (event) {
            if(this.to.click) {
            this.to.click(this);
            }
        }
        if (event) {
            if(this.to.change) {
                this.to.change(this);
            }
        }
    }

    nestedObjectByString = function(o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        const a = s.split('.');
        for (let i = 0, n = a.length; i < n; ++i) {
            const k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    }

    setNestedObjectValue(path, value) {
        let schema = this.model;  // a moving reference to internal objects within obj
        const pList = path.split('.');
        const len = pList.length;
        for (let i = 0; i < len - 1; i++) {
            const elem = pList[i];
            if (!schema[elem] ) {
                schema[elem] = {};
            }
            schema = schema[elem];
        }

        schema[pList[len - 1]] = value;
        delete this.model[pList[len - 1]];
        this.model[pList[len - 1]] = schema[pList[len - 1]];
    }
}
