import { Component, OnInit } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
@Component({
    selector: 'jhi-formly-datepicker',
    template: `
    <div class="ui-fluid">
    <p-calendar [(ngModel)]="model[key]" dataType="string" [maxDate]="maxDate">
    </p-calendar>
    </div>
  `,
})
export class FormlyDatePickerFieldComponent extends FieldType implements OnInit {
   maxDate: any = '';
    constructor() {
        super();
    }

    // change(event) {
    //     console.log('Event', this.dp.transform(event,'yyyy-MM-ddTHH:mm:ss.sssZ'));
    // }

    ngOnInit() {
        //console.log(event);
        console.log(this.model);
        console.log(this.model[this.key]);
        // console.log(this.formControl);
        const currentDate = new Date();
        this.maxDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
        //console.log('Max Date', this.maxDate);
    }
}
