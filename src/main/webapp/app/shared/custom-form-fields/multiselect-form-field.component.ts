import { FieldType } from '@ngx-formly/core';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AmassFilterService } from '../../shared/genericfilter/filters.service';
import { DataProviderService } from '../../entities/data-provider/data-provider.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Principal } from '../auth/principal.service';

@Component({
    selector: 'jhi-formly-multiselect',
    template: `
	<div class="ui-fluid">
    <p-multiSelect [options]="to.options" [(ngModel)]="model[key]" [formControl]="formControl"
     filter="true" (keydown)="searchValue($event)" (onChange)="onChange($event)" [defaultLabel]="to.placeholder || 'Choose'" [panelStyleClass]="field.className">
    <ng-template let-listItem pTemplate="item" *ngIf="to.attributes && to.attributes['template']">
        <img *ngIf="listItem.img" [src]="this._DomSanitizer.bypassSecurityTrustUrl('data:image/png;base64,' + listItem.img)" onerror="this.style.display='none'"
        style="width:24px;display:inline-block;vertical-align:middle"/>
        <div style="font-size:14px;float:right;">{{listItem.label}}</div>
    </ng-template>
    <ng-template let-listItem pTemplate="item" *ngIf="!to.attributes || !to.attributes['template']">
        <span [matTooltip]="listItem.label">{{listItem.label}}</span>
    </ng-template>
        <p-footer *ngIf="!filterChanges && hidesection && to.attributes['lookupmodule']">
            <button *ngIf="!newfield" (click)="addItem($event)">+ Add <span [ngStyle]="{'padding': '0 5px'}">{{this.field.templateOptions.label}}</span></button>
            <span *ngIf="newfield"><input type="text" width="75px" [(ngModel)]="newvalues"></span>
            <button type="button" *ngIf="newfield" (click)="addNewField(newvalues)">+ Add<span [ngStyle]="{'padding': '0 5px'}">{{this.field.templateOptions.label}}</span></button>
        </p-footer>
    </p-multiSelect>
	</div>
    `,
    styleUrls: ['./custom-fields.css']
})

export class FormlyMultiSelectFieldComponent extends FieldType {
    newvalues: string = '';
    addField: boolean = false;
    newfield: boolean = false;
    lookupcode: any = '';
    hidesection: boolean = false;
    filterChanges: number = 0;
    providerId: number = 0;
    @ViewChild('multiselect') any;
    constructor(
        public _DomSanitizer: DomSanitizer,
        private filterService: AmassFilterService,
        private multiselect: ElementRef,
        private dataProviderService: DataProviderService,
        private snackBar: MatSnackBar,
        private principal: Principal
        ) {
        super();
    }

    onChange(event) {
    //    console.log(event);
        this.principal.unsavedPopup$.next(true);
        if (event) {
            if(this.to.change) {
            this.to.change(this);
            }
        }
    }
    searchValue(event) {    // This implementation is used to add new data in Multiselect field.
        //this.to.change(this);
        const liItems = this.multiselect.nativeElement.querySelectorAll('.ui-multiselect-items.ui-multiselect-list li');
        const filtered = [].filter.call(liItems, function(el) {
            const style = window.getComputedStyle(el);
            return (style.display !== 'none');
          });
          this.filterChanges = filtered.length;
      //    console.log(this);
     //     console.log(this.model.id);
          this.providerId = this.model.id;
        this.lookupcode =  this.field.templateOptions.attributes.lookupmodule;
        if (!this.filterChanges) {
            this.hidesection = true;
        }
    }
    addItem(event) {
        this.newfield = true;
    }
    addNewField(event) {
        const name = event;
        this.to.change(this);
        this.filterService.getGeographicMultiselect(event, this.lookupcode).subscribe(data => {
            if (this.lookupcode) {
                this.hidesection = false;
                this.newvalues = '';
            }
            const value = 'New ' + event + ' saved';
            this.snackBar.open(value, 'Close', {
               duration: 10000,
            });
        },response => this.processError(response, name));

        if (this.lookupcode === 'provider_tag') {
            this.dataProviderService.tagMultiselect(event, this.providerId).subscribe(data => {
                const tags = data;
                const value = 'New ' + event + ' saved';
                this.snackBar.open(value, 'Close', {
                  duration: 10000,
                });
            },response => this.processError(response, name));
        }
    }

    private processError(response, name) {
        console.log('RESPONSE',response);
        if (response.status === 409 && response.error === 'Lookupcode already in use') {
            const value =  'This '+ name + ' already exists';
            this.snackBar.open(value, 'Close', {
                duration: 5000,
              });
        }
    }
}
