import { Component, OnInit } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { Principal } from '../auth/principal.service';

@Component({
    selector: 'jhi-formly-checkbox',
    template: `
        <div class="ui-g">
            <ng-container *ngIf="to.attributes && to.attributes['multiple'] === 'true'">
                <div class="ui-g-12" *ngFor="let option of to.options">
                    <p-checkbox [name]="key" [value]="option.value" [(ngModel)]="modelKey" [inputId]="option.label" [required]="modelKey && modelKey.length ? false : true" (onChange)="change($event)">
                    </p-checkbox>
                    <span>{{option.label}}</span>
                </div>
            </ng-container>
            <ng-container *ngIf="to.attributes && to.attributes['multiple'] === 'false'">
                <div class="ui-g-12">
                    <p-checkbox [(ngModel)]="modelKey" [inputId]="modelKey" binary="true" (onChange)="change($event)"></p-checkbox>
                </div>
            </ng-container>
        </div>
    `,
    styleUrls: ['./custom-fields.css']
})
export class FormlyFieldCheckboxComponent extends FieldType implements OnInit {
    public modelKey: any = {};
    // public mandatory: boolean = false;

    constructor(private principal: Principal) {
        super();
    }

    change(event) {
        console.log(event, this.key, this.modelKey);
        this.principal.unsavedPopup$.next(true);
        this.setNestedObjectValue(this.key, this.modelKey);
        if (event) {
            if(this.to.change) {
                this.to.change(this);
            }
        }
    }

    ngOnInit() {
        setTimeout(() => {
            this.modelKey = this.nestedObjectByString(this.model, this.key);
            this.setNestedObjectValue(this.key, this.modelKey);
            console.log('CHECKBOX',this.model, this.modelKey, this.key);
            // this.modelKey=[];
            // this.modelKey = ['United States', 'China'];
            // this.modelKey = [2226, 2227, 2228];
        }, 5000);
    }

    nestedObjectByString = function(o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        const a = s.split('.');
        for (let i = 0, n = a.length; i < n; ++i) {
            const k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    }

    setNestedObjectValue(path, value) {
        let schema = this.model;  // a moving reference to internal objects within obj
        const pList = path.split('.');
        const len = pList.length;
        for (let i = 0; i < len - 1; i++) {
            const elem = pList[i];
            if (!schema[elem] ) {
                schema[elem] = {};
            }
            schema = schema[elem];
        }

        schema[pList[len - 1]] = value;
        delete this.model[pList[len - 1]];
        this.model[pList[len - 1]] = schema[pList[len - 1]];
    }
}
