import { FieldType } from '@ngx-formly/core';
import { Component, ViewChild, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-formly-slider',
    template: `
    <div class="ui-fluid">
    <div><input type="text" pInputText [(ngModel)]="modelKey" name="sliderValue" class="ui-inputtext" (change)="onChange($event)" />
    </div>
    <div style="margin-top: 10px;margin-bottom: 5px;">
    <p-slider [(ngModel)]="modelKey" #slider [min]="to.attributes['min']" [max]="to.attributes['max']"
    [step]="to.attributes['step']"></p-slider>
    </div>
	</div>
    `,
    styleUrls: ['./custom-fields.css']
})
export class FormlySliderFieldComponent extends FieldType implements OnInit {
    public modelKey: any;
    @ViewChild('slider') slider: any;
    sliderValue: any;
    constructor() {
        super();
    }

    ngOnInit() {
        this.modelKey = this.nestedObjectByString(this.model, this.key);
        this.setNestedObjectValue(this.key, this.modelKey);
    }

    onChange(event) {
        if (event && event.target) {
            this.slider.value = event.target.value;
        }
    }

    nestedObjectByString = function(o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        const a = s.split('.');
        for (let i = 0, n = a.length; i < n; ++i) {
            const k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    }

    setNestedObjectValue(path, value) {
        let schema = this.model;  // a moving reference to internal objects within obj
        const pList = path.split('.');
        const len = pList.length;
        for (let i = 0; i < len - 1; i++) {
            const elem = pList[i];
            if (!schema[elem] ) {
                schema[elem] = {};
            }
            schema = schema[elem];
        }

        schema[pList[len - 1]] = value;
        delete this.model[pList[len - 1]];
        this.model[pList[len - 1]] = schema[pList[len - 1]];
    }

}
