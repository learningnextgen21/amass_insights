import { FieldType } from '@ngx-formly/core';
import { Component, OnInit } from '@angular/core';
import { Principal } from '../auth/principal.service';

@Component({
    selector: 'jhi-formly-editor',
    template: `
    <div>
    <p-editor [attr.id]="id" name="p-name" [(ngModel)]="model[key]" (onTextChange)="change($event)" [formControl]="formControl" [style]="{'height': ''}" *ngIf="showEditor">
    <p-header>
        <span class="ql-formats">
            <button class="ql-bold" aria-label="Bold"></button>
            <button class="ql-italic" aria-label="Italic"></button>
            <button class="ql-underline" aria-label="Underline"></button>
        </span>
        <span class="ql-formats">
        <select title="Text Color" class="ql-color" defaultValue="rgb(0, 0, 0)">
        </select>
        <span class="ql-format-separator"></span>
        <select title="Background Color" class="ql-background" defaultValue="rgb(255, 255, 255)">
        </select>
        </span>
        <span class="ql-formats">
            <button class="ql-link"></button>
            <!-- <button class="ql-direction"></button> -->
        </span>
        <span class="ql-formats">
            <button class="ql-list" value="ordered"></button>
            <button class="ql-list" value="bullet"></button>
            <button class="ql-align" value=""></button>
            <button class="ql-align" value="center"></button>
            <button class="ql-align" value="right"></button>
            <button class="ql-align" value="justify"></button>
        </span>
    </p-header>
    </p-editor>
    <ng-container *ngIf="showValidation"><span style="color: red">This field has a maximum length of {{to.attributes?.maximumLength}} characters. Please shorten this text to fit under this length.</span></ng-container>
    </div>
    `
})
export class FormlyEditorFieldComponent extends FieldType implements OnInit {
    showEditor: boolean = false;
    showValidation: boolean = false;
    modelValue: any;

    constructor(private principal: Principal) {
        super();
    }

    change(event) {
        // console.log(event);
        this.principal.unsavedPopup$.next(true);
        if (event) {
            this.to.change(this);
        }
        if(event && event.textValue && event.textValue.length > this.to?.attributes?.maximumLength) {
            // const myTextbox = document.getElementById(this.id);
            // myTextbox.addEventListener('keypress', function(event) {
            //     event.preventDefault();
            // }, false);
            this.showValidation = true;
        } else if(event && event.textValue && event.textValue.length <= this.to?.attributes?.maximumLength) {
            // const myTextbox = document.getElementById(this.id);
            // myTextbox.addEventListener('keypress', function(event) {
            //     event.preventDefault();
            // }, true);
            this.showValidation = false;
        }
    }

    ngOnInit() {
        if (this.key === 'longDescription') {
            this.showEditor = true;
        }
        if (this.key === 'keyDataFields' || this.key === 'productDetails' || this.key === 'useCasesOrQuestionsAddressed') {
            setTimeout(() => {
                this.showEditor = true;
            }, 28000);
        }
    }
}
