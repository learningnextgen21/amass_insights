import { FieldType } from '@ngx-formly/core';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Principal } from '../auth/principal.service';

@Component({
    selector: 'jhi-formly-autocomplete',
    template: `
	<div class="ui-fluid">
    <p-autoComplete [(ngModel)]="modelKey" [suggestions]="to.options" forceSelection="true"
    (completeMethod)="change($event)" (onSelect)="onselect($event)" (onBlur)="onblur($event)" [dropdown]="to.attributes['dropdown'] ? true : false" [minLength]="to.attributes['minLength'] || '3'"
    [multiple]="to.attributes['multiple'] || false" [field]="to.attributes['field'] || 'label'" [formControl]="formControl" [inputStyleClass]="field.className">
    <ng-template let-listItem pTemplate="item" *ngIf="to.attributes && to.attributes['template']">
        <div class="ui-helper-clearfix" style="border-bottom:1px solid #D5D5D5">
            <img *ngIf="listItem.img" [src]="this._DomSanitizer.bypassSecurityTrustUrl('data:image/png;base64,' + listItem.img)" onerror="this.style.display='none'"
            style="width:32px;display:inline-block;margin:5px 5px 5px 5px; float: left;"/>
            <div style="margin:10px 10px 0 0">{{listItem[to.attributes['field']]}}</div>
        </div>
    </ng-template>
    </p-autoComplete>
	</div>
    `,
    styleUrls: ['./custom-fields.css']
})

export class FormlyAutoCompleteFieldComponent extends FieldType implements OnInit {
    public modelKey: any = {};
//    public orgID: any;
    private selected: boolean = false;
    constructor(public _DomSanitizer: DomSanitizer, private principal: Principal) {
        super();
    }
    onselect(event) {
        //console.log('Select',event);
        if (event) {
            this.selected = true;
            this.principal.unsavedPopup$.next(true);
            if(this.to['click']) {
                this.to.click(event); // passing the event to the particular autocomplete in all the forms.
                this.selected = true;
            }
        }
    }

    change(event) {
        this.to.change(this);
        this.setNestedObjectValue(this.key, this.modelKey);
        const evt = event;
        if (evt && evt.query) {
            evt.query = (evt.query).toLowerCase();
        }
        setTimeout(() => {
         this.to.change(evt);  // To pass the event after 2 seconds to list out the related datas.
        }, 2000);
    }


    onblur(event) {
        const evt = event;
        evt['selectedVal'] = this.selected;
        if (this.to['blur']) {
            this.to.blur(evt);      // To blur the event when the user is not selected in the list.
            this.selected = false;
        }
    }

    ngOnInit() {
        this.selected = false;
        this.modelKey = this.nestedObjectByString(this.model, this.key);
        this.setNestedObjectValue(this.key, this.modelKey);
    }

    nestedObjectByString = function(o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        const a = s.split('.');
        for (let i = 0, n = a.length; i < n; ++i) {
            const k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    }

    setNestedObjectValue(path, value) {
        let schema = this.model;  // a moving reference to internal objects within obj
        const pList = path.split('.');
        const len = pList.length;
        for (let i = 0; i < len - 1; i++) {
            const elem = pList[i];
            if (!schema[elem] ) {
                schema[elem] = {};
            }
            schema = schema[elem];
        }

        schema[pList[len - 1]] = value;
        delete this.model[pList[len - 1]];
        this.model[pList[len - 1]] = schema[pList[len - 1]];
    }
}
