import { Component, OnInit } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { DataProviderService } from 'app/entities/data-provider';

@Component({
  selector: 'jhi-formly-fieldset',
  template: `
  <div class="row">
    <!-- <p-fieldset><p-header>Edited {{ model[key] }}</p-header>{{ model[key] }}</p-fieldset> -->
    <div class="col-sm-2">

    </div>
    <div class="col-sm-10">
        <fieldset>
            <span *ngIf="!arrData && !objData" style="color: red;font-weight: bold;">{{ modelData }}</span>
            <span *ngIf="objData" style="color: red;font-weight: bold;">{{ modelData }}</span>
            <ng-container *ngIf="arrData">
                <li *ngFor="let item of newArrData;" style="display: inline;padding-right: 10px;"><span style="color: red;font-weight: bold;">{{ item }}</span></li>
            </ng-container>
        </fieldset>
    </div>
   </div><br>
   `,
  styleUrls: ['./custom-fields.css']
})
export class FormlyFieldsetFormComponent extends FieldType implements OnInit {

    public editedModel: any;
    public modelData: any;
    public newArrData: any[]=[];
    public newArrObj: any[]=[];
    public arrData: boolean = false;
    public objData: boolean = false;
    public pendingEdits: boolean = false;

    constructor(private dataProviderService: DataProviderService) {
        super()
    }

  ngOnInit() {
    this.dataProviderService.recordUpdated$.subscribe(result => {
        if(result === true) {
        this.pendingEdits = true;
        }
    });
    // if (this.pendingEdits === true) {
        setTimeout(() => {
            // console.log(this.model);
            this.editedModel = this.model['editedData'];
            console.log('Edited Model', this.editedModel);
            this.modelData = this.editedModel ? this.editedModel[this.key] : '';
            // console.log(this.editedModel[this.key]);
            const newArr = [];
            if(this.editedModel && this.editedModel[this.key]) {
                if(Array.isArray(this.editedModel[this.key])) {
                    // console.log('New Array', this.editedModel[this.key], this.key);
                    this.arrData = true;
                    this.newArrData = this.editedModel[this.key];
                    // newArr.forEach(element => {
                    //     if(element.desc) {
                    //         this.newArrData.push(element.desc);
                    //     } else if(element.lookupcode) {
                    //         this.newArrData.push(element.lookupcode);
                    //     }
                    // });
                } else if(typeof(this.editedModel[this.key]) === 'object' && this.editedModel[this.key] !== null) {
                    const obj = this.editedModel[this.key];
                    // console.log(obj);
                } else if(typeof(this.editedModel[this.key]) === 'boolean') {
                    const freeTrialAvailable = this.editedModel[this.key];
                    // console.log('Free', freeTrialAvailable);
                    this.modelData = freeTrialAvailable;
                    // console.log('Free 1', this.modelData);
                }
            }
        }, 10000);
    //}
  }
}
