import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FieldType } from '@ngx-formly/core';
import { DataProviderService } from 'app/entities/data-provider/data-provider.service';
import { Principal } from '../auth/principal.service';

@Component({
    selector: 'jhi-formly-fileUpload',
    template: `
    <div class="ui-fluid">
    <p-fileUpload #fubauto name="fileUploads" customUpload="true" [accept]="to.attributes && to.attributes['accept'] ? to.attributes['accept'] : false" maxFileSize="100000000" (uploadHandler)="onUpload($event, fubauto)" chooseLabel="Browse" [panelStyleClass]="field.className" multiple="multiple" invalidFileSizeMessageDetail="Maximum file upload size is 100MB.">
    <ng-template pTemplate="content" *ngIf="to.attributes && to.attributes['logoType']">
    <img *ngIf="to.attributes && to.attributes['role'] && logo !== ''" [src]="_DomSanitizer.bypassSecurityTrustUrl('data:image/png;base64,' + logo)" style="width:128px;max-height: 175px;" class="provider-logo"/>
        <ul *ngIf="uploadedFiles.length">
            <li *ngFor="let file of uploadedFiles">{{file.name}} - Logo uploaded successfully</li>
        </ul>
    </ng-template>
    <ng-template pTemplate="content" *ngIf="to.attributes && to.attributes['fileType']">
        <ul *ngIf="uploadedFiles.length">
            <li *ngFor="let file of uploadedFiles">{{file.name}} - File uploaded successfully</li>
        </ul>
    </ng-template>
    </p-fileUpload>
    </div>
  `,
})

export class FormlyFileUploadFieldComponent extends FieldType implements OnInit {
    uploadedFiles: any[] = [];
    logo: string = '';

    constructor(
        public _DomSanitizer: DomSanitizer,
        public dataProviderService: DataProviderService,
        public principal: Principal
    ) {
        super();
    }
    onUpload(event, fubauto) {
        this.principal.unsavedPopup$.next(true);
        this.uploadedFiles = event.files;
        this.to.change(event.files);
        fubauto.clear();
    }

    ngOnInit() {
        setTimeout(() => {
            console.log('Logo',this.model);
            console.log(this.formControl);
            this.logo = this.model && this.model.logo && this.model.logo.logo ? this.model.logo.logo : '';
        }, 7000);
        this.dataProviderService.changeLogoUpload$.subscribe(event => {
			console.log('event', event);
            this.logo = event;
		});
    }
}
