import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { Principal } from '../auth/principal.service';

@Component({
    selector: 'jhi-formly-textarea',
    template: `
    <div class="ui-fluid">
    <textarea [attr.id]="id" [(ngModel)]="modelKey" [formControl]="formControl" (change)="change($event)" [rows]="2" [cols]="100" pInputTextarea style="min-height: 54px">
    </textarea>
    <ng-container *ngIf="showValidation"><span style="color: red">This field has a maximum length of {{to.attributes?.maximumLength}} characters. Please shorten this text to fit under this length.</span></ng-container>
    </div>
  `,
})
export class FormlyTextAreaFieldComponent extends FieldType {
    public modelKey: any = {};
    showValidation: boolean = false;
    constructor(private principal: Principal) {
        super();
    }

    change(event) {
        console.log(event);
        this.principal.unsavedPopup$.next(true);
        if (event) {
            this.to.change(this);
        }
        if(event && event.target && event.target.textLength > this.to?.attributes?.maximumLength) {
            this.showValidation = true;
        } else if(event && event.target && event.target.textLength <= this.to?.attributes?.maximumLength) {
            this.showValidation = false;
        }
    }

    ngOnInit() {
        this.modelKey = this.nestedObjectByString(this.model, this.key);
        this.setNestedObjectValue(this.key, this.modelKey);
    }

    nestedObjectByString = function(o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        const a = s.split('.');
        for (let i = 0, n = a.length; i < n; ++i) {
            const k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    }

    setNestedObjectValue(path, value) {
        let schema = this.model;  // a moving reference to internal objects within obj
        const pList = path.split('.');
        const len = pList.length;
        for (let i = 0; i < len - 1; i++) {
            const elem = pList[i];
            if (!schema[elem] ) {
                schema[elem] = {};
            }
            schema = schema[elem];
        }

        schema[pList[len - 1]] = value;
        delete this.model[pList[len - 1]];
        this.model[pList[len - 1]] = schema[pList[len - 1]];
    }
}
