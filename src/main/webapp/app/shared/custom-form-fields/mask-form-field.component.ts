import { FieldType } from '@ngx-formly/core';
import { Component, OnInit } from '@angular/core';
import { Principal } from '../auth/principal.service';

@Component({
    selector: 'jhi-formly-mask',
    template: `
	<div class="ui-fluid">
    <p-inputMask [mask]="to.attributes['mask']" [(ngModel)]="modelKey" [formControl]="formControl" (onComplete)="onComplete(modelKey)" (keypress)="change($event)" [placeholder]="to.placeholder"></p-inputMask>
	</div>
    `,
    styleUrls: ['./custom-fields.css']
})
export class FormlyMaskFieldComponent extends FieldType implements OnInit {
    public modelKey: any = {};
    constructor(private principal: Principal) {
        super();
    }

    ngOnInit() {
        this.modelKey = this.nestedObjectByString(this.model, this.key);
        this.setNestedObjectValue(this.key, this.modelKey);
    }

    onComplete(modelValue) {
        this.setNestedObjectValue(this.key, modelValue);
        this.to.change(this.field, modelValue);
    }

    change(event) {
        this.principal.unsavedPopup$.next(true);
        if (event) {
          this.to.keypress(this); // To pass this event to open up the button after entering a value in input field particularly in Provider Profile Form Page in Documents Section.
        }
    }

    nestedObjectByString = function(o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        const a = s.split('.');
        for (let i = 0, n = a.length; i < n; ++i) {
            const k = a[i];
            if (o && k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    }

    setNestedObjectValue(path, value) {
        let schema = this.model;  // a moving reference to internal objects within obj
        const pList = path.split('.');
        const len = pList.length;
        for (let i = 0; i < len - 1; i++) {
            const elem = pList[i];
            if (!schema[elem] ) {
                schema[elem] = {};
            }
            schema = schema[elem];
        }

        schema[pList[len - 1]] = value;
        delete this.model[pList[len - 1]];
        this.model[pList[len - 1]] = schema[pList[len - 1]];
    }

}
