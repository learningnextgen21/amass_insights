import { FieldType } from '@ngx-formly/core';
import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { Principal } from '../auth/principal.service';

@Component({
	selector: 'jhi-formly-dropdown',
	template: `
    <div class="ui-grid ui-grid-responsive ui-fluid">
        <div class="ui-grid-row">
            <div class="ui-grid-col-12">
                <p-dropdown [options]="to.options" [(ngModel)]="modelSelectedId" [autoWidth]="false" placeholder="Select"
                [showClear]="'true' || to.attributes['showClear']" (onChange)="change($event)" [panelStyleClass]="field.className"
                [styleClass]="field.fieldGroupClassName" [formControl]="formControl">

                    <ng-template let-item pTemplate="selectedItem">
                        <span [matTooltip]="item.label">{{ item.label}}</span>
                    </ng-template>

                    <ng-template let-listItem pTemplate="item">
                        <div class="item-drop1">
                            <span [matTooltip]="listItem.label">{{listItem.label}}</span>
                        </div>
                    </ng-template>

                </p-dropdown>
            </div>
        </div>
    </div>
    `,
    styleUrls: ['./custom-fields.css']
})

export class FormlyDropdownFieldComponent extends FieldType implements OnInit {
    modelKey: any = {};

    selectedValue: any;
    modelSelectedValue:any;  // to bind the selectedValue data
    modelSelectedId: any;

    constructor(private principal: Principal) {
        super();
    }

    change(event) {
        // console.log(event);
        if (event) {
        //     this.selectedValue['description'] = event.value ? event.value['label'] : '';
            this.principal.unsavedPopup$.next(true);
            if (this.to.change) {
                this.to.change(this);
            }
        }
      //  if (event.value !== null) {
           // this.setNestedObjectValue(this.key, this.modelKey);
     //        this.model[this.key] = event.value ? event.value['value'] : null;
      //  }
    }

    ngOnInit() {
        this.modelKey = this.nestedObjectByStringObject(this.model, this.key);
        if (typeof this.modelKey === 'object') {
            this.selectedValue = this.modelKey;
            console.log('selected value ', this.selectedValue);
         //newly added line
         if(this.selectedValue !== null){
          //  this.modelSelectedValue = this.selectedValue===null ? this.selectedValue=null : this.selectedValue.description || this. selectedValue.lookupcode;  // modelKey description or lookup
            this.modelSelectedValue = this.selectedValue.description || this. selectedValue.lookupcode;  // modelKey description or lookup
            this.modelSelectedId = this.key === 'permission' ? this.selectedValue.name : this.selectedValue.id ;
         }
            // end
        }
        else {
            this.selectedValue = this.model[this.nestedObjectByStringValue(this.model, this.key)];
          //newly added line
            if (this.selectedValue !== undefined && typeof this.selectedValue === 'object' && Object.entries(this.selectedValue).length > 0) {   //modelKey - selectedValue -  object type
                this.modelSelectedValue = this.selectedValue.description || this.selectedValue.lookupcode;
                this.modelSelectedId = this.selectedValue.id;
            }
            if ((this.selectedValue === undefined || this.selectedValue === null) || (this.selectedValue !== undefined && Object.entries(this.selectedValue).length === 0) ||  this.modelSelectedValue === '')

             {
                if (this.to.options) {
                    if(this.to.options !== undefined) {
                        this.selectedValue = this.to.options && this.to.options[0] ? this.to.options[0].value : null
                    }
                }
                // this.selectedValue = (this.to.options!== undefined) ? this.to.options[0].value : null // to set select option 0th value
                this.modelSelectedValue = null;
                this.modelSelectedId = (this.key === 'permission' ) ? this.to.options[1].value : (this.key === 'privacy' ) ? 'Public' : null;  // (privacy dropdwn- Public id is 582) by new  provider creation
            }
            else {
                this.modelSelectedValue = (this.modelSelectedValue === '' || this.modelSelectedValue === undefined) ? this.selectedValue : this.modelSelectedValue;   // selectedValue is string type
                this.modelSelectedId =  ((this.modelSelectedId ==='' || this.modelSelectedId=== undefined) &&  this.key === 'privacy') ? this.modelSelectedValue : this.modelSelectedId;
                this.modelSelectedId =  ((this.modelSelectedId ==='' || this.modelSelectedId=== undefined) && this.key === 'priority') ? this.modelSelectedValue : this.modelSelectedId;
                this.modelSelectedId =  ((this.modelSelectedId ==='' || this.modelSelectedId=== undefined) && this.key === 'investorClientBucket') ? this.modelSelectedValue : this.modelSelectedId;
                this.modelSelectedId =  ((this.modelSelectedId ==='' || this.modelSelectedId=== undefined) && this.key === 'mainAssetClass') ? this.modelSelectedValue : this.modelSelectedId;
                this.modelSelectedId =  ((this.modelSelectedId ==='' || this.modelSelectedId=== undefined) && this.key === 'dataUpdateFrequency') ? this.modelSelectedValue : this.modelSelectedId;
            }
            // end

        }
       // console.log( 'ngmodel- modelkey:' , this.model[this.key]);
       // console.log( 'modelSelectedValue:' , this.key, this.modelSelectedValue);


        if (!this.to.attributes) {
            this.to.attributes = {};
        }

        if (!this.to.attributes['label']) {
            this.to.attributes['label'] = 'label';
        }

        if (!this.to.attributes['field']) {
            this.to.attributes['field'] = 'label';
        }

        if (!this.to.attributes['showClear']) {
            this.to.attributes['showClear'] = 'true';
        }
    }

    /*ngAfterContentChecked() {
        if (this.selectedValue === undefined) {
            this.selectedValue = this.to.options[0].value;
        }
    }*/

    nestedObjectByStringValue = function(o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        const a = s.split('.');
        return a[0];
    }

    nestedObjectByStringObject = function(o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        const a = s.split('.');
        for (let i = 0, n = a.length; i < n; ++i) {
            const k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    }

    /* nestedObjectByString = function(o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        const a = s.split('.');
        for (let i = 0, n = a.length; i < n; ++i) {
            const k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    }

    setNestedObjectValue(path, value) {
        let schema = this.model;  // a moving reference to internal objects within obj
        const pList = path.split('.');
        const len = pList.length;
        for (let i = 0; i < len - 1; i++) {
            const elem = pList[i];
            if (!schema[elem] ) {
                schema[elem] = {};
            }
            schema = schema[elem];
        }

        schema[pList[len - 1]] = value;
        delete this.model[pList[len - 1]];
        this.model[pList[len - 1]] = schema[pList[len - 1]];
    } */
}
