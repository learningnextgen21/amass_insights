import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { Principal } from '../auth/principal.service';

@Component({
  selector: 'jhi-formly-input',
  template: `
    <div class="ui-fluid">
   <div *ngIf="to.attributes && to.attributes['placeholders']; else otherText ">
   <input [type]="type" [attr.id]="id" [placeholder]="to.attributes['placeholders']" pInputText [(ngModel)]="modelKey" [formControl]="formControl" size="118" (keypress)="change($event)" (keydown)="keydown($event)" [ngClass]="field.className"/>
</div>
<ng-template #otherText>
<input [type]="type" [attr.id]="id" pInputText [(ngModel)]="modelKey" [formControl]="formControl" size="118" (keypress)="change($event)" (keydown)="keydown($event)" [ngClass]="field.className"/>
</ng-template>
      </div>
  `,
  styleUrls: ['./custom-fields.css']
})

export class FormlyFieldCustomInput extends FieldType implements OnInit {
  public modelKey: any = {};
  constructor(private principal: Principal) {
    super();
}
  change(event) {
    if (event) {
        // console.log(event);
        this.to.keypress(this); // To pass this event to open up the button after entering a value in input field particularly in Provider Profile Form Page in Documents Section.
        this.principal.unsavedPopup$.next(true);
    }
  }

  keydown(event) {
    if (this.to.type === 'number') {
      if ([69, 187, 188, 189, 190].includes(event.keyCode)) {
        event.preventDefault();
      }
    }
  }

  get type() {
    return this.to.type || 'text';
  }

  ngOnInit() {
    this.modelKey = this.nestedObjectByString(this.model, this.key);
    this.setNestedObjectValue(this.key, this.modelKey);
    if (this.to?.maxLength) {
        // console.log(this.key, this.to?.maxLength.toString());
        // document.getElementById(this.id)?.setAttribute('maxlength', this.to?.maxLength.toString());
        setTimeout(() => {
            document.getElementById(this.id)?.setAttribute('maxlength', this.to?.maxLength.toString());
        }, 500);
    }
    if(this.key === 'maximumYearlyPrice') {
        console.log('Maximum', this.key);
        setTimeout(() => {
            document.getElementById(this.id)?.setAttribute('maxlength', this.to?.maxLength?.toString());
        }, 20000);
    }
    if(this.key === 'minimumYearlyPrice') {
        console.log('Maximum', this.key);
        setTimeout(() => {
            document.getElementById(this.id)?.setAttribute('maxlength', this.to?.maxLength?.toString());
        }, 20000);
    }
  }

nestedObjectByString = function(o, s) {
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    const a = s.split('.');
    for (let i = 0, n = a.length; i < n; ++i) {
        const k = a[i];
        if (k in o) {
            o = o[k];
        } else {
            return;
        }
    }
    return o;
}

setNestedObjectValue(path, value) {
    let schema = this.model;  // a moving reference to internal objects within obj
    const pList = path.split('.');
    const len = pList.length;
    for (let i = 0; i < len - 1; i++) {
        const elem = pList[i];
        if (!schema[elem] ) {
            schema[elem] = {};
        }
        schema = schema[elem];
    }

    schema[pList[len - 1]] = value;
    delete this.model[pList[len - 1]];
    this.model[pList[len - 1]] = schema[pList[len - 1]];
}
}
