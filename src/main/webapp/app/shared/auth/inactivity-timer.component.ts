import { Component, OnDestroy, OnInit, Inject, EventEmitter, Output , Input} from '@angular/core';
import { Subject, timer, Subscription } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { AuthServerProvider } from './auth-session.service';
import { Principal } from './principal.service';
import { Router } from '@angular/router';
import { SigninModalService } from 'app/dialog/signindialog.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

/* <h1>User logout in:  {{ (minutesDisplay) }}:{{ (secondsDisplay) && (secondsDisplay <=59) ? secondsDisplay : '00' }}
    </h1> */

@Component({
    selector: 'jhi-inactivity-timer',
    template: `
    <div></div>
  `,
    styles: []
})
export class InactivityTimerComponent implements OnDestroy, OnInit {
    minutesDisplay = 0;
    secondsDisplay = 0;

    endTime = 1;

    unsubscribe$: Subject<void> = new Subject();
    timerSubscription: Subscription;
    dialogRef: MatDialogRef<GenericConfirmDialogComponent>;
    @Output() onExpire = new EventEmitter();
    @Output() beforeSessionExpire = new EventEmitter();
    @Input() pageName: string;

    constructor(
        private authService: AuthServerProvider,
        private principal: Principal,
        private router: Router,
        private dialog: MatDialog
    ) {}

    ngOnInit() {
        // console.log('pageName',this.pageName);
        this.resetTimer();
        this.authService.userActionOccured.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
            if (this.timerSubscription) {
                // console.log('unsubscribed');
                this.timerSubscription.unsubscribe();
            }
            // console.log('called reset');
            this.resetTimer();
        });
    }

    ngOnDestroy() {
        this.timerSubscription.unsubscribe();
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    resetTimer(endTime: number = this.endTime) {
        const interval = 1000;
        const duration = endTime * 60 * 8;
        this.timerSubscription = timer(0, interval)
            .pipe(take(duration))
            .subscribe(
                value => {
                    // console.log("Started Reset Timer", value, duration, interval);
                    this.render((duration - +value) * interval)
                },
                err => { },
                () => {
                    if(this.pageName && this.pageName === 'list')
                    {
                        this.beforeSessionExpire.emit(true);
                        this.waitForUserAction();
                    }
                    else
                    {
                        // console.log('Opened Dialog');
                        this.openDialog();
                        // this.beforeSessionExpire.emit(true);
                        // this.waitForUserAction();
                    }
                }
            );
    }

    waitForUserAction(endTime: number = this.endTime) {
        const interval = 1000;
        const duration = endTime * 60 * 2;
        this.timerSubscription = timer(0, interval)
            .pipe(take(duration))
            .subscribe(
                value => {
                    // console.log("Waited for User Action", value, duration, interval);
                    this.render((duration - +value) * interval)
                },
                err => { },
                () => {
                    console.log('user no action expired');
                    this.onExpire.emit(true);
                    this.principal.inactivityFormSubmit$.next(true);
                    this.dialogRef.close();
                    this.dialog.closeAll();
                    this.resetTimer();
                }
            );
    }

    private render(count) {
        this.secondsDisplay = this.getSeconds(count);
        this.minutesDisplay = this.getMinutes(count);
        // console.log(this.secondsDisplay, this.minutesDisplay);
    }

    private getSeconds(ticks: number) {
        const seconds = ((ticks % 60000) / 1000).toFixed(0);
        // console.log(seconds);
        return this.pad(seconds);
    }

    private getMinutes(ticks: number) {
        const minutes = Math.floor(ticks / 60000);
        // console.log(minutes);
        return this.pad(minutes);
    }

    private pad(digit: any) {
        return digit <= 9 ? '0' + digit : digit;
    }

    private logout() {
        this.authService.logout().subscribe();
        this.principal.authenticate(null);
        this.router.navigate(['../']);
    }

    openDialog() {
        // console.log(this.dialogRef);
        if (this.dialogRef === null || this.dialogRef === undefined || this.dialogRef.componentInstance === null) {
            this.dialogRef = this.dialog.open(GenericConfirmDialogComponent, {
                width: '250px',
                disableClose: true
            });
            setTimeout(() => {
                this.timerSubscription.unsubscribe();
                this.waitForUserAction();
            }, 3000);

            // this.dialogRef.componentInstance.onConfirmAction.subscribe(result => {
            //     console.log(result);
            //     if (!result) {
            //         console.log('Resetted');
            //         this.resetTimer();
            //     }
            // });
            // this.dialogRef.afterClosed().subscribe(result=> {
            //     console.log('Result', result);
            //     if (!result) {
            //         console.log('Resetted');
            //         this.timerSubscription.unsubscribe();
            //         this.resetTimer();
            //     }
            // });
        }
    }
}

/**
 * @title Dialog Overview
 */
@Component({
  selector: 'jhi-generic-confirm-dialog',
  templateUrl: './generic-confirm-dialog.component.html'
})

export class GenericConfirmDialogComponent {

  onConfirmAction = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<GenericConfirmDialogComponent>) {}

  onNoClick(): void {
    this.dialogRef.close(false);
    //this.onConfirmAction.emit(false);
  }

  confirm() {
      this.onConfirmAction.emit(true);
  }

}
