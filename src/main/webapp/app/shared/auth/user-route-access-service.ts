import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { Principal } from '../';
import { LoginModalService } from '../login/login-modal.service';
import { StateStorageService } from './state-storage.service';
import { SigninModalService } from '../../dialog/signindialog.service';
import { AccountService } from 'app/shared/auth/account.service';
import { DataProviderService } from 'app/entities/data-provider';

@Injectable()
export class UserRouteAccessService implements CanActivate {

	constructor(private router: Router,
				private loginModalService: LoginModalService,
				private signinModalService: SigninModalService,
				private principal: Principal,
				private stateStorageService: StateStorageService) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> {

		const authorities = route.data['authorities'];
		if (!authorities || authorities.length === 0) {
			return true;
		}

		return this.checkLogin(authorities, state.url);
	}

	checkLogin(authorities: string[], url: string): Promise<boolean> {
		const principal = this.principal;
		return Promise.resolve(principal.identity().then(account => {

			if (account && principal.hasAnyAuthorityDirect(authorities)) {
				return true;
			}

			this.stateStorageService.storeUrl(url);
            console.log('URL', url, principal.hasAnyAuthorityDirect(['ROLE_PROVIDER']));
            if (url === '/mndatrial' || '/how-it-works-providers' || '/how-it-works-investors') {
                this.router.navigate(['accessdenied']).then(() => {
                    // only show the login dialog, if the user hasn't logged in yet
                    if (!account) {
                        // this.loginModalService.open();
                        this.signinModalService.openDialog();
                    }
                });
                return false;
            }
			return true;
		}));
	}
}

@Injectable()
export class ProviderRouteAccessService implements CanActivate {

	constructor(private router: Router,
				private signinModalService: SigninModalService,
				private principal: Principal,
                private account: AccountService,
				private stateStorageService: StateStorageService) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> {
        const interestTypes = route.data['interestTypes'];
        let allowAccess = true;
		if (!interestTypes || interestTypes.length === 0) {
			return true;
		}
		if (route && route.params && route.params['recordID']) {
            /* this.account.getProviderEdit(route.params['recordID']).subscribe(data => {
                if (data && data.recordUpdated) {
                    allowAccess = false;
                    this.router.navigate(['accessdenied']);
                }
            }, error => {
                allowAccess = true;
                return true;
            }); */
			return this.checkLogin(route.params['recordID'], state.url, interestTypes, allowAccess);
		}
	}

	checkLogin(params: string, url: string, interestTypes: number[], allowAccess?: boolean): Promise<boolean> {
		const dataProvider = this.account;
		const principal = this.principal;
		return Promise.resolve(dataProvider.findProviderByID(params).toPromise().then(data => {
                const provider = data['hits'].total && data['hits'].hits[0]._source ? data['hits'].hits[0]._source : null;
                const providerInterestTypes = provider ? provider.userProviderStatus : [];
                principal.identity().then(account => {
                    if (!allowAccess) {
                        this.stateStorageService.storeUrl(url);
                        this.router.navigate(['accessdenied']).then(() => {
                            if (!account) {
                                this.signinModalService.openDialog();
                            }
                        });
                        return false;
                    }
                    if ((providerInterestTypes && dataProvider.hasAnyInterestType(interestTypes, providerInterestTypes) && allowAccess)
                    || (account && account.authorities.indexOf('ROLE_ADMIN') !== -1)) {
                        return true;
                    }
                    this.stateStorageService.storeUrl(url);
                    this.router.navigate(['accessdenied']).then(() => {
                        if (!account) {
                            this.signinModalService.openDialog();
                        }
                    });
                }).catch(error => {
                    return false;
                });
                return true;
            }).catch(error => {
                principal.identity().then(account => {
                    this.stateStorageService.storeUrl(url);
                    this.router.navigate(['accessdenied']).then(() => {
                        if (!account) {
                            this.signinModalService.openDialog();
                            return false;
                        }
                    });
                });
                return false;
		}));
	}
}

@Injectable()
export class UserRouteAccessUnrestrictedService implements CanActivate {

	constructor(private router: Router,
				private loginModalService: LoginModalService,
				private signinModalService: SigninModalService,
				private principal: Principal,
				private stateStorageService: StateStorageService) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> {

		const authorities = route.data['authorities'];
		if (!authorities || authorities.length === 0) {
			return true;
		}

		return this.checkLogin(authorities, state.url);
	}

	checkLogin(authorities: string[], url: string): Promise<boolean> {
		const principal = this.principal;
		return Promise.resolve(principal.identity().then(account => {
            if(!this.principal.isAuthenticated() && url === '/providers') {
                this.router.navigate(['unproviders']);
            }
			return true;
		}));
	}
}
