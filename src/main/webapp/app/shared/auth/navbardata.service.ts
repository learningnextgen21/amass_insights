import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavbarDataService {

  private _navbarDataSource =  new Subject<any>();
  navbarData$ = this._navbarDataSource.asObservable();

  constructor() { }

  sendData(data: any) {
    this._navbarDataSource.next(data);
  }

}