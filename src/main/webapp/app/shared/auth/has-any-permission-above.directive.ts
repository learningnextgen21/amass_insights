import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { Principal } from './principal.service';

/**
 * @whatItDoes Conditionally includes an HTML element if current user has any
 * of the authorities passed as the `expression`.
 *
 * @howToUse
 * ```
 *     <some-element *jhiHasAnyPermissionAbove="100">...</some-element>
 *
 * ```
 */
@Directive({
	selector: '[jhiHasAnyPermissionAbove]'
})
export class HasAnyPermissionAboveDirective {

	private permission: number;

	constructor(private principal: Principal, private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {
	}

	@Input()
	set jhiHasAnyPermissionAbove(value: number) {
		this.permission = value;
		this.updateView();
		// Get notified each time authentication state changes.
		this.principal.getAuthenticationState().subscribe(identity => this.updateView());
	}

	private updateView(): void {
		this.principal.hasAnyPermissionAbove(this.permission).then(result => {
            console.log(result);
			this.viewContainerRef.clear();
			if (result) {
				this.viewContainerRef.createEmbeddedView(this.templateRef);
			}
		});
	}
}
