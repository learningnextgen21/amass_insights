import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AccountService  {
	constructor(private http: HttpClient) { }

	get(): Observable<any> {
		return this.http.get('api/account');
	}

	save(account: any): Observable<any> {
		return this.http.post('api/account', account);
	}

	findProviderByID(recordID: string): Observable<any> {
		return this.http.post('api/es/providersearch?detail=true', {
            '_source': {
				'exclude': [
					'ownerOrganization.description',
					'researchMethodsCompleted'
				],
				'include': [
					'assetClasses',
					'authoredArticles',
					'authoredArticles.publishedDate',
					'authoredArticles.purpose',
					'authoredArticles.summaryHTML',
					'authoredArticles.title',
					'authoredArticles.url',
					'categories',
					'collectionMethodsExplanation',
					'completeness',
					'consumerPartners.id',
					'consumerPartners.mainDataIndustry.dataIndustry',
					'consumerPartners.mainDataCategory.datacategory',
					'consumerPartners.logo.logo',
					'consumerPartners.ownerOrganization.name',
					'consumerPartners.shortDescription',
					'consumerPartners.providerName',
					'consumerPartners.recordID',
					'createdDate',
					'dataProvider',
					'dataRoadmap',
					'summary',
					'dataUpdateFrequency',
					'deliveryFrequencyNotes',
					'dataUpdateFrequencyNotes',
					'dateCollectionBegan',
					'dateCollectionRangeExplanation',
					'deliveryFormats',
					'deliveryMethods',
					'distributionPartners.id',
					'distributionPartners.mainDataIndustry.dataIndustry',
					'distributionPartners.mainDataCategory.datacategory',
					'distributionPartners.logo.logo',
					'distributionPartners.ownerOrganization.name',
					'distributionPartners.recordID',
					'distributionPartners.shortDescription',
					'distributionPartners.providerName',
					'documentsMetadata.description',
					'documentsMetadata.fileID',
					'documentsMetadata.fileName',
					'documentsMetadata.fileSize',
					'documentsMetadata.dateCreated',
					'documentsMetadata.dateModified',
					'documentsMetadata.dataResourcePurpose.purpose',
					'documentsMetadata.dataResourcePurposeType.purposeType',
					'features',
					'freeTrialAvailable',
					'geographicalFoci.description',
					'geographicalFoci.id',
					'historicalDateRangeEstimate',
					'id',
					'industries',
					'investor_clients',
					'scoreInvestorsWillingness.description',
					'investorTypes.id',
					'investorTypes.description',
					'keyDataFields',
					'logo.logo',
					'longDescription',
					'mainAssetClass',
					'mainDataCategory.datacategory',
					'mainDataCategory.id',
					'mainDataCategory.explanation',
					'mainDataIndustry.dataIndustry',
					'mainDataIndustry.id',
					'mainDataIndustry.explanation',
					'marketplaceStatus',
					'notes',
					'orgRelationshipStatus',
					'scoreOverall.description',
					'ownerOrganization',
					'potentialDrawbacks',
					'pricingModels',
					'productDetails',
					'providerName',
					'providerType.providerType',
					'providerType.id',
					'providerType.explanation',
					'providerPartners.id',
					'providerPartners.mainDataIndustry.dataIndustry',
					'providerPartners.mainDataCategory.datacategory',
					'providerPartners.logo.logo',
					'providerPartners.ownerOrganization.name',
					'providerPartners.providerName',
					'providerPartners.shortDescription',
					'providerTags',
					'providerPartners.recordID',
					'dataLanguagesAvailable',
					'sampleOrPanelSize',
					'deliveryFrequencies',
					'dataLicenseType',
					'dataLicenseTypeDetails',
					'useCasesOrQuestionsAddressed',
					'legalAndComplianceIssues',
					'publicCompaniesCovered',
					'scoreReadiness.description',
					'recordID',
					'relevantArticles',
					'securityTypes',
					'shortDescription',
					'sources',
					'scoreUniqueness.description',
					'uniqueValueProps',
					'updatedDate',
					'userPermission',
                    'scoreValue.description',
                    'deliveryMethod.lookupcode',
                    'deliveryFormat.lookupcode',
					'website',
					'relevantSectors',
					'scoreReadiness',
					'scoreUniqueness',
					'scoreInvestorsWillingness',
					'scoreValue',
                    'scoreOverall',
                    'deliveryMethods',
                    'deliveryFormats',
                    'deliveryFrequencies',
                    'freeTrialAvailable',
                    'link'
				]
			},
			'filter': {
				'query': {
					'match': {
						'recordID': recordID
					}
				}
			}
		});
    }

    getProviderEdit(recordID?: string): Observable<any> {
        return this.http.get('api/provider_profile_edit/' + recordID);
    }

	hasAnyInterestType(interestTypes: number[], providerInterestTypes: number[]): boolean {
		console.log(interestTypes);
		console.log(providerInterestTypes);
		if (!interestTypes || !providerInterestTypes) {
			return false;
		}

		for (let i = 0; i < interestTypes.length; i++) {
			if (providerInterestTypes.indexOf(interestTypes[i]) !== -1) {
				return true;
			}
		}

		return false;
	}
}
