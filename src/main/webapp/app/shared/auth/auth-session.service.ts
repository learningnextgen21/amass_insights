import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthServerProvider {

    _userActionOccured: Subject<void> = new Subject();
    get userActionOccured(): Observable<void> {
        return this._userActionOccured.asObservable();
    }

    notifyUserAction() {
        this._userActionOccured.next();
    }

    constructor(
        private http: HttpClient,
        private router: Router
    ) {}

    login(credentials): Observable<any> {
        const data = 'j_username=' + encodeURIComponent(credentials.username) +
            '&j_password=' + encodeURIComponent(credentials.password) +
            '&remember-me=' + credentials.rememberMe + '&submit=Login';
        const headers = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        return this.http.post('api/authentication', data, { headers });
    }

    logout(): Observable<any> {
        // logout from the server
        return this.http.post('api/logout', {});
    }
}
