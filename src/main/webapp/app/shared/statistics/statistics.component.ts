import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Principal } from '../index';
import { ApplyDialogService } from '../index';
import { GoogleAnalyticsEventsService } from '../googleanalytics/google-analytics-events.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApplyAccountComponent } from '../../account/apply/apply.component';
@Component({
	selector: 'jhi-amass-statistics',
    templateUrl: 'statistics.component.html',
    styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit, OnChanges {
	public providerCount: number;
	public categoryCount: number;
	public articleCount: number;
    public featureCount: number;
    public eventcount: number;
    public isAuthenticated: boolean;
    mdDialogRef: MatDialogRef<any>;
    count: any;
    activeEventData: any;
    activeArticleData: any;
    activeProviderData: any;
    mouseActive: boolean;
    @Output() countStats: boolean;
    @Input() sliderValue: any;
    @Output() activeDataList: EventEmitter<any> = new EventEmitter();
	constructor(
        private http: HttpClient,
        private principal: Principal,
        private applyDialog: ApplyDialogService,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private router: Router,
        public dialog: MatDialog,
    ) {
    }

	ngOnInit() {
        this.isAuthenticated = this.principal.isAuthenticated() ? this.principal.isAuthenticated() : false;

		this.http.get<number>('api/ur/providercount', {}).subscribe(data => {
			this.providerCount = data;
		});
		this.http.get<number>('api/ur/categorycount', {}).subscribe(data => {
			this.categoryCount = data;
		});
        this.http.get<number>('api/ur/articlecount', {}).subscribe(data => {
            this.articleCount = data;
        });
        this.http.get<number>('api/ur/featurecount', {}).subscribe(data => {
            this.featureCount = data;
        });
        this.http.get<number>('api/ur/eventcount', {}).subscribe(data => {
            this.eventcount = data;
        });
        this.count = '0';
      // this.count = this.sliderValue;
    }
    ngOnChanges() {
       // console.log('onchange....', this.sliderValue);
        this.count = this.sliderValue;
    }
    eventData(value) {
        this.count = '1';
        this.activeEventData = value;
        this.activeDataList.emit(this.activeEventData);
       // this.mouseActive = true;
    }
    eventOut(value) {
        setTimeout(() => {
            this.count = '';
        this.activeEventData = value;
        this.activeDataList.emit(this.activeEventData);
        }, 20000);
    }
    articleData(value) {
        this.count = '2';
        this.activeArticleData = value;
        this.activeDataList.emit(this.activeArticleData);
      //  this.mouseActive = true;
    }
    articleOut(value) {
        setTimeout(() => {
            this.count = '';
        this.activeArticleData = value;
        this.activeDataList.emit(this.activeArticleData);
        }, 20000);
    }
    providerData(value) {
        console.log(value);
        this.count = '3';
        this.activeProviderData = value;
        this.activeDataList.emit(this.activeProviderData);
    }
    providerOut(value) {
        console.log(value);
        setTimeout(() => {
            this.count = '';
        this.activeProviderData = value;
        this.activeDataList.emit(this.activeProviderData);
        }, 20000);
    }
    signUps() {
        //this.applyDialog.open();
        if(!this.isAuthenticated)
        {
            this.router.navigateByUrl('unproviders');

        }
    }
    signUp() {
        this.applyDialog.open();
    }
    article() {
        this.router.navigate(['news']);
       /*  this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '570px'
        }); */
    }
    events() {
        this.router.navigate(['events']);
       /*  this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '570px'
        }); */
    }
    triggerGETracking(label: string) {
        console.log(label);
        this.googleAnalyticsService.emitEvent('Data Point Counts', 'Popup Link Clicked',  label);
    }
}
