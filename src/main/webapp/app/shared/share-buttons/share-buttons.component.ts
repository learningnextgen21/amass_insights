import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'jhi-share-buttons',
    templateUrl: './share-buttons.component.html',
    styleUrls: [
        './share-buttons.scss'
    ]
})
export class AmassShareButtonsComponent implements OnInit {
    @Input() shareButtons: ShareButtonsModel[];

    constructor() { }

    ngOnInit() {
        console.log(this.shareButtons);
    }

}

export class ShareButtonsModel {
	constructor(
        public buttonTooltip?: string,
        public url?: string,
        public type?: string,
        public description?: any
	) {

    }
}
