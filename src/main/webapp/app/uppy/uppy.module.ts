import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UppyComponent } from './uppy/uppy.component';
import { UppyService } from './uppy.service';
import { UppyDialogComponent } from './uppy/uppy-dialog.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [UppyComponent],
    exports: [UppyComponent],
    providers: [UppyService, UppyDialogComponent]
})
export class UppyModule { }
