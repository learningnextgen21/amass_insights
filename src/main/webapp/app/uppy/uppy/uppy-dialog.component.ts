import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Field } from '@ngx-formly/core';
import { UppyService } from 'app/uppy';
import {filter, takeUntil} from 'rxjs/operators';
import { contains } from 'ramda';
import { Subject} from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'jhi-uppy-dialog',
    template: `
    <button type="button" class="close" aria-label="Close" (click)="closeUppyDialog()" [ngStyle]="{'margin-top': '-22px','margin-right': '-14px'}">
        <span aria-hidden="true">&times;</span>
    </button>
    <div>
        <jhi-uppy [plugins]="uppyPlugins" [on]="uppyEvent"></jhi-uppy>
    </div>
  `,
})
export class UppyDialogComponent implements OnInit, AfterViewInit {
    providerRecordID: string = '';
    uppyEvent = new Subject<[string, any, any, any]>();
	uppyPlugins = [
		['Dashboard', {
			target: '.DashboardContainer',
            replaceTargetContent: true,
            animateOpenClose: true,
            browserBackButtonClose: true,
            closeModalOnClickOutside: false,
            closeAfterFinish: false,
            inline: true,
            width: 550,
            height: 450,
			restrictions: {
				maxFileSize: 200000000,
				maxNumberOfFiles: 10,
				minNumberOfFiles: 1,
				allowedFileTypes: ['image/*', 'video/*']
			}
		}],
		['XHRUpload', {
			endpoint: '/api/ur/resources-multiple-file-upload?providerId=' + this.providerRecordID,
			formData: true,
			bundle: true,
			fieldName: 'fileUploads'
        }],
        // ['Url', {
        //     target: 'Dashboard',
        //     companionUrl: 'http://localhost:9000',
        // }],
        // ['Tus', { endpoint: 'https://master.tus.io/files/' }],
		// ['GoogleDrive', {
		// 	target: 'Dashboard',
        //     companionUrl: 'http://localhost:9000'
        // }],
        /*
		['Dropbox', { target: 'Dashboard', serverUrl: 'https://companion.uppy.io' }] */
	];
	onDestroy$ = new Subject<void>();

    constructor(private uppyService: UppyService, private matDialog: MatDialog) {
        this.uppyEvent.pipe(
			takeUntil(this.onDestroy$),
			filter(([ev]) => contains(ev, ['file-added'])),)
			.subscribe(
				([ev, data1, data2, data3]) => {
					/* console.log("Received '" + ev + "' event from instance 1", 'Upload complete');
					console.log(data1);
					console.log(data2);
					console.log(data3);
					console.log(ev); */
				},
				err => console.log(err),
				() => console.log('done')
			);
    }

    ngOnInit() {
        console.log('ID', this.providerRecordID);
        this.uppyPlugins = [
            ['Dashboard', {
                target: '.DashboardContainer',
                replaceTargetContent: true,
                animateOpenClose: true,
                browserBackButtonClose: true,
                closeModalOnClickOutside: false,
                closeAfterFinish: false,
                inline: true,
                width: 550,
                height: 450,
                restrictions: {
                    maxFileSize: 200000000,
                    maxNumberOfFiles: 10,
                    minNumberOfFiles: 1,
                    allowedFileTypes: ['image/*', 'video/*']
                }
            }],
            ['XHRUpload', {
                endpoint: '/api/ur/resources-multiple-file-upload?providerId=' + this.providerRecordID,
                formData: true,
                bundle: true,
                fieldName: 'fileUploads'
            }],
            // ['Url', {
            //     target: 'Dashboard',
            //     companionUrl: 'http://localhost:9000'
            // }],
            // ["Tus", { endpoint: 'https://master.tus.io/files/' }],
            // ['GoogleDrive', {
            //     target: 'Dashboard',
            //     companionUrl: 'http://localhost:9000',
            // }]
            /*,
            ['Dropbox', { target: 'Dashboard', serverUrl: 'https://companion.uppy.io' }] */
        ];
    }
    ngAfterViewInit() {
        const uppy = this.uppyService.uppy;
    }

    closeUppyDialog() {
		this.matDialog.closeAll();
	}
}