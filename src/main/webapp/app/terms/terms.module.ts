import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TERMS_ROUTE, TermsComponent } from './';

@NgModule({
	imports: [
		RouterModule.forRoot([ TERMS_ROUTE ], { useHash: true })
	],
	declarations: [
		TermsComponent,
	],
	entryComponents: [
	],
	providers: [

	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceTermsModule {}
