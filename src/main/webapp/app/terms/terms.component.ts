import { Component, OnInit, Inject } from '@angular/core';
import {WINDOW} from '../layouts/main/window.service';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';

@Component({
	selector: 'jhi-terms',
	templateUrl: './terms.component.html',
	styleUrls: [
		'terms.scss',
		'../../content/scss/amass-form.scss'
	]

})
export class TermsComponent implements OnInit {

	constructor(
        @Inject(WINDOW) private window: Window,
        public googleAnalyticsService: GoogleAnalyticsEventsService
	) {
	}

	ngOnInit() {

	}
    mailTo(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    privacyPolicy(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    anchorLink(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
	scrollTo(selector) {
		const element = document.getElementById(selector);

		element.scrollIntoView(true);
		// account for fixed header
		const scrolledY = this.window.scrollY;
		// reduce fixed height header
		if (scrolledY) {
			this.window.scroll(0, scrolledY - 75);
		}
	}
}
