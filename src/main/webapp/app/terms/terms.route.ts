import { Route } from '@angular/router';
import { TermsComponent } from './';

export const TERMS_ROUTE: Route = {
	path: 'terms',
	component: TermsComponent,
	data: {
		authorities: [],
		pageTitle: 'terms.title',
		breadcrumb: 'Terms Of Use',
		type: 'static'
	}
};
