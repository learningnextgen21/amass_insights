import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { PrivacyComponent } from './';

export const PRIVACY_ROUTE: Route = {
	path: 'privacy-policy',
	component: PrivacyComponent,
	data: {
		authorities: [],
		pageTitle: 'home.title',
		breadcrumb: 'Privacy Policy',
		type: 'static'
	}
};
