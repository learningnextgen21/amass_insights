import { Component, OnInit, Inject } from '@angular/core';

import {WINDOW} from '../layouts/main/window.service';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';

@Component({
	selector: 'jhi-privacy',
	templateUrl: './privacy.component.html',
	styleUrls: [
		'privacy.scss',
		'../../content/scss/amass-form.scss'
	]

})
export class PrivacyComponent implements OnInit {

	constructor(
        @Inject(WINDOW) private window: Window,
        public googleAnalyticsService: GoogleAnalyticsEventsService
	) {
	}

	ngOnInit() {

    }
    mailTo(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    contactUs(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    termsService(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    terms(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    anchorLinks(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }

	scrollTo(selector) {
		const element = document.getElementById(selector);

		element.scrollIntoView(true);
		// account for fixed header
		const scrolledY = this.window.scrollY;
		// reduce fixed height header
		if (scrolledY) {
			this.window.scroll(0, scrolledY - 75);
		}
	}
}
