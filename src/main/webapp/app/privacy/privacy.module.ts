import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PRIVACY_ROUTE, PrivacyComponent } from './';

@NgModule({
	imports: [
		RouterModule.forRoot([ PRIVACY_ROUTE ], { useHash: true })
	],
	declarations: [
		PrivacyComponent,
	],
	entryComponents: [
	],
	providers: [
		PrivacyComponent
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplacePrivacyModule {}
