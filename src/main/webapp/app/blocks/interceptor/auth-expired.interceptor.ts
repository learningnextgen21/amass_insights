import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { LoginService, SigninModalService, StateStorageService } from '../../shared/index';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class AuthExpiredInterceptor implements HttpInterceptor {

	constructor(private router: Router, private loginService: LoginService, private signinModalService: SigninModalService, private stateStorageService: StateStorageService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap(
                (event: HttpEvent<any>) => {},
                (err: any) => {
                    if (err instanceof HttpErrorResponse) {
                        if (err.status === 401 && (window.location.href.match(/\?/g) || []).length < 2 && !err.url.includes('/api/authentication')) {
                            const destination = this.stateStorageService.getDestinationState();
                            if (destination) {
                                const to = destination.destination;
                                const toParams = destination.params;
                                if (to.name === 'accessdenied') {
                                    this.stateStorageService.storePreviousState(to.name, toParams);
                                }
                            }
                            if (err.url && err.url.indexOf('/api/account') === -1) {
                                this.loginService.logout();
                                this.signinModalService.openDialog('Session expired! Please re-login.');
                            }
                            this.router.navigate(['/']);
                        }
                    }
                }
            )
        );
    }
}
