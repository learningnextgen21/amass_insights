import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRequestsComponent } from './user-requests.component';
import { UserRequestsDetailComponent } from './user-requests-detail.component';
import { UserRequestsDialogComponent } from './user-requests-dialog.component';
import { UserRequestsDeleteDialogComponent } from './user-requests-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class UserRequestsResolve implements CanActivate {

    constructor(private principal: Principal) { }

    canActivate() {
        return this.principal.identity().then((account) => this.principal.hasAnyAuthority(['ROLE_ADMIN']));
    }
}

@Injectable()
export class UserRequestsResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const userMgmtRequestsRoute: Routes = [
    {
        path: 'user-requests',
        component: UserRequestsComponent,
        resolve: {
            'pagingParams': UserRequestsResolvePagingParams
        },
        data: {
            pageTitle: 'userManagement.home.title'
        }
    },
    {
        path: 'user-requests/:login',
        component: UserRequestsDetailComponent,
        data: {
            pageTitle: 'userManagement.home.title'
        }
    }
];

export const userRequestsDialogRoute: Routes = [
    {
        path: 'user-requests-new',
        component: UserRequestsDialogComponent,
        outlet: 'popup'
    },
    {
        path: 'user-requests/:login/edit',
        component: UserRequestsDialogComponent,
        outlet: 'popup'
    },
    {
        path: 'user-requests/:login/delete',
        component: UserRequestsDeleteDialogComponent,
        outlet: 'popup'
    }
];
