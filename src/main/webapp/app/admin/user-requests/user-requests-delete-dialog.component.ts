import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { User, UserService } from '../../shared';
import { UserRequestsModalService } from './user-requests-modal.service';

@Component({
	selector: 'jhi-user-requests-mgmt-delete-dialog',
	templateUrl: './user-requests-delete-dialog.component.html'
})
export class UserRequestsMgmtDeleteDialogComponent {

	user: User;

	constructor(
		private userService: UserService,
		public activeModal: NgbActiveModal,
		private eventManager: JhiEventManager
	) {
	}

	clear() {
		this.activeModal.dismiss('cancel');
	}

	confirmDelete(login) {
		this.userService.delete(login).subscribe((response) => {
			this.eventManager.broadcast({ name: 'userListModification',
				content: 'Deleted a user'});
			this.activeModal.dismiss(true);
		});
	}
}

@Component({
	selector: 'jhi-user-requests-delete-dialog',
	template: ''
})
export class UserRequestsDeleteDialogComponent implements OnInit, OnDestroy {

	modalRef: NgbModalRef;
	routeSub: any;

	constructor(
		private route: ActivatedRoute,
		private userModalService: UserRequestsModalService
	) {}

	ngOnInit() {
		this.routeSub = this.route.params.subscribe((params) => {
			this.modalRef = this.userModalService.open(UserRequestsDeleteDialogComponent as Component, params['login']);
		});
	}

	ngOnDestroy() {
		this.routeSub.unsubscribe();
	}
}
