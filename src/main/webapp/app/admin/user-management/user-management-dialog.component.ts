import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserModalService } from './user-modal.service';
import { AmassFilterService, JhiLanguageHelper, User, UserService } from '../../shared';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig, FormlyTemplateOptions } from '@ngx-formly/core';
@Component({
    selector: 'jhi-user-mgmt-dialog',
    templateUrl: './user-management-dialog.component.html'
})
export class UserMgmtDialogComponent implements OnInit {

    user: User;
    languages: any[] = [];
	language: any[] = [];
    authorities: any[]=[];
    isSaving: Boolean;
    relatedProvidersForm = new FormGroup({});
	relatedProvidersOptions: FormlyFormOptions = {};
    basicModel: any = {};
    adminForm = new FormGroup({});
	adminModel: any = {};
    adminOptions: FormlyFormOptions = {};
    adminUserForm = new FormGroup({});
	adminUserModel: any = {};
    adminUserOptions: FormlyFormOptions = {};
	providerPartners: any[] = [];
	unlockedPartners:any[] =[];
	selectedValue: any[] = [];
	providerPaymentMethodsOffered: any[] = [];
	authoritiesValue: any[] = [];
    userAuthorities:any[]=[];
	valueChanged: boolean = false;
    formSubmitted: boolean = false;
	userId:boolean=false;
    adminExpansionPanel: boolean = false;
    adminUserExpansionPanel: boolean = false;
    relatedEntitiesPanel: boolean = false;
    alertMessage: string = '';
    alertType: string = '';
	tabIndex: number = 0;
	progressBarColor = 'primary';

    constructor(
        public activeModal: NgbActiveModal,
        private languageHelper: JhiLanguageHelper,
        private userService: UserService,
        private eventManager: JhiEventManager,
        private filterService: AmassFilterService,
		public dialog: MatDialog,

    ) {
      //this.userId=true;
	  this.authorities=[];
      this.adminExpansionPanel = true;
      this.adminUserExpansionPanel = false;
      this.relatedEntitiesPanel = false;
	}

    adminUserFields: FormlyFieldConfig[] = [
        {
			key: 'authorities',
			type: 'checkbox',
			//defaultValue: 'ROLE_SILVER',
			/* className: 'customSmallWidth-dropdown', */
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'User Role:',
				required: true,
				options:[{
					value: 8,
					label: 'Investor'
			},
			{

				value: 7,
				label: 'Provider'
		     }],
				/* attributes: {
					field: 'label',
				}, */
				attributes: {
                    // field: 'id',
                    multiple: 'true',
                    placeholderText: '',
                    labelTooltip: '',
                    descriptionTooltip: '',
                    placeholderTooltip: ''
                },
				change: (event: any) => {
					console.log('eve',event);
					/* let oldAuthorities =[];
					 oldAuthorities =this.user.authorities; */
					if(event){
						this.user.authorities = [];
						if(!event['modelKey'].includes(6))
						{
							event['modelKey'].push(6);

						}
						this.user.authorities = event['modelKey'];
						console.log('old',this.user.authorities);
					}

					/* const newArr = event['modelKey'];
                   this.user.authorities = newArr; */
				}
			},

		},
        {
			key: 'rolePermission',
			type: 'radioButton',
			//defaultValue: 582,
			/* className: 'customSmallWidth-dropdown', */
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'User Permission:',
				required: true,
				//description: 'Who should be able to see this data profile?',
				options: this.providerPaymentMethodsOffered,
			},
		},
    ]
    adminFields: FormlyFieldConfig[] = [
		{
            key: 'id',
            type: 'input',
			wrappers: ['form-field-horizontal'],
           // hideExpression: !this.userId,
			hideExpression: ((model: any, formState: any) => this.userId),
            templateOptions: {
                label: 'ID',
                fieldName: 'id',
				keypress: (event) => {
				}
            }
        },
        {
			key: 'login',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Login',
				placeholder: '',
				required: true,
				attributes: {

				},
                keypress: (event) => {
				},
                maxLength : 256,
				//change: this.basicInput
			},
		},

        {
			key: 'firstName',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'First Name:',
				placeholder: '',
				attributes: {

				},
                keypress: (event) => {
				},
                maxLength : 256,
				//change: this.basicInput
			},
		},
        {
			key: 'lastName',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Last Name:',
				placeholder: '',
				attributes: {

				},
                keypress: (event) => {
				},
                maxLength : 256,
				//change: this.basicInput
			},
		},
        {
			key: 'email',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Email',
				placeholder: '',
				required: true,
				attributes: {

				},
                keypress: (event) => {
				},
                maxLength : 256,
				//change: this.basicInput
			},
		},
		{
			key: 'langKey',
            type: 'dropdown',
            className: 'customSmallWidth-dropdown',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Language:',
                options: [{
					value: 'en',
					label: 'English'
			            },
			         {
				value: 'es',
				label: 'Espanol'
		        }
		           ],
				attributes: {
                    field: 'label'
                },
                //change: this.basicInput
			},
		},

    ];

     relatedProvidersFields: FormlyFieldConfig[];
    ngOnInit() {
		console.log('userValue',this.user);
		this.basicModel = this.user;
        this.basicModel.rolePermission = this.user?.rolePermission?.id;

		/* if(this.user && this.user.authorities){
		const iterator = this.user.authorities.values();
            for (let elements of iterator) {
                 console.log(elements);
           if (elements === 'ROLE_PROVIDER') {
      this.basicModel.authorities = elements;
                 } else if (elements === 'ROLE_INVESTOR') {
       this.basicModel.authorities = elements;
                 }
           }
		   console.log('auth',this.basicModel.authorities);
		} */
		if(!this.user?.id){
			this.userId=true;
		}
		this.loadValues();
        this.filterService.setAuthorisedUrl();
        this.isSaving = false;
        this.authorities = [];
        this.userService.authorities().subscribe((authorities) => {
            this.authorities = authorities;
        });
		if(this.user?.id){
		  this.userService.findUserValueById(this.user.id).subscribe((data) => {
            this.userAuthorities = data?.body?.authorities;
			this.basicModel.authorities= [];
			this.basicModel.authorities = this.userAuthorities;
			console.log('aut',this.basicModel.authorities, this.userAuthorities);
			if (this.basicModel.authorities) {
                const authorities = [];
                for (const obj in this.basicModel.authorities) {
                    if (obj && this.basicModel.authorities[obj].id !== 6) {
                        authorities.push(this.basicModel.authorities[obj].id)
                    }
                    console.log('Pushed', authorities);
                }
                delete this.basicModel.authorities;
                this.basicModel.authorities = authorities;
				this.authoritiesValue = this.basicModel.authorities

            }
			console.log('userauth',this.basicModel.authorities);
        });
	}
		this.valueChanged = false;
        this.filterService.getLookupCodes('USER_PERMISSION').subscribe(data => {
			const listItems = data && data['hits'] && data['hits']['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerPaymentMethodsOffered.push({

						value: listItems[i]._source.id,
						label: listItems[i]._source.lookupcode

				});
			}
		});
       /*  this.languageHelper.getAll().then((languages) => {
			for (let i = 0; i < languages.length; i++) {
				this.languages.push({
					value: languages,
					label: languages
				});
			}
			console.log('lang',this.languages);
        }); */
		this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
			console.log('lan',this.languages);
        });
		/* if(this.languages){
			for (let i = 0; i < this.languages.length; i++) {
				this.language.push({
					value: this.languages[i],
					label: this.languages[i]
				});
			}
		} */


    }
	loadValues(){
		if(this.user.id){
			this.userService.findUserProvider(this.user.id).subscribe(data =>{
				if(data){
					console.log('datavalue',data);
					this.user.providerPartners=data.body.providerPartners;
					this.user.unlockedPartners=data.body.unlockedPartners;
					 console.log('provider',this.user.providerPartners);
					if (this.user.providerPartners) {
					 const providerPartners = [];
					 for (const obj in this.user.providerPartners) {
						 if (obj) {
							 const partnerName = !this.user.providerPartners[obj].locked ? this.user.providerPartners[obj].providerName : 'Provider Locked';
							 providerPartners.push({
								 id: this.user.providerPartners[obj].id,
								 providerName: partnerName
							 });
						 }
					 }
					 delete this.user.providerPartners;
					 this.user.providerPartners = providerPartners;

					 if (this.user.unlockedPartners) {
						const unlockedPartners = [];
						for (const obj in this.user.unlockedPartners) {
							if (obj) {
								const partnerName = !this.user.unlockedPartners[obj].locked ? this.user.unlockedPartners[obj].providerName : 'Provider Locked';
								unlockedPartners.push({
									id: this.user.unlockedPartners[obj].id,
									providerName: partnerName
								});
							}
						}
						delete this.user.unlockedPartners;
						this.user.unlockedPartners = unlockedPartners;

					 this.basicModel=this.user;

				}
				this.relatedProvidersFields = [
					{
						key: 'unlockedPartners',
						type: 'autocomplete',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Data Providers Unlocked:',
							description: '',
							options: this.unlockedPartners,
							attributes:{
								field : 'providerName',
								'multiple': 'true',
								'minLength': '1'
							},
							change: (event: any) => {
								const queryObj = event;
								this.unlockedPartners = [];
								const selected = this.basicModel['unlockedPartners'];
								///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
								if(selected && selected.length) {
									this.selectedValue = selected.map(item => item.id);
								}
								if(queryObj.query) {
									this.filterService.getAllProviders(queryObj.query).subscribe(d => {
									const listItems = d && d['hits'] ? d['hits']['hits'] : [];
									for (let i = 0; i < listItems.length; ++i) {
										const item = listItems[i]._source;
										if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
											if (!this.selectedValue.includes(item.id)) {
												this.unlockedPartners.push({
													id: item.id,
													providerName: item.providerName,
													img: item.logo ? item.logo.logo : null
												});
											}
										}
									}
										this.relatedProvidersFields[0].templateOptions.options = this.unlockedPartners;
									});
								}
							},
							blur: (event: any) => {
								if (event && event.target && !event['selectedVal']) {
									event.target.value = '';
								}
							}
						},
					},
					{
						key: 'providerPartners',
						type: 'autocomplete',
						wrappers: ['form-field-horizontal'],
						templateOptions: {
							label: 'Data Providers Operated:',
							description: '',
							options: this.providerPartners,
							attributes:{
								field : 'providerName',
								'multiple': 'true',
								'minLength': '1'
							},
							change: (event: any) => {
								const queryObj = event;
								this.providerPartners = [];
								const selected = this.basicModel['providerPartners'];
								///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
								if(selected && selected.length) {
									this.selectedValue = selected.map(item => item.id);
								}
								if(queryObj.query) {
									this.filterService.getAllProviders(queryObj.query).subscribe(d => {
									const listItems = d && d['hits'] ? d['hits']['hits'] : [];
									for (let i = 0; i < listItems.length; ++i) {
										const item = listItems[i]._source;
										if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
											if (!this.selectedValue.includes(item.id)) {
												this.providerPartners.push({
													id: item.id,
													providerName: item.providerName,
													img: item.logo ? item.logo.logo : null
												});
											}
										}
									}
										this.relatedProvidersFields[1].templateOptions.options = this.providerPartners;
									});
								}
							},
							blur: (event: any) => {
								if (event && event.target && !event['selectedVal']) {
									event.target.value = '';
								}
							}
						},
					}
					
				];
			 }
			}
			 console.log('userpartner',this.user);
			});
		 }
		 else{
			this.relatedProvidersFields = [
				
				{
					key: 'unlockedPartners',
					type: 'autocomplete',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Data Providers Unlocked:',
						description: '',
						options: this.unlockedPartners,
						attributes:{
							field : 'providerName',
							'multiple': 'true',
							'minLength': '1'
						},
						change: (event: any) => {
							const queryObj = event;
							this.unlockedPartners = [];
							const selected = this.basicModel['unlockedPartners'];
							///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
							if(selected && selected.length) {
								this.selectedValue = selected.map(item => item.id);
							}
							if(queryObj.query) {
								this.filterService.getAllProviders(queryObj.query).subscribe(d => {
								const listItems = d && d['hits'] ? d['hits']['hits'] : [];
								for (let i = 0; i < listItems.length; ++i) {
									const item = listItems[i]._source;
									if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
										if (!this.selectedValue.includes(item.id)) {
											this.unlockedPartners.push({
												id: item.id,
												providerName: item.providerName,
												img: item.logo ? item.logo.logo : null
											});
										}
									}
								}
									this.relatedProvidersFields[0].templateOptions.options = this.unlockedPartners;
								});
							}
						},
						blur: (event: any) => {
							if (event && event.target && !event['selectedVal']) {
								event.target.value = '';
							}
						}
					},
				},
				{
					key: 'providerPartners',
					type: 'autocomplete',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Data Providers Operated:',
						description: '',
						options: this.providerPartners,
						attributes:{
							field : 'providerName',
							'multiple': 'true',
							'minLength': '1'
						},
						change: (event: any) => {
							const queryObj = event;
							this.providerPartners = [];
							const selected = this.basicModel['providerPartners'];
							///_that.selectedValue = selected && selected.length ? selected.map(item => item.id) : [];
							if(selected && selected.length) {
								this.selectedValue = selected.map(item => item.id);
							}
							if(queryObj.query) {
								this.filterService.getAllProviders(queryObj.query).subscribe(d => {
								const listItems = d && d['hits'] ? d['hits']['hits'] : [];
								for (let i = 0; i < listItems.length; ++i) {
									const item = listItems[i]._source;
									if (listItems[i]._source.providerName !== '' && listItems[i]._source.providerName !== 'RECORD_NULL' && listItems[i]._source.providerName !== 'PROVIDER_NULL') {
										if (!this.selectedValue.includes(item.id)) {
											this.providerPartners.push({
												id: item.id,
												providerName: item.providerName,
												img: item.logo ? item.logo.logo : null
											});
										}
									}
								}
									this.relatedProvidersFields[1].templateOptions.options = this.providerPartners;
								});
							}
						},
						blur: (event: any) => {
							if (event && event.target && !event['selectedVal']) {
								event.target.value = '';
							}
						}
					},
				}
			];
		 }
	}

    clear() {
        this.activeModal.dismiss('cancel');
    }
	openConfirmationDialog() {

		return false;
	}
	closeSignUpDialog() {
		console.log('called',this.valueChanged);
        this.dialog.closeAll();
    }

    save() {
        this.isSaving = true;
        console.log('user',this.user);
        if (this.user.id !== null) {
           // this.userService.update(this.user).subscribe((response) => this.onSaveSuccess(response), () => this.onSaveError());
        } else {
            //this.userService.create(this.user).subscribe((response) => this.onSaveSuccess(response), () => this.onSaveError());
        }
    }

    private onSaveSuccess(result, panelName) {
		console.log('res',result);
		if(result && result.body){
			this.basicModel.id=result.body.id;
		}
        this.eventManager.broadcast({ name: 'userListModification', content: 'OK' });
        this.alertType = 'success';
        if (panelName === 'Basic') {
            this.alertMessage = 'Basic Details saved successfully';
            this.isSaving = true;
            this.adminExpansionPanel = false;
            this.adminUserExpansionPanel = true;
            setTimeout(() => {
                this.isSaving = false;
            }, 3000);
        } else if (panelName === 'Admin') {
            this.alertMessage = 'Admin Details saved successfully';
            this.isSaving = true;
            this.adminUserExpansionPanel = false;
            this.relatedEntitiesPanel = true;
            setTimeout(() => {
                this.isSaving = false;
            }, 3000);
        } else if (panelName === 'Related Providers') {
            this.alertMessage = 'Related Providers Details saved successfully';
            this.isSaving = true;
            this.relatedEntitiesPanel = false;
            setTimeout(() => {
                this.isSaving = false;
                this.activeModal.dismiss(result);
            }, 2000);
        }
        // this.activeModal.dismiss(result);
    }
	submitProviderEdit(model, panelName?: string, currentPanel?: string, nextPanel?: string, tabIndex?: number, form?: string, skipRedirect?: boolean) {

      /*   console.log('users',this.user);
        console.log('model',model);
		this.authorities.push[model.authorities];
		model.authorities = this.authorities;
		this.user.authorities = this.authorities; */
		/* this.userAuthorities.push(model.authorities,'ROLE_USER');
		console.log('auth',this.userAuthorities);
		model.authorities=this.userAuthorities; */
		console.log('model',model,this.authoritiesValue);
		if(model.authorities === undefined){
			model.authorities = this.authoritiesValue;
		}
		if(model && model.authorities.includes(7) && model.authorities.includes(8)){
			model.authorities = ['ROLE_PROVIDER','ROLE_INVESTOR','ROLE_USER'];
		}
		else if(model && model.authorities.includes(7)){
			model.authorities = ['ROLE_PROVIDER','ROLE_USER'];
		}
		else if(model && model.authorities.includes(8)){
			model.authorities = ['ROLE_INVESTOR','ROLE_USER'];

	}
		this.user=model;
		console.log('user',this.user);
		if(model && model.id){
			this.userService.update(this.user).subscribe((response) => this.onSaveSuccess(response, panelName), () => this.onSaveError());
		}
		else{
			this.userService.create(this.user).subscribe((response) => this.onSaveSuccess(response, panelName), () => this.onSaveError());
		}

}
submitProviderPartner(model, panelName?: string, currentPanel?: string, nextPanel?: string, tabIndex?: number, form?: string, skipRedirect?: boolean){
	if(model.id){
		this.userService.createProvider(this.user).subscribe((response) => this.onSaveSuccess(response, panelName), () => this.onSaveError());
	}
}
formSave(events: any){

}
    private onSaveError() {
        this.alertType = 'danger';
        this.alertMessage = 'Form Failed to save!';
        this.isSaving = true;
    }
}

@Component({
    selector: 'jhi-user-dialog',
    template: ''
})
export class UserDialogComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private userModalService: UserModalService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['login'] ) {
                this.modalRef = this.userModalService.open(UserMgmtDialogComponent as Component, params['login']);
            } else {
                this.modalRef = this.userModalService.open(UserMgmtDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
