import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Log } from './log.model';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class LogsService {
    constructor(private http: HttpClient) { }

    changeLevel(log: Log): Observable<HttpResponse<any>> {
        return this.http.put('management/logs', log, { observe: 'response'});
    }

    findAll(): Observable<any> {
        return this.http.get('management/logs', { observe: 'response' });
    }
}
