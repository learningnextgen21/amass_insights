import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiTrackerService } from './../shared/tracker/tracker.service';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyFieldCustomInput } from 'app/shared/custom-form-fields/input-form-field.component';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { DatamarketplaceSharedModule } from '../shared';
import { FormlyFieldButton } from '../shared/custom-form-fields/button-field.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';


import {
	adminState,
	AuditsComponent,
	UserMgmtComponent,
	UserDialogComponent,
	UserDeleteDialogComponent,
	UserMgmtDetailComponent,
	UserMgmtDialogComponent,
	UserMgmtDeleteDialogComponent,
	LogsComponent,
	JhiMetricsMonitoringModalComponent,
	JhiMetricsMonitoringComponent,
	JhiHealthModalComponent,
	JhiHealthCheckComponent,
	JhiConfigurationComponent,
	JhiDocsComponent,
	AuditsService,
	JhiConfigurationService,
	JhiHealthService,
	JhiMetricsService,
	JhiTrackerComponent,
	LogsService,
	UserResolvePagingParams,
	UserResolve,
	UserModalService,
	UserRequestsResolvePagingParams,
	UserRequestsResolve,
	UserRequestsModalService,
	UserRequestsComponent,
	UserRequestsDeleteDialogComponent,
    	UserRequestsMgmtDeleteDialogComponent,
	UserRequestsDetailComponent
} from './';
import { UserRequestsDialogComponent, UserRequestsMgmtDialogComponent } from './user-requests/user-requests-dialog.component';
import { FormlyHorizontalWrapperComponent } from 'app/entities/data-provider/horizontal-wrapper';
export function maxlengthValidationMessage(err, field) {
    return `This field has a maximum length of  ${field.templateOptions.maxLength}  characters. Please shorten this text to fit under this length.`;
  }
  import {
    FormlyEditorFieldComponent,
    FormlyAutoCompleteFieldComponent,
    FormlyMultiSelectFieldComponent,
    FormlyDropdownFieldComponent,
    FormlyMaskFieldComponent,
    FormlySliderFieldComponent,
    FormlyRadioButtonFieldComponent,
    FormlyTextAreaFieldComponent,
}  from '../shared';
import { FormlyFieldCheckboxComponent } from 'app/shared/custom-form-fields/checkbox-form-field.component';
@NgModule({
	imports: [
		DatamarketplaceSharedModule,
		ReactiveFormsModule,
		RouterModule.forRoot(adminState, { useHash: true }),
		FormlyModule.forRoot({
            wrappers: [{ name: 'form-field-horizontal', component: FormlyHorizontalWrapperComponent  }],
            validationMessages: [
                { name: 'required', message: 'This field is required' },
                { name: 'maxlength', message: maxlengthValidationMessage },
            ],
            types: [{
                name: 'inputEditor', component: FormlyEditorFieldComponent
            }, {
                name: 'autocomplete', component: FormlyAutoCompleteFieldComponent
            }, {
                name: 'multiselect', component: FormlyMultiSelectFieldComponent
            },  {
                name: 'button', component: FormlyFieldButton
            }, {
                name: 'input', component: FormlyFieldCustomInput
            },
			{
                name: 'dropdown', component: FormlyDropdownFieldComponent
            },
			{
                name: 'checkbox', component: FormlyFieldCheckboxComponent
            }
		]
        }),
        FormlyBootstrapModule,
	],
	declarations: [
		AuditsComponent,
		UserMgmtComponent,
		UserDialogComponent,
		UserDeleteDialogComponent,
		UserMgmtDetailComponent,
		UserMgmtDialogComponent,
		UserMgmtDeleteDialogComponent,
		UserRequestsComponent,
		UserRequestsDialogComponent,
		UserRequestsDeleteDialogComponent,
        	UserRequestsMgmtDeleteDialogComponent,
		UserRequestsDetailComponent,
		UserRequestsMgmtDialogComponent,
		LogsComponent,
		JhiConfigurationComponent,
		JhiHealthCheckComponent,
		JhiHealthModalComponent,
		JhiDocsComponent,
		JhiTrackerComponent,
		JhiMetricsMonitoringComponent,
		JhiMetricsMonitoringModalComponent
	],
	entryComponents: [
		UserMgmtDialogComponent,
		UserMgmtDeleteDialogComponent,
		UserRequestsDialogComponent,
		UserRequestsDeleteDialogComponent,
		JhiHealthModalComponent,
		JhiMetricsMonitoringModalComponent,
	],
	providers: [
		AuditsService,
		JhiConfigurationService,
		JhiHealthService,
		JhiMetricsService,
		LogsService,
		JhiTrackerService,
		UserResolvePagingParams,
		UserResolve,
		UserModalService,
		UserRequestsResolvePagingParams,
		UserRequestsResolve,
		UserRequestsModalService
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceAdminModule {}
