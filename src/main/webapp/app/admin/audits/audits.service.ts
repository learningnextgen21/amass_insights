import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { createRequestOption } from 'app/shared';
import { Audit } from './audit.model';

@Injectable()
export class AuditsService {
    constructor(private http: HttpClient) {}

    query(req: any): Observable<any> {
        const params: HttpParams = createRequestOption(req);
        params.set('fromDate', req.fromDate);
        params.set('toDate', req.toDate);

        const requestURL = 'management/audits';

        return this.http.get(requestURL, {
            params,
            observe: 'response'
        });
    }
}
