import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class JhiConfigurationService {

    constructor(private http: HttpClient) {
    }

    get(): Observable<any> {
        return this.http.get('management/configprops');
    }

    getEnv(): Observable<any> {
        return this.http.get('management/env');
    }
}
