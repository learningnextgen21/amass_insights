import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';

import { LoginModalService, Principal,AuthServerProvider,StateStorageService,SigninModalService } from '../shared';
import { Router} from '@angular/router';

import { SigninDialogComponent } from '../dialog/signindialog.component';

import { ApplyAccountComponent } from '../account/apply/apply.component';
import { ContactService } from './contact.service';
import { NgForm } from '@angular/forms';
import { LoaderService } from '../loader/loaders.service';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';

@Component({
	selector: 'jhi-contact',
	templateUrl: './contact.component.html',
	styleUrls: [
		'contact.scss',
		'../../content/scss/amass-form.scss',
		'../home/home.scss'
	]

})
export class ContactComponent implements OnInit, AfterViewInit {
	modalRef: NgbModalRef;
	mdDialogRef: MatDialogRef<any>;
	contact: any;
	inquiryTypes = [];
	contactFields: any;
	validationError: any;
	error: any;
	responseMessage: any;
    formValid: boolean;
    recaptchaValidation: any;
    lat = 40.748822;
    lng = -74.006041;

	@ViewChild(MatInput) input;

    form = new FormGroup({});
    model: any = {};
    options: FormlyFormOptions = {};

    fields: FormlyFieldConfig[] = [
        {
            key: 'userName',
            type: 'input',
            templateOptions: {
                label: 'Name',
                required: true,
                keypress: (event) => {
				}
            },
            validation: {
                messages: {
                    required: 'Name is required'
                }
            },
        },
        {
            key: 'userEmail',
            type: 'input',
            templateOptions: {
                label: 'Email',
                type: 'email',
                pattern: '^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$',
                required: true,
                keypress: (event) => {
				}
            },
            validation: {
                messages: {
                    required: 'Email id is required',
                    pattern: 'Please enter valid email id'
                }
            },
        },
        {
            key: 'company',
            type: 'input',
            templateOptions: {
                label: 'Company',
                type: 'text',
                required: true,
                keypress: (event) => {
				}
            },
            validation: {
                messages: {
                    required: 'Company is required'
                }
            },
        },
        {
            key: 'requestTopic',
            type: 'select',
            defaultValue: 'General Inquiry',
            templateOptions: {
                label: 'Inquiry Type',

               /*  placeholder: 'Inquiry Type', */
               /*  required: true, */
                options: [
                    { value: 'General Inquiry', label: 'General Inquiry'},
                    { value: 'Help Finding Data', label: 'Help Finding Data' },
                    { value: 'Help Selling My Data', label: 'Help Selling My Data' },
                    { value: 'Press', label: 'Press' },
                    { value: 'Tech Support', label: 'Tech Support' },
                ],
            },
            /* validation: {
                messages: {
                    required: 'Inquiry Type is required'
                }
            }, */
        },
        {
            key: 'message',
            type: 'textarea',
            templateOptions: {
                label: 'Message',
                maxLength: 1000,
                required: true,
                keypress: event => {
                    if (event && event.model && event.model.message && event.model.message.length >= 1000) {
                        return false;
                    }
                }
            },
            validation: {
                messages: {
                    required: 'Message is required'
                }
            },
        },
    ];

	constructor(
		private principal: Principal,
		private loginModalService: LoginModalService,
		private eventManager: JhiEventManager,
		public dialog: MatDialog,
        private contactService: ContactService,
        private loaderService: LoaderService,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private stateService: StateStorageService,
        private authService: AuthServerProvider,
		private signinModalService: SigninModalService,
		private router: Router
	) {
	}

	ngOnInit() {
        this.loaderService.display(true);
		this.responseMessage = null;
		this.error = null;
		this.contactFields = {};
        this.formValid = false;
        this.recaptchaValidation = null;
		this.inquiryTypes = ['General Inquiry', 'Help Finding Data', 'Help Selling My Data', 'Press', 'Tech Support'];
        this.validationError = null;
    }

	ngAfterViewInit() {
        this.loaderService.display(false);
		/* this.input.focus();
		setTimeout(() => {
			this.input.focus();
		}, 1000); */
	}

	isAuthenticated() {
		return this.principal.isAuthenticated();
	}

	login() {
		this.modalRef = this.loginModalService.open();
	}

	openSigninDialog() {
		this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
			width: '400px'
		});
	}

	openApplyDialog() {
		this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '400px'
		});
	}

	contactSubmit(contactForm: NgForm) {
		if (contactForm.valid && this.formValid) {
			if (contactForm.controls.email.errors) {
				this.validationError = 'Please enter a valid email address.';
			} else {
				this.contactService.submit(this.contactFields).subscribe((res) => {
					this.error = null;
					this.responseMessage = res;
					this.validationError = null;
				}, (response) => {
					this.error = response;
					this.responseMessage = null;
					this.validationError = null;
				});
			}
		} else if (contactForm.controls.email.errors) {
			this.validationError = 'Please enter a valid email address.';
		} else if (!this.formValid) {
            this.validationError = 'Recaptcha verification expired. Please check the checkbox again.';
        } else {
			this.validationError = 'All required fields have not been filled out. Please refill and resend.';
		}

    }
    eventSubmit(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    cancel(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    eventMail(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    footerLink(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }

	closeResponse() {
		this.responseMessage = null;
		for (const item in this.contactFields) {
			if (this.contactFields.hasOwnProperty(item)) {
				this.contactFields[item] = null;
			}
		}
	}

	resolved(captchaResponse: string) {
        this.googleAnalyticsService.emitEvent('Contact Us', 'reCAPTCHA Completed', 'Contact Us - reCAPTCHA');
        if (captchaResponse !== null) {
            this.formValid = true;
            this.recaptchaValidation = 'valid';
            this.validationError = null;
        } else {
            this.formValid = false;
            this.recaptchaValidation = 'expired';
            this.validationError = '';
        }
    }

    submit() {
        if (this.form.valid) {
            if (this.recaptchaValidation === null) {
                this.validationError = 'Please complete CAPTCHA and resend.';
            } else if (this.recaptchaValidation === 'expired') {
                this.validationError = '';
            } else {
                this.contactFields = this.model;
                this.formSubmit();
            }
        } else {
            if (this.recaptchaValidation === null) {
                this.validationError = 'Please complete CAPTCHA and resend.';
            } else if (this.recaptchaValidation === 'expired') {
                this.validationError = '';
            }
        }
    }

    formSubmit() {
        this.contactService.submit(this.contactFields).subscribe(res => {
            this.error = null;
            this.responseMessage = res;
            this.validationError = null;
        }, response => {
            this.error = response;
            this.responseMessage = null;
            this.validationError = null;
        });
    }
    onExpireSession(event) {
		//console.log('eventcheck',event);
        if (event) {
			//console.log('eventcheck',event);
            //this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
				setTimeout(() => {
				this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
			}, 2000);
			});
        }
    }
	  beforeSessionExpires(event) {
        if (event) {
           // this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
}
}
