import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RecaptchaModule } from 'ng-recaptcha';

import { DatamarketplaceSharedModule } from '../shared';

import { CONTACT_ROUTE, ContactComponent } from './';
import { ContactService } from './contact.service';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { CommonModule } from '@angular/common';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { AgmCoreModule } from '@agm/core';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormlyModule.forRoot(),
        FormlyBootstrapModule,
        DatamarketplaceSharedModule,
        RouterModule.forRoot([CONTACT_ROUTE], { useHash: true }),
        RecaptchaModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD7411gR5UFC7nQF_jHNRNOuoaVqwzYphA'
          })
    ],
    declarations: [ContactComponent],
    entryComponents: [],
    providers: [ContactService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceContactModule {}
