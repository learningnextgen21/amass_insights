import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ContactService {
	constructor(private http: HttpClient) {

	}

	submit(req: any): Observable<any> {
		return this.http.post('api/ur/contactus', req );
	}
}
