import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { StripeComponent } from '.';

export const STRIPE_ROUTE: Route = {
	path: 'checkout',
	component: StripeComponent,
	data: {
		authorities: ['ROLE_USER'],
		pageTitle: 'home.title',
		breadcrumb: 'Checkout & Subscribe',
		type: 'static'
    },
    canActivate: [UserRouteAccessService]
};
