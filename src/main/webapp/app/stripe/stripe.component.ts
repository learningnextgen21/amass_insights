import { Component, OnInit, Inject,AfterViewInit,OnDestroy, ChangeDetectorRef} from '@angular/core';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';
import {WINDOW} from '../layouts/main/window.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import {AccordionModule} from 'primeng/accordion';
// import { StripeService, Elements, Element as StripeElement, ElementsOptions } from 'ngx-stripe';
import { StripeService } from 'ngx-stripe';
import {
    StripeCardElement,
    StripeElementsOptions
} from '@stripe/stripe-js';
import * as moment from 'moment';
import {Payment, IPayment} from './payment.model';
import { Observable } from 'rxjs';
import {PaymentService} from './payment.service';
import { UserService } from './../shared';
import { SigninModalService } from './../dialog/signindialog.service';
import { Principal,AuthServerProvider,StateStorageService} from './../shared';
import { LoginService} from './../shared/index';


@Component({
    selector: 'jhi-stripe',
    templateUrl: 'stripe.component.html',
    styleUrls: [
        './stripe.scss',
        '../home/home.css'
    ]
})
export class StripeComponent implements OnInit,AfterViewInit{
    stripeTest: FormGroup;
   // amount = 9.99;
    currencySymbol: string;
    currency = 'USD';
    payment: IPayment = new Payment(0, moment(), '', '', 0, 'Description test', false);
    moment: moment.Moment;

    elements: any;
    card: StripeCardElement;
    elementsOptions: StripeElementsOptions = {
        locale: 'en'
    };
    isSaving: boolean;
    checkout: any;
    amount: any;
    subscription: any;
    pay: string;
    check: any;
    isError: boolean;
    error: string;
    public price : boolean;
    pageName = 'list';
    //cardHandler = this.onChange.bind(this);

    constructor(
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private fb: FormBuilder,
        public router: Router,
        private stripeService: StripeService,
        protected paymentService: PaymentService,
        @Inject(WINDOW) private window: Window,
        private cd: ChangeDetectorRef,
        private userService: UserService,
        private signinModalService: SigninModalService,
        private loginService: LoginService, 
        private principal: Principal,
        private stateService: StateStorageService,
        private authService: AuthServerProvider,
        ) {
        this.checkout = {};
        this.check = {};
        }

    ngOnInit() {
        this.userService.userauthorities().subscribe((authorities) => {
            console.log('authorities',authorities);
           this.price = authorities;
           console.log('price',this.price);
           if(this.price)
           {
            this.router.navigateByUrl('/settings');
           }
        });
        this.paymentService.userCheckout().subscribe(data =>{
            console.log(data);
            if(data === null)
            {
                this.router.navigateByUrl('/upgrade');

            }
            else
            {
            this.checkout = data['user'];
            this.amount=data['amount'];
             this.subscription=data['subscription'];
            console.log(this.checkout);
            console.log('subscription',this.subscription);
            if(this.subscription === 'Starter')
            {
                this.pay = 'Activate Starter Subscription for $1,000/month';
            }
            else{
                this.pay = 'Activate Starter Subscription for $1,000/month';
            }
        }
        });
        this.stripeTest = this.fb.group({
            name: ['', [Validators.required]],
            email: ['', [Validators.required]],
            privacy: ['', [Validators.required]],
            //card: ['', [Validators.required]],

        });
       
        
      

        switch (this.currency) {
            case 'usd': {
                this.currencySymbol = '$';
                break;
            }
            case 'eur': {
                this.currencySymbol = '€';
                break;
            }
            // All supported currencies can be found here: https://stripe.com/docs/currencies
            default:
                this.currencySymbol = '$';
        }
        
    }
    /* ngAfterViewInit() {
        this.check.addEventListener('change',this.cardHandler);
    }
ngOnDestroy() {
    this.check.removeEventListener('change', this.cardHandler);
    this.check.destroy();
    }
    onChange({ error }) {
        if (error) {
          this.error = error.message;
        } else {
          this.error = null;
        }
        this.cd.detectChanges();
      } */

      ngAfterViewInit() {
        this.stripeService.elements({
            locale: 'en'
        }).subscribe(elements => {
            this.elements = elements;
            // Only mount the element the first time
            if (!this.card) {
                this.card = this.elements.create('card', {
                    style: {
                        base: {
                            iconColor: '#666EE8',
                            color: '#31325F',
                            lineHeight: '40px',
                            fontWeight: 300,
                         //   fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                            fontSize: '18px',
                            '::placeholder': {
                                color: '#CFD7E0'
                            }
                        },
                        /* invalid: {
                            color: '#fa755a',
                            iconColor: '#fa755a'
                          } */
                    },
                });
                this.card.mount('#card-element');
                this.check = this.card;
                this.check.addEventListener('change', function(event) {
                    console.log(1);
                    const displayError = document.getElementById('card-errors');
                    if (event.error) {
                      displayError.textContent = event.error.message;
                      console.log('error',displayError.textContent);
                    } else {
                      displayError.textContent = '';
                      console.log('error1',displayError.textContent);
        
                    }
                  });
            }
            
        });
      }
    protected onSaveSuccess() {
        this.isSaving = false;
      alert('Payment succeeded! You should receive an email confirmation shortly. After pressing OK you should see your subscription activated. Please email info@amassinsights.com if you have any issues. Thank you for your business.');
        this.router.navigateByUrl('/providers');

    }
    protected onSaveError(res) {
        this.isSaving = false;
        console.log('response',res);
        console.log(res.status);
        if(res.status == 400){
            alert('Payment method declined! Please use an alternate payment method or fix any mistakes in your payment method information. If you think this is an error, please email info@amassinsights.com.');
            
        }
       else if(res.status == 401 || res.status == 403)
        {
            alert('Payment failed! Please check your payment method information and update any mistakes. If you think this is an error, please email info@amassinsights.com.');
            this.router.navigate(['accessdenied']).then(() => {
                this.loginService.logout();
                this.signinModalService.openDialog('Session expired! Please re-login.');				
        });
        }
        else
        {
            alert('Payment failed! Please check your payment method information and update any mistakes. If you think this is an error, please email info@amassinsights.com.');
            
        }
    }


    protected subscribeToSaveResponse(result: Observable<HttpResponse<IPayment>>) {
        console.log('result',result);
        result.subscribe((res: HttpResponse<IPayment>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError(res));
    }

    buy(event: any) {
        console.log(event);
        const name = this.stripeTest.get('name').value;
        const email = this.stripeTest.get('email').value;
        console.log('Name', name);
        console.log('email', email);
        console.log('cardDdetails',this.card);
        this.stripeService.createToken(this.card, { name }).subscribe(result => {
            console.log('Result', result);
            if (result.token) {
                // Use the token to create a charge or a customer
                // https://stripe.com/docs/charges
                //console.log('token',result.token);
                this.payment.amount = this.amount * 100;
                this.payment.currency = this.currency;
                this.payment.token = result.token.id;
                this.payment.date = moment();
                this.payment.name = name;
                this.payment.email = email;
                console.log('Ranjithkumar', this.payment);
                this.subscribeToSaveResponse(this.paymentService.createPaymentCurrentUser(this.payment));
            } else if (result.error) {
                // Error creating the token
                console.log('Error creating the token!');
                console.log(result.error.message);
                this.error = result.error.message;

            }
        });
    }
    
        
    btnClick1 = function() {
        this.router.navigateByUrl('/terms');
}

btnClick2 = function() {
    this.router.navigateByUrl('/privacy-policy');
}
onExpireSession(event) {
    //console.log('eventcheck',event);
    if (event) {
        //console.log('eventcheck',event);
        //this.formSubmitted = true;
        this.authService.logout().subscribe(d => {
            this.stateService.storeUrl(null);
            this.principal.authenticate(null);
            this.router.navigate(['../../']);
            setTimeout(() => {
            this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
        }, 2000);
        }); 
    }
}
  beforeSessionExpires(event) {
    if (event) {
        //this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
    }
}
}
