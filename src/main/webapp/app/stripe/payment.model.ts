import { Moment } from 'moment';

export interface IPayment {
    id?: number;
    date?: Moment;
    token?: string;
    currency?: string;
    amount?: number;
    description?: string;
    capture?: boolean;
    receipt?: any;
    name?: string;
    email?: string;
}

export class Payment implements IPayment {
    constructor(
        public id?: number,
        public date?: Moment,
        public token?: string,
        public currency?: string,
        public amount?: number,
        public description?: string,
        public capture?: boolean,
        public receipt?: any,
        public name?: string,
        public email?: string
    ) {
        this.capture = this.capture || false;
    }
}
