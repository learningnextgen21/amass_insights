import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DatamarketplaceSharedModule } from '../shared';
import { STRIPE_ROUTE, StripeComponent } from '.';
import { AccordionModule } from 'primeng/accordion';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        AccordionModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot([ STRIPE_ROUTE ], { useHash: true })
    ],
    declarations: [ StripeComponent ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap:    [ StripeComponent ]
})
export class DataMarketplaceStripeModule { }
