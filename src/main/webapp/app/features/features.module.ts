import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RecaptchaModule } from 'ng-recaptcha';

import { DatamarketplaceSharedModule } from '../shared';

import { FEATURE_ROUTE, FeatureComponent } from './';
import { FeatureService } from './features.service';

@NgModule({
	imports: [
		DatamarketplaceSharedModule,
		RouterModule.forRoot( FEATURE_ROUTE , { useHash: true }),
		RecaptchaModule.forRoot()
	],
	declarations: [
        FeatureComponent
	],
	entryComponents: [
	],
	providers: [
		FeatureService
	],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceFeatureModule {}
