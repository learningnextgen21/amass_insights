import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Account, LoginModalService, Principal ,AuthServerProvider,StateStorageService,SigninModalService} from '../shared';

import { SigninDialogComponent } from '../dialog/signindialog.component';

import { ApplyAccountComponent } from '../account/apply/apply.component';
import { FeatureService } from './features.service';
import { DataArticleService } from '../entities/data-article/data-article.service';
import { DataArticle } from '../entities/data-article/data-article.model';
import { JhiAlertService } from 'ng-jhipster';
import { DatePipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';
import { PriceService } from './../price/price.service';

@Component({
	selector: 'jhi-blog',
	templateUrl: './features.component.html',
	styleUrls: [
		'features.scss',
		'../../content/scss/amass-form.scss'
	]

})
export class FeatureComponent implements OnInit {
	account: Account;
	modalRef: NgbModalRef;
	mdDialogRef: MatDialogRef<any>;
	contact: any;
	inquiryTypes = [];
	contactFields: any;
	validationError: any;
	error: any;
	responseMessage: any;
    formValid: boolean;
    subscription: any;
    totalItems: number;
    dataArticles: DataArticle[];
    articleObj: any;
	features: string;
	plan: boolean;
	pageName = 'list';
	constructor(
		private principal: Principal,
		private loginModalService: LoginModalService,
		private eventManager: JhiEventManager,
		public dialog: MatDialog,
        private contactService: FeatureService,
        private dataArticleService: DataArticleService,
        private alertService: JhiAlertService,
        private datePipe: DatePipe,
		public _DomSanitizer: DomSanitizer,
		private router: Router,
		public googleAnalyticsService: GoogleAnalyticsEventsService,
		public priceService: PriceService,
		private stateService: StateStorageService,
        private authService: AuthServerProvider,
		private signinModalService: SigninModalService,
	) {
	}

	ngOnInit() {
		this.responseMessage = null;
		this.error = null;
		this.contactFields = {};
		this.formValid = false;
		this.inquiryTypes = ['General Inquiry', 'Help Finding Data', 'Help Selling My Data', 'Press', 'Tech Support'];
		this.principal.identity().then(account => {
			this.account = account;
		});
        this.validationError = null;
	}
	signUp() {
	  if(!this.isAuthenticated())
	  {
		this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '570px'
        }); 
	  }
	}
	isAuthenticated() {
        return this.principal.isAuthenticated();
    }
	provider() {
		this.router.navigate(['/providers']);
	}
	unrestrictedUser(message:string)
    {
        localStorage.setItem('SearchData',message);
        if(this.isAuthenticated()) {
            this.router.navigateByUrl('providers');
        }
        else {
            this.router.navigateByUrl('unproviders');
        }
    }
	contactUs(events) {
		this.router.navigate(['/contact-us']);
		this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
	}
	
	submitPrice(events: any)
{
console.log(events);
    this.priceService.upgradePrice(events).subscribe(data => {
        console.log(data);
        if (data === null)
        {
           /*  this.window.location.href = '/#/checkout';
            this.window.location.reload(true);
     */
    this.router.navigateByUrl('/checkout');

    
        }  
    });

}
submitFeatures(events: string)
{
this.features = events;
this.plan = true;
}
onExpireSession(event) {
	//console.log('eventcheck',event);
	if (event) {
		//console.log('eventcheck',event);
		//this.formSubmitted = true;
		this.authService.logout().subscribe(d => {
			this.stateService.storeUrl(null);
			this.principal.authenticate(null);
			this.router.navigate(['../../']);
			setTimeout(() => {
			this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
		}, 2000);
		}); 
	}
}
  beforeSessionExpires(event) {
	if (event) {
		//this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
	}
}

}
