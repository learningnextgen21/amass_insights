import { Routes } from '@angular/router';

import { FeatureComponent } from './features.component';

export const FEATURE_ROUTE: Routes = [{
	path: 'features',
	component: FeatureComponent,
	data: {
		authorities: [],
		pageTitle: 'home.title',
		breadcrumb: 'Features'
	}
}/* , {
	path: 'blog',
	component: BlogSingleComponent,
	data: {
		authorities: [],
		pageTitle: 'home.title',
		breadcrumb: 'Blog',
		type: 'static'
	}
} */];
