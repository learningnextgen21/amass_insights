import { Routes } from '@angular/router';

import { PressComponent } from './press.component';

export const BLOG_ROUTE: Routes = [{
	path: 'press',
	component: PressComponent,
	data: {
		authorities: [],
		pageTitle: 'home.title',
		breadcrumb: 'Press'
	}
}/* , {
	path: 'blog',
	component: BlogSingleComponent,
	data: {
		authorities: [],
		pageTitle: 'home.title',
		breadcrumb: 'Blog',
		type: 'static'
	}
} */];
