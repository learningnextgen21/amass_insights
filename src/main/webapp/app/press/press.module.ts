import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RecaptchaModule } from 'ng-recaptcha';

import { DatamarketplaceSharedModule } from '../shared';

import { BLOG_ROUTE, PressComponent } from './';
import { PressService } from './press.service';

@NgModule({
	imports: [
        CommonModule,
		DatamarketplaceSharedModule,
		RouterModule.forRoot( BLOG_ROUTE , { useHash: true }),
		RecaptchaModule.forRoot()
	],
	declarations: [
        PressComponent,
	],
	entryComponents: [
	],
	providers: [
		PressService
	],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplacePressModule {}
