import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Account, LoginModalService, Principal } from '../shared';

import { SigninDialogComponent } from '../dialog/signindialog.component';

import { ApplyAccountComponent } from '../account/apply/apply.component';
import { PressService } from './press.service';
import { DataArticleService } from '../entities/data-article/data-article.service';
import { DataArticle } from '../entities/data-article/data-article.model';
import { JhiAlertService } from 'ng-jhipster';
import { DatePipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';

@Component({
	selector: 'jhi-blog',
	templateUrl: './press.component.html',
	styleUrls: [
		'press.scss',
		'../../content/scss/amass-form.scss'
	]

})
export class PressComponent implements OnInit {
	account: Account;
	modalRef: NgbModalRef;
	mdDialogRef: MatDialogRef<any>;
	contact: any;
	inquiryTypes = [];
	contactFields: any;
	validationError: any;
	error: any;
	responseMessage: any;
    formValid: boolean;
    subscription: any;
    totalItems: number;
    dataArticles: DataArticle[];
	articleObj: DataArticle[] = [];
	dataList: any;
	loader:boolean;

	constructor(
		private principal: Principal,
		private loginModalService: LoginModalService,
		private eventManager: JhiEventManager,
		public dialog: MatDialog,
		private contactService: PressService,
        private dataArticleService: DataArticleService,
        private alertService: JhiAlertService,
        private datePipe: DatePipe,
		public _DomSanitizer: DomSanitizer,
		private router: Router,
		public googleAnalyticsService: GoogleAnalyticsEventsService
	) {
	}

	ngOnInit() {
		this.loader = true;
		this.responseMessage = null;
		this.error = null;
		this.contactFields = {};
		this.formValid = false;
		this.inquiryTypes = ['General Inquiry', 'Help Finding Data', 'Help Selling My Data', 'Press', 'Tech Support'];
		this.principal.identity().then(account => {
			this.account = account;
		});
		this.registerAuthenticationSuccess();
        this.validationError = null;
        /* const req = {
            'filter': {
                'query': {
                    'bool': {
                        'must': {
                            'terms': {
                                'authorOrganization.id': [7828]
                            }
                        }
                    }
                }
            }
        }; */
        /* this.subscription = this.dataArticleService.elasticSearchAuthenticated(req).subscribe(
            res => this.onSuccess(res, res.headers),
            res => this.onError(res)
		); */
		this.subscription = this.dataArticleService.pressArticles().subscribe(data =>{
			console.log(data);
			this.loader = false;
			this.totalItems = data['hits'] ? data['hits'].total : 0;
			//const items = data['hits'].hits;
		if (data['hits'].hits) {
			for (let i = 0; i <= data['hits'].hits.length; i++) {
				this.dataList = data['hits'] && data['hits'].hits[i] ? data['hits'].hits[i]._source : 0;
				this.articleObj.push(this.dataList);
                console.log(this.articleObj);
			}
			console.log('array....', this.articleObj);
        }
		});
	}

	registerAuthenticationSuccess() {
		this.eventManager.subscribe('authenticationSuccess', (message) => {
			this.principal.identity().then(account => {
				this.account = account;
			});
		});
	}

	isAuthenticated() {
		return this.principal.isAuthenticated();
	}

	login() {
		this.modalRef = this.loginModalService.open();
	}

	openSigninDialog() {
		this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
			width: '400px'
		});
	}

	openApplyDialog() {
		this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
			width: '400px'
		});
    }
	pressArticle(label){
		// this.router.navigate(['/news']);
		this.googleAnalyticsService.emitEvent('Press Article', 'Internal Link Clicked', label);
	}
}
