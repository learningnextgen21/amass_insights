import { Route } from '@angular/router';

import { AmassNavbarComponent } from './layouts';

export const navbarRoute: Route = {
    path: '',
    component: AmassNavbarComponent,
    outlet: 'navbar'
};
