import { Route } from '@angular/router';
import { CanDeactivateGuard, UserRouteAccessService, CanDeactivateAddProviderAdminFormGuard } from 'app/shared';
import { AddProviderAdminComponent } from './add-provider-admin.component';

export const addProviderAdminRoute: Route = {
    path: 'add-provider-admin',
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'datamarketplaceApp.dataProvider.home.title',
        breadcrumb: 'Providers > Add New > Admin',
        type: 'dynamic'
    },
    component: AddProviderAdminComponent,
    canActivate: [UserRouteAccessService],
    canDeactivate: [CanDeactivateAddProviderAdminFormGuard]
};
