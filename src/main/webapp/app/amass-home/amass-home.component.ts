import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Account, LoginModalService, Principal } from '../shared';

import { SigninDialogComponent } from '../dialog/signindialog.component';

import { ApplyAccountComponent } from '../account/apply/apply.component';

@Component({
    selector: 'jhi-amass-home',
    templateUrl: './amass-home.component.html',
    styleUrls: [
        'amass-home.scss'
    ]

})
export class AmassHomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    mdDialogRef: MatDialogRef<any>;

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        public dialog: MatDialog
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    openSigninDialog() {
        this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
            width: '400px'
        });
    }

    openApplyDialog() {
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '400px'
        });
    }
}
