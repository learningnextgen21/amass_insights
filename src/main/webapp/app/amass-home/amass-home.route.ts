import { Route } from '@angular/router';

import { UserRouteAccessService } from '../shared';
import { AmassHomeComponent } from './';

export const HOME_ROUTE: Route = {
    path: '',
    component: AmassHomeComponent,
    data: {
        authorities: [],
        pageTitle: 'home.title'
    }
};
