import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DatamarketplaceSharedModule } from '../shared';
import { ABOUT_ROUTE, AboutComponent } from './';

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        RouterModule.forRoot([ ABOUT_ROUTE ], { useHash: true })
    ],
    declarations: [ AboutComponent ],
    bootstrap:    [ AboutComponent ]
})
export class DataMarketplaceAboutModule { }
