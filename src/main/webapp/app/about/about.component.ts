import { Component, OnInit, Inject } from '@angular/core';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';
import {WINDOW} from '../layouts/main/window.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'jhi-about',
    templateUrl: 'about.component.html',
    styleUrls: [
        './about.scss',
        '../home/home.css'
    ]
})
export class AboutComponent implements OnInit {

    constructor(
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        public router: Router,
        @Inject(WINDOW) private window: Window,
        ) {

        }

    ngOnInit() {

    }
    eventlinkedin(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    contactUs(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    scrollTo(selector) {
		const element = document.getElementById(selector);

		element.scrollIntoView(true);
		// account for fixed header
		const scrolledY = this.window.scrollY;
        // reduce fixed height header
        if (scrolledY) {
			this.window.scroll(0, scrolledY - 63);
		}
    }
    teamEvent(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    pressPage() {
        this.router.navigate(['/press']);
    }
    amassView(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
}
