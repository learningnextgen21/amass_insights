import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';

import { DataOrganization } from '../entities/data-organization/data-organization.model';
import { ResponseWrapper, createRequestOption } from 'app/shared';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable()
export class AddOrganizationAdminService {
    private resourceUrl = 'api/providers';
    private resourceElasticSearchUrl = 'api/es/organizationsearch';
    private resourceFollowUrl = 'api/follow-provider?providerID';
    private resourceExpandUrl = 'api/expand-provider?providerID';
    private resourceUnlockUrl = 'api/unlock-provider?providerID';
    private resourceConfirmExpandUrl = 'api/confirm-expand-provider?providerID';
    private resourceConfirmUnlockUrl = 'api/confirm-unlock-provider?providerID';
    private resourceCancelExpandUrl = 'api/cancel-expand-provider?providerID';
    private resourceCancelUnlockUrl = 'api/cancel-unlock-provider?providerID';
    private elasticSearchGeneric = '/api/es/qsearch';
    private resourceCancelMoreInfoUrl = 'api/cancel-contact-us-provider-more-info?providerID';
    private categoriesExplanationUrl = 'api/data-categories/desc/';
    private featuresExplanationUrl = 'api/data-features/desc/';
    private industriesExplanationUrl = 'api/data-industries/desc/';
    private providerTagsExplanationUrl = 'api/data-provider-tags/desc/';
    private providerTypesExplanationUrl = 'api/data-provider-types/desc/';
    private dataSourcesExplanationUrl = 'api/data-sources/desc/';
    private fileDownloadUrl = 'api/filedownload/';
    private providerTypesUrl = 'api/data-provider-types';
    private providerEditUrl = 'api/provider_profile_edit';
    private providerTagUrl = 'api/data-provider-tags';
    private dataProviderTagUrl = 'api/data-provider-tags';
    private providerNewTagUrl = 'api/provider-tag';
    private providerRemoveTagUrl = 'api/remove-provider-tag';
    private organizationAdminUrl = 'api/data-organizations';
    private providerLogo = '/api/provider-logo-upload';
    private featureProvider = '/api/ur/providersearch';

	constructor(private http: HttpClient) { }

    findByIdForOrganizationForm(recordID: string): Observable<DataOrganization> {
		const req = {
			'_source': {
				'exclude': [
					
				],
				'include': [
                    'name',
                    'city',
                    'state',
                    'country',
                    'zipCode',
                    'yearFounded',
                    'yearExit',
                    'headcountBucket',
                    'headcountNumber',
                    'headCount',
                    'marketplaceStatus',
                    'researchMethods',
                    'ieiStatus'
				]
			},
			'filter': {
				'query': {
					'match': {
						'recordID': recordID
					}
				}
			}
		};
		return this.http.post(this.resourceElasticSearchUrl + '?detail=true', req);
	}

    addOrganizationAdmin(request?: any): Observable<any> {
        const model = request;
       if(request && request.researchMethodsCompleted) {
           model.researchMethodsCompleted = request.researchMethodsCompleted;
       }
       if (request && request.headcountBucket && request.headcountBucket.id) {
           model.headcountBucket = {
               id: request.headcountBucket.id
            };
       } else {
           delete model.headcountBucket;
       }
       if (request && request.ieiStatus && request.ieiStatus.id) {
           model.ieiStatus = {
               id: request.ieiStatus.id
           };
       } else {
           delete model.ieiStatus;
       }
       if (request && request.marketplaceStatus && request.marketplaceStatus.id) {
           model.marketplaceStatus = {
               id: request.marketplaceStatus.id
           };
        } else {
            delete model.marketplaceStatus;
        }
        return this.http.post(this.organizationAdminUrl, model);
    }

    updateOrganizationAdmin(request?: any): Observable<any> {
        const model = request;
       if(request && request.researchMethodsCompleted) {
           model.researchMethodsCompleted = request.researchMethodsCompleted;
       }
       if (request && request.headcountBucket ) {
           if(request.headcountBucket.hasOwnProperty('id')){
            model.headcountBucket = 
                 request.headcountBucket;
            
           }else{
            model.headcountBucket = {
                id: request.headcountBucket
             };
           }
           
       } else {
           delete model.headcountBucket;
       }
       if (request && request.ieiStatus ) {
        if(request.ieiStatus.hasOwnProperty('id')){
            model.ieiStatus = 
                 request.ieiStatus;
            
           }else{
            model.ieiStatus = {
                id: request.ieiStatus
             };
           }
       } else {
           delete model.ieiStatus;
       }
       if (request && request.marketplaceStatus && request.marketplaceStatus.id) {
           model.marketplaceStatus = {
               id: request.marketplaceStatus.id
           };
        } else {
            delete model.marketplaceStatus;
        }
        return this.http.put(this.organizationAdminUrl, model);
    }
}
