
import {of as observableOf, Subject} from 'rxjs';
import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, Inject, Injectable, Input, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { DomSanitizer } from '@angular/platform-browser';
import { AddOrganizationAdminService } from './add-organization-admin.service';
import { ContactDialogComponent } from 'app/shared/contactdialog/contact-dialog.component';
import { Principal } from 'app/shared/auth/principal.service';
import { DataSource } from '@angular/cdk/table';
import { GoogleAnalyticsEventsService } from 'app/shared/googleanalytics/google-analytics-events.service';
import { LoaderService } from '../loader/loaders.service';
import { NestedTreeControl, FlatTreeControl } from '@angular/cdk/tree';
import { WINDOW } from 'app/layouts';
import { SigninModalService, AmassFilterService, CSRFService, AuthServerProvider, StateStorageService } from 'app/shared';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig, FormlyTemplateOptions, Field } from '@ngx-formly/core';
import { ConfirmDialogComponent } from 'app/shared/dialogs/confirm-dialog.component';
import { UppyService, UppyComponent } from 'app/uppy';
import { contains } from 'ramda';
let _that = null;
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie';
import { DataOrganization } from '../../app/entities/data-organization/data-organization.model';
import { environmentDev } from 'app/environments/environment.dev';

@Component({
	selector: 'jhi-add-organization-admin-form',
	templateUrl: './add-organization-admin-form.component.html',
	styleUrls: [ '../../content/css/material-tab.css']
})
export class AddOrganizationAdminFormComponent implements OnInit {
	dialogRef: MatDialogRef<ConfirmDialogComponent>;
    formSubmitted: boolean;
    routerOutletRedirect: string;

    dataOrganization: DataOrganization;
    providerID: any;
	private subscription: Subscription;
	private eventSubscriber: Subscription;
	isLoading: boolean;
	isAdmin: boolean;
	isPendingUnlock: boolean;
	isPendingExpand: boolean;
	domSanitizer: any;
	authoredArticles: any;
	relevantArticles: any;
	tabIndex: number;
	relevantArticlePage: number;
	relevantArticleLinks: any;
	relevantArticleFrom: number;
	totalRelevantArticles: number;
	authoredArticlePage: number;
	authoredArticleLinks: any;
	authoredArticleFrom: number;
	totalAuthoredArticles: number;
	recordID: string;
	profileTabLabel: any;
    ProfileCategory: any;
    equitesDelete: any;

	progressBarColor = 'primary';
	progressBarMode = 'determinate';
    mdTooltipDelay: number;
    trackname: any;
    trackownerOrganizationCity: any;
    trackownerOrganizationState: any;
    trackownerOrganizationCountry: any;
    trackownerOrganizationYearfound: number;
    trackownerOrganizationHeadcount: any;
    trackownerOrganizationHeadcountNumber: any;
    trackownerOrganizationHeadcountBucket: any;
    tracknumberOfAnalystEmployees: any;
    trackInvestor: any;
    trackNumberOfInvestor: any;

    organizationApproximateHeadcount: any[];
    organizationMarketplaceStatus: any[];
    organizationResearchMethods: any[];
    organizationIEIStatus: any[];

	basicExpansionPanel: boolean;
	organizationExpansionPanel: boolean;
	adminExpansionPanel: boolean;
    panelName: string;
    overAll:string;
	progressSpinner:boolean;
	basicForm = new FormGroup({});
	basicModel: any = {};
	basicOptions: FormlyFormOptions = {};

	organizationDetailsForm = new FormGroup({});
	organizationDetailsModel: any = {};
	organizationDetailsOptions: FormlyFormOptions = {};

	adminForm = new FormGroup({});
	adminModel: any = {};
	adminOptions: FormlyFormOptions = {};

	basicFields: FormlyFieldConfig[] = [
        {
            key: 'id',
            type: 'input',
            className: 'col-md-3',
            hideExpression: true,
            templateOptions: {
                label: 'ID',
                fieldName:'id'
            }
        },
		{
			key: 'name',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Organization Name:',
				placeholder: '',
				required: true,
				attributes: {
					'placeholderText' : 'Name of the organization.',
					'labelTooltip': 'This is the name of the company, subsidiary, government organization or other formal entity.Usually found in the footer of the website.Do not include suffixes, such as Company, LLC, Inc, Ltd, etc.Be aware of parents vs. subsidiaries. Sometimes you will need to add both in order to represent the relationship between both.This is not always the same name as the provider (but it often is).',
                    'placeholderTooltip': 'This is the name of the company, subsidiary, government organization or other formal entity.Usually found in the footer of the website.Do not include suffixes, such as Company, LLC, Inc, Ltd, etc.Be aware of parents vs. subsidiaries. Sometimes you will need to add both in order to represent the relationship between both.This is not always the same name as the provider (but it often is).',
                    'trackNames': 'Organization Name',
                    'trackCategory': 'Organization Admin - Input',
                    'trackLabel': 'Organization Admin - Textbox'
                },
                maxLength : 128,
                keypress:(event)=> {

                },
				change: this.basicInput
			},
		},
	];

	organizationDetailsFields: FormlyFieldConfig[] = [
        {
			key: 'city',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'City:',
                type: 'input',
                required: true,
                attributes: {
                    'placeholderText' : 'City of the headquarters of the organization.',
                    'trackNames': 'City',
                    'trackCategory': 'Organization Admin - Input',
                    'trackLabel': 'Organization Admin - Textbox'
                },
                maxLength : 50,
                keypress:(event)=> {

                },
				change: this.basicInput
            },

		},
		{
			key: 'state',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'State:',
				type: 'input',
                attributes: {
                    'placeholderText' : 'State of the headquarters of the organization.',
                    'trackNames': 'State',
                    'trackCategory': 'Organization Admin - Input',
                    'trackLabel': 'Organization Admin - Textbox'
                },
                maxLength : 50,
                keypress:(event)=> {

                },
				change: this.basicInput
			},
		},
        {
			key: 'country',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Country:',
                type: 'input',
                required: true,
                attributes: {
                    'placeholderText' : 'Country of the headquarters of the organization.',
                    'trackNames': 'Country',
                    'trackCategory': 'Organization Admin - Input',
                    'trackLabel': 'Organization Admin - Textbox'
                },
                maxLength : 50,
                keypress:(event)=> {

                },
				change: this.basicInput
			},
		},
		{
			key: 'zipCode',
			type: 'mask',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Zip Code:',
				attributes: {
                    'placeholderText' : 'Zip code of the headquarters of the organization.',
                    'mask': '99999',
                    'trackNames': 'Zip Code',
                    'trackCategory': 'Organization Admin - Input',
                    'trackLabel': 'Organization Admin - Textbox'
                },
                maxLength: 5,
                change: this.basicInput,
                keypress:(event)=> {

                },
			},
		},
		{
			key: 'yearFounded',
			type: 'mask',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Year Founded:',
                attributes: {
                    'placeholderText' : 'Year that the organization was founded.',
                    'mask': '9999',
                    'trackNames': 'Year Founded',
                    'trackCategory': 'Organization Admin - Input',
                    'trackLabel': 'Organization Admin - Textbox'
                },
                maxLength: 4,
                change: (event: Field, value: number) => {
                    const currentDate = new Date();
                    if (value && (value < 1700 || value > currentDate.getFullYear())) {
                        this.openSnackBar('Year must be between 1700 and ' + currentDate.getFullYear() + '!', 'Close');
                    }
                    this.basicInput(event);
                },
                keypress:(event)=> {

                },
			},
        },
        {
			key: 'yearExit',
			type: 'mask',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Exit Year:',
                description: 'What year did the company undergo an ownership change, whether through acquisition or public offering? Leave blank if this is not applicable.',
                attributes: {
                    'placeholderText' : '',
                    'mask': '9999',
                    'trackNames': 'Exit Year',
                    'trackCategory': 'Organization Admin - Input',
                    'trackLabel': 'Organization Admin - Textbox'
                },
                maxLength: 4,
                change: (event: Field, value: number) => {
                    const currentDate = new Date();
                    if (value && (value < 1700 || value > currentDate.getFullYear())) {
                        this.openSnackBar('Year must be between 1700 and ' + currentDate.getFullYear() + '!', 'Close');
                    }
                    this.basicInput(event);
                },
                keypress:(event)=> {

                },
			},
		},
		{
			key: 'headcountBucket.id',
            type: 'dropdown',
            className: 'customSmallWidth-dropdown',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Approximate Headcount:',
                options: this.organizationApproximateHeadcount,
				attributes: {
                    field: 'label',
                    'placeholderText' : 'An approximate range of the number of people that work at the organization.'
                },
                change: this.basicInput
			},
		},
		{
			key: 'headcountNumber',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Headcount:',
                type: 'number',
                attributes: {
                    'placeholderText' : 'The precise number of people that work at the organization.',
                    'trackNames': 'Headcount',
                    'trackCategory': 'Organization Admin - Input',
                    'trackLabel': 'Organization Admin - Textbox'
                },
                maxLength : 25,
                keypress:(event)=> {

                },
				change: this.basicInput
			},
        },
        {
			key: 'headCount',
			type: 'input',
            wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Headcount Details:',
                attributes: {
                    'placeholderText' : 'Details regarding the number of people that work at the organization.',
                    'trackNames': 'Headcount Details',
                    'trackCategory': 'Organization Admin - Input',
                    'trackLabel': 'Organization Admin - Textbox'
                },
                maxLength : 64,
                keypress:(event)=> {

                },
				change: this.basicInput
			},
		},
	];

    adminFields: FormlyFieldConfig[] = [
		{
			key: 'marketplaceStatus.id',
            type: 'dropdown',
            className: 'customSmallWidth-dropdown',
            defaultValue: environmentDev.dev ? 2043 : 1985,
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Marketplace Status:',
                description: 'What should an external client understand about the status of our research on this provider?',
				required: true,
                options: this.organizationMarketplaceStatus,
                attributes: {
                    'placeholderText' : '',
                    field: 'label',
                    'labelTooltip': 'The only statuses IEI will be using are ""Minimal"" and ""Complete"". Ignore all other statuses.Explanation of choices:""Minimal"": When you\'ve only done the minimal research""Complete"": When the full research has been completed',
                    'descriptionTooltip': 'The only statuses IEI will be using are ""Minimal"" and ""Complete"". Ignore all other statuses.Explanation of choices:""Minimal"": When you\'ve only done the minimal research""Complete"": When the full research has been completed'
                },
                change: this.basicInput
			},
		},
		{
			key: 'researchMethodsCompleted',
            type: 'multiselect',
            className: 'custom-multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Research Methods Completed:',
				required: true,
				options: this.organizationResearchMethods,
                attributes: {
                    'placeholderText' : 'What types/methods of researching an organization have been completed?',
					field: 'desc',
                    'labelTooltip': 'Fill these in as you research an organization, and especially after you\'re done researching an organization.',
					'descriptionTooltip': 'Fill these in as you research an organization, and especially after you\'re done researching an organization.',
                },
                change: this.basicInput
            },
        },
        {
			key: 'ieiStatus.id',
            type: 'dropdown',
            className: 'widthSticky-dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'IEI Status:',
				required: false,
				options: this.organizationMarketplaceStatus,
                attributes: {
                    'placeholderText' : 'What\'s the status of this organization\'s research?',
					field: 'label',
                    'labelTooltip': 'Explanation of choices:""Needs IEI QA"": the IEI researcher is unsure about the quality of his/her work and wants IEI\'s QA manager to review it""Needs Amass QA"": the IEI researcher and the IEI QA manager is unsure about the quality of his/her work and wants an Amass employee to review it""Done"": IEI is confident about their work and have finished all of the steps in the research process',
					'descriptionTooltip': 'Explanation of choices:""Needs IEI QA"": the IEI researcher is unsure about the quality of his/her work and wants IEI\'s QA manager to review it""Needs Amass QA"": the IEI researcher and the IEI QA manager is unsure about the quality of his/her work and wants an Amass employee to review it""Done"": IEI is confident about their work and have finished all of the steps in the research process',
                },
                change: this.basicInput
            },
		},
	];

    csrf: string;
    organisationRecordID:any;

	constructor(
		private dataOrganizationService: AddOrganizationAdminService,
		private filterService: AmassFilterService,
		private route: ActivatedRoute,
		private router: Router,
		public dialog: MatDialog,
        private principal: Principal,
        private authService: AuthServerProvider,
		public _DomSanitizer: DomSanitizer,
		public googleAnalyticsService: GoogleAnalyticsEventsService,
		private loaderService: LoaderService,
		private signinModalService: SigninModalService,
		@Inject(WINDOW) private window: Window,
		public snackBar: MatSnackBar,
		private uppyService: UppyService,
        private csrfService: CSRFService,
        private el: ElementRef,
        private stateService: StateStorageService,
        private cookieService: CookieService
	) {
        this.routerOutletRedirect = '';
		this.domSanitizer = DomSanitizer;
		this.formSubmitted = false;

	}
	openUppyDialog(events:any){
		//this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        const dialogRef = this.dialog.open(UppyComponent, {
			width: '500px',
			height: '3000px'
		});
		// dialogRef.afterClosed().subscribe(result => {
		// 	console.log('The dialog was closed');
		//   });
	  }
	ngOnInit() {
        _that = this;
        this.trackownerOrganizationCity =  0;
        this.trackownerOrganizationState =  0;
        this.trackownerOrganizationCountry = 0;
        this.trackownerOrganizationYearfound = 0;
        this.trackownerOrganizationHeadcount =  0;
        this.trackownerOrganizationHeadcountBucket =  0;
        this.trackownerOrganizationHeadcountNumber =  0;
        this.csrf = this.csrfService.getCSRF();
        this.filterService.setAuthorisedUrl();
		this.loaderService.display(false);
		this.isLoading = true;
		this.isPendingUnlock = false;
		this.isPendingExpand = false;
		this.tabIndex = 0;
		this.basicExpansionPanel = true;
		this.organizationExpansionPanel = false;
		this.adminExpansionPanel= false;
		this.route.queryParams.subscribe(params => {
			if (params['pending-unlock']) {
				this.isPendingUnlock = true;
			}

			if (params['pending-expand']) {
				this.isPendingExpand = true;
			}
		});
		this.subscription = this.route.params.subscribe(params => {
			this.recordID = params['recordID'];
			this.load(params['recordID']);
        });
		this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
		this.relevantArticlePage = 0;
		this.relevantArticleLinks = {
			last: 0
		};
		this.authoredArticlePage = 0;
		this.authoredArticleLinks = {
			last: 0
		};
        this.mdTooltipDelay = 500;

        this.organizationApproximateHeadcount = [];
		this.filterService.getLookupCodes('HEADCOUNT_BUCKET').subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			// this.organizationApproximateHeadcount.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.organizationApproximateHeadcount.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
            }
            this.organizationDetailsFields[6].templateOptions.options = this.organizationApproximateHeadcount;
		});

        this.organizationMarketplaceStatus = [];
		this.filterService.getLookupCodes('MARKETPLACE_STATUS').subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			// this.organizationMarketplaceStatus.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.organizationMarketplaceStatus.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
            }
            this.adminFields[0].templateOptions.options = this.organizationMarketplaceStatus;
        });

        this.organizationResearchMethods = [];
		this.filterService.getLookupCodes('RESEARCH_METHOD').subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.organizationResearchMethods.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.adminFields[1].templateOptions.options = this.organizationResearchMethods;
        });

        this.organizationIEIStatus = [];
		this.filterService.getLookupCodes('IEI_STATUS').subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			// this.organizationIEIStatus.push({label : '', value: null });
			for (let i = 0; i < listItems.length; i++) {
				this.organizationIEIStatus.push({
					value: listItems[i]._source.id,
					label: listItems[i]._source.lookupcode
				});
            }
            this.adminFields[2].templateOptions.options = this.organizationIEIStatus;
		});
    }

	getLinkImgUrl(desc) {
		if (desc) {
			const fileName = ((desc).toLowerCase()).replace(' ', '_');
			const link = 'content/images/' + fileName  + '.png';
			return link;
		}
	}

	providerProfileTab(event: MatTabChangeEvent) {

	   /*  this.profileTabLabel = 'Provider Profile - Tab ' + event.tab.textLabel;
		this.ProfileCategory = 'Provider Profile ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent(this.ProfileCategory, 'Tab Link Clicked', this.profileTabLabel); */
	}

	providerFollow(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	unlockProviders(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	requestMoreInfo(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	providerPartnerTab(event: MatTabChangeEvent) {
		this.profileTabLabel = 'Provider Profile -  ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent('Partner Provider', 'Internal Link Clicked', this.profileTabLabel);
	}

	dataFieldLink(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	profileWebsite(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	profileArticle(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
	samplesTab(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	load(recordID) {
		this.dataOrganizationService.findByIdForOrganizationForm(recordID).subscribe(dataOrganization => {
			this.loaderService.display(false);
            this.dataOrganization = dataOrganization['hits'].total ? dataOrganization['hits'].hits[0]._source : null;
			this.isLoading = false;
			this.constructFormModel(this.dataOrganization);
            this.basicModel = this.dataOrganization;
            this.providerID = this.dataOrganization ? this.dataOrganization.id : null;
		}, error => {
			if (error.status === 401) {
                this.stateService.storeUrl(null);
                this.router.navigate(['']);
				this.signinModalService.openDialog('Session expired! Please re-login.');
			}
			this.dataOrganization = null;
			this.isLoading = false;
		});
	   // this.loaderService.display(false);
    }

    constructFormModel(organization: DataOrganization) {
        this.trackname = organization.name ? organization.name.length : 0;
        if (organization) {
            this.trackownerOrganizationCity = organization.city ? organization.city.length : 0;
            this.trackownerOrganizationState = organization.state ? organization.state.length : 0;
            this.trackownerOrganizationCountry = organization.country ? organization.country.length : 0;
            this.trackownerOrganizationYearfound = organization.yearFounded;
            this.trackownerOrganizationHeadcount = organization.headCount ? organization.headCount.length : 0;
            this.trackownerOrganizationHeadcountBucket = organization.headcountBucket ? organization.headcountBucket : 0;
            this.trackownerOrganizationHeadcountNumber = organization.headcountNumber ? organization.headcountNumber : 0;
        }
    }

	previousState() {
		this.window.history.back();
	}

	openConfirmationDialog(nextState?: RouterStateSnapshot) {
        const redirect = nextState.url;
        this.dialog.closeAll();
        this.dialogRef = this.dialog.open(ConfirmDialogComponent, {
            disableClose: false
        });
        this.dialogRef.componentInstance.confirmMessage = 'You have some unsaved edits in this form.';
        this.dialogRef.componentInstance.button = {
            leave: true,
            confirm: true,
            leaveText: 'Leave and Don\'t Save',
            confirmText: 'Save and Submit'
        };

        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // do confirmation actions
                if (result === 'leave') {
                    if (redirect === '/') {
                        this.authService.logout().subscribe();
                        this.formSubmitted = true;
                        this.principal.authenticate(null);
                        this.stateService.storeUrl(null);
                        this.router.navigate(['../../']);
                    } else {
                        this.formSubmitted = true;
                        if (this.principal.isAuthenticated()) {
                            this.stateService.storeUrl(null);
                            this.router.navigate([redirect]);
                            this.dialog.closeAll();
                        } else {
                            this.stateService.storeUrl(null);
                            this.router.navigate(['']);
                            this.dialog.closeAll();
                        }
                    }

                }

                if (result === 'save') {
                    if (redirect === '/') {
                        this.submitOrganizationAdmin(this.basicModel);
                        this.authService.logout().subscribe();
                        this.formSubmitted = true;
                        this.principal.authenticate(null);
                        this.stateService.storeUrl(null);
                        this.router.navigate(['../../']);
                    } else {
                        if (this.principal.isAuthenticated()) {
                            this.submitOrganizationAdmin(this.basicModel);
                        } else {
                            this.formSubmitted = true;
                            this.stateService.storeUrl(null);
                            this.router.navigate([redirect]);
                        }
                    }
                }
            }
            this.dialogRef = null;
        });
		return false;
	}

	trackByIndex(index) {
		return index;
	}

    updateProvider(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, submitMessage) {
       this.progressSpinner=true;
        this.dataOrganizationService.updateOrganizationAdmin(model).subscribe(data => {
            if (data) {
                this.progressSpinner=false;
                this.formSubmitted = true;
                //this.basicModel = data;
                this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                this.loaderService.display(false);
                if (currentPanel && currentPanel !== '') {
                    this[currentPanel] = false;
                    if (nextPanel && nextPanel !== '') {
                        this[nextPanel] = true;
                    }
                    if (tabIndex) {
                        this.tabIndex = tabIndex;
                    }
                } else {
                    if (skipRedirect) {
                        return true;
                    }
                    this.stateService.storeUrl(null);
                    this.router.navigate(['providers']);
                }
            } else {
                this.progressSpinner=false;
                this.formSubmitted = false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        }, error => {
            if (error.status === 401) {
                this.progressSpinner=false;
                this.formSubmitted = false;
                this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
            } else {
                this.progressSpinner=false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        });
    }

    insertProvider(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, submitMessage) {
        this.progressSpinner=true;
        this.dataOrganizationService.addOrganizationAdmin(model).subscribe(data => {
            if (data) {
                this.progressSpinner=false;
                this.formSubmitted = true;
                this.basicModel = data;
                this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                this.loaderService.display(false);


                if (currentPanel && currentPanel !== '') {
                    this[currentPanel] = false;
                    if (nextPanel && nextPanel !== '') {
                        this[nextPanel] = true;
                    }
                    if (tabIndex) {
                        this.tabIndex = tabIndex;
                    }
                }
            } else {
                this.formSubmitted = false;
                this.progressSpinner=false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        }, error => {
            if (error.status === 401) {
                this.formSubmitted = false;
                this.progressSpinner=false;
                this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
            } else {
                this.progressSpinner=false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        });
    }

	submitOrganizationAdmin(model, panelName?: string, currentPanel?: string, nextPanel?: string, tabIndex?: number, form?: string, skipRedirect?: boolean) {
        //this.panelName=panelName;
        this.overAll=panelName;
		if(panelName !=='Overall Details')
		{
			this.panelName = panelName;
		}
		else
		{
			panelName = '';
		}
        const formName = panelName ? panelName + '' : 'Form';
        const submitMessage = `The Organization with the name "${model.name}" has been successfully submitted and has been added to the database immediately.`;
        const filterArr = Array.prototype.filter;
        if (form && this[form].valid) {
            const autoCompleteFields = this.el.nativeElement.querySelectorAll('#' + form + ' .ui-autocomplete-input');
            const filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                return node.value === '';
            });
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else {
                if (model && !model.recordID) {
                    this.insertProvider(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, submitMessage);
                } else {
                    this.updateProvider(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, submitMessage);
                }
            }
        } else {
            let invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
            let autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
            let filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                return node.required && node.value === '';
            });
            if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                setTimeout(() => {
                    filteredAutoCompleteFields[0].focus();
                }, 1000);
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else if (invalidElements.length > 0) {
                if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                    invalidElements[0].querySelectorAll('input')[0].focus();
                } else {
                    setTimeout(() => {
                        invalidElements[0].focus();
                    }, 1000);
                }
                this.openSnackBar('Please fill all the required fields!', 'Close');
                this.expandFormPanels();
            } else {
                this.tabIndex = this.tabIndex === 0 ? 1 : 0;
                setTimeout(() => {
                    invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
                    autoCompleteFields = this.el.nativeElement.querySelectorAll('.ui-autocomplete-input');
                    filteredAutoCompleteFields = filterArr.call( autoCompleteFields, function( node ) {
                        return node.required && node.value === '';
                    });
                    if (filteredAutoCompleteFields && filteredAutoCompleteFields.length) {
                        setTimeout(() => {
                            filteredAutoCompleteFields[0].focus();
                        }, 1000);
                        this.openSnackBar('Please fill all the required fields!', 'Close');
                        this.expandFormPanels();
                    } else if (invalidElements.length > 0) {
                        if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                            invalidElements[0].querySelectorAll('input')[0].focus();
                        } else {
                            setTimeout(() => {
                                invalidElements[0].focus();
                            }, 1000);
                        }
                        this.openSnackBar('Please fill all the required fields!', 'Close');
                        this.expandFormPanels();
                    } else {
                        if (model && !model.recordID) {
                            this.insertProvider(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, submitMessage);
                        } else {
                            this.updateProvider(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, submitMessage);
                        }
                    }
                }, 2000);
            }
        }
    }

    expandFormPanels() {
        this.basicExpansionPanel = true;
        this.organizationExpansionPanel = true;
		this.adminExpansionPanel= true;
    }

    formSubmit(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    formSave(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    basicInput($event) {

        const inputKey = $event;

        if (inputKey.key === 'city') {
            $event.model.city = inputKey.formControl.value;
        }
        if (inputKey.key === 'state') {
            $event.model.state = inputKey.formControl.value;
        }
        if (inputKey.key === 'country') {
            $event.model.country = inputKey.formControl.value;
        }
        if (inputKey.key === 'yearFounded') {
            $event.model.yearFounded = inputKey.formControl.value;
        }
        if (inputKey.key === 'zipCode') {
            $event.model.zipCode = inputKey.formControl.value;
        }
        if (inputKey.key === 'yearExit') {
            $event.model.yearExit = inputKey.formControl.value;
        }
        if (inputKey.key === 'headcountNumber') {
            $event.model.headcountNumber = inputKey.formControl.value;
        }
        if (inputKey.key === 'headCount') {
            $event.model.headCount = inputKey.formControl.value;
        }
        if (inputKey.key === 'marketplaceStatus.id') {
            $event.model.marketplaceStatus.id = inputKey.formControl.value;
        }
        if (inputKey.key === 'headcountBucket.id') {
            $event.model.headcountBucket = inputKey.formControl.value;
        }
        if (inputKey.key === 'ieiStatus.id') {
            $event.model.ieiStatus = inputKey.formControl.value;
        }
        if (inputKey.key === 'researchMethodsCompleted') {
            $event.model.researchMethodsCompleted = inputKey.formControl.value;
        }
        /* delete entry */
        /* delete entry */
        if (inputKey.key === 'name') {
            if (inputKey.model.name.length < _that.trackname) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.name.length > _that.trackname) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'city') {

            if (inputKey.model.city.length < _that.trackownerOrganizationCity) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.city.length > _that.trackownerOrganizationCity) {
                 _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'state') {
            if (inputKey.model.state.length < _that.trackownerOrganizationState) {
                 _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.state.length > _that.trackownerOrganizationState) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
             }
        } else if (inputKey.key === 'country') {
            if (inputKey.model.country.length < _that.trackownerOrganizationCountry) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.country.length > _that.trackownerOrganizationCountry) {
                _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'yearFounded') {
            if (inputKey.model.yearFounded < _that.trackownerOrganizationYearfound) {
               _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.yearFounded > _that.trackownerOrganizationYearfound) {
              _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'headcountBucket') {
            if (inputKey.model.headcountBucket < _that.trackownerOrganizationHeadcountBucket) {
               _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.headcountBucket > _that.trackownerOrganizationHeadcountBucket) {
              _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'headcountNumber') {
            if (inputKey.model.headcountNumber < _that.trackownerOrganizationHeadcountNumber) {
               _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.headcountNumber > _that.trackownerOrganizationHeadcountNumber) {
              _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        } else if (inputKey.key === 'headCount') {
            if (inputKey.model.headCount < _that.trackownerOrganizationHeadcount) {
               _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Deleted', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            } else if (inputKey.model.headCount > _that.trackownerOrganizationHeadcount) {
              _that.googleAnalyticsService.emitEvent(inputKey.templateOptions.attributes.trackCategory, 'Added', inputKey.templateOptions.attributes.trackLabel + ' - ' + inputKey.templateOptions.attributes.trackNames);
            }
        }
    }

    openSnackBar(message: string, action: string, delay?: number) {
        this.snackBar.open(message, action, {
            duration: delay ? delay : 10000
        });
    }

	submitBasic(model) {
		// console.log(model);
		this.basicExpansionPanel = false;
		this.organizationExpansionPanel = true;
	}

	submitCompanyDetails(model) {
		// console.log(model);
		this.organizationExpansionPanel = false;
		this.adminExpansionPanel = true;
	}

	submitAdmin(model) {
		// console.log(model);
		this.adminExpansionPanel = false;
	}


    beforeSessionExpires(event) {
        if (event) {
           // this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }

    onExpireSession(event) {
        if (event) {
            this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
                this.signinModalService.openDialog('Your session has expired due to inactivity. Please login again.');
            });
        }
    }

    @HostListener('document:keyup', [])
    @HostListener('document:click', [])
    @HostListener('document:wheel', [])
    @HostListener('window:scroll', [])
    resetTimer() {
        this.authService.notifyUserAction();
    }

    @HostListener('window:popstate', [])
    onPopState() {
        this.cookieService.put('logoutClicked', '0');

    }

    @HostListener('window:beforeunload', ['$event'])
    preventUser($event) {
        $event.preventDefault();
        return $event.returnValue = "Are you sure you want to exit?";
    }
}
