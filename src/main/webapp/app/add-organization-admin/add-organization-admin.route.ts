import { Route } from '@angular/router';
import { CanDeactivateGuard, UserRouteAccessService } from 'app/shared';
import { AddOrganizationAdminFormComponent } from './add-organization-admin-form.component';

export const addOrganizationAdminRoute: Route = {
    path: 'add-organization-admin',
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'datamarketplaceApp.dataOrganization.home.title',
        breadcrumb: 'Organization > Add New > Admin',
        type: 'dynamic'
    },
    component: AddOrganizationAdminFormComponent,
    canActivate: [UserRouteAccessService],
    canDeactivate: [CanDeactivateGuard]
};
