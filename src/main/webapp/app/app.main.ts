import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ProdConfig } from './blocks/config/prod.config';
import { DatamarketplaceAppModule } from './app.module';
import {environment} from '../app/environments/environment';
import './shared/icons';

ProdConfig();

if (module['hot']) {
    module['hot'].accept();
}

console.log(environment);
if (environment.prod) {
    document.write('<script type="text/javascript">' +
    'var googleScript = document.createElement(\'script\');' +
    'googleScript.type = \'text/javascript\';' +
    'googleScript.src = \'https://www.googletagmanager.com/gtag/js?id=UA-63556202-1\';' +
    'document.getElementsByTagName(\'head\')[0].appendChild(googleScript);' +
    'window.dataLayer = window.dataLayer || [];' +
    'function gtag(){dataLayer.push(arguments);}' +
    'gtag(\'js\', new Date());' +
    'gtag(\'config\', \'UA-63556202-1\');' +
    '(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=' +
    'function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;' +
    'e=o.createElement(i);r=o.getElementsByTagName(i)[0];' +
    'e.src=\'//www.google-analytics.com/analytics.js\';' +
    'r.parentNode.insertBefore(e,r)}(window,document,\'script\',\'ga\'));' +
    'ga(\'create\',\'UA-63556202-1\');'
    + '</script>');
} else if (environment.dev) {
    document.write('<script type="text/javascript">' +
    'var googleScript = document.createElement(\'script\');' +
    'googleScript.type = \'text/javascript\';' +
    'googleScript.src = \'https://www.googletagmanager.com/gtag/js?id=UA-63556202-1\';' +
    'document.getElementsByTagName(\'head\')[0].appendChild(googleScript);' +
    'window.dataLayer = window.dataLayer || [];' +
    'function gtag(){dataLayer.push(arguments);}' +
    'gtag(\'js\', new Date());' +
    'gtag(\'config\', \'UA-63556202-1\');' +
    '(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=' +
    'function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;' +
    'e=o.createElement(i);r=o.getElementsByTagName(i)[0];' +
    'e.src=\'//www.google-analytics.com/analytics.js\';' +
    'r.parentNode.insertBefore(e,r)}(window,document,\'script\',\'ga\'));' +
    'ga(\'create\',\'UA-63556202-1\');'
    + '</script>');
} else if (environment.qa) {
    console.log('I\'m runing in QA profile');
    document.write('<script type="text/javascript">' +
    'var googleScript = document.createElement(\'script\');' +
    'googleScript.type = \'text/javascript\';' +
    'googleScript.src = \'https://www.googletagmanager.com/gtag/js?id=UA-63556202-2\';' +
    'document.getElementsByTagName(\'head\')[0].appendChild(googleScript);' +
    'window.dataLayer = window.dataLayer || [];' +
    'function gtag(){dataLayer.push(arguments);}' +
    'gtag(\'js\', new Date());' +
    'gtag(\'config\', \'UA-63556202-2\');' +
    '(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=' +
    'function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;' +
    'e=o.createElement(i);r=o.getElementsByTagName(i)[0];' +
    'e.src=\'//www.google-analytics.com/analytics.js\';' +
    'r.parentNode.insertBefore(e,r)}(window,document,\'script\',\'ga\'));' +
    'ga(\'create\',\'UA-63556202-2\');'
    + '</script>');
}

platformBrowserDynamic()
    .bootstrapModule(DatamarketplaceAppModule, { preserveWhitespaces: true })
    .then(success => console.log(`Application started`))
    .catch(err => console.error(err));
