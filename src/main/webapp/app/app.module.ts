import './vendor.ts';

import { NgModule, VERSION, Injector, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerModule } from '@angular/platform-browser';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClient } from '@angular/common/http';

import { DatamarketplaceSharedModule, UserRouteAccessService, CanDeactivateGuard, ProviderRouteAccessService, UserRouteAccessUnrestrictedService, CanDeactivateAdminFormGuard, CanDeactivateAddProviderAdminFormGuard } from './shared';
import { DatamarketplaceHomeModule } from './home/home.module';
import { DataMarketplaceAboutModule } from './about/about.module';
import { DataMarketplacePriceModule } from './price/price.module';
import { DataMarketplaceWorksModule } from './works/howworks.module';
import { DataMarketplaceInvestmentWorksModule } from './works/investment-howworks.module'
import { DatamarketplaceContactModule } from './contact/contact.module';
import { DatamarketplaceAdminModule } from './admin/admin.module';
import { DatamarketplaceAccountModule } from './account/account.module';
import { DatamarketplaceEntityModule } from './entities/entity.module';

import { PaginationConfig } from './blocks/config/uib-pagination.config';
import { SigninDialogComponent } from './dialog/signindialog.component';
import { SettingsComponentComponent } from './account/settings/settings.component';
import {  EmailActionComponentComponent } from './account/email-action/email-action.component';

import { ForgotPasswordDialogComponent } from './dialog/forgot-password.component';

import { ApplyAccountComponent } from './account/apply/apply.component';
import { BreadcrumbComponent } from './layouts/breadcrumb/breadcrumb.component';
import { LoaderService } from './loader/loaders.service';

import {
    JhiMainComponent,
    LayoutRoutingModule,
    AmassNavbarComponent,
    AmassFooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent,
    PageNotFoundComponent,
    WINDOW_PROVIDERS
} from './layouts';
import { ContactDialogComponent } from './shared/contactdialog/contact-dialog.component';
import { DatamarketplaceBlogModule } from './blog/blog.module';
import { DatamarketplacePressModule } from './press/press.module';
import { DatamarketplaceFeatureModule } from './features/features.module';
import { DatamarketplaceTermsModule } from './terms/index';
import { DatamarketplacePrivacyModule } from './privacy/index';
import { GoogleAnalyticsEventsService } from './shared/googleanalytics/google-analytics-events.service';
import { RecaptchaModule } from 'ng-recaptcha';
import { PasswordStrengthBarComponent } from './account';
import { IntercomModule } from 'ng-intercom';
import { INTERCOM_ID } from './app.constants';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { PasswordComponent } from './account/password/password.component';
import { PasswordResetFinishComponent } from './account/password-reset/finish/password-reset-finish.component';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { JhiEventManager } from 'ng-jhipster';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { DatamarketplaceAddProviderAdminModule } from './add-provider-admin/add-provider-admin.module';
// import { NewsLetterSignUpComponent } from './account/newsletter-signup/newsletter-signup.component';
// import { NewsLetterFooterComponent } from './account/newsletter-signup/newsletter-footer.component';
import { DatamarketplaceAddOrganizationAdminFormModule } from './add-organization-admin/add-organization-admin.module'
import { DatamarketplaceAddResourceAdminFormModule } from './add-resource-admin/add-resource-admin.module';
import { DatamarketplaceAddLinkAdminFormModule } from './entities/data-provider/data-provider-link-admin-form.module';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatamarketplaceAddProviderAdminDialogModule } from './add-provider-admin-dialog/add-provider-admin-dialog.module';
import { DataMarketplaceStripeModule } from './stripe/stripe.module';
import { AccountActivationComponent } from './account/account-activation-page/account-activation.component';
import { DatamarketplaceMndaModule } from './mnda/mnda.module';
import { DatamarketplaceResellerAgreementModule } from './reseller-agreement/reseller-agreement.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GestureConfig } from "../gesture-config";
import { DatamarketplaceNewsLetterModule } from './account/newsletter-signup/newsletter.module';
import { DatamarketplacePasswordModule } from './account/password/password.module';
import { DatamarketplaceInvestorModule } from './entities/investor/investor.module';

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './i18n/', '.json');
}

@NgModule({
	imports: [
		BrowserModule,
		HttpClientModule,
        LayoutRoutingModule,
        NgbModule,
        NgxWebstorageModule.forRoot({ prefix: 'jhi', separator: '-'}),
        ShareButtonModule,
        ShareButtonsModule.withConfig({
            debug: true
        }),
        FormsModule,
		ReactiveFormsModule,
		DatamarketplaceSharedModule,
		DatamarketplaceHomeModule,
        DataMarketplaceAboutModule,
        DataMarketplacePriceModule,
        DataMarketplaceStripeModule,
        DataMarketplaceWorksModule,
        DataMarketplaceInvestmentWorksModule,
		DatamarketplaceInvestorModule,
		DatamarketplaceContactModule,
		DatamarketplaceAdminModule,
		DatamarketplaceAccountModule,
		DatamarketplaceBlogModule,
		DatamarketplacePressModule,
		DatamarketplaceFeatureModule,
		DatamarketplaceTermsModule,
		DatamarketplacePrivacyModule,
		DatamarketplaceMndaModule,
		DatamarketplaceResellerAgreementModule,
		DatamarketplaceAddProviderAdminModule,
		DatamarketplaceAddProviderAdminDialogModule,
		DatamarketplaceAddOrganizationAdminFormModule,
		DatamarketplaceAddResourceAdminFormModule,
		DatamarketplaceAddLinkAdminFormModule,
        DatamarketplaceEntityModule,
        DatamarketplaceNewsLetterModule,
        DatamarketplacePasswordModule,
		RecaptchaModule,
		IntercomModule.forRoot({
			appId: INTERCOM_ID, // 'sxhxyqtp', // from your Intercom config
			updateOnRouterChange: true // will automatically run `update` on router event changes. Default: `false`
		}),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			}
        }),
		NoopAnimationsModule,
		HammerModule
	],
	declarations: [
		JhiMainComponent,
		AmassNavbarComponent,
		ErrorComponent,
		PageNotFoundComponent,
		PageRibbonComponent,
		ActiveMenuDirective,
		AmassFooterComponent,
		SigninDialogComponent,
        SettingsComponentComponent,
        EmailActionComponentComponent,
		ForgotPasswordDialogComponent,
		ApplyAccountComponent,
		PasswordResetFinishComponent,
		BreadcrumbComponent,
        ContactDialogComponent,
        AccountActivationComponent
	],
	providers: [
		ProfileService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthExpiredInterceptor,
			multi: true,
			deps: [Injector]
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: ErrorHandlerInterceptor,
			multi: true,
			deps: [JhiEventManager]
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: NotificationInterceptor,
			multi: true,
			deps: [Injector]
		},
		{
			provide: MatDialogRef, useValue: {}
		},
		{
			provide: MAT_DIALOG_DATA, useValue: []
		},
		PaginationConfig,
        UserRouteAccessService,
        ProviderRouteAccessService,
        UserRouteAccessUnrestrictedService,
		CanDeactivateGuard,
		CanDeactivateAddProviderAdminFormGuard,
        CanDeactivateAdminFormGuard,
		WINDOW_PROVIDERS,
		GoogleAnalyticsEventsService,
		LoaderService,
		{ provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig }
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
	bootstrap: [ JhiMainComponent ],
    entryComponents: [SigninDialogComponent, SettingsComponentComponent, EmailActionComponentComponent, ForgotPasswordDialogComponent,
     ApplyAccountComponent, ContactDialogComponent]
})
export class DatamarketplaceAppModule {}
