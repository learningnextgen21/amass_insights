import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommentsComponent } from './';
import { SidebarModule } from 'primeng/sidebar';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AmassMaterialModule, HasAnyPermissionAboveDirective } from '../shared';
import { CommentsService } from './comments.service';
import { EditorModule } from 'primeng/editor';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SidebarModule,
        AmassMaterialModule,
        EditorModule
    ],
    declarations: [CommentsComponent, HasAnyPermissionAboveDirective],
    providers: [
        CommentsService
    ],
    exports: [CommentsComponent, HasAnyPermissionAboveDirective],
    bootstrap:    [ CommentsComponent ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CommentsModule { }
