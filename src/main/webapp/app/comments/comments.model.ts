import { DataProvider } from '../entities/data-provider';
import { User } from '../shared';

export class CommentsModel {
    constructor(
        public id?: number,
        public comment?: string,
        public privacy?: string,
        public byUserId?: User,
        public releventProviders?: DataProvider[],
        public createdDate?: any
    ) {

    }
}
