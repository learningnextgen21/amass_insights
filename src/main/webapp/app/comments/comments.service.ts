import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CommentsService {
	constructor(private httpClient: HttpClient) {

	}

	submit(req: any): Observable<any> {
		return this.httpClient.post('api/data-comment', req);
    }
    filter(): Observable<any> {
        return this.httpClient.get('api/comment');
    }
}
