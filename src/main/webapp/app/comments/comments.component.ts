import { Component, OnInit, Input, OnDestroy, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { CommentsModel } from './comments.model';
import { FormGroup } from '@angular/forms';
import { CommentsService } from './comments.service';
import { DataProvider } from '../entities/data-provider';
import { Principal } from '../shared';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';
import { DataProviderService } from '../entities/data-provider/data-provider.service';
@Component({
    selector: 'jhi-comments',
    templateUrl: './comments.component.html',
    styleUrls: [
        './comments.component.css',
        '../../content/scss/amass-form.scss'
    ]
})
export class CommentsComponent implements OnInit, OnDestroy {
    @Input() comments: CommentsModel[];
    @Input() commentsButtonType: string;
    @Input() focusForm: boolean;
    @Input() displayCommentSidebar: boolean;
    @Input() user: any;
    @Input() provider: DataProvider;
    @Output() onCommentsClose = new EventEmitter();
    @Output() reloadComments = new EventEmitter();
    @Input() privacyType: any;
    comment: CommentsModel;
    commentTypes: any[];
    formGroup = new FormGroup({});
    blocked: boolean;
    commentsTitle: any;
    commentText: string;
    commentPrivateBtn: boolean;
    commentPublicBtn: boolean;
    modeselect: any;
    providerList: any;
    providerID: any;
    changeTextValue: string;

    constructor(
        private commentsService: CommentsService,
        private principal: Principal,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private dataProviderService: DataProviderService,
    ) {
        this.comment = {};
        this.commentTypes = ['private ', 'public'];
        this.providerList = {};
    }

    ngOnInit() {
       // console.log('Comments', this.comments);
        this.modeselect = 'All';
        this.blocked = !this.principal.hasAnyPermissionAboveDirect(200);
        this.commentsTitle = 'Save a Note or Post a Question/Request Related to this Provider';
        this.commentText = 'Write a private note or post a request for clarifications about this provider here...*';
        this.commentPrivateBtn = true;
        this.commentPublicBtn = true;
        this.changeTextValue = '';
    }

    ngOnDestroy() {
        this.displayCommentSidebar = false;
    }

    onShow() {
        this.comment = {};
        this.commentText = '';
       // console.log('Provider', this.provider);
        this.providerID = this.provider;
       // console.log('Provider 1', this.providerID);
        /* All comments displayed based on the privacy type*/
        if (this.privacyType === 581) { //private
            this.modeselect = 'Private';
            this.commentsTitle = 'Take a Note';
            this.commentText = 'Write a private note about this Private here...*';
            this.comment.privacy = 'Private';
            this.changeTextValue = this.comment.privacy;
            this.commentPrivateBtn = true;
            this.commentPublicBtn = false;
        } else if (this.privacyType === 582) { // public
            this.modeselect = 'Public';
            this.commentsTitle = 'Post a Comment';
            this.commentText = 'Post your thoughts or a request for clarifications about this Public here...*';
            this.comment.privacy = 'Public';
            this.changeTextValue = this.comment.privacy;
            this.commentPublicBtn = true;
            this.commentPrivateBtn = false;
        } else if (this.privacyType === 1) { // all
            this.modeselect = 'All';
            this.commentsTitle = 'Save a Note or Post a Question/Request Related to this Provider';
            this.commentText = 'Write a private note or post a request for clarifications about this All here...*';
            this.comment.privacy = 'All';
            this.changeTextValue = this.comment.privacy;
            this.commentPublicBtn = true;
            this.commentPrivateBtn = true;
        }
        if (this.focusForm && this.commentsButtonType === 'addNotes') {
            this.comment.privacy = 'private';
        } else {
            this.comment = {};
        }
    }

    onHide(event) {
        this.displayCommentSidebar = false;
        this.onCommentsClose.emit(false);
        this.modeselect = '';
            this.commentsTitle = '';
    }

    postComment() {
        this.providerList = {
            id: this.provider
        }
        if (this.comment) {
            this.comment.byUserId = {
                id: this.user.id
            };
            this.comment.releventProviders = [{
                id: this.provider && this.provider.id ? this.provider.id : this.providerList.id
            }];
        }
        this.commentsService.submit(this.comment).subscribe(data => {
            console.log('modeselect',this.modeselect);
            this.reloadComments.emit(this.modeselect);
            this.comment = {};
        }, error => {
        });
    }
    selectPrivacyFilter(filter: any) {
        this.commentsTitle = '';
        this.commentText = '';
        const values = filter;
        if (values === 581) {
            this.commentPrivateBtn = true;
            this.commentPublicBtn = false;
            // this.commentsTitle = 'Take a Note';
            this.commentText = 'Write a private note about this Private here...*';
            this.comment.privacy = 'Private';
            this.changeTextValue = this.comment.privacy;
            this.modeselect = 'Private';
        } else if (values === 582) {
            this.commentPublicBtn = true;
            this.commentPrivateBtn = false;
            // this.commentsTitle = 'Post a Comment';
            this.commentText = 'Post your thoughts or a request for clarifications about this Public here...*';
            this.comment.privacy = 'Public';
            this.changeTextValue = this.comment.privacy;
            this.modeselect = 'Public';
        } else {
            this.comment.privacy = 'All';
            this.commentPublicBtn = true;
            this.commentPrivateBtn = true;
            this.changeTextValue = this.comment.privacy;
            this.commentsTitle = 'Save a Note or Post a Question/Request Related to this Provider';
            this.commentText = 'Write a private note or post a request for clarifications about this All here...*';
            this.modeselect = 'All';
        }
        console.log('modeselect1',this.modeselect);
        this.dataProviderService.getCommentsByID(this.providerID, filter).subscribe(comments => {
            this.comments = comments;
        });
    }
    /* privacyFilter(event) {
        console.log(event);
        if (event) {
            this.commentsTitle = 'Take a Note';
        }
    } */
    privacyComment(events) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

    changeText(event: any) {
       if(event === 'Private') {
            this.commentsTitle = 'Take a Note';
       } else if(event === 'Public') {
            this.commentsTitle = 'Post a Comment';
       } else if(event === 'All') {
            this.commentsTitle = 'Save a Note or Post a Question/Request Related to this Provider';
       }
    }
}
