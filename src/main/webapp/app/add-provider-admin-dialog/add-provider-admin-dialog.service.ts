import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JhiDateUtils } from 'ng-jhipster';
import { DataProvider } from 'app/entities/data-provider/data-provider.model';
import { createRequestOption } from 'app/shared';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class DataProviderService {

	private resourceUrl = 'api/providers';
	private resourceElasticSearchUrl = 'api/es/providersearch';
	private resourceFollowUrl = 'api/follow-provider?providerID';
	private resourceExpandUrl = 'api/expand-provider?providerID';
	private resourceUnlockUrl = 'api/unlock-provider?providerID';
	private resourceConfirmExpandUrl = 'api/confirm-expand-provider?providerID';
	private resourceConfirmUnlockUrl = 'api/confirm-unlock-provider?providerID';
	private resourceCancelExpandUrl = 'api/cancel-expand-provider?providerID';
	private resourceCancelUnlockUrl = 'api/cancel-unlock-provider?providerID';
	private elasticSearchGeneric = '/api/es/qsearch';
	private resourceCancelMoreInfoUrl = 'api/cancel-contact-us-provider-more-info?providerID';
	private categoriesExplanationUrl = 'api/data-categories/desc/';
	private featuresExplanationUrl = 'api/data-features/desc/';
	private industriesExplanationUrl = 'api/data-industries/desc/';
	private providerTagsExplanationUrl = 'api/data-provider-tags/desc/';
	private providerTypesExplanationUrl = 'api/data-provider-types/desc/';
	private dataSourcesExplanationUrl = 'api/data-sources/desc/';
    private fileDownloadUrl = 'api/filedownload/';
    private providerTypesUrl = 'api/data-provider-types';
    private providerEditUrl = 'api/provider_profile_edit';
    private providerTagUrl = 'api/data-provider-tags';
    private dataProviderTagUrl = 'api/data-provider-tags';
    private providerNewTagUrl = 'api/provider-tag';
    private providerAdminUrl = 'api/providers';

	constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

	create(dataProvider: DataProvider): Observable<DataProvider> {
		const copy = this.convert(dataProvider);
		return this.http.post(this.resourceUrl, copy);
	}

	update(dataProvider: DataProvider): Observable<DataProvider> {
		const copy = this.convert(dataProvider);
		return this.http.put(this.resourceUrl, copy);
    }

    getProvidersTypes() {
		return this.http.get(this.providerTypesUrl);
	}

	find(recordID: string): Observable<DataProvider> {
		return this.http.get(`${this.resourceUrl}/${recordID}`);
	}

	findById(recordID: string): Observable<DataProvider> {
		const req = {
			'_source': {
				'exclude': [
					'ownerOrganization.description',
					'researchMethodsCompleted'
				],
				'include': [
					'assetClasses',
					'authoredArticles',
					'authoredArticles.publishedDate',
					'authoredArticles.purpose',
					'authoredArticles.summaryHTML',
					'authoredArticles.title',
					'authoredArticles.url',
					'categories',
					'collectionMethodsExplanation',
					'completeness',
					'consumerPartners.id',
					'consumerPartners.mainDataIndustry.dataIndustry',
					'consumerPartners.mainDataCategory.datacategory',
					'consumerPartners.logo.logo',
					'consumerPartners.ownerOrganization.name',
					'consumerPartners.shortDescription',
					'consumerPartners.providerName',
					'consumerPartners.recordID',
					'createdDate',
					'dataProvider',
					'dataRoadmap',
					'summary',
					'dataUpdateFrequency',
					'deliveryFrequencyNotes',
					'dataUpdateFrequencyNotes',
					'dateCollectionBegan',
					'dateCollectionRangeExplanation',
					'deliveryFormats',
					'deliveryMethods',
					'distributionPartners.id',
					'distributionPartners.mainDataIndustry.dataIndustry',
					'distributionPartners.mainDataCategory.datacategory',
					'distributionPartners.logo.logo',
					'distributionPartners.ownerOrganization.name',
					'distributionPartners.recordID',
					'distributionPartners.shortDescription',
					'distributionPartners.providerName',
					'documentsMetadata.description',
					'documentsMetadata.fileID',
					'documentsMetadata.fileName',
					'documentsMetadata.fileSize',
					'documentsMetadata.dateCreated',
					'documentsMetadata.dateModified',
					'documentsMetadata.dataResourcePurpose.purpose',
					'documentsMetadata.dataResourcePurposeType.purposeType',
					'features',
					'freeTrialAvailable',
					'geographicalFoci.description',
					'geographicalFoci.id',
					'historicalDateRangeEstimate',
					'id',
					'industries',
					'investor_clients',
					'scoreInvestorsWillingness.description',
					'investorTypes.id',
					'investorTypes.description',
					'keyDataFields',
					'logo.logo',
					'longDescription',
					'mainAssetClass',
					'mainDataCategory.datacategory',
					'mainDataCategory.id',
					'mainDataCategory.explanation',
					'mainDataIndustry.dataIndustry',
					'mainDataIndustry.id',
					'mainDataIndustry.explanation',
					'marketplaceStatus',
					'notes',
					'orgRelationshipStatus',
					'scoreOverall.description',
					'ownerOrganization',
					'potentialDrawbacks',
					'pricingModels',
					'productDetails',
					'providerName',
					'providerType.providerType',
					'providerType.id',
					'providerType.explanation',
					'providerPartners.id',
					'providerPartners.mainDataIndustry.dataIndustry',
					'providerPartners.mainDataCategory.datacategory',
					'providerPartners.logo.logo',
					'providerPartners.ownerOrganization.name',
					'providerPartners.providerName',
					'providerPartners.shortDescription',
					'providerTags',
					'providerPartners.recordID',
					'dataLanguagesAvailable',
					'sampleOrPanelSize',
					'deliveryFrequencies',
					'dataLicenseType',
					'dataLicenseTypeDetails',
					'useCasesOrQuestionsAddressed',
					'legalAndComplianceIssues',
					'publicCompaniesCovered',
					'scoreReadiness.description',
					'recordID',
					'relevantArticles',
					'securityTypes',
					'shortDescription',
					'sources',
					'scoreUniqueness.description',
					'uniqueValueProps',
					'updatedDate',
					'userPermission',
                    'scoreValue.description',
                    'deliveryMethod.lookupcode',
                    'deliveryFormat.lookupcode',
					'website',
					'relevantSectors',
					'scoreReadiness',
					'scoreUniqueness',
					'scoreInvestorsWillingness',
					'scoreValue',
                    'scoreOverall',
                    'deliveryMethods',
                    'deliveryFormats',
                    'deliveryFrequencies',
                    'freeTrialAvailable',
                    'link',
                    'relevantSectors',
                    'createdDate'
				]
			},
			'filter': {
				'query': {
					'match': {
						'recordID': recordID
					}
				}
			}
		};
		return this.http.post(this.resourceElasticSearchUrl + '?detail=true', req);
	}

	query(req?: any): Observable<HttpResponse<DataProvider[]>> {
		const options = createRequestOption(req);
		return this.http.post<DataProvider[]>(this.resourceElasticSearchUrl, req, { observe: 'response' });
	}

	elasticSearchAuthoredArticles(recordID: string, from: number): Observable<any> {
		const query = {
			'_source': {
				'include': [
					'id',
					'title',
					'url',
					'author',
					'authorOrganization.name',
					'publishedDate',
					'authorDataProvider.id',
					'visualUrl',
					'authorDataProvider.providerName',
					'createdDate',
					'summary',
					'summaryHTML',
					'stream',
					'purpose',
					'articleTopics'
				]
			},
			'sort': [
				{
					'publishedDate': 'desc'
				}
			],
			'from': from,
			'filter': {
				'query': {
					'bool': {
						'should': {
							'match':
								{ 'authorDataProvider.recordID': recordID }
						}
					}
				}
			}
		};
		return this.http.post(this.elasticSearchGeneric + '?type=dataarticle', query);
	}

	elasticSearchRelevantArticles(recordID: string, from: number): Observable<any> {
		const query = {
			'_source': {
				'include': [
					'id',
					'title',
					'url',
					'author',
					'authorOrganization.name',
					'publishedDate',
					'authorDataProvider.id',
					'visualUrl',
					'authorDataProvider.providerName',
					'createdDate',
					'summary',
					'summaryHTML',
					'stream',
					'purpose',
					'articleTopics'
				]
			},
			'sort': [
				{
					'publishedDate': 'desc'
				}
			],
			'from': from,
			'filter': {
				'query': {
					'bool': {
						'should': {
							'match':
								{ 'relevantProviders.recordID': recordID }
						}
					}
				}
			}
		};
		return this.http.post(this.elasticSearchGeneric + '?type=dataarticle', query);
	}

	queryWithSearchFilter(req?: any, statusFilter?: any): Observable<any> {
		const newReq = Object.assign({
			'_source': {
				'include': [
					'id',
					'createdDate',
					'completeness',
                    'categories.id',
					'providerName',
					'mainDataCategory',
					'mainDataIndustry',
					'shortDescription',
					'marketplaceStatus',
					'userPermission',
					'recordID',
					'logo.logo',
					'updatedDate',
					'ownerOrganization',
					'scoreReadiness',
					'scoreInvestorsWillingness',
					'scoreUniqueness',
					'scoreValue',
                    'scoreOverall',
                    'deliveryMethods',
                    'deliveryFormats',
                    'deliveryFrequencies',
                    'freeTrialAvailable',
                    'relevantSectors',
                    'createdDate',
                    'providerTags'
				]
			}
		}, req);
		const options = createRequestOption(newReq);
		let elasticSearchUrl = '';
		switch (statusFilter) {
			case 'Followed':
				elasticSearchUrl = this.resourceElasticSearchUrl + '?followedProviders=true';
				break;
			case 'Unlocked':
				elasticSearchUrl = this.resourceElasticSearchUrl + '?unlockedProviders=true';
				break;
			default:
				elasticSearchUrl = this.resourceElasticSearchUrl;
				break;
		}

		return this.http.post(elasticSearchUrl, newReq);
	}

	getMouseOverExplanation(id: number, type: string): Observable<any> {
		let url;
		switch (type) {
			case 'dataCategory':
			url = this.categoriesExplanationUrl + id;
			break;
			case 'dataFeature':
			url = this.featuresExplanationUrl + id;
			break;
			case 'dataIndustry':
			url = this.industriesExplanationUrl + id;
			break;
			case 'dataProviderTag':
			url = this.providerTagsExplanationUrl + id;
			break;
			case 'dataProviderType':
			url = this.providerTypesExplanationUrl + id;
			break;
			case 'dataSource':
			url = this.dataSourcesExplanationUrl + id;
			break;
			default:
			url = null;
			break;
		}
		return this.http.get(url, {responseType: 'text'});
    }

    getMouseOverExplanationByType(url: string): Observable<any> {
		return this.http.get('api/' + url + '?size=500');
	}

	downloadFile(fileID: string): Observable<any> {
		return this.http.get(this.fileDownloadUrl + fileID);
	}

	delete(id: number): Observable<any> {
		return this.http.delete(`${this.resourceUrl}/${id}`);
	}

	search(req?: any): Observable<HttpResponse<any>> {
		const options = createRequestOption(req);
		return this.http.post(this.resourceElasticSearchUrl, req, { observe: 'response' });
    }

    providerEdit(req?: any): Observable<any> {
        const model = req;
        delete model['id'];
        if (model && model.dataUpdateFrequency) {
            model.dataUpdateFrequency = model.dataUpdateFrequency.lookupcode;
        }
        if (model && model.scoreInvestorsWillingness) {
            model.scoreInvestorsWillingness = model.scoreInvestorsWillingness.id;
        }
        if (model && model.pricingModels && model.pricingModels.length > 0) {
            model.pricingModels = model.pricingModels[0].desc;
        } else {
            model.pricingModels = '';
        }
        if (model && model.mainAssetClass) {
            model.mainAssetClass = model.mainAssetClass.id;
        }
        if (model && model.ownerOrganization) {
            model['city'] = model.ownerOrganization.city;
            model['state'] = model.ownerOrganization.state;
            model['country'] = model.ownerOrganization.country;
            model['zipCode'] = model.ownerOrganization.zipCode;
            model['headCount'] = model.ownerOrganization.headCount;
            model['yearFounded'] = model.ownerOrganization.yearFounded;
        }
		return this.http.post(this.providerEditUrl, model);
    }
    providerAdmin(request?: any): Observable<any> {
        const model = request;
        if (request && request.mainAssetClass) {
            model.mainAssetClass = {
                id: request.mainAssetClass.id
            };
        }
        if (request && request.scoreInvestorsWillingness) {
            model.scoreInvestorsWillingness = {
                id: request.scoreInvestorsWillingness.id
            };
        }
        if (request && request.scoreOverall) {
            model.scoreOverall = {
                id: request.scoreOverall.id
            };
        }
        if (request && request.scoreReadiness) {
            model.scoreReadiness = {
                id: request.scoreReadiness.id
            };
        }
        if (request && request.scoreUniqueness) {
            model.scoreUniqueness = {
                id: request.scoreUniqueness.id
            };
        }
        if (request && request.scoreValue) {
            model.scoreValue = {
                id: request.scoreValue.id
            };
        }
        return this.http.put(this.providerAdminUrl, model);
    }

    getProviderEdit(recordID?: string): Observable<DataProvider> {
        return this.http.get('api/provider_profile_edit/' + recordID);
    }

	elasticSearch(req?: any): Observable<HttpResponse<any>> {
		const options = createRequestOption(req);
		return this.http.post(this.resourceElasticSearchUrl, req, {observe: 'response'});
	}

	getTotal(): Observable<HttpResponse<any>> {
		return this.http.post(this.resourceElasticSearchUrl, {
			size: 0,
			aggs: {

			}
		}, {observe: 'response'});
    }

     tagList(list): Observable<any> {
        return this.http.post(this.dataProviderTagUrl, {
                'tagProviders': [
                    {
                        id: list.providerID
                      }
                    ],
                  'providerTag': list.tag,
                  'privacy': 'Private'
        });
    }
    newTagList(newtag): Observable<any> {
        return this.http.post(this.providerNewTagUrl, {}, {
            params: {
                tagId: newtag.tagId,
                providerId: newtag.providerId
            }
        });
    }

	followAction(id?: number): Observable<any> {
		return this.http.get(`${this.resourceFollowUrl}=${id}`, {});
	}

	expandAction(id?: number): Observable<any> {
		return this.http.post(`${this.resourceExpandUrl}=${id}`, '');
	}

	confirmExpandAction(id?: number): Observable<any> {
		return this.http.post(`${this.resourceConfirmExpandUrl}=${id}`, '');
	}

	cancelExpandAction(id?: number): Observable<any> {
		return this.http.post(`${this.resourceCancelExpandUrl}=${id}`, '');
	}

	unlockAction(id?: number): Observable<any> {
		return this.http.post(`${this.resourceUnlockUrl}=${id}`, '');
	}

	confirmUnlockAction(id?: number): Observable<any> {
		return this.http.post(`${this.resourceConfirmUnlockUrl}=${id}`, '');
	}

	cancelUnlockAction(id?: number): Observable<any> {
		return this.http.post(`${this.resourceCancelUnlockUrl}=${id}`, '');
	}

	cancelMoreInformationAction(id?: number, interest?): Observable<any> {
		return this.http.get(`${this.resourceCancelMoreInfoUrl}=${id}&interestType=${interest}`, {});
    }

    getAllDataCategories(): Observable<any> {
        return this.http.get('api/all-data-categories');
    }

    getCommentsByID(recordID: string): Observable<any> {
        return this.http.get('api/comment?recordID=' + recordID, {});
    }

	private convert(dataProvider: DataProvider): DataProvider {
		const copy: DataProvider = Object.assign({}, dataProvider);
		copy.createdDate = this.dateUtils
			.convertLocalDateToServer(dataProvider.createdDate);

		copy.updatedDate = this.dateUtils.toDate(dataProvider.updatedDate);
		return copy;
	}
}
