
import {of as observableOf, Subject} from 'rxjs';
import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, Inject, Injectable, Input, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { DomSanitizer } from '@angular/platform-browser';
import { DataProvider } from 'app/entities/data-provider/data-provider.model';
import { DataProviderService } from 'app/entities/data-provider/data-provider.service';
import { Principal } from 'app/shared/auth/principal.service';
import { DataSource } from '@angular/cdk/table';
import { GoogleAnalyticsEventsService } from 'app/shared/googleanalytics/google-analytics-events.service';
import { LoaderService } from '../loader/loaders.service';
import { WINDOW } from 'app/layouts';
import { SigninModalService, AmassFilterService, CSRFService, AuthServerProvider, StateStorageService } from 'app/shared';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { ConfirmDialogComponent } from 'app/shared/dialogs/confirm-dialog.component';
import { UppyService, UppyComponent } from 'app/uppy';
import { contains } from 'ramda';
let _that = null;
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie';

@Component({
	selector: 'jhi-add-provider-admin-dialog',
	templateUrl: './add-provider-admin-dialog.component.html',
	styleUrls: [ '../../content/css/material-tab.css']
})
export class AddProviderAdminDialogComponent implements OnInit, AfterViewInit {
	dialogRef: MatDialogRef<ConfirmDialogComponent>;
	formSubmitted: boolean;
	valueChanged: boolean;
    routerOutletRedirect: string;

	dataProvider: DataProvider;
	private subscription: Subscription;
	private eventSubscriber: Subscription;
	isLoading: boolean;
	isAdmin: boolean;
	isPendingUnlock: boolean;
	isPendingExpand: boolean;
	domSanitizer: any;
	authoredArticles: any;
	relevantArticles: any;
	tabIndex: number;
	relevantArticlePage: number;
	relevantArticleLinks: any;
	relevantArticleFrom: number;
	totalRelevantArticles: number;
	authoredArticlePage: number;
	authoredArticleLinks: any;
	authoredArticleFrom: number;
	totalAuthoredArticles: number;
	recordID: string;
	profileTabLabel: any;
    ProfileCategory: any;
    trackname: any;
    trackwebsite: any;
    trackownerOrganizationCity: any;
    trackownerOrganizationState: any;
    trackownerOrganizationCountry: any;
    trackownerOrganizationYearfound: number;
    trackownerOrganizationHeadcount: any;
    trackInvestor: any;
    trackDataCollection: any;
    equitesDelete: any;

	progressBarColor = 'primary';
	progressBarMode = 'determinate';
	mdTooltipDelay: number;

	dataProviderTypes: any[];
	mainDataIndustriesArr: any[];
	dataIndustries: any[];
	mainDataCategories: any[];
	dataCategories: any[];
	dataFeatures: any[];
	dataSources: any[];
	geographicalFocus: any[];
	providerTags: any[];
    providerWillingness: any[];
    providerUniquness: any[];
    providerReadiness: any[];
    providerOverall: any[];
    providerValue: any[];
	providerRelevantInvestors: any[];
	providerMainAssetClass: any[];
	providerRelevantAssetClass: any[];
	relevantSecurityTypes: any[];
	relevantSectors: any[];
	deliveryMethods: any[];
	deliveryFormats: any[];
	deliveryFrequencies: any[];
	dataUpdateFrequency: any[];
	pricingModel: any[];

	basicExpansionPanel: boolean;
	companyExpansionPanel: boolean;
	descriptionsExpansionPanel: boolean;
	categorizationsExpansionPanel: boolean;
	investigationExpansionPanel: boolean;
	relatedProvidersExpansionPanel: boolean;
	dataDetailsExpansionPanel: boolean;
	dataDeliveryExpansionPanel: boolean;
	triallingExpansionPanel: boolean;
	licensingExpansionPanel: boolean;
	pricingExpansionPanel: boolean;
	discussionExpansionPanel: boolean;

	basicForm = new FormGroup({});
	basicModel: any = {};
	providerFormModel: any = {};
	basicOptions: FormlyFormOptions = {};

	companyDetailsForm = new FormGroup({});
	companyDetailsModel: any = {};
	companyDetailsOptions: FormlyFormOptions = {};

	descriptionsForm = new FormGroup({});
	descriptionsModel: any = {};
	descriptionsOptions: FormlyFormOptions = {};

	categorizationsForm = new FormGroup({});
	categorizationsModel: any = {};
	categorizationsOptions: FormlyFormOptions = {};

	investigationForm = new FormGroup({});
	investigationOptions: FormlyFormOptions = {};

	relatedProvidersForm = new FormGroup({});
	relatedProvidersOptions: FormlyFormOptions = {};

	dataDetailsForm = new FormGroup({});
    dataDetailsOptions: FormlyFormOptions = {};

    dataQualityForm = new FormGroup({});
	dataQualityOptions: FormlyFormOptions = {};

	dataDeliveryForm = new FormGroup({});
	dataDeliveryOptions: FormlyFormOptions = {};

	triallingForm = new FormGroup({});
	triallingOptions: FormlyFormOptions = {};

	licensingForm = new FormGroup({});
    licensingOptions: FormlyFormOptions = {};

    pricingForm = new FormGroup({});
	pricingOptions: FormlyFormOptions = {};

	discussionForm = new FormGroup({});
	discussionOptions: FormlyFormOptions = {};

	basicFields: FormlyFieldConfig[] = [
		{
			key: 'providerName',
			type: 'input',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Provider Name',
				placeholder: 'Name of the data provider.',
				description: 'What is the name of your brand which acts as a data provider? This is often the name of the company.',
				required: true,
				attributes: {
					'labelTooltip': 'Usually found in the logo on the website. This should be the name of the brand or division of the organization that collects/provides/analyzes data. It is not always the same name as the organization (but it often is).',
                    'descriptionTooltip': 'Usually found in the logo on the website. This should be the name of the brand or division of the organization that collects/provides/analyzes data. It is not always the same name as the organization (but it often is).',
                    'placeholderTooltip': 'Usually found in the logo on the website. This should be the name of the brand or division of the organization that collects/provides/analyzes data. It is not always the same name as the organization (but it often is).'
				},
				change: this.basicInput,
				keypress:(event)=> {
					if(event) {
					  this.valueChanged = true;
					}
				},
			},
		},
		{
			key: 'website',
			type: 'input',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Provider Website',
				type: 'input',
				placeholder: 'Link to the website of the data provider or the division within the company/organization that provides/analyzes data.',
				description: 'What is the link to the home webpage of your brand that acts as a data provider?',
				required: true,
				attributes: {
					'labelTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                    'descriptionTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.',
                    'placeholderTooltip': 'If your data business is a brand or division of a larger company, this should be the most appropriate URL to the data provider business.'
				},
				change: this.basicInput,
				keypress:(event)=> {
					if(event) {
					  this.valueChanged = true;
					}
				},
            },
            validation: {
                messages: {
                    required: 'Website is required'
                }
            }
		},
		{
			key: '',
			type: 'fileUpload',
			className: 'customWidth',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Provider Logo',
				change: ( event : any) => {
					this.dataProviderService.onUpload(this.recordID, event[0]).subscribe(data=>{
					});
				}
			}
		}
	];

	companyDetailsFields: FormlyFieldConfig[] = [
        /* {
			key: 'ownerOrganization',
			type: 'input',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Owner Organization:',
                required: true,
				placeholder: 'Organization (company/subsidiary) that owns this data provider.',
                attributes: {
                    'labelTooltip': 'Often this is just one link to an organization of the same name.',
                    'placeholderTooltip':'Often this is just one link to an organization of the same name.'
                }
			},
		}, */
        {
			key: 'ownerOrganization.city',
			type: 'input',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'City:',
                type: 'input',
                required: true,
				description: 'City of the headquarters of the owner organization.',
				change: this.basicInput
			},
		},
		{
			key: 'ownerOrganization.state',
			type: 'input',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'State:',
				type: 'input',
				description: 'State of the headquarters of the owner organization.',
				change: this.basicInput
			},
		},
        {
			key: 'ownerOrganization.country',
			type: 'input',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Country:',
                type: 'input',
                required: true,
				description: 'Country of the headquarters of the organization.',
				change: this.basicInput
			},
		},
		{
			key: 'ownerOrganization.zipCode',
			type: 'mask',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Zip Code:',
				description: 'Zip code of the headquarters of the owner organization.',
				attributes: {
					'mask': '99999'
				}
			},
		},
		{
			key: 'ownerOrganization.yearFounded',
			type: 'input',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Year Founded:',
				description: 'Year that the owner organization was founded.',
				change: this.basicInput
			},
		},
		{
			key: 'ownerOrganization.headCount',
			type: 'input',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Headcount Details::',
				description: 'ADetails regarding the number of people that work at the organization.',
				change: this.basicInput
			},
		},
	];

	descriptionsFields: FormlyFieldConfig[] = [
		{
			key: 'shortDescription',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Short Description:',
				minRows: 2,
				maxRows: 100,
				required: true,
				placeholder: 'Short one sentence (or less) description of the specific types of data or information that the data provider collects/provides/analyzes.',
                description: '',
                attributes: {
					'labelTooltip': 'Do not include the name of the provider. This should be a brief (1 sentence or less) summary of the specific data/analytics/products that the provider offers that are relevant to investment research or analysis. This is not just a general description of the company and everything they do.If there are several datasets or analytics, describe each briefly, or group them together.Also, it needs to be specific enough to be understood in such a brief statement, so not using any of their ""marketing jargon"".Example: if the company does consulting for the healthcare sector but also provides healthcare analytics to hospitals, this description should focus on the analytics not the consulting.This needs to be informative on the data component of the business and re-phrased in order to be more subjective. Example: The short description you originally wrote just describes who ChannelAdvisor helps, not how they are helping them and what the company is actually doing. And for ChannelAdvisor, since it doesn\'t directly operate in the finance industry, we don\'t really care who they are helping, but more about the core of the data they are collecting (in the hopes that data or analysis is also useful in our use case).',
                    'placeholderTooltip': 'Do not include the name of the provider.This should be a brief (1 sentence or less) summary of the specific data/analytics/products that the provider offers that are relevant to investment research or analysis. This is not just a general description of the company and everything they do.If there are several datasets or analytics, describe each briefly, or group them together.Also, it needs to be specific enough to be understood in such a brief statement, so not using any of their ""marketing jargon"".Example: if the company does consulting for the healthcare sector but also provides healthcare analytics to hospitals, this description should focus on the analytics not the consulting.This needs to be informative on the data component of the business and re-phrased in order to be more subjective. Example: The short description you originally wrote just describes who ChannelAdvisor helps, not how they are helping them and what the company is actually doing. And for ChannelAdvisor, since it doesn\t directly operate in the finance industry, we don\'t really care who they are helping, but more about the core of the data they are collecting (in the hopes that data or analysis is also useful in our use case).',
				}
			},
		},
		{
			key: 'longDescription',
			type: 'inputEditor',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Long Description:',
                required: true,
				placeholder: 'Longer description of the data provider, its datasets, and wider business. Usually 1-3 paragraphs in length.',
                description: '',
                attributes:{
                    'labelTooltip': 'Quickest way to fill this in is by copying straight from the data provider\'s website or other resource (suggestions listed below). This should be a more general description of the data provider\'s businesses and operations. But it should be also specific enough in nature to have a pretty thorough understanding of what the data provider does.Eliminate paragraphs that do not give any information about what the company does, or a sentence explaining the founder and office locations. Do not go line by line cleaning up the description. The description does not have to explicitly include all products and services, but it should reference all the capabilities provided.It\'s sometimes difficult to capture this precisely from the homepage - try looking through the ""About"" section of the website.Other good sources to find the core functions of a data provider are: Crunchbase Angellist Wikipedia LinkedIn Do not include information that appears elsewhere in this database, such as date founded, location, CEO, or the like.',
                    'placeholderTooltip': 'Quickest way to fill this in is by copying straight from the data provider\'s website or other resource (suggestions listed below). This should be a more general description of the data provider\'s businesses and operations. But it should be also specific enough in nature to have a pretty thorough understanding of what the data provider does.Eliminate paragraphs that do not give any information about what the company does, or a sentence explaining the founder and office locations. Do not go line by line cleaning up the description. The description does not have to explicitly include all products and services, but it should reference all the capabilities provided.It\'s sometimes difficult to capture this precisely from the homepage - try looking through the ""About"" section of the website.Other good sources to find the core functions of a data provider are: Crunchbase Angellist Wikipedia LinkedIn Do not include information that appears elsewhere in this database, such as date founded, location, CEO, or the like.'
                }
			},
		},
	];

	categorizationsFields: FormlyFieldConfig[];

	investigationFields: FormlyFieldConfig[] = [
		{
			key: 'scoreInvestorsWillingness.id',
			type: 'dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Willingness to Provide to Asset Managers Score:*',
				description: 'How willing is the data provider to sell their data or analytics products to the institutional investment industry (e.g. hedge funds)?',
				required: false,
				options: this.providerWillingness,
				attributes: {
					field: 'label'
				}
			},
        },
        /* -----------------------------------newly added admin field----------------- */
        /* ----------------------------newly added dropdown-------------------------- */
        {
			key: 'scoreUniqueness.id',
			type: 'dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Uniqueness Score:',
				placeholder: 'An Amass-created subjective score of how unique this provider\'s data or analytics is compared to traditional investment research techniques.',
				required: false,
				options: this.providerUniquness,
				attributes: {
					field: 'label'
				}
			},
        },
        {
			key: 'scoreValue.id',
			type: 'dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Value Score:',
				placeholder: 'An Amass-created subjective score of how valuable this provider\'s data or analytics is compared to traditional investment research techniques.',
				required: false,
				options: this.providerValue,
				attributes: {
					field: 'label'
				}
			},
        },
        {
			key: 'scoreReadiness.id',
			type: 'dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Readiness Score:',
				placeholder: 'An Amass-created subjective score of how ready this provider\'s data or analytics is to be deployed within investment research.',
				required: false,
				options: this.providerReadiness,
				attributes: {
					field: 'label'
				}
			},
        },
        {
			key: 'scoreOverall.id',
			type: 'dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Overall Score:',
				placeholder: 'An Amass-created subjective score of the overall importance of integrating this provider\'s data or analytics into an investment research process.',
				required: false,
				options: this.providerOverall,
				attributes: {
					field: 'label'
				}
			},
		},
		{
			key: 'investor_clients',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Number of Investor Clients Details:',
				description: 'How many clients in the investment management industry does this provider have?',
                change: this.basicInput,
                attributes: {
                    'labelTooltip': 'Usually the provider will not offer up this information, especially if they sell to the investment management industry, but may just give you a sense for how many.',
                    'descriptionTooltip': 'Usually the provider will not offer up this information, especially if they sell to the investment management industry, but may just give you a sense for how many.'
                }
            },
		},
		{
			key: 'investorTypes',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Relevant Investor Type:',
				placeholder: 'The types of investors the provider\'s data would be useful for or would have the ability to use.',
				description: 'What types of investors will this data provider\'s offerings be useful for?',
                options: this.providerRelevantInvestors,
                attributes: {
                    'labelTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                    'descriptionTooltip': 'If applicable, use the provider\'s previous client types to fill this in.',
                    'placeholderTooltip': 'If applicable, use the provider\'s previous client types to fill this in.'

                }
			},
		},
		{
			key: 'mainAssetClass.id',
			type: 'dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Main Asset Class:*',
                required: false,
				placeholder: 'Sometimes the data provider will belong in more than one asset class - choose the primary asset class, and add all other asset classes in the following question.',
				description: 'Which security asset class is the provider\'s data offerings the most useful to research?',
				options: this.providerMainAssetClass,
				attributes: {
                    field: 'label',
                        'labelTooltip': 'For some Providers this will be explicitly stated. Bonds are part of the Fixed Income class, stocks are Public Equities, Foreign Exchange is Currencies. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.',
                        'descriptionTooltip': 'For some Providers this will be explicitly stated. Bonds are part of the Fixed Income class, stocks are Public Equities, Foreign Exchange is Currencies. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.',
                        'placeholderTooltip': 'For some Providers this will be explicitly stated. Bonds are part of the Fixed Income class, stocks are Public Equities, Foreign Exchange is Currencies. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.'
				}
			},
		},
		{
			key: 'assetClasses',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Relevant Asset Classes:',
                required: true,
				placeholder: 'All of the financial security asset classes that the provider\'s data can be used for or has any relevance towards.',
				description: 'What are all the asset classes this provider\'s data offerings can be used to research?',
                options: this.providerRelevantAssetClass,
                attributes: {
                    'labelTooltip': 'At minimum, the Main Asset Class should be listed here. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.',
                    'descriptionTooltip': 'At minimum, the Main Asset Class should be listed here. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.',
                    'placeholderTooltip': 'At minimum, the Main Asset Class should be listed here. Sometimes it won\'t be stated or immediately obvious, but you need to use intuition to decide which asset class the given Provider is most relevant to. For example, a Provider with information about agriculture, materials, or oil & gas is most relevant to Commodities. A Provider with information about Transactions is most relevant to Public Equities and Private Equities. Ask if you are unsure which asset class to use.'
                }
			},
		},
		{
			key: 'securityTypes',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Relevant Security Types:',
                required: true,
				placeholder: 'All of the financial security types the provider\'s data can be used for or has any relevance towards.',
				description: 'What are all the security types this provider\'s data offerings can be used to research?',
                options: this.relevantSecurityTypes,
                attributes: {
                    'labelTooltip': 'Only add the obvious choices here. If the website talks about their data\'s use for specific securities add them. Look through the list of Security types. These are specific names, and will often be referred to directly.',
                    'descriptionTooltip': 'Only add the obvious choices here. If the website talks about their data\'s use for specific securities add them. Look through the list of Security types. These are specific names, and will often be referred to directly.',
                    'placeholderTooltip': 'Only add the obvious choices here. If the website talks about their data\'s use for specific securities add them. Look through the list of Security types. These are specific names, and will often be referred to directly.'

                }
			},
		},
		{
			key: 'relevantSectors',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Relevant Sectors:',
                required: true,
				placeholder: 'All of the sectors the provider\'s data could be used for or has any relevance towards.',
				description: 'What sectors are the companies/securities in that this data could be used for?',
				options: this.relevantSectors
			},
		},
		{
			key: 'publicCompaniesCovered',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Public Companies Covered Details:',
				minRows: 2,
				maxRows: 100,
				placeholder: 'The public companies/tickers the provider\'s data can be used for or are relevant towards.',
				description: 'What companies/tickers can this data be used for?',
				required: true,
			},
		},
	];

	relatedProvidersFields: FormlyFieldConfig[] = [
	];

	dataDetailsFields: FormlyFieldConfig[] = [
		{
			key: 'collectionMethodsExplanation',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Collection Methods Explanation:',
				minRows: 2,
				maxRows: 100,
				placeholder: 'Details about how and where this provider gets their raw data',
				description: 'Provide an overview of how the provider collects their data. Has there been any changes to their collection methods over time? If so, does this affect the ability to compare between time periods?',
			},
		},
		{
			key: 'sampleOrPanelSize',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Sample or Panel Size:',
				minRows: 2,
				maxRows: 100,
				placeholder: 'Details about the sample or panel size of this provider\'s raw data',
			},
		},
		{
			key: 'dateCollectionBegan',
			type: 'input',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Date Collection Began:',
				type: 'input',
				description: 'What is the approximate earliest date of consistent data within your offerings?',
                change: this.basicInput,
                attributes: {
                    'labelTooltip': 'the date format is US style: MM/DD/YYYY (month then day then year)',
                    'descriptionTooltip': 'the date format is US style: MM/DD/YYYY (month then day then year)'
                }
			},
		},
		{
			key: 'dateCollectionRangeExplanation',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Date Collection Range Explanation:',
				minRows: 2,
                maxRows: 100,
                placeholder: 'Details about how much historical data this provider has',
			},
		},
		{
			key: 'outOfSampleData',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Out-of-Sample Data:',
				minRows: 2,
				maxRows: 100,
				description: 'If applicable, when did the data provider\'s data become out-of-sample (or when did people first begin trading on it)?',
			},
		},
		{
			key: 'productDetails',
			type: 'inputEditor',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Product Details:',
                placeholder: 'Further details regarding the provider\'s data products, such as pricing or details that don\'t fit into any other field.'
			},
		},
    ];
    /*---------------------------------- newly added section for admin------------------------------- */
    /* dataQualityFields: FormlyFieldConfig[] = [
        {
			key: 'qualityMethods',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Sample or Panel Biases:',
				description: 'Are there any known biases within the data sample or panel, such as certain demographics, geographies or other common characteristics?',
			},
        },
        {
			key: 'qualityMethods',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
                label: 'Discussion - Cleanliness:',
                placeholder: 'For example, have you backfilled or imputed certain data?',
				description: 'Discuss the effort the data provider has put into clean and/or normalizing their data, such as the work done and the methods used.',
			},
		},
    ] */
	dataDeliveryFields: FormlyFieldConfig[] = [
		{
			key: 'deliveryMethods',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Delivery Methods:',
				description: 'Using what methods does this provider typically deliver their data products?',
				options: this.deliveryMethods
			},
		},
		{
			key: 'deliveryFormats',
			type: 'multiselect',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Delivery Formats:',
				description: 'In what formats does this provider typically deliver their data products?',
				options: this.deliveryFormats
			},
        },
        /* -------------------------newly added dropdowm----------------- */
		{
			key: 'deliveryFrequencies',
			type: 'dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Delivery Frequency:',
				description: 'How often does this provider typically deliver their data or analytics?',
                options: this.deliveryFrequencies
			},
		},
		{
			key: 'deliveryFrequencyNotes',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Delivery Frequency Notes:',
				minRows: 2,
				maxRows: 100,
				description: 'Please provide any additional details regarding how often the provider is able to deliver their data offerings.',
			},
		},
		{
			key: 'dataUpdateFrequency',
			type: 'dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data Update Frequency:',
				description: 'How often does this provider typically update their data products?',
				options: this.dataUpdateFrequency,
				attributes: {
					field: 'lookupcode',
					'dropdown': 'true'
				},
				change: (event: any) => {
					const queryObj = event;
					this.dataUpdateFrequency = [];

					this.filterService.getDataUpdateFrequency(queryObj.query).subscribe(d => {
						const listArray = [];
						const listItems = (d && d['hits'].total) ? d['hits']['hits'] : [];
						for (let i = 0; i < listItems.length; ++i) {
							const item = listItems[i]._source;

							listArray.push({
								id: item.id,
								lookupcode: item.lookupcode,
								recordCount: item.recordCount
							});
						}
						this.dataUpdateFrequency = [...listArray];
						this.dataDeliveryFields[4].templateOptions.options = this.dataUpdateFrequency;
					});
				}
			},
		},
		{
			key: 'dataUpdateFrequencyNotes',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data Update Frequency Notes:',
				minRows: 2,
				maxRows: 100,
				placeholder: 'Details about the frequency that this provider updates their data products',
			},
		},
	];

	triallingFields: FormlyFieldConfig[] = [
		{
			key: 'freeTrialAvailable',
			type: 'checkbox',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Free Trial Available:',
				type: 'checkbox',
				description: 'Is a free trial available for this provider\'s data products?',
			},
		},
		{
			key: 'trialDuration',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Trial Duration:',
				minRows: 2,
				maxRows: 100,
				description: 'For how long can the data provider offer a trial?',
			},
		},
	];

	licensingFields: FormlyFieldConfig[] = [
		{
			key: 'dataLicenseTypeDetails',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data License Type Details:',
				minRows: 2,
                maxRows: 100,
                placeholder: 'Any relevant additional details regarding the provider\'s data licenses'
			},
		},
    ];

    /* ----------------------------- newly added admin form fields ---------------------------- */

     pricingFields: FormlyFieldConfig[] = [
		{
			key: 'pricingModels.0',
			type: 'dropdown',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Pricing Model:*',
				placeholder: 'The general model for how you price your data products.',
				description: 'What\'s the general model for how this provider prices their data products?',
				required: false,
				options: this.pricingModel,
				attributes: {
					field: 'desc',
					'dropdown': 'true'
				},
				change: (event: any) => {
					const queryObj = event;
					this.pricingModel = [];

					this.filterService.getPricingModel(queryObj.query).subscribe(d => {
						const listArray = [];
						const listItems = (d && d['hits'].total) ? d['hits']['hits'] : [];
						for (let i = 0; i < listItems.length; ++i) {
							const item = listItems[i]._source;

							listArray.push({
								id: item.id,
								desc: item.description,
								locked: item.locked ? true : false
							});
						}
						this.pricingModel = [...listArray];
						this.pricingFields[0].templateOptions.options = this.pricingModel;
					});
				}
			},
        },
    ]

	discussionFields: FormlyFieldConfig[] = [
		{
			key: 'useCasesOrQuestionsAddressed',
			type: 'inputEditor',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Use Cases or Questions Addressed:',
				placeholder: 'Ideas for use cases or questions that could be answered by this provider\'s data products within investment research.',
			},
		},
		{
			key: 'discussionCleanlines',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Discussion - Cleanliness:',
				minRows: 2,
				maxRows: 100,
				description: 'Please discuss the effort you have put into clean and/or normalizing your data, such as the work done and the methods used.',
			},
		},
		{
			key: 'discussionsAnalytics',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Discussion - Analytics:',
				minRows: 2,
				maxRows: 100,
				description: 'Discuss the effort the data provider has put into analyzing, aggregating and/or productizing their data, such as any backtests performed or research/analytics/summary products created.',
			},
		},
		{
			key: 'potentialDrawbacks',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Potential Drawbacks:',
				minRows: 2,
                maxRows: 100,
                placeholder: 'Downsides to this provider\'s offering that decrease the overall appeal of its products for investment research'
			},
        },
        /* ------------------------------newly added admin form field---------------------------- */
        {
			key: 'uniqueValueProps',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Unique Value Props:',
				minRows: 2,
                maxRows: 100,
                description: 'What makes the data provider or their data products unique or differentiated?',
                placeholder: 'Unique aspects of this provider\'s offerings that increase the overall appeal of its products for investment research.'
			},
        },
        {
			key: 'legalComplianceIssues',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Legal & Compliance Issues:',
				minRows: 2,
                maxRows: 100,
                placeholder: 'Glaring or obvious legal or compliance issues to be aware of'
			},
        },
        /* ---------------------------------------------------------- */
		{
			key: 'dataRoadmap',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Data Roadmap:',
				minRows: 2,
				maxRows: 100,
				description: 'What are the data provider\'s future plans regarding their data and analytics? Any forthcoming new products?',
			},
		},
		{
			key: 'notes',
			type: 'textarea',
			wrappers: ['form-field-horizontal'],
			templateOptions: {
				label: 'Notes:',
				minRows: 2,
				maxRows: 100,
				placeholder: 'Other further useful notes that don\'t fit into any other field.',
			},
		},
	];

	csrf: string;
	// uppyEvent = new Subject<[string, any, any, any]>();
	// uppyPlugins = [
	// 	['Dashboard', {
	// 		target: '.DashboardContainer',
	// 		replaceTargetContent: true,
	// 		inline: true,
	// 		restrictions: {
	// 			maxFileSize: 200000000,
	// 			maxNumberOfFiles: 10,
	// 			minNumberOfFiles: 1,
	// 			allowedFileTypes: ['image/*', 'video/*']
	// 		}
	// 	}],
	// 	['XHRUpload', {
	// 		endpoint: '/api/ur/multiple-file-upload?providerId=' + this.recordID,
	// 		formData: true,
	// 		bundle: true,
	// 		fieldName: 'fileUploads'
    //     }]/* ,
	// 	['GoogleDrive', {
	// 		target: 'Dashboard',
	// 		serverUrl: 'https://companion.uppy.io'
	// 	}],
	// 	['Dropbox', { target: 'Dashboard', serverUrl: 'https://companion.uppy.io' }] */
	// ];
	// onDestroy$ = new Subject<void>();

	constructor(
		private dataProviderService: DataProviderService,
	    private filterService: AmassFilterService,
	    private route: ActivatedRoute,
	    private router: Router,
	    public dialog: MatDialog,
        private principal: Principal,
        private authService: AuthServerProvider,
	    public _DomSanitizer: DomSanitizer,
	    public googleAnalyticsService: GoogleAnalyticsEventsService,
	    private loaderService: LoaderService,
	    private signinModalService: SigninModalService,
		//@Inject(WINDOW) private window: Window,
		public snackBar: MatSnackBar,
		private uppyService: UppyService,
        private csrfService: CSRFService,
        private el: ElementRef,
        private stateService: StateStorageService,
        private cookieService: CookieService
	) {
        this.routerOutletRedirect = '';
		this.domSanitizer = DomSanitizer;
		this.formSubmitted = false;
		// this.uppyEvent
		// 	.takeUntil(this.onDestroy$)
		// 	.filter(([ev]) => contains(ev, ['file-added']))
		// 	.subscribe(
		// 		([ev, data1, data2, data3]) => {
		// 			/* console.log("Received '" + ev + "' event from instance 1", 'Upload complete');
		// 			console.log(data1);
		// 			console.log(data2);
		// 			console.log(data3);
		// 			console.log(ev); */
		// 		},
		// 		err => console.log(err),
		// 		() => console.log('done')
		// 	);
	}
	openUppyDialog(events:any){
		//this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        const dialogRef = this.dialog.open(UppyComponent, {
			width: '500px',
			height: '3000px'
		});
		// dialogRef.afterClosed().subscribe(result => {
		// 	console.log('The dialog was closed');
		//   });
	  }
	ngOnInit() {
		_that = this;
		this.csrf = this.csrfService.getCSRF();
		this.loaderService.display(true);
		this.isLoading = true;
		this.isPendingUnlock = false;
		this.isPendingExpand = false;
		this.tabIndex = 0;
		this.valueChanged = false;
		this.basicExpansionPanel = true;
		this.companyExpansionPanel = false;
		this.descriptionsExpansionPanel = false;
		this.categorizationsExpansionPanel = false;
		this.investigationExpansionPanel = false;
		this.relatedProvidersExpansionPanel = true;
		this.dataDetailsExpansionPanel = false;
		this.dataDeliveryExpansionPanel = false;
		this.triallingExpansionPanel = false;
		this.licensingExpansionPanel = false;
		this.pricingExpansionPanel = false;
		this.route.queryParams.subscribe(params => {
			if (params['pending-unlock']) {
				this.isPendingUnlock = true;
			}

			if (params['pending-expand']) {
				this.isPendingExpand = true;
			}
		});
		this.subscription = this.route.params.subscribe(params => {
			this.recordID = params['recordID'];
        });
		this.isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
		this.relevantArticlePage = 0;
		this.relevantArticleLinks = {
			last: 0
		};
		this.authoredArticlePage = 0;
		this.authoredArticleLinks = {
			last: 0
		};
		this.mdTooltipDelay = 500;

		this.mainDataIndustriesArr = [];
		this.filterService.getMainDataIndustry().subscribe(data => {
			const listItems = (data['aggregations'] && data['aggregations'].dataindustries) ? data['aggregations'].dataindustries.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i].mainDataIndustry.hits.hits[0]._source.mainDataIndustry;

				this.mainDataIndustriesArr.push({
					id: listItems[i].key,
					dataIndustry: item.dataIndustry
				});
			}
		});

		this.dataIndustries = [];
		this.filterService.getRelatedDataIndustry().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.dataIndustry !== '' && listItems[i]._source.dataIndustry !== 'DATA_INDUSTRY_NULL') {
					this.dataIndustries.push({
						id: listItems[i]._source.id,
						desc: listItems[i]._source.dataIndustry
					});
				}
			}
		});

		this.mainDataCategories = [];
		this.filterService.getAllDataCategories().subscribe(data => {
			const listItems = data && data['hits'] ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i]._source;
				if (item.datacategory !== '' && item.datacategory !== 'DATA_CATEGORY_NULL') {
					this.mainDataCategories.push({
						id: item.id,
						datacategory: item.datacategory,
						img: item.logo ? item.logo.logo : null
					});
				}
			}
		});

		this.dataCategories = [];
		this.filterService.getAllDataCategories().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.datacategory !== '' && listItems[i]._source.datacategory !== 'DATA_CATEGORY_NULL') {
					this.dataCategories.push({
						id: listItems[i]._source.id,
						desc: listItems[i]._source.datacategory,
						locked: listItems[i]._source.locked,
						img: listItems[i]._source.logo ? listItems[i]._source.logo.logo : null
					});
				}
			}
		});

		this.dataFeatures = [];
		this.filterService.getDataFeatures().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.dataFeatures.push({
					id: listItems[i]._source.id,
					desc: listItems[i]._source.dataFeature
				});
			}
		});

		this.dataSources = [];
		this.filterService.getDataSource().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.dataSources.push({
					value: {
						id: listItems[i]._id,
						desc: listItems[i]._source.dataSource,
						locked: listItems[i]._source.locked ? listItems[i]._source.locked : false
					},
					label: listItems[i]._source.dataSource
				});
			}
		});

		this.geographicalFocus = [];
		this.filterService.getGeographicFocus().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.geographicalFocus.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
		});
		this.providerTags = [];
		/* this.filterService.getDataProviderTagUrl().subscribe(data => {
			const listItems = data;
			for (let i = 0; i < listItems.length; i++) {
                const item = listItems[i];
                if (item.privacy === 'Public') {
                    this.providerTags.push({
                        value: {
                            id: listItems[i].id,
                            desc: listItems[i].providerTag,
                            locked: listItems[i].locked ? listItems[i].locked : false
                          //  locked: listItems[i]._source.locked ? listItems[i]._source.locked : false
                        },
                        label: listItems[i].providerTag
                    });
                }
			}
        }); */
        this.filterService.getDataProviderPublicTagUrl().subscribe(data => {
            const listItems = data;
			for (let i = 0; i < listItems.length; i++) {
                const item = listItems[i];
                    this.providerTags.push({
                        value: {
                            id: listItems[i].id,
                            desc: listItems[i].providerTag,
                            locked: listItems[i].locked ? listItems[i].locked : false
                        },
                        label: listItems[i].providerTag
                    });
			}
		});
		this.providerWillingness = [];
		this.filterService.getWillingnessToProvideToInvestors().subscribe(data => {
            const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.description !== '') {
					this.providerWillingness.push({
						value: listItems[i]._source.id,
						label: listItems[i]._source.description
					});
				}
            }
			this.investigationFields[0].templateOptions.options = this.providerWillingness;
        });
        this.providerUniquness = [];
		this.filterService.getUniqueness().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.description !== '') {
					this.providerUniquness.push({
						value: listItems[i]._source.id,
						label: listItems[i]._source.description
					});
				}
			}
			this.investigationFields[1].templateOptions.options = this.providerUniquness;
        });
        this.providerReadiness = [];
		this.filterService.getReadiness().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.description !== '') {
					this.providerReadiness.push({
						value: listItems[i]._source.id,
						label: listItems[i]._source.description
					});
				}
			}
			this.investigationFields[3].templateOptions.options = this.providerReadiness;
        });
        this.providerValue = [];
		this.filterService.getValue().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.description !== '') {
					this.providerValue.push({
						value: listItems[i]._source.id,
						label: listItems[i]._source.description
					});
				}
			}
			this.investigationFields[2].templateOptions.options = this.providerValue;
        });
        this.providerOverall = [];
		this.filterService.getOverall().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				if (listItems[i]._source.description !== '') {
					this.providerOverall.push({
						value: listItems[i]._source.id,
						label: listItems[i]._source.description
					});
				}
			}
			this.investigationFields[4].templateOptions.options = this.providerOverall;
		});
		this.providerRelevantInvestors = [];
		this.filterService.getInvestorType().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerRelevantInvestors.push({
					value: {
						id: listItems[i]._source.id,
						description: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}

			this.investigationFields[6].templateOptions.options = this.providerRelevantInvestors;
		});
		this.providerMainAssetClass = [];
		this.filterService.getMainAssetClass().subscribe(data => {
			const listItems = (data['aggregations'] && data['aggregations'].mainAssetclass
		) ? data['aggregations'].mainAssetclass.buckets : [];
			for (let i = 0; i < listItems.length; ++i) {
				const item = listItems[i].mainAssetClass.hits.hits[0]._source.mainAssetClass;
				this.providerMainAssetClass.push({
					value: listItems[i].key,
					label: item.description
				});
			}
			this.investigationFields[7].templateOptions.options = this.providerMainAssetClass;
		});
		this.providerRelevantAssetClass = [];
		this.filterService.getRelatedAssetClass().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.providerRelevantAssetClass.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.description
					},
					label: listItems[i]._source.description
				});
			}
			this.investigationFields[8].templateOptions.options = this.providerRelevantAssetClass;
		});

		this.relevantSecurityTypes = [];
		this.filterService.getSecurityType().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.relevantSecurityTypes.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode,
						locked: listItems[i]._source.locked ? listItems[i]._source : false
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.investigationFields[9].templateOptions.options = this.relevantSecurityTypes;
		});

		this.relevantSectors = [];
		this.filterService.getRelevantSectors().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.relevantSectors.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.description,
						locked: listItems[i]._source.locked ? listItems[i]._source : false
					},
					label: listItems[i]._source.description
				});
			}
			this.investigationFields[10].templateOptions.options = this.relevantSectors;
		});

		this.dataProviderService.getProvidersTypes().subscribe((data: any[]) => {
			const providerTypes = data;
			const pTypesArr = [];
			providerTypes.map((val, index) => {
				pTypesArr.push({
					value: val.id,
					label: val.providerType
				});
			});

			this.dataProviderTypes = pTypesArr;
			this.categorizationsFields = [
				{
					key: 'providerType.id',
					type: 'radioButton',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Provider Type:',
						description: 'How does the provider handle data and what does the provider do with it? Is the provider creating their own analytics, strictly acting as a pass-through data provider, or just collecting data? In other words, at what step in the data monetization process does the provider sit?',
						required: true,
                        options: pTypesArr,
                        attributes: {
                            'labelTooltip': 'Definitions of the types of data providers:""Aggregator"": a company that collects data from multiple different sources or internally, but is not in the habit of providing it to external partners/customers for reasons aside from their core business (if that\'s outside of being a data provider).""Provider"": a company that is primarily in the business of collecting data and providing that data in a fairly raw format to partners. So basically, a company that does not do much analysis on the data that they are selling, such as a data marketplace or financial data provider.""Analyzer"": a company that is already in the business of analyzing the data they collect internally or from 3rd parties and provided that analyzed information to other parties. So basically, a company that is in the business of analyzing and reformatting the data they collect and sell, such as an Investment Research company or a company that sells reports or data visualizations.""Investor"": a company that uses specific data to make their investments.Most companies are providers and analyzers.',
                            'descriptionTooltip': 'Definitions of the types of data providers:""Aggregator"": a company that collects data from multiple different sources or internally, but is not in the habit of providing it to external partners/customers for reasons aside from their core business (if that\'s outside of being a data provider).""Provider"": a company that is primarily in the business of collecting data and providing that data in a fairly raw format to partners. So basically, a company that does not do much analysis on the data that they are selling, such as a data marketplace or financial data provider.""Analyzer"": a company that is already in the business of analyzing the data they collect internally or from 3rd parties and provided that analyzed information to other parties. So basically, a company that is in the business of analyzing and reformatting the data they collect and sell, such as an Investment Research company or a company that sells reports or data visualizations.""Investor"": a company that uses specific data to make their investments.Most companies are providers and analyzers.'
                        }
					},
                },
                /*------------------- newwly added dropdown - previously autocomplete------------------------- */
				{
					key: 'mainDataIndustry',
					type: 'autocomplete',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Main Data Industry:',
						type: 'autocomplete',
						placeholder: 'Primary industry that the data provider operates in and provides data to.',
						// description: 'What is the primary industry you provide data to?',
						required: true,
						attributes: {
							field: 'dataIndustry',
                            'dropdown': 'true',
                            'labelTooltip': 'See "Data Industries" table for a definition of each data industry.Some providers will belong in more than one industry, e.g. marketing and retail - choose the primary industry that it provides data to, and add the primary and all other industries to the Industries column.If the company provides data for use in investing, Investment Management is the correct industry.In the past, this column was used a different type of categorization, not an industry, so some of the industries may need to be completely changed or moved to a different industry. If you think this is the case, contact Jordan [Administrator].',
                            'placeholderTooltip': 'See "Data Industries" table for a definition of each data industry.Some providers will belong in more than one industry, e.g. marketing and retail - choose the primary industry that it provides data to, and add the primary and all other industries to the Industries column.If the company provides data for use in investing, Investment Management is the correct industry.In the past, this column was used a different type of categorization, not an industry, so some of the industries may need to be completely changed or moved to a different industry. If you think this is the case, contact Jordan [Administrator].'
						},
						options: this.mainDataIndustriesArr,
						change: (event: any) => {
							const queryObj = event;
							this.mainDataIndustriesArr = [];

							this.filterService.getMainDataIndustry(queryObj.query).subscribe(d => {
								const listArray = [];
								const listItems = (d['aggregations'] && d['aggregations'].dataindustries) ? d['aggregations'].dataindustries.buckets : [];
								for (let i = 0; i < listItems.length; ++i) {
									const item = listItems[i].mainDataIndustry.hits.hits[0]._source.mainDataIndustry;
									if (item.dataIndustry !== 'DATA_INDUSTRY_NULL') {
										listArray.push({
											id: listItems[i].key,
											dataIndustry: item.dataIndustry
										});
									}
								}
								this.mainDataIndustriesArr = [...listArray];
								this.categorizationsFields[1].templateOptions.options = this.mainDataIndustriesArr;
							});
						},
                        blur: (event: any) => {
                            if (event && event.target && !event['selectedVal']) {
                                event.target.value = '';
                            }
                        }
					},
				},
				{
					key: 'industries',
					type: 'autocomplete',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Data Industries:',
						placeholder: 'All the industries that the provider operates in and provides data to.',
						description: 'What are all the industries you provide data to, aside from your main industry?',
						options: this.dataIndustries,
						required: true,
						attributes: {
                            field: 'desc',
                            'labelTooltip': 'See "Data Industries" table for a definition of each data industry.Always make sure the Main Industry is included in here.Be sure to add ""Investment Management"" if the company provides data for use in investing.Use the company website for this. Often the website will have a list of the sectors or industries that they serve. These should all be recorded.If the webpage does not clearly tell you which industries are served, just add the main data industry and move on',
                            'explanation': 'If you provide data for use in investing, please add Investment Management as one of your industries.',
                            'placeholderTooltip': 'See "Data Industries" table for a definition of each data industry.Always make sure the Main Industry is included in here. Be sure to add ""Investment Management"" if the company provides data for use in investing.Use the company website for this. Often the website will have a list of the sectors or industries that they serve. These should all be recorded. If the webpage does not clearly tell you which industries are served, just add the main data industry and move on.',
							'multiple': 'true',
							'dropdown': 'true'
						},
						change: (event: any) => {
							const queryObj = event;
							this.dataIndustries = [];

							this.filterService.getRelatedDataIndustry(queryObj.query).subscribe(d => {
								const listItems = d ? d['hits']['hits'] : [];
								for (let i = 0; i < listItems.length; i++) {
									if (listItems[i]._source.dataIndustry !== '' && listItems[i]._source.dataIndustry !== 'DATA_INDUSTRY_NULL') {
										this.dataIndustries.push({
											id: listItems[i]._source.id,
											desc: listItems[i]._source.dataIndustry
										});
									}
                                }
								this.categorizationsFields[2].templateOptions.options = this.dataIndustries;
							});
						},
                        blur: (event: any) => {
                            if (event && event.target && !event['selectedVal']) {
                                event.target.value = '';
                            }
                        }
					},
				},
				{
					key: 'mainDataCategory',
					type: 'autocomplete',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Main Data Category:',
						placeholder: 'The main type of data or information that the data provider collects/provides/analyzes.',
						description: 'Which data category do the majority of your data offerings fit into? Which classification best describes the main type of data/information/research you collect/provide/analyze?',
						required: true,
						options: this.mainDataCategories,
						attributes: {
							field: 'datacategory',
							'labelTooltip': 'See "Data Categories" table for a definition of each data category. This is the most important categorization to get correct. If you are unsure of what category to choose, please check with Jordan. The provider has usually built a core business within one particular type of data or business model before expanding to other things. Some providers will belong in more than one category - choose the primary data category, and add the primary and all other industries to the Data Categories column. Data Categories are meant to be an overarching classification that can encompass several Data Features and are meant to generally describe what the data is or can be used to understand more deeply.In the past, this column was used slightly differently, and so sometimes the Data Category is better suited as an Industry or Data Feature and the category should be more specific than the industry but less specific than the data feature. If you think this is the case, contact Jordan Hauer.',
                            'placeholderTooltip': 'See " Data Categories" table for a definition of each data category. This is the most important categorization to get correct. If you are unsure of what category to choose, please check with Jordan. The provider has usually built a core business within one particular type of data or business model before expanding to other things. Some providers will belong in more than one category - choose the primary data category, and add the primary and all other industries to the Data Categories column. Data Categories are meant to be an overarching classification that can encompass several Data Features and are meant to generally describe what the data is or can be used to understand more deeply. In the past, this column was used slightly differently, and so sometimes the Data Category is better suited as an Industry or Data Feature and the category should be more specific than the industry but less specific than the data feature. If you think this is the case, contact Jordan Hauer.'
						},
						change: (event: any) => {
							const queryObj = event;
							this.mainDataCategories = [];

							this.filterService.getAllDataCategories(queryObj.query).subscribe(d => {
								const listItems = d && d['hits'] ? d['hits']['hits'] : [];
								for (let i = 0; i < listItems.length; ++i) {
									const item = listItems[i]._source;
									if (listItems[i]._source.datacategory !== '' && listItems[i]._source.datacategory !== 'DATA_CATEGORY_NULL') {
										this.mainDataCategories.push({
											id: item.id,
											datacategory: item.datacategory,
											img: item.logo ? item.logo.logo : null
										});
									}
								}
								this.categorizationsFields[3].templateOptions.options = this.mainDataCategories;
							});
						},
                        blur: (event: any) => {
                            if (event && event.target && !event['selectedVal']) {
                                event.target.value = '';
                            }
                        }
					},
				},
				{
					key: 'categories',
					type: 'autocomplete',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Data Categories:',
						placeholder: 'All the data categories this data provider collects/provides/analyzes.',
						options: this.dataCategories,
						required: true,
						attributes: {
							field: 'desc',
							'labelTooltip': 'See "Data Categories" table for a definition of each data category. Always make sure the Main Data Category is included in here. Try to get up to 7 most appropriate data categories for each company. Some companies may have less than this and some other may have many more. In such unusual cases, seek assistance.',
							'multiple': 'true',
							'template': 'true',
                            'placeholderTooltip': 'See "Data Categories" table for a definition of each data category. Always make sure the Main Data Category is included in here. Try to get up to 7 most appropriate data categories for each company. Some companies may have less than this and some other may have many more. In such unusual cases, seek assistance.'
						},
						change: (event: any) => {
							const queryObj = event;
							this.dataCategories = [];
							this.filterService.getAllDataCategories(queryObj.query).subscribe(d => {
								const listItems = d ? d['hits']['hits'] : [];
								for (let i = 0; i < listItems.length; i++) {
									if (listItems[i]._source.datacategory !== '' && listItems[i]._source.datacategory !== 'DATA_CATEGORY_NULL') {
										this.dataCategories.push({
											id: listItems[i]._source.id,
											desc: listItems[i]._source.datacategory,
											img: listItems[i]._source.logo ? listItems[i]._source.logo.logo : null
										});
									}
								}
								this.categorizationsFields[4].templateOptions.options = this.dataCategories;
							});
						},
                        blur: (event: any) => {
                            if (event && event.target && !event['selectedVal']) {
                                event.target.value = '';
                            }
                        }
					},
				},
				{
					key: 'features',
					type: 'autocomplete',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Data Features:',
						placeholder: 'What is the specific data or information being collected or organized by this provider?',
						options: this.dataFeatures,
						attributes: {
							field: 'desc',
							'labelTooltip': 'See "Data Features" table for a definition of each data feature. Use similar companies as reference for what to fill in here. Use the links between Data Features and Data Categories to help determine which Data Features to consider. The company (within providers and analyzers) often describes many of the data features provided. In short, this field is a more detailed version of a data category. If you think of a table or spreadsheet or any structured data (data with cleanly organized columns and rows), a data feature usually fits into one or a combination of only a few columns. Example: "Store Location" would be one or a few fields describing the address of a store (address, city, state, zip, etc.). "Product Ratings" is likely one columns with the number of stars a consumer rates a product, but it could also be a count of the number of ratings each product has. The important thing is that the data provider possesses information about product ratings.',
							'multiple': 'true',
                            'placeholderTooltip': 'See "Data Features"   table for a definition of each data feature. Use similar companies as reference for what to fill in here. Use the links between Data Features and Data Categories to help determine which Data Features to consider. The company (within providers and analyzers) often describes many of the data features provided. In short, this field is a more detailed version of a data category. If you think of a table or spreadsheet or any structured data (data with cleanly organized columns and rows), a data feature usually fits into one or a combination of only a few columns. Example: ""Store Location"" would be one or a few fields describing the address of a store (address, city, state, zip, etc.). ""Product Ratings"" is likely one columns with the number of stars a consumer rates a product, but it could also be a count of the number of ratings each product has. The important thing is that the data provider possesses information about product ratings.'
						},
						change: (event: any) => {
							const queryObj = event;
							this.dataFeatures = [];

							this.filterService.getDataFeatures(queryObj.query).subscribe(d => {
								const listArray = [];
								const listItems = d ? d['hits']['hits'] : [];
								this.dataFeatures = listItems;
								for (let i = 0; i < listItems.length; i++) {
									listArray.push({
										id: listItems[i]._source.id,
										dataFeature: listItems[i]._source.dataFeature,
										desc: listItems[i]._source.dataFeature
									});
								}
								this.dataFeatures = [...listArray];
								this.categorizationsFields[5].templateOptions.options = this.dataFeatures;
							});
						},
                        blur: (event: any) => {
                            if (event && event.target && !event['selectedVal']) {
                                event.target.value = '';
                            }
                        }
					},
				},
				{
					key: 'sources',
					type: 'multiselect',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Data Sources:',
						placeholder: 'The original (physical) source of the provider\'s data, not to be confused with external Provider Partners (the companies the provider receives data from).',
						description: 'What is the original source of the data being produced by this data provider?',
						options: this.dataSources,
						attributes: {
							field: 'desc',
                            'labelTooltip': 'See "Data Sources" table for a definition of each data source. Fill this in if the data source is explicitly stated on the company\'s website or through another source. If the source is not clear, leave blank. This is meant to be a more generic indicator of where the data came from.',
                            'descriptionTooltip': 'See "Data Sources" table for a definition of each data source. Fill this in if the data source is explicitly stated on the company\'s website or through another source. If the source is not clear, leave blank. This is meant to be a more generic indicator of where the data came from.',
                            'placeholderTooltip': 'See "Data Sources" table for a definition of each data source. Fill this in if the data source is explicitly stated on the company\'s website or through another source. If the source is not clear, leave blank. This is meant to be a more generic indicator of where the data came from.'
						}
					},
				},
				{
					key: 'geographicalFoci',
					type: 'multiselect',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Geographical Focus:',
						placeholder: 'The geographies that the data provider focuses on collecting/providing/analyzing data about.',
						required: true,
						options: this.geographicalFocus,
						attributes: {
							'labelTooltip': 'If a data provider covers numerous countries across continents, then it is multinational. If a data provider only covers one continent, certain countries or a specific region, then select each one. This is not the location of the company itself.',
                            'placeholderTooltip': 'If a data provider covers numerous countries across continents, then it is multinational. If a data provider only covers one continent, certain countries or a specific region, then select each one. This is not the location of the company itself.'
						}
                    },
                    validation: {
                        messages: {
                            required: 'Geographical Focus is required'
                        }
                    }
				},
				{
					key: 'providerTags',
					type: 'multiselect',
					wrappers: ['form-field-horizontal'],
					templateOptions: {
						label: 'Tags:',
						placeholder: 'Special circumstances for a data provider.',
						options: this.providerTags,
						attributes: {
                            field: 'desc',
                            'labelTooltip': 'See "Data Provider Tags" table for a definition of each tag. Particular attributes of the provider or the provider\'s data that are differentiators would be added as new Tags which wouldn\'t fit into any of the other categorizations.',
                            'placeholderTooltip': 'See "Data Provider Tags" table for a definition of each tag. Particular attributes of the provider or the provider\'s data that are differentiators would be added as new Tags which wouldn\'t fit into any of the other categorizations.'
						}
					},
				},
			];
			this.companyDetailsModel = this.dataProvider;
		});

		this.deliveryMethods = [];
		this.filterService.getDeliveryMethods().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.deliveryMethods.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode,
						locked: listItems[i]._source.locked ? listItems[i]._source.locked : false
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDeliveryFields[0].templateOptions.options = this.deliveryMethods;
		});

		this.deliveryFormats = [];
		this.filterService.getDeliveryFormat().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.deliveryFormats.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode,
						locked: listItems[i]._source.locked ? listItems[i]._source.locked : false
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDeliveryFields[1].templateOptions.options = this.deliveryFormats;
		});

		this.deliveryFrequencies = [];
		this.filterService.deliveryFrequency().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.deliveryFrequencies.push({
					value: {
						id: listItems[i]._source.id,
						desc: listItems[i]._source.lookupcode
					},
					label: listItems[i]._source.lookupcode
				});
			}
			this.dataDeliveryFields[2].templateOptions.options = this.deliveryFrequencies;
		});

		this.dataUpdateFrequency = [];
		this.filterService.getDataUpdateFrequency().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.dataUpdateFrequency.push({
					id: listItems[i]._source.id,
					lookupcode: listItems[i]._source.lookupcode,
					recordCount: listItems[i]._source.recordCount
				});
			}
			this.dataDeliveryFields[4].templateOptions.options = this.dataUpdateFrequency;
		});

		 this.pricingModel = [];
		this.filterService.getPricingModel().subscribe(data => {
			const listItems = data ? data['hits']['hits'] : [];
			for (let i = 0; i < listItems.length; i++) {
				this.pricingModel.push({
					id: listItems[i]._source.id,
					desc: listItems[i]._source.description,
					locked: listItems[i]._source.locked ? true : false
				});
			}
			this.pricingFields[0].templateOptions.options = this.pricingModel;
		});
	}

	ngAfterViewInit() {
        const uppy = this.uppyService.uppy;
        this.loaderService.display(false);
	}

	getLinkImgUrl(desc) {
		if (desc) {
			const fileName = ((desc).toLowerCase()).replace(' ', '_');
			const link = 'content/images/' + fileName  + '.png';
			return link;
		}
	}

	providerProfileTab(event: MatTabChangeEvent) {

	   /*  this.profileTabLabel = 'Provider Profile - Tab ' + event.tab.textLabel;
		this.ProfileCategory = 'Provider Profile ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent(this.ProfileCategory, 'Tab Link Clicked', this.profileTabLabel); */
	}

	providerFollow(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	unlockProviders(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	requestMoreInfo(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	providerPartnerTab(event: MatTabChangeEvent) {
		this.profileTabLabel = 'Provider Profile -  ' + event.tab.textLabel;
		this.googleAnalyticsService.emitEvent('Partner Provider', 'Internal Link Clicked', this.profileTabLabel);
	}

	dataFieldLink(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	profileWebsite(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	profileArticle(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
	samplesTab(events) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	load(recordID) {
		this.dataProviderService.findById(recordID).subscribe(dataProvider => {
			this.loaderService.display(false);
			this.dataProvider = dataProvider['hits'].total ? dataProvider['hits'].hits[0]._source : null;
			this.isLoading = false;
			this.constructFormModel(this.dataProvider);
			this.companyDetailsModel = this.dataProvider;

			if (this.dataProvider.geographicalFoci) {
				const geographicalFoci = [];
				for (const obj in this.dataProvider.geographicalFoci) {
					if (obj) {
						geographicalFoci.push({
							id: this.dataProvider.geographicalFoci[obj].id,
							desc: this.dataProvider.geographicalFoci[obj].description
						});
					}
				}
				delete this.dataProvider.geographicalFoci;
				this.dataProvider.geographicalFoci = geographicalFoci;
			}
			if (this.dataProvider.deliveryFrequencies) {
				const deliveryFrequencies = [];
				for (const obj in this.dataProvider.deliveryFrequencies) {
					if (obj) {
						deliveryFrequencies.push({
							id: this.dataProvider.deliveryFrequencies[obj].id,
							desc: this.dataProvider.deliveryFrequencies[obj].lookupcode
						});
					}
				}
				delete this.dataProvider.deliveryFrequencies;
				this.dataProvider.deliveryFrequencies = deliveryFrequencies;
			}
			if (this.dataProvider.investorTypes) {
				const investorTypes = [];
				for (const obj in this.dataProvider.investorTypes) {
					if (obj) {
						investorTypes.push({
							id: this.dataProvider.investorTypes[obj].id,
							description: this.dataProvider.investorTypes[obj].description
						});
					}
				}
				delete this.dataProvider.investorTypes;
				this.dataProvider.investorTypes = investorTypes;
			}
			if (this.dataProvider.dataUpdateFrequency) {
				const updateFrequency = {};
				updateFrequency['id'] = this.dataProvider.dataUpdateFrequency.id;
				updateFrequency['lookupcode'] = this.dataProvider.dataUpdateFrequency.lookupcode,
				updateFrequency['recordCount'] = this.dataProvider.dataUpdateFrequency.recordCount;
				delete this.dataProvider.dataUpdateFrequency;
				this.dataProvider.dataUpdateFrequency = updateFrequency;
			}
			 if (this.dataProvider.pricingModel) {
				const pricingModels = this.dataProvider.pricingModel;
				// delete this.dataProvider.pricingModels;
				// this.dataProvider.pricingModels = pricingModels[0];
			}
			this.basicModel = this.dataProvider;

		}, error => {
			if (error.status === 401) {
                this.stateService.storeUrl(null);
                this.router.navigate(['']);
				this.signinModalService.openDialog('Session expired! Please re-login.');
			}
			this.dataProvider = null;
			this.isLoading = false;
		});
	   // this.loaderService.display(false);
    }

    constructFormModel(provider: DataProvider) {
        this.trackname = provider.providerName.length;
      //  console.log(this.trackname);
        this.trackwebsite = provider.website.length;
        this.trackownerOrganizationCity = provider.ownerOrganization.city.length;
        this.trackownerOrganizationState = provider.ownerOrganization.state.length;
        this.trackownerOrganizationCountry = provider.ownerOrganization.country.length;
        this.trackownerOrganizationYearfound = provider.ownerOrganization.yearFounded;
        this.trackownerOrganizationHeadcount = provider.ownerOrganization.headCount.length;
        this.trackInvestor = provider.investor_clients.length;
        this.trackDataCollection = provider.dateCollectionBegan.length;
        this.providerFormModel = {
            'recordID': provider.recordID,
            'providerName': provider.providerName,
            'website': provider.website
        };
        this.descriptionsModel = {
            'recordID': provider.recordID,
            'shortDescription': provider.shortDescription,
            'longDescription': provider.longDescription
        };
        this.companyDetailsModel = {
            'recordID': provider.recordID,
            'ownerOrganization': provider.ownerOrganization
        };
        this.categorizationsModel = {
            'recordID': provider.recordID,
            'providerType': provider.providerType,
            'mainDataCategory': provider.mainDataCategory,
            'mainDataIndustry': provider.mainDataIndustry
        };
    }

	previousState() {
		//this.window.history.back();
	}

	// ngOnDestroy() {
	// 	this.onDestroy$.next();
    //     this.onDestroy$.complete();
    //     this.cookieService.put('logoutClicked', '0');
	// }

	openConfirmationDialog(nextState?: RouterStateSnapshot) {
		//const redirect = nextState.url;
		//console.log(redirect);
        //this.dialog.closeAll();
        this.dialogRef = this.dialog.open(ConfirmDialogComponent, {
            disableClose: false
        });
        this.dialogRef.componentInstance.confirmMessage = 'You have some unsaved edits in this form.';
        this.dialogRef.componentInstance.button = {
            leave: true,
            confirm: true,
            leaveText: 'Leave and Don\'t Save',
            confirmText: 'Save and Submit'
        };

        this.dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // do confirmation actions
                if (result === 'leave') {
                    this.dialog.closeAll();
                }

                if (result === 'save') {
                    this.submitProviderEdit(this.basicModel);
                }
            }
            this.dialogRef = null;
        });

		return false;
	}

	trackByIndex(index) {
		return index;
    }

    updateProvider(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, submitMessage) {
        this.dataProviderService.providerAdmin(model).subscribe(data => {
            if (data) {
                this.formSubmitted = true;
                this.basicModel = data;
                this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                this.loaderService.display(false);
                if (currentPanel && currentPanel !== '') {
                    this[currentPanel] = false;
                    if (nextPanel && nextPanel !== '') {
                        this[nextPanel] = true;
                    }
                    if (tabIndex) {
                        this.tabIndex = tabIndex;
                    }
                } else {
                    if (skipRedirect) {
                        return true;
                    }
                    this.stateService.storeUrl(null);
                    this.router.navigate(['providers']);
                }
            } else {
                this.formSubmitted = false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        }, error => {
			if (error.status === 401) {
                this.formSubmitted = false;
                this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
            } else {
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        });
    }

    insertProvider(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, submitMessage) {
        this.dataProviderService.addProviderAdmin(model).subscribe(data => {
            if (data) {
                this.formSubmitted = true;
                this.basicModel = data;
                this.openSnackBar(panelName ? '' + formName + ' saved successfully!' : submitMessage, 'Close');
                this.loaderService.display(false);

                if (currentPanel && currentPanel !== '') {
                    this[currentPanel] = false;
                    if (nextPanel && nextPanel !== '') {
                        this[nextPanel] = true;
                    }
                    if (tabIndex) {
                        this.tabIndex = tabIndex;
                    }
                } else {
                    if (skipRedirect) {
                        return true;
                    }
					this.stateService.storeUrl(null);
					this.router.navigate(['providers', data.recordID, 'admin']);
					this.dialog.closeAll();
                }
            } else {
                this.formSubmitted = false;
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        }, error => {
			if (error.status === 401) {
                this.formSubmitted = false;
                this.openSnackBar('Your Session has been expired! Please re-login.', 'Close');
            } else {
                this.openSnackBar('' + formName + ' failed to save!', 'Close');
            }
        });
    }

	submitProviderEdit(model, panelName?: string, currentPanel?: string, nextPanel?: string, tabIndex?: number, form?: string, skipRedirect?: boolean) {
        const formName = panelName ? panelName + '' : 'Form';
       // const submitMessage = `The Provider with the name "${model.providerName}" has been successfully submitted and has been added to the database immediately.`;
        if (form && this[form].valid) {
            if (model && !model.recordID) {
				const insertSubmitMessage = `The Provider with the name "${model.providerName}" has been successfully submitted and has been added to the database immediately.`;
                this.insertProvider(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, insertSubmitMessage);
            } else {
				const updateSubmitMessage = 'Your edits have been successfully submitted and have been applied to the profile immediately.';
                this.updateProvider(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, updateSubmitMessage);
            }
        } else {
            const invalidElements = this.el.nativeElement.querySelectorAll('.ng-invalid:not(form)');
            if (invalidElements.length > 0) {
                if (invalidElements[0].localName && invalidElements[0].localName.startsWith('p-')) {
                    invalidElements[0].querySelectorAll('input')[0].focus();
                } else {
                    setTimeout(() => {
                        invalidElements[0].focus();
                    }, 1000);
				}
				this.openSnackBar('Please fill all the required fields!', 'Close');
            } else {
                if (model && !model.recordID) {
					const insertSubmitMessage = `The Provider with the name "${model.providerName}" has been successfully submitted and has been added to the database immediately.`;
                    this.insertProvider(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, insertSubmitMessage);
                } else {
					const updateSubmitMessage = 'Your edits have been successfully submitted and have been applied to the profile immediately.';
                    this.updateProvider(model, panelName, formName, currentPanel, nextPanel, tabIndex, skipRedirect, updateSubmitMessage);
                }
            }
        }
    }

	closeSignUpDialog() {
		if(this.valueChanged === false) {
            this.dialog.closeAll();
        } else if(this.valueChanged === true) {
            if(this.formSubmitted) {
                this.openConfirmationDialog();
            } else {
				this.openConfirmationDialog();
            }
        }
	}
    formSubmit(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    formSave(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    basicInput($event) {
        const inputKey = $event;
        /* delete entry */
        if (inputKey.key === 'providerName') {
            if (inputKey.model.providerName.length < _that.trackname) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Deleted', 'Provider Profile Form - Textbox - Provider Name');
            } else if (inputKey.model.providerName.length > _that.trackname) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Added', 'Provider Profile Form - Textbox - Provider Name');
            }
        } else if (inputKey.key === 'website') {
             if ( inputKey.model.website.length < _that.trackwebsite) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Deleted', 'Provider Profile Form - Textbox - Provider Website');
            } else if (inputKey.model.website.length > _that.trackwebsite) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Added', 'Provider Profile Form - Textbox - Provider Website');
            }
        } else if (inputKey.key === 'ownerOrganization.city') {
            if (inputKey.model.ownerOrganization.city.length < _that.trackownerOrganizationCity) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Deleted', 'Provider Profile Form - Dropdown - City');
            } else if (inputKey.model.ownerOrganization.city.length > _that.trackownerOrganizationCity) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Added', 'Provider Profile Form - Dropdown - City');
            }
        } else if (inputKey.key === 'ownerOrganization.state') {
             if (inputKey.model.ownerOrganization.state.length < _that.trackownerOrganizationState) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Deleted', 'Provider Profile Form - Dropdown - State');
            } else if (inputKey.model.ownerOrganization.state.length > _that.trackownerOrganizationState) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Added', 'Provider Profile Form - Dropdown - State');
            }
        } else if (inputKey.key === 'ownerOrganization.country') {
            if (inputKey.model.ownerOrganization.country.length < _that.trackownerOrganizationCountry) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Deleted', 'Provider Profile Form - Dropdown  - Country');
            } else if (inputKey.model.ownerOrganization.country.length > _that.trackownerOrganizationCountry) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Added', 'Provider Profile Form - Dropdown  - Country');
            }
        } else if (inputKey.key === 'ownerOrganization.yearFounded') {
            if (inputKey.model.ownerOrganization.yearFounded < _that.trackownerOrganizationYearfound) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Deleted', 'Provider Profile Form - Textbox - Year Founded');
            } else if (inputKey.model.ownerOrganization.yearFounded > _that.trackownerOrganizationYearfound) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Added', 'Provider Profile Form - Textbox - Year Founded');
            }
        } else if (inputKey.key === 'ownerOrganization.headCount') {
            if (inputKey.model.ownerOrganization.headCount.length < _that.trackownerOrganizationHeadcount) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Deleted', 'Provider Profile Form - Textbox - Headcount');
            } else if (inputKey.model.ownerOrganization.headCount.length > _that.trackownerOrganizationHeadcount) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Added', 'Provider Profile Form - Textbox - Headcount');
            }
        } else if (inputKey.key === 'investor_clients') {
            if (inputKey.model.investor_clients.length < _that.trackInvestor) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Deleted', 'Provider Profile Form - Textbox - Number of Investor Clients');
            } else if (inputKey.model.investor_clients.length > _that.trackInvestor) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Added', 'Provider Profile Form - Textbox - Number of Investor Clients');
            }
        } else if (inputKey.key === 'dateCollectionBegan') {
            if (inputKey.model.dateCollectionBegan.length < _that.trackDataCollection) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Deleted', 'Provider Profile Form - Textbox - Date Collection Began');
            } else if (inputKey.model.dateCollectionBegan.length > _that.trackDataCollection) {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Added', 'Provider Profile Form - Textbox - Date Collection Began');
            }
        } else if (inputKey.key === 'publicEquitesCovered') {
            if (inputKey.model.publicEquitesCovered !== '') {
                _that.googleAnalyticsService.emitEvent('Provider Profile Form - Input', 'Added', 'Provider Profile Form - Textbox - # of Public Equities Covered');
            }
        }
    }

    openSnackBar(message: string, action: string, delay?: number) {
        this.snackBar.open(message, action, {
            duration: delay ? delay : 10000,
			panelClass: ['blue-snackbar']
        });
    }

	submitBasic(model) {
		// console.log(model);
		this.basicExpansionPanel = false;
		this.companyExpansionPanel = true;
	}

	submitCompanyDetails(model) {
		// console.log(model);
		this.companyExpansionPanel = false;
		this.descriptionsExpansionPanel = true;
	}

	submitDescriptions(model) {
		// console.log(model);
		this.descriptionsExpansionPanel = false;
		this.categorizationsExpansionPanel = true;
	}

	submitCategorizations(model) {
		// console.log(model);
		this.categorizationsExpansionPanel = false;
		this.investigationExpansionPanel = true;
	}

	submitInvestigationDetails(model) {
		this.investigationExpansionPanel = false;
		this.tabIndex = 1;
	}

	submitRelatedProviders(model) {
		// console.log(model);
		this.relatedProvidersExpansionPanel = false;
		this.dataDetailsExpansionPanel = true;
	}

	submitDataDetails(model) {
		// console.log(model);
		this.dataDetailsExpansionPanel = false;
		this.dataDeliveryExpansionPanel = true;
	}

	submitDataDelivery(model) {
		// console.log(model);
		this.dataDeliveryExpansionPanel = false;
		this.triallingExpansionPanel = true;
	}

	submitTrialling(model) {
		// console.log(model);
		this.triallingExpansionPanel = false;
		this.licensingExpansionPanel = true;
	}

	submitLicensing(model) {
		// console.log(model);
		this.licensingExpansionPanel = false;
		this.pricingExpansionPanel = true;
	}

	submitPricing(model) {
		// console.log(model);
		this.pricingExpansionPanel = false;
		this.discussionExpansionPanel = true;
	}

	submitDiscussion(model) {
		// console.log(model);
		this.discussionExpansionPanel = false;
    }

    beforeSessionExpires(event) {
        if (event) {
            this.submitProviderEdit(this.basicModel, '', '', '', 0, '', true);
        }
    }

    onExpireSession(event) {
        if (event) {
            this.formSubmitted = true;
            this.authService.logout().subscribe(d => {
                this.stateService.storeUrl(null);
                this.principal.authenticate(null);
                this.router.navigate(['../../']);
                this.signinModalService.openDialog('Session expired! Please re-login.');
            });
        }
    }

    @HostListener('document:keyup', [])
    @HostListener('document:click', [])
    @HostListener('document:wheel', [])
    @HostListener('window:scroll', [])
    resetTimer() {
        this.authService.notifyUserAction();
    }

    @HostListener('window:popstate', [])
    onPopState() {
        this.cookieService.put('logoutClicked', '0');

    }

    @HostListener('window:beforeunload', ['$event'])
    preventUser($event) {
        $event.preventDefault();
        return $event.returnValue = "Are you sure you want to exit?";
    }
}
