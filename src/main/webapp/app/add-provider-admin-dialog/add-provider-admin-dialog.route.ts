import { Route } from '@angular/router';
import { CanDeactivateGuard, UserRouteAccessService, CanDeactivateAddProviderAdminFormGuard } from 'app/shared';
//import { AddProviderAdminComponent } from '../add-provider-admin/add-provider-admin.component';
import { AddProviderAdminDialogComponent } from './add-provider-admin-dialog.component';

export const addProviderAdminDialogRoute: Route = {
    path: 'add-provider-admin-dialog',
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'datamarketplaceApp.dataProvider.home.title',
        breadcrumb: 'Providers > Add New > Admin',
        type: 'dynamic'
    },
    component: AddProviderAdminDialogComponent,
    canActivate: [UserRouteAccessService],
    canDeactivate: [CanDeactivateAddProviderAdminFormGuard]
};
