import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
    DatamarketplaceSharedModule,
    FormlyEditorFieldComponent,
    FormlyAutoCompleteFieldComponent,
    FormlyMultiSelectFieldComponent,
    FormlyDropdownFieldComponent,
    FormlyMaskFieldComponent,
    FormlySliderFieldComponent,
    FormlyRadioButtonFieldComponent,
    FormlyTextAreaFieldComponent
} from '../shared';

//import {AddProviderAdminComponent} from '../add-provider-admin/add-provider-admin.component';
import { FormlyModule } from '@ngx-formly/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyHorizontalWrapperComponent } from 'app/entities/data-provider/horizontal-wrapper';
import { GenericProfileModule } from 'app/shared/generic-profile';

import { MatMenuModule } from '@angular/material/menu';
import { addProviderAdminRoute } from '../add-provider-admin/add-provider-admin.route';
import { AddProviderAdminDialogComponent } from './add-provider-admin-dialog.component';
import { addProviderAdminDialogRoute } from './add-provider-admin-dialog.route';
import { DataProviderService } from 'app/add-provider-admin/add-provider-admin.service';

@NgModule({
    imports: [
        DatamarketplaceSharedModule,
        ReactiveFormsModule,
        MatMenuModule,
        RouterModule.forRoot([addProviderAdminDialogRoute], { useHash: true }),
        FormlyModule.forRoot({
            wrappers: [{ name: 'form-field-horizontal', component: FormlyHorizontalWrapperComponent  }],
            validationMessages: [
                { name: 'required', message: 'This field is required' },
            ],
            types: [{
                name: 'inputEditor', component: FormlyEditorFieldComponent
            }, {
                name: 'autocomplete', component: FormlyAutoCompleteFieldComponent
            }, {
                name: 'multiselect', component: FormlyMultiSelectFieldComponent
            }, {
                name: 'dropdown', component: FormlyDropdownFieldComponent
            }, {
                name: 'mask', component: FormlyMaskFieldComponent
            }, {
                name: 'slider', component: FormlySliderFieldComponent
            }, {
                name: 'radioButton', component: FormlyRadioButtonFieldComponent
            }, {
                name: 'textarea', component: FormlyTextAreaFieldComponent
            }]
        }),
        FormlyBootstrapModule,
        GenericProfileModule
    ],
    declarations: [
        AddProviderAdminDialogComponent
    ],
    entryComponents: [

    ],
    providers: [
        DataProviderService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DatamarketplaceAddProviderAdminDialogModule {}
