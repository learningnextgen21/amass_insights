import { Component, OnInit, AfterViewInit, Renderer2, ElementRef, Inject, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';

import { LoginService } from '../shared/login/login.service';
import { StateStorageService } from '../shared/auth/state-storage.service';
import { SigninDialogComponent } from '../dialog/signindialog.component';
import { PasswordResetInitService } from '../account/password-reset/init/password-reset-init.service';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'jhi-app-forgot-dialog',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./dialog.component.css']
})
export class ForgotPasswordDialogComponent implements OnInit, AfterViewInit {

	error: string;
	errorEmailNotExists: string;
	resetAccount: any;
	success: string;
	errorMessage: string;
    inactiveUser: string;
    @ViewChild(MatInput) input;

	constructor(
		private passwordResetInitService: PasswordResetInitService,
		private eventManager: JhiEventManager,
		private loginService: LoginService,
		private stateStorageService: StateStorageService,
		private elementRef: ElementRef,
		private renderer: Renderer2,
		private router: Router,
		public mdDialog: MatDialog,
		public dialogRef: MatDialogRef<any>,
		@Inject(MAT_DIALOG_DATA) public data: any,
        private http: HttpClient,
        public googleAnalyticsService: GoogleAnalyticsEventsService
	) {

	}

	ngOnInit() {
		this.resetAccount = {};
	}

	ngAfterViewInit() {
        this.input.focus();
        setTimeout(() => {
            this.input.focus();
      }, 1000);
		/* this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#forgorPasswordEmail'), 'focus', []); */
	}

	openSigninDialog(events: any) {
		this.mdDialog.closeAll();

		this.dialogRef = this.mdDialog.open(SigninDialogComponent, {
			width: '400px'
        });
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}

	closeForgotPasswordDialog() {
		this.mdDialog.closeAll();
	}

	resetPassword() {
		this.error = null;
		this.errorEmailNotExists = null;
		this.http.get('api/ur/user-status?loginID=' + this.resetAccount.email, {observe: 'response', responseType: 'text'}).subscribe(res => {
			if (res.status === 200) {
				this.errorMessage='';
				if(res.body === 'Valid user')
				{
				if (res.headers.get('x-datamarketplaceapp-params') && res.headers.get('x-datamarketplaceapp-params') !== 'Authentication failed') {
					this.inactiveUser = res.headers.get('x-datamarketplaceapp-params');
					this.errorEmailNotExists = null;
					return;
				} else {
					this.inactiveUser = null;
					this.passwordResetInitService.save(this.resetAccount.email).subscribe(() => {
						this.success = 'OK';
						return;
					}, response => {
						this.success = null;
						if (response.status === 400 && response.error === 'email address not registered') {
							this.errorEmailNotExists = 'ERROR';
							return;
						} else if (response.status === 400 && response.error === 'Your email is associated with a registered LinkedIn account. Please Login with LinkedIn.') {
                            this.error = response.error;
							return;
						} else {
							this.error = 'ERROR';
							return;
						}
					});
				}
			}
			else 
			{
              this.errorMessage=res.body;
			  console.log(this.errorMessage);
			}
			}
		}, err => {
			this.inactiveUser = null;
			this.errorEmailNotExists = 'ERROR';
			return;
		});
    }
    passwordReset(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
}
