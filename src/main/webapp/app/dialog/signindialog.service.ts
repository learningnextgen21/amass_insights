import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { JhiLoginModalComponent } from '../shared/login/login.component';

import { SigninDialogComponent } from './signindialog.component';
import { Overlay } from '@angular/cdk/overlay';

@Injectable()
export class SigninModalService {
	private isOpen = false;
	public message: string;
	constructor(
		private modalService: NgbModal,
        public mdDialogService: MatDialog,
        private overlay: Overlay
	) {}

	open(): NgbModalRef {
		if (this.isOpen) {
			return;
		}
		this.isOpen = true;
		const modalRef = this.modalService.open(JhiLoginModalComponent, {
			container: 'nav'
		});
		modalRef.result.then((result) => {
			this.isOpen = false;
		}, (reason) => {
			this.isOpen = false;
		});
		return modalRef;
	}

	openDialog(message?: string): MatDialogRef<any> {
		this.message = message ? message : null;
		if (this.isOpen) {
			return;
		}
		this.isOpen = true;
		const modalRef = this.mdDialogService.open(SigninDialogComponent, {
            width: '400px',
            scrollStrategy: this.overlay.scrollStrategies.noop()
		});
		modalRef.componentInstance.message = this.message;
		modalRef.afterClosed().subscribe((result) => {
			this.isOpen = false;
		});

		modalRef.afterClosed().subscribe((result) => {
			this.isOpen = false;
		});
		return modalRef;
	}
}
