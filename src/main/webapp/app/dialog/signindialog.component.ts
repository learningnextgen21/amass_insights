import { Component, OnInit, AfterViewInit, Renderer2, ElementRef, Inject, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';

import { LoginService } from '../shared/login/login.service';
import { StateStorageService } from '../shared/auth/state-storage.service';

import { ForgotPasswordDialogComponent } from './forgot-password.component';
import { ApplyAccountComponent } from '../account/apply/apply.component';
//import { setTimeout } from 'timers';
import { Intercom } from 'ng-intercom';
import { INTERCOM_ID } from '../app.constants';
import { Principal, CSRFService } from '../shared';
import { GoogleAnalyticsEventsService } from '../shared/googleanalytics/google-analytics-events.service';
import { HttpClient } from '@angular/common/http';
import { AmassSettingService } from '../account/settings/settings.service';

@Component({
    selector: 'jhi-app-dialog',
    templateUrl: './signindialog.component.html',
    styleUrls: ['./dialog.component.css']
})
export class SigninDialogComponent implements OnInit, AfterViewInit {

	authenticationError: boolean;
	authenticationErrorMessage: any;
	password: string;
	rememberMe: boolean;
	username: string;
	credentials: any;
	inactiveUser: string;
	message: string;
	@ViewChild(MatInput) input;
	account: any;
    csrf: string;
    existingUserAuthentication: boolean;
userOnboarding: boolean;
	constructor(
		private eventManager: JhiEventManager,
		private loginService: LoginService,
		private stateStorageService: StateStorageService,
		private elementRef: ElementRef,
		private renderer: Renderer2,
		private router: Router,
		public dialogRef: MatDialogRef<any>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		public mdDialog: MatDialog,
		public http: HttpClient,
		private principal: Principal,
		private intercom: Intercom,
		public googleAnalyticsService: GoogleAnalyticsEventsService,
		private csrfService: CSRFService,
		private amassSettingService: AmassSettingService
	) {
		this.credentials = {};
		this.inactiveUser = null;
		this.authenticationError = false;
	}

	ngOnInit() {
		this.csrf = this.csrfService.getCSRF();
	}

	ngAfterViewInit() {
		// this.input.focus();
		setTimeout(() => {
			// this.input.focus();
		}, 1000);
		/* this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#username'), 'focus', []); */
	}

  	login(events: any) {
		this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
		this.inactiveUser = null;
		this.message = null;
		this.authenticationError = false;
		this.authenticationErrorMessage = null;
		if (this.username && this.password) {
			this.loginService.login({
				username: this.username,
				password: this.password,
				rememberMe: this.rememberMe
			}).then(() => {
				this.authenticationError = false;
				this.dialogRef.close();
				if (this.router.url === '/register' || (/^\/activate\//.test(this.router.url)) ||
					(/^\/reset\//.test(this.router.url))) {
					this.router.navigate(['']);
				}

				this.eventManager.broadcast({
					name: 'authenticationSuccess',
					content: 'Sending Authentication Success'
				});

				// // previousState was set in the authExpiredInterceptor before being redirected to login modal.
				// // since login is succesful, go to stored previousState and clear previousState
				const redirect = this.stateStorageService.getUrl();
                const isProvider = this.principal.hasAnyAuthorityDirect(['ROLE_PROVIDER']);
				console.log('Redirect', redirect);
				if (redirect) {
					// console.log('redirecting...', redirect);
                    // console.log(isProvider);
					if (redirect.indexOf('?') !== -1) {
						const redirectParams = this.parseQueryString(redirect);
						this.stateStorageService.storeUrl(null);
						// console.log(redirectParams);
						this.router.navigate([redirect.split('?')[0]], {queryParams: redirectParams});
					} else  if (redirect === '/mndatrial' && isProvider) {
						// console.log('else redirecting...');
						this.stateStorageService.storeUrl(null);
						this.router.navigate([redirect]);
					} else  if (redirect !== '/mndatrial' && isProvider) {
						// console.log('else redirecting...');
						this.stateStorageService.storeUrl(null);
						this.router.navigate([redirect]);
					} else  if (redirect === '/mndatrial' && !isProvider) {
						console.log('else redirecting...');
						this.stateStorageService.storeUrl(null);
						this.router.navigateByUrl('accessdenied');
                        this.principal.authenticationChange.next(true);
					} else  if (redirect === '/how-it-works-providers' && !isProvider) {
						console.log('else redirecting...');
						this.stateStorageService.storeUrl(null);
						this.router.navigateByUrl('accessdenied');
                        this.principal.authenticationChange.next(true);
					} else  if (redirect) {
						// console.log('else redirecting...');
						this.stateStorageService.storeUrl(null);
						this.router.navigate([redirect]);
					}
				} else {
                    // this.stateStorageService.resetPreviousUrl();
                    const isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN']);
					const isInvestor = this.principal.hasAnyAuthorityDirect(['ROLE_INVESTOR']);
					const isProvider = this.principal.hasAnyAuthorityDirect(['ROLE_PROVIDER']);
                    // console.log(isAdmin, isInvestor, isProvider);
                    let role = {
                        admin: isAdmin,
                        provider: isProvider,
                        investor: isInvestor
                    };
                    this.principal.userRole$.next(role);
					// if(isInvestor || isProvider) {
                    //     this.userOnboarding = true;
					// }
					this.amassSettingService.checkUser().subscribe(data => {
                        console.log('DATA', data);
                        if(data && !data.userOnboarding && !isAdmin) {
                            // console.log('Get Started');
                            this.router.navigate(['/get-started']);
                        } else if (isProvider && data && data.userOnboarding && !data.userRoleOnboarding) {
                            // console.log('Role Onboarding');
                            this.router.navigate(['/provider-onboarding']);
                        } else if (isProvider && data && data.userOnboarding && data.userRoleOnboarding) {
                            // console.log('User Role Onboarding');
                            this.stateStorageService.storeUrl(null);
                            this.router.navigate(['/investors']);
                        } else {
                            // console.log('Providers');
                            this.stateStorageService.storeUrl(null);
                            this.router.navigate(['/providers']);
                        }
					});
				}
			}, error => {
				setTimeout(() => {
					this.csrf = this.csrfService.getCSRF();
                }, 1000);
                this.inactiveUser = null;
                this.authenticationError = true;
				this.http.get('api/ur/user-status?loginID=' + this.username, {observe: 'response', responseType: 'text'}).subscribe(res => {
					if (res.headers.get('x-datamarketplaceapp-params') !== 'Authentication failed' && error.status !== 401 && error.status !== 409) {
						this.inactiveUser = null;
                        this.authenticationError = false;
						return;
                    } else if (error.status === 409) {
                        this.existingUserAuthentication = true;
                        this.authenticationError = false;
                    }
                     else {
						this.inactiveUser = null;
                        this.authenticationError = true;
                        this.existingUserAuthentication = false;
						this.authenticationErrorMessage = 'Incorrect login email and/or password. Please try again.';
						return;
					}
				}, err => {
					this.inactiveUser = err;
					this.authenticationError = false;
					return;
				});
                // this.authenticationErrorMessage = 'Incorrect login email and/or password. Please try again.';
			}).catch(error => {


			});
		} else if (!this.username && this.password) {
			this.authenticationError = true;
			this.authenticationErrorMessage = 'Please enter valid username/email to login';
		} else if (this.username && !this.password) {
			this.authenticationError = true;
			this.authenticationErrorMessage = 'Please enter password to login.';
		} else {
			this.authenticationError = true;
			this.authenticationErrorMessage = 'Please enter valid username/email and password to login.';
		}
    }
    remember(event) {
        if (event.checked = true) {

            this.googleAnalyticsService.emitEvent('Login', 'Checkbox Clicked', 'Login - Checkbox - Remember Me');
        }
    }
	contactUs(events: any) {
		this.googleAnalyticsService.emitEvent(events.label, events.category, events.event);
	}

	// parses the query string
	parseQueryString(queryString: string): {} {
		let myquery;
		const params = {};
		// if the query string is NULL
		if (queryString === null || !queryString) {
			queryString = window.location.search.substring(1);
			return params;
		}
		myquery = queryString.split('?');
		const queries = myquery[1].split('&');
		queries.forEach((indexQuery: string) => {
			const indexPair = indexQuery.split('=');
			const queryKey = decodeURIComponent(indexPair[0]);
			const queryValue = decodeURIComponent(indexPair.length > 1 ? indexPair[1] : '');
			params[queryKey] = queryValue;
		});

		return params;
	}

	forgotPassword(events: any) {
		this.googleAnalyticsService.emitEvent(events.label, events.category, events.event);
		this.mdDialog.closeAll();
		if (navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1) {
			setTimeout(() => {
				this.dialogRef = this.mdDialog.open(ForgotPasswordDialogComponent, {
					width: '400px'
				});
			}, 500);
		} else {
			this.dialogRef = this.mdDialog.open(ForgotPasswordDialogComponent, {
				width: '400px'
			});
		}
	}

	apply(events: any) {
        // this.googleAnalyticsService.emitEvent(events.label, events.category, events.event);
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
		this.mdDialog.closeAll();

		this.dialogRef = this.mdDialog.open(ApplyAccountComponent, {
			width: '570px'
		});
	}

	closeLoginDialog(events: any) {
		this.mdDialog.closeAll();
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
}
