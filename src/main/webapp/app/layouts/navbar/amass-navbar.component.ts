import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiLanguageService } from 'ng-jhipster';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material/dialog';

import { ProfileService } from '../profiles/profile.service';
import { JhiLanguageHelper, Principal, LoginModalService, LoginService, AuthServerProvider } from '../../shared';

import { VERSION, DEBUG_INFO_ENABLED } from '../../app.constants';

import { SigninDialogComponent } from '../../dialog/signindialog.component';
import { ApplyAccountComponent } from '../../account/apply/apply.component';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { ContactDialogComponent } from '../../shared/contactdialog/contact-dialog.component';
import { DataProvider } from '../../entities/data-provider/data-provider.model';
import { CookieService } from 'ngx-cookie';
import { SessionStorageService } from 'ngx-webstorage';
import { Overlay } from '@angular/cdk/overlay';
import { AmassSettingService } from '../../account/settings/settings.service';
import { AddProviderAdminDialogComponent } from 'app/add-provider-admin-dialog/add-provider-admin-dialog.component';
import { HostListener } from '@angular/core';
import { NewsLetterSignUpComponent } from 'app/account/newsletter-signup/newsletter-signup.component';
import { NavbarDataService } from 'app/shared/auth/navbardata.service';

@Component({
    selector: 'jhi-navbar',
    templateUrl: './amass-navbar.component.html',
    styleUrls: [
        'amass-navbar.scss'
    ]
})
export class AmassNavbarComponent implements OnInit {
    dataProvider: DataProvider;
    inProduction: boolean;
    isNavbarCollapsed: boolean;
    userName: string;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    mdDialogRef: MatDialogRef<any>;
    userLevel: boolean;
    @Output() onSearch = new EventEmitter();
    providerId: any;
    mDialogRef: MatDialogRef<any>;
    investor: boolean;
    userSearch: string;
    roleInvestor: boolean = false;
    roleProvider: boolean = false;
    roleAdmin: boolean = false;

    constructor(
        private loginService: LoginService,
        private authServerProvider: AuthServerProvider,
        private languageService: JhiLanguageService,
        private languageHelper: JhiLanguageHelper,
        private sessionStorage: SessionStorageService,
        public principal: Principal,
        private loginModalService: LoginModalService,
        private profileService: ProfileService,
        private router: Router,
        public dialog: MatDialog,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private cookieService: CookieService,
        private overlay: Overlay,
        private amassSettingService: AmassSettingService,
        private navbarData:NavbarDataService
    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
        loginService.getLoggedInName.subscribe(name => this.changeName(name));
    }

    ngOnInit() {
        // this.changeLanguage('en');
        this.userSearch='header';
        this.navbarData.navbarData$.subscribe(message => {
            console.log('message',message);
            if(message === true)
            {
                this.investor = true;
            }
        });
        const isAdmin = this.principal.hasAnyAuthorityDirect(['ROLE_INVESTOR']);
        this.principal.userRole$.subscribe(roleData => {
            this.roleProvider = roleData['provider'];
            this.roleInvestor = roleData['investor'];
            this.roleAdmin = roleData['admin'];
            //console.log("ROLE DATA", roleData, this.roleProvider, this.roleInvestor);
        });
        //console.log(isAdmin, this.roleInvestor, this.roleProvider);
        this.amassSettingService.checkUser().subscribe(data => {
            console.log('data',data);
            console.log('useronboarding',data.userOnboarding);
            if(isAdmin && data && !data.userOnboarding)
            {
                console.log('navbartrue');
                this.investor= false;
            }
            else{
                this.investor = true;
            }
           
        });
        this.languageHelper.getAll().then(languages => {
            this.languages = languages;
        });

        this.profileService.getProfileInfo().then(profileInfo => {
            this.inProduction = profileInfo.inProduction;
            this.swaggerEnabled = profileInfo.swaggerEnabled;
        });
        this.userLevel = this.principal.hasAnyPermissionAboveDirect(200);
        /* this.amassSettingService.setAuthorisedUrl().subscribe(data => {
            this.providerId = data;
            console.log(this.providerId);
            console.log(this.providerId[0].providerRecordID);
        }); */
    }

    changeName(name) {
        this.userName = name;
    }

    changeLanguage(languageKey: string) {
        this.sessionStorage.store('locale', languageKey);
      this.languageService.changeLanguage(languageKey);
    }
    account(events: any) {
        this.amassSettingService.setAuthorisedUrl().subscribe(data => {
            this.providerId = data;
            console.log(this.providerId);
        });
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    newProvider(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    suggestProvider(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout(events) {
        this.collapseNavbar();
        this.cookieService.remove('Cancel');
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        if (this.router.url.includes('providers') && (this.router.url.includes('edit') || this.router.url.includes('admin'))) {
            this.router.navigate(['../']);
            this.cookieService.put('logoutClicked', '1');
        } else {
            this.cookieService.put('logoutClicked', '0');
            this.authServerProvider.logout().subscribe(d => {
                this.principal.authenticate(null);
                this.changeName('User');
                this.router.navigate(['../']);
            }, error => {

            });
        }
    }
    openAboutPage(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }

    openContactPage(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }

    openProvidersPage(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    openLogoutNewsPage(events: any) {
       /*  this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        }); */
       // this.router.navigate(['news']);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    openNewsPage(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    openSigninDialog(events: any) {
        window.scroll(0, 0);
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        this.mdDialogRef = this.dialog.open(SigninDialogComponent, {
            width: '400px'
        });
    }

    openAddProviderDialog() {
        this.mdDialogRef = this.dialog.open(AddProviderAdminDialogComponent, {
            width: '1200px',
            disableClose: true
        });
    }

    openApplyDialog(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
        this.mdDialogRef = this.dialog.open(ApplyAccountComponent, {
            width: '570px'
        });
    }
    openContactDialog(dialogTitle: string, dialogSubTitle: string, dialogType: string, requestType: number) {
        const dialogRef = this.dialog.open(ContactDialogComponent, {
            width: '400px'
        });
        dialogRef.componentInstance.requestType = requestType ? requestType : null;
        dialogRef.componentInstance.dialogTitle = dialogTitle ? dialogTitle : 'Contact Us';
        dialogRef.componentInstance.dialogSubTitle = dialogSubTitle ? dialogSubTitle : null;
        dialogRef.componentInstance.dialogType = dialogType ? dialogType : 'contactUs';

        dialogRef.componentInstance.onButtonAction.subscribe((action: any) => {
            if (action === 'success') {
                console.log('request button');
            }
        });
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }

    entities(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    admin(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    entitiesPageNav(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }

    adminPageNav(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event , events.label);
    }
    searchProvider(event) {
        this.onSearch.emit(event);
    }

    copyAccount(account) {
        return {
            activated: account.activated,
            email: account.email,
            firstName: account.firstName,
            langKey: account.langKey,
            lastName: account.lastName,
            login: account.login,
            imageUrl: account.imageUrl
        };
    }

    @HostListener('mouseleave', ['$event'])
	beforeunloadHandler(event: any) {
		//console.log('POPUP', event);
		const target = event.relatedTarget ? event.relatedTarget.className : null;
		//console.log('RELATED TARGET', target);
		const isSignupCancelled = this.cookieService.get('Cancel');
		console.log('isSignupCancelled',isSignupCancelled);
		setTimeout(() => {
			if (!this.isAuthenticated() && isSignupCancelled === undefined && isSignupCancelled !== 'True' && target === null && this.router.url === '/' && !this.mdDialogRef) {
                
					this.mDialogRef = this.dialog.open(NewsLetterSignUpComponent,{
						width: '500px'
					});
					this.mDialogRef.afterClosed().subscribe(result=> {
						console.log('Result', result);
						if(result === undefined) {
							this.cookieService.put('Cancel', 'True');
						}
					});
					return false;
			  	
			}
	    }, 5000);
	}
}
