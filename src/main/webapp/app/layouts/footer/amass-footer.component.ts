import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import {Principal} from '../../shared';
import { ProfileService } from '../profiles/profile.service';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';

@Component({
	selector: 'jhi-footer',
	templateUrl: './amass-footer.component.html',
	styleUrls: [
		'amass-footer.css',
		'../../../content/scss/amass-form.scss'
	]
})
export class AmassFooterComponent implements OnInit {
	public currentyear: number;
    public newsletterEmail: string;
    isProvider: boolean;
	@ViewChild('subscribeBtn') subscribeBtn: ElementRef;

	public inProduction: boolean;

	constructor(
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private principal: Principal,
        private profileService?: ProfileService
    ) {
		this.currentyear = new Date().getFullYear();
	}

	ngOnInit() {
		document.getElementById('linkedin-follow-btn').innerHTML = document.getElementById('linkedin-follow').innerHTML;
		this.profileService.getProfileInfo().then(profileInfo => {
			this.inProduction = profileInfo.inProduction;
        });
        this.principal.userRole$.subscribe(roleData => {
            this.isProvider = roleData['provider'];
            // console.log("ROLE DATA", roleData, this.isProvider);
        });
    }
    footerLink(events: any) {
        console.log(events);
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }

	isAuthenticated() {
		return this.principal.isAuthenticated();
	}

	triggerSubscribeClick() {
		const el: HTMLElement = this.subscribeBtn.nativeElement as HTMLElement;
        el.click();
	}
}
