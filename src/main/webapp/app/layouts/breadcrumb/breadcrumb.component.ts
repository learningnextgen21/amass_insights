
import {filter} from 'rxjs/operators';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, Params, PRIMARY_OUTLET } from '@angular/router';

import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { Principal } from 'app/shared';

interface IBreadcrumb {
  label: string;
  params?: Params;
  url: string;
}

@Component({
  selector: 'jhi-breadcrumb',
  template: `
	<ol class='amass-breadcrumb' *ngIf="!routUrl">
	  <li *ngIf="!isAuthenticated()"><a [routerLink]=[homeUrl] (click)="breadCrumbNav({'label':'Sitemap - Home ','event':'Internal Link Clicked'})">Home</a><i class='fa fa-angle-right' aria-hidden='true'></i></li>
    <li *ngIf="isAuthenticated()"><a routerLink="providers" (click)="breadCrumbNav({'label':'Sitemap - Home ','event':'Internal Link Clicked'})">Home</a><i class='fa fa-angle-right' aria-hidden='true'></i></li>
    <li *ngFor='let breadcrumb of breadcrumbs; let last = last;'>
        <a [routerLink]='[breadcrumb.url, breadcrumb.params]' *ngIf='!last; else noLink;'
        (click)="breadCrumbNav({'label':'Sitemap - ' + breadcrumb.label,'event':'Internal Link Clicked'}); onClickLink(breadcrumb.url, breadcrumb.params)">{{breadcrumb.label}}</a>
		<ng-template #noLink>{{breadcrumb.label}}</ng-template>
		<i class='fa fa-angle-right' *ngIf='!last' aria-hidden='true'></i>
	  </li>
	</ol>
  `,
  styleUrls: ['./breadcrumb.css']
})
export class BreadcrumbComponent implements OnInit {

  public breadcrumbs: IBreadcrumb[];
  breadCrumbCategory: any;
  homeUrl: string = '';
  @Output() onClick = new EventEmitter();
  routUrl: boolean;
  isAuthenticate: boolean;
  constructor(
    private activatedRoute: ActivatedRoute,
    private principal: Principal,
    private router: Router,
    public googleAnalyticsService: GoogleAnalyticsEventsService
  ) {
	this.breadcrumbs = [];
  }

  ngOnInit() {
      this.principal.identity().then((account) => {
        console.log('BreadCrumb', account);
        //this.homeUrl = 'providers';
      });
     
	const ROUTE_DATA_BREADCRUMB = 'breadcrumb';

	//  subscribe to the NavigationEnd event
	this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe((event) => {

	  //  set breadcrumbs
	  const root: ActivatedRoute = this.activatedRoute.root;
	  this.breadcrumbs = this.getBreadcrumbs(root);
    });
    this.activatedRoute.queryParams.subscribe(params => {
        if (params['signup'] && params['signup'] === 'true') {
            this.routUrl = true;
            console.log(this.routUrl);
        }
        if (params['login'] && params['login'] === 'true') {
            this.routUrl = true;
            console.log(this.routUrl);
        }
    });
  }

  breadCrumbNav(events: any) {
      if (events.label === 'Sitemap - Providers') {
           this.breadCrumbCategory = 'Providers List';
      }else if (events.label === 'Sitemap - News') {
        this.breadCrumbCategory = 'News Articles and Blog Posts';
      }else {
        this.breadCrumbCategory = 'Home';
      }
    this.googleAnalyticsService.emitEvent(this.breadCrumbCategory, events.event , events.label);

  }
  isAuthenticated() {
    return this.principal.isAuthenticated();
}
  onClickLink(url, params) {
    this.onClick.emit(url);
  }

  private getBreadcrumbs(route: ActivatedRoute, url = '', breadcrumbs: IBreadcrumb[] = []): IBreadcrumb[] {
	const ROUTE_DATA_BREADCRUMB = 'breadcrumb';

	//  get the child routes
	const children: ActivatedRoute[] = route.children;

	//  return if there are no more children
	if (children.length === 0) {
	  return breadcrumbs;
	}

	//  iterate over each children
	for (const child of children) {
	  //  verify primary route
	  if (child.outlet !== PRIMARY_OUTLET) {
		continue;
	  }

	  //  verify the custom data property 'breadcrumb' is specified on the route
	  if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
		return this.getBreadcrumbs(child, url, breadcrumbs);
	  }

	  //  get the route's URL segment
	  const routeURL = child.snapshot.url.map((segment) => segment.path).join('/');

	  //  append route URL to URL
	  url += `/${routeURL}`;

	  //  add breadcrumb
	  const breadcrumb: IBreadcrumb = {
		label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
		params: child.snapshot.params,
		'url': url
	  };
    //  breadcrumbs.push(breadcrumb);
      if (breadcrumb.label !== '') {
        breadcrumbs.push(breadcrumb);
      }

	  //  recursive
	  return this.getBreadcrumbs(child, url, breadcrumbs);
	}

	//  we should never get here, but just in case
	return breadcrumbs;
  }

}
