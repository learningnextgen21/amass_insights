import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ErrorComponent } from './error.component';
import { PageNotFoundComponent } from './page-not-found.component';

export const errorRoute: Routes = [
	{
		path: 'error',
		component: ErrorComponent,
		data: {
			authorities: [],
			pageTitle: 'error.title'
		},
	},
	{
		path: 'accessdenied',
		component: ErrorComponent,
		data: {
			authorities: [],
			pageTitle: 'error.accessdeniedtitle',
			pageContentTitle: 'Access Denied',
			error403: true,
			breadcrumb: 'Access Denied'
		},
	},
	{
		path: '404',
		component: PageNotFoundComponent,
		data: {
			authorities: [],
            pageTitle: 'error.404',
            pageContentTitle: '404',
            breadcrumb: 'Error'
		}
	}
];
