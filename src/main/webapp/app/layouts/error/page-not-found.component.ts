import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';
import { Principal } from 'app/shared';

@Component({
    selector: 'jhi-page-not-found',
    templateUrl: './page-not-found.component.html'
})
export class PageNotFoundComponent implements OnInit {
    errorMessage: string;
    error404: boolean;
    pageTitle: string;

    constructor(
        private route: ActivatedRoute,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
        private principal: Principal
    ) {
    }

    ngOnInit() {
        this.principal.identity().then(account => {
            if (account) {
                // console.log(account);
            }
        });
        this.route.data.subscribe(routeData => {
            if (routeData.error404) {
                this.error404 = routeData.error404;
            }
            if (routeData.errorMessage) {
                this.errorMessage = routeData.errorMessage;
            }
            if (routeData.pageContentTitle) {
                this.pageTitle = routeData.pageContentTitle;
			}
        });
    }
    contactUs(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
    home(events: any) {
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
    }
}
