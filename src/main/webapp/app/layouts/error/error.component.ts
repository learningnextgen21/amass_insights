import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';
import { SigninModalService, Principal } from '../../shared/index';
import { GoogleAnalyticsEventsService } from '../../shared/googleanalytics/google-analytics-events.service';

@Component({
	selector: 'jhi-error',
	templateUrl: './error.component.html'
})
export class ErrorComponent implements OnInit {
	errorMessage: string;
	error403: boolean;
	isLoggedIn: boolean;
	pageTitle: string;

	constructor(
		private route: ActivatedRoute,
		public signinModalService: SigninModalService,
		private principal: Principal,
        private location: Location,
        public googleAnalyticsService: GoogleAnalyticsEventsService,
	) {

	}

	ngOnInit() {
        this.principal.authenticationChange.subscribe(result => {
            this.principal.identity().then(account => {
                if (account) {
                    this.isLoggedIn = true;
                } else {
                    this.login();
                }
            });
        });
        this.principal.identity().then(account => {
            if (account) {
                this.isLoggedIn = true;
            } else {
                this.login();
            }
        });
		this.route.data.subscribe(routeData => {
			if (routeData.error403) {
				this.error403 = routeData.error403;
			}
			if (routeData.errorMessage) {
				this.errorMessage = routeData.errorMessage;
			}
			if (routeData.pageContentTitle) {
				this.pageTitle = routeData.pageContentTitle;
			}
		});
	}

	login() {
        this.signinModalService.openDialog();
        this.googleAnalyticsService.emitEvent('Login', 'Popup Link Clicked', 'Access Denied - Login Popup');
	}

	goBack(events: any) {
        this.location.back();
        this.googleAnalyticsService.emitEvent(events.category, events.event, events.label);
	}
}
