import { Component, OnInit, HostListener, Inject, ViewChild, ChangeDetectorRef, AfterViewChecked } from '@angular/core';

import { WINDOW_PROVIDERS, WINDOW } from './window.service';
import { Router, ActivatedRouteSnapshot, NavigationEnd, RoutesRecognized,Event } from '@angular/router';

import { JhiLanguageHelper, StateStorageService, Principal } from '../../shared';
import { AmassFooterComponent } from '../footer/amass-footer.component';
import { Intercom } from 'ng-intercom';
import { INTERCOM_ID } from '../../app.constants';
import { LoaderService } from '../../loader/loaders.service';
import { SessionStorageService } from 'ngx-webstorage';
import { JhiLanguageService } from 'ng-jhipster';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { NewsLetterSignUpComponent } from 'app/account/newsletter-signup/newsletter-signup.component';
import { CookieService } from 'ngx-cookie';
import { filter } from 'rxjs/operators';
import { ViewportScroller, DOCUMENT } from '@angular/common'; //import
import { LoginService} from '../../shared/index';

@Component({
	selector: 'jhi-main',
	templateUrl: './main.component.html',
	styleUrls: [
		'main.css'
	]
})
export class JhiMainComponent implements OnInit, AfterViewChecked {
	public isScrolled: boolean;
	public isHome: boolean;
	showLoader: boolean;
	isNS;
	isRightClickEnabled: boolean;
	@ViewChild(AmassFooterComponent)
    private amassFooter: AmassFooterComponent;
	routerOutletRef;
	mDialogRef: MatDialogRef<any>;

	constructor(
		@Inject(DOCUMENT) private document: Document,
		@Inject(WINDOW) private window: Window,
		private jhiLanguageHelper: JhiLanguageHelper,
		private router: Router,
		private $storageService: StateStorageService,
		private intercom: Intercom,
		private loaderService: LoaderService,
		private cdRef: ChangeDetectorRef,
		private dialog: MatDialog,
		private principal: Principal,
        private sessionStorage: SessionStorageService,
		private languageService: JhiLanguageService,
		private cookieService: CookieService, 
		private viewPortScroller: ViewportScroller,//inject
		private loginService: LoginService
	) {
		
		this.isHome = true;
	}

	private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
		this.isRightClickEnabled = false;
		if (routeSnapshot.data && (!routeSnapshot.data['type'] || (routeSnapshot.data['type'] && routeSnapshot.data['type'] === 'dynamic'))) {
			document.oncontextmenu = this.mischandler;
			document.onmousedown = this.mousehandler;
			document.onmouseup = this.mousehandler;
			document.onselect = this.mischandler;
			this.isRightClickEnabled = false;
		} else {
			document.oncontextmenu = null;
			this.isRightClickEnabled = true;
        }
        let title: string = routeSnapshot.data && routeSnapshot.data['pageTitle'] ? routeSnapshot.data['pageTitle'] : 'datamarketplaceApp';
		if (routeSnapshot.firstChild) {
			title = this.getPageTitle(routeSnapshot.firstChild) || title;
        }
		return title;
	}

	ngOnInit() {
		 //console.log('Router in Main', this.router);
        if (!this.sessionStorage.retrieve('locale')) {
            this.changeLanguage('en');
        }
		this.isNS = (navigator.appName === 'Netscape') ? 1 : 0;
		this.isScrolled = false;
		this.router.events.subscribe(event => {
			//console.log('1', event);
			if (event instanceof NavigationEnd) {
				if(event.url !== '/'){
					setTimeout(() => {
						this.beforeunloadHandler(event);
					}, 2000);
				}
				window.scroll(0, 0);
                console.log('main component title.....', this.getPageTitle(this.router.routerState.snapshot.root));
                this.jhiLanguageHelper.updateTitle(this.getPageTitle(this.router.routerState.snapshot.root));
				ga('set', 'page', event.urlAfterRedirects);
				ga('send', 'pageview');
				if (event.url === '/') {
					this.isHome = true;
					//console.log('URL',event.url);
				} else {
                    this.isHome = false;
				}
				setTimeout(() => {
					window.scroll(0, 0);
				},2000); 	
				
			}
			this.loaderService.status.subscribe((val: boolean) => {
				this.showLoader = val;
			});
		/* 	setTimeout(() => {
				window.scroll(0, 0);
			},2000);  */
		});
		
		this.intercom.boot({
			app_id: INTERCOM_ID,
			widget: {
				activator: '#intercom'
			}
		});
    }

    changeLanguage(languageKey: string) {
        this.sessionStorage.store('locale', languageKey);
      this.languageService.changeLanguage(languageKey);
    }

	mischandler() {
		return false;
	}

	mousehandler(e) {
		const myevent = (this.isNS) ? e : event;
		const eventbutton = (this.isNS) ? myevent.which : myevent.button;
		if ((eventbutton === 2) || (eventbutton === 3)) {
			return false;
		}
	}

	@HostListener('window:scroll', [])
	onWindowScroll() {
		const number = this.window.pageYOffset || this.document.documentElement.scrollTop;
		if (number > 0) {
			this.isScrolled = true;
		} else if (this.isScrolled && number <= 0) {
			this.isScrolled = false;
		}
	}

	@HostListener('mouseleave', ['$event'])
	beforeunloadHandler(event: any) {
		//console.log('POPUP', event);
		// const target = event.relatedTarget ? event.relatedTarget.className : null;
		// //console.log('RELATED TARGET', target);
		// const isSignupCancelled = this.cookieService.get('Cancel');
		// console.log('isSignupCancelled',isSignupCancelled);
		// setTimeout(() => {
		// 	if (!this.isAuthenticated() && isSignupCancelled === undefined && isSignupCancelled !== 'True' && target === null && this.router.url === '/') {
        //         if(!this.dialog) {
		// 			this.mDialogRef = this.dialog.open(NewsLetterSignUpComponent,{
		// 				width: '500px'
		// 			});
		// 			this.mDialogRef.afterClosed().subscribe(result=> {
		// 				console.log('Result', result);
		// 				if(result === undefined) {
		// 					this.cookieService.put('Cancel', 'True');
		// 				}
		// 			});
		// 			return false;
		// 		}
		// 	}
	    // }, 5000);
	}

	isAuthenticated() {
        return this.principal.isAuthenticated();
    }

	ngAfterViewChecked() {
		this.cdRef.detectChanges();
    }

    onClickLink(event) {
        if (this.routerOutletRef) {
            this.routerOutletRef['routerOutletRedirect'] = event;
        }
    }
	
	onActivate(event) {
	/* 	this.router.events
		.pipe(filter((routerEvent: Event) => routerEvent instanceof NavigationEnd))
		.subscribe(() => window.scrollTo(0, 0)); */
		this.routerOutletRef = event;
		/* this.router.events.pipe(
            filter(event => event instanceof NavigationEnd))
            .subscribe(() => this.viewPortScroller.scrollToPosition([0, 0])); */
		window.scroll(0,0);
	}
/* 	doBeforeUnload() {
		// Alert the user window is closing 
		this.loginService.logout();
		return false;
	}
	
	doUnload() {
		// Clear session or do something
		this.loginService.logout();
	} */
	
}
