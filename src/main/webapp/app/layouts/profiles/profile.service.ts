import { Injectable } from '@angular/core';
import { ProfileInfo } from './profile-info.model';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class ProfileService {

    private profileInfoUrl = 'api/profile-info';

    private profileInfo: Promise<ProfileInfo>;

    constructor(private http: HttpClient) {}

    getProfileInfo(): Promise<ProfileInfo> {
        if (!this.profileInfo) {
            this.profileInfo = this.http
                .get<ProfileInfo>(this.profileInfoUrl, { observe: 'response' })
                .pipe(
                    map((res: HttpResponse<ProfileInfo>) => {
                        const data = res.body;
                        const pi = new ProfileInfo();
                        pi.activeProfiles = data['activeProfiles'];
                        pi.ribbonEnv = data.ribbonEnv;
                        if (pi.activeProfiles) {
                            pi.inProduction = pi.activeProfiles.includes('prod');
                            pi.inDevelopment = data.activeProfiles.includes('dev');
                            pi.inQA = data.activeProfiles.includes('qa');
                            pi.swaggerEnabled = pi.activeProfiles.includes('swagger');
                        }
                        return pi;
                    })
                )
                .toPromise();
        }
        return this.profileInfo;
    }
}
