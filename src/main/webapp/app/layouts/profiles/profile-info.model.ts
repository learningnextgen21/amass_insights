export class ProfileInfo {
    activeProfiles: string[];
    ribbonEnv: string;
    inProduction: boolean;
    inDevelopment: boolean;
    inQA: boolean;
    swaggerEnabled: boolean;
}
