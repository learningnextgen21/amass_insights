var googleScript = document.createElement('script');
googleScript.type = 'text/javascript';
googleScript.src = 'https://www.googletagmanager.com/gtag/js?id=UA-63556202-2';
document.getElementsByTagName('head')[0].appendChild(googleScript);
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-63556202-2');

(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
e=o.createElement(i);r=o.getElementsByTagName(i)[0];
e.src='//www.google-analytics.com/analytics.js';
r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
ga('create','UA-63556202-2');

