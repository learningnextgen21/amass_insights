package com.ai.mp.utils;

import java.io.InputStream;

public class FileUtils {
    public static InputStream getResourceAsStream(String fileName){
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return classLoader.getResourceAsStream(fileName);
    }
}
