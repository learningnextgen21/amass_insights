package com.ai.mp.utils;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.Transient;
import java.util.Objects;

public class BasicBean {
	String id, desc;

    public BasicBean(String id, String desc) {
        this.id = id;
        this.desc = desc;
    }

    public BasicBean() {
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    @Transient
    @JsonSerialize
    private boolean locked;

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		BasicBean b = (BasicBean) o;
		if (b.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), b.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "DataProvider{" + "id=" + getId() + ", description='" + getDesc() + "'" + "}";
	}
}
