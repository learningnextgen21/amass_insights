package com.ai.mp.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;

@Converter
public class SetToBeanConverter implements AttributeConverter<Object, String> {
	ObjectMapper mapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(Object data) {
		try {
			return mapper.writeValueAsString(data);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object convertToEntityAttribute(String data) {

		try {
			if (data != null) {
				return mapper.readValue(data, Object.class);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}

