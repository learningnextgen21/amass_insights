package com.ai.mp.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Converter
public class JpaJsonDocumentsConverter implements AttributeConverter<List<String>, String> {

	// ObjectMapper is thread safe
	private final static ObjectMapper objectMapper = new ObjectMapper();

	private Logger log = LoggerFactory.getLogger(getClass());

	@Override
	public String convertToDatabaseColumn(List<String> meta) {
		String jsonString = "";
		try {
			log.debug("Start convertToDatabaseColumn");

			// convert list of POJO to json
			jsonString = objectMapper.writeValueAsString(meta);
			log.debug("convertToDatabaseColumn" + jsonString);

		} catch (JsonProcessingException ex) {
			log.error(ex.getMessage());
		}
		return jsonString;
	}

	@Override
	public List<String> convertToEntityAttribute(String dbData) {
		List<String> list = new ArrayList<String>();
		try {
			log.debug("Start convertToEntityAttribute");

			// convert json to list of POJO
			list = Arrays.asList(objectMapper.readValue(dbData, String[].class));
			log.debug("JsonDocumentsConverter.convertToDatabaseColumn" + list);

		} catch (IOException ex) {
			log.error(ex.getMessage());
		}
		return list;
	}
}