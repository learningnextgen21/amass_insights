package com.ai.mp.utils;

import com.google.gson.*;
import org.springframework.format.datetime.DateFormatter;

import java.lang.reflect.Type;
import java.time.LocalDate;

public class DateToStringTypeAdapter implements JsonSerializer<LocalDate>, JsonDeserializer<LocalDate> {
    private static DateFormatter formatter = new DateFormatter();
    static
    {
        formatter.setPattern("yyyy-mm-dd");
    }
    @Override
    public LocalDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if(json != null) {
            return LocalDate.parse(json.getAsString());
        }
        return null;
    }

    @Override
    public JsonElement serialize(LocalDate src, Type typeOfSrc, JsonSerializationContext context) {
        if(src != null)
            return new JsonPrimitive(src.toString());//Element(src.toString());
        return new JsonPrimitive("");
    }
}
