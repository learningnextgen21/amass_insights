package com.ai.mp.web.rest;

import com.ai.mp.domain.DataComment;
import com.ai.mp.domain.DataEvent;
import com.ai.mp.repository.DataEventRepository;
import com.ai.mp.repository.search.DataEventSearchRepository;
import com.ai.mp.service.DataEventService;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@RestController
@RequestMapping("/api")
public class DataEventResource {

    private final DataEventRepository dataEventRepository;
    private final DataEventSearchRepository dataEventSearchRepository;
    private final Logger log = LoggerFactory.getLogger(DataEventResource.class);
 private DataEventService dataEventService;

    public DataEventResource(DataEventRepository dataEventRepository, DataEventSearchRepository dataEventSearchRepository,DataEventService dataEventService) {
        this.dataEventRepository = dataEventRepository;
        this.dataEventSearchRepository = dataEventSearchRepository;
    this.dataEventService=dataEventService;
    }


    @PostMapping(path = "/data_event",
        produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    public ResponseEntity<DataEvent> createEvent(@RequestBody DataEvent dataEvent) throws URISyntaxException, IOException {
        DataEvent result=new DataEvent();
        result=dataEventRepository.save(dataEvent);
        return ResponseEntity.created(new URI("/api/data_event/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @GetMapping("/data_event")
    @Timed
    public ResponseEntity<List<DataEvent>> getAllDataEvents(@ApiParam Pageable pageable) {
        Page<DataEvent> page = dataEventSearchRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/data_event");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    @PutMapping(path = "/data_event",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    public ResponseEntity<DataEvent> updateEvent(@RequestBody DataEvent dataEvent) throws URISyntaxException, IOException {
        DataEvent result=new DataEvent();
        result=dataEventRepository.save(dataEvent);
        return ResponseEntity.created(new URI("/api/data_event/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);   
}
    @GetMapping("/event-ext/")
    @Timed
    public ResponseEntity<List<DataEvent>> geDataEventExt(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataEvent for query {}", query);
        Page<DataEvent> page = dataEventService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/event");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);    
    }
    
    @GetMapping("/event/db/{id}")
    @Timed
    public ResponseEntity<DataEvent> getDataEventFromDB(@PathVariable Long id) {
        log.debug("REST request to get DataEvent : {}", id);
        DataEvent dataEvent = dataEventRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataEvent));
    }
    
    @DeleteMapping("/event/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataEvent(@PathVariable Long id) {
        log.debug("REST request to delete DataComment : {}", id);
        dataEventRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    @GetMapping("/_search/event")
    @Timed
    public ResponseEntity<List<DataEvent>> searchDataProviders(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataEvent for query {}", query);
        Page<DataEvent> page = dataEventService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/event");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }   
    
    
}