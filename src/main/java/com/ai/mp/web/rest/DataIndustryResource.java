package com.ai.mp.web.rest;

import com.ai.mp.domain.DataIndustry;
import com.ai.mp.repository.DataIndustryRepository;
import com.ai.mp.repository.search.DataIndustrySearchRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing DataIndustry.
 */
@RestController
@RequestMapping("/api")
public class DataIndustryResource {

    private final Logger log = LoggerFactory.getLogger(DataIndustryResource.class);

    private static final String ENTITY_NAME = "dataIndustry";

    private final DataIndustryRepository dataIndustryRepository;

    private final DataIndustrySearchRepository dataIndustrySearchRepository;
    public DataIndustryResource(DataIndustryRepository dataIndustryRepository, DataIndustrySearchRepository dataIndustrySearchRepository) {
        this.dataIndustryRepository = dataIndustryRepository;
        this.dataIndustrySearchRepository = dataIndustrySearchRepository;
    }

    /**
     * POST  /data-industries : Create a new dataIndustry.
     *
     * @param dataIndustry the dataIndustry to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dataIndustry, or with status 400 (Bad Request) if the dataIndustry has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/data-industries")
    @Timed
    public ResponseEntity<DataIndustry> createDataIndustry(@Valid @RequestBody DataIndustry dataIndustry) throws URISyntaxException {
        log.debug("REST request to save DataIndustry : {}", dataIndustry);
        if (dataIndustry.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dataIndustry cannot already have an ID")).body(null);
        }
        DataIndustry result = dataIndustryRepository.save(dataIndustry);
        dataIndustrySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/data-industries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /data-industries : Updates an existing dataIndustry.
     *
     * @param dataIndustry the dataIndustry to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dataIndustry,
     * or with status 400 (Bad Request) if the dataIndustry is not valid,
     * or with status 500 (Internal Server Error) if the dataIndustry couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/data-industries")
    @Timed
    public ResponseEntity<DataIndustry> updateDataIndustry(@Valid @RequestBody DataIndustry dataIndustry) throws URISyntaxException {
        log.debug("REST request to update DataIndustry : {}", dataIndustry);
        if (dataIndustry.getId() == null) {
            return createDataIndustry(dataIndustry);
        }
        DataIndustry result = dataIndustryRepository.save(dataIndustry);
        dataIndustrySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataIndustry.getId().toString()))
            .body(result);
    }

    /**
     * GET  /data-industries : get all the dataIndustries.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dataIndustries in body
     */
    @GetMapping("/data-industries")
    @Timed
    public ResponseEntity<List<DataIndustry>> getAllDataIndustries(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DataIndustries");
        Page<DataIndustry> page = dataIndustryRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/data-industries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /data-industries/:id : get the "id" dataIndustry.
     *
     * @param id the id of the dataIndustry to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dataIndustry, or with status 404 (Not Found)
     */
    @GetMapping("/data-industries/{id}")
    @Timed
    public ResponseEntity<DataIndustry> getDataIndustry(@PathVariable Long id) {
        log.debug("REST request to get DataIndustry : {}", id);
        DataIndustry dataIndustry = dataIndustryRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataIndustry));
    }

    /**
     * DELETE  /data-industries/:id : delete the "id" dataIndustry.
     *
     * @param id the id of the dataIndustry to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/data-industries/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataIndustry(@PathVariable Long id) {
        log.debug("REST request to delete DataIndustry : {}", id);
        dataIndustryRepository.delete(id);
        dataIndustrySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/data-industries?query=:query : search for the dataIndustry corresponding
     * to the query.
     *
     * @param query the query of the dataIndustry search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/data-industries")
    @Timed
    public ResponseEntity<List<DataIndustry>> searchDataIndustries(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataIndustries for query {}", query);
        Page<DataIndustry> page = dataIndustrySearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/data-industries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/data-industries/desc/{id}")
    @Timed
    public String getExplanationByID(@PathVariable Long id) {
        log.debug("REST request to search for a page of DataIndustry for query {}", id);
        DataIndustry element = dataIndustrySearchRepository.findOne(id);
        return element.getExplanation();
    }
}
