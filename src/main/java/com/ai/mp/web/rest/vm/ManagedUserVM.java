package com.ai.mp.web.rest.vm;

import com.ai.mp.domain.Authority;
import com.ai.mp.domain.DataCategory;
import com.ai.mp.domain.LookupCode;
import com.ai.mp.service.dto.UserDTO;
import org.springframework.social.linkedin.api.Company;

import javax.validation.constraints.Size;

import java.time.Instant;
import java.util.Set;

/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */
public class ManagedUserVM extends UserDTO {

    public static final int PASSWORD_MIN_LENGTH = 4;

    public static final int PASSWORD_MAX_LENGTH = 100;

    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;

    public ManagedUserVM() {
        // Empty constructor needed for Jackson.
    }

    public ManagedUserVM(Long id, String login, String password, String firstName, String lastName, String Company,
                         String email, boolean activated, boolean interest_sell_data_product, boolean notify_news_update, boolean professional_investor, String imageUrl, String langKey,
                         String createdBy, Instant createdDate, String lastModifiedBy, Instant lastModifiedDate,LookupCode rolePermission,
                         Set<String> authorities, Set<DataCategory> categories, Set<Integer> permissions) {

        super(id, login, firstName, lastName, Company, email, activated, interest_sell_data_product, notify_news_update, professional_investor, imageUrl, langKey,
            createdBy, createdDate, lastModifiedBy, lastModifiedDate, rolePermission, authorities, categories, permissions);

        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "ManagedUserVM{" +
            "} " + super.toString();
    }
}
