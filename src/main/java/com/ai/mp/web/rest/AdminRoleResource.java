package com.ai.mp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ai.mp.domain.AdminRole;

import com.ai.mp.repository.AdminRoleRepository;
import com.ai.mp.repository.search.AdminRoleSearchRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AdminRole.
 */
@RestController
@RequestMapping("/api")
public class AdminRoleResource {

    private final Logger log = LoggerFactory.getLogger(AdminRoleResource.class);

    private static final String ENTITY_NAME = "adminRole";

    private final AdminRoleRepository adminRoleRepository;

    private final AdminRoleSearchRepository adminRoleSearchRepository;
    public AdminRoleResource(AdminRoleRepository adminRoleRepository, AdminRoleSearchRepository adminRoleSearchRepository) {
        this.adminRoleRepository = adminRoleRepository;
        this.adminRoleSearchRepository = adminRoleSearchRepository;
    }

    /**
     * POST  /admin-roles : Create a new adminRole.
     *
     * @param adminRole the adminRole to create
     * @return the ResponseEntity with status 201 (Created) and with body the new adminRole, or with status 400 (Bad Request) if the adminRole has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/admin-roles")
    @Timed
    public ResponseEntity<AdminRole> createAdminRole(@Valid @RequestBody AdminRole adminRole) throws URISyntaxException {
        log.debug("REST request to save AdminRole : {}", adminRole);
        if (adminRole.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new adminRole cannot already have an ID")).body(null);
        }
        AdminRole result = adminRoleRepository.save(adminRole);
        adminRoleSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/admin-roles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /admin-roles : Updates an existing adminRole.
     *
     * @param adminRole the adminRole to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated adminRole,
     * or with status 400 (Bad Request) if the adminRole is not valid,
     * or with status 500 (Internal Server Error) if the adminRole couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/admin-roles")
    @Timed
    public ResponseEntity<AdminRole> updateAdminRole(@Valid @RequestBody AdminRole adminRole) throws URISyntaxException {
        log.debug("REST request to update AdminRole : {}", adminRole);
        if (adminRole.getId() == null) {
            return createAdminRole(adminRole);
        }
        AdminRole result = adminRoleRepository.save(adminRole);
        adminRoleSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, adminRole.getId().toString()))
            .body(result);
    }

    /**
     * GET  /admin-roles : get all the adminRoles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of adminRoles in body
     */
    @GetMapping("/admin-roles")
    @Timed
    public ResponseEntity<List<AdminRole>> getAllAdminRoles(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of AdminRoles");
        Page<AdminRole> page = adminRoleRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/admin-roles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /admin-roles/:id : get the "id" adminRole.
     *
     * @param id the id of the adminRole to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the adminRole, or with status 404 (Not Found)
     */
    @GetMapping("/admin-roles/{id}")
    @Timed
    public ResponseEntity<AdminRole> getAdminRole(@PathVariable Long id) {
        log.debug("REST request to get AdminRole : {}", id);
        AdminRole adminRole = adminRoleRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(adminRole));
    }

    /**
     * DELETE  /admin-roles/:id : delete the "id" adminRole.
     *
     * @param id the id of the adminRole to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/admin-roles/{id}")
    @Timed
    public ResponseEntity<Void> deleteAdminRole(@PathVariable Long id) {
        log.debug("REST request to delete AdminRole : {}", id);
        adminRoleRepository.delete(id);
        adminRoleSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/admin-roles?query=:query : search for the adminRole corresponding
     * to the query.
     *
     * @param query the query of the adminRole search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/admin-roles")
    @Timed
    public ResponseEntity<List<AdminRole>> searchAdminRoles(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AdminRoles for query {}", query);
        Page<AdminRole> page = adminRoleSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/admin-roles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
