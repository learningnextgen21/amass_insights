package com.ai.mp.web.rest;

import com.ai.mp.domain.DataSource;
import com.ai.mp.repository.DataSourceRepository;
import com.ai.mp.repository.search.DataSourceSearchRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing DataSource.
 */
@RestController
@RequestMapping("/api")
public class DataSourceResource {

    private final Logger log = LoggerFactory.getLogger(DataSourceResource.class);

    private static final String ENTITY_NAME = "dataSource";

    private final DataSourceRepository dataSourceRepository;

    private final DataSourceSearchRepository dataSourceSearchRepository;
    public DataSourceResource(DataSourceRepository dataSourceRepository, DataSourceSearchRepository dataSourceSearchRepository) {
        this.dataSourceRepository = dataSourceRepository;
        this.dataSourceSearchRepository = dataSourceSearchRepository;
    }

    /**
     * POST  /data-sources : Create a new dataSource.
     *
     * @param dataSource the dataSource to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dataSource, or with status 400 (Bad Request) if the dataSource has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/data-sources")
    @Timed
    public ResponseEntity<DataSource> createDataSource(@Valid @RequestBody DataSource dataSource) throws URISyntaxException {
        log.debug("REST request to save DataSource : {}", dataSource);
        if (dataSource.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dataSource cannot already have an ID")).body(null);
        }
        DataSource result = dataSourceRepository.save(dataSource);
        dataSourceSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/data-sources/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /data-sources : Updates an existing dataSource.
     *
     * @param dataSource the dataSource to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dataSource,
     * or with status 400 (Bad Request) if the dataSource is not valid,
     * or with status 500 (Internal Server Error) if the dataSource couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/data-sources")
    @Timed
    public ResponseEntity<DataSource> updateDataSource(@Valid @RequestBody DataSource dataSource) throws URISyntaxException {
        log.debug("REST request to update DataSource : {}", dataSource);
        if (dataSource.getId() == null) {
            return createDataSource(dataSource);
        }
        DataSource result = dataSourceRepository.save(dataSource);
        dataSourceSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataSource.getId().toString()))
            .body(result);
    }

    /**
     * GET  /data-sources : get all the dataSources.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dataSources in body
     */
    @GetMapping("/data-sources")
    @Timed
    public ResponseEntity<List<DataSource>> getAllDataSources(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DataSources");
        Page<DataSource> page = dataSourceRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/data-sources");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /data-sources/:id : get the "id" dataSource.
     *
     * @param id the id of the dataSource to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dataSource, or with status 404 (Not Found)
     */
    @GetMapping("/data-sources/{id}")
    @Timed
    public ResponseEntity<DataSource> getDataSource(@PathVariable Long id) {
        log.debug("REST request to get DataSource : {}", id);
        DataSource dataSource = dataSourceRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataSource));
    }

    /**
     * DELETE  /data-sources/:id : delete the "id" dataSource.
     *
     * @param id the id of the dataSource to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/data-sources/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataSource(@PathVariable Long id) {
        log.debug("REST request to delete DataSource : {}", id);
        dataSourceRepository.delete(id);
        dataSourceSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/data-sources?query=:query : search for the dataSource corresponding
     * to the query.
     *
     * @param query the query of the dataSource search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/data-sources")
    @Timed
    public ResponseEntity<List<DataSource>> searchDataSources(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataSources for query {}", query);
        Page<DataSource> page = dataSourceSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/data-sources");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    @GetMapping("/data-sources/desc/{id}")
    @Timed
    public String getExplanationByID(@PathVariable Long id) {
        log.debug("REST request to search for a page of DataSource for query {}", id);
        DataSource element = dataSourceSearchRepository.findOne(id);
        return element.getExplanation();
    }
}
