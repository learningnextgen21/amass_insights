package com.ai.mp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ai.mp.config.ApplicationProperties;
import com.ai.mp.domain.CardDetails;
import com.ai.mp.domain.Payment;
import com.ai.mp.domain.Products;
import com.ai.mp.domain.User;
import com.ai.mp.domain.UserCheckout;
import com.ai.mp.repository.CardRepository;
import com.ai.mp.repository.PaymentRepository;
import com.ai.mp.repository.UserCheckoutRepository;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.security.SecurityUtils;
import com.ai.mp.service.MailService;
import com.ai.mp.service.PaymentService;
import com.ai.mp.web.rest.errors.BadRequestAlertException;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stripe.Stripe;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.exception.RateLimitException;
import com.stripe.exception.StripeException;
import com.stripe.model.Card;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.Plan;
import com.stripe.model.Product;
import com.stripe.model.Subscription;
import com.stripe.model.Token;

import io.github.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class PaymentResource {
    private final Logger log = LoggerFactory.getLogger(PaymentResource.class);

    private static final String ENTITY_NAME = "payment";

    private final UserCheckoutRepository userCheckoutRepository;

    private final MailService mailService;

    private final ApplicationProperties applicationProperties;

    /**
     * PUT /payments/currentuser : Updates an existing payment.
     *
     * @param payment the payment to create with the current connected user
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     *         payment, or with status 400 (Bad Request) if the payment is not
     *         valid, or with status 500 (Internal Server Error) if the payment
     *         couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws StripeException
     */
    @PutMapping("/payments/currentuser")
    public ResponseEntity<?> createPaymentCurrentUser(@Valid @RequestBody Payment payment)
        throws URISyntaxException, StripeException {
        log.debug("REST request to update Payment : {}", payment);
        ResponseEntity<Payment> p = null;
        if (payment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    //    System.out.println("TEST");
        String userstr = SecurityUtils.getCurrentUserLogin();
       User users=userRepository.findUserByLogin(userstr);
        if (userstr!=null) {
            Optional<User> user = userRepository.findOneByLogin(userstr);
            payment.setUser(user.get());
        }

        // Set your secret key: remember to change this to your live secret key in
        // production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
       if(applicationProperties.getStripeKey()!=null)
        Stripe.apiKey=applicationProperties.getStripeKey();





       // Token is created using Checkout or Elements!
        // Get the payment token ID submitted by the form:
        // String token = request.getParameter("stripeToken");
	  		/*Map<String, Object> card = new HashMap<>();
	  		card.put("number", "4242424242424242");
	  		card.put("exp_month", 12);
	  		card.put("exp_year", 2020);
	  		card.put("cvc", "314");
	  		Map<String, Object> paramss = new HashMap<>();
	  		paramss.put("card", card);

	  		Token token = Token.create(paramss);
	  		System.out.println("!!!!!"+token.toString());

	  		payment.setToken(token.getId());*/

        /*System.out.println("paymentrs"+payment.getCurrency());
        payment.setCurrency("INR");
        payment.setAmount(50);*/

        try {
            Map<String, Object> paramss = new HashMap<>();
            paramss.put(
              "description",payment.getDescription());
            paramss.put(
                    "email",payment.getEmail());
            paramss.put(
                    "name",payment.getName());
            //paramss.put("source", payment.getToken());
            Customer customer = Customer.create(paramss);
        //    System.out.println("customer"+customer.getId());
        //    System.out.println("sources"+customer.getSources());
            Map<String, Object> params1 = new HashMap<>();
            params1.put("source", payment.getToken());
            Card card =
         		   (Card) customer.getSources().create(params1);
            Map<String, Object> params = new HashMap<>();
            params.put("amount", payment.getAmount());
            params.put("currency", payment.getCurrency());
            params.put("description", payment.getDescription());
            params.put("source", card.getId());
            params.put("capture", true);
            params.put("customer", customer.getId());
           // Charge charge = Charge.create(params);
           // System.out.println("Charge"+charge);
        //    System.out.println("Payement"+payment);
          
            Payment result = new Payment();
                UserCheckout checkOut=userCheckoutRepository.finduser(payment.getUser().getId());
                checkOut.setPayment(true);
                checkOut=userCheckoutRepository.save(checkOut);
               String subscriptionStatus= paymentService.addProduct(checkOut,customer.getId());
               if(!subscriptionStatus.equalsIgnoreCase("incomplete"))
               {
                if(checkOut.getSubscription().equalsIgnoreCase("Silver"))
                {
                	  result = paymentRepository.save(payment);
                     CardDetails cards=new CardDetails();
                     cards.setCustomerId(customer.getId());
                     cards.setCardId(card.getId());
                     cards.setPayment(result);
                     cards.setUser(users);
                      cardRepository.save(cards);
                	String subscription ="ROLE_SILVER";
                    userRepository.updateRole(result.getUser().getId(),subscription);
                    result.setExpirationDate(LocalDate.now().plusYears(1));
                    paymentRepository.save(result);
                }
                else
                {
                	  result = paymentRepository.save(payment);
                     CardDetails cards=new CardDetails();
                     cards.setCustomerId(customer.getId());
                     cards.setCardId(card.getId());
                     cards.setPayment(result);
                     cards.setUser(users);
                      cardRepository.save(cards);
                	String subscription ="ROLE_BRONZE";
                    userRepository.updateRole(result.getUser().getId(),subscription);
                    result.setExpirationDate(LocalDate.now( )
                            .plusMonths( 1 ));
                    paymentRepository.save(result);
                }
                mailService.sendUpgradeConfirmation(checkOut);
                mailService.sendUpgradeConfirmationUser(checkOut);
           

            p = ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, payment.getId().toString()))
                .body(result);
            //result.setReceipt(charge.toJson());
           // this.updatePayment(result);
               }
               else
               {
                   return new ResponseEntity<>(400, HttpStatus.BAD_REQUEST);

               }
        } catch (CardException e) {
            // Since it's a decline, CardException will be caught
        //    System.out.println("Status is: " + e.getCode());
         //   System.out.println("Message is: " + e.getMessage());
           // throw new BadRequestAlertException("CardException", ENTITY_NAME, "");
            if(e.getCode().equalsIgnoreCase("incorrect_zip"))
            {
            	UserCheckout userCheckout=userCheckoutRepository.finduser(users.getId());
            	userCheckout.setSubscribed(false);
            	userCheckoutRepository.save(userCheckout);
            }
            return new ResponseEntity<>(e.getCode(), HttpStatus.BAD_REQUEST);

        } catch (RateLimitException e) {
            // Too many requests made to the API too quickly
            throw new BadRequestAlertException("RateLimitException", ENTITY_NAME,
                "Too many requests made to the API too quickly");
        } catch (InvalidRequestException e) {
            // Invalid parameters were supplied to Stripe's API
            throw new BadRequestAlertException("InvalidRequestException", ENTITY_NAME,
                "Invalid parameters were supplied to Stripe's API");
        } catch (AuthenticationException e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            throw new BadRequestAlertException("AuthenticationException", ENTITY_NAME,
                "Authentication with Stripe's API failed, maybe you changed API keys recently");
        } catch (StripeException e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            throw new BadRequestAlertException("An error occured", ENTITY_NAME, "Error");
        } catch (Exception e) {
            // Something else happened, completely unrelated to Stripe
            throw new BadRequestAlertException("An error occured", ENTITY_NAME, "Error");
        }
        return p;
    }
    private final UserRepository userRepository;
    private final PaymentRepository paymentRepository;
    private final CardRepository cardRepository;
    private final PaymentService paymentService;
    public PaymentResource(PaymentRepository paymentRepository, UserRepository userRepository,UserCheckoutRepository userCheckoutRepository,MailService mailService,ApplicationProperties applicationProperties,CardRepository cardRepository,PaymentService paymentService) {
        this.userRepository = userRepository;
        this.paymentRepository = paymentRepository;
        this.userCheckoutRepository=userCheckoutRepository;
        this.mailService=mailService;
        this.applicationProperties=applicationProperties;
        this.cardRepository=cardRepository;
        this.paymentService=paymentService;
    }

    /**
     * POST  /payments : Create a new payment.
     *
     * @param payment the payment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new payment, or with status 400 (Bad Request) if the payment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payments")
    public ResponseEntity<Payment> createPayment(@Valid @RequestBody Payment payment) throws URISyntaxException {
        log.debug("REST request to save Payment : {}", payment);
        if (payment.getId() != null) {
            throw new BadRequestAlertException("A new payment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Payment result = paymentRepository.save(payment);
        return ResponseEntity.created(new URI("/api/payments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /payments : Updates an existing payment.
     *
     * @param payment the payment to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated payment,
     * or with status 400 (Bad Request) if the payment is not valid,
     * or with status 500 (Internal Server Error) if the payment couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/payments")
    public ResponseEntity<Payment> updatePayment(@Valid @RequestBody Payment payment) throws URISyntaxException {
        log.debug("REST request to update Payment : {}", payment);
        if (payment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Payment result = paymentRepository.save(payment);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, payment.getId().toString()))
            .body(result);
    }

    /**
     * GET  /payments : get all the payments.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of payments in body
     */
    @GetMapping("/payments")
    public List<Payment> getAllPayments() {
        log.debug("REST request to get all Payments");
        return paymentRepository.findAll();
    }

    /**
     * GET  /payments/:id : get the "id" payment.
     *
     * @param id the id of the payment to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the payment, or with status 404 (Not Found)
     */
    @GetMapping("/payments/{id}")
    public ResponseEntity<Payment> getPayment(@PathVariable Long id) {
        log.debug("REST request to get Payment : {}", id);
        Optional<Payment> payment = paymentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(payment);
    }

    /**
     * DELETE  /payments/:id : delete the "id" payment.
     *
     * @param id the id of the payment to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/payments/{id}")
    public ResponseEntity<Void> deletePayment(@PathVariable Long id) {
        log.debug("REST request to delete Payment : {}", id);
        paymentRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    @GetMapping("/payments/{userid}")
    @Timed
    public List<Payment> paymentDetails(@PathVariable Long id) {

    String user=SecurityUtils.getCurrentUserLogin();
    User users=userRepository.findByLogin(user);
    List<Payment>payment=paymentRepository.findByUser(users.getId());
	return payment;
    }
    @GetMapping("/cardDetais")
    @Timed
    public ResponseEntity<?>cardDetails() {
    	String user=SecurityUtils.getCurrentUserLogin();
        User users=userRepository.findByLogin(user);
       CardDetails cardDetails= cardRepository.findByUser(users.getId());
       if(null!=cardDetails)
       {
    	   Stripe.apiKey = applicationProperties.getStripeKey();
           Card card=null;
           ObjectMapper objectMapper = new ObjectMapper();
           objectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
           List<CardDetails> cardDetailList=new ArrayList<CardDetails>();
           try {
        	 /* for(CardDetails cardDetail:cardDetails)
        	  {*/
    		Customer customer =
    				   Customer.retrieve(cardDetails.getCustomerId());
    		card =
    				  (Card) customer.getSources().retrieve(
    						  cardDetails.getCardId()
    				  );

    		cardDetails.setName(card.getName());
    		cardDetails.setExpMonth(card.getExpMonth());
    		cardDetails.setExpYear(card.getExpYear());
    		cardDetails.setAddressZip(card.getAddressZip());
    		cardDetails.setCardNumber(card.getLast4());
    		/*cardDetailList.add(cardDetail);
        	  }*/
    	} catch (StripeException e) {
    		// TODO Auto-generated catch block
    		//e.printStackTrace();
        	return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);

    	}

       }
       else
       {
       	return new ResponseEntity("Card Details Not found", HttpStatus.OK);

       }
    	return new ResponseEntity(cardDetails, HttpStatus.OK);


    }

    @PostMapping("/updateCardDetails")
    @Timed
    public ResponseEntity<CardDetails> updateCardDetails(@Valid @RequestBody CardDetails cardDetail) throws URISyntaxException, StripeException {
    	Stripe.apiKey = applicationProperties.getStripeKey();
    	String user=SecurityUtils.getCurrentUserLogin();
        User users=userRepository.findByLogin(user);
       CardDetails cardDetails= cardRepository.findByUser(users.getId());
    //    System.out.println("expyear"+cardDetail.getExpYear());
    //    System.out.println("name"+cardDetail.getName());
    //    System.out.println("address_zip"+cardDetail.getAddressZip());
    //    System.out.println("exp_month"+cardDetail.getExpMonth());
    //    System.out.println("cusid"+cardDetail.getCustomerId());
    	try {
       Customer customer =
    			  Customer.retrieve(cardDetails.getCustomerId());
    	Card card =
    			  (Card) customer.getSources().retrieve(cardDetails.getCardId());

    	Map<String, Object> params = new HashMap<>();
    	params.put("name", cardDetail.getName());
    	params.put("address_zip", cardDetail.getAddressZip());
    	params.put("exp_month", cardDetail.getExpMonth());
    	params.put("exp_year", cardDetail.getExpYear());

    	Card cards= (Card) card.update(params);
    	}
    	catch(Exception e)
    	{
    	//	System.out.println("message"+e.getMessage());
        	return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

    	}
    	return new ResponseEntity(cardDetails, HttpStatus.OK);
    }
    @PostMapping("/saveCardDetails")
    @Timed
    public ResponseEntity<CardDetails> saveCardDetails(@Valid @RequestBody String tokenId) throws URISyntaxException, StripeException {
    	Stripe.apiKey = applicationProperties.getStripeKey();
    	String user=SecurityUtils.getCurrentUserLogin();
        User users=userRepository.findByLogin(user);
        CardDetails cardDetails= cardRepository.findByUser(users.getId());
        try {
            Customer customer =
         			  Customer.retrieve(cardDetails.getCustomerId());
            Map<String, Object> params = new HashMap<>();
            params.put("source", tokenId);
            Card card =
            		  (Card) customer.getSources().create(params);
    }
	catch(Exception e)
	{
	//	System.out.println("message"+e.getMessage());
    	return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	return new ResponseEntity(cardDetails, HttpStatus.OK);
    }


    @PostMapping("/product")
    @Timed
    public ResponseEntity<Products> product() throws URISyntaxException, StripeException {
    	List<Object> items = new ArrayList<>();
		Map<String, Object> item1 = new HashMap<>();
    	Stripe.apiKey = applicationProperties.getStripeKey();
    	/*Map<String, Object> params = new HashMap<>();
    	params.put("name", "Bronze");
    	Product product = Product.create(params);
    	System.out.println("product"+product);
    	Products products = new Products();
    	products.setProductId(product.getId());
    	products.setProductName(product.getName());

    	Map<String, Object> productss = new HashMap<>();
    	productss.put("id",product.getId());
    	productss.put("name",product.getName());
    	Map<String, Object> paramss = new HashMap<>();
    	paramss.put("amount", "100");
    	paramss.put("currency", "INR");
    	paramss.put("interval", "month");
    	paramss.put("product", productss);
    	Plan plan = Plan.create(paramss);
    	System.out.println("plan"+plan);*/
    	//products.setPlanId(plan.getId());
    	item1.put("plan","plan_HEFC2QUpwyI1TV");
		items.add(item1);
		Map<String, Object> paramsItem = new HashMap<>();
		paramsItem.put("customer","cus_HEFcUuRvWlXIr6" );
		paramsItem.put("items", items);
		Subscription subscription =
		  Subscription.create(paramsItem);
    //	System.out.println("subscription"+subscription);

		//products.setSubscriptionId(subscription.getId());
		//productRepository.save(products);

    	return new ResponseEntity<>(HttpStatus.OK);


    }
    }
