package com.ai.mp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ai.mp.domain.DataResources;
import com.ai.mp.domain.User;
import com.ai.mp.repository.DataResourceRepository;
import com.ai.mp.service.DataResourceService;
import com.ai.mp.service.ElasticsearchGenericService;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.service.UserService;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.util.Utils;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class DataResource {

    private final Logger log = LoggerFactory.getLogger(DataProviderResource.class);
    private static final String ENTITY_NAME = "dataResources";
    private final UserService userService;
    private final DataResourceService dataResourceService;
    private final DataResourceRepository dataResourceRepository;
    private final ElasticsearchIndexService elasticsearchIndexService;
    private final ElasticsearchGenericService elasticSearchGenericService;


    public DataResource(UserService userService,DataResourceService dataResourceService,DataResourceRepository dataResourceRepository,ElasticsearchIndexService elasticsearchIndexService,ElasticsearchGenericService elasticSearchGenericService)
    {
        this.userService=userService;
        this.dataResourceService=dataResourceService;
        this.dataResourceRepository=dataResourceRepository;
        this.elasticsearchIndexService=elasticsearchIndexService;
        this.elasticSearchGenericService = elasticSearchGenericService;
    }

    @PostMapping("/resources")
    @Timed
    public ResponseEntity<DataResources> createDataResources(@Valid @RequestBody DataResources dataResources) throws URISyntaxException {

        log.debug("REST request to save DataResources : {}", dataResources);
        DataResources resources=new DataResources();
        if(dataResources.getId()!=null)
        {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME,"idexists" , "A new dataResources cannot already have an ID")).body(null);

        }

        User user = userService.getUser();
        dataResources.setRecordID("recr"+ Utils.randomString(13,13));
        dataResources.setCreatorUserId(user.getId());
        resources=dataResourceService.save(dataResources);

        if(resources.getId()!=null)
        {
            elasticsearchIndexService.reindexSingleResource(resources.getId());
        }
        return ResponseEntity.created(new URI("/api/data-resources/"+resources.getId())).headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, resources.getId().toString())).body(resources);

    }

    @PutMapping("/resources")
    @Timed
    public ResponseEntity<?> updateDataResources(@Valid @RequestBody DataResources dataResources) throws URISyntaxException {

        if(dataResources.getId()==null)
        {
            return createDataResources(dataResources);
        }
        User user = userService.getUser();
        DataResources resource=new DataResources();
        if(dataResources.getId()!=null)
        {
            DataResources resources=dataResourceRepository.findOne(dataResources.getId());
            if(null!=resources.getWebCreatedDate())
            {
                dataResources.setWebCreatedDate(resources.getWebCreatedDate());
            }
            dataResources.setCreatorUserId(resources.getCreatorUserId());
            dataResources.setEditorUserId(user.getId());
        }
        resource=dataResourceRepository.save(dataResources);
        if(resource.getId()!=null)
        {
            elasticsearchIndexService.reindexSingleResource(resource.getId());
        }


        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, resource.getId()
            .toString())).body("{\"status\": \"success\",\"message\": \"Updated successfully\"}");
    }

    @GetMapping("/resources/{id}")
    @Timed
    public ResponseEntity<DataResources>getDataResources(@PathVariable Long id)
    {
        log.debug("REST request to get DataResources : {}", id);
        DataResources resources=dataResourceRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(resources));
    }

    @DeleteMapping("/resources/{id}")
    @Timed
    public ResponseEntity<Void>deleteDataResource(@PathVariable Long id)
    {
        log.debug("REST request to get DataResources : {}", id);
        dataResourceRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();

    }


    @PostMapping("/resourcesList")
    @Timed
    public ResponseEntity<List<DataResources>>resourcesList(@RequestBody List<DataResources>resources)
    {
        List<DataResources> resourcesList=dataResourceService.saveList(resources);
        return new ResponseEntity <List<DataResources>> (resourcesList, HttpStatus.ACCEPTED);

    }

    @DeleteMapping("/resources")
    @Timed
    public ResponseEntity<?>deleteResources(@RequestParam(value="providerId", required=true) String providerId, @RequestParam(value="resourceId", required=true) String resourceId)
    {

  Long provider=Long.parseLong(providerId);
  Long resource=Long.parseLong(resourceId);
    	dataResourceService.deleteResourceProviders(provider,resource);
        elasticsearchIndexService.reindexSingleResource(resource);

        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, resource
            .toString())).body("{\"status\": \"success\",\"message\": \"Updated successfully\"}");
    }
    
    @PostMapping("/resourceLock")
    @Timed
    public ResponseEntity<List<DataResources>>resourceLock(@RequestBody List<DataResources>resources)
    {
        List<DataResources>  resource=elasticSearchGenericService.resourceLock(resources);
    	return new ResponseEntity <List<DataResources>> (resource, HttpStatus.ACCEPTED);

    }

}
