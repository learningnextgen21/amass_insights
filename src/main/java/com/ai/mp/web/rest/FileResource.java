package com.ai.mp.web.rest;

import com.ai.core.StrUtils;
import com.ai.mp.config.ApplicationProperties;
import com.ai.mp.domain.*;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.repository.DataResourceRepository;
import com.ai.mp.repository.DocumentsMetadataRepository;
import com.ai.mp.repository.search.DataProviderSearchRepository;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.service.UserService;
import com.ai.mp.utils.RandomUtil;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.util.Utils;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.ai.mp.repository.ProviderLogoRepository;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

@RestController
@RequestMapping("/api")
public class FileResource {


    @Autowired
    private ProviderLogoRepository providerLogoRepository;

    @Autowired
    private DataProviderSearchRepository providerSearchRepository;

    private final DocumentsMetadataRepository documentsMetadataRepository;
    private final ApplicationProperties applicationProperties;
    private final DataProviderRepository dataProviderRepository;
    private final UserService userService;
    private  final DataResourceRepository dataResourceRepository;
    private final ElasticsearchIndexService elasticsearchIndexService;

    public FileResource(DataProviderRepository dataProviderRepository, DocumentsMetadataRepository documentsMetadataRepository, ApplicationProperties applicationProperties, ProviderLogoRepository providerLogoRepository, DataProviderSearchRepository providerSearchRepository, UserService userService, DataResourceRepository dataResourceRepository, ElasticsearchIndexService elasticsearchIndexService) {
        this.documentsMetadataRepository = documentsMetadataRepository;
        this.applicationProperties = applicationProperties;
        this.dataProviderRepository = dataProviderRepository;
        this.providerLogoRepository=providerLogoRepository;
        this.providerSearchRepository=providerSearchRepository;
        this.userService = userService;
        this.dataResourceRepository = dataResourceRepository;
        this.elasticsearchIndexService = elasticsearchIndexService;
    }
    private byte[] fileStream;
    private String fileName;

    public byte[] getFileStream() {
        return fileStream;
    }

    public void setFileStream(byte[] fileStream) {
        this.fileStream = fileStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Timed
    @GetMapping(value = "/filedownload/{fileID}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE, name="name of api")
    @ApiOperation(value = "getFile", notes = "API to download the file from S3 bucket which is linked to provider. The s3 bucket is accessed from the application server by mounting it as a drive using s3fs. s3fs is an open source FUSE based utility. For more details: https://github.com/s3fs-fuse/s3fs-fuse")
    public ResponseEntity getFile(@PathVariable("fileID") @ApiParam(value = "The google drive based file ID as retrieved via Google Drive API") String fileID) throws IOException{
        if(StrUtils.isNotBlank(fileID)) {
            DocumentsMetadata documentsMetadata = documentsMetadataRepository.findByFileIDEquals(fileID);
            if(documentsMetadata != null) {
//                InputStream is = GoogleDriveInfo.downloadFile(fileID);
                File file = new File(applicationProperties.getDocumentsFolder() + documentsMetadata.getFile_path());
                if (file.exists())
                {
                    return ResponseEntity.ok()
                        .header("Content-Disposition", "attachment; filename=" + documentsMetadata.getFileName() + "." + documentsMetadata.getFile_extension())
                        .contentLength(file.length())
                        .lastModified(file.lastModified())
                        .contentType(MediaType.APPLICATION_OCTET_STREAM)
                        .body(new FileSystemResource(file));
                } else {
                    return ResponseEntity.ok().body("file not found");
                }
            }
        }
        return null;
    }

    @RequestMapping(value = "/single-file-upload", method =RequestMethod.POST)
    @Timed
    public ResponseEntity<DocumentsMetadata> fileUpload(@RequestParam(value="providerId")String recordID,
                                                       @RequestParam(value="fileUploads")
                                                           MultipartFile fileUploads) throws URISyntaxException, IOException, MissingServletRequestParameterException {
       // log.debug("REST request to save FileUpload : {}", fileUploads);

        DataProvider udp = dataProviderRepository.findByRecordID(recordID);
        //DocumentsMetadata opp=documentsMetadataRepository.findOne(dataProvider);

        String  sFilesDirectory =  applicationProperties.getDocumentsMetadataFolder() + udp.getRecordID() + "-" + udp.getProviderName();
        String downloadFile=applicationProperties.getDownloadFile() + udp.getRecordID() + "-" + udp.getProviderName();
        File dirFiles = new File(sFilesDirectory);
        dirFiles.mkdirs();
        DocumentsMetadata documentsMetadata=new DocumentsMetadata();

           documentsMetadata.setFileName(fileUploads.getOriginalFilename());
           setFileName(fileUploads.getOriginalFilename());
            fileStream = IOUtils.toByteArray(fileUploads.getInputStream());
            File sFiles = new File(dirFiles, fileName);
            writeFile(fileStream, sFiles);
            String key = RandomUtil.generateConfirmationKey();
            documentsMetadata.setFileID(key);
            documentsMetadata.setFile_path(sFiles.toString());
        documentsMetadata.setFile_path(downloadFile + "/"+fileUploads.getOriginalFilename());
            documentsMetadata.setFileName(fileName);
            documentsMetadata.setDataProvider(udp);
           documentsMetadataRepository.save(documentsMetadata);

        //fileUploadRepository.save(fileUpload);
        return null;
    }


    @RequestMapping(value = "/ur/multiple-file-upload", method =RequestMethod.POST)
    @Timed
    public ResponseEntity<DocumentsMetadata> multiple(@RequestParam(value="providerId")String recordID,
                                                        @RequestParam(value="fileUploads")
                                                            MultipartFile[] fileUploads) throws URISyntaxException, IOException, MissingServletRequestParameterException {
        // log.debug("REST request to save FileUpload : {}", fileUploads);
        DataProvider udp = dataProviderRepository.findByRecordID(recordID);
        //DocumentsMetadata opp=documentsMetadataRepository.findOne(dataProvider);

        String  sFilesDirectory =  applicationProperties.getDocumentsMetadataFolder() + udp.getRecordID() + "-" + udp.getProviderName();
        String downloadFile=applicationProperties.getDownloadFile() + udp.getRecordID() + "-" + udp.getProviderName();
        File dirFiles = new File(sFilesDirectory);
        dirFiles.mkdirs();
       // documentsMetadata.setFileName(fileUploads.getOriginalFilename());
        for (MultipartFile sFile : fileUploads) {
            DocumentsMetadata documentsMetadata = new DocumentsMetadata();
            setFileName(sFile.getOriginalFilename());
            fileStream = IOUtils.toByteArray(sFile.getInputStream());
            File sFiles = new File(dirFiles, fileName);
            writeFile(fileStream, sFiles);
            String key = RandomUtil.generateConfirmationKey();
            documentsMetadata.setFileID(key);
            //documentsMetadata.setFileID("w234ewr5rwwer"+1);
            documentsMetadata.setFile_path(downloadFile + "/"+sFile.getOriginalFilename());
            documentsMetadata.setFileName(fileName);
            documentsMetadataRepository.save(documentsMetadata);
        }
        //fileUploadRepository.save(fileUpload);
        return null;
    }

    public void writeFile(byte[] fileStream, File file) throws IOException {
        InputStream in;
        in = new ByteArrayInputStream(fileStream);
        OutputStream out = new FileOutputStream(file);
        byte buf[] = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.flush();
        out.close();

    }

    @RequestMapping(value = "/provider-logo-upload", method =RequestMethod.POST)
    @Timed
    public ResponseEntity<ProviderLogo> LogoUpload(@RequestParam(value="providerId")String recordID,
                                                        @RequestParam(value="fileUploads")
                        
    MultipartFile fileUploads) throws URISyntaxException, IOException, MissingServletRequestParameterException {

    	//byte[] file = null;
        DataProvider udp = dataProviderRepository.findByRecordID(recordID);

        /* String  sFilesDirectory =  applicationProperties.getDocumentsMetadataFolder() + udp.getRecordID() + "-" + udp.getProviderName();

        File dirFiles = new File(sFilesDirectory);
        dirFiles.mkdirs();
        setFileName(fileUploads.getOriginalFilename());
        fileStream = IOUtils.toByteArray(fileUploads.getInputStream());
        File sFiles = new File(dirFiles, fileName);
        writeFile(fileStream, sFiles); */

        //DataProvider p = dataProviderRepository.findByRecordIDEqualsAndLogoIsNull(recordID);
        ProviderLogo logo = providerLogoRepository.findByProviderRecordID(recordID);
                if (udp != null) {
            try {
                byte[] image = fileUploads.getBytes();//Files.readAllBytes(Paths.get(sFiles.toURI()));
                if (image != null) {
                    if (logo == null) {
                        ProviderLogo pl = new ProviderLogo(image, udp.getRecordID());
                        providerLogoRepository.save(pl);
                    } else {
                        logo.setLogo(image);
                        providerLogoRepository.save(logo);
                    }

                    DataProvider fullProvider = dataProviderRepository.findOneWithEagerRelationships(udp.getId());
                    providerSearchRepository.save(fullProvider);
                   // log.info("Updated provider with logo id: {} website: {}", p.getId(), p.getWebsite());
                //file=image;
                logo.setLogo(image);
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        //fileUploadRepository.save(fileUpload);
        return new ResponseEntity<ProviderLogo>(logo, HttpStatus.OK);
    }
    
   @GetMapping("/files/{id}")
   @Timed
public ResponseEntity<DocumentsMetadata>getDocumentMetaData(@PathVariable Long id)
{

	   DocumentsMetadata documentsMetadata=documentsMetadataRepository.findOne(id);
	   
	return ResponseUtil.wrapOrNotFound(Optional.ofNullable(documentsMetadata));
	
}
    
   @DeleteMapping("/files/{id}")
   @Timed
   public ResponseEntity<Void> deleteDataLinks(@PathVariable Long id) {
	   documentsMetadataRepository.delete(id);
       return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
   }


    /*@RequestMapping(value = "/ur/resource-file-upload", method =RequestMethod.POST)
    @Timed
    public ResponseEntity<DocumentsMetadata> resource(@RequestParam(value="providerId")String recordID,@RequestParam(value="fileUploads")
        MultipartFile fileUploads) throws URISyntaxException, IOException, MissingServletRequestParameterException {
        // log.debug("REST request to save FileUpload : {}", fileUploads);
        // DataProvider udp = dataProviderRepository.findByRecordID(recordID);
        //DocumentsMetadata opp=documentsMetadataRepository.findOne(dataProvider);
        DataProvider udp = dataProviderRepository.findByRecordID(recordID);

        String  sFilesDirectory =  applicationProperties.getDocumentsMetadataFolder();

        File dirFiles = new File(sFilesDirectory);
        dirFiles.mkdirs();

        // documentsMetadata.setFileName(fileUploads.getOriginalFilename());
        DocumentsMetadata documentsMetadata=new DocumentsMetadata();
        documentsMetadata.setFileName(fileUploads.getOriginalFilename());
        setFileName(fileUploads.getOriginalFilename());
        fileStream = IOUtils.toByteArray(fileUploads.getInputStream());
        File sFiles = new File(dirFiles, fileName);
        writeFile(fileStream, sFiles);
        String key = RandomUtil.generateConfirmationKey();
        documentsMetadata.setFileID(key);
        documentsMetadata.setFile_path(sFiles.toString());
        documentsMetadata.setFileName(fileName);
        String ext1 = FilenameUtils.getExtension(sFiles.getAbsolutePath());
        documentsMetadata.setFileExtension(ext1);
        DocumentsMetadata document=documentsMetadataRepository.save(documentsMetadata);
        // User user = userService.getUser();
        //  System.out.println("$$$$"+user.getId());
        DataResources resources=new DataResources();
        resources.setRecordID("recr"+ Utils.randomString(13,13));
        // resources.setCreatorUserId(user.getId());
        resources.setName(document.getFileName());
        resources.setFile(document);
        Set<DataProvider> dataProviders = new HashSet<DataProvider>();
        dataProviders.add(udp);
        resources.setRelavantDataProviders(dataProviders);
        resources=dataResourceRepository.save(resources);
        if(resources.getId()!=null)
        {
            elasticsearchIndexService.reindexSingleResource(resources.getId());
        }



        return ResponseEntity.created(new URI("/ur/resource-file-upload" + document.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, document.getId().toString()))
            .body(document);
        //fileUploadRepository.save(fileUpload);
    }*/


    @RequestMapping(value = "/ur/resource-file-upload", method =RequestMethod.POST)
    @Timed
    public ResponseEntity<DocumentsMetadata> resource(@RequestParam(value="providerId")String recordID,@RequestParam(value="fileUploads")
        MultipartFile fileUploads) throws URISyntaxException, IOException, MissingServletRequestParameterException {
        // log.debug("REST request to save FileUpload : {}", fileUploads);
        // DataProvider udp = dataProviderRepository.findByRecordID(recordID);
        //DocumentsMetadata opp=documentsMetadataRepository.findOne(dataProvider);
        DataProvider udp = dataProviderRepository.findByRecordID(recordID);

        String  sFilesDirectory =  applicationProperties.getDocumentsMetadataFolder() + udp.getRecordID() + "-" + udp.getProviderName();
        String downloadFile=applicationProperties.getDownloadFile() + udp.getRecordID() + "-" + udp.getProviderName();
        File dirFiles = new File(sFilesDirectory);
        dirFiles.mkdirs();

        // documentsMetadata.setFileName(fileUploads.getOriginalFilename());
        DocumentsMetadata documentsMetadata=new DocumentsMetadata();
        documentsMetadata.setFileName(fileUploads.getOriginalFilename());
        //System.out.println(documentsMetadata.getFileName());
        setFileName(fileUploads.getOriginalFilename());
        fileStream = IOUtils.toByteArray(fileUploads.getInputStream());
        File sFiles = new File(dirFiles, fileName);
        writeFile(fileStream, sFiles);
        String key = RandomUtil.generateConfirmationKey();
        documentsMetadata.setFileID(key);
        documentsMetadata.setFile_path(downloadFile + "/"+fileUploads.getOriginalFilename());
        documentsMetadata.setFileName(fileName);
        String fileNames = documentsMetadata.getFileName().split("\\.")[0];
        documentsMetadata.setFileName(fileNames);
       // System.out.println(documentsMetadata.getFileName());
        String ext1 = FilenameUtils.getExtension(sFiles.getAbsolutePath());
        documentsMetadata.setFileExtension(ext1);
        documentsMetadata.setDataProvider(udp);
        documentsMetadata.setRecordID("recf"+ Utils.randomString(13,13));
        DocumentsMetadata document=documentsMetadataRepository.save(documentsMetadata);
        return ResponseEntity.created(new URI("/ur/resource-file-upload" + document.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, document.getId().toString()))
            .body(document);
        //fileUploadRepository.save(fileUpload);
    }

    @RequestMapping(value = "/ur/resources-multiple-file-upload", method =RequestMethod.POST)
    @Timed
    public ResponseEntity<MultipartFile[]> resources(@RequestParam(value="providerId")String recordID,
                                                            @RequestParam(value="fileUploads")
                                                          MultipartFile[] fileUploads) throws URISyntaxException, IOException, MissingServletRequestParameterException {
        // log.debug("REST request to save FileUpload : {}", fileUploads);
        DataProvider udp = dataProviderRepository.findByRecordID(recordID);
        //DocumentsMetadata opp=documentsMetadataRepository.findOne(dataProvider);

        String  sFilesDirectory =  applicationProperties.getDocumentsMetadataFolder() + udp.getRecordID() + "-" + udp.getProviderName();
        String downloadFile=applicationProperties.getDownloadFile() + udp.getRecordID() + "-" + udp.getProviderName();
        File dirFiles = new File(sFilesDirectory);
        dirFiles.mkdirs();
        // documentsMetadata.setFileName(fileUploads.getOriginalFilename());
        for (MultipartFile sFile : fileUploads) {
            DocumentsMetadata documentsMetadata = new DocumentsMetadata();
            setFileName(sFile.getOriginalFilename());
            fileStream = IOUtils.toByteArray(sFile.getInputStream());
            File sFiles = new File(dirFiles, fileName);
            writeFile(fileStream, sFiles);
            String key = RandomUtil.generateConfirmationKey();
            documentsMetadata.setRecordID("recf"+ Utils.randomString(13,13));
            documentsMetadata.setFileID(key);
            String ext1 = FilenameUtils.getExtension(sFiles.getAbsolutePath());
            documentsMetadata.setFileExtension(ext1);
            documentsMetadata.setFile_path(downloadFile + "/"+sFile.getOriginalFilename());
            documentsMetadata.setFileName(fileName);
            DocumentsMetadata doc=documentsMetadataRepository.save(documentsMetadata);

           // User user = userService.getUser();
            DataResources resources=new DataResources();
            resources.setRecordID("recr"+ Utils.randomString(13,13));
           // resources.setCreatorUserId(user.getId());
            resources.setName(doc.getFileName());
            resources.setFile(doc);
            Set<DataProvider> dataProviders = new HashSet<DataProvider>();
            dataProviders.add(udp);
            resources.setRelavantDataProviders(dataProviders);
            resources=dataResourceRepository.save(resources);
            if(resources.getId()!=null)
            {
                elasticsearchIndexService.reindexSingleResource(resources.getId());
            }
        }
        //fileUploadRepository.save(fileUpload);
        return null;
    }
    @RequestMapping(value = "/ur/user-resource-file-upload", method =RequestMethod.POST)
    @Timed
    public ResponseEntity<DocumentsMetadata> Userresource(@RequestParam(value="userId")String userID,@RequestParam(value="fileUploads")
        MultipartFile fileUploads) throws URISyntaxException, IOException, MissingServletRequestParameterException {
        // log.debug("REST request to save FileUpload : {}", fileUploads);
        // DataProvider udp = dataProviderRepository.findByRecordID(recordID);
        //DocumentsMetadata opp=documentsMetadataRepository.findOne(dataProvider);
        //DataProvider udp = dataProviderRepository.findByRecordID(recordID);
        String  sFilesDirectory =  applicationProperties.getDocumentsMetadataUserFolder() + userID;
        String downloadFile=applicationProperties.getDownloadUserFile() + userID;
        File dirFiles = new File(sFilesDirectory);
        dirFiles.mkdirs();

        // documentsMetadata.setFileName(fileUploads.getOriginalFilename());
        DocumentsMetadata documentsMetadata=new DocumentsMetadata();
        documentsMetadata.setFileName(fileUploads.getOriginalFilename());
        //System.out.println(documentsMetadata.getFileName());
        setFileName(fileUploads.getOriginalFilename());
        fileStream = IOUtils.toByteArray(fileUploads.getInputStream());
        File sFiles = new File(dirFiles, fileName);
        writeFile(fileStream, sFiles);
        String key = RandomUtil.generateConfirmationKey();
        documentsMetadata.setFileID(key);
        documentsMetadata.setFile_path(downloadFile + "/"+fileUploads.getOriginalFilename());
        documentsMetadata.setFileName(fileName);
        String fileNames = documentsMetadata.getFileName().split("\\.")[0];
        documentsMetadata.setFileName(fileNames);
       // System.out.println(documentsMetadata.getFileName());
        String ext1 = FilenameUtils.getExtension(sFiles.getAbsolutePath());
        documentsMetadata.setFileExtension(ext1);
        //documentsMetadata.setDataProvider(udp);
        documentsMetadata.setRecordID("recf"+ Utils.randomString(13,13));
        DocumentsMetadata document=documentsMetadataRepository.save(documentsMetadata);
        return ResponseEntity.created(new URI("/ur/resource-file-upload" + document.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, document.getId().toString()))
            .body(document);
        //fileUploadRepository.save(fileUpload);
    }
}
