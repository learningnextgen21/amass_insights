package com.ai.mp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ai.mp.domain.UserNotification;

import com.ai.mp.repository.UserNotificationRepository;
import com.ai.mp.repository.search.UserNotificationSearchRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AmassUserNotification.
 */
@RestController
@RequestMapping("/api")
public class UserNotificationResource {

    private final Logger log = LoggerFactory.getLogger(UserNotificationResource.class);

    private static final String ENTITY_NAME = "amassUserNotification";

    private final UserNotificationRepository amassUserNotificationRepository;

    private final UserNotificationSearchRepository amassUserNotificationSearchRepository;
    public UserNotificationResource(UserNotificationRepository amassUserNotificationRepository, UserNotificationSearchRepository amassUserNotificationSearchRepository) {
        this.amassUserNotificationRepository = amassUserNotificationRepository;
        this.amassUserNotificationSearchRepository = amassUserNotificationSearchRepository;
    }

    /**
     * POST  /amass-user-notifications : Create a new amassUserNotification.
     *
     * @param amassUserNotification the amassUserNotification to create
     * @return the ResponseEntity with status 201 (Created) and with body the new amassUserNotification, or with status 400 (Bad Request) if the amassUserNotification has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/amass-user-notifications")
    @Timed
    public ResponseEntity<UserNotification> createAmassUserNotification(@Valid @RequestBody UserNotification amassUserNotification) throws URISyntaxException {
        log.debug("REST request to save AmassUserNotification : {}", amassUserNotification);
        if (amassUserNotification.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new amassUserNotification cannot already have an ID")).body(null);
        }
        UserNotification result = amassUserNotificationRepository.save(amassUserNotification);
        amassUserNotificationSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/amass-user-notifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /amass-user-notifications : Updates an existing amassUserNotification.
     *
     * @param amassUserNotification the amassUserNotification to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated amassUserNotification,
     * or with status 400 (Bad Request) if the amassUserNotification is not valid,
     * or with status 500 (Internal Server Error) if the amassUserNotification couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/amass-user-notifications")
    @Timed
    public ResponseEntity<UserNotification> updateAmassUserNotification(@Valid @RequestBody UserNotification amassUserNotification) throws URISyntaxException {
        log.debug("REST request to update AmassUserNotification : {}", amassUserNotification);
        if (amassUserNotification.getId() == null) {
            return createAmassUserNotification(amassUserNotification);
        }
        UserNotification result = amassUserNotificationRepository.save(amassUserNotification);
        amassUserNotificationSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, amassUserNotification.getId().toString()))
            .body(result);
    }

    /**
     * GET  /amass-user-notifications : get all the amassUserNotifications.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of amassUserNotifications in body
     */
    @GetMapping("/amass-user-notifications")
    @Timed
    public ResponseEntity<List<UserNotification>> getAllAmassUserNotifications(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of AmassUserNotifications");
        Page<UserNotification> page = amassUserNotificationRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/amass-user-notifications");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /amass-user-notifications/:id : get the "id" amassUserNotification.
     *
     * @param id the id of the amassUserNotification to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the amassUserNotification, or with status 404 (Not Found)
     */
    @GetMapping("/amass-user-notifications/{id}")
    @Timed
    public ResponseEntity<UserNotification> getAmassUserNotification(@PathVariable Long id) {
        log.debug("REST request to get AmassUserNotification : {}", id);
        UserNotification amassUserNotification = amassUserNotificationRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(amassUserNotification));
    }

    /**
     * DELETE  /amass-user-notifications/:id : delete the "id" amassUserNotification.
     *
     * @param id the id of the amassUserNotification to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/amass-user-notifications/{id}")
    @Timed
    public ResponseEntity<Void> deleteAmassUserNotification(@PathVariable Long id) {
        log.debug("REST request to delete AmassUserNotification : {}", id);
        amassUserNotificationRepository.delete(id);
        amassUserNotificationSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/amass-user-notifications?query=:query : search for the amassUserNotification corresponding
     * to the query.
     *
     * @param query the query of the amassUserNotification search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/amass-user-notifications")
    @Timed
    public ResponseEntity<List<UserNotification>> searchAmassUserNotifications(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AmassUserNotifications for query {}", query);
        Page<UserNotification> page = amassUserNotificationSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/amass-user-notifications");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
