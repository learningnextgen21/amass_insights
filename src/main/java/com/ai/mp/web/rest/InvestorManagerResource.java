package com.ai.mp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ai.mp.domain.DataInvestmentManager;
import com.ai.mp.repository.DataInvestmentMangersRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class InvestorManagerResource {
    private static final String ENTITY_NAME = "investmentManager";

    private final DataInvestmentMangersRepository dataInvestmentMangersRepository;
    InvestorManagerResource(DataInvestmentMangersRepository dataInvestmentMangersRepository)
    {
    	this.dataInvestmentMangersRepository=dataInvestmentMangersRepository;
    }
	
	@PostMapping("/investor-manager")
    @Timed
    public ResponseEntity<DataInvestmentManager> createInvestmentManger(@Valid @RequestBody DataInvestmentManager investment) throws URISyntaxException {
        if (investment.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new lookupCode cannot already have an ID")).body(null);
        }
        DataInvestmentManager result = dataInvestmentMangersRepository.save(investment);
        return ResponseEntity.created(new URI("/api/investor-manager/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
	 @PutMapping("/investor-manager")
	    @Timed
	    public ResponseEntity<DataInvestmentManager> updateInvestManagers(@Valid @RequestBody DataInvestmentManager investment) throws URISyntaxException {
	        if (investment.getId() == null) {
	            return createInvestmentManger(investment);
	        }
	        DataInvestmentManager result = dataInvestmentMangersRepository.save(investment);
	        return ResponseEntity.ok()
	            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, investment.getId().toString()))
	            .body(result);
	    }
	
	    @GetMapping("/investor-manager/{id}")
	    @Timed
	    public ResponseEntity<DataInvestmentManager> getInvestmentMangers(@PathVariable Long id) {
		 DataInvestmentManager investor = dataInvestmentMangersRepository.findOne(id);
	        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(investor));
	    }

	    @DeleteMapping("/investor-manager/{id}")
	    @Timed
	    public ResponseEntity<Void> deleteInvestmentMangers(@PathVariable Long id) {
	    	dataInvestmentMangersRepository.delete(id);
	        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	    }
	
	
}
