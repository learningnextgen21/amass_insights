package com.ai.mp.web.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ai.mp.domain.Search;
import com.ai.mp.domain.User;
import com.ai.mp.repository.SearchRepository;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.security.SecurityUtils;
import com.codahale.metrics.annotation.Timed;

@RestController
@RequestMapping("/api")
public class SearchResource {

	private final SearchRepository searchRepository;
	
	private final UserRepository userRepository;
	
	SearchResource(SearchRepository searchRepository,UserRepository userRepository)
	{
		this.searchRepository=searchRepository;
		this.userRepository=userRepository;
	}
	
	@PostMapping("/search")
	@Timed
	public ResponseEntity<?> createSearch(@RequestBody Search search)
	{    	
		String user=SecurityUtils.getCurrentUserLogin();
    User users=userRepository.findByLogin(user);
    search.setUser(users);
    searchRepository.save(search);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping("/search/{id}")
	public ResponseEntity<Search>getSearch(@PathVariable Long id)
	{
		Search search=searchRepository.findOne(id);
		
		return new ResponseEntity<Search>(search, HttpStatus.OK);
	}
	
	@PutMapping("/search")
	public ResponseEntity<?>updateSearch(@RequestBody Search search)
	{
		if(null==search.getId())
		{
			createSearch(search);
		}
		else
		{
			String user=SecurityUtils.getCurrentUserLogin();
		    User users=userRepository.findByLogin(user);
		    search.setUser(users);
		   searchRepository.save(search);
		}
		return new ResponseEntity<Search>(HttpStatus.OK);
	}
	
	@DeleteMapping("/search")
	public ResponseEntity<?>deleteSearch(@PathVariable Long id)
     {
		if(null!=id)
		{
			searchRepository.delete(id);
		}
		
		return new ResponseEntity<Search>(HttpStatus.OK);
	
      }
}