package com.ai.mp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ai.mp.domain.Domains;
import com.ai.mp.domain.Emails;
import com.ai.mp.repository.DataDomainsRepository;
import com.ai.mp.repository.EmailsRepository;
import com.ai.mp.repository.search.DataDomainSearchRepository;
import com.ai.mp.repository.search.EmailsSearchRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class DataDomainsResource {

    private static final String ENTITY_NAME = "domains";
    
    private final DataDomainsRepository dataDomainsRepository;
    private final DataDomainSearchRepository dataDomainSearchRepository;
    
    DataDomainsResource(DataDomainsRepository dataDomainsRepository,DataDomainSearchRepository dataDomainSearchRepository)
    {
    	this.dataDomainsRepository=dataDomainsRepository;
    	this.dataDomainSearchRepository=dataDomainSearchRepository;		
    }
    
    @PostMapping("/domains")
    @Timed
    public ResponseEntity<Domains> createDomains(@Valid @RequestBody Domains domains) throws URISyntaxException {
        if (domains.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new lookupCode cannot already have an ID")).body(null);
        }
        Domains result = dataDomainsRepository.save(domains);
        return ResponseEntity.created(new URI("/api/domains/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    
    @PutMapping("/domains")
    @Timed
    public ResponseEntity<Domains> updateDomains(@Valid @RequestBody Domains domains) throws URISyntaxException {
        if (domains.getId() == null) {
            return createDomains(domains);
        }
        Domains result = dataDomainsRepository.save(domains);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    
    @GetMapping("/domains/{id}")
    @Timed
    public ResponseEntity<Domains> getDomains(@PathVariable Long id) {
	 Domains domains = dataDomainsRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(domains));
    }

    @DeleteMapping("/domains/{id}")
    @Timed
    public ResponseEntity<Void> deleteDomains(@PathVariable Long id) {
    	dataDomainsRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/domains")
    @Timed
    public ResponseEntity<List<Domains>> getAllDataDomains(@ApiParam Pageable pageable) {
        Page<Domains> page = searchAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/domains");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    
    @Transactional(readOnly = true)
    public Page<Domains> searchAll(Pageable pageable) {
        Page<Domains> result = dataDomainSearchRepository.findAll(pageable);
        return result;
    }

}
