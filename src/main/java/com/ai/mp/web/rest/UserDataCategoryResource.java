package com.ai.mp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ai.core.StrUtils;
import com.ai.mp.domain.CategoryLogo;
import com.ai.mp.domain.UserDataCategory;

import com.ai.mp.repository.UserDataCategoryRepository;
import com.ai.mp.repository.search.UserDataCategorySearchRepository;
import com.ai.mp.service.UserDataCategoryService;
import com.ai.mp.batch.DownloadLogo;
import com.ai.mp.utils.GenericHttpResponse;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AmassUserDataCategory.
 */
@RestController
@RequestMapping("/api")
public class UserDataCategoryResource {

    private final Logger log = LoggerFactory.getLogger(UserDataCategoryResource.class);

    private static final String ENTITY_NAME = "amassUserDataCategory";

    private final UserDataCategoryRepository amassUserDataCategoryRepository;
    private final UserDataCategoryService userDataCategoryService;
    private final UserDataCategorySearchRepository amassUserDataCategorySearchRepository;
    private final DownloadLogo downloadLogo;

    public UserDataCategoryResource(UserDataCategoryRepository amassUserDataCategoryRepository, UserDataCategorySearchRepository amassUserDataCategorySearchRepository, UserDataCategoryService userDataCategoryService,DownloadLogo downloadLogo) {
        this.amassUserDataCategoryRepository = amassUserDataCategoryRepository;
        this.amassUserDataCategorySearchRepository = amassUserDataCategorySearchRepository;
        this.userDataCategoryService = userDataCategoryService;
        this.downloadLogo = downloadLogo;

    }


    /**
     * POST  /amass-user-data-categories : Create a new amassUserDataCategory.
     *
     * @param amassUserDataCategory the amassUserDataCategory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new amassUserDataCategory, or with status 400 (Bad Request) if the amassUserDataCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/amass-user-data-categories")
    @Timed
    public ResponseEntity<UserDataCategory> createAmassUserDataCategory(@Valid @RequestBody UserDataCategory amassUserDataCategory) throws URISyntaxException {
        log.debug("REST request to save AmassUserDataCategory : {}", amassUserDataCategory);
        if (amassUserDataCategory.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new amassUserDataCategory cannot already have an ID")).body(null);
        }
        UserDataCategory result = amassUserDataCategoryRepository.save(amassUserDataCategory);
        amassUserDataCategorySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/amass-user-data-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /amass-user-data-categories : Updates an existing amassUserDataCategory.
     *
     * @param amassUserDataCategory the amassUserDataCategory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated amassUserDataCategory,
     * or with status 400 (Bad Request) if the amassUserDataCategory is not valid,
     * or with status 500 (Internal Server Error) if the amassUserDataCategory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/amass-user-data-categories")
    @Timed
    public ResponseEntity<UserDataCategory> updateAmassUserDataCategory(@Valid @RequestBody UserDataCategory amassUserDataCategory) throws URISyntaxException {
        log.debug("REST request to update AmassUserDataCategory : {}", amassUserDataCategory);
        if (amassUserDataCategory.getId() == null) {
            return createAmassUserDataCategory(amassUserDataCategory);
        }
        UserDataCategory result = amassUserDataCategoryRepository.save(amassUserDataCategory);
        amassUserDataCategorySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, amassUserDataCategory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /amass-user-data-categories : get all the amassUserDataCategories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of amassUserDataCategories in body
     */
    @GetMapping("/amass-user-data-categories")
    @Timed
    public ResponseEntity<List<UserDataCategory>> getAllAmassUserDataCategories(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of AmassUserDataCategories");
        Page<UserDataCategory> page = amassUserDataCategoryRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/amass-user-data-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /amass-user-data-categories/:id : get the "id" amassUserDataCategory.
     *
     * @param id the id of the amassUserDataCategory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the amassUserDataCategory, or with status 404 (Not Found)
     */
    @GetMapping("/amass-user-data-categories/{id}")
    @Timed
    public ResponseEntity<UserDataCategory> getAmassUserDataCategory(@PathVariable Long id) {
        log.debug("REST request to get AmassUserDataCategory : {}", id);
        UserDataCategory amassUserDataCategory = amassUserDataCategoryRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(amassUserDataCategory));
    }

    /**
     * DELETE  /amass-user-data-categories/:id : delete the "id" amassUserDataCategory.
     *
     * @param id the id of the amassUserDataCategory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/amass-user-data-categories/{id}")
    @Timed
    public ResponseEntity<Void> deleteAmassUserDataCategory(@PathVariable Long id) {
        log.debug("REST request to delete AmassUserDataCategory : {}", id);
        amassUserDataCategoryRepository.delete(id);
        amassUserDataCategorySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/amass-user-data-categories?query=:query : search for the amassUserDataCategory corresponding
     * to the query.
     *
     * @param query the query of the amassUserDataCategory search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/amass-user-data-categories")
    @Timed
    public ResponseEntity<List<UserDataCategory>> searchAmassUserDataCategories(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AmassUserDataCategories for query {}", query);
        Page<UserDataCategory> page = amassUserDataCategorySearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/amass-user-data-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping ("/follow-category")
    @Timed
    public ResponseEntity<GenericHttpResponse> updateFollowedCategory(@RequestParam long  dataCategoryID) {
        if(dataCategoryID <= 0)

        {

            return ResponseEntity.badRequest().body(new GenericHttpResponse("Follow Category", "Missing Category ID", "failed"));
        }
        userDataCategoryService.followCategory(dataCategoryID);
        return ResponseEntity.ok().body(new GenericHttpResponse("Follow Category", "Request processed successfully", "success"));
    }

    @GetMapping ("/ur/follow-category")
    @Timed
    public ResponseEntity<GenericHttpResponse> updateFollowedCategory(@RequestParam(value = "key") String key) {
        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Unfollow Category", "Missing Unfollow key", "failed"));
        }
        userDataCategoryService.followCategory(key, false);
        return ResponseEntity.ok().body(new GenericHttpResponse("Unfollow Category", "Request processed successfully", "success"));
    }

    @GetMapping ("/ur/unfollow-category")
    @Timed
    public ResponseEntity<GenericHttpResponse> unfollowCategory(@RequestParam(value = "key") String key) {
        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Unfollow Category", "Missing Unfollow key", "failed"));
        }
        userDataCategoryService.followCategory(key, true);
        return ResponseEntity.ok().body(new GenericHttpResponse("Unfollow Category", "Request processed successfully", "success"));
    }

    @GetMapping("/ur/category-logo")
    @Timed
    public void getCategoryLogo()
    {


        downloadLogo.DatacategoryFileLogo();

    }
}
