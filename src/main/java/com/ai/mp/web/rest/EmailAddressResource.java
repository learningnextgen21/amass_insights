package com.ai.mp.web.rest;

import com.ai.mp.domain.DataArticle;
import com.ai.mp.domain.EmailAddress;
import com.ai.mp.repository.EmailAddressRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.ai.util.Utils;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.URI;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api")
public class EmailAddressResource {
    private final Logger log = LoggerFactory.getLogger(EmailAddressResource.class);

    private static final String ENTITY_NAME = "emailAddress";
    private final EmailAddressRepository emailAddressRepository;

    public EmailAddressResource(EmailAddressRepository emailAddressRepository) {
        this.emailAddressRepository = emailAddressRepository;
    }

    @PostMapping("/email-address")
    @Timed
    public ResponseEntity<EmailAddress> createEmailAddress(@Valid @RequestBody EmailAddress emailAddress) throws URISyntaxException {
        log.debug("REST request to save DataFeature : {}", emailAddress);
        if (emailAddress.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new emailAddress cannot already have an ID")).body(null);
        }
        EmailAddress emailID=emailAddressRepository.findByEmailAddress(emailAddress.getEmailAddress());
        if (emailID!=null)
        {
            return new ResponseEntity("email address already in use", HttpStatus.BAD_REQUEST);
        }
            emailAddress.setRecordID("rece"+ Utils.randomString(13,13));
            emailAddress.setEmailAddress(emailAddress.getEmailAddress());
            EmailAddress result = emailAddressRepository.save(emailAddress);

       // dataFeatureSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/email-address/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @GetMapping("/email-address")
    @Timed
    public ResponseEntity<List<EmailAddress>> getAllEmailAddress(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DataFeatures");
        Page<EmailAddress> page = emailAddressRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/email-address");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @DeleteMapping("/email-address/{id}")
    @Timed
    public ResponseEntity<Void> deleteEmailAddress(@PathVariable Long id) {
        log.debug("REST request to delete DataFeature : {}", id);
        emailAddressRepository.delete(id);
       // dataFeatureSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @PutMapping("/email-address")
    @Timed
    public ResponseEntity<EmailAddress> updateEmailAddress(@Valid @RequestBody EmailAddress emailAddress) throws URISyntaxException {
        log.debug("REST request to update DataFeature : {}", emailAddress);
        if (emailAddress.getId() == null) {
            return createEmailAddress(emailAddress);
        }
        EmailAddress result = emailAddressRepository.save(emailAddress);
      //  dataFeatureSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, emailAddress.getId().toString()))
            .body(result);
   }

    @GetMapping("/email-address/{id}")
    @Timed
    public ResponseEntity<EmailAddress> getEmailAddress(@PathVariable Long id) {
        log.debug("REST request to get EmailAddress : {}", id);
        EmailAddress emailAddress = emailAddressRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(emailAddress));
    }


}
