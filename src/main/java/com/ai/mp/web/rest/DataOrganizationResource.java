package com.ai.mp.web.rest;

import com.ai.mp.domain.DataOrganization;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.User;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.service.DataOrganizationService;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.service.UserService;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.ai.util.Utils;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing DataOrganization.
 */
@RestController
@RequestMapping("/api")
public class DataOrganizationResource {

    private final Logger log = LoggerFactory.getLogger(DataOrganizationResource.class);

    private static final String ENTITY_NAME = "dataOrganization";

    private final DataOrganizationService dataOrganizationService;
private final UserService userService;
private final DataProviderRepository dataProviderRepository;    
private final ElasticsearchIndexService elasticsearchIndexService;

    public DataOrganizationResource(DataOrganizationService dataOrganizationService,UserService userService, DataProviderRepository dataProviderRepository,ElasticsearchIndexService elasticsearchIndexService) {
        this.dataOrganizationService = dataOrganizationService;
        this.userService = userService;
       this.dataProviderRepository=dataProviderRepository;
       this.elasticsearchIndexService=elasticsearchIndexService;
    }

    
    
    /**
     * POST  /data-organizations : Create a new dataOrganization.
     *
     * @param dataOrganization the dataOrganization to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dataOrganization, or with status 400 (Bad Request) if the dataOrganization has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @PostMapping("/data-organizations")
    @Timed
    public ResponseEntity<DataOrganization> createDataOrganization(@Valid @RequestBody DataOrganization dataOrganization) throws URISyntaxException {
        log.debug("REST request to save DataOrganization : {}", dataOrganization);
        if (dataOrganization.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dataOrganization cannot already have an ID")).body(null);
        }
        User user = userService.getUser();
        dataOrganization.setCreatorUserId(user.getId());
        dataOrganization.setEditorUserId(user.getId());
        LocalDate localDate = LocalDate.now();
        dataOrganization.setCreatedDate(localDate);
        dataOrganization.setUpdateddate(localDate);
        dataOrganization.setRecordID("recO"+ Utils.randomString(13,13));
        DataOrganization result = dataOrganizationService.save(dataOrganization);
        return ResponseEntity.created(new URI("/api/data-organizations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /data-organizations : Updates an existing dataOrganization.
     *
     * @param dataOrganization the dataOrganization to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dataOrganization,
     * or with status 400 (Bad Request) if the dataOrganization is not valid,
     * or with status 500 (Internal Server Error) if the dataOrganization couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */

    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @PutMapping("/data-organizations")
    @Timed
    public ResponseEntity<DataOrganization> updateDataOrganization(@Valid @RequestBody DataOrganization dataOrganization) throws URISyntaxException {
        log.debug("REST request to update DataOrganization : {}", dataOrganization);
        if (dataOrganization.getId() == null) {
            return createDataOrganization(dataOrganization);
        }
        User user = userService.getUser();
        dataOrganization.setEditorUserId(user.getId());
        if(dataOrganization.getId()!=null)
        {
        	   DataOrganization data = dataOrganizationService.findOne(dataOrganization.getId());
        	   dataOrganization.setWebCreatedDate(data.getWebCreatedDate());
        	   dataOrganization.setCreatorUserId(data.getCreatorUserId());
        	   LocalDate localDate = LocalDate.now();
        	   dataOrganization.setCreatedDate(data.getCreatedDate());
        	   dataOrganization.setUpdateddate(localDate);
        }
        DataOrganization result = dataOrganizationService.save(dataOrganization);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataOrganization.getId().toString()))
            .body(result);
    }



    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @PutMapping("/data-organizations-providermapping")
    @Timed
    public ResponseEntity<DataOrganization> updateDataOrganizations(@Valid @RequestBody DataOrganization dataOrganization,@RequestParam(name = "ProviderId") Long ProviderId) throws URISyntaxException {
        log.debug("REST request to update DataOrganization : {}", dataOrganization);
        if (dataOrganization.getId() == null) {
            return createDataOrganization(dataOrganization);
        }
        User user = userService.getUser();
        dataOrganization.setEditorUserId(user.getId());
        if(dataOrganization.getId()!=null)
        {
            DataOrganization data = dataOrganizationService.findOne(dataOrganization.getId());
            dataOrganization.setWebCreatedDate(data.getWebCreatedDate());
            dataOrganization.setCreatorUserId(data.getCreatorUserId());
            LocalDate localDate = LocalDate.now();
            dataOrganization.setCreatedDate(data.getCreatedDate());
            dataOrganization.setUpdateddate(localDate);
        }
        DataOrganization result = dataOrganizationService.save(dataOrganization);
        if(null!=ProviderId)
        {
            DataProvider data=dataProviderRepository.findOne(ProviderId);
            data.setOwnerOrganization(result);
            DataProvider provider=dataProviderRepository.save(data);
            elasticsearchIndexService.reindexSingleProvider(provider.getId());
        }

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataOrganization.getId().toString()))
            .body(result);
    }



    /**
     * GET  /data-organizations : get all the dataOrganizations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dataOrganizations in body
     */
    @GetMapping("/data-organizations")
    @Timed
    public ResponseEntity<List<DataOrganization>> getAllDataOrganizations(@ApiParam Pageable pageable) {
        Page<DataOrganization> page = dataOrganizationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/data-organizations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /data-organizations/:id : get the "id" dataOrganization.
     *
     * @param id the id of the dataOrganization to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dataOrganization, or with status 404 (Not Found)
     */
    @GetMapping("/data-organizations/{id}")
    @Timed
    public ResponseEntity<DataOrganization> getDataOrganization(@PathVariable Long id) {
        log.debug("REST request to get DataOrganization : {}", id);
        DataOrganization dataOrganization = dataOrganizationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataOrganization));
    }

    /**
     * DELETE  /data-organizations/:id : delete the "id" dataOrganization.
     *
     * @param id the id of the dataOrganization to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/data-organizations/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataOrganization(@PathVariable Long id) {
        log.debug("REST request to delete DataOrganization : {}", id);
        dataOrganizationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/data-organizations?query=:query : search for the dataOrganization corresponding
     * to the query.
     *
     * @param query the query of the dataOrganization search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/data-organizations")
    @Timed
    public ResponseEntity<List<DataOrganization>> searchDataOrganizations(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataOrganizations for query {}", query);
        Page<DataOrganization> page = dataOrganizationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/data-organizations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/organization-cities")
    @Timed
    public Set<String> getDistinctCities()
    {
        return dataOrganizationService.getDistinctCities();
    }

}
