package com.ai.mp.web.rest;

import com.ai.mp.batch.NotificationUtils;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.EmailAddress;
import com.ai.mp.security.AuthoritiesConstants;
import com.ai.mp.service.DataProviderService;
import com.ai.mp.service.ElasticsearchGenericService;
import com.codahale.metrics.annotation.Timed;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.lucene.search.Sort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import static com.ai.mp.config.ElasticsearchConfiguration.*;

/**
 * REST controller for managing Elasticsearch index.
 */
@RestController
@RequestMapping("/api")
public class ElasticsearchGenericResource {
    private  DataProviderService dataProviderService;


    private final ElasticsearchGenericService service;

    private final NotificationUtils notificationUtils;

    private final HttpServletRequest request;
    
    public ElasticsearchGenericResource(ElasticsearchGenericService elasticsearchGenericService,NotificationUtils notificationUtils,HttpServletRequest request) {
        this.service = elasticsearchGenericService;
        this.notificationUtils=notificationUtils;
        this.request = request;
    }

    private final Logger log = LoggerFactory.getLogger(ElasticsearchGenericResource.class);

    @RequestMapping(value = "/es/qsearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
    public String querySearch(@RequestBody String query, String type) throws URISyntaxException {
        JsonObject jsonObject = service.search(query, type);
        return jsonObject.toString();
    }

    @RequestMapping(value = "/es/categorysearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
    public String categoriesSearch(@RequestBody String query, String type) throws URISyntaxException {
        JsonObject jsonObject = service.categoriesSearch(query, type);
        return jsonObject.toString();
    }

    @RequestMapping(value = "/es/providersearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
    public String providerSearch(@RequestBody String query, @RequestParam(value = "followedProviders", required = false) boolean followedProviders, @RequestParam(value = "unlockedProviders", required = false) boolean unlockedProviders, @RequestParam(value = "detail", required = false) boolean detail) throws URISyntaxException {
      // System.out.println("query"+query);
    	JsonObject jsonObject = service.search(query, INDEX_DATA_PROVIDER, followedProviders, unlockedProviders, detail);
        return jsonObject.toString();
    }
    @RequestMapping(value = "/es/userSearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
    public String userSearch(@RequestBody String query, @RequestParam(value = "followedProviders", required = false) boolean followedProviders, @RequestParam(value = "unlockedProviders", required = false) boolean unlockedProviders, @RequestParam(value = "detail", required = false) boolean detail) throws URISyntaxException {
      // System.out.println("query"+query);
    	JsonObject jsonObject = service.search(query, INDEX_DATA_USERS, followedProviders, unlockedProviders, detail);
        return jsonObject.toString();
    }
    
    @RequestMapping(value = "/es/emailSearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
    public String emailSearch(@RequestBody String query, @RequestParam(value = "followedProviders", required = false) boolean followedProviders, @RequestParam(value = "unlockedProviders", required = false) boolean unlockedProviders, @RequestParam(value = "detail", required = false) boolean detail) throws URISyntaxException {
      // System.out.println("query"+query);
    	JsonObject jsonObject = service.search(query, INDEX_EMAIL_ADDRESS, followedProviders, unlockedProviders, detail);
        return jsonObject.toString();
    }
    
    @RequestMapping(value = "/es/investorsearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
    public String investorSearch(@RequestBody String query, @RequestParam(value = "followedProviders", required = false) boolean followedProviders, @RequestParam(value = "unlockedProviders", required = false) boolean unlockedProviders, @RequestParam(value = "detail", required = false) boolean detail) throws URISyntaxException {
      // System.out.println("query"+query);
    	JsonObject jsonObject = service.search(query, INDEX_DATA_INVESTMENT_MANAGERS, followedProviders, unlockedProviders, detail);
        return jsonObject.toString();
    }
    
    @RequestMapping(value = "/ur/es/providersearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public String unRestrictedProviderSearch(@RequestBody String query, @RequestParam(value = "followedProviders", required = false) boolean followedProviders, @RequestParam(value = "unlockedProviders", required = false) boolean unlockedProviders, @RequestParam(value = "detail", required = false) boolean detail) throws URISyntaxException {
      // System.out.println("query"+query);
    	JsonObject jsonObject = service.searchUnrestrictedProviders(query);
        return jsonObject.toString();
    }
    @RequestMapping(value = "/es/datacategorysearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
    public String categorySearch(@RequestBody String query, @RequestParam(value = "followedDatacategory", required = false) boolean followedDatacategory, @RequestParam(value = "unlockedDataCategory", required = false) boolean unlockedDatacategory) throws URISyntaxException {
        JsonObject jsonObject = service.searchcategory(query,followedDatacategory,unlockedDatacategory);
        return jsonObject.toString();
    }


    @RequestMapping(value = "/es/maincategory", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
    public String getMainCategoryList(@RequestBody String query) throws URISyntaxException {
        JsonObject jsonObject = service.getCategoryFilterForCurrentUser(query);
        return jsonObject.toString();
    }
    @RequestMapping(value = "/es/dataProvidersExt", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public ResponseEntity<String>getDataProvidersExt(@RequestHeader(value = "size", required = false) Integer size, @RequestHeader(value = "page", required = false) Integer page, @RequestBody(required=false)String sort) throws Exception {

        JsonObject jsonObject = service.getDataProvidersExt(INDEX_DATA_PROVIDER, size, page,sort);
        // System.out.println(jsonObject.get("error"));
        JsonElement error=jsonObject.get("error");
        JsonElement errormsg=jsonObject.get("errorMessage");
        // System.out.println(jsonObject.get("errormsg"));
        try {
            if (error.getAsString().equals("403")) {
                return new ResponseEntity<String>(String.valueOf(jsonObject.getAsJsonObject()),HttpStatus.FORBIDDEN);
            }
        }
        catch (NullPointerException e) {
            return new ResponseEntity<String>(String.valueOf(jsonObject.getAsJsonObject()),HttpStatus.OK);
        }

        return new ResponseEntity<String>(String.valueOf(jsonObject.getAsJsonObject()),HttpStatus.OK);
    }

    @RequestMapping(value = "/es/dataProviderExt", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public String getDataProviderExt(@RequestHeader(value = "recordID", required = true) String recordID) throws URISyntaxException {
        JsonObject jsonObject = service.getDataProviderExt(INDEX_DATA_PROVIDER, recordID);
        return jsonObject.toString();
    }

    @RequestMapping(value = "/es/eventsearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
    public String eventsearch(@RequestBody String query) throws URISyntaxException {
        JsonObject jsonObject = service.eventSearch(query, INDEX_DATA_EVENT);
        return jsonObject.toString();
    }


    @RequestMapping(value = "/es/organizationsearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
    public String organizationsearch(@RequestBody String query) throws URISyntaxException {
        JsonObject jsonObject = service.organizationsearch(query, INDEX_DATA_ORGANIZATION);
        return jsonObject.toString();
    }

    @RequestMapping(value = "/es/matchessearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
    public String matchessearch(@RequestParam(value="key", required=false) String key,@RequestParam(value="order", required=false) String order,@RequestParam(value="itemsFrom", required=false) String itemsFrom) throws URISyntaxException {
    	/*System.out.println("key"+key);
    	System.out.println("order"+order);*/
    	request.setAttribute("key", key);
    	request.setAttribute("order", order);
    	request.setAttribute("itemsFrom", itemsFrom);
    	String query=service.matchSearch();
    	boolean myMatches = true;
    	request.setAttribute("myMatches", myMatches);
    	//System.out.println("111"+query);
        JsonObject jsonObject = service.search(query, INDEX_DATA_PROVIDER, false, false, false);
		return jsonObject.toString();
    	
    
    }
    @RequestMapping(value = "/es/providerTagsearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
    public String providerTagSearch(@RequestBody String query, @RequestParam(value = "followedProviders", required = false) boolean followedProviders, @RequestParam(value = "unlockedProviders", required = false) boolean unlockedProviders, @RequestParam(value = "detail", required = false) boolean detail) throws URISyntaxException {
      // System.out.println("query"+query);
    	JsonObject jsonObject = service.providerTagsearch(query, INDEX_DATA_TAG, followedProviders, unlockedProviders, detail);
        return jsonObject.toString();
    }
    @RequestMapping(value = "/ur/es/providerTagsearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public String unrestictedproviderTagSearch(@RequestBody String query, @RequestParam(value = "followedProviders", required = false) boolean followedProviders, @RequestParam(value = "unlockedProviders", required = false) boolean unlockedProviders, @RequestParam(value = "detail", required = false) boolean detail) throws URISyntaxException {
      // System.out.println("query"+query);
    	JsonObject jsonObject = service.providerTagsearch(query, INDEX_DATA_TAG, followedProviders, unlockedProviders, detail);
        return jsonObject.toString();
    }
}
