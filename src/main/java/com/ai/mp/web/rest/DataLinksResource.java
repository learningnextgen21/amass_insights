package com.ai.mp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.ai.mp.repository.search.DataLinkSearchRepository;
import com.ai.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ai.mp.domain.DataLinks;
import com.ai.mp.domain.User;
import com.ai.mp.repository.DataLinksRepository;
import com.ai.mp.service.DataLinkService;
import com.ai.mp.service.UserService;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class DataLinksResource {

    private final Logger log = LoggerFactory.getLogger(DataLinksResource.class);

    private static final String ENTITY_NAME = "dataLinks";

    private final DataLinkService dataLinkService;
    private final UserService userService;
    private final DataLinksRepository dataLinksRepository;
    private final DataLinkSearchRepository dataLinkSearchRepository;

    public DataLinksResource(DataLinkService dataLinkService, UserService userService, DataLinksRepository dataLinksRepository, DataLinkSearchRepository dataLinkSearchRepository)
    {
        this.dataLinkService=dataLinkService;
        this.userService=userService;
        this.dataLinksRepository=dataLinksRepository;
        this.dataLinkSearchRepository = dataLinkSearchRepository;
    }
    @PostMapping("/links")
    @Timed
    public ResponseEntity<DataLinks> createDataLinks(@Valid @RequestBody DataLinks dataLinks) throws URISyntaxException {
        log.debug("REST request to save DataLinks : {}", dataLinks);

        if (dataLinks.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dataLinks cannot already have an ID")).body(null);
        }
        User user = userService.getUser();
        dataLinks.setCreatorUserId(user.getId());
        dataLinks.setRecordID("recl"+ Utils.randomString(13,13));
        DataLinks result = dataLinkService.save(dataLinks);
        DataLinks dataLink = dataLinkService.findOne(result.getId());
        dataLinkSearchRepository.save(dataLink);
        return ResponseEntity.created(new URI("/api/data-links/" + dataLink.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, dataLink.getId().toString()))
            .body(dataLink);
    }

    @PutMapping("/links")
    @Timed
    public ResponseEntity<DataLinks> updateDataLinks(@Valid @RequestBody DataLinks dataLinks) throws URISyntaxException {
        log.debug("REST request to update DataLinks : {}", dataLinks);
        if (dataLinks.getId() == null) {
            return createDataLinks(dataLinks);
        }
        User user = userService.getUser();
        if(dataLinks.getId()!=null)
        {
            DataLinks links=dataLinksRepository.findOne(dataLinks.getId());
            dataLinks.setWebCreatedDate(links.getWebCreatedDate());
            dataLinks.setCreatorUserId(links.getCreatorUserId());
            dataLinks.setEditorUserId(user.getId());
        }

        DataLinks result = dataLinkService.save(dataLinks);
        DataLinks dataLink = dataLinkService.findOne(result.getId());
        dataLinkSearchRepository.save(dataLink);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataLink.getId().toString()))
            .body(result);
    }

    @GetMapping("/links")
    @Timed
    public ResponseEntity<List<DataLinks>> getAllDataLinks(@ApiParam Pageable pageable) {
        Page<DataLinks> page = dataLinkService.searchAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/data-links");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    @GetMapping("/links/{id}")
    @Timed
    public ResponseEntity<DataLinks> getDataLinks(@PathVariable Long id) {
        log.debug("REST request to get DataLinks : {}", id);
        DataLinks dataLinks = dataLinkService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataLinks));
    }
    @DeleteMapping("/links/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataLinks(@PathVariable Long id) {
        log.debug("REST request to delete DataLinks : {}", id);
        dataLinkService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
