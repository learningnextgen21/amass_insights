package com.ai.mp.web.rest;

import com.ai.mp.batch.DBUtils;
import com.ai.mp.batch.NotificationUtils;
import com.ai.mp.domain.EmailAddress;
import com.ai.mp.security.AuthoritiesConstants;
import com.ai.mp.service.ElasticsearchGenericService;
import com.codahale.metrics.annotation.Timed;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.ai.mp.config.ElasticsearchConfiguration.INDEX_EMAIL_ADDRESS;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * REST controller for managing Elasticsearch index.
 */
@RestController
@RequestMapping("/api/etl")
public class DataProcessingResource {

    private final Logger log = LoggerFactory.getLogger(DataProcessingResource.class);

    private final ElasticsearchGenericService elasticsearchGenericService;
    public DataProcessingResource(DBUtils dbutils, NotificationUtils notificationUtils,ElasticsearchGenericService elasticsearchGenericService) {
        this.dbutils = dbutils;
        this.notificationUtils = notificationUtils;
        this.elasticsearchGenericService=elasticsearchGenericService;
    }

    private final DBUtils dbutils;
    private final NotificationUtils notificationUtils;

    @RequestMapping(value = "/load",
        method = RequestMethod.GET)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public boolean load(@RequestParam(name = "folderPath") String folderPath) throws URISyntaxException {
        File file = new File(folderPath);
        if(!file.exists())
            return false;
        return dbutils.loadData(file);
    }

    @RequestMapping(value = "/split",
        method = RequestMethod.GET)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public boolean split(@RequestParam(name = "folderPath") String folderPath) throws URISyntaxException {
        File file = new File(folderPath);
        if(!file.exists())
            return false;
        return dbutils.processStagingData(file);
    }

    @RequestMapping(value = "/populate",
        method = RequestMethod.GET)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public boolean populate(@RequestParam(name = "folderPath") String folderPath) throws URISyntaxException {
        return dbutils.populateDataModel(new File(folderPath));
    }

    @RequestMapping(value = "/processall",
        method = RequestMethod.GET)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public boolean processAll(@RequestParam(name = "folderPath") String folderPath) throws URISyntaxException {
        File file = new File(folderPath);
        if(!file.exists())
            return false;
        return dbutils.process(file);
    }

    @RequestMapping(value = "/generatenotifications",
        method = RequestMethod.GET)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public void generateNotifications(@RequestParam(name = "pastDays") int pastDays, @RequestParam(name="loginID") String loginID, @RequestParam(name="mailTo") String mailTo) {
        if(pastDays < 0)
        {
            pastDays = 10;
        }
        notificationUtils.generateNotifications(pastDays, loginID, mailTo);
    }

    @RequestMapping(value = "/generate_generic_notifications",
        method = RequestMethod.GET)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public void generateGenericNotifications(@RequestParam(name="fullName") String fullName,@RequestParam(name = "pastDays") int pastDays, @RequestParam(name="mailTo") String mailTo) {
        if(pastDays < 0)
        {
            pastDays = 10;
        }
        notificationUtils.generateGenericNotifications(fullName, pastDays, mailTo);
    }


    @RequestMapping(value = "/sendallnotifications",
        method = RequestMethod.GET)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public void sendallnotifications(@RequestParam(name = "pastDays") int pastDays, @RequestParam(name = "test") boolean test) {
        if(pastDays < 0)
        {
            pastDays = 10;
        }
        notificationUtils.sendallNotifications(pastDays,test);
    }
    @RequestMapping(value="/mailjetnotifications",method=RequestMethod.POST)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public void sendmailjetnotification(@RequestParam("mailjetTemplateID")int mailjetTemplateID,@RequestBody String emailAddressQuery)
    {
        JsonObject jsonObject =elasticsearchGenericService.emailSearch(emailAddressQuery,INDEX_EMAIL_ADDRESS);
        List<EmailAddress>emailList=new ArrayList<EmailAddress>();
        Gson gson=new Gson();
        JsonArray hitsArray = jsonObject.getAsJsonObject("hits").getAsJsonArray("hits");
        for (JsonElement e : hitsArray) {
            JsonObject obj = e.getAsJsonObject();
            JsonObject sourceObject = obj.getAsJsonObject("_source");
            EmailAddress emails = gson.fromJson(sourceObject, EmailAddress.class);
            EmailAddress emailAddress=new EmailAddress();
            emailAddress.setId(emails.getId());
            emailAddress.setEmailAddress(emails.getEmailAddress());
            emailAddress.setRecordID(emails.getRecordID());
            emailList.add(emailAddress);
        }
        notificationUtils.sendmailjetnotification(mailjetTemplateID,emailList);

    }

}
