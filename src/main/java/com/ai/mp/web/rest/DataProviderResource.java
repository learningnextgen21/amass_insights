package com.ai.mp.web.rest;

import com.ai.mp.batch.DBUtils;
import com.ai.mp.config.ApplicationProperties;
import com.ai.mp.domain.DataOrganization;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.DataProviderEdit;
import com.ai.mp.domain.User;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.repository.search.DataArticleSearchRepository;
import com.ai.mp.repository.search.DataCategorySearchRepository;
import com.ai.mp.repository.search.DataEventSearchRepository;
import com.ai.mp.repository.search.DataInvestmentMangerSearchRepository;
import com.ai.mp.repository.search.DataProviderSearchRepository;
import com.ai.mp.service.DataOrganizationService;
import com.ai.mp.service.DataProviderService;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.service.UserService;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.ai.util.Utils;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.codehaus.groovy.runtime.DefaultGroovyMethods.round;

/**
 * REST controller for managing DataProvider.
 */
@RestController
@RequestMapping("/api")
public class DataProviderResource {

    private final Logger log = LoggerFactory.getLogger(DataProviderResource.class);
    private final UserService userService;
    private final DataProviderRepository dataProviderRepository;
   private final DataProviderSearchRepository dataProviderSearchRepository;
    private static final String ENTITY_NAME = "dataProvider";
    private final ElasticsearchIndexService elasticsearchIndexService;
    private final DataOrganizationService dataOrganizationService;
    private final DataProviderService dataProviderService;
    private final DBUtils dbutils;
    private final ApplicationProperties applicationProperties;
    private final DataEventSearchRepository dataEventSearchReopsitory;
    private final DataArticleSearchRepository dataArticleSearchRepository;
    private final DataCategorySearchRepository dataCategorySearchRepository;
    private final DataInvestmentMangerSearchRepository dataInvestmentManagerSearchRepository;
    public DataProviderResource(UserService userService, DataProviderRepository dataProviderRepository, ElasticsearchIndexService elasticsearchIndexService, DataOrganizationService dataOrganizationService, DataProviderService dataProviderService, DBUtils dbutils, ApplicationProperties applicationProperties,DataProviderSearchRepository dataProviderSearchRepository,DataEventSearchRepository dataEventSearchReopsitory,DataArticleSearchRepository dataArticleSearchRepository,DataCategorySearchRepository dataCategorySearchRepository,DataInvestmentMangerSearchRepository dataInvestmentManagerSearchRepository) {
        this.userService = userService;
        this.dataProviderRepository = dataProviderRepository;
        this.elasticsearchIndexService = elasticsearchIndexService;
        this.dataOrganizationService = dataOrganizationService;
        this.dataProviderService = dataProviderService;
        this.dbutils = dbutils;
        this.applicationProperties = applicationProperties;
        this.dataProviderSearchRepository = dataProviderSearchRepository;
        this.dataEventSearchReopsitory = dataEventSearchReopsitory;
        this.dataArticleSearchRepository = dataArticleSearchRepository;
        this.dataCategorySearchRepository = dataCategorySearchRepository;
        this.dataInvestmentManagerSearchRepository=dataInvestmentManagerSearchRepository;
    }

    /**
     * POST  /data-providers : Create a new dataProvider.
     *
     * @param dataProvider the dataProvider to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dataProvider, or with status 400 (Bad Request) if the dataProvider has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
        @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @PostMapping("/providers")
    @Timed
    public ResponseEntity<DataProvider> createDataProvider(@Valid @RequestBody DataProvider dataProvider) throws URISyntaxException {
        log.debug("REST request to save DataProvider : {}", dataProvider);
        if (dataProvider.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dataProvider cannot already have an ID")).body(null);
        }
        DataOrganization org=new DataOrganization();
        DataProvider provider = new DataProvider();
        User user = userService.getUser();

       /* DataOrganization dataOrganization1 = dataProvider.getOwnerOrganization();
        if (dataOrganization1 == null) {
        	DataOrganization dataOrganization =new DataOrganization();
            dataOrganization.setRecordID("recO"+ Utils.randomString(13,13));
            dataOrganization.setName(dataProvider.getProviderName());
            dataOrganization.setWebsite(dataProvider.getWebsite());
          //  dataOrganization.setEditorUserId(user.getId());
            dataOrganization.setCreatorUserId(user.getId());
            dataOrganization.setEditorUserId(user.getId());
            org = dataOrganizationService.save(dataOrganization);
            dataProvider.setOwnerOrganization(org);
        }else
        {
            org = dataOrganizationService.save(dataOrganization1);
            dataProvider.setOwnerOrganization(org);
        }*/
        dataProvider.setRecordID("recp"+ Utils.randomString(13,13));
        dataProvider.setCreatorUserId(user.getId());
        dataProvider.setEditorUserId(user.getId());
        LocalDate localDate = LocalDate.now();
        dataProvider.setCreatedDate(localDate);
        dataProvider.setUpdatedDate(localDate);
        provider = dataProviderService.save(dataProvider);

        if(provider.getId() !=null)        {

            elasticsearchIndexService.reindexSingleProvider(provider.getId());
        }
        return ResponseEntity.created(new URI("/api/data-providers/" + provider.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, provider.getId().toString()))
            .body(provider);
    }

    /**
     * PUT  /data-providers : Updates an existing dataProvider.
     *
     * @param dataProvider the dataProvider to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dataProvider,
     * or with status 400 (Bad Request) if the dataProvider is not valid,
     * or with status 500 (Internal Server Error) if the dataProvider couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @PutMapping("/providers")
    @Timed
    public ResponseEntity<?> updateDataProvider(@Valid @RequestBody DataProvider dataProvider) throws URISyntaxException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, SecurityException, NoSuchFieldException {
        // log.debug("REST request to update DataProvider : {}", dataProvider);
        if (dataProvider.getId() == null) {
            return createDataProvider(dataProvider);
        }
        DataProvider newProvider = new DataProvider();
        User user = userService.getUser();
        if(dataProvider.getId()!=null)
        {
            DataProvider provider = dataProviderRepository.findOne(dataProvider.getId());
            dataProvider.setWebCreatedDate(provider.getWebCreatedDate());
            dataProvider.setCreatorUserId(provider.getCreatorUserId());
            dataProvider.setEditorUserId(user.getId());
            LocalDate localDate = LocalDate.now();
            dataProvider.setCreatedDate(provider.getCreatedDate());
            dataProvider.setUpdatedDate(localDate);

        }
        if(dataProvider.isInactive()==null)
        {
            dataProvider.setInactive(false);
        }
        if(dataProvider.getFreeTrialAvailable()==null)
        {
            dataProvider.setFreeTrialAvailable(false);
        }
        flippingtable(dataProvider);
        String historicalDate=dataProviderService.historicalDate(dataProvider);
        if(null!=historicalDate)
        {
            dataProvider.setHistoricalDateRange(historicalDate);
        }
        dataProvider.setTagAdded(true);
       DataProvider provider=dataProviderService.addTag(dataProvider);
        newProvider=dataProviderRepository.save(provider);
        int amountOfFieldsNotNull=0;
        int amountOfFields = 0;
        Class<DataProvider>  aClass = DataProvider.class;
        Field[] fields = aClass.getDeclaredFields();
        for(Field field : fields) {
            amountOfFields++;
            Field declaredField = newProvider.getClass().getDeclaredField(field.getName());
            declaredField.setAccessible(true);
            Object value = declaredField.get(newProvider);
           if(value !=null) {
                if(!check(value))
                {
                    amountOfFieldsNotNull++;

                }
            }
        }
        int transientField=5;
        int finalamountOfFields=amountOfFields-transientField;
        float completness=amountOfFieldsNotNull*100f/finalamountOfFields;
        float finalCompleteness=round(completness, 2);
        newProvider.setCompleteness(finalCompleteness);
        newProvider=dataProviderRepository.save(newProvider);
         dataProviderService.updateJsonTag(newProvider);
         dataProviderService.updateUserProvider(dataProvider);
         dataProviderService.updateEmailAddress(dataProvider);
         //dbutils.populateDataModel();
        //dbutils.populateDataModelProvider();
        elasticsearchIndexService.reindexSingleProvider(newProvider.getId());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, newProvider.getId().toString()))
            .body("{\"status\": \"success\",\"message\": \"Updated successfully\"}");
    }

    private void flippingtable(DataProvider dataProvider) {
        // TODO Auto-generated method stub
        for (DataProvider dataProviderPartner : dataProvider.getProviderPartners()) {
            dataProviderService.insertPartner(dataProviderPartner.getId(), dataProvider.getId());
            elasticsearchIndexService.reindexSingleProvider(dataProviderPartner.getId());

        }
    }

    private boolean check(Object value) {
        // TODO Auto-generated method stub

        boolean check=false;
        try {
            check= ObjectUtils.isEmpty(value);
        }
        catch(Exception e)
        {
        }

        return check;
    }

    public static float round(float number, int decimalPlace) {
        BigDecimal bd = new BigDecimal(number);
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }


    /**
     * GET  /data-providers : get all the dataProviders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dataProviders in body
     */
    @GetMapping("/providers")
    @Timed
    public ResponseEntity<List<DataProvider>> getAllDataProviders(@ApiParam Pageable pageable) {
        Page<DataProvider> page = dataProviderService.searchAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/data-providers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /data-providers : get all the dataProviders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dataProviders in body
     */
    @GetMapping("/providers-ext/")
    @Timed
    public ResponseEntity<List<DataProvider>> getDataProvidersExt(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataProviders for query {}", query);
        Page<DataProvider> page = dataProviderService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/providers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /data-providers/:id : get the "id" dataProvider.
     *
     * @param id the id of the dataProvider to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dataProvider, or with status 404 (Not Found)
     */
//    @GetMapping("/data-providers/{id}")
//    @Timed
//    public ResponseEntity<DataProvider> getDataProvider(@PathVariable Long id) {
//        log.debug("REST request to get DataProvider : {}", id);
//        DataProvider dataProvider = dataProviderService.searchOne(id);
//        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataProvider));
//    }

    @GetMapping("/providers/db/{id}")
    @Timed
    public ResponseEntity<DataProvider> getDataProviderFromDB(@PathVariable Long id) {
        log.debug("REST request to get DataProvider : {}", id);
        DataProvider dataProvider = dataProviderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataProvider));
    }
    @GetMapping(path = "/providers/{recordId}")
    @Timed
    public ResponseEntity<DataProvider> providersRecord(@PathVariable String recordId) throws URISyntaxException, IOException {
        DataProvider dataProvider = dataProviderRepository.findByRecordID(recordId);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataProvider));
    }
    /**
     * DELETE  /data-providers/:id : delete the "id" dataProvider.
     *
     * @param id the id of the dataProvider to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/providers/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataProvider(@PathVariable Long id) {
        log.debug("REST request to delete DataProvider : {}", id);
        dataProviderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/data-providers?query=:query : search for the dataProvider corresponding
     * to the query.
     *
     * @param query the query of the dataProvider search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/providers")
    @Timed
    public ResponseEntity<List<DataProvider>> searchDataProviders(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataProviders for query {}", query);
        Page<DataProvider> page = dataProviderService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/providers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


   /* @GetMapping("/data-updatefrequency")
    @Timed
    public Set<DataProvider> getDataUpdateFrequency()
    {



    	return dataProviderService.getDataUpdateFrequency();
    	 Page<DataArticle> page = null;
    	  HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/data-articles");
        return dataProviderService.getDataUpdateFrequency();

    }*/

   /*@RequestMapping(value = "/es/qdeliveryferquency", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.USER + "')")
   // @GetMapping("/qdeliveryferquency/db/{id}")
    public Set<DataProvider> queryDeliveryFrequency() throws URISyntaxException {
        Set<DataProvider> dataProviders = dataProviderService.getDataUpdateFrequency();
        return dataProviders;
    }*/

    @GetMapping("/ur/providerscount/{entityType}")
    @Timed
    public ResponseEntity<Long> providerscount(@PathVariable String entityType) throws URISyntaxException, IOException {
    	Long value= null;
       // System.out.println("entityType"+entityType);
		if(entityType.equalsIgnoreCase("providers"))
	    {
			value= dataProviderSearchRepository.count();

	    }
	     else if(entityType.equalsIgnoreCase("events"))
	    	{
		    value= dataEventSearchReopsitory.count();

	    	}
	     else if(entityType.equalsIgnoreCase("categories"))
	     {
			    value= dataCategorySearchRepository.count();

	     }
	     else if(entityType.equalsIgnoreCase("articles"))
	     {
	    	 value=dataArticleSearchRepository.count();
	     }
	     else if(entityType.equalsIgnoreCase("investors"))
	     {
	    	 value=dataInvestmentManagerSearchRepository.count();
	     }
		
    	return new ResponseEntity<Long>(value, HttpStatus.OK);


    }
    
   @PostMapping("/providersTag")
   // @Transactional
    /*@Scheduled(cron = "0 0/10 * * * ?")
    @Transactional*/
    protected void getdataProviderTag()
    {
    List<DataProvider> dataProvider=dataProviderRepository.findByTagAdded(true);
   // System.out.println("size"+dataProvider.size());
    if(null!=dataProvider)
    {
    	for(DataProvider provider:dataProvider)
    	{
            DataProvider dp = dataProviderRepository.findOneWithEagerRelationshipsTag(provider.getId());
           DataProvider dataProviders= dataProviderService.addTag(dp);
           dataProviders.setTagAdded(false);
           dataProviderRepository.save(dataProviders);
           //dataProviderSearchRepository.save(dataProviders);
           dbutils.populateDataModelProvider();
           elasticsearchIndexService.reindexSingleProvider(dataProviders.getId());
    	}
    }
    }
   @PutMapping("/providerUpdate")
   @Timed
   public ResponseEntity<?> updateUserDataProviderEdits(@Valid @RequestBody DataProvider dataProvider) throws URISyntaxException {
	   dataProvider=dataProviderRepository.save(dataProvider);

	   if(dataProvider.getId() !=null)        {

           elasticsearchIndexService.reindexSingleProvider(dataProvider.getId());
       }
	   dataProviderService.sendMailToUser(dataProvider);
       return ResponseEntity.created(new URI("/api/data-providerUpdate/" + dataProvider.getId()))
           .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, dataProvider.getId().toString()))
           .body(dataProvider);
	   
   }
   
}
