package com.ai.mp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ai.mp.domain.DataInvestmentManager;
import com.ai.mp.domain.LookupCode;
import com.ai.mp.security.AuthoritiesConstants;
import com.ai.mp.security.SecurityUtils;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;

/**
 * REST controller for managing Elasticsearch index.
 */
@RestController
@RequestMapping("/api")
public class ElasticsearchIndexResource {

    private final Logger log = LoggerFactory.getLogger(ElasticsearchIndexResource.class);

    private final ElasticsearchIndexService elasticsearchIndexService;

    public ElasticsearchIndexResource(ElasticsearchIndexService elasticsearchIndexService) {
        this.elasticsearchIndexService = elasticsearchIndexService;
    }
    /**
     * POST  /elasticsearch/index -> Reindex all Elasticsearch documents
     */
    @RequestMapping(value = "/elasticsearch/index",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public ResponseEntity<Void> reindexAll() {
        log.info("REST request to reindex Elasticsearch by user : {}", SecurityUtils.getCurrentUserLogin());
        elasticsearchIndexService.reindexAll();
        return ResponseEntity.accepted()
            .headers(HeaderUtil.createAlert("elasticsearch.reindex.accepted", null))
            .build();
    }

    /**
     * POST  /elasticsearch/index -> Reindex Elasticsearch Provider documents
     */
    @RequestMapping(value = "/elasticsearch/index/providers",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public ResponseEntity<Void> reindexProviders(@RequestParam (name = "rebuildMapping") boolean rebuildMapping) throws URISyntaxException {
        log.info("REST request to reindex Elasticsearch Provider documents by user : {}", SecurityUtils.getCurrentUserLogin());
        elasticsearchIndexService.reindexProviders(rebuildMapping);
        return ResponseEntity.accepted()
            .headers(HeaderUtil.createAlert("elasticsearch.providers.reindex.accepted", null))
            .build();
    }

    @RequestMapping(value = "/elasticsearch/index/provider/{id}",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public ResponseEntity<Void> reindexSingleProvider(@PathVariable Long id) {
        log.info("REST request to reindex Elasticsearch Provider document by user : {}, provider ID: {}", SecurityUtils.getCurrentUserLogin(), id);
        elasticsearchIndexService.reindexSingleProvider(id);
        return ResponseEntity.accepted()
            .headers(HeaderUtil.createAlert("elasticsearch.providers.reindex.accepted", String.valueOf(id)))
            .build();
    }

    @RequestMapping(value = "/elasticsearch/index/articles",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public ResponseEntity<Void> reindexArticles() {
        log.info("REST request to reindex Elasticsearch Provider documents by user : {}", SecurityUtils.getCurrentUserLogin());
        elasticsearchIndexService.reindexArticles();
        return ResponseEntity.accepted()
            .headers(HeaderUtil.createAlert("elasticsearch.articles.reindex.accepted", null))
            .build();
    }

    @RequestMapping(value = "/elasticsearch/index/events",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public ResponseEntity<Void> reindexEvents() {
        log.info("REST request to reindex Elasticsearch Events documents by user : {}", SecurityUtils.getCurrentUserLogin());
        elasticsearchIndexService.reindexEvents();
        return ResponseEntity.accepted()
            .headers(HeaderUtil.createAlert("elasticsearch.events.reindex.accepted", null))
            .build();
    }

    @RequestMapping(value = "/elasticsearch/index/lookupcodes",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public ResponseEntity<Void> reindexLookupCode() {
        log.info("REST request to reindex Elasticsearch LookupCode documents by user : {}", SecurityUtils.getCurrentUserLogin());
        elasticsearchIndexService.reindexLookupCode(true);
        return ResponseEntity.accepted()
            .headers(HeaderUtil.createAlert("elasticsearch.lookupCode.reindex.accepted", null))
            .build();
    }

    @RequestMapping(value = "/elasticsearch/index/dataorganization",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public ResponseEntity<Void> reindexdataorganization() {
        log.info("REST request to reindex Elasticsearch dataorganization documents by user : {}", SecurityUtils.getCurrentUserLogin());
        elasticsearchIndexService.reindexdataorganization();
        return ResponseEntity.accepted()
            .headers(HeaderUtil.createAlert("elasticsearch.dataorganization.reindex.accepted", null))
            .build();
    }


    @RequestMapping(value = "/elasticsearch/index/dataresources",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public ResponseEntity<Void> reindexdataresource() {
        log.info("REST request to reindex Elasticsearch dataresources documents by user : {}", SecurityUtils.getCurrentUserLogin());

        elasticsearchIndexService.reindexResources();

        return ResponseEntity.accepted()
            .headers(HeaderUtil.createAlert("elasticsearch.dataresource.reindex.accepted", null))
            .build();
    }

    @RequestMapping(value = "/elasticsearch/index/resource/{id}",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public ResponseEntity<Void> reindexSingleResource(@PathVariable Long id) {

        elasticsearchIndexService.reindexSingleResource(id);

        return ResponseEntity.accepted()
            .headers(HeaderUtil.createAlert("elasticsearch.resources.reindex.accepted", String.valueOf(id)))
            .build();
    }
    @RequestMapping(value = "/elasticsearch/index/email",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public ResponseEntity<Void> reindexEmail() {
        log.info("REST request to reindex Elasticsearch Email documents by user : {}", SecurityUtils.getCurrentUserLogin());
        elasticsearchIndexService.reindexEmail();
        return ResponseEntity.accepted()
            .headers(HeaderUtil.createAlert("elasticsearch.email.reindex.accepted", null))
            .build();
    }

    @RequestMapping(value = "/elasticsearch/index/dataTag",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
    public ResponseEntity<Void> reindexDataTag() {
        log.info("REST request to reindex Elasticsearch Tag documents by user : {}", SecurityUtils.getCurrentUserLogin());
        elasticsearchIndexService.reindexDataTag();
        return ResponseEntity.accepted()
            .headers(HeaderUtil.createAlert("elasticsearch.tag.reindex.accepted", null))
            .build();
    }
    
    @RequestMapping(value = "/elasticsearch/index/investorManagers",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
        @Timed
        @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
        public ResponseEntity<Void> reindexInvestorManagers() {
            log.info("REST request to reindex Elasticsearch Email documents by investment : {}", SecurityUtils.getCurrentUserLogin());
            elasticsearchIndexService.reindexInvestorManagers();
            return ResponseEntity.accepted()
                .headers(HeaderUtil.createAlert("elasticsearch.email.reindex.accepted", null))
                .build();
        }
    
    @RequestMapping(value = "/elasticsearch/index/emails",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
        @Timed
        @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
        public ResponseEntity<Void> reindexEmails() {
            log.info("REST request to reindex Elasticsearch Email documents by emails : {}", SecurityUtils.getCurrentUserLogin());
            elasticsearchIndexService.reindexEmails();
            return ResponseEntity.accepted()
                .headers(HeaderUtil.createAlert("elasticsearch.emails.reindex.accepted", null))
                .build();
        }
    @RequestMapping(value = "/elasticsearch/index/dataSource",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
        @Timed
        @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
        public ResponseEntity<Void> dataSource() {
            log.info("REST request to reindex Elasticsearch Email documents by dataSource : {}", SecurityUtils.getCurrentUserLogin());
            elasticsearchIndexService.reindexDatasources();
            return ResponseEntity.accepted()
                .headers(HeaderUtil.createAlert("elasticsearch.dataSource.reindex.accepted", null))
                .build();
        }
    
    @RequestMapping(value = "/elasticsearch/index/domains",
            method = RequestMethod.POST,
            produces = MediaType.TEXT_PLAIN_VALUE)
        @Timed
        @PreAuthorize("hasAuthority('" + AuthoritiesConstants.ADMIN + "')")
        public ResponseEntity<Void> reindexDomains() {
            log.info("REST request to reindex Elasticsearch Email documents by domains : {}", SecurityUtils.getCurrentUserLogin());
            elasticsearchIndexService.reindexDomain();
            return ResponseEntity.accepted()
                .headers(HeaderUtil.createAlert("elasticsearch.domains.reindex.accepted", null))
                .build();
        }
}
