package com.ai.mp.web.rest;

import com.ai.mp.config.ApplicationProperties;
import com.ai.mp.domain.DataArticle;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.repository.DataArticleRepository;
import com.ai.mp.service.DataArticleService;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.service.MailService;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.naming.LinkException;
import javax.validation.Valid;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DataArticle.
 */
@RestController
@RequestMapping("/api")
public class DataArticleResource {

    private final Logger log = LoggerFactory.getLogger(DataArticleResource.class);

    private static final String ENTITY_NAME = "dataArticle";

    private final DataArticleService dataArticleService;
    private final DataArticleRepository dataArticleRepository;

    private final ApplicationProperties applicationProperties;
    private final ElasticsearchIndexService elasticSearchIndexService;

    private  final MailService mailService;

    public DataArticleResource(DataArticleService dataArticleService, DataArticleRepository dataArticleRepository, ApplicationProperties applicationProperties, MailService mailService, ElasticsearchIndexService elasticSearchIndexService) {
        this.dataArticleService = dataArticleService;
        this.dataArticleRepository = dataArticleRepository;
        this.applicationProperties = applicationProperties;
        this.mailService = mailService;
        this.elasticSearchIndexService=elasticSearchIndexService;
    }

    /**
     * POST  /data-articles : Create a new dataArticle.
     *
     * @param dataArticle the dataArticle to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dataArticle, or with status 400 (Bad Request) if the dataArticle has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/articles")
    @Timed
    public ResponseEntity<DataArticle> createDataArticle(@Valid @RequestBody DataArticle dataArticle) throws URISyntaxException {
        log.debug("REST request to save DataArticle : {}", dataArticle);
        if (dataArticle.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dataArticle cannot already have an ID")).body(null);
        }
        DataArticle result = dataArticleService.save(dataArticle);
        return ResponseEntity.created(new URI("/api/data-articles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /data-articles : Updates an existing dataArticle.
     *
     * @param dataArticle the dataArticle to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dataArticle,
     * or with status 400 (Bad Request) if the dataArticle is not valid,
     * or with status 500 (Internal Server Error) if the dataArticle couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/articles")
    @Timed
    public ResponseEntity<DataArticle> updateDataArticle(@Valid @RequestBody DataArticle dataArticle) throws URISyntaxException {
        log.debug("REST request to update DataArticle : {}", dataArticle);
        if (dataArticle.getId() == null) {
            return createDataArticle(dataArticle);
        }
        DataArticle result = dataArticleService.save(dataArticle);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataArticle.getId().toString()))
            .body(result);
    }

    /**
     * GET  /data-articles : get all the dataArticles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dataArticles in body
     */
    @GetMapping("/articles")
    @Timed
    public ResponseEntity<List<DataArticle>> getAllDataArticles(@ApiParam Pageable pageable) {
        Page<DataArticle> page = dataArticleService.searchAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/data-articles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /data-articles/:id : get the "id" dataArticle.
     *
     * @param id the id of the dataArticle to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dataArticle, or with status 404 (Not Found)
     */
    @GetMapping("/articles/{id}")
    @Timed
    public ResponseEntity<DataArticle> getDataArticle(@PathVariable Long id) {
        log.debug("REST request to get DataArticle : {}", id);
        DataArticle dataArticle = dataArticleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataArticle));
    }

    /**
     * DELETE  /data-articles/:id : delete the "id" dataArticle.
     *
     * @param id the id of the dataArticle to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/articles/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataArticle(@PathVariable Long id) {
        log.debug("REST request to delete DataArticle : {}", id);
        dataArticleService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/data-articles?query=:query : search for the dataArticle corresponding
     * to the query.
     *
     * @param query the query of the dataArticle search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/articles")
    @Timed
    public ResponseEntity<List<DataArticle>> searchDataArticles(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataArticles for query {}", query);
        Page<DataArticle> page = dataArticleService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/articles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/articles-ext/")
    @Timed
    public ResponseEntity<List<DataArticle>> getDataArticleExt(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataArticle for query {}", query);
        Page<DataArticle> page = dataArticleService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/articles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/socialArticle")
    public ResponseEntity<?> getArticleSocialApi()
    {

        DataArticle article = mailService.getArticleApi();
        if(article!=null)
        {
            for(DataProvider dp:article.getRelevantProviders())
            {
            //    System.out.println("!!!"+dp.getId());
                elasticSearchIndexService.reindexSingleProvider(dp.getId());

            }
        }
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @Timed
    @Transactional
    @Scheduled(cron = "0 0 18 * *  MON-FRI")
    protected void getArticleSocialApis() {

    //    System.out.println("cron job started  for article share to admin");
     //   System.out.println("!!!!!"+applicationProperties.getArticle());
        if (applicationProperties.getArticle()) {

            DataArticle article = mailService.getArticleApi();
            indexProvider(article);
        }
    }

    public  void indexProvider(DataArticle article)
    {

        if(article!=null)
        {
            for(DataProvider dp:article.getRelevantProviders())
            {
            //    System.out.println("index"+dp.getId());
                elasticSearchIndexService.reindexSingleProvider(dp.getId());
            }
        }
    }

    /*Twitter4j
    Twitter */
    @Scheduled(cron = "0 0 7 * * MON-FRI")
    @Transactional
    protected void getArticleTwitter() throws OAuthExpectationFailedException, OAuthCommunicationException, OAuthMessageSignerException, IOException, LinkException {
     //   System.out.println("cron job started  for article share to admin");
    	 if (applicationProperties.getArticle()) {
    	DataArticle da= mailService.sendTwitter();
        DataArticle dp=mailService.sendlinkedIn();
        DataArticle dataArticle = dataArticleRepository.findOneWithEagerRelationships(da.getId());
        if(dataArticle!=null)
        {
            dataArticleRepository.updateSentOnly(dataArticle.getId());
        }
        if(da!=null && dataArticle!=null)
        {

            try {
                indexProvider(dataArticle);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        if(dp!=null && dataArticle!=null)
        {
           // DataArticle dataArticle = dataArticleRepository.findOneWithEagerRelationships(dp.getId());

            try {
                indexProvider(dataArticle);

            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
       
    	 }
    }

}
