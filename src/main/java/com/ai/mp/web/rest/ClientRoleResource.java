package com.ai.mp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ai.mp.domain.ClientRole;

import com.ai.mp.repository.ClientRoleRepository;
import com.ai.mp.repository.search.ClientRoleSearchRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ClientRole.
 */
@RestController
@RequestMapping("/api")
public class ClientRoleResource {

    private final Logger log = LoggerFactory.getLogger(ClientRoleResource.class);

    private static final String ENTITY_NAME = "clientRole";

    private final ClientRoleRepository clientRoleRepository;

    private final ClientRoleSearchRepository clientRoleSearchRepository;
    public ClientRoleResource(ClientRoleRepository clientRoleRepository, ClientRoleSearchRepository clientRoleSearchRepository) {
        this.clientRoleRepository = clientRoleRepository;
        this.clientRoleSearchRepository = clientRoleSearchRepository;
    }

    /**
     * POST  /client-roles : Create a new clientRole.
     *
     * @param clientRole the clientRole to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientRole, or with status 400 (Bad Request) if the clientRole has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-roles")
    @Timed
    public ResponseEntity<ClientRole> createClientRole(@Valid @RequestBody ClientRole clientRole) throws URISyntaxException {
        log.debug("REST request to save ClientRole : {}", clientRole);
        if (clientRole.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new clientRole cannot already have an ID")).body(null);
        }
        ClientRole result = clientRoleRepository.save(clientRole);
        clientRoleSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/client-roles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-roles : Updates an existing clientRole.
     *
     * @param clientRole the clientRole to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientRole,
     * or with status 400 (Bad Request) if the clientRole is not valid,
     * or with status 500 (Internal Server Error) if the clientRole couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-roles")
    @Timed
    public ResponseEntity<ClientRole> updateClientRole(@Valid @RequestBody ClientRole clientRole) throws URISyntaxException {
        log.debug("REST request to update ClientRole : {}", clientRole);
        if (clientRole.getId() == null) {
            return createClientRole(clientRole);
        }
        ClientRole result = clientRoleRepository.save(clientRole);
        clientRoleSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientRole.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-roles : get all the clientRoles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of clientRoles in body
     */
    @GetMapping("/client-roles")
    @Timed
    public ResponseEntity<List<ClientRole>> getAllClientRoles(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ClientRoles");
        Page<ClientRole> page = clientRoleRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/client-roles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /client-roles/:id : get the "id" clientRole.
     *
     * @param id the id of the clientRole to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientRole, or with status 404 (Not Found)
     */
    @GetMapping("/client-roles/{id}")
    @Timed
    public ResponseEntity<ClientRole> getClientRole(@PathVariable Long id) {
        log.debug("REST request to get ClientRole : {}", id);
        ClientRole clientRole = clientRoleRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientRole));
    }

    /**
     * DELETE  /client-roles/:id : delete the "id" clientRole.
     *
     * @param id the id of the clientRole to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-roles/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientRole(@PathVariable Long id) {
        log.debug("REST request to delete ClientRole : {}", id);
        clientRoleRepository.delete(id);
        clientRoleSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/client-roles?query=:query : search for the clientRole corresponding
     * to the query.
     *
     * @param query the query of the clientRole search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/client-roles")
    @Timed
    public ResponseEntity<List<ClientRole>> searchClientRoles(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ClientRoles for query {}", query);
        Page<ClientRole> page = clientRoleSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/client-roles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
