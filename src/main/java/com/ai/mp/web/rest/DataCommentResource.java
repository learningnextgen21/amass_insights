package com.ai.mp.web.rest;

import com.ai.mp.domain.DataComment;
import com.ai.mp.domain.DataOrganization;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.User;
import com.ai.mp.repository.DataCommentRepository;
import com.ai.mp.service.DataCommentService;
import com.ai.mp.service.UserProviderService;
import com.ai.mp.service.UserService;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.ai.util.Utils;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class DataCommentResource {
    private final UserService userService;
    private final Logger log = LoggerFactory.getLogger(DataCommentResource.class);
    private static final String ENTITY_NAME = "dataComment";
    private final DataCommentService dataCommentService;
private final DataCommentRepository dataCommentRepository;
    private final UserProviderService userProviderService;

    public DataCommentResource(DataCommentRepository dataCommentRepository, DataCommentService dataCommentService, UserService userService, UserProviderService userProviderService) {
        this.dataCommentRepository = dataCommentRepository;
        this.dataCommentService = dataCommentService;
        this.userService = userService;
        this.userProviderService = userProviderService;
    }

    @GetMapping("/comments")
    @Timed
    public ResponseEntity<List<DataComment>> getAllComments(@ApiParam Pageable pageable) {
        Page<DataComment> page = dataCommentRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/data-comments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping(path = "/data-comment", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    @Timed
    public ResponseEntity<DataComment> createComment(@RequestBody DataComment dataComment) throws URISyntaxException, IOException {
        dataComment.setRecordID("rec1"+Utils.randomString(13,13));
        User user = userService.getUser();
        dataComment.setByUserId(user);
        DataComment result = dataCommentRepository.save(dataComment);
        userProviderService.processProviderComment(result);
        return ResponseEntity.created(new URI("/api/data_comment/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @GetMapping("/comment")
    @Timed
    public ResponseEntity<List<DataComment>> getComment(@RequestParam(value = "id") Long id,@RequestParam(value = "privacy") Long privacy) {
        User user = userService.getUser();
        List<DataComment> result=new ArrayList<>();
        if (privacy==582) {
            result = dataCommentRepository.getPublic(id);
        }else if (privacy==581)
        {
            result = dataCommentRepository.getPrivate(id, user.getId());
        }else {
            result = dataCommentRepository.getCommentByRecordID(id, user.getId());
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(result));
    }

    @PutMapping("/data-comment")
    @Timed
    public ResponseEntity<DataComment> updateDataComment(@Valid @RequestBody DataComment dataComment) throws URISyntaxException {
        User user = userService.getUser();
         dataComment.setByUserId(user);
    	DataComment result = dataCommentRepository.save(dataComment);
        userProviderService.processProviderComment(result);
        return ResponseEntity.created(new URI("/api/data_comment/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
   }
    
   /* @GetMapping("/comment-ext/")
    @Timed
    public ResponseEntity<List<DataComment>> getDataCommentExt(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataComment for query {}", query);
        Page<DataComment> page = dataCommentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/comment");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }   */
    @GetMapping("/comment/db/{id}")
    @Timed
    public ResponseEntity<DataComment> getDataCommentFromDB(@PathVariable Long id) {
        log.debug("REST request to get DataComment : {}", id);
        DataComment dataComment = dataCommentRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataComment));
    }
    @DeleteMapping("/comment/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataComment(@PathVariable Long id) {
        log.debug("REST request to delete DataComment : {}", id);
        dataCommentRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    /*@GetMapping("/_search/comment")
    @Timed
    public ResponseEntity<List<DataComment>> searchDataProviders(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataComment for query {}", query);
        Page<DataComment> page = dataCommentService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/comment");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    */
    
    
    
    
}
