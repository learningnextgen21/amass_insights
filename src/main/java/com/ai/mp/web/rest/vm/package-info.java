/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ai.mp.web.rest.vm;
