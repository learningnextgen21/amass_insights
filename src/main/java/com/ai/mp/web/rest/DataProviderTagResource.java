package com.ai.mp.web.rest;

import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.DataProviderTag;
import com.ai.mp.domain.User;
import com.ai.mp.batch.DBUtils;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.repository.DataProviderTagRepository;
import com.ai.mp.repository.search.DataProviderSearchRepository;
import com.ai.mp.repository.search.DataProviderTagSearchRepository;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.service.UserService;
import com.ai.mp.utils.BasicBean;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.ai.util.Utils;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing DataProviderTag.
 */
@RestController
@RequestMapping("/api")
public class DataProviderTagResource {

    @PersistenceContext
    EntityManager entityManager;

    private final UserService userService;

    private final Logger log = LoggerFactory.getLogger(DataProviderTagResource.class);
    private static final String ENTITY_NAME = "dataProviderTag";

    private final DataProviderTagRepository dataProviderTagRepository;
    private final ElasticsearchIndexService elasticsearchIndexService;
    private final DataProviderRepository dataProviderRepository;
    private final DataProviderSearchRepository dataProviderSearchRepository;

    private final DBUtils dbutils;

    private final DataProviderTagSearchRepository dataProviderTagSearchRepository;
    public DataProviderTagResource(UserService userService, DataProviderTagRepository dataProviderTagRepository, ElasticsearchIndexService elasticsearchIndexService, DataProviderSearchRepository dataProviderSearchRepository, DataProviderRepository dataProviderRepository, DataProviderTagSearchRepository dataProviderTagSearchRepository, DBUtils dbutils) {
        this.userService = userService;
        this.dataProviderRepository = dataProviderRepository;
        this.dataProviderSearchRepository = dataProviderSearchRepository;
        this.dbutils = dbutils;
        this.dataProviderTagRepository = dataProviderTagRepository;
        this.elasticsearchIndexService = elasticsearchIndexService;
        this.dataProviderTagSearchRepository = dataProviderTagSearchRepository;
    }

    /**
     * POST  /data-provider-tags : Create a new dataProviderTag.
     *
     * @param dataProviderTag the dataProviderTag to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dataProviderTag, or with status 400 (Bad Request) if the dataProviderTag has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/data-provider-tags")
    @Timed
    public ResponseEntity<DataProviderTag> createDataProviderTag(@Valid @RequestBody DataProviderTag dataProviderTag) throws URISyntaxException {
        log.debug("REST request to save DataProviderTag : {}", dataProviderTag);
        if (dataProviderTag.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dataProviderTag cannot already have an ID")).body(null);
        }
        User user = userService.getUser();
        dataProviderTag.setByUser(user);
        dataProviderTag.setRecordID("rec0"+Utils.randomString(13,13));
        dataProviderTag.setCreatedDate(LocalDate.now());
        dataProviderTag.setUpdatedDate(Instant.now());
        if(dataProviderTag.getPrivacy().equalsIgnoreCase("private"))
        {
        	dataProviderTag.setType("tag");
        }
        DataProviderTag result = dataProviderTagRepository.save(dataProviderTag);
        List<BasicBean> pro=dataProviderTag.getTagProviders();
        for (BasicBean tag:pro) {
            long num = Long.parseLong(tag.getId());
            dataProviderTagRepository.insetProviderTag(result.getId(),num);
        }
        dbutils.populateDataModelJson();
        dataProviderTagSearchRepository.save(result);
        //Set<DataProvider> providers = result.getProviderTags();
        for (BasicBean tag:pro) {
        	 long num = Long.parseLong(tag.getId());
            elasticsearchIndexService.reindexSingleProvider(num);
        }
        return ResponseEntity.created(new URI("/api/data-provider-tags/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    @PostMapping(path = "/provider-tag")
    @Timed
    public long providerTag(@RequestParam(name = "tagId") long tagId, @RequestParam(name = "providerId") long providerId) throws URISyntaxException {
        int result = dataProviderTagRepository.insetProviderTag(tagId,providerId);
        dbutils.populateDataModelJson();
        if (result==1) {
            elasticsearchIndexService.reindexSingleProvider(providerId);
        }
        return providerId;

    }

    /**
     * PUT  /data-provider-tags : Updates an existing dataProviderTag.
     *
     * @param dataProviderTag the dataProviderTag to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dataProviderTag,
     * or with status 400 (Bad Request) if the dataProviderTag is not valid,
     * or with status 500 (Internal Server Error) if the dataProviderTag couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/data-provider-tags")
    @Timed
    public ResponseEntity<DataProviderTag> updateDataProviderTag(@Valid @RequestBody DataProviderTag dataProviderTag) throws URISyntaxException {
        log.debug("REST request to update DataProviderTag : {}", dataProviderTag);
        if (dataProviderTag.getId() == null) {
            return createDataProviderTag(dataProviderTag);
        }
        dataProviderTag.setUpdatedDate(Instant.now());
        DataProviderTag result = dataProviderTagRepository.save(dataProviderTag);
        dataProviderTagSearchRepository.save(result);
        dbutils.populateDataModelJson();
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataProviderTag.getId().toString()))
            .body(result);
    }

    /**
     * GET  /data-provider-tags : get all the dataProviderTags.
     *
     //* @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dataProviderTags in body
     */

    @GetMapping("/provider-tags")
    @Timed
    public ResponseEntity<List<DataProviderTag>> getAllProviderTags(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DataProviderTags");
        Page<DataProviderTag> page = dataProviderTagRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/provider-tags");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/data-provider-tags")
    @Timed
    public ResponseEntity<List<DataProviderTag>> getAllDataProviderTags() {
        log.debug("REST request to get a page of DataProviderTags");
        User user = userService.getUser();
        List<DataProviderTag> result = dataProviderTagRepository.getAllTagsByUserId(user.getId());
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(result));
    }

    @GetMapping("/public-tags")
    @Timed
    public ResponseEntity<List<DataProviderTag>> getAllPublicTags() {
        List<DataProviderTag> dataProviderTags=new ArrayList<>();
        DataProviderTag dataProviderTag=new DataProviderTag();
        Query q = entityManager.createNativeQuery("SELECT dt.id,dt.provider_tag,dt.privacy,dt.record_id,dt.type FROM datamarketplace.data_provider_tag dt where dt.privacy='Public'");
        List<?> authors = q.getResultList();
        for(int i=0; i<authors.size();i++){
            Object[] row = (Object[]) authors.get(i);
            dataProviderTag=new DataProviderTag();
            dataProviderTag.setId(Long.parseLong(row[0].toString()));
            dataProviderTag.setProviderTag(row[1].toString());
            dataProviderTag.setPrivacy(row[2].toString());
            dataProviderTag.setRecordID(row[3].toString());
            if(null!=row[4])
            {
                dataProviderTag.setType(row[4].toString());

            }
            dataProviderTags.add(dataProviderTag);
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataProviderTags));
        //return null;
    }

    /**
     * GET  /data-provider-tags/:id : get the "id" dataProviderTag.
     *
     * @param id the id of the dataProviderTag to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dataProviderTag, or with status 404 (Not Found)
     */
    @GetMapping("/data-provider-tags/{id}")
    @Timed
    public ResponseEntity<DataProviderTag> getDataProviderTag(@PathVariable Long id) {
        log.debug("REST request to get DataProviderTag : {}", id);
        DataProviderTag dataProviderTag = dataProviderTagRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataProviderTag));
    }

    /**
     * DELETE  /data-provider-tags/:id : delete the "id" dataProviderTag.
     *
     * @param id the id of the dataProviderTag to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/data-provider-tags/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataProviderTag(@PathVariable Long id) {
        log.debug("REST request to delete DataProviderTag : {}", id);
        dataProviderTagRepository.delete(id);
        dataProviderTagSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    @PostMapping(path = "/remove-provider-tag")
    @Timed
    public void removeProviderTag(@RequestParam(name = "tagId") long tagId, @RequestParam(name = "providerId") long providerId) throws URISyntaxException {
    	 int result=dataProviderTagRepository.deleteProviderTag(tagId,providerId);
         /* File dir = new File("E:\\");
          Boolean  update = dbutils.populateDataModel(dir); */
          dbutils.populateDataModelJson();
          if (result==1) {
              elasticsearchIndexService.reindexSingleProvider(providerId);
          }
    }
    /**
     * SEARCH  /_search/data-provider-tags?query=:query : search for the dataProviderTag corresponding
     * to the query.
     *
     * @param query the query of the dataProviderTag search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/data-provider-tags")
    @Timed
    public ResponseEntity<List<DataProviderTag>> searchDataProviderTags(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataProviderTags for query {}", query);
        Page<DataProviderTag> page = dataProviderTagSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/data-provider-tags");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/data-provider-tags/desc/{id}")
    @Timed
    public String getExplanationByID(@PathVariable Long id) {
        log.debug("REST request to search for a page of DataProviderTag for query {}", id);
        DataProviderTag element = dataProviderTagRepository.findOneById(id);
        return element.getExplanation();
    }
    
    @PostMapping("/data-provider-usertag")
    @Timed
    public String getUserTag()
    {
    	//List<User>list=dataProviderTagRepository.getUserValue();
    	Query q = entityManager.createNativeQuery("SELECT * FROM datamarketplace.jhi_user");
        List<?> authors = q.getResultList();
        for(int i=0; i<authors.size();i++){
            Object[] row = (Object[]) authors.get(i);       
        	DataProviderTag dataProviderTag=dataProviderTagRepository.findUserValue(Long.parseLong(row[0].toString()));
        	if(null==dataProviderTag)
        	{
                String record1="recl"+ Utils.randomString(13,13);
                dataProviderTagRepository.userUpdateTag(record1,Long.parseLong(row[0].toString()));
        	}
        }
        
		return "updated";
    	
    }
    @GetMapping("/data-provider-tagsValue/{value}")
    @Timed
    public ResponseEntity<DataProviderTag> getTagValue(@PathVariable String value) {
        log.debug("REST request to get a page of DataProviderTags");
        User user = userService.getUser();
        DataProviderTag result = dataProviderTagRepository.getTagsByUserId(user.getId(),value);
    	return new ResponseEntity<DataProviderTag>(result, HttpStatus.OK);
    }
    @GetMapping("/tagProvider/{tagName}")
    @Timed
    public ResponseEntity<DataProviderTag> getTagProvider(@PathVariable String tagName) {
    	//System.out.println("called");
        User user = userService.getUser();
        String value=tagName;
       // System.out.println("value"+value);
        Long userId=user.getId();
        //System.out.println("userId"+userId);
        DataProviderTag result = dataProviderTagRepository.getTagsByUserId(userId,value);
        DataProviderTag tag=dataProviderTagRepository.getTagProvider(result.getId());
        List<Long>list=new ArrayList<Long>();
        if(null !=tag.getProviderTags())
        {
        for(DataProvider dataProvider: tag.getProviderTags())
        {
        	list.add(dataProvider.getId());
        }
        if(tagName.equalsIgnoreCase("Interested"))
        {
            tag.setInterested(list);
        }
        else if(tagName.equalsIgnoreCase("Not Interested"))
        {
        	tag.setNotInterested(list);
        }
        else if(tagName.equalsIgnoreCase("In Use"))
        {
        	tag.setInUse(list);
        }
        else
        {
        	tag.setTesting(list);
        }
        }
        //System.out.println("called");
		return new ResponseEntity<DataProviderTag>(tag, HttpStatus.OK);

    	
    }
}
