package com.ai.mp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ai.mp.domain.Emails;
import com.ai.mp.repository.EmailsRepository;
import com.ai.mp.repository.search.EmailsSearchRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class EmailsResource {

    private static final String ENTITY_NAME = "emails";
	
    private final EmailsRepository emailsRepository;
    private final EmailsSearchRepository emailsSearchRepository;
    EmailsResource(EmailsRepository emailsRepository,EmailsSearchRepository emailsSearchRepository)
    {
    	this.emailsRepository=emailsRepository;
    	this.emailsSearchRepository=emailsSearchRepository;		
    }
    
    @PostMapping("/emails")
    @Timed
    public ResponseEntity<Emails> createEmails(@Valid @RequestBody Emails emails) throws URISyntaxException {
        if (emails.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new lookupCode cannot already have an ID")).body(null);
        }
        Emails result = emailsRepository.save(emails);
        return ResponseEntity.created(new URI("/api/investor-manager/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    
    @PutMapping("/emails")
    @Timed
    public ResponseEntity<Emails> updateEmails(@Valid @RequestBody Emails emails) throws URISyntaxException {
        if (emails.getId() == null) {
            return createEmails(emails);
        }
        Emails result = emailsRepository.save(emails);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    
    @GetMapping("/emails/{id}")
    @Timed
    public ResponseEntity<Emails> getEmails(@PathVariable Long id) {
	 Emails emails = emailsRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(emails));
    }

    @DeleteMapping("/emails/{id}")
    @Timed
    public ResponseEntity<Void> deleteEmails(@PathVariable Long id) {
    	emailsRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/emails")
    @Timed
    public ResponseEntity<List<Emails>> getAllDataEmails(@ApiParam Pageable pageable) {
        Page<Emails> page = searchAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/emails");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    
    @Transactional(readOnly = true)
    public Page<Emails> searchAll(Pageable pageable) {
        Page<Emails> result = emailsSearchRepository.findAll(pageable);
        return result;
    }
}
