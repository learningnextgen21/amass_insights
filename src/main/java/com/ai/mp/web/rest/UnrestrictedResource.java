
package com.ai.mp.web.rest;

import com.ai.core.StrUtils;
import com.ai.mp.domain.DataArticle;
import com.ai.mp.domain.Email;
import com.ai.mp.domain.EmailAddress;
import com.ai.mp.domain.IpAddress;
import com.ai.mp.domain.User;
import com.ai.mp.domain.UserRequest;
import com.ai.mp.repository.DataArticleRepository;
import com.ai.mp.repository.EmailAddressRepository;
import com.ai.mp.repository.IpAddressRepository;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.service.*;
import com.ai.mp.utils.GenericHttpResponse;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.StringTokenizer;

import static com.ai.mp.config.ElasticsearchConfiguration.INDEX_DATA_EVENT;
import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

/**
 * REST controller for managing Elasticsearch index.
 */
@RestController
@RequestMapping("/api")
public class UnrestrictedResource {
    private final EmailAddressRepository emailAddressRepository;
    private final UserRepository userRepository;
    private final ElasticsearchGenericService elasticsearchService;
    private final UserService userService;
    private final Environment environment;
	private final UserProviderService userProviderService;
    private final EmailAddressService emailAddressService;
    private final MailService mailService;
    private final DataArticleRepository dataArticleRepository;
    private final IpAddressRepository ipAddressRepository;
    public UnrestrictedResource(EmailAddressRepository emailAddressRepository, UserRepository userRepository, ElasticsearchGenericService elasticsearchGenericService, UserService userService, Environment environment, UserProviderService userProviderService, EmailAddressService emailAddressService, MailService mailService, DataArticleRepository dataArticleRepository,IpAddressRepository ipAddressRepository) {
        this.emailAddressRepository = emailAddressRepository;
        this.userRepository = userRepository;
        this.elasticsearchService = elasticsearchGenericService;
        this.userService = userService;
        this.environment = environment;
        this.userProviderService = userProviderService;
        this.emailAddressService = emailAddressService;
        this.mailService = mailService;
        this.dataArticleRepository = dataArticleRepository;
        this.ipAddressRepository=ipAddressRepository;
    }

    private final Logger log = LoggerFactory.getLogger(UnrestrictedResource.class);

    @RequestMapping(value = "/ur/providercount", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public long getProviderCount() {
        return elasticsearchService.getProviderCount();
    }

    @RequestMapping(value = "/ur/eventcount", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public long getEventCount() {
        return elasticsearchService.getEventCount();
    }

    @RequestMapping(value = "/ur/categorycount", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public long getCategoryCount() {
        return elasticsearchService.getCategoryCount();
    }

    @RequestMapping(value = "/ur/articlecount", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public long getArticleCount() {
        return elasticsearchService.getArticleCount();
    }

    @RequestMapping(value = "/ur/featurecount", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public long getFeatureCount() {
        return elasticsearchService.getFeatureCount();
    }

    @RequestMapping(value = "/ur/contactus", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public ResponseEntity<GenericHttpResponse> contactUs(@RequestBody UserRequest userRequest) {
        userProviderService.contactUs(userRequest);
        return ResponseEntity.ok().body(new GenericHttpResponse("Contact us", "Thank you for contacting Amass Insights. <br>We will be in touch with you soon", "success"));
    }

    @RequestMapping(value = "/ur/contact-us-header-request-logout", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public ResponseEntity<GenericHttpResponse> contactUsHeader(@RequestBody UserRequest userRequest) {
        userProviderService.contactUsHeader(userRequest);
        return ResponseEntity.ok().body(new GenericHttpResponse("Contact us", "Thank you for contacting Amass Insights. <br>We will be in touch with you soon", "success"));
    }

    @RequestMapping(value = "/ur/user-status", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public ResponseEntity<Void> checkUserStatus(@RequestParam String loginID) {
        if(StrUtils.isBlank(loginID))
        {
            return new ResponseEntity("Please provide a valid login ID", HttpStatus.BAD_REQUEST);
        }
        User user = userService.getUser(loginID);
        if(user != null && !user.getActivated())
        {
            String message = "Your account has not yet been activated by the Amass admins. Please allow for at least 24 hours to verify your account. If you think this is an error, <a href='" + environment.getProperty("spring.application.url") +  "/#/contact-us'>Contact Us</a>";
            return new ResponseEntity(message, HttpStatus.OK);
        }
        return new ResponseEntity("Valid user", HttpStatus.OK);
    }

    @RequestMapping(value = "/ur/reset_key", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public ResponseEntity<User> checkResetKey(@RequestParam String resetKey) {
        User userDetails=new User();
        if(StrUtils.isBlank(resetKey))
        {
            return new ResponseEntity("Please provide a valid Reset Key", HttpStatus.BAD_REQUEST);
        }
        else{
         userDetails = userRepository.findByResetKey(resetKey);
        }

        if (userDetails==null){
            return new ResponseEntity("Invalid Key", HttpStatus.BAD_REQUEST);

        }

        return new ResponseEntity("Valid Key", HttpStatus.OK);

    }

    @GetMapping("/ur/newsletter-subscription")
    @Timed
    public ResponseEntity<GenericHttpResponse> subcriptionNewUser(@RequestParam(value = "key") String key) {

        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Subscription confirmation Error", "Missing Confirmation Key", "failed"));
        }
        userService.subscribeMailingList(key);

        return ResponseEntity.ok().body(new GenericHttpResponse("Subscription confirmation", "Your email address has been confirmed and you have been added to the newsletter.Thank you for your interest in Insights!","success"));
    }

    @GetMapping("/ur/newsletter-unsubscription")
    @Timed
    public ResponseEntity<GenericHttpResponse> unsubcriptionNewUser(@RequestParam(value = "key") String key) {

        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("UnSubscription confirmation Error", "Missing Confirmation Key", "failed"));
        }
        userService.unsubscribeMailingList(key);
        return ResponseEntity.ok().body(new GenericHttpResponse("Newsletter Unsubscription Confirmed", "Your email address has been unsubscribed from the newsletter. We're sorry to see you go! Please let us know if there's anything we could have improved to make your newsletter experience better!","success"));
    }
    
    @GetMapping("/ur/unsubscription")
    @Timed
    public ResponseEntity<GenericHttpResponse> unSubcription(@RequestParam(value = "key") String key) {

        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("UnSubscription confirmation Error", "Missing Confirmation Key", "failed"));
        }
        userService.unSubscriptionMailingList(key);

        return ResponseEntity.ok().body(new GenericHttpResponse("UnSubscription confirmation", "Your email address has been confirmed and you have been added to the newsletter.Thank you for your interest in Insights!","success"));
    }
    @RequestMapping(value = "/ur/qsearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public String news(@RequestBody String query, @RequestParam(value="type", required=true) String type, @RequestParam(value="lookupmodule", required=false) String lookupmodule, @RequestParam(value="sort", required=false) String sort) throws URISyntaxException {
        String queryBody = "";
        if (query == null) {
            queryBody = "{}";
        } else {
            queryBody = query;
        }
        JsonObject jsonObject = elasticsearchService.newsSearch(queryBody, type,lookupmodule, sort);
        return jsonObject.toString();
    }

    @RequestMapping(value = "/ur/eventsearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public String eventsearch() throws URISyntaxException {
        JsonObject jsonObject = elasticsearchService.eventsSearch(INDEX_DATA_EVENT);
        return jsonObject.toString();
    }

    @RequestMapping(value = "/ur/providersearch", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public String providerSearch(@RequestParam(value = "type", required = true) String type) throws URISyntaxException {
        JsonObject jsonObject = elasticsearchService.providerSearch(type);
        return jsonObject.toString();
    }
    @RequestMapping(value = "/ur/searchdata", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public String searchdata(@RequestBody String sort, @RequestParam(value = "type", required = true) String type) throws URISyntaxException {
    	JsonObject jsonObject = elasticsearchService.SearchDataProvider(sort,type);
        return jsonObject.toString();
    }
    @RequestMapping(value = "/ur/providerDetail", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public String searchdataProviderDetail(@RequestParam(value = "recordID", required = true) String type) throws URISyntaxException {
        JsonObject jsonObject = elasticsearchService.SearchDataProviderDetail(type);
        return jsonObject.toString();
    }
    @PostMapping("/ur/email-address")
    @Timed
    public ResponseEntity<EmailAddress> createEmailAddress(@Valid @RequestBody EmailAddress emailAddress) throws URISyntaxException {
        log.debug("REST request to save DataFeature : {}", emailAddress);
        if (emailAddress.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new emailAddress cannot already have an ID")).body(null);
        }
        EmailAddress emailID=emailAddressRepository.findByEmailAddress(emailAddress.getEmailAddress());
        User user=userRepository.findByLogin(emailAddress.getEmailAddress());
        if(emailID!=null)
        {
        if(!emailID.isSubscribedNewsletter())
        {
        	if(user!=null)
        	{
        	 userService.sentSubscriptionConfirmationEmail(user);
        	 user.setNotify_news_update(true);
        	 userRepository.save(user);
        	 emailID.setSubscribedNewsletter(true);
        	EmailAddress result= emailAddressRepository.save(emailID);
            // return new ResponseEntity("mailsent", HttpStatus.OK);
        	 return ResponseEntity.created(new URI("/ur/email-address/" + result.getId()))
        	            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
        	            .body(result);
        	}
        	else
        	{
        		EmailAddress result=emailAddressService.createEmailAddress(emailAddress);
        	       mailService.sendEmailAddressCreationEmail(result);
                   //return new ResponseEntity("mailsent", HttpStatus.OK);
        	       return ResponseEntity.created(new URI("/ur/email-address/" + result.getId()))
           	            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
           	            .body(result);
        	}
        }
        else
        {
            return new ResponseEntity("email address already in use", HttpStatus.BAD_REQUEST);

        }
        }
      /*  if (emailID!=null)
        {
            return new ResponseEntity("email address already in use", HttpStatus.BAD_REQUEST);
        }*/
        
       /* emailAddress.setSubscribedNewsletter(true);
        emailAddress.setRecordID("rece"+ Utils.randomString(13,13));
        emailAddress.setEmailAddress(emailAddress.getEmailAddress());
        EmailAddress result = emailAddressRepository.save(emailAddress);*/

       EmailAddress result=emailAddressService.createEmailAddress(emailAddress);
       mailService.sendEmailAddressCreationEmail(result);

        // dataFeatureSearchRepository.save(result);
        return ResponseEntity.created(new URI("/ur/email-address/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    @GetMapping("/ur/sign-up-newsletter-subscription")
    @Timed
    public ResponseEntity<GenericHttpResponse> signUpNewsLetter(@RequestParam(value = "key") String key) {

        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Subscription confirmation Error", "Missing Confirmation Key", "failed"));
        }
        emailAddressService.subscribeMailingList(key);

        return ResponseEntity.ok().body(new GenericHttpResponse("Subscription confirmation", "Your email address has been confirmed and you have been added to the newsletter.Thank you for your interest in Insights!","success"));
    }

    @GetMapping("/ur/unsubscriptionemailaddress")
    @Timed
    public ResponseEntity<GenericHttpResponse> unSubcriptionEmailAddress(@RequestParam(value = "key") String key) {

        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("UnSubscription confirmation Error", "Missing Confirmation Key", "failed"));
        }
        emailAddressService.unSubscriptionMailingList(key);

        return ResponseEntity.ok().body(new GenericHttpResponse("UnSubscription confirmation", "Your email address has been confirmed and you have been added to the newsletter.Thank you for your interest in Insights!","success"));
    }

    @GetMapping("/ur/activationUser")
    @Timed
    public ResponseEntity<?> activationUser(@RequestParam(value = "key")String key) throws URISyntaxException
    {
        User user= userRepository.findByActivationKey(key);
        if(null!=user)
        {
        	user.setConfirmed(true);
            user.setConfirmationKey(null);
        	userRepository.save(user);
        	EmailAddress emailAddress= emailAddressRepository.findByEmailAddress(user.getEmail());
        	emailAddress.setActive(true);
        	emailAddressRepository.save(emailAddress);
            user.setPassword(null);
            user.setNotify_news_update(true);
        }
        else
        {
            return new ResponseEntity<>("The link you are attempting to access is no longer active.", HttpStatus.INTERNAL_SERVER_ERROR);

        }        return ResponseEntity.created(new URI("/ur/activationUser/" + user.getId()))
        .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, user.getId().toString()))
        .body(user);
    }

    @PutMapping("/ur/userAccounts")
    @Timed
    public ResponseEntity<?> inviteUser(@Valid @RequestBody User user)
    {
        if(null!=user.getId())
        {
            User userAccount = userService.createinviteUserAccount(user);
            if(userAccount.getActivated())
            {
                mailService.inviteUserAccount(userAccount);

            }
        }
        
        else
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        }
        
        
        return new ResponseEntity<>(HttpStatus.ACCEPTED);

    }

    @RequestMapping(value = "/ur/pressNews", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    public String pressNews(@RequestParam(value = "press", required = false) boolean press) throws URISyntaxException {
        JsonObject jsonObject = elasticsearchService.pressNews(press);
        return jsonObject.toString();
    }

    @GetMapping("/ur/cancel-linkedIn")
    @Timed
    public ResponseEntity<?>cancelLinkedIn(@RequestParam(value = "key") String key)throws URISyntaxException
    {
        DataArticle article=dataArticleRepository.findByRecordID(key);
        if(article.getId()!=null)
        {
            dataArticleRepository.updateLinkedInCancel(article.getId());
        }
        LocalDate today = article.getPostDate();
        String postDate = today.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy"));
        return ResponseEntity.ok().body(new GenericHttpResponse("Social Post Cancellation", "The LinkedIn social post scheduled for "+ postDate +" has been successfully cancelled.","success"));

    }


    @GetMapping("/ur/cancel-twitter")
    @Timed
    public ResponseEntity<?>canceltwitter(@RequestParam(value = "key") String key) throws URISyntaxException
    {
        DataArticle article=dataArticleRepository.findByRecordID(key);
        if(article.getId()!=null)
        {
            dataArticleRepository.updateTwitterCancel(article.getId());
        }
        LocalDate today = article.getPostDate();
        String postDate = today.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy"));

        return ResponseEntity.ok().body(new GenericHttpResponse("Social Post Cancellation", "The Twitter social post scheduled for "+ postDate +" has been successfully cancelled.","success"));

    }
    
    @PostMapping("/ur/ipaddress")
    @Timed
    public ResponseEntity<?> userIpaddress(HttpServletRequest request) throws UnknownHostException
    {
    	 IpAddress ipAddressCheckCount=new IpAddress();
    	// InetAddress myIP=InetAddress.getLocalHost();
		 String ipAddressValue= getClientIpAddress(request);
		IpAddress ipAddressCheck=ipAddressRepository.findByIpAddressEquals(ipAddressValue);

		if(null!=ipAddressCheck)
		{
	    IpAddress ipAddressList=ipAddressRepository.findIpAddressGreater(ipAddressCheck.getIpAddress());

	          if(null!=ipAddressList)
	          {
	        	  ipAddressCheck.setCount(1);
			  	   Instant instant=Instant.now();
			  	ipAddressCheck.setWebUpdateDate(instant);
			ipAddressCheck.setActive(true);
			  	ipAddressRepository.save(ipAddressCheck);
	          }
	          else
	          {
			if(ipAddressCheck.isActive())
			{
				int count=ipAddressCheck.getCount()+1;
				ipAddressCheck.setCount(count);
				Instant instant=Instant.now();
		  	  	ipAddressCheck.setWebUpdateDate(instant);
				ipAddressRepository.save(ipAddressCheck);
			}
			else
			{
				ipAddressCheck.setCount(1);
		  	   Instant instant=Instant.now();
		  	ipAddressCheck.setWebUpdateDate(instant);
		ipAddressCheck.setActive(true);
		  	ipAddressRepository.save(ipAddressCheck);
			}
	          }
		}
		else
		{
		  IpAddress ipAddress=new IpAddress();
		  ipAddress.setIpAddress(ipAddressValue);
		ipAddress.setCount(1);
	  	   Instant instant=Instant.now();
	  	   ipAddress.setWebCreatedDate(instant);
	  	   ipAddress.setWebUpdateDate(instant);
	  	ipAddress.setActive(true);
	  	ipAddressRepository.save(ipAddress);
		}
		  ipAddressCheckCount =ipAddressRepository.findByIpAddressEquals(ipAddressValue);
          return new ResponseEntity<>(ipAddressCheckCount,HttpStatus.ACCEPTED);
    	
    	
    }
   
    
    public static String getClientIpAddress(HttpServletRequest request) {
        String xForwardedForHeader = request.getHeader("X-Forwarded-For");
        if (xForwardedForHeader == null) {
            return request.getRemoteAddr();
        } else {
            // As of https://en.wikipedia.org/wiki/X-Forwarded-For
            // The general format of the field is: X-Forwarded-For: client, proxy1, proxy2 ...
            // we only want the client
            return new StringTokenizer(xForwardedForHeader, ",").nextToken().trim();
        }
    }
    
  /*  @PostMapping("/ur/ipaddresscron")
    @Timed
    @Transactional
    @Scheduled(cron = "0 0 0 * * ?")
    protected void getIpAddress() {
    	System.out.println("cron called");
    	List<IpAddress> ipAddressList=ipAddressRepository.findIpAddressGreater();
    	for(IpAddress ipAddress:ipAddressList)
    	{
    		ipAddress.setActive(false);
    		ipAddressRepository.save(ipAddress);
    	}
    }*/
 
}
