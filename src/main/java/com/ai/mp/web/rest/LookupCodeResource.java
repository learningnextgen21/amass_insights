package com.ai.mp.web.rest;

import com.ai.mp.domain.User;
import com.ai.mp.service.UserService;
import com.codahale.metrics.annotation.Timed;
import com.ai.mp.domain.LookupCode;

import com.ai.mp.repository.LookupCodeRepository;
import com.ai.mp.repository.search.LookupCodeSearchRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing LookupCode.
 */
@RestController
@RequestMapping("/api")
public class LookupCodeResource {

    private final Logger log = LoggerFactory.getLogger(LookupCodeResource.class);
    private final UserService userService;

    private static final String ENTITY_NAME = "lookupCode";

    private final LookupCodeRepository lookupCodeRepository;

    private final LookupCodeSearchRepository lookupCodeSearchRepository;
    public LookupCodeResource(LookupCodeRepository lookupCodeRepository, LookupCodeSearchRepository lookupCodeSearchRepository,UserService userService) {
        this.lookupCodeRepository = lookupCodeRepository;
        this.lookupCodeSearchRepository = lookupCodeSearchRepository;
        this.userService = userService;
    }

    /**
     * POST  /lookup-codes : Create a new lookupCode.
     *
     * @param lookupCode the lookupCode to create
     * @return the ResponseEntity with status 201 (Created) and with body the new lookupCode, or with status 400 (Bad Request) if the lookupCode has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/lookup-codes")
    @Timed
    public ResponseEntity<LookupCode> createLookupCode(@Valid @RequestBody LookupCode lookupCode) throws URISyntaxException {
        log.debug("REST request to save LookupCode : {}", lookupCode);
        if (lookupCode.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new lookupCode cannot already have an ID")).body(null);
        }
        LookupCode result = lookupCodeRepository.save(lookupCode);
        lookupCodeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/lookup-codes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /lookup-codes : Updates an existing lookupCode.
     *
     * @param lookupCode the lookupCode to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated lookupCode,
     * or with status 400 (Bad Request) if the lookupCode is not valid,
     * or with status 500 (Internal Server Error) if the lookupCode couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/lookup-codes")
    @Timed
    public ResponseEntity<LookupCode> updateLookupCode(@Valid @RequestBody LookupCode lookupCode) throws URISyntaxException {
        log.debug("REST request to update LookupCode : {}", lookupCode);
        if (lookupCode.getId() == null) {
            return createLookupCode(lookupCode);
        }
        LookupCode result = lookupCodeRepository.save(lookupCode);
        lookupCodeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, lookupCode.getId().toString()))
            .body(result);
    }

    /**
     * GET  /lookup-codes : get all the lookupCodes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of lookupCodes in body
     */
    @GetMapping("/lookup-codes")
    @Timed
    public ResponseEntity<List<LookupCode>> getAllLookupCodes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of LookupCodes");
        Page<LookupCode> page = lookupCodeRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/lookup-codes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /lookup-codes/:id : get the "id" lookupCode.
     *
     * @param id the id of the lookupCode to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the lookupCode, or with status 404 (Not Found)
     */
    @GetMapping("/lookup-codes/{id}")
    @Timed
    public ResponseEntity<LookupCode> getLookupCode(@PathVariable Long id) {
        log.debug("REST request to get LookupCode : {}", id);
        LookupCode lookupCode = lookupCodeRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lookupCode));
    }

    /**
     * DELETE  /lookup-codes/:id : delete the "id" lookupCode.
     *w
     * @param id the id of the lookupCode to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/lookup-codes/{id}")
    @Timed
    public ResponseEntity<Void> deleteLookupCode(@PathVariable Long id) {
        log.debug("REST request to delete LookupCode : {}", id);
        lookupCodeRepository.delete(id);
        lookupCodeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/lookup-codes?query=:query : search for the lookupCode corresponding
     * to the query.
     *
     * @param query the query of the lookupCode search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/lookup-codes")
    @Timed
    public ResponseEntity<List<LookupCode>> searchLookupCodes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of LookupCodes for query {}", query);
        Page<LookupCode> page = lookupCodeSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/lookup-codes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/lookup-code-creation")
    @Timed
    public ResponseEntity<LookupCode> createNewLookupCode(@Valid @RequestBody LookupCode lookupCode) throws URISyntaxException {
        log.debug("REST request to save LookupCode : {}", lookupCode);
        if (lookupCode.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new lookupCode cannot already have an ID")).body(null);
        }
        List<LookupCode> lookUpCode=lookupCodeRepository.findBylookupmoduleEquals(lookupCode.getLookupmodule());
        for(LookupCode lookUp:lookUpCode)
        {
        	if(lookupCode.getLookupcode().equalsIgnoreCase(lookUp.getLookupcode()))
        	{
        		return new ResponseEntity("Lookupcode already in use",HttpStatus.CONFLICT);
        		// return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "lookupCodeexists", "A new lookupCode cannot already have an lookupcode")).body(null);
        	}
        }
        LookupCode look=new LookupCode();
        User user = userService.getUser();
        look.setLookupmodule(lookupCode.getLookupmodule());
        look.setCreatedby(user.getId().intValue());
        look.setLookupcode(lookupCode.getLookupcode());
        look.setDescription(lookupCode.getDescription());
        LookupCode result = lookupCodeRepository.save(look);
        lookupCodeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/lookup-code-creation/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @GetMapping("/lookup-code/{value}")
    @Timed
    public ResponseEntity<LookupCode> getLookupCodeValue(@PathVariable String value) {
		
    	LookupCode lookupCode= lookupCodeRepository.findByLookupcode(value);
    	
    	return new ResponseEntity<LookupCode>(lookupCode, HttpStatus.OK);
    	
    	
    }
    
    
}
