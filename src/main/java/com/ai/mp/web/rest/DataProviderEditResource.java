package com.ai.mp.web.rest;

import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ai.mp.batch.DBUtils;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.DataProviderEdit;
import com.ai.mp.domain.LookupCode;
import com.ai.mp.domain.User;
import com.ai.mp.repository.DataProviderEditRepository;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.repository.LookupCodeRepository;
import com.ai.mp.service.DataProviderEditService;
import com.ai.mp.service.MailService;
import com.ai.mp.service.UserService;
import com.ai.mp.utils.BasicBean;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import io.github.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class DataProviderEditResource {

    private final DataProviderEditRepository dataProviderEditRepository;
    private final DataProviderRepository dataProviderRepository;
    private final LookupCodeRepository lookupcodeRepository;
    private final DBUtils dbutils;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MailService mailService;

    @Autowired
    UserService userService;

    @Autowired
    DataProviderEditService dataProviderEditService;
    
    public DataProviderEditResource(DataProviderEditRepository dataProviderEditRepository,DataProviderRepository dataProviderRepository,LookupCodeRepository lookupcodeRepository,DBUtils dbutils) {
        this.dataProviderEditRepository = dataProviderEditRepository;
        this.dataProviderRepository=dataProviderRepository;
        this.lookupcodeRepository=lookupcodeRepository;
        this.dbutils=dbutils;
    }


    @PostMapping(path = "/provider_profile_edit",
        produces={MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    public ResponseEntity<DataProviderEdit> providersEdit(@RequestBody DataProviderEdit dataProviderEdit,@RequestParam(value="formName", required=false) String formName,BindingResult results,HttpSession session) throws URISyntaxException, IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
     //   System.out.println("formName!!!"+formName);
     //   System.out.println("recordId,"+dataProviderEdit.getRecordID());
        User user = userService.getUser();
        DataProviderEdit result = new DataProviderEdit();
        DataProvider provider=new DataProvider();
        dataProviderEdit.setRecordUpdated(true);
        // DataProviderEdit recordBind = dataProviderEditRepository.findByRecordID(dataProviderEdit.getRecordID());
       // DataProviderEdit data = dataProviderEditRepository.findByRecordIDEqualsAndEditorUserIdEquals(dataProviderEdit.getRecordID(),user.getId());
        DataProviderEdit data = dataProviderEditRepository.findByrecordUpdated(dataProviderEdit.getRecordID(),user.getId());
        dataProviderEdit.setEditorUserId(user.getId());
        if(data!=null)
        {
        	if(data.getRecordUpdated())
        	{
            dataProviderEdit.setId(data.getId());
        	}

            if(null ==session.getAttribute("id"))
            {
            session.setAttribute("id", data);
            }}
        DataProviderEdit providerEdit=new DataProviderEdit();
DataProviderEdit dataProvider=(DataProviderEdit) session.getAttribute("id");
//System.out.println("inds"+dataProviderEdit.getIndustries().size());
        //String dataProviderJson = objectMapper.writeValueAsString(dataProviderEdit);
        //objectMapper.readerForUpdating(recordBind).readValue(dataProviderJson);
        if(formName.equalsIgnoreCase("Form"))
        {
            dataProviderEdit.setEditorUserId(user.getId());

            DataProvider dataProviders=dataProviderRepository.findOneIdWithEagerRelationships(dataProviderEdit.getRecordID());

            //DataProviderEdit dataProvider=dataProviderEditRepository.findByRecordIDEqualsAndEditorUserIdEquals(dataProviderEdit.getRecordID(),user.getId());
            
            if(dataProviderEdit.getId()!=null)
            {
                boolean setShortDescription=Objects.equals(dataProvider.getShortDescription(), dataProviderEdit.getShortDescription());
                if(!setShortDescription)
                {
                    providerEdit.setShortDescription(dataProviderEdit.getShortDescription());

                }

                boolean setLongDescription=Objects.equals(dataProvider.getLongDescription(), dataProviderEdit.getLongDescription());
                if(!setLongDescription)
                {
                	if(null!=dataProviderEdit.getLongDescription())
                	{
                        providerEdit.setLongDescription(dataProviderEdit.getLongDescription().replaceAll("\\<[^>]*>",""));
                	}
                }

                boolean setNotes=Objects.equals(dataProvider.getNotes(), dataProviderEdit.getNotes());
                if(!setNotes)
                {
                    providerEdit.setNotes(dataProviderEdit.getNotes());

                }

                boolean a=Objects.equals(dataProvider.getDataUpdateFrequency(), dataProviderEdit.getDataUpdateFrequency());
                if(!a)
                {
                    providerEdit.setDataUpdateFrequency(dataProviderEdit.getDataUpdateFrequency());

                }

                boolean setDataUpdateFrequencyNotes=Objects.equals(dataProvider.getDataUpdateFrequencyNotes(), dataProviderEdit.getDataUpdateFrequencyNotes());
                if(!setDataUpdateFrequencyNotes)
                {
                    providerEdit.setDataUpdateFrequencyNotes(dataProviderEdit.getDataUpdateFrequencyNotes());

                }
            	/*if(!dataProvider.getDateCollectionBegan().toString().equalsIgnoreCase(dataProviderEdit.getDateCollectionBegan()))

            	{
            	providerEdit.setDateCollectionBegan(dataProviderEdit.getDateCollectionBegan());
            	}*/
                boolean setDateCollectionRangeExplanation=Objects.equals(dataProvider.getDateCollectionRangeExplanation(), dataProviderEdit.getDateCollectionRangeExplanation());
                if(!setDateCollectionRangeExplanation)
                {
                    providerEdit.setDateCollectionRangeExplanation(dataProviderEdit.getDateCollectionRangeExplanation());

                }

                boolean setCollectionMethodsExplanation=Objects.equals(dataProvider.getCollectionMethodsExplanation(), dataProviderEdit.getCollectionMethodsExplanation());
                if(!setCollectionMethodsExplanation)
                {
                    providerEdit.setCollectionMethodsExplanation(dataProviderEdit.getCollectionMethodsExplanation());

                }
                boolean setPotentialDrawbacks=Objects.equals(dataProvider.getPotentialDrawbacks(), dataProviderEdit.getPotentialDrawbacks());
                if(!setPotentialDrawbacks)
                {
                    providerEdit.setPotentialDrawbacks(dataProviderEdit.getPotentialDrawbacks());

                }
                boolean setKeyDataFields=Objects.equals(dataProvider.getKeyDataFields(), dataProviderEdit.getKeyDataFields());
                if(!setKeyDataFields)
                {
                	if(null!=dataProviderEdit.getKeyDataFields())
                	{
                        providerEdit.setKeyDataFields(dataProviderEdit.getKeyDataFields().replaceAll("\\<[^>]*>",""));
                	}

                }

                boolean setSummary=Objects.equals(dataProvider.getSummary(), dataProviderEdit.getSummary());
                if(!setSummary)
                {
                    providerEdit.setSummary(dataProviderEdit.getSummary());

                }

                boolean setInvestor_clients=Objects.equals(dataProvider.getInvestor_clients(), dataProviderEdit.getInvestor_clients());
                if(!setInvestor_clients)
                {
                    providerEdit.setInvestor_clients(dataProviderEdit.getInvestor_clients());

                }


                boolean setPublicCompaniesCovered=Objects.equals(dataProvider.getPublicCompaniesCovered(), dataProviderEdit.getPublicCompaniesCovered());
                if(!setPublicCompaniesCovered)
                {
                    providerEdit.setPublicCompaniesCovered(dataProviderEdit.getPublicCompaniesCovered());

                }
                boolean setFreeTrialAvailable=Objects.equals(dataProvider.getFreeTrialAvailable(), dataProviderEdit.getFreeTrialAvailable());
                if(!setFreeTrialAvailable)
                {
                    providerEdit.setFreeTrialAvailable(dataProviderEdit.getFreeTrialAvailable());

                }
                boolean setProductDetails=Objects.equals(dataProvider.getProductDetails(), dataProviderEdit.getProductDetails());
                if(!setProductDetails)
                {
                	if(null!=dataProviderEdit.getProductDetails())
                	{
                        providerEdit.setProductDetails(dataProviderEdit.getProductDetails().replaceAll("\\<[^>]*>",""));
                	}

                }
            /*	if(!dataProvider.getProductDetails().equalsIgnoreCase(dataProviderEdit.getProductDetails()))
            	{
            		providerEdit.setProductDetails(dataProviderEdit.getProductDetails());
            	}
            	if(!dataProvider.getMainDataCategory().getId().toString().equalsIgnoreCase(dataProviderEdit.getMainDataCategory().getId().toString()))
            	{
            		providerEdit.setMainDataCategory(dataProviderEdit.getMainDataCategory());
            	}
            	if(!dataProvider.getMainDataIndustry().getId().toString().equalsIgnoreCase(dataProviderEdit.getMainDataIndustry().getId().toString()))

            	{
            	providerEdit.setMainDataIndustry(dataProviderEdit.getMainDataIndustry());
            	}*/


             /*   boolean b=Objects.equals(dataProvider.getScoreInvestorsWillingness(), dataProviderEdit.getScoreInvestorsWillingness());
                if(!b)
                {
                    providerEdit.setScoreInvestorsWillingness(dataProviderEdit.getScoreInvestorsWillingness());

                }*/
                boolean setscoreInvestorWillingness=Objects.equals(dataProvider.getScoreInvestorsWillingness(), dataProviderEdit.getScoreInvestorsWillingness());
                if(!setscoreInvestorWillingness)
                {
                    //providerEdit.setPointInTimeAccuracy(dataProviderEdit.getPointInTimeAccuracy());
                    if(dataProviderEdit.getScoreInvestorsWillingness()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getScoreInvestorsWillingness().longValue());
                        if(lookupcode!=null)
                        {
                            provider.setScoreInvestorsWillingness(lookupcode);
                        }
                    }
                }
                /*boolean c=Objects.equals(dataProvider.getPricingModels(), dataProviderEdit.getPricingModels());
                if(!c)
                {
                    providerEdit.setDataUpdateFrequency(dataProviderEdit.getDataUpdateFrequency());

                }*/
                boolean d=Objects.equals(dataProvider.getUserPermission(), dataProviderEdit.getUserPermission());
                if(!d)
                {
                    providerEdit.setUserPermission(dataProviderEdit.getUserPermission());

                }
                boolean sample=Objects.equals(dataProvider.getSampleOrPanelSize(), dataProviderEdit.getSampleOrPanelSize());
                if(!sample)
                {
                    providerEdit.setSampleOrPanelSize(dataProviderEdit.getSampleOrPanelSize());

                }
                boolean useCase=Objects.equals(dataProvider.getUseCasesOrQuestionsAddressed(), dataProviderEdit.getUseCasesOrQuestionsAddressed());
                if(!useCase)
                {
                	if(null!=dataProviderEdit.getUseCasesOrQuestionsAddressed())
                	{
                        providerEdit.setUseCasesOrQuestionsAddressed(dataProviderEdit.getUseCasesOrQuestionsAddressed().replaceAll("\\<[^>]*>",""));	
                	}

                }
                boolean dataLicense=Objects.equals(dataProvider.getDataLicenseTypeDetails(), dataProviderEdit.getDataLicenseTypeDetails());
                if(!dataLicense)
                {
                    providerEdit.setDataLicenseTypeDetails(dataProviderEdit.getDataLicenseTypeDetails());

                }
                boolean setDataRoadmap=Objects.equals(dataProvider.getDataRoadmap(), dataProviderEdit.getDataRoadmap());
                if(!setDataRoadmap)
                {
                    providerEdit.setDataRoadmap(dataProviderEdit.getDataRoadmap());

                }
                boolean setDeliveryFrequencyNotes=Objects.equals(dataProvider.getDeliveryFrequencyNotes(), dataProviderEdit.getDeliveryFrequencyNotes());
                if(!setDeliveryFrequencyNotes)
                {
                    providerEdit.setDeliveryFrequencyNotes(dataProviderEdit.getDeliveryFrequencyNotes());

                }
                boolean setOutOfSampleData=Objects.equals(dataProvider.getOutOfSampleData(), dataProviderEdit.getOutOfSampleData());
                if(!setOutOfSampleData)
                {
                    providerEdit.setOutOfSampleData(dataProviderEdit.getOutOfSampleData());

                }
                boolean setTrialDuration=Objects.equals(dataProvider.getTrialDuration(), dataProviderEdit.getTrialDuration());
                if(!setTrialDuration)
                {
                    providerEdit.setTrialDuration(dataProviderEdit.getTrialDuration());

                }
                boolean setCity=Objects.equals(dataProvider.getCity(), dataProviderEdit.getCity());
                if(!setCity)
                {
                    providerEdit.setCity(dataProviderEdit.getCity());

                }
                boolean setState=Objects.equals(dataProvider.getState(), dataProviderEdit.getState());
                if(!setState)
                {
                    providerEdit.setState(dataProviderEdit.getState());

                }
                boolean setCountry=Objects.equals(dataProvider.getCountry(), dataProviderEdit.getCountry());
                if(!setCountry)
                {
                    providerEdit.setCountry(dataProviderEdit.getCountry());

                }
                boolean setZipCode=Objects.equals(dataProvider.getZipCode(), dataProviderEdit.getZipCode());
                if(!setZipCode)
                {
                    providerEdit.setZipCode(dataProviderEdit.getZipCode());

                }
                boolean setYearFounded=Objects.equals(dataProvider.getYearFounded(), dataProviderEdit.getYearFounded());
                if(!setYearFounded)
                {
                    providerEdit.setYearFounded(dataProviderEdit.getYearFounded());

                }
                boolean setHeadCount=Objects.equals(dataProvider.getHeadCount(), dataProviderEdit.getHeadCount());
                if(!setHeadCount)
                {
                    providerEdit.setHeadCount(dataProviderEdit.getHeadCount());

                }
                boolean setPublicEquitesCovered=Objects.equals(dataProvider.getPublicEquitiesCoveredCount(), dataProviderEdit.getPublicEquitiesCoveredCount());
                if(!setPublicEquitesCovered)
                {
                    providerEdit.setPublicEquitiesCoveredCount(dataProviderEdit.getPublicEquitiesCoveredCount());

                }
                boolean setPublicEquitesCoveredRange=Objects.equals(dataProvider.getPublicEquitiesCoveredRange(), dataProviderEdit.getPublicEquitiesCoveredRange());
                if(!setPublicEquitesCoveredRange)
                {
                    providerEdit.setPublicEquitiesCoveredRange(dataProviderEdit.getPublicEquitiesCoveredRange());

                }
                boolean setMainAssetClass=Objects.equals(dataProvider.getMainAssetClass(), dataProviderEdit.getMainAssetClass());
                if(!setMainAssetClass)
                {
                    providerEdit.setMainAssetClass(dataProviderEdit.getMainAssetClass());

                }
                boolean setSecRegulated=Objects.equals(dataProvider.getSecRegulated(), dataProviderEdit.getSecRegulated());
                if(!setSecRegulated)
                {
                    //providerEdit.setSecRegulated(dataProviderEdit.getSecRegulated());
                    if(dataProviderEdit.getSecRegulated()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getSecRegulated());
                        provider.setSecRegulated(lookupcode);
                    }
                }
                boolean setDataLicenceType=Objects.equals(dataProvider.getDataLicenseType(), dataProviderEdit.getDataLicenseType());
                if(!setDataLicenceType)
                {
                    //providerEdit.setSecRegulated(dataProviderEdit.getSecRegulated());
                    if(dataProviderEdit.getDataLicenseType()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getDataLicenseType());
                        provider.setDataLicenseType(lookupcode);
                    }
                }
                boolean setDuplicatesCleaned=Objects.equals(dataProvider.getDuplicatesCleaned(), dataProviderEdit.getDuplicatesCleaned());
                if(!setDuplicatesCleaned)
                {
                    //providerEdit.setSecRegulated(dataProviderEdit.getSecRegulated());
                    if(dataProviderEdit.getDuplicatesCleaned()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getDuplicatesCleaned());
                        provider.setDuplicatesCleaned(lookupcode);
                    }
                }
                
                /*boolean setProviderType=Objects.equals(dataProvider.getProviderType().getId(), dataProviderEdit.getProviderType().getId());
                {
                	if(!setProviderType)
                	{
                		if(dataProviderEdit.getProviderType()!=null)
                		{
                			DataProviderType providers=dataProviderTypeRepository.findOne(dataProviderEdit.getProviderType().getId());
                			if(providers!=null)
                			{
                				provider.setProviderType(providers);
                			}
                		}
                	}
                }*/

                /*if(!dataProvider.getProviderType().getId().toString().equalsIgnoreCase(dataProviderEdit.getProviderType().getId().toString())) {
            		providerEdit.setProviderType(dataProviderEdit.getProviderType());
            	}*/
                boolean setInvestorClientsCount=Objects.equals(dataProvider.getInvestorClientsCount(), dataProviderEdit.getInvestorClientsCount());
                if(!setInvestorClientsCount)
                {
                    providerEdit.setInvestorClientsCount(dataProviderEdit.getInvestorClientsCount());

                }
                boolean setTrialAgreementType=Objects.equals(dataProvider.getTrialAgreementType(), dataProviderEdit.getTrialAgreementType());
                if(!setTrialAgreementType)
                {
                    //providerEdit.setTrialAgreementType(dataProviderEdit.getTrialAgreementType());
                    if(dataProviderEdit.getTrialAgreementType()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getTrialAgreementType());
                        if(lookupcode!=null)
                        {
                            provider.setTrialAgreementType(lookupcode);
                        }
                    }
                }
                boolean setTrialPricingModel=Objects.equals(dataProvider.getTrialPricingModel(), dataProviderEdit.getTrialPricingModel());
                if(!setTrialPricingModel)
                {
                    //providerEdit.setTrialPricingModel(dataProviderEdit.getTrialPricingModel());
                    if(dataProviderEdit.getTrialPricingModel()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getTrialPricingModel());
                        if(lookupcode!=null)
                        {

                            provider.setTrialPricingModel(lookupcode);
                        }
                    }
                }
                boolean setTrialPricingDetails=Objects.equals(dataProvider.getTrialPricingDetails(), dataProviderEdit.getTrialPricingDetails());
                if(!setTrialPricingDetails)
                {
                    providerEdit.setTrialPricingDetails(dataProviderEdit.getTrialPricingDetails());

                }
                boolean setPricingDetails=Objects.equals(dataProvider.getPricingDetails(), dataProviderEdit.getPricingDetails());
                if(!setPricingDetails)
                {
                	//System.out.println("setPricingDetails"+setPricingDetails+" "+dataProviderEdit.getPricingDetails());
                    providerEdit.setPricingDetails(dataProviderEdit.getPricingDetails());

                }
                boolean setLagTime=Objects.equals(dataProvider.getLagTime(), dataProviderEdit.getLagTime());
                if(!setLagTime)
                {
                    providerEdit.setLagTime(dataProviderEdit.getLagTime());

                }
                boolean setCompetitiveDifferentiators=Objects.equals(dataProvider.getCompetitiveDifferentiators(), dataProviderEdit.getCompetitiveDifferentiators());
                if(!setCompetitiveDifferentiators)
                {
                    providerEdit.setCompetitiveDifferentiators(dataProviderEdit.getCompetitiveDifferentiators());

                }

                boolean setDiscussionAnalytics=Objects.equals(dataProvider.getDiscussionAnalytics(), dataProviderEdit.getDiscussionAnalytics());
                if(!setDiscussionAnalytics)
                {
                    providerEdit.setDiscussionAnalytics(dataProviderEdit.getDiscussionAnalytics());

                }
                boolean setDiscussionCleanliness=Objects.equals(dataProvider.getDiscussionCleanliness(), dataProviderEdit.getDiscussionCleanliness());
                if(!setDiscussionCleanliness)
                {
                    providerEdit.setDiscussionCleanliness(dataProviderEdit.getDiscussionCleanliness());

                }
                boolean setSampleOrPanelBiases=Objects.equals(dataProvider.getSampleOrPanelBiases(), dataProviderEdit.getSampleOrPanelBiases());
                if(!setSampleOrPanelBiases)
                {
                    providerEdit.setSampleOrPanelBiases(dataProviderEdit.getSampleOrPanelBiases());

                }
                boolean setDatasetSize=Objects.equals(dataProvider.getDatasetSize(), dataProviderEdit.getDatasetSize());
                if(!setDatasetSize)
                {
                    providerEdit.setDatasetSize(dataProviderEdit.getDatasetSize());

                }
                boolean setDatasetNumberOfRows=Objects.equals(dataProvider.getDatasetNumberOfRows(), dataProviderEdit.getDatasetNumberOfRows());
                if(!setDatasetNumberOfRows)
                {
                    providerEdit.setDatasetNumberOfRows(dataProviderEdit.getDatasetNumberOfRows());

                }
                boolean setNumberOfAnalystEmployees=Objects.equals(dataProvider.getNumberOfAnalystEmployees(), dataProviderEdit.getNumberOfAnalystEmployees());
                if(!setNumberOfAnalystEmployees)
                {
                    providerEdit.setNumberOfAnalystEmployees(dataProviderEdit.getNumberOfAnalystEmployees());

                }
                boolean setPointInTimeAccuracy=Objects.equals(dataProvider.getPointInTimeAccuracy(), dataProviderEdit.getPointInTimeAccuracy());
                if(!setPointInTimeAccuracy)
                {
                    //providerEdit.setPointInTimeAccuracy(dataProviderEdit.getPointInTimeAccuracy());
                    if(dataProviderEdit.getPointInTimeAccuracy()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getPointInTimeAccuracy());
                        if(lookupcode!=null)
                        {
                            provider.setPointInTimeAccuracy(lookupcode);
                        }
                    }
                }
                boolean setDataSourcesDetails=Objects.equals(dataProvider.getDataSourcesDetails(), dataProviderEdit.getDataSourcesDetails());
                if(!setDataSourcesDetails)
                {
                    providerEdit.setDataSourcesDetails(dataProviderEdit.getDataSourcesDetails());

                }
                boolean setDataGaps=Objects.equals(dataProvider.getDataGaps(), dataProviderEdit.getDataGaps());
                if(!setDataGaps)
                {
                    //providerEdit.setDataGaps(dataProviderEdit.getDataGaps());
                    if(dataProviderEdit.getDataGaps()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getDataGaps());
                        if(lookupcode!=null)
                        {
                            provider.setDataGaps(lookupcode);
                        }
                    }
                }
                boolean setIncludesOutliers=Objects.equals(dataProvider.getIncludesOutliers(), dataProviderEdit.getIncludesOutliers());
                if(!setIncludesOutliers)
                {
                    //providerEdit.setIncludesOutliers(dataProviderEdit.getIncludesOutliers());
                    if(dataProviderEdit.getIncludesOutliers()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getIncludesOutliers());
                        if(lookupcode!=null)
                        {
                            provider.setIncludesOutliers(lookupcode);
                        }
                    }
                }
                boolean setNormalized=Objects.equals(dataProvider.getNormalized(), dataProviderEdit.getNormalized());
                if(!setNormalized)
                {
                    //providerEdit.setNormalized(dataProviderEdit.getNormalized());
                    if(dataProviderEdit.getNormalized()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getNormalized());
                        if(lookupcode!=null)
                        {
                            provider.setNormalized(lookupcode);
                        }
                    }
                }
                boolean setInvestorClientBucket=Objects.equals(dataProvider.getInvestorClientBucket(), dataProviderEdit.getInvestorClientBucket());
                if(!setInvestorClientBucket)
                {
                    providerEdit.setInvestorClientBucket(dataProviderEdit.getInvestorClientBucket());

                }
                boolean setSubscriptionTimePeriod=Objects.equals(dataProvider.getSubscriptionTimePeriod(), dataProviderEdit.getSubscriptionTimePeriod());
                if(!setSubscriptionTimePeriod)
                {
                    //providerEdit.setSubscriptionTimePeriod(dataProviderEdit.getSubscriptionTimePeriod());
                    if(dataProviderEdit.getSubscriptionTimePeriod()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getSubscriptionTimePeriod());
                        if(lookupcode!=null)
                        {
                            provider.setSubscriptionTimePeriod(lookupcode);
                        }
                    }
                }
                boolean setYearFirstDataProductLaunched=Objects.equals(dataProvider.getYearFirstDataProductLaunched(), dataProviderEdit.getYearFirstDataProductLaunched());
                if(!setYearFirstDataProductLaunched)
                {
                    providerEdit.setYearFirstDataProductLaunched(dataProviderEdit.getYearFirstDataProductLaunched());

                }
                boolean setMonthlyPriceRange=Objects.equals(dataProvider.getMonthlyPriceRange(), dataProviderEdit.getMonthlyPriceRange());
                if(!setMonthlyPriceRange)
                {
                    //providerEdit.setMonthlyPriceRange(dataProviderEdit.getMonthlyPriceRange());
                    if(dataProviderEdit.getMonthlyPriceRange()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getMonthlyPriceRange());
                        if(lookupcode!=null)
                        {
                            provider.setMonthlyPriceRange(lookupcode);
                        }
                    }
                }
                boolean setSubscriptionModel=Objects.equals(dataProvider.getSubscriptionModel(), dataProviderEdit.getSubscriptionModel());
                if(!setSubscriptionModel)
                {
                    //providerEdit.setSubscriptionModel(dataProviderEdit.getSubscriptionModel());
                    if(dataProviderEdit.getSubscriptionModel()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getSubscriptionModel());
                        if(lookupcode!=null)
                        {
                            provider.setSubscriptionModel(lookupcode);
                        }
                    }
                }
                boolean setMinimumYearlyPrice=Objects.equals(dataProvider.getMinimumYearlyPrice(), dataProviderEdit.getMinimumYearlyPrice());
                if(!setMinimumYearlyPrice)
                {
                    providerEdit.setMinimumYearlyPrice(dataProviderEdit.getMinimumYearlyPrice());

                }
                boolean setMaximumYearlyPrice=Objects.equals(dataProvider.getMaximumYearlyPrice(), dataProviderEdit.getMaximumYearlyPrice());
                if(!setMaximumYearlyPrice)
                {
                    providerEdit.setMaximumYearlyPrice(dataProviderEdit.getMaximumYearlyPrice());

                }
                boolean setNumberOfDataSources=Objects.equals(dataProvider.getNumberOfDataSources(), dataProviderEdit.getNumberOfDataSources());
                if(!setNumberOfDataSources)
                {
                    //providerEdit.setNumberOfDataSources(dataProviderEdit.getNumberOfDataSources());
                    if(dataProviderEdit.getNumberOfDataSources()!=null)
                    {
                        LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getNumberOfDataSources());
                        if(lookupcode!=null)
                        {
                            provider.setNumberOfDataSources(lookupcode);
                        }
                    }
                }

                boolean setPrivacy=Objects.equals(dataProvider.getPrivacy(), dataProviderEdit.getPrivacy());
                if(!setPrivacy)
                {
                    providerEdit.setPrivacy(dataProviderEdit.getPrivacy());

                }
                boolean setYearExit=Objects.equals(dataProvider.getYearExit(), dataProviderEdit.getYearExit());
                if(!setYearExit)
                {
                    providerEdit.setYearExit(dataProviderEdit.getYearExit());

                }
                boolean setFinancialPosition=Objects.equals(dataProvider.getFinancialPosition(), dataProviderEdit.getFinancialPosition());
                if(!setFinancialPosition)
                {
                    providerEdit.setFinancialPosition(dataProviderEdit.getFinancialPosition());

                }
                boolean setClientBase=Objects.equals(dataProvider.getClientBase(), dataProviderEdit.getClientBase());
                if(!setClientBase)
                {
                    providerEdit.setClientBase(dataProviderEdit.getClientBase());

                }

                boolean setHeadcountNumber=Objects.equals(dataProvider.getHeadcountNumber(), dataProviderEdit.getHeadcountNumber());
                if(!setHeadcountNumber)
                {
                    providerEdit.setHeadcountNumber(dataProviderEdit.getHeadcountNumber());

                }
                
               /* boolean setHeadcountBucket=Objects.equals(dataProvider.getHeadcountBucket(), dataProviderEdit.getHeadcountBucket());
                if(!setHeadcountBucket)
                {
                    providerEdit.setHeadcountBucket(dataProviderEdit.getHeadcountBucket());

                }*/
               /* boolean setPublicCompaniesCoveredNum=Objects.equals(dataProvider.getPublicCompaniesCoveredNum(), dataProviderEdit.getPublicCompaniesCoveredNum());
                if(!setPublicCompaniesCoveredNum)
                {
                    providerEdit.setPublicCompaniesCoveredNum(dataProviderEdit.getPublicCompaniesCoveredNum());

                }*/
              /*  boolean setProviderName=Objects.equals(dataProvider.getProviderName(), dataProviderEdit.getProviderName());
                if(!setProviderName)
                {
                    providerEdit.setProviderName(dataProviderEdit.getProviderName());

                }
                boolean setWebsite=Objects.equals(dataProvider.getWebsite(), dataProviderEdit.getWebsite());
                if(!setWebsite)
                {
                    providerEdit.setWebsite(dataProviderEdit.getWebsite());

                }*/
                if(!dataProviders.getProviderName().equalsIgnoreCase(dataProviderEdit.getProviderName()))
                {
                    providerEdit.setProviderName(dataProviderEdit.getProviderName());
                }
                if(!dataProviders.getWebsite().equalsIgnoreCase(dataProviderEdit.getWebsite()))
                {
                    providerEdit.setWebsite(dataProviderEdit.getWebsite());
                }
                if(null!=dataProviderEdit.getProviderType()&& null!=dataProviderEdit.getProviderType().getProviderType())
                {
                	//System.out.println("ind"+dataProviderEdit.getProviderType().getProviderType());
                    providerEdit.setProviderType(dataProviderEdit.getProviderType());;
                }
                if(null!=dataProviderEdit.getMainDataIndustry()&& null!=dataProviderEdit.getMainDataIndustry().getDataIndustry())
                {
                	//System.out.println("ind"+dataProviderEdit.getMainDataIndustry().getDataIndustry());
                    providerEdit.setMainDataIndustry(dataProviderEdit.getMainDataIndustry());
                }
                if(null!=dataProviderEdit.getMainDataCategory()&& null!=dataProviderEdit.getMainDataCategory().getDatacategory())
                {
                	//System.out.println("ind"+dataProviderEdit.getMainDataCategory().getDatacategory());
                    providerEdit.setMainDataCategory(dataProviderEdit.getMainDataCategory());
                }
            }
            else
            {

                if(!dataProviders.getProviderName().equalsIgnoreCase(dataProviderEdit.getProviderName()))
                {
                    providerEdit.setProviderName(dataProviderEdit.getProviderName());
                }
                if(!dataProviders.getWebsite().equalsIgnoreCase(dataProviderEdit.getWebsite()))
                {
                    providerEdit.setWebsite(dataProviderEdit.getWebsite());
                }
            }

        /*	if(!dataProvider.getpublic)
        	}*/
            result = dataProviderEditRepository.save(dataProviderEdit);
            dbutils.populateDataModelProviderEdit();
           DataProviderEdit edit= dataProviderEditRepository.findById(result.getId());
           DataProviderEdit editChanged= checkJsonValue(result);
            mailService.sendProviderEditEmail(user,edit,providerEdit,provider,editChanged);
            mailService.sendAdminEmailProviderEdit(user, edit,providerEdit,provider,editChanged);
        }


       /* else {
            dataProviderEdit.setEditorUserId(user.getId());
             result = dataProviderEditRepository.save(dataProviderEdit);
            mailService.sendProviderEditEmail(user,result);
            mailService.sendAdminEmailProviderEdit(user, result);
        }*/
        /*int i = 0;
        Field[] field = result.getClass().getDeclaredFields();

        for(int j=0 ; j<field.length ; j++){
            String name = field[j].getName();
            name = name.substring(0,1).toUpperCase()+name.substring(1);
            String type = field[j].getGenericType().toString();
            Method m = result.getClass().getMethod("get"+name);
           // String value1=null,value2=null;
            System.out.println("M Value: "+type);
            System.out.println("M Value: "+m);
            if(!type.equals("class.java.lang.String")){
                String value1 = String.valueOf(m.invoke(result));
                if(value1.equals("null"))
                    i++;
            }
            else {
                String  value2 = (String) m.invoke(result);
                if(value2==null)
                    i++;
            }

            float completeness=100*(field.length-i)/field.length;
            result.setCompleteness(completeness);
            result = dataProviderEditRepository.save(result);

        }*/
        else
        {
        	if(null!=dataProviderEdit.getLongDescription())
        	{
            	dataProviderEdit.setLongDescription(dataProviderEdit.getLongDescription().replaceAll("\\<[^>]*>",""));
            	//System.out.println("long"+dataProviderEdit.getLongDescription());
        	}
        	if(null!=dataProviderEdit.getKeyDataFields())
        	{
            	dataProviderEdit.setKeyDataFields(dataProviderEdit.getKeyDataFields().replaceAll("\\<[^>]*>",""));

        	}
        	if(null!=dataProviderEdit.getProductDetails())
        	{
            	dataProviderEdit.setProductDetails(dataProviderEdit.getProductDetails().replaceAll("\\<[^>]*>",""));
        	}
        	if(null!=dataProviderEdit.getUseCasesOrQuestionsAddressed())
        	{
            	dataProviderEdit.setUseCasesOrQuestionsAddressed(dataProviderEdit.getUseCasesOrQuestionsAddressed().replaceAll("\\<[^>]*>",""));
        	}
            result = dataProviderEditRepository.save(dataProviderEdit);
            dbutils.populateDataModelProviderEdit();

        }
        return ResponseEntity.created(new URI("/api/provider_profile_edit/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    private DataProviderEdit checkJsonValue(DataProviderEdit dataProviderEdit) throws JsonParseException, JsonMappingException, IOException {
		// TODO Auto-generated method stub
    	 DataProvider dataProviders=dataProviderRepository.findOneIdWithEagerRelationships(dataProviderEdit.getRecordID());
    	 DataProviderEdit dataProviderEditChange= new DataProviderEdit();


    	 ObjectMapper mapper = new ObjectMapper();
    	 System.out.println("dataProviders"+dataProviders.getMainDataCategory().getDatacategory());
    	 
//sources
		 
		 String sourcesJson = new Gson().toJson(dataProviders.getSources());
		    ArrayList<BasicBean> sourcesListJson = mapper.readValue(sourcesJson,
		            new TypeReference<ArrayList<BasicBean>>(){});
		    
		    String editsourcesJson = new Gson().toJson(dataProviderEdit.getSources());
		    ArrayList<BasicBean> editsourcesListJson = mapper.readValue(editsourcesJson,
		            new TypeReference<ArrayList<BasicBean>>(){});
		    
		    ArrayList<BasicBean> sourcesList= new ArrayList<BasicBean>();     
			 for(BasicBean b:editsourcesListJson)
			 {
				 if(null!=sourcesListJson)
				 {
					 if(!sourcesListJson.contains(b)) {
							sourcesList.add(b);
						}
				 }
				
		   
			 }
			 dataProviderEditChange.setSources(sourcesList);
    	 
    	 //geographical	   
       ArrayList<BasicBean> geolist= new ArrayList<BasicBean>();     
       List<String>geoGraphical=dataProviders.getGeographicalFoci().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
       String editgeoJson = new Gson().toJson(dataProviderEdit.getGeographicalFoci());
	    ArrayList<BasicBean> editgeoListJson = mapper.readValue(editgeoJson,
	            new TypeReference<ArrayList<BasicBean>>(){});
       for(BasicBean b:editgeoListJson)
    	 {
    		 if(null!=geoGraphical)
    		 {
    				if(!geoGraphical.contains(b.getDesc()))
    	    		{
    	    			geolist.add(b);
    	    		}
    		 }
    		 else {
    			 geolist.add(b);
    		 }
        
    	 }
    	 dataProviderEditChange.setGeographicalFoci(geolist);
    	 
    	//tags
		 String tagsJson = new Gson().toJson(dataProviders.getProviderTags());
		    ArrayList<BasicBean> tagsListJson = mapper.readValue(tagsJson,
		            new TypeReference<ArrayList<BasicBean>>(){});
		    
		    ArrayList<BasicBean> tagsList= new ArrayList<BasicBean>();   
		    
		    String edittagJson = new Gson().toJson(dataProviderEdit.getProviderTags());
		    ArrayList<BasicBean> edittagListJson = mapper.readValue(edittagJson,
		            new TypeReference<ArrayList<BasicBean>>(){});
		    
			 for(BasicBean b:edittagListJson)
			 {
				 if(null!=tagsListJson)
				 {
					 if(!tagsListJson.contains(b)) {
							tagsList.add(b);
						} 
				 }
				 else {
						tagsList.add(b);
				 }
		   
			 }
			 dataProviderEditChange.setProviderTags(tagsList);
    	 
//target organization type
    	 ArrayList<BasicBean> targetOrganizationlist= new ArrayList<BasicBean>();     
         List<String>targetOrganization=dataProviders.getOrganizationType().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
         String editOrganizationTypeJson = new Gson().toJson(dataProviderEdit.getOrganizationType());
		    ArrayList<BasicBean> editOrganizationTypeListJson = mapper.readValue(editOrganizationTypeJson,
		            new TypeReference<ArrayList<BasicBean>>(){});
         
         for(BasicBean b:editOrganizationTypeListJson)
      	 {
      		 if(null!=targetOrganization)
      		 {
      				if(!targetOrganization.contains(b.getDesc()))
      	    		{
      					targetOrganizationlist.add(b);
      	    		}
      		 }
      		 else {
      			targetOrganizationlist.add(b);
      		 }
      	 }
      	dataProviderEditChange.setOrganizationType(targetOrganizationlist);
      	
      //managerType
    	 ArrayList<BasicBean> managerTypeList= new ArrayList<BasicBean>();     
       List<String>managerType=dataProviders.getManagerType().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
       String editManagerTypeJson = new Gson().toJson(dataProviderEdit.getManagerType());
	    ArrayList<BasicBean> editManagerTypeListJson = mapper.readValue(editManagerTypeJson,
	            new TypeReference<ArrayList<BasicBean>>(){});
       
       for(BasicBean b:editManagerTypeListJson)
    	 {
    		 if(null!=managerType)
    		 {
    				if(!managerType.contains(b.getDesc()))
    	    		{
    					managerTypeList.add(b);
    	    		}
    		 }
    		 else {
    			managerTypeList.add(b);
    		 }
    	 }
    	dataProviderEditChange.setManagerType(managerTypeList);
    	
    	 //investor Type
   	 ArrayList<BasicBean> investorTypeList= new ArrayList<BasicBean>();     
      List<String>investorType=dataProviders.getInvestorTypes().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
   	
      String editInvestorTypeJson = new Gson().toJson(dataProviderEdit.getInvestorTypes());
	    ArrayList<BasicBean> editInvestorTypeListJson = mapper.readValue(editInvestorTypeJson,
	            new TypeReference<ArrayList<BasicBean>>(){});
      
      for(BasicBean b:editInvestorTypeListJson)
   	 {
   		 if(null!=investorType)
   		 {
   				if(!investorType.contains(b.getDesc()))
   	    		{
   					investorTypeList.add(b);
   	    		}
   		 }
   		 else {
   			 investorTypeList.add(b);
   		 }
   	 }
   	dataProviderEditChange.setInvestorTypes(investorTypeList);
   	
    //target strategies	 
 	 ArrayList<BasicBean> targetStrategieslist= new ArrayList<BasicBean>();     
    List<String>targetStrategies=dataProviders.getStrategies().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
    String editStrategiesTypeJson = new Gson().toJson(dataProviderEdit.getStrategies());
    ArrayList<BasicBean> editStrategiesTypeListJson = mapper.readValue(editStrategiesTypeJson,
            new TypeReference<ArrayList<BasicBean>>(){});
    
    for(BasicBean b:editStrategiesTypeListJson)
 	 {
 		 if(null!=targetStrategies)
 		 {
 				if(!targetStrategies.contains(b.getDesc()))
 	    		{
 					targetStrategieslist.add(b);
 	    		}
 		 }
 		 else {
 			targetStrategieslist.add(b);
 		 }
 	 }
 	dataProviderEditChange.setStrategies(targetStrategieslist);
 	
 	//target research styles
	 ArrayList<BasicBean> targetResearchStylesList= new ArrayList<BasicBean>();     
    List<String>targetResearchStyles=dataProviders.getResearchStyles().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
    String editResearchStyleTypeJson = new Gson().toJson(dataProviderEdit.getResearchStyles());
    ArrayList<BasicBean> editResearchStyleTypeListJson = mapper.readValue(editResearchStyleTypeJson,
            new TypeReference<ArrayList<BasicBean>>(){});
    
    for(BasicBean b:editResearchStyleTypeListJson)
 	 {
 		 if(null!=targetResearchStyles)
 		 {
 				if(!targetResearchStyles.contains(b.getDesc()))
 	    		{
 					targetResearchStylesList.add(b);
 	    		}
 		 }
 		 else {
 			targetResearchStylesList.add(b);
 		 }
 	 }
 	dataProviderEditChange.setResearchStyles(targetResearchStylesList);
    	
//targetInvestmenttimeframe
    	 ArrayList<BasicBean> targetInvestmentlist= new ArrayList<BasicBean>();     
         List<String>targetInvestment=dataProviders.getInvestingTimeFrame().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
         String editInvestingTimeFrameJson = new Gson().toJson(dataProviderEdit.getInvestingTimeFrame());
         ArrayList<BasicBean> editInvestingTimeFrameListJson = mapper.readValue(editInvestingTimeFrameJson,
                 new TypeReference<ArrayList<BasicBean>>(){});
         
         if(null!=editInvestingTimeFrameListJson)
         {
        	 for(BasicBean b:editInvestingTimeFrameListJson)
          	 {
          		 if(null!=targetInvestment)
          		 {
          				if(!targetInvestment.contains(b.getDesc()))
          	    		{
          					targetInvestmentlist.add(b);
          	    		}
          		 }
          		 else {
          			targetInvestmentlist.add(b);
          		 }
          	 }
         }
        
       	dataProviderEditChange.setInvestingTimeFrame(targetInvestmentlist);
       	
      //target public equities marketcap
     	 ArrayList<BasicBean> targetPublicEquitiesMarketCapList= new ArrayList<BasicBean>();     
        List<String>targetPublicEquitiesMarketCap=dataProviders.getEquitiesMarketCap().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
        String editEquitiesMarketCapJson = new Gson().toJson(dataProviderEdit.getEquitiesMarketCap());
        ArrayList<BasicBean> editEquitiesMarketCapListJson = mapper.readValue(editEquitiesMarketCapJson,
                new TypeReference<ArrayList<BasicBean>>(){});
        if(null!=editEquitiesMarketCapListJson)
        {
        	 for(BasicBean b:editEquitiesMarketCapListJson)
         	 {
         		 if(null!=targetPublicEquitiesMarketCap)
         		 {
         				if(!targetPublicEquitiesMarketCap.contains(b.getDesc()))
         	    		{
         					targetPublicEquitiesMarketCapList.add(b);
         	    		}
         		 }
         		 else {
         			targetPublicEquitiesMarketCapList.add(b);
         		 }
         	 }	
        }
       
     	dataProviderEditChange.setEquitiesMarketCap(targetPublicEquitiesMarketCapList);
     	
     	//target public equities style
     	 ArrayList<BasicBean> targetPublicEquitiesStylesList= new ArrayList<BasicBean>();     
        List<String>targetPublicEquitiesStyles=dataProviders.getEquitiesStyle().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
        String editEquitiesStyleJson = new Gson().toJson(dataProviderEdit.getEquitiesStyle());
        ArrayList<BasicBean> editEquitiesStyleListJson = mapper.readValue(editEquitiesStyleJson,
                new TypeReference<ArrayList<BasicBean>>(){});
        if(null!=editEquitiesStyleListJson)
        {
        	for(BasicBean b:editEquitiesStyleListJson)
        	 {
        		 if(null!=targetPublicEquitiesStyles)
        		 {
        				if(!targetPublicEquitiesStyles.contains(b.getDesc()))
        	    		{
        					targetPublicEquitiesStylesList.add(b);
        	    		}
        		 }
        		 else {
        			targetPublicEquitiesStylesList.add(b);
        		 }
        	 }
        }
        
     	dataProviderEditChange.setEquitiesStyle(targetPublicEquitiesStylesList);
     	
     	//assetClass
		 String assetClassJson = new Gson().toJson(dataProviders.getAssetClasses());
		    ArrayList<BasicBean> assetClassJsonListJson = mapper.readValue(assetClassJson,
		            new TypeReference<ArrayList<BasicBean>>(){});
		    String editAssetClassJson = new Gson().toJson(dataProviderEdit.getAssetClasses());
	        ArrayList<BasicBean> editAssetClassListJson = mapper.readValue(editAssetClassJson,
	                new TypeReference<ArrayList<BasicBean>>(){});
		    
		    ArrayList<BasicBean> assetClassList= new ArrayList<BasicBean>();   
		    if(null!=editAssetClassListJson) {
		    	 for(BasicBean b:editAssetClassListJson)
				 {
					 if(null!=assetClassJsonListJson)
					 {
						 if(!assetClassJsonListJson.contains(b)) {
								assetClassList.add(b);
							}	 
					 }
					 else {
							assetClassList.add(b);
					 }
			   
				 }
		    }
			
			 dataProviderEditChange.setAssetClasses(assetClassList);
			 
			//securityTypes
			 String securityTypeJson = new Gson().toJson(dataProviders.getSecurityTypes());
			    ArrayList<BasicBean> securityTypeListJson = mapper.readValue(securityTypeJson,
			            new TypeReference<ArrayList<BasicBean>>(){});
			    
			    String editSecurityTypeJson = new Gson().toJson(dataProviderEdit.getSecurityTypes());
			    ArrayList<BasicBean> editSecurityTypeListJson = mapper.readValue(editSecurityTypeJson,
			            new TypeReference<ArrayList<BasicBean>>(){});
			    ArrayList<BasicBean> securityTypeList= new ArrayList<BasicBean>(); 
			    if(null!=editSecurityTypeListJson) {
			    	for(BasicBean b:editSecurityTypeListJson)
					 {
						 if(null!=securityTypeListJson)
						 {
							 if(!securityTypeListJson.contains(b)) {
									securityTypeList.add(b);
								} 
						 }
						 else
						 {
								securityTypeList.add(b);

						 }
					 }	
			    }
				 
				 dataProviderEditChange.setSecurityTypes(securityTypeList);
				 
				//sectors
				 String sectorsJson = new Gson().toJson(dataProviders.getRelevantSectors());
				    ArrayList<BasicBean> sectorsListJson = mapper.readValue(sectorsJson,
				            new TypeReference<ArrayList<BasicBean>>(){});
				    
				    String editSectorsJson = new Gson().toJson(dataProviderEdit.getRelevantSectors());
				    ArrayList<BasicBean> editSectorsListJson = mapper.readValue(editSectorsJson,
				            new TypeReference<ArrayList<BasicBean>>(){});
				    
				    ArrayList<BasicBean> sectorsList= new ArrayList<BasicBean>(); 
				    if(null!=editSectorsListJson) {
				    	 for(BasicBean b:editSectorsListJson)
						 {
							 if(null!=sectorsListJson)
							 {
								 if(!sectorsListJson.contains(b)) {
										sectorsList.add(b);
									} 
							 }
							 else {
									sectorsList.add(b);

							 }
					   
						 }
				    }
					
					 dataProviderEditChange.setRelevantSectors(sectorsList);
					
					 //identifiers Available
				   	 ArrayList<BasicBean> identifiersAvailableList= new ArrayList<BasicBean>();     
				      List<String>identifiersAvailable=dataProviders.getidentifiersAvailable().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
				   	
				      String editIdentifiersAvailableJson = new Gson().toJson(dataProviderEdit.getIdentifiersAvailable());
					    ArrayList<BasicBean> editIdentifiersAvailableListJson = mapper.readValue(editIdentifiersAvailableJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
					    
				      if(null!=editIdentifiersAvailableListJson) {
				    	  for(BasicBean b:editIdentifiersAvailableListJson)
						   	 {
						   		 if(null!=identifiersAvailable)
						   		 {
						   				if(!identifiersAvailable.contains(b.getDesc()))
						   	    		{
						   					identifiersAvailableList.add(b);
						   	    		}
						   		 }
						   		 else {
						   			identifiersAvailableList.add(b);
						   		 }
						   	 }
				      }
				     
				   	dataProviderEditChange.setIdentifiersAvailable(identifiersAvailableList);
				   	
				    //Access Offered
				  	 ArrayList<BasicBean> accessOfferedList= new ArrayList<BasicBean>();     
				     List<String>accessOffered=dataProviders.getAccessOffered().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
				  	
				     String editAccessOfferedJson = new Gson().toJson(dataProviderEdit.getAccessOffered());
					    ArrayList<BasicBean> editAccessOfferedJsonList = mapper.readValue(editAccessOfferedJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
				     if(null!=editAccessOfferedJsonList) {
				    	 for(BasicBean b:editAccessOfferedJsonList)
					  	 {
					  		 if(null!=accessOffered)
					  		 {
					  				if(!accessOffered.contains(b.getDesc()))
					  	    		{
					  					accessOfferedList.add(b);
					  	    		}
					  		 }
					  		 else {
					  			accessOfferedList.add(b);
					  		 }
					  	 }
				     }
				     
				  	dataProviderEditChange.setAccessOffered(accessOfferedList);
				  	
				  //producttypes
				  	 ArrayList<BasicBean> productTypesList= new ArrayList<BasicBean>();     
				     List<String>productTypes=dataProviders.getDataProductTypes().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
				  	
				 	
				     String editDataProductTypesJson = new Gson().toJson(dataProviderEdit.getDataProductTypes());
					    ArrayList<BasicBean> editDataProductTypesJsonList = mapper.readValue(editDataProductTypesJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
				     
				     if(null!=editDataProductTypesJsonList) {
				    	 for(BasicBean b:editDataProductTypesJsonList)
					  	 {
					  		 if(null!=productTypes)
					  		 {
					  				if(!productTypes.contains(b.getDesc()))
					  	    		{
					  					productTypesList.add(b);
					  	    		}
					  		 }
					  		 else {
					  			productTypesList.add(b);
					  		 }
					  	 }
				     }
				     
				  	dataProviderEditChange.setDataProductTypes(productTypesList);
				  	
				  	 //types
				  	 ArrayList<BasicBean> typesList= new ArrayList<BasicBean>();     
				     List<String>types=dataProviders.getDataTypes().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
				  	
				     String editDataTypesJson = new Gson().toJson(dataProviderEdit.getDataTypes());
					    ArrayList<BasicBean> editDataTypesJsonList = mapper.readValue(editDataTypesJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
				     if(null!=editDataTypesJsonList) {
				    	 for(BasicBean b:editDataTypesJsonList)
					  	 {
					  		 if(null!=types)
					  		 {
					  				if(!types.contains(b.getDesc()))
					  	    		{
					  					typesList.add(b);
					  	    		}
					  		 }
					  		 else {
					  			typesList.add(b);
					  		 }
					  	 }
				     }
				     
				  	dataProviderEditChange.setDataTypes(typesList);
				  	
				  	
					//Language Available
			      	 ArrayList<BasicBean> languageList= new ArrayList<BasicBean>();     
			         List<String>language=dataProviders.getLanguagesAvailable().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
			         String editLanguageJson = new Gson().toJson(dataProviderEdit.getLanguagesAvailable());
					    ArrayList<BasicBean> editLanguageJsonList = mapper.readValue(editLanguageJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
			         if(null!=editLanguageJsonList) {
			        	 for(BasicBean b:editLanguageJsonList)
				      	 {
				      		 if(null!=language)
				      		 {
				      				if(!language.contains(b.getDesc()))
				      	    		{
				      					languageList.add(b);
				      	    		}
				      		 }
				      		 else {
				      			languageList.add(b);
				      		 }
				      	 } 
			         }
			         else {
					      	dataProviderEditChange.setLanguagesAvailable(languageList);
			         }
			         //System.out.println("lang"+dataProviderEditChange.getLanguagesAvailable().size());
			      	
			      //deliveryMethod
					 String deliveryMethodJson = new Gson().toJson(dataProviders.getDeliveryMethods());
					    ArrayList<BasicBean> deliveryMethodListJson = mapper.readValue(deliveryMethodJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
					    String editDeliveryMethodJson = new Gson().toJson(dataProviderEdit.getDeliveryMethods());
					    ArrayList<BasicBean> editDeliveryMethodListJson = mapper.readValue(editDeliveryMethodJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
					    
					    ArrayList<BasicBean> deliveryMethodTypeList= new ArrayList<BasicBean>();   
					    if(null!=editDeliveryMethodListJson) {
					    	for(BasicBean b:editDeliveryMethodListJson)
							 {
								 if(null!=deliveryMethodListJson)
								 {
										if(!deliveryMethodListJson.contains(b)) {
											deliveryMethodTypeList.add(b);
										}
								 }
								 else
								 {
										deliveryMethodTypeList.add(b);

								 }
					    }
						 
						
							 dataProviderEditChange.setDeliveryMethods(deliveryMethodTypeList);
						 }	
						 
						//deliveryFormats
						 String deliveryFormatsJson = new Gson().toJson(dataProviders.getDeliveryFormats());
						    ArrayList<BasicBean> deliveryFormatsListJson = mapper.readValue(deliveryFormatsJson,
						            new TypeReference<ArrayList<BasicBean>>(){});
						    
						    String editDeliveryFormatsJson = new Gson().toJson(dataProviderEdit.getDeliveryFormats());
						    ArrayList<BasicBean> editDeliveryFormatsListJson = mapper.readValue(editDeliveryFormatsJson,
						            new TypeReference<ArrayList<BasicBean>>(){});
						    
						    
						    ArrayList<BasicBean> deliveryFormatsTypeList= new ArrayList<BasicBean>();  
						    if(null!=editDeliveryFormatsListJson) {
						    	for(BasicBean b:editDeliveryFormatsListJson)
								 {
									 if(null!=deliveryFormatsListJson)
									 {
										 if(!deliveryFormatsListJson.contains(b)) {
												deliveryFormatsTypeList.add(b);
											} 
									 }
									 else
									 {
											deliveryFormatsTypeList.add(b);

									 }
							   
								 }	 
						 
						    }
							 
							 dataProviderEditChange.setDeliveryFormats(deliveryFormatsTypeList);
						 
//delivery Frequency
        ArrayList<BasicBean> deliveryFrequencyList= new ArrayList<BasicBean>();     
        List<String>deliveryFrequency=dataProviders.getDeliveryFrequencies().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
        String editDeliveryFrequencyJson = new Gson().toJson(dataProviderEdit.getDeliveryFrequencies());
	    ArrayList<BasicBean> editDeliveryFrequencyJsonList = mapper.readValue(editDeliveryFrequencyJson,
	            new TypeReference<ArrayList<BasicBean>>(){});
	    
        if(null!=editDeliveryFrequencyJsonList) {
        	for(BasicBean b:editDeliveryFrequencyJsonList)
        	 {
        		 if(null!=deliveryFrequency)
        		 {
        				if(!deliveryFrequency.contains(b.getDesc()))
        	    		{
        					deliveryFrequencyList.add(b);
        	    		}
        		 }
        		 else {
        			deliveryFrequencyList.add(b);
        		 }
            
        	 }
        }
        
     	 dataProviderEditChange.setDeliveryFrequencies(deliveryFrequencyList);
  
   	
      	 //paymentmethodsoffered
      	 ArrayList<BasicBean> paymentMethodsOfferedList= new ArrayList<BasicBean>();     
         List<String>paymentMethodsOffered=dataProviders.getPaymentMethodsOffered().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
      
         String editPaymentsMehtodOfferedJson = new Gson().toJson(dataProviderEdit.getPaymentMethodsOffered());
 	    ArrayList<BasicBean> editPaymentsMehtodOfferedJsonList = mapper.readValue(editPaymentsMehtodOfferedJson,
 	            new TypeReference<ArrayList<BasicBean>>(){});
 	    
         if(null!=editPaymentsMehtodOfferedJsonList) {
        	 for(BasicBean b:editPaymentsMehtodOfferedJsonList)
          	 {
          		 if(null!=paymentMethodsOffered)
          		 {
          				if(!paymentMethodsOffered.contains(b.getDesc()))
          	    		{
          					paymentMethodsOfferedList.add(b);
          	    		}
          		 }
          		 else {
          			paymentMethodsOfferedList.add(b);
          		 }
          	 }
         }
         
      	dataProviderEditChange.setPaymentMethodsOffered(paymentMethodsOfferedList);
      	 //datagapsreason
      	 ArrayList<BasicBean> dataGapsReasonList= new ArrayList<BasicBean>();     
         List<String>dataGapsReason=dataProviders.getDataGapsReasons().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
      	

         String editDataGapsJson = new Gson().toJson(dataProviderEdit.getDataGapsReasons());
 	    ArrayList<BasicBean> editDataGapsJsonList = mapper.readValue(editDataGapsJson,
 	            new TypeReference<ArrayList<BasicBean>>(){});
         
 	    if(null!=editDataGapsJsonList) {
 	    	for(BasicBean b:editDataGapsJsonList)
 	      	 {
 	      		 if(null!=dataGapsReason)
 	      		 {
 	      				if(!dataGapsReason.contains(b.getDesc()))
 	      	    		{
 	      					dataGapsReasonList.add(b);
 	      	    		}
 	      		 }
 	      		 else {
 	      			dataGapsReasonList.add(b);
 	      		 }
 	      	 }
 	    }
         
      	dataProviderEditChange.setDataGapsReasons(dataGapsReasonList);
      	//outlier Reason
      	 ArrayList<BasicBean> outlierReasonList= new ArrayList<BasicBean>();     
         List<String>outlierReason=dataProviders.getOutlierReasons().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
      	
         String editOutliersJson = new Gson().toJson(dataProviderEdit.getOutlierReasons());
  	    ArrayList<BasicBean> editOutliersJsonList = mapper.readValue(editOutliersJson,
  	            new TypeReference<ArrayList<BasicBean>>(){});
         
  	    if(null!=editOutliersJsonList) {
  	    	for(BasicBean b:editOutliersJsonList)
  	      	 {
  	      		 if(null!=outlierReason)
  	      		 {
  	      				if(!outlierReason.contains(b.getDesc()))
  	      	    		{
  	      					outlierReasonList.add(b);
  	      	    		}
  	      		 }
  	      		 else {
  	      			outlierReasonList.add(b);
  	      		 }
  	      	 }
  	    }
         
      	dataProviderEditChange.setOutlierReasons(outlierReasonList);
     	//Bias Reason
      	 ArrayList<BasicBean> biasList= new ArrayList<BasicBean>();     
         List<String>bias=dataProviders.getBiases().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
      	
         String editBiasesJson = new Gson().toJson(dataProviderEdit.getBiases());
   	    ArrayList<BasicBean> editBiasesJsonList = mapper.readValue(editBiasesJson,
   	            new TypeReference<ArrayList<BasicBean>>(){});
         if(null!=editBiasesJsonList) {
        	 for(BasicBean b:editBiasesJsonList)
          	 {
          		 if(null!=bias)
          		 {
          				if(!bias.contains(b.getDesc()))
          	    		{
          					biasList.add(b);
          	    		}
          		 }
          		 else {
          			biasList.add(b);
          		 }
          	 }
         }
        
      	dataProviderEditChange.setBiases(biasList);
      
      	 //pricingModel	
    	 String pricingModeljson = new Gson().toJson(dataProviders.getPricingModels());
       ArrayList<BasicBean> listFromPricingmodel= mapper.readValue(pricingModeljson,
               new TypeReference<ArrayList<BasicBean>>(){});
       
       String editPricingModeljson = new Gson().toJson(dataProviderEdit.getDataProvidersPricingModels());
       ArrayList<BasicBean> editListFromPricingmodel= mapper.readValue(editPricingModeljson,
               new TypeReference<ArrayList<BasicBean>>(){});
       
       ArrayList<BasicBean> pricingModelList= new ArrayList<BasicBean>();  
       if(null!=editListFromPricingmodel) {
    	   for(BasicBean b:editListFromPricingmodel)
      	 {
      		 if(null!=listFromPricingmodel)
      		 {
      			 if(!listFromPricingmodel.contains(b)) {
      				 pricingModelList.add(b);
      	    		} 
      		 }
      		 else {
      			 pricingModelList.add(b);
      		 }
          
      	 }
       }
    	 
    	 dataProviderEditChange.setDataProvidersPricingModels(pricingModelList);
    //industries	
    	 String json = new Gson().toJson(dataProviders.getIndustries());
       ArrayList<BasicBean> listFromJackson = mapper.readValue(json,
               new TypeReference<ArrayList<BasicBean>>(){});
       
       String editjson = new Gson().toJson(dataProviderEdit.getIndustries());
       ArrayList<BasicBean> editListFromJackson = mapper.readValue(editjson,
               new TypeReference<ArrayList<BasicBean>>(){});
       
       
       ArrayList<BasicBean> list= new ArrayList<BasicBean>();     
       if(null!=editListFromJackson) {
    	   for(BasicBean b:editListFromJackson)
      	 {
      		 if(null!=listFromJackson)
      		 {
      			 if(!listFromJackson.contains(b)) {
      	    			list.add(b);
      	    		} 
      		 }
      		 else {
  	    			list.add(b);
      		 }
          
      	 }
       }
    	 
    	 dataProviderEditChange.setIndustries(list);
//categories		
   	String categoriesJson = new Gson().toJson(dataProviders.getCategories());
    ArrayList<BasicBean> categoriesListJson = mapper.readValue(categoriesJson,
            new TypeReference<ArrayList<BasicBean>>(){});
    
	String editCategoriesJson = new Gson().toJson(dataProviderEdit.getCategories());
    ArrayList<BasicBean> editCategoriesListJson = mapper.readValue(editCategoriesJson,
            new TypeReference<ArrayList<BasicBean>>(){});
    
    
    ArrayList<BasicBean> categoriesList= new ArrayList<BasicBean>();     
    if(null!=editCategoriesListJson) {
    	for(BasicBean b:editCategoriesListJson)
   	 {
   		 if(null!=categoriesListJson)
   		 {
   				if(!categoriesListJson.contains(b)) {
   					categoriesList.add(b);
   				} 
   		 }
   		 else {
   				categoriesList.add(b);
   		 }
      
   	 }
    }
	 
	 dataProviderEditChange.setCategories(categoriesList);
	//features		
	   	String featuresJson = new Gson().toJson(dataProviders.getFeatures());
	    ArrayList<BasicBean> featuresListJson = mapper.readValue(featuresJson,
	            new TypeReference<ArrayList<BasicBean>>(){});
	    
		String editFeaturesJson = new Gson().toJson(dataProviderEdit.getFeatures());
	    ArrayList<BasicBean> editFeaturesListJson = mapper.readValue(editFeaturesJson,
	            new TypeReference<ArrayList<BasicBean>>(){});
	    
	    ArrayList<BasicBean> featuresList= new ArrayList<BasicBean>();  
	    if(null!=editFeaturesListJson) {
	    	for(BasicBean b:editFeaturesListJson)
			 {
				 if(null!=featuresListJson)
				 {
						if(!featuresListJson.contains(b)) {
							featuresList.add(b);
						} 
				 }
				 else {
						featuresList.add(b);
				 }
		   
			 }
	    }
		 
		 dataProviderEditChange.setFeatures(featuresList);
		 
		 if(null!=dataProviders.getMainDataCategory()) {
         	  boolean setCategory=Objects.equals(dataProviders.getMainDataCategory(), dataProviderEdit.getMainDataCategory());
               if(!setCategory)
               {
                   //providerEdit.setPointInTimeAccuracy(dataProviderEdit.getPointInTimeAccuracy());
                   if(dataProviderEdit.getMainDataCategory()!=null)
                   {
                      
                	   dataProviderEditChange.setMainDataCategory(dataProviderEdit.getMainDataCategory());
                       
                   }
               } 
          }
          else {
         	 if(dataProviderEdit.getMainDataCategory()!=null)
              {
                 
         		dataProviderEditChange.setMainDataCategory(dataProviderEdit.getMainDataCategory());
                  
              }
          }
        
        if(null!=dataProviders.getMainDataIndustry()) {
        	  boolean setIndustry=Objects.equals(dataProviders.getMainDataIndustry(), dataProviderEdit.getMainDataIndustry());
              if(!setIndustry)
              {
                  //providerEdit.setPointInTimeAccuracy(dataProviderEdit.getPointInTimeAccuracy());
                  if(dataProviderEdit.getMainDataIndustry()!=null)
                  {
                     
                	  dataProviderEditChange.setMainDataIndustry(dataProviderEdit.getMainDataIndustry());
                      
                  }
              } 
         }
         else {
        	 if(dataProviderEdit.getMainDataIndustry()!=null)
             {
                
        		 dataProviderEditChange.setMainDataIndustry(dataProviderEdit.getMainDataIndustry());
                 
             }
         }
        
		 
 		return dataProviderEditChange;
	}


	@GetMapping(path = "/provider_profile_edit/{recordId}")
    @Timed
    public ResponseEntity<DataProviderEdit> providersUpdation(@PathVariable String recordId) throws URISyntaxException, IOException {
        //User user = userService.getUser();
        //DataProviderEdit dataProviderEdit = dataProviderEditRepository.findByRecordID(recordId);
       // DataProviderEdit dataProviderEdit = dataProviderEditRepository.findByrecordUpdated(recordId,user.getId());
        DataProviderEdit dataProviderEdit = dataProviderEditRepository.findByrecordUpdated(recordId);

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataProviderEdit));

    }
    
    @GetMapping(path = "/providerJson/{recordId}")
    @Timed
    public ResponseEntity<DataProvider> providersDataUpdation(@PathVariable String recordId) throws URISyntaxException, IOException {
        DataProvider dataProviders=dataProviderRepository.findOneIdWithEagerRelationships(recordId);
        JSONArray jsonArray = new JSONArray(dataProviders.getIndustries());


        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataProviders));

    }
    
    @GetMapping(path = "/providerAdmin/{recordId}")
    @Timed
    public ResponseEntity<DataProvider> userEditedValue(@PathVariable String recordId) throws JsonParseException, JsonMappingException, IOException
    {
    	//User user=userService.getUser();
    	//long id=424;
    	DataProviderEdit edit= dataProviderEditRepository.findByrecordUpdated(recordId);
    	//edit=checkJsonValue(edit);
    //User user=userService.getUser(edit.getEditorUserId());
    	DataProvider provider=new DataProvider();
   	 DataProvider dataProviders=dataProviderRepository.findOneIdWithEagerRelationships(edit.getRecordID());
    	provider=dataProviderEditService.checkChangedvalues(edit,dataProviders);
    	//provider.setUser(user);
		return new ResponseEntity<DataProvider>(provider, HttpStatus.OK);
    	
    }
    
    
    
    
    
}
