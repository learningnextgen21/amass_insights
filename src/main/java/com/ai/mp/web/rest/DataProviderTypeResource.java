package com.ai.mp.web.rest;

import com.ai.mp.domain.DataProviderType;
import com.ai.mp.repository.DataProviderTypeRepository;
import com.ai.mp.repository.search.DataProviderTypeSearchRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing DataProviderType.
 */
@RestController
@RequestMapping("/api")
public class DataProviderTypeResource {

    private final Logger log = LoggerFactory.getLogger(DataProviderTypeResource.class);

    private static final String ENTITY_NAME = "dataProviderType";

    private final DataProviderTypeRepository dataProviderTypeRepository;

    private final DataProviderTypeSearchRepository dataProviderTypeSearchRepository;
    public DataProviderTypeResource(DataProviderTypeRepository dataProviderTypeRepository, DataProviderTypeSearchRepository dataProviderTypeSearchRepository) {
        this.dataProviderTypeRepository = dataProviderTypeRepository;
        this.dataProviderTypeSearchRepository = dataProviderTypeSearchRepository;
    }

    /**
     * POST  /data-provider-types : Create a new dataProviderType.
     *
     * @param dataProviderType the dataProviderType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dataProviderType, or with status 400 (Bad Request) if the dataProviderType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/data-provider-types")
    @Timed
    public ResponseEntity<DataProviderType> createDataProviderType(@Valid @RequestBody DataProviderType dataProviderType) throws URISyntaxException {
        log.debug("REST request to save DataProviderType : {}", dataProviderType);
        if (dataProviderType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dataProviderType cannot already have an ID")).body(null);
        }
        DataProviderType result = dataProviderTypeRepository.save(dataProviderType);
        dataProviderTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/data-provider-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /data-provider-types : Updates an existing dataProviderType.
     *
     * @param dataProviderType the dataProviderType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dataProviderType,
     * or with status 400 (Bad Request) if the dataProviderType is not valid,
     * or with status 500 (Internal Server Error) if the dataProviderType couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/data-provider-types")
    @Timed
    public ResponseEntity<DataProviderType> updateDataProviderType(@Valid @RequestBody DataProviderType dataProviderType) throws URISyntaxException {
        log.debug("REST request to update DataProviderType : {}", dataProviderType);
        if (dataProviderType.getId() == null) {
            return createDataProviderType(dataProviderType);
        }
        DataProviderType result = dataProviderTypeRepository.save(dataProviderType);
        dataProviderTypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataProviderType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /data-provider-types : get all the dataProviderTypes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dataProviderTypes in body
     */
    @GetMapping("/data-provider-types")
    @Timed
    public ResponseEntity<List<DataProviderType>> getAllDataProviderTypes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DataProviderTypes");
        Page<DataProviderType> page = dataProviderTypeRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/data-provider-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /data-provider-types/:id : get the "id" dataProviderType.
     *
     * @param id the id of the dataProviderType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dataProviderType, or with status 404 (Not Found)
     */
    @GetMapping("/data-provider-types/{id}")
    @Timed
    public ResponseEntity<DataProviderType> getDataProviderType(@PathVariable Long id) {
        log.debug("REST request to get DataProviderType : {}", id);
        DataProviderType dataProviderType = dataProviderTypeRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataProviderType));
    }

    /**
     * DELETE  /data-provider-types/:id : delete the "id" dataProviderType.
     *
     * @param id the id of the dataProviderType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/data-provider-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataProviderType(@PathVariable Long id) {
        log.debug("REST request to delete DataProviderType : {}", id);
        dataProviderTypeRepository.delete(id);
        dataProviderTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/data-provider-types?query=:query : search for the dataProviderType corresponding
     * to the query.
     *
     * @param query the query of the dataProviderType search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/data-provider-types")
    @Timed
    public ResponseEntity<List<DataProviderType>> searchDataProviderTypes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataProviderTypes for query {}", query);
        Page<DataProviderType> page = dataProviderTypeSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/data-provider-types");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/data-provider-types/desc/{id}")
    @Timed
    public String getExplanationByID(@PathVariable Long id) {
        log.debug("REST request to search for a page of DataProviderType for query {}", id);
        DataProviderType element = dataProviderTypeSearchRepository.findOne(id);
        return element.getExplanation();
    }
}
