package com.ai.mp.web.rest;

import com.ai.core.StrUtils;
import com.ai.mp.domain.*;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.service.UserProviderService;
import com.ai.mp.service.UserService;
import com.ai.mp.utils.GenericHttpResponse;
import com.codahale.metrics.annotation.Timed;
import com.ai.mp.repository.UserProviderRepository;
import com.ai.mp.repository.search.UserProviderSearchRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Query;
import javax.validation.Valid;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ai.core.Constants.INTEREST_TYPE_FOLLOW;
import static com.ai.core.Constants.INTEREST_TYPE_UNLOCK;
import static com.ai.core.Constants.INTEREST_TYPE_UNLOCK_PENDING_USER_CONFIRM;
import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AmassUserProvider.
 */
@RestController
@RequestMapping("/api")
public class UserProviderResource {

    private final Logger log = LoggerFactory.getLogger(UserProviderResource.class);

    private static final String ENTITY_NAME = "amassUserProvider";

    private final DataProviderRepository dataProviderRepository;
    private final UserService userService;
    private final UserProviderRepository userProviderRepository;
    private final UserProviderService userProviderService;
    private final UserProviderSearchRepository userProviderSearchRepository;
    public UserProviderResource(DataProviderRepository dataProviderRepository, UserProviderRepository userProviderRepository, UserProviderService userProviderService, UserProviderSearchRepository userProviderSearchRepository,UserService userService) {
        this.dataProviderRepository = dataProviderRepository;
        this.userProviderRepository = userProviderRepository;
        this.userProviderService = userProviderService;
        this.userProviderSearchRepository = userProviderSearchRepository;
        this.userService = userService;
    }

    /**
     * POST  /amass-user-providers : Create a new amassUserProvider.
     *
     * @param amassUserProvider the amassUserProvider to create
     * @return the ResponseEntity with status 201 (Created) and with body the new amassUserProvider, or with status 400 (Bad Request) if the amassUserProvider has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/amass-user-providers")
    @Timed
    public ResponseEntity<UserProvider> createAmassUserProvider(@Valid @RequestBody UserProvider amassUserProvider) throws URISyntaxException {
        log.debug("REST request to save AmassUserProvider : {}", amassUserProvider);
        if (amassUserProvider.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new amassUserProvider cannot already have an ID")).body(null);
        }
        UserProvider result = userProviderRepository.save(amassUserProvider);
        userProviderSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/amass-user-providers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /amass-user-providers : Updates an existing amassUserProvider.
     *
     * @param amassUserProvider the amassUserProvider to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated amassUserProvider,
     * or with status 400 (Bad Request) if the amassUserProvider is not valid,
     * or with status 500 (Internal Server Error) if the amassUserProvider couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/amass-user-providers")
    @Timed
    public ResponseEntity<UserProvider> updateAmassUserProvider(@Valid @RequestBody UserProvider amassUserProvider) throws URISyntaxException {
        log.debug("REST request to update AmassUserProvider : {}", amassUserProvider);
        if (amassUserProvider.getId() == null) {
            return createAmassUserProvider(amassUserProvider);
        }
        UserProvider result = userProviderRepository.save(amassUserProvider);
        userProviderSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, amassUserProvider.getId().toString()))
            .body(result);
    }

    /**
     * GET  /amass-user-providers : get all the amassUserProviders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of amassUserProviders in body
     */
    @GetMapping("/amass-user-providers")
    @Timed
    public ResponseEntity<List<UserProvider>> getAllAmassUserProviders(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of AmassUserProviders");
        Page<UserProvider> page = userProviderRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/amass-user-providers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /amass-user-providers/:id : get the "id" amassUserProvider.
     *
     * @param id the id of the amassUserProvider to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the amassUserProvider, or with status 404 (Not Found)
     */
    @GetMapping("/amass-user-providers/{id}")
    @Timed
    public ResponseEntity<UserProvider> getAmassUserProvider(@PathVariable Long id) {
        log.debug("REST request to get AmassUserProvider : {}", id);
        UserProvider amassUserProvider = userProviderRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(amassUserProvider));
    }

    /**
     * DELETE  /amass-user-providers/:id : delete the "id" amassUserProvider.
     *
     * @param id the id of the amassUserProvider to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/amass-user-providers/{id}")
    @Timed
    public ResponseEntity<Void> deleteAmassUserProvider(@PathVariable Long id) {
        log.debug("REST request to delete AmassUserProvider : {}", id);
        userProviderRepository.delete(id);
        userProviderSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/amass-user-providers?query=:query : search for the amassUserProvider corresponding
     * to the query.
     *
     * @param query the query of the amassUserProvider search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/amass-user-providers")
    @Timed
    public ResponseEntity<List<UserProvider>> searchAmassUserProviders(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AmassUserProviders for query {}", query);
        Page<UserProvider> page = userProviderSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/amass-user-providers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    @GetMapping ("/follow-provider")
    @Timed
    public ResponseEntity<GenericHttpResponse> updateFollowedProviders(@RequestParam long providerID) {
        if(providerID <= 0)
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Follow Provider", "Missing Provider ID", "failed"));
        }
        userProviderService.followProvider(providerID);
        return ResponseEntity.ok().body(new GenericHttpResponse("Follow Provider", "Request processed successfully", "success"));
    }

    @GetMapping ("/operator-permission")
    @Timed
    public List<UserProvider> operatorPermission() {
        List<UserProvider> providers=userProviderService.getProviderIDByInterestTypeByUser();

        List<UserProvider>userProvider=new ArrayList<UserProvider>();
        for(UserProvider provider:providers)
        {

            DataProvider dataProvider=  dataProviderRepository.findByRecordID(provider.getProviderRecordID());
            provider.setProviderName(dataProvider.getProviderName());
            DataProvider data= dataProviderRepository.findOneWithEagerRelationships(dataProvider.getId());
            if(null!=data.getOwnerOrganization())
            {
                provider.setOrganizationName(data.getOwnerOrganization().getName());
            }
            userProvider.add(provider);

        }
        if (providers.size()<=0) {
            return null;

        }
        else
            return userProvider;

    }

    @GetMapping ("/ur/useroperator-permission/{id}")
    @Timed
    public List<UserProvider> useroperatorpermission(@PathVariable Long id) {

        List<UserProvider> providers=userProviderService.getProviderIDByInterestTypeByUsers(id);

        List<UserProvider>userProvider=new ArrayList<UserProvider>();
        for(UserProvider provider:providers)
        {

            DataProvider dataProvider=  dataProviderRepository.findByRecordID(provider.getProviderRecordID());
            provider.setProviderName(dataProvider.getProviderName());
            DataProvider data= dataProviderRepository.findOneWithEagerRelationships(dataProvider.getId());
            if(null!=data.getOwnerOrganization())
            {
                provider.setOrganizationName(data.getOwnerOrganization().getName());
            }
            userProvider.add(provider);

        }
        if (providers.size()<=0) {
            return null;

        }
        else
            return userProvider;


    }
    @GetMapping ("/ur/unfollow-provider")
    @Timed
    public ResponseEntity<GenericHttpResponse> unfollowProvider(@RequestParam(value = "key") String key) {
        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Unfollow Provider", "Missing Unfollow key", "failed"));
        }
        userProviderService.followProvider(key, true);
        return ResponseEntity.ok().body(new GenericHttpResponse("Unfollow Provider", "Request processed successfully", "success"));
    }

    @GetMapping ("/ur/follow-provider")
    @Timed
    public ResponseEntity<GenericHttpResponse> followProvider(@RequestParam(value = "key") String key) {
        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Follow Provider", "Missing follow key", "failed"));
        }
        userProviderService.followProvider(key, false);
        return ResponseEntity.ok().body(new GenericHttpResponse("Follow Provider", "Request processed successfully", "success"));
    }

//    @PostMapping ("/expand-provider")
//    @Timed
//    public ResponseEntity<GenericHttpResponse> updateExpandProviders(@RequestParam long providerID) {
//        if(providerID <= 0)
//        {
//            return ResponseEntity.badRequest().body(new GenericHttpResponse("Expand Provider", "Missing Provider ID", "failed"));
//        }
//        userService.processProviderUnlock(providerID, INTEREST_TYPE_EXPAND_PENDING_USER_CONFIRM);
//        return ResponseEntity.ok().body(new GenericHttpResponse("Expand Provider", "Request processed successfully", "success"));
//    }

//    @PostMapping ("/confirm-unlock-provider")
//    @Timed
//    public ResponseEntity<GenericHttpResponse> confirmUnlockProviders(@RequestParam long providerID) {
//        if(providerID <= 0)
//        {
//            return ResponseEntity.badRequest().body(new GenericHttpResponse("Unlock provider", "Missing Provider ID", "failed"));
//        }
//        userService.processProviderUnlock(providerID, INTEREST_TYPE_UNLOCK_PENDING);
//        return ResponseEntity.ok().body(new GenericHttpResponse("Unlock provider", "Request processed successfully", "success"));
//    }

//    @PostMapping ("/confirm-expand-provider")
//    @Timed
//    public ResponseEntity<GenericHttpResponse> confirmExpandProviders(@RequestParam long providerID) {
//        if(providerID <= 0)
//        {
//            return ResponseEntity.badRequest().body(new GenericHttpResponse("Expand provider", "Missing Provider ID", "failed"));
//        }
//        userService.processProviderUnlock(providerID, INTEREST_TYPE_EXPAND_PENDING);
//        return ResponseEntity.ok().body(new GenericHttpResponse("Expand provider", "Request processed successfully", "success"));
//    }
//    @PostMapping ("/users/cancel-unlock-provider")
//    @Timed
//    public void cancelUnlockProviders(@RequestParam long providerID) {
//        userService.processProviderUnlock(providerID, INTEREST_TYPE_UNLOCK, true);
//    }
//
//    @PostMapping ("/users/cancel-expand-provider")
//    @Timed
//    public void cancelExpandProviders(@RequestParam long providerID) {
//        userService.processProviderUnlock(providerID, INTEREST_TYPE_UNLOCK, true);
//    }

    @GetMapping ("/ur/approve-unlock-provider")
    @Timed
    public ResponseEntity<GenericHttpResponse> approveUnlockProvider(@RequestParam(value = "key") String key) {
        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Unlock provider", "Missing Provider ID", "failed"));
        }
        userProviderService.approveUnlockProviderRequest(key);
        return ResponseEntity.ok().body(new GenericHttpResponse("Unlock provider", "Request processed successfully", "success"));
    }

    @GetMapping ("/ur/approve-expand-provider")
    @Timed
    public ResponseEntity<GenericHttpResponse> approveExpandProvider(@RequestParam(value = "key") String key) {
        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Unlock provider", "Missing Provider ID", "failed"));
        }
        userProviderService.approveUnlockProviderRequest(key);
        return ResponseEntity.ok().body(new GenericHttpResponse("Unlock provider", "Request processed successfully", "success"));
    }


    @GetMapping ("/ur/reject-unlock-provider")
    @Timed
    public ResponseEntity<GenericHttpResponse> denyUnlockProvider(@RequestParam(value = "key") String key) {
        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Unlock provider", "Missing request ID", "failed"));
        }
        userProviderService.rejectUnlockProviderRequest(key);
        return ResponseEntity.ok().body(new GenericHttpResponse("Unlock provider", "Request processed successfully", "success"));
    }


    @PostMapping ("/contact-us-provider-more-info")
    @Timed
    public ResponseEntity<GenericHttpResponse> contactUsForProvider(@RequestBody UserRequest userRequest) {
        if(userRequest.getProviderID() <= 0)
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Need more provider info", "Missing Provider ID", "failed"));
        }
        userProviderService.processProviderMoreInfo(userRequest);
        return ResponseEntity.ok().body(new GenericHttpResponse("Need more provider info", "Request processed successfully", "success"));
    }

    @PostMapping ("/contact-us-request-header-info")
    @Timed
    public ResponseEntity<GenericHttpResponse> requestForProvider(@RequestBody UserRequest userRequest) {
        userProviderService.requestProviderMoreInfo(userRequest);
        return ResponseEntity.ok().body(new GenericHttpResponse("Request help Finding Data", "Request processed successfully", "success"));
    }

    @GetMapping ("/ur/resubmit-contact-us-provider-more-info")
    @Timed
    public ResponseEntity<GenericHttpResponse> resubmitContactUsForProvider(@RequestParam(value = "key") String key) {
        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Resubmit provider info request", "Missing request ID", "failed"));
        }

        userProviderService.resubmitProviderMoreInfo(key);
        return ResponseEntity.ok().body(new GenericHttpResponse("Resubmit provider info request", "Request processed successfully", "success"));
    }

    @GetMapping ("/ur/cancel-contact-us-provider-more-info")
    @Timed
    public ResponseEntity<GenericHttpResponse> cancelContactUsForProvider(@RequestParam(value = "key") String key) {
        if(StrUtils.isBlank(key))
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Cancel provider info request", "Missing request ID", "failed"));
        }

        userProviderService.cancelProcessProviderMoreInfo(key);
        return ResponseEntity.ok().body(new GenericHttpResponse("Cancel provider info request", "Request processed successfully", "success"));
    }

    @GetMapping ("/cancel-contact-us-provider-more-info")
    @Timed
    public ResponseEntity<GenericHttpResponse> cancelContactUsForProvider(@RequestParam long providerID,@RequestParam int interestType) {
        if(providerID <= 0)
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Cancel provider info request", "Missing provider ID", "failed"));
        }
        userProviderService.cancelProcessProviderMoreInfo(providerID,interestType);
        return ResponseEntity.ok().body(new GenericHttpResponse("Cancelled the request", "Request processed successfully", "success"));
    }

    @PostMapping ("/contact-us-category-unlock")
    @Timed
    public ResponseEntity<GenericHttpResponse> contactUsForCategory(@RequestBody UserRequest userRequest) {
        if(userRequest.getCategoryID() <= 0)
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Unlock category", "Missing Category ID", "failed"));
        }
        if(userRequest.getProviderID() <= 0)
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Unlock category", "Missing Provider ID", "failed"));
        }
        userRequest.setRequestTopic("Unlock category");
        userProviderService.processCategoryInterest(userRequest.getCategoryID(), userRequest.getProviderID(), INTEREST_TYPE_UNLOCK_PENDING_USER_CONFIRM);
        return ResponseEntity.ok().body(new GenericHttpResponse("Unlock category", "Request processed successfully", "success"));
    }

    @PostMapping ("/contact-us-provider-unlock")
    @Timed
    public ResponseEntity<GenericHttpResponse> updateUnlockProviders(@RequestBody UserRequest userRequest) {
        if(userRequest.getProviderID() <= 0)
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Unlock provider", "Missing Provider ID", "failed"));
        }
        //This status is currently not in use as we are not requesting the user to confirm the request, hence it directly goes to the amdin queue.
//        userService.processProviderUnlock(userRequest.getProviderID(), INTEREST_TYPE_UNLOCK_PENDING_USER_CONFIRM);
        userProviderService.processProviderUnlock(userRequest);
        return ResponseEntity.ok().body(new GenericHttpResponse("Unlock provider", "Request processed successfully", "success"));
    }

    @GetMapping ("/ur/admin-request-more-info")
    @Timed
    public ResponseEntity<GenericHttpResponse> AdminRequestMoreInfo(@RequestParam(value = "key") String key)
    {
        userProviderService.processNeedMoreInfoForUnlock(key);
        return ResponseEntity.ok().body(new GenericHttpResponse("Unlock provider need more info", "Request processed successfully", "success"));
    }

    /*@PostMapping ("/tag-user")
    @Timed
    public ResponseEntity<GenericHttpResponse> tagUser() {
        userProviderService.tagProviders();
        return ResponseEntity.ok().body(new GenericHttpResponse("Request help Finding Data", "Request processed successfully", "success"));
    }*/

    @PostMapping ("/follow-providerTag")
    @Timed
    public ResponseEntity<GenericHttpResponse> updateFollowedProvidersTag(@RequestParam long providerID) {
    	User user = userService.getUser();
        UserProvider up = userProviderRepository.findOneByUserIDEqualsAndProviderIDEqualsAndInterestTypeEquals(user.getId(), providerID, INTEREST_TYPE_FOLLOW);
        if(up!=null)
        {
            if (up.getInactive()) {
                userProviderService.followProvider(providerID);
            }
        }
        else
        {
            userProviderService.followProvider(providerID);
        }
    	return ResponseEntity.ok().body(new GenericHttpResponse("Follow Provider", "Request processed successfully", "success"));


    }
  @PostMapping ("/follow-providerTagss")
    @Timed
   // @Scheduled(cron = "0 0 7 * * ?")
     @Transactional
    public void getMyMatches() {
    	userProviderService.myMatchesProviderEmail();

    }
    
    @GetMapping("/providerpermission/{providerId}")
    @Timed
    public ResponseEntity<UserProvider> providerpermission(@PathVariable Long providerId) throws URISyntaxException, IOException {
    	User user = userService.getUser();
        UserProvider userProvider= userProviderService.getMyMatchesValue(user.getId(),providerId, INTEREST_TYPE_UNLOCK);
    	return new ResponseEntity<UserProvider>(userProvider, HttpStatus.OK);
    }
    
    @PostMapping ("/domain-operatorpermission")
    @Timed
    public void getAllUsers() {

    	List<User> user=userService.getAllUser();
    	userProviderService.addOperatorPermissionToUserDomain(user);
    }
    @GetMapping("/providerLogo/{providerId}")
    @Timed
    public ResponseEntity<DataProvider> providerLogo(@PathVariable Long providerId) throws URISyntaxException, IOException {
    	User user = userService.getUser();
        DataProvider userProvider= userProviderService.getLogo(providerId,user.getId());
    	return new ResponseEntity<DataProvider>(userProvider, HttpStatus.OK);
    }
    
    @GetMapping("/userProviderOperated/{id}")
    @Timed
    public ResponseEntity<User> providerDetails(@PathVariable Long id) throws URISyntaxException, IOException {
        User userProvider= userProviderService.getProviderDetails(id);
    	return new ResponseEntity<User>(userProvider, HttpStatus.OK);
    }
    
    @PostMapping ("/providerPermissionAccess")
    @Timed
    public ResponseEntity<User>providerPermissionAccess(@RequestBody User user) {

    	userProviderService.providerAccessPermission(user);
    	return new ResponseEntity<User>(user, HttpStatus.OK);

    }
    
}

