package com.ai.mp.web.rest;

import com.ai.mp.config.ApplicationProperties;
import com.ai.mp.domain.SocialUserConnection;
import com.ai.mp.repository.SocialUserConnectionRepository;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.security.UserNotActivatedException;
import com.ai.mp.service.SocialService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.social.support.URIBuilder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Resource to return information about the currently running Spring profiles.
 */
@RestController
@RequestMapping("/signin")
public class SocialResource {
    private final Logger log = LoggerFactory.getLogger(SocialResource.class);
    private final SocialService socialService;
    private final SocialUserConnectionRepository socialUserConnectionRepository;
    private final UserRepository userRepository;
    private final UserDetailsService userDetailsService;
    private final ApplicationProperties applicationProperties;
    public SocialResource(
        SocialService socialService,
        SocialUserConnectionRepository socialUserConnectionRepository,
        UserRepository userRepository,
        UserDetailsService userDetailsService,
        ApplicationProperties applicationProperties) {
        this.socialService = socialService;
        this.socialUserConnectionRepository = socialUserConnectionRepository;
        this.userRepository = userRepository;
        this.userDetailsService = userDetailsService;
        this.applicationProperties = applicationProperties;
    }

    @GetMapping("/linkedin")
    public RedirectView getActiveProfiles(@RequestParam(value = "code", required = false) String code, @RequestParam(value = "state") String state, @RequestParam(value = "error", required = false) String error, @CookieValue(name = "jhi-NG_TRANSLATE_LANG_KEY", required = false, defaultValue = "en") String langKey, @CookieValue(name = "ROLE", required = false) String role,@CookieValue(name="LINKEDIN",required=false)boolean linkedin,@CookieValue(name="NEWS",required=false)boolean news) {
     //   System.out.println("linkedin!!!!!!"+linkedin);
    //    System.out.println("news"+news);
    	if (code == null) {
            return new RedirectView(URIBuilder.fromUri("/#/")
                .build().toString(), true);
        } else {
            String access_token = socialService.getAccessToken(code, state);
            JSONObject userProfile = socialService.getUserProfile(access_token);
            JSONArray emailObject = socialService.getUserEmail(access_token).getJSONArray("elements");
            //System.out.println("profile//////"+userProfile);
            String preferredLanguage = userProfile.getJSONObject("firstName").getJSONObject("preferredLocale").getString("language");
            String preferredCountry = userProfile.getJSONObject("firstName").getJSONObject("preferredLocale").getString("country");
            String email = emailObject.getJSONObject(0).getJSONObject("handle~").getString("emailAddress");
            String firstName = userProfile.getJSONObject("firstName").getJSONObject("localized").getString(preferredLanguage + "_" + preferredCountry);
            String lastName = userProfile.getJSONObject("lastName").getJSONObject("localized").getString(preferredLanguage + "_" + preferredCountry);
            String providerUserId = userProfile.getString("id");
            JSONArray profilePicture = null;
            String imageUrl = "";
            try {
                profilePicture = userProfile.getJSONObject("profilePicture").getJSONObject("displayImage~").getJSONArray("elements");
                imageUrl = profilePicture.getJSONObject(0).getJSONArray("identifiers").getJSONObject(0).getString("identifier");
            } catch(JSONException e) {
                log.error("Profile picture not found!" + e.getMessage());
                profilePicture = null;
                imageUrl = "";
            }
           // System.out.println("FIRSTNAME>>>>>>"+firstName);
           // System.out.println("LASTNAME>>>>>>>"+lastName);
          //  System.out.println("EMAIL>>>>>>" + email);
           // System.out.println("PROVIDERUSERID>>"+providerUserId);
          //  System.out.println("IMAGEURL>>>>>>"+imageUrl);
          //  System.out.println("LANGUAGE>>>>>>"+langKey);
            if (email != null) {
                /*boolean socialUserExists = socialUserConnectionRepository.findSocialUserConnectionOneByUserId(email).isPresent();*/
                boolean mainUserExists = userRepository.findOneByEmail(email).isPresent();
                SocialUserConnection socialUser = new SocialUserConnection();
                socialUser.setAccessToken(access_token);
                socialUser.setDisplayName(firstName + " " + lastName);
                socialUser.setUserId(email);
                socialUser.getExpireTime();
                socialUser.setProviderId("linkedin");
                socialUser.setProviderUserId(providerUserId);
                socialUser.setImageURL(imageUrl);
                if(linkedin)
                {
                    socialUser.setRank((long) 1);

                    socialService.createSocialUserConnection(socialUser);

                }
                boolean socialUserExists = socialUserConnectionRepository.findSocialUserConnectionOneByUserId(email).isPresent();
                boolean userExists = socialService.createSocialLinkedinUser(socialUser, firstName, lastName, langKey, role,linkedin,mainUserExists,socialUserExists,news);
                if (mainUserExists && socialUserExists) {
                    UserDetails user = null;
                    try {
                        user = userDetailsService.loadUserByUsername(email);
                    } catch (UserNotActivatedException ex){
                        return new RedirectView(URIBuilder.fromUri("/#/social-register/linkedin")
                        .queryParam("success", "true")
                        .queryParam("active", "0")
                        .build().toString(), true);
                    }
                    Authentication newAuth = new UsernamePasswordAuthenticationToken(
                        user,
                        null,
                        user.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(newAuth);
                    return new RedirectView(URIBuilder.fromUri(applicationProperties.getRedirectUrl())
                        .build().toString(), true);
                }
                return new RedirectView(URIBuilder.fromUri("/#/social-register/linkedin")
                    .queryParam("success", "true")
                    .queryParam("existingUser", userExists ? "true" : "false")
                    .build().toString(), true);
            }
            return new RedirectView(URIBuilder.fromUri("/#/social-register/no-provider")
                    .queryParam("success", "false")
                    .build().toString(), true);
        }
    }
}
