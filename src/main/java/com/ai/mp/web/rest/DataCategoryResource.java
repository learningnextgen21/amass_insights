package com.ai.mp.web.rest;

import com.ai.mp.domain.CategoryLogo;
import com.ai.mp.domain.DataCategory;
import com.ai.mp.repository.CategoryLogoRepository;
import com.ai.mp.repository.DataCategoryRepository;
import com.ai.mp.repository.search.DataCategorySearchRepository;
import com.ai.mp.service.UserService;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing DataCategory.
 */
@RestController
@RequestMapping("/api")
public class DataCategoryResource {

    private final Logger log = LoggerFactory.getLogger(DataCategoryResource.class);
    private static final String ENTITY_NAME = "dataCategory";
    private final DataCategoryRepository dataCategoryRepository;
    private final UserService userService;
    private final DataCategorySearchRepository dataCategorySearchRepository;
    private final CategoryLogoRepository categoryLogoRepository;

    public DataCategoryResource(DataCategoryRepository dataCategoryRepository, DataCategorySearchRepository dataCategorySearchRepository, UserService userService,CategoryLogoRepository categoryLogoRepository) {
        this.dataCategoryRepository = dataCategoryRepository;
        this.dataCategorySearchRepository = dataCategorySearchRepository;
        this.userService = userService;
        this.categoryLogoRepository=categoryLogoRepository;

    }
    /**
     * POST  /data-categories : Create a new dataCategory.
     *
     * @param dataCategory the dataCategory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dataCategory, or with status 400 (Bad Request) if the dataCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/data-categories")
    @Timed
    public ResponseEntity<DataCategory> createDataCategory(@Valid @RequestBody DataCategory dataCategory) throws URISyntaxException {
        log.debug("REST request to save DataCategory : {}", dataCategory);
        if (dataCategory.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dataCategory cannot already have an ID")).body(null);
        }
        DataCategory result = dataCategoryRepository.save(dataCategory);
        dataCategorySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/data-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /data-categories : Updates an existing dataCategory.
     *
     * @param dataCategory the dataCategory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dataCategory,
     * or with status 400 (Bad Request) if the dataCategory is not valid,
     * or with status 500 (Internal Server Error) if the dataCategory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/data-categories")
    @Timed
    public ResponseEntity<DataCategory> updateDataCategory(@Valid @RequestBody DataCategory dataCategory) throws URISyntaxException {
        log.debug("REST request to update DataCategory : {}", dataCategory);
        if (dataCategory.getId() == null) {
            return createDataCategory(dataCategory);
        }
        DataCategory result = dataCategoryRepository.save(dataCategory);
        dataCategorySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataCategory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /data-categories : get all the dataCategories.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dataCategories in body
     */
    @GetMapping("/data-categories")
    @Timed
    public ResponseEntity<List<DataCategory>> getDataCategories(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DataCategories");
        Page<DataCategory> page = dataCategoryRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/data-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/all-data-categories")
    @Timed
    public ResponseEntity<List<DataCategory>> getAllDataCategories() {
        log.debug("REST request to get a page of DataCategories");
        List<DataCategory> page = dataCategoryRepository.findAll();
        for(DataCategory dc: page){
            CategoryLogo categoryLogo=categoryLogoRepository.findByCategoryRecordID(dc.getRecordID());
            dc.setLogo(categoryLogo);
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(page));
    }
    /**
     * GET  /data-categories/:id : get the "id" dataCategory.
     *
     * @param id the id of the dataCategory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dataCategory, or with status 404 (Not Found)
     */
    @GetMapping("/data-categories/{id}")
    @Timed
    public ResponseEntity<DataCategory> getDataCategory(@PathVariable Long id) {
        log.debug("REST request to get DataCategory : {}", id);
        DataCategory dataCategory = dataCategoryRepository.findOne(id);
        if(!userService.isAdminUser())
        {
            List<Long> categoryIDs = userService.getAllCategoryIDsForCurrentUser();
            if (!categoryIDs.contains(dataCategory.getId())) {
                dataCategory.setDatacategory(RandomStringUtils.randomAlphabetic(dataCategory.getDatacategory().length()));
                dataCategory.setExplanation(RandomStringUtils.randomAlphabetic(dataCategory.getDatacategory().length()));
                dataCategory.setLocked(true);
            }
        }
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataCategory));
    }

    /**
     * DELETE  /data-categories/:id : delete the "id" dataCategory.
     *
     * @param id the id of the dataCategory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/data-categories/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataCategory(@PathVariable Long id) {
        log.debug("REST request to delete DataCategory : {}", id);
        dataCategoryRepository.delete(id);
        dataCategorySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/data-categories?query=:query : search for the dataCategory corresponding
     * to the query.
     *
     * @param query the query of the dataCategory search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/data-categories")
    @Timed
    public ResponseEntity<List<DataCategory>> searchDataCategories(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataCategories for query {}", query);
        Page<DataCategory> page = dataCategorySearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/data-categories");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/data-categories/desc/{id}")
    @Timed
    public String getExplanationByID(@PathVariable Long id) {
        log.debug("REST request to search for a page of DataCategory for query {}", id);
        DataCategory element = dataCategorySearchRepository.findOne(id);
        return element.getExplanation();
    }
}
   /* @GetMapping ("/follow-category")
    @Timed
    public ResponseEntity<GenericHttpResponse> updateFollowedProviders(@RequestParam Long dataCategoryID) {
        if(dataCategoryID <= 0)
        {
            return ResponseEntity.badRequest().body(new GenericHttpResponse("Follow category", "Missing Category  ID", "failed"));
        }

        dataCategoryService.followCategory(dataCategoryID);
        //userProviderService.followProvider(providerID);
        return ResponseEntity.ok().body(new GenericHttpResponse("Follow category", "Request processed successfully", "success"));
    }
}*/
