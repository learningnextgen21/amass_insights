package com.ai.mp.web.rest;

import com.ai.mp.batch.DBUtils;
import com.ai.mp.config.Constants;
import com.ai.mp.domain.Authority;
import com.ai.mp.domain.CardDetails;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.DataProviderTag;
import com.ai.mp.domain.EmailAddress;
import com.ai.mp.domain.ExternalClient;
import com.ai.mp.domain.User;
import com.ai.mp.domain.UserCheckout;
import com.ai.mp.repository.CardRepository;
import com.ai.mp.repository.DataProviderTagRepository;
import com.ai.mp.repository.EmailAddressRepository;
import com.ai.mp.repository.ExternalClientRepository;
import com.ai.mp.repository.UserCheckoutRepository;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.repository.search.UserSearchRepository;
import com.ai.mp.security.AuthoritiesConstants;
import com.ai.mp.security.SecurityUtils;
import com.ai.mp.service.ElasticsearchGenericService;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.service.MailService;
import com.ai.mp.service.UserService;
import com.ai.mp.service.dto.UserDTO;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.ai.mp.web.rest.vm.ManagedUserVM;
import com.codahale.metrics.annotation.Timed;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;

import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.ai.mp.config.ElasticsearchConfiguration.INDEX_DATA_PROVIDER;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing users.
 * <p>
 * This class accesses the User entity, and needs to fetch its collection of authorities.
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no View Model and DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </ul>
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this case.
 */
@RestController
@RequestMapping("/api")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    private static final String ENTITY_NAME = "userManagement";
    
    @PersistenceContext
    EntityManager entityManager;


    private final UserRepository userRepository;

    private final CardRepository cardRepository;
    private final MailService mailService;

    private final UserService userService;

    private final UserSearchRepository userSearchRepository;
    private final ExternalClientRepository externalClientRepository;
    private final UserCheckoutRepository userCheckoutRepository;
    private final ElasticsearchGenericService service;
    private final DataProviderTagRepository dataProviderTagRepository;
    private final DBUtils dbutils;
    private final ElasticsearchIndexService elasticSearchIndexService;
    private final EmailAddressRepository emailAddressRepository;
    public UserResource(UserRepository userRepository, MailService mailService,
                        UserService userService, UserSearchRepository userSearchRepository,ExternalClientRepository externalClientRepository,UserCheckoutRepository userCheckoutRepository,CardRepository cardRepository,ElasticsearchGenericService service,DataProviderTagRepository dataProviderTagRepository,DBUtils dbutils,ElasticsearchIndexService elasticSearchIndexService,EmailAddressRepository emailAddressRepository) {
        this.userRepository = userRepository;
        this.externalClientRepository = externalClientRepository;
        this.mailService = mailService;
        this.userService = userService;
        this.userSearchRepository = userSearchRepository;
        this.userCheckoutRepository=userCheckoutRepository;
        this.cardRepository=cardRepository;
        this.service = service;
        this.dataProviderTagRepository = dataProviderTagRepository;
        this.dbutils = dbutils;		
        this.elasticSearchIndexService = elasticSearchIndexService;
        this.emailAddressRepository=emailAddressRepository;
    }

    /**
     * POST  /users  : Creates a new user.
     * <p>
     * Creates a new user if the login and email are not already used, and sends an
     * mail with an activation link.
     * The user needs to be activated on creation.
     *
     * @param managedUserVM the user to create
     * @return the ResponseEntity with status 201 (Created) and with body the new user, or with status 400 (Bad Request) if the login or email is already in use
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/users")
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity createUser(@Valid @RequestBody ManagedUserVM managedUserVM) throws URISyntaxException {
        log.debug("REST request to save User : {}", managedUserVM);

        if (managedUserVM.getId() != null) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new user cannot already have an ID"))
                .body(null);
            // Lowercase the user login before comparing with database
        } else if (userRepository.findOneByLogin(managedUserVM.getLogin().toLowerCase()).isPresent()) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "userexists", "Login already in use"))
                .body(null);
        } else if (userRepository.findOneByEmail(managedUserVM.getEmail()).isPresent()) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "emailexists", "Email already in use"))
                .body(null);
        } else {
            User newUser = userService.createUser(managedUserVM);
            mailService.sendCreationEmail(newUser);
            return ResponseEntity.created(new URI("/api/users/" + newUser.getLogin()))
                .headers(HeaderUtil.createAlert("userManagement.created", newUser.getLogin()))
                .body(newUser);
        }
    }

    /**
     * PUT  /users : Updates an existing User.
     *
     * @param managedUserVM the user to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated user,
     * or with status 400 (Bad Request) if the login or email is already in use,
     * or with status 500 (Internal Server Error) if the user couldn't be updated
     */
    @PutMapping("/users")
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody ManagedUserVM managedUserVM) {
        log.debug("REST request to update User : {}", managedUserVM);
        //System.out.println(managedUserVM.getAuthorities());
//        String[] external= managedUserVM.getAuthorities().toArray(new String[0]);
        String external = managedUserVM.getAuthorities().stream().filter(x -> (x.contains("ROLE_API_EXT")))
            .findAny().orElse(null);
        if (external != null) {
            //ExternalClient externalClient = new ExternalClient();
            User userOptional = userRepository.findUserByLogin(managedUserVM.getLogin());
            ExternalClient externalClient=externalClientRepository.findById(userOptional.getId());
            if(externalClient!=null) {
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date cdate = new Date();
                externalClient.setCreatedDate(cdate);
                Date edate = new Date();
                edate.setTime(edate.getTime() + 30L * 24 * 60 * 60 * 1000);
                externalClient.setExpiryDate(edate);
                Integer apilimits = 1000;
                externalClient.setApiLimit(apilimits);
                externalClientRepository.save(externalClient);
            }else
            {
                ExternalClient externalClients = new ExternalClient();
                externalClients.setId(userOptional.getId());
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date cdate = new Date();
                externalClients.setCreatedDate(cdate);
                Date edate = new Date();
                edate.setTime(edate.getTime() + 30L * 24 * 60 * 60 * 1000);
                externalClients.setExpiryDate(edate);
                Integer apilimits = 1000;
                externalClients.setApiLimit(apilimits);
                externalClientRepository.save(externalClients);
            }

        }
        Optional<User> existingUser = userRepository.findOneByEmail(managedUserVM.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getId().equals(managedUserVM.getId()))) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "emailexists", "Email already in use")).body(null);
        }
        existingUser = userRepository.findOneByLogin(managedUserVM.getLogin().toLowerCase());
        if (existingUser.isPresent() && (!existingUser.get().getId().equals(managedUserVM.getId()))) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "userexists", "Login already in use")).body(null);
        }
        boolean activateUser = false;

        if (!existingUser.get().getActivated() && managedUserVM.isActivated()) {
            activateUser = true;
        }
        Optional<UserDTO> updatedUser = userService.updateUser(managedUserVM, activateUser);
        return ResponseUtil.wrapOrNotFound(updatedUser,
            HeaderUtil.createAlert("userManagement.updated", managedUserVM.getLogin()));
    }

    /**
     * GET  /users : get all users.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all users
     */
    @GetMapping("/users")
    @Timed
    public ResponseEntity<List<UserDTO>> getAllUsers(@ApiParam Pageable pageable) {
        final Page<UserDTO> page = userService.getAllManagedUsers(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * @return a string list of the all of the roles
     */
    @GetMapping("/users/authorities")
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    public List<String> getAuthorities() {
        return userService.getAuthorities();
    }

    /**
     * GET  /users/:login : get the "login" user.
     *
     * @param login the login of the user to find
     * @return the ResponseEntity with status 200 (OK) and with body the "login" user, or with status 404 (Not Found)
     */
    @GetMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
    @Timed
    public ResponseEntity<UserDTO> getUser(@PathVariable String login) {
        log.debug("REST request to get User : {}", login);
        return ResponseUtil.wrapOrNotFound(
            userService.getUserWithAuthoritiesByLogin(login)
                .map(UserDTO::new));
    }

    /**
     * DELETE /users/:login : delete the "login" User.
     *
     * @param login the login of the user to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<Void> deleteUser(@PathVariable String login) {
        log.debug("REST request to delete User: {}", login);
        userService.deleteUser(login);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("userManagement.deleted", login)).build();
    }

    /**
     * SEARCH  /_search/users/:query : search for the User corresponding
     * to the query.
     *
     * @param query the query to search
     * @return the result of the search
     */
    @GetMapping("/_search/users/{query}")
    @Timed
    public List<User> search(@PathVariable String query) {
        return StreamSupport
            .stream(userSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    @PostMapping("/usercheckout")
    @Timed
    public ResponseEntity<?>usercheckout(@RequestParam(value="name")String name)
    {
        final String user = SecurityUtils.getCurrentUserLogin();
        User users=userRepository.findByLogin(user);
        if(users!=null)
        {
            UserCheckout userCheckout=new UserCheckout();
            userCheckout.setUser(users);
            userCheckout.setSubscription(name);
            userCheckout.setSubscribed(true);
            if(name.equalsIgnoreCase("Bronze"))
            {
                userCheckout.setExpirationDate(LocalDate.now( )
                    .plusMonths( 1 ));
                userCheckout.setAmount(999);;

            }
            if(name.equalsIgnoreCase("STARTER"))
            {
            	 userCheckout.setExpirationDate(LocalDate.now( )
                         .plusMonths( 1 ));
                userCheckout.setAmount(1000);
            }
            userCheckoutRepository.save(userCheckout);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.ACCEPTED);

    }

    @GetMapping("/checkout")
    @Timed
    public ResponseEntity<UserCheckout>checkout()
    {
        String user=SecurityUtils.getCurrentUserLogin();
        User users=userRepository.findByLogin(user);
        UserCheckout userCheckout=userCheckoutRepository.finduser(users.getId());
        return new ResponseEntity<UserCheckout>(userCheckout, HttpStatus.OK);
        //return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userCheckout));
    }
    
    @GetMapping("/userAuthority")
    @Timed
    public ResponseEntity<Boolean> authority()
    {
    	String user=SecurityUtils.getCurrentUserLogin();
        User users=userRepository.findByLogin(user);
       User userAuthoriry= userRepository.findUser(users.getId());
       boolean roleLevel=false;
       for(Authority authority:userAuthoriry.getAuthorities())
       {
    	if(authority.getRoleLevel()>=200)
    	{
    		roleLevel=true;
    	}
       }
       return new ResponseEntity<Boolean>(roleLevel, HttpStatus.OK);
        
    }
    @GetMapping("/restrictedAuthority")
    @Timed
    public ResponseEntity<Boolean> restrictedAuthority()
    {
    	String user=SecurityUtils.getCurrentUserLogin();
        User users=userRepository.findByLogin(user);
       User userAuthoriry= userRepository.findUser(users.getId());
       boolean roleLevel=false;
       for(Authority authority:userAuthoriry.getAuthorities())
       {
    	if(authority.getName().equalsIgnoreCase("ROLE_R_SILVER"))
    	{
    		roleLevel=true;
    	}
       }
       return new ResponseEntity<Boolean>(roleLevel, HttpStatus.OK);
        
    }
    @PostMapping ("/cardDetails")
    @Timed
    public ResponseEntity<CardDetails>cardDetails(@RequestBody CardDetails cardDetails)
    {
    	CardDetails card=cardRepository.save(cardDetails);
		return new ResponseEntity<CardDetails>(card, HttpStatus.OK);
    	
    }
    @PutMapping ("/saveData")
    @Timed
    public ResponseEntity<User>saveData(@RequestBody User userData)
    {
    	String user=SecurityUtils.getCurrentUserLogin();
        User users=userRepository.findByLogin(user);
        if(null!=userData.getDataRequests())
        {
        users.setDataRequests(userData.getDataRequests());
        }
        if(null!=userData.getDataRequirements())
        {
   users.setDataRequirements(userData.getDataRequirements());
        }
        if(null!=userData.getCategories())
        {
   users.setCategories(userData.getCategories());
        }
        if(null!=userData.getAssetClasses())
        {
        	users.setAssetClasses(userData.getAssetClasses());
        }
        if(null!=userData.getGeographies())
        {
        	users.setGeographies(userData.getGeographies());
        }
        if(null!=userData.getSectors())
        {
        	users.setSectors(userData.getSectors());
        }
        if(null!=userData.getFundName())
        {
        	users.setFundName(userData.getFundName());
        }
   User userUpdated=userRepository.save(users);
return new ResponseEntity<User>(userUpdated, HttpStatus.OK);
    }
    @GetMapping("/userRetrieve")
    @Timed
    public ResponseEntity<User>userDate()
    {
    	String user=SecurityUtils.getCurrentUserLogin();
        User users=userRepository.findByLogin(user);
        User usersData=userRepository.findUserData(users.getId());
        String query=service.matchSearch();
        JsonObject jsonObject = service.search(query, INDEX_DATA_PROVIDER, false, false, false);
        JsonArray hitsArray = jsonObject.getAsJsonObject("hits").getAsJsonArray("hits");
        if(hitsArray.size()<=0)
        {
        	usersData.setMyMatchesCount(true);
        }
		return  new ResponseEntity<User>(usersData, HttpStatus.OK);
    }
    
    @PutMapping("/userOnboarding")
    @Timed
    public ResponseEntity<User>userOnboarding(@RequestBody User userData)
    {
    	String user=SecurityUtils.getCurrentUserLogin();
        User users=userRepository.findByLogin(user);
       // User usersData=userRepository.findUserData(users.getId());
        boolean value= false;
        userData.setUserOnboarding(true);
       User userOnboarding= userRepository.save(userData);
       User usersData=userRepository.findUserData(userOnboarding.getId());
       usersData.setMymatchSearch(true);
       usersData=userRepository.save(usersData);
       Authority authoritiess= usersData.getAuthorities().stream().filter(x -> (x.getName().contains("ROLE_INVESTOR")))
               .findAny().orElse(null);
       if(authoritiess !=null)
       {
    	   value=true;   
       }
       mailService.inviteUseronboardingadmin(userOnboarding,value);
      
       DataProviderTag dataProviderTag=dataProviderTagRepository.findUserValue(usersData.getId());
       String query=service.matchSearch();
       //System.out.println("query"+query);
       Gson gson = new Gson();
       JsonObject jsonObject = service.search(query, INDEX_DATA_PROVIDER, false, false, false);
       JsonArray hitsArray = jsonObject.getAsJsonObject("hits").getAsJsonArray("hits");
       //System.out.println("hitsArray"+hitsArray.size());
       for (JsonElement e : hitsArray) {
    	  // System.out.println("elasticcalled");
           JsonObject obj = e.getAsJsonObject();
           JsonObject sourceObject = obj.getAsJsonObject("_source");
           
           DataProvider provider = gson.fromJson(sourceObject, DataProvider.class);
           if(!users.getUserOnboarding())
           { 
        	  // System.out.println("useronboarding");
         int result= dataProviderTagRepository.insetProviderTag(dataProviderTag.getId(),provider.getId());
         if (result==1) {
        	 elasticSearchIndexService.reindexSingleProvider(provider.getId());
         }
           }

       }
       dbutils.populateDataModelJson();

		return  new ResponseEntity<User>(userOnboarding, HttpStatus.OK);

    }
    @PutMapping("/privacy")
    @Timed
    public ResponseEntity<User>user(@RequestBody User userData)
    {
       User userOnboarding= userRepository.save(userData);
		return  new ResponseEntity<User>(userOnboarding, HttpStatus.OK);

    }
    @GetMapping("/checkUser")
    @Timed
    public ResponseEntity<User>checkUser()
    {
    	String user=SecurityUtils.getCurrentUserLogin();
        User users=userRepository.findByLogin(user);
  		return  new ResponseEntity<User>(users, HttpStatus.OK);
    }
    @PutMapping("/activateMymatches")
    @Timed
    public ResponseEntity<User>activateMymatches()
    {
    	String user=SecurityUtils.getCurrentUserLogin();
        User users=userRepository.findByLogin(user);
        User userData=userRepository.findUserData(users.getId());
        userData.setOnBoarding(true);
       User userOnboarding= userRepository.save(userData);
		return  new ResponseEntity<User>(userOnboarding, HttpStatus.OK);

    }
    
	@GetMapping("/checkUserTag")
    @Timed
    public ResponseEntity<User>checkUserTag()
    {
        Query q = entityManager.createNativeQuery("select distinct by_user_id  from data_provider_tag where privacy='Private'");
        List<?> authors = q.getResultList();
        User user = new User();
        for(int i=0; i<authors.size();i++)
    	{
            BigInteger row =  (BigInteger) authors.get(i);
            user.setId((row.longValue()));
            User users=userRepository.findOne(user.getId());
            if(null!=users) {
            	
            }
            else
            {
            	dataProviderTagRepository.deleteByUserId(user.getId());
            }

    	}
		return null;
    }
	  @DeleteMapping("/deleteUser")
	  @Timed
	  public ResponseEntity<?> deleteUsers(@RequestParam(value="login")String login) {
		 // System.out.println("login"+login);
	        User users=userRepository.findByLogin(login);
	        //System.out.println("user"+users.getId());
	        userRepository.delete(users.getId());
	       EmailAddress emailAddress= emailAddressRepository.findByUser(users.getId());
	       emailAddress.setUser(null);
	       emailAddressRepository.save(emailAddress);
		return new ResponseEntity<>(HttpStatus.OK);
		  
	  }
	  @PutMapping("/providerOnboarding")
	    @Timed
	    public ResponseEntity<User>providerOnboarding(@RequestBody User userData,@RequestParam(value = "checkBox", required = false) String checkBox)
	    {
	       User providerOnboarding= userRepository.save(userData);
	       if(checkBox.equalsIgnoreCase("providerUserOnboardingForm"))
	       {
		       mailService.inviteProvideronboardingAdmin(providerOnboarding);
	       }
			return  new ResponseEntity<User>(providerOnboarding, HttpStatus.OK);

	    }
	  
	  
	  @GetMapping("/checkUserById/{id}")
	    @Timed
	    public ResponseEntity<User>checkUserById(@PathVariable Long id)
	    {
	        User users=userRepository.findUserData(id);
	  		return  new ResponseEntity<User>(users, HttpStatus.OK);
	    }
	  
}
