package com.ai.mp.web.rest;

import com.ai.mp.domain.DataFeature;
import com.ai.mp.repository.DataFeatureRepository;
import com.ai.mp.repository.search.DataFeatureSearchRepository;
import com.ai.mp.web.rest.util.HeaderUtil;
import com.ai.mp.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing DataFeature.
 */
@RestController
@RequestMapping("/api")
public class DataFeatureResource {

    private final Logger log = LoggerFactory.getLogger(DataFeatureResource.class);

    private static final String ENTITY_NAME = "dataFeature";

    private final DataFeatureRepository dataFeatureRepository;

    private final DataFeatureSearchRepository dataFeatureSearchRepository;
    public DataFeatureResource(DataFeatureRepository dataFeatureRepository, DataFeatureSearchRepository dataFeatureSearchRepository) {
        this.dataFeatureRepository = dataFeatureRepository;
        this.dataFeatureSearchRepository = dataFeatureSearchRepository;
    }

    /**
     * POST  /data-features : Create a new dataFeature.
     *
     * @param dataFeature the dataFeature to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dataFeature, or with status 400 (Bad Request) if the dataFeature has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/data-features")
    @Timed
    public ResponseEntity<DataFeature> createDataFeature(@Valid @RequestBody DataFeature dataFeature) throws URISyntaxException {
        log.debug("REST request to save DataFeature : {}", dataFeature);
        if (dataFeature.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dataFeature cannot already have an ID")).body(null);
        }
        DataFeature result = dataFeatureRepository.save(dataFeature);
        dataFeatureSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/data-features/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /data-features : Updates an existing dataFeature.
     *
     * @param dataFeature the dataFeature to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dataFeature,
     * or with status 400 (Bad Request) if the dataFeature is not valid,
     * or with status 500 (Internal Server Error) if the dataFeature couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/data-features")
    @Timed
    public ResponseEntity<DataFeature> updateDataFeature(@Valid @RequestBody DataFeature dataFeature) throws URISyntaxException {
        log.debug("REST request to update DataFeature : {}", dataFeature);
        if (dataFeature.getId() == null) {
            return createDataFeature(dataFeature);
        }
        DataFeature result = dataFeatureRepository.save(dataFeature);
        dataFeatureSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataFeature.getId().toString()))
            .body(result);
    }

    /**
     * GET  /data-features : get all the dataFeatures.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dataFeatures in body
     */
    @GetMapping("/data-features")
    @Timed
    public ResponseEntity<List<DataFeature>> getAllDataFeatures(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of DataFeatures");
        Page<DataFeature> page = dataFeatureRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/data-features");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /data-features/:id : get the "id" dataFeature.
     *
     * @param id the id of the dataFeature to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dataFeature, or with status 404 (Not Found)
     */
    @GetMapping("/data-features/{id}")
    @Timed
    public ResponseEntity<DataFeature> getDataFeature(@PathVariable Long id) {
        log.debug("REST request to get DataFeature : {}", id);
        DataFeature dataFeature = dataFeatureRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataFeature));
    }

    /**
     * DELETE  /data-features/:id : delete the "id" dataFeature.
     *
     * @param id the id of the dataFeature to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/data-features/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataFeature(@PathVariable Long id) {
        log.debug("REST request to delete DataFeature : {}", id);
        dataFeatureRepository.delete(id);
        dataFeatureSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/data-features?query=:query : search for the dataFeature corresponding
     * to the query.
     *
     * @param query the query of the dataFeature search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/data-features")
    @Timed
    public ResponseEntity<List<DataFeature>> searchDataFeatures(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of DataFeatures for query {}", query);
        Page<DataFeature> page = dataFeatureSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/data-features");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/data-features/desc/{id}")
    @Timed
    public String getExplanationByID(@PathVariable Long id) {
        log.debug("REST request to search for a page of DataFeatures for query {}", id);
        DataFeature element = dataFeatureSearchRepository.findOne(id);
        return element.getExplanation();
    }
}
