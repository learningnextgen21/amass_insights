package com.ai.mp.web.rest;

import com.ai.mp.repository.SocialUserConnectionRepository;
import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import com.ai.mp.domain.EmailAddress;
import com.ai.mp.domain.PersistentToken;
import com.ai.mp.domain.User;
import com.ai.mp.repository.EmailAddressRepository;
import com.ai.mp.repository.PersistentTokenRepository;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.security.SecurityUtils;
import com.ai.mp.service.MailService;
import com.ai.mp.service.UserProviderService;
import com.ai.mp.service.UserService;
import com.ai.mp.service.dto.UserDTO;
import com.ai.mp.web.rest.vm.KeyAndPasswordVM;
import com.ai.mp.web.rest.vm.ManagedUserVM;
import com.ai.util.Utils;
import com.ai.mp.web.rest.util.HeaderUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.*;

import javax.naming.LinkException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final UserRepository userRepository;
    private final Environment environment;
    private final UserService userService;
    private static final String EMAIL_REGEX =
        "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*" +  // local-part
            "@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";	// domain part

    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);

    private static final String ENTITY_NAME = "user";

    private final MailService mailService;
    private final EmailAddressRepository emailAddressRepository;

    private final SocialUserConnectionRepository socialUserConnectionRepository;

    private final PersistentTokenRepository persistentTokenRepository;
    
    private final UserProviderService userProviderService;

    private static final String CHECK_ERROR_MESSAGE = "Incorrect password";

    public AccountResource(UserRepository userRepository, UserService userService,
                           MailService mailService, PersistentTokenRepository persistentTokenRepository, SocialUserConnectionRepository socialUserConnectionRepository, Environment environment,
                           EmailAddressRepository emailAddressRepository,UserProviderService userProviderService) {

        this.environment = environment;
        this.userRepository = userRepository;
        this.userService = userService;
        this.mailService = mailService;
        this.persistentTokenRepository = persistentTokenRepository;
        this.socialUserConnectionRepository = socialUserConnectionRepository;
        this.emailAddressRepository=emailAddressRepository;
        this.userProviderService=userProviderService;
    }

    /**
     * POST  /register : register the user.
     *
     * @param managedUserVM the managed user View Model
     * @return the ResponseEntity with status 201 (Created) if the user is registered or 400 (Bad Request) if the login or email is already in use
     */
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @PostMapping(path = "/register",
        produces={MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    @Timed
    public ResponseEntity registerAccount(@Valid @RequestBody ManagedUserVM managedUserVM) {

        HttpHeaders textPlainHeaders = new HttpHeaders();
        textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);
        if (!checkPasswordLength(managedUserVM.getPassword())) {
            return new ResponseEntity<>(CHECK_ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
        }
        return userRepository.findOneByLogin(managedUserVM.getLogin().toLowerCase())
            .map(user -> new ResponseEntity<>("login already in use", textPlainHeaders, HttpStatus.BAD_REQUEST))
            .orElseGet(() -> userRepository.findOneByEmail(managedUserVM.getEmail())
                    .map(user -> new ResponseEntity<>("email address already in use", textPlainHeaders, HttpStatus.BAD_REQUEST))
                    .orElseGet(() -> {
//                    User user = userService
//                        .createUser(managedUserVM.getLogin(), managedUserVM.getPassword(),
//                            managedUserVM.getFirstName(), managedUserVM.getLastName(),
//                            managedUserVM.getEmail().toLowerCase(), managedUserVM.getImageUrl(),
//                            managedUserVM.getLangKey(), managedUserVM.getCompany());
                        User user = userService.createUser(managedUserVM, managedUserVM.getPassword());
                        mailService.sendCreationEmail(user);
                        //userService.sentSubscriptionConfirmationEmail(user);
                        return new ResponseEntity<>(HttpStatus.CREATED);
                    })
            );
    }

    /**
     * GET  /activate : activate the registered user.
     *
     * @param key the activation key
     * @return the ResponseEntity with status 200 (OK) and the activated user in body, or status 500 (Internal Server Error) if the user couldn't be activated
     */

    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @GetMapping("/activate")
    @Timed
    public ResponseEntity<String> activateAccount(@RequestParam(value = "key") String key) {
        return userService.activateRegistration(key)
            .map(user -> new ResponseEntity<String>(HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @GetMapping("/confirm")
    @Timed
    public ResponseEntity<String> confirmAccount(@RequestParam(value = "key") String key,@CookieValue(name = "ROLE", required = false) String role) {
        return userService.confirmRegistration(key,role)
            .map(user -> new ResponseEntity<String>(HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * GET  /authenticate : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request
     * @return the login if the user is authenticated
     */

    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @GetMapping("/authenticate")
    @Timed
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
     * GET  /account : get the current user.
     *
     * @return the ResponseEntity with status 200 (OK) and the current user in body, or status 500 (Internal Server Error) if the user couldn't be returned
     */

    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @GetMapping("/account")
    @Timed
    public ResponseEntity<UserDTO> getAccount() {
        return Optional.ofNullable(userService.getUserWithAuthorities())
            .map(user -> new ResponseEntity<>(new UserDTO(user), HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * POST  /account : update the current user information.
     *
     * @param userDTO the current user information
     * @return the ResponseEntity with status 200 (OK), or status 400 (Bad Request) or 500 (Internal Server Error) if the user couldn't be updated
     */
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @PostMapping("/account")
    @Timed
    @ResponseStatus(code = HttpStatus.CONFLICT)
    public ResponseEntity saveAccount(@Valid @RequestBody UserDTO userDTO) {
        final String userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<User> existingUser = userRepository.findOneByEmail(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("user-management", "emailexists", "Email already in use")).body(null);
        }
        return userRepository
            .findOneByLogin(userLogin)
            .map(u -> {
                userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(),userDTO.getCompany(), userDTO.getEmail(), userDTO.isNotify_news_update(), userDTO.getLangKey(), userDTO.getImageUrl());
                return new ResponseEntity(HttpStatus.OK);

            })
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * POST  /account/change_password : changes the current user's password
     *     * @param password the new password
     * @return the ResponseEntity with status 200 (OK), or status 400 (Bad Request) if the new password is not strong enough
     */
    /*@ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @PostMapping(path = "/account/change_password",
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public ResponseEntity changePassword(@RequestBody String password) {
        if (!checkPasswordLength(password)) {
            return new ResponseEntity<>(CHECK_ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
        }
        userService.changePassword(password);
        return new ResponseEntity<>(HttpStatus.OK);
    }*/


    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @PostMapping(path = "/account/change_password",
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public ResponseEntity<?> changePassword(@RequestBody String password,@RequestParam String currentPassword) {
        if (!checkPasswordLength(password)) {
            return new ResponseEntity<>(CHECK_ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
        }

        User user=userRepository.findByLogin(SecurityUtils.getCurrentUserLogin());
        boolean currentPasswordCheck= BCrypt.checkpw(currentPassword, user.getPassword());
        if(currentPasswordCheck)
        {
            userService.changePassword(password);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * GET  /account/sessions : get the current open sessions.
     *
     * @return the ResponseEntity with status 200 (OK) and the current open sessions in body,
     *  or status 500 (Internal Server Error) if the current open sessions couldn't be retrieved
     */
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @GetMapping("/account/sessions")
    @Timed
    public ResponseEntity<List<PersistentToken>> getCurrentSessions() {
        return userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin())
            .map(user -> new ResponseEntity<>(
                persistentTokenRepository.findByUser(user),
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * DELETE  /account/sessions?series={series} : invalidate an existing session.
     *
     * - You can only delete your own sessions, not any other user's session
     * - If you delete one of your existing sessions, and that you are currently logged in on that session, you will
     *   still be able to use that session, until you quit your browser: it does not work in real time (there is
     *   no API for that), it only removes the "remember me" cookie
     * - This is also true if you invalidate your current session: you will still be able to use it until you close
     *   your browser or that the session times out. But automatic login (the "remember me" cookie) will not work
     *   anymore.
     *   There is an API to invalidate the current session, but there is no API to check which session uses which
     *   cookie.
     *
     * @param series the series of an existing session
     * @throws UnsupportedEncodingException if the series couldnt be URL decoded
     */
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @DeleteMapping("/account/sessions/{series}")
    @Timed
    public void invalidateSession(@PathVariable String series) throws UnsupportedEncodingException {
        String decodedSeries = URLDecoder.decode(series, "UTF-8");
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(u ->
            persistentTokenRepository.findByUser(u).stream()
                .filter(persistentToken -> StringUtils.equals(persistentToken.getSeries(), decodedSeries))
                .findAny().ifPresent(t -> persistentTokenRepository.delete(decodedSeries)));
    }

    /**
     * POST   /account/reset_password/init : Send an email to reset the password of the user
     *
     * @param mail the mail of the user
     * @return the ResponseEntity with status 200 (OK) if the email was sent, or status 400 (Bad Request) if the email address is not registered
     */

    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @PostMapping(path = "/account/reset_password/init",
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public ResponseEntity requestPasswordReset(@RequestBody String mail) {
        User currentUser = userService.getUser(mail);
        if(currentUser != null && !currentUser.getActivated()) {
            String message = "Your account has not yet been activated by the Amass admins. Please allow for at least 24 hours to verify your account. If you think this is an error, <a href='" + environment.getProperty("spring.application.url") + "/#/contact-us'>Contact Us</a>";
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        if(!socialUserConnectionRepository.findAllByUserIdOrderByProviderIdAscRankAsc(mail).isEmpty()){
            return new ResponseEntity<>("Your email is associated with a registered LinkedIn account. Please Login with LinkedIn.", HttpStatus.BAD_REQUEST);
        }
        return userService.requestPasswordReset(mail)
            .map(user -> {
                mailService.sendPasswordResetMail(user);
                return new ResponseEntity<>("email was sent", HttpStatus.OK);
            }).orElse(new ResponseEntity<>("email address not registered", HttpStatus.BAD_REQUEST));
    }

    /**
     * POST   /account/reset_password/finish : Finish to reset the password of the user
     *
     * @param keyAndPassword the generated key and the new password
     * @return the ResponseEntity with status 200 (OK) if the password has been reset,
     * or status 400 (Bad Request) or 500 (Internal Server Error) if the password could not be reset
     */
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 409, message = "The request could not be completed due to a conflict with the current state of the target resource")})
    @PostMapping(path = "/account/reset_password/finish",
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public ResponseEntity<String> finishPasswordReset(@RequestBody KeyAndPasswordVM keyAndPassword) {
        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            return new ResponseEntity<>(CHECK_ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
        }
        return userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey())
            .map(user -> {


                mailService.sendPasswordResetConfirmationMail(user);
                return new ResponseEntity<String>(HttpStatus.OK);
            })
            .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));

    }

    private boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) &&
            password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH &&
            password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
    }

    @PostMapping("/activationLink")
    @Timed
    public ResponseEntity<String> activationLink(@RequestParam(value = "email")String email,@RequestParam(value="invite_type") String inviteType)
    {
        if (!emailValidator(email))
        {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        EmailAddress address= emailAddressRepository.findByEmailAddress(email);
        User userLogin=userRepository.findByLogin(email);


        User user=new User();

        if((null==address)&&(null==userLogin))
        {
            EmailAddress emailAddress=new EmailAddress();
            user= userService.createAccountUser(email,inviteType);
            emailAddress.setUser(user.getId());
            emailAddress.setEmailAddress(email);
            emailAddress.setRecordID("rece"+ Utils.randomString(13,13));
            user.setFirstName("User");
            emailAddressRepository.save(emailAddress);
            if(inviteType.equalsIgnoreCase("Provider"))
            {
                mailService.sendUserProviderActivation(user,inviteType);
            }
            if(inviteType.equalsIgnoreCase("Investor"))
            {
            	mailService.sendInvestor(user,inviteType);
            }
            else if(inviteType.equalsIgnoreCase("default")) {
                mailService.sendUserAccountActivation(user,inviteType);
            }
        }

        else if(null==address)
        {
            if(null!=userLogin.getId())
            {
                EmailAddress emailAddress=new EmailAddress();
                emailAddress.setEmailAddress(email);
                emailAddress.setUser(userLogin.getId());
                emailAddressRepository.save(emailAddress);
                if(userLogin.getActivated())
                {
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

                }
                if(inviteType.equalsIgnoreCase("Provider"))
                {
                    mailService.sendUserProviderActivation(user,inviteType);
                }
                if(inviteType.equalsIgnoreCase("Investor"))
                {
                	mailService.sendInvestor(user,inviteType);
                }
                else if(inviteType.equalsIgnoreCase("default")) {
                    mailService.sendUserAccountActivation(userLogin,inviteType);
                }
            }
        }else if(null==address.getUser()) {

            if(null==userLogin){
                User users=new User();
                users= userService.createAccountUser(email,inviteType);
                EmailAddress emailExist= emailAddressRepository.findByEmailAddress(email);
                emailExist.setUser(users.getId());
                emailAddressRepository.save(emailExist);
                if(inviteType.equalsIgnoreCase("Provider"))
                {
                    mailService.sendUserProviderActivation(users,inviteType);

                }
                if(inviteType.equalsIgnoreCase("Investor"))
                {
                    mailService.sendInvestor(users,inviteType);

                }
                else if(inviteType.equalsIgnoreCase("default")) {
                    mailService.sendUserAccountActivation(users,inviteType);
                }
            }
            else if(null!=userLogin)
            {
                EmailAddress emailExist= emailAddressRepository.findByEmailAddress(email);
                emailExist.setUser(userLogin.getId());
                emailAddressRepository.save(emailExist);
                if(userLogin.getActivated())
                {
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

                }
                if(inviteType.equalsIgnoreCase("Provider"))
                {
                    mailService.sendUserProviderActivation(userLogin,inviteType);
                }
                if(inviteType.equalsIgnoreCase("Investor"))
                {
                    mailService.sendInvestor(userLogin,inviteType);

                }
                else if(inviteType.equalsIgnoreCase("default")){
                    mailService.sendUserAccountActivation(userLogin,inviteType);
                }
            }
        } else if((null!=address.getUser())&&(null==userLogin))
        {
            User userActivate=userService.createAccountUser(email,inviteType);
            EmailAddress emailExist= emailAddressRepository.findByEmailAddress(email);
            emailExist.setUser(userActivate.getId());
            emailAddressRepository.save(emailExist);
            if(inviteType.equalsIgnoreCase("Provider"))
            {
                mailService.sendUserProviderActivation(userActivate,inviteType);
            }
            if(inviteType.equalsIgnoreCase("Investor"))
            {
                mailService.sendInvestor(userActivate,inviteType);

            }
            else if(inviteType.equalsIgnoreCase("default")){
                mailService.sendUserAccountActivation(userActivate,inviteType);
            }
        }
        else if(null!=userLogin.getConfirmationKey())
        {
            if(inviteType.equalsIgnoreCase("Provider"))
            {
                mailService.sendUserProviderActivation(userLogin,inviteType);
            }
            if(inviteType.equalsIgnoreCase("Investor"))
            {
                mailService.sendInvestor(userLogin,inviteType);

            }
            else if(inviteType.equalsIgnoreCase("default")) {
                mailService.sendUserAccountActivation(userLogin,inviteType);
            }

        }else {
            return new ResponseEntity<>("email address is already associated",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.ACCEPTED);

    }
    public static boolean emailValidator(String email) {

        if (email == null) {
            return false;
        }

        Matcher matcher = EMAIL_PATTERN.matcher(email);
        return matcher.matches();
    }
    @PostMapping("/getLinkedIn")
    @Timed
    public ResponseEntity<String> getLinkedIn() throws LinkException
    {
        mailService.sendlinkedIn();
        return new ResponseEntity<>(HttpStatus.ACCEPTED);


    }
    
    @PostMapping("/grantOperator")
    @Timed
    public ResponseEntity<String> grantOperator(@RequestParam(value = "userId", required = false) Long userId, @RequestParam(value = "providerId", required = false) Long providerId)
    {
           userProviderService.grantOperatorPermission(userId,providerId);
    	return new ResponseEntity<>(HttpStatus.ACCEPTED);


    }
    
}

