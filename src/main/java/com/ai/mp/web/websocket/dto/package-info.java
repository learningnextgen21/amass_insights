/**
 * Data Access Objects used by WebSocket services.
 */
package com.ai.mp.web.websocket.dto;
