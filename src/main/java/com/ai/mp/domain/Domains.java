package com.ai.mp.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.google.gson.annotations.JsonAdapter;

@Entity
@Table(name="data_domains")
@Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="domains")
public class Domains implements Serializable{
	 

	private static final long serialVersionUID = 1L;

	@Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;
	
	@Column(name = "domain")
	private String domain;
	
	
	 @ManyToMany(fetch=FetchType.LAZY)
	  @JoinTable(name="data_investor_domain",joinColumns=@JoinColumn(name="domain_id",referencedColumnName="id"),
	   inverseJoinColumns=@JoinColumn(name="investor_id",referencedColumnName="id"))
	private Set<DataInvestmentManager> ownerInvestmentManager;
	
	 @ManyToMany(fetch=FetchType.LAZY)
	  @JoinTable(name="data_provider_domain",joinColumns=@JoinColumn(name="domain_id",referencedColumnName="id"),
	   inverseJoinColumns=@JoinColumn(name="provider_id",referencedColumnName="id"))
	private Set<DataProvider> ownerDataProvider;
	
	 @ManyToMany(fetch=FetchType.LAZY)
	  @JoinTable(name="data_domains_email_addresses",joinColumns=@JoinColumn(name="domain_id",referencedColumnName="id"),
	   inverseJoinColumns=@JoinColumn(name="email_address_id",referencedColumnName="id"))
	  private Set<EmailAddress> EmailAddresses;
	
	    @ManyToOne
		@JoinColumn(name="type_id")
		private LookupCode type;
	
	  public LookupCode getType() {
		return type;
	}

	public void setType(LookupCode type) {
		this.type = type;
	}

	@Column(name = "record_id")
	 private String recordId;
	  
	  @JsonAdapter(DateToStringTypeAdapter.class)
	    @NotNull
	    @Column(name = "created_date", nullable = false)
	    private LocalDate createdDate;
	  
	  @JsonAdapter(DateToStringTypeAdapter.class)
	    @Column(name = "last_updated_date")
	    private LocalDate LastUpdatedDate;
	  
	  @ManyToOne
	  @JoinColumn(name="generic_email_domain_id")
	  private LookupCode genericEmailDomain;

	public Long getId() {
		return id;
	}

	public String getDomain() {
		return domain;
	}

	

	public Set<EmailAddress> getEmailAddresses() {
		return EmailAddresses;
	}

	

	public String getRecordId() {
		return recordId;
	}

	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public LocalDate getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public LookupCode getGenericEmailDomain() {
		return genericEmailDomain;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	
	public void setEmailAddresses(Set<EmailAddress> emailAddresses) {
		EmailAddresses = emailAddresses;
	}

	

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public Set<DataInvestmentManager> getOwnerInvestmentManager() {
		return ownerInvestmentManager;
	}

	public Set<DataProvider> getOwnerDataProvider() {
		return ownerDataProvider;
	}

	public void setOwnerInvestmentManager(Set<DataInvestmentManager> ownerInvestmentManager) {
		this.ownerInvestmentManager = ownerInvestmentManager;
	}

	public void setOwnerDataProvider(Set<DataProvider> ownerDataProvider) {
		this.ownerDataProvider = ownerDataProvider;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	public void setLastUpdatedDate(LocalDate lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public void setGenericEmailDomain(LookupCode genericEmailDomain) {
		this.genericEmailDomain = genericEmailDomain;
	}
	  
	  
	@Override
	public String toString() {
		return "Domains [id=" + id + ", domain=" + domain + ", ownerInvestmentManager=" + ownerInvestmentManager
				+ ", ownerDataProvider=" + ownerDataProvider + ", EmailAddresses=" + EmailAddresses + ", type=" + type
				+ ", recordId=" + recordId + ", createdDate=" + createdDate + ", LastUpdatedDate=" + LastUpdatedDate
				+ ", genericEmailDomain=" + genericEmailDomain + "]";
	}
}
