package com.ai.mp.domain;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import org.springframework.data.elasticsearch.annotations.Document;
import org.hibernate.annotations.Cache;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "mp_documents_metadata")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "documentsmetadata")
public class DocumentsMetadata implements Serializable {

    private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "File_ID")
  private String fileID;

  @Column(name = "File_Path")
  private String filePath;
  
  @Column(name = "File_Extension")
  private String fileExtension;

  @Column(name = "Date_Created")
  private String dateCreated;

  @Column(name = "Date_Modified")
  private String dateModified;

  @Column(name = "File_Weblink")
  private String file_weblink;
  @Column(name = "File_Size")
  private Long fileSize;
  @Column(name = "Last_Modified_By")
  private String lastModifiedBy;
  @Column(name = "File_Owners")
  private String fileOwners;

    @Size(max = 50)
    @Column(name = "record_id", length = 50)
    private String recordID;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "provider_id", referencedColumnName = "id")
    private DataProvider dataProvider;


    public String getFilePath() {
        return filePath;
    }
    
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public String getRecordID() {
        return recordID;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Column(name = "file_name")
    private String fileName;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFileID() {
    return fileID;
  }

  public void setFileID(String file_ID) {
    this.fileID = file_ID;
  }

  public String getFile_path() {
    return filePath;
  }

  public void setFile_path(String file_path) {
    this.filePath = file_path;
  }

  public String getFile_extension() {
    return fileExtension;
  }

  public void setFileExtension(String file_extension) {
    this.fileExtension = file_extension;
  }

  public String getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(String date_created) {
    this.dateCreated = date_created;
  }

  public String getDateModified() {
    return dateModified;
  }

  public void setDateModified(String date_modified) {
    this.dateModified = date_modified;
  }

  public String getFile_weblink() {
    return file_weblink;
  }
    public void setFile_weblink(String file_weblink) {
    this.file_weblink = file_weblink;
  }

  public Long getFileSize() {
    return fileSize;
  }

  public void setFileSize(Long fileSize) {
    this.fileSize = fileSize;
  }

  public String getLastModifiedBy() {
    return lastModifiedBy;
  }

    public DataProvider getDataProvider() {
        return dataProvider;
    }

    public void setDataProvider(DataProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

  public void setLastModifiedBy(String last_modified_by) {
    this.lastModifiedBy = last_modified_by;
  }

  public String getFileOwners() {
    return fileOwners;
  }

  public void setFileOwners(String file_owners) {
    this.fileOwners = file_owners;
  }

    @Override
    public String toString() {
        return "DocumentsMetadata{" +
            "id=" + id +
            ", fileID='" + fileID + '\'' +
            ", filePath='" + filePath + '\'' +
            ", fileExtension='" + fileExtension + '\'' +
            ", dateCreated='" + dateCreated + '\'' +
            ", dateModified='" + dateModified + '\'' +
            ", file_weblink='" + file_weblink + '\'' +
            ", fileSize=" + fileSize +
            ", lastModifiedBy='" + lastModifiedBy + '\'' +
            ", fileOwners='" + fileOwners + '\'' +
            ", recordID='" + recordID + '\'' +
            ", dataProvider=" + dataProvider +
            ", fileName='" + fileName + '\'' +
            '}';
    }
}
