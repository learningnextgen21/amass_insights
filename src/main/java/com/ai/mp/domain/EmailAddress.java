package com.ai.mp.domain;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "data_email_address")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "emailaddress")
public class EmailAddress implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "record_id", length = 50)
    private String recordID;

    @Column(name = "email_address")
    private String emailAddress;

    @Size(max = 20)
    @Column(name = "confirmation_key", length = 20)
    @JsonIgnore
    private String confirmationkey;

    @ManyToOne
    private LookupCode type;

    //@NotNull
    @Column(name="primary_mail",nullable = false)
    private boolean primary = false;

    //@NotNull
    @Column(name="active",nullable = false)
    private boolean active = false;

    @Column(name = "last_updated_date")
    private Date lastUpdatedDate;


    //@JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "created_date", nullable = false)
    private Date createdDate;


    public Long getDataProvider() {
		return dataProvider;
	}

	public void setDataProvider(Long dataProvider) {
		this.dataProvider = dataProvider;
	}

	@Column (name ="user")
    private Long user;


    @Column (name ="data_provider")
    private Long dataProvider;

    @Column(name="subscribed_to_alt_data_newsletter")
    private boolean subscribedNewsletter = false;


    @Column(name="exclude_from_newsletter")
    private boolean excludeFromNewsletter = false;


   // @NotNull
    @Column(name = "date_confirmed_subscription", nullable = false)
    private Date dateConfirmedSubscription;


  //  @NotNull
    @Column(name = "date_confirmed_unsubscription", nullable = false)
    private Date dateConfirmedUnubscription;

    
    public Instant getWebCreatedDate() {
		return webCreatedDate;
	}

	public void setWebCreatedDate(Instant webCreatedDate) {
		this.webCreatedDate = webCreatedDate;
	}

	public Instant getWebUpdateDate() {
		return webUpdateDate;
	}

	public void setWebUpdateDate(Instant webUpdateDate) {
		this.webUpdateDate = webUpdateDate;
	}

	@CreatedDate
    @Column(name = "web_created_date")
    @JsonIgnore
    private Instant webCreatedDate = Instant.now();

    @LastModifiedDate
    @Column(name = "web_updated_date")
    @JsonIgnore
    private Instant webUpdateDate = Instant.now();

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public LookupCode getType() {
        return type;
    }

    public void setType(LookupCode type) {
        this.type = type;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public boolean isSubscribedNewsletter() {
        return subscribedNewsletter;
    }

    public void setSubscribedNewsletter(boolean subscribedNewsletter) {
        this.subscribedNewsletter = subscribedNewsletter;
    }

    public boolean isExcludeFromNewsletter() {
        return excludeFromNewsletter;
    }

    public void setExcludeFromNewsletter(boolean excludeFromNewsletter) {
        this.excludeFromNewsletter = excludeFromNewsletter;
    }

    public Date getDateConfirmedSubscription() {
        return dateConfirmedSubscription;
    }

    public void setDateConfirmedSubscription(Date dateConfirmedSubscription) {
        this.dateConfirmedSubscription = dateConfirmedSubscription;
    }

    public Date getDateConfirmedUnubscription() {
        return dateConfirmedUnubscription;
    }

    public void setDateConfirmedUnubscription(Date dateConfirmedUnubscription) {
        this.dateConfirmedUnubscription = dateConfirmedUnubscription;
    }

    public String getConfirmationkey() {
        return confirmationkey;
    }

    public void setConfirmationkey(String confirmationkey) {
        this.confirmationkey = confirmationkey;
    }

    @Override
    public String toString() {
        return "EmailAddress{" +
            "id=" + id +
            ", recordID='" + recordID + '\'' +
            ", emailAddress='" + emailAddress + '\'' +
            ", confirmationkey='" + confirmationkey + '\'' +
            ", type=" + type +
            ", primary=" + primary +
            ", active=" + active +
            ", lastUpdatedDate=" + lastUpdatedDate +
            ", createdDate=" + createdDate +
            ", user=" + user +
            ", subscribedNewsletter=" + subscribedNewsletter +
            ", excludeFromNewsletter=" + excludeFromNewsletter +
            ", dateConfirmedSubscription=" + dateConfirmedSubscription +
            ", dateConfirmedUnubscription=" + dateConfirmedUnubscription +
             ", webCreatedDate=" + webCreatedDate +
            ", webUpdateDate=" + webUpdateDate +
             ", dataProvider=" + dataProvider +

            '}';
    }
}
