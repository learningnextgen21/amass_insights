package com.ai.mp.domain;

import com.ai.core.Constants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.SQLInsert;
import org.hibernate.annotations.WhereJoinTable;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "data_comments")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "datacomments")
public class DataComment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "record_id")
    private String recordID;

    @Column(name = "title")
    private String title;

    @Column(name = "comment")
    private String comment;

    @Column(name = "privacy")
    private String privacy;

   /* @Column(name = "by_user_id")
    private Long byUserId;*/

    @ManyToOne
//    @JsonIgnore
    private User byUser;

    @Column(name = "to_user_id")
    private Long toUserId;

    @Column(name = "receive_notifications")
    private Boolean receiveNotifications;

    @Column(name = "anonymity")
    private Boolean anonymity;

    @Column(name = "inactive")
    private Boolean inactive;

    @Column(name = "created_date")
    private Instant createdDate = Instant.now();

    @Column(name = "updated_date")
    private Instant updatedDate = Instant.now();

    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_comments",
        joinColumns = @JoinColumn(name="comment_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "comment_type = " + Constants.LOOKUP_RELEVENT_PROVIDER_TYPE)
    @SQLInsert(sql = "insert into data_provider_comments (comment_id, data_providers_id, comment_type) values (?, ?, 578)")
    private Set<DataProvider> releventProviders = new HashSet<>();

    @CreatedDate
    @Column(name = "web_created_date")
    @JsonIgnore
    private Instant webCreatedDate = Instant.now();

    @LastModifiedDate
    @Column(name = "web_updated_date")
    @JsonIgnore
    private Instant webUpdateDate = Instant.now();

    public Instant getWebCreatedDate() {
		return webCreatedDate;
	}

	public void setWebCreatedDate(Instant webCreatedDate) {
		this.webCreatedDate = webCreatedDate;
	}

	public Instant getWebUpdateDate() {
		return webUpdateDate;
	}

	public void setWebUpdateDate(Instant webUpdateDate) {
		this.webUpdateDate = webUpdateDate;
	}

	public Long getCreatorUserId() {
		return creatorUserId;
	}

	public void setCreatorUserId(Long creatorUserId) {
		this.creatorUserId = creatorUserId;
	}

	public Long getEditorUserId() {
		return editorUserId;
	}

	public void setEditorUserId(Long editorUserId) {
		this.editorUserId = editorUserId;
	}

	//  @NotNull
    @Column(name = "creator_user_id")
    private Long creatorUserId;

    // @NotNull
    @Column(name = "editor_user_id")
    private Long editorUserId;
    
    

    public User getByUserId() {
        return byUser;
    }

    public void setByUserId(User byUserId) {
        this.byUser = byUserId;
    }

    public Set<DataProvider> getReleventProviders() {
        return releventProviders;
    }

    public void setReleventProviders(Set<DataProvider> releventProviders) {
        this.releventProviders = releventProviders;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }



    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public Boolean getReceiveNotifications() {
        return receiveNotifications;
    }

    public void setReceiveNotifications(Boolean receiveNotifications) {
        this.receiveNotifications = receiveNotifications;
    }

    public Boolean getAnonymity() {
        return anonymity;
    }

    public void setAnonymity(Boolean anonymity) {
        this.anonymity = anonymity;
    }

    public Boolean getInactive() {
        return inactive;
    }

    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "DataComment{" +
            "id=" + id +
            ", recordID='" + recordID + '\'' +
            ", title='" + title + '\'' +
            ", comment='" + comment + '\'' +
            ", privacy='" + privacy + '\'' +
            ", byUser=" + byUser +
            ", toUserId=" + toUserId +
            ", receiveNotifications=" + receiveNotifications +
            ", anonymity=" + anonymity +
            ", inactive=" + inactive +
            ", createdDate=" + createdDate +
            ", updatedDate=" + updatedDate +
            ", releventProviders=" + releventProviders +
            ", webCreatedDate='" + getWebCreatedDate() + "'" +
            ", webUpdateDate='" + getWebUpdateDate() + "'" +
            ", creatorUserId='" + getCreatorUserId() + "'" +
            ", editorUserId='" + getEditorUserId() + "'" +
            '}';
    }
}
