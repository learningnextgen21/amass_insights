package com.ai.mp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "external_client")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "externalclient")
@Transactional
public class ExternalClient implements Serializable {

   /* @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;*/

    @Id
    @Column (name ="id")
    private Long id;

    @Column (name ="created_date")
    private Date createdDate;

    @Column (name ="api_limit")
    private Integer apiLimit;

    @Column (name ="expiry_date")
    private Date expiryDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getApiLimit() {
        return apiLimit;
    }

    public void setApiLimit(Integer apiLimit) {
        this.apiLimit = apiLimit;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public String toString() {
        return "ExternalClient{" +
            "id=" + id +
            ", createdDate=" + createdDate +
            ", apiLimit=" + apiLimit +
            ", expiryDate=" + expiryDate +
            '}';
    }
}
