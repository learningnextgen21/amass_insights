package com.ai.mp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * A UserProvider.
 */
@Entity
@Table(name = "mp_user_provider")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "userprovider")
public class UserProvider implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "user_id", nullable = false)
    private Long userID;

    @NotNull
    @Column(name = "provider_id", nullable = false)
    private Long providerID;

    @Max(value = 2)
    @Column(name = "get_notifications")
    private Integer getNotifications;

    @Column(name = "notification_type")
    private Integer notificationType;

    @NotNull
    @Column(name = "interest_type")
    private Integer interestType;

    private Boolean inactive;

    public String getProviderRecordID() {
        return providerRecordID;
    }

    public void setProviderRecordID(String providerRecordID) {
        this.providerRecordID = providerRecordID;
    }

    @Column(name = "provider_record_id")
    private String providerRecordID;

    public String getDeletionKey() {
        return deletionKey;
    }

    public void setDeletionKey(String deletionKey) {
        this.deletionKey = deletionKey;
    }

    private String deletionKey;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "provider_id", insertable = false, updatable = false)
    private DataProvider provider;
    
    @Transient
    @JsonSerialize
private String providerName;
    
    @Transient
    @JsonSerialize
private String organizationName;

    public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public DataProvider getProvider() {
        return provider;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    public User getUser(){
        return  user;
    }

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserID() {
        return userID;
    }

    public UserProvider userID(Long userID) {
        this.userID = userID;
        return this;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public Long getProviderID() {
        return providerID;
    }

    public UserProvider providerID(Long providerID) {
        this.providerID = providerID;
        return this;
    }

    public void setProviderID(Long providerID) {
        this.providerID = providerID;
    }

    public Integer getGetNotifications() {
        return getNotifications;
    }

    public UserProvider getNotifications(Integer getNotifications) {
        this.getNotifications = getNotifications;
        return this;
    }

    public void setGetNotifications(Integer getNotifications) {
        this.getNotifications = getNotifications;
    }

    public Integer getNotificationType() {
        return notificationType;
    }

    public UserProvider notificationType(Integer notificationType) {
        this.notificationType = notificationType;
        return this;
    }

    public void setNotificationType(Integer notificationType) {
        this.notificationType = notificationType;
    }

    public Integer getInterestType() {
        return interestType;
    }

    public UserProvider interestType(Integer interestType) {
        this.interestType = interestType;
        return this;
    }

    public void setInterestType(Integer interestType) {
        this.interestType = interestType;
    }

    public Boolean getInactive() {
        return inactive;
    }

    public UserProvider inactive(Boolean inactive) {
        this.inactive = inactive;
        return this;
    }

    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public UserProvider createdDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public UserProvider updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserProvider amassUserProvider = (UserProvider) o;
        if (amassUserProvider.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), amassUserProvider.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserProvider{" +
            "id=" + getId() +
            ", userID='" + getUserID() + "'" +
            ", providerID='" + getProviderID() + "'" +
            ", getNotifications='" + getGetNotifications() + "'" +
            ", notificationType='" + getNotificationType() + "'" +
            ", interestType='" + getInterestType() + "'" +
            ", inactive='" + getInactive() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", providerName='" + getProviderName() + "'" +
            ", organizationName='" + getOrganizationName()+ "'" +
     
            "}";
    }
}
