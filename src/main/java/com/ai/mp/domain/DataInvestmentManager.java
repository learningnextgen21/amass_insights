package com.ai.mp.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.google.gson.annotations.JsonAdapter;

@Entity
@Table(name = "data_inv_managers")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "datainvestmentmanager")
public class DataInvestmentManager implements Serializable {
	 private static final long serialVersionUID = 1L;

	 @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     private Long id;
	    
	 @Size(max = 50)
     @Column(name = "record_id", length = 50)
     private String recordID;
	    
	 @JsonAdapter(DateToStringTypeAdapter.class)
     @NotNull
     @Column(name = "last_updated_date")
     private LocalDate lastUpdatedDate;

	 @Column(name = "name")
     private String name;

	 @JsonAdapter(DateToStringTypeAdapter.class)
     @NotNull
     @Column(name = "created_date", nullable = false)
     private LocalDate createdDate;
		
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_organization_inv_managers",
         joinColumns = @JoinColumn(name="inv_managers_id", referencedColumnName="id"),
		 inverseJoinColumns = @JoinColumn(name="organization_inv_managers_id", referencedColumnName="id"))
     private Set<DataOrganization> dataOrganizationInvestor = new HashSet<>();
		    
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_types",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="type_id", referencedColumnName="id"))
     private Set<LookupCode> dataTypes = new HashSet<>();
		    
     @Column(name="head_quarters_location")
	 private String headQuartersLocation;

	 public String getHeadQuartersLocation() {
		return headQuartersLocation;
	}

	public void setHeadQuartersLocation(String headQuartersLocation) {
		this.headQuartersLocation = headQuartersLocation;
	}

	@ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_geographical_focus",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="geographical_foci_id", referencedColumnName="id"))
     private Set<LookupCode> geographicalFoci = new HashSet<>();
		    
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_investment_strategies",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="investment_strategies_id", referencedColumnName="id"))
     private Set<LookupCode> investmentStrategies = new HashSet<>();
		    
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_assest_class",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="asset_class_id", referencedColumnName="id"))
     private Set<LookupCode> assetClass = new HashSet<>();
		    
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_sector",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="sector_id", referencedColumnName="id"))
     private Set<LookupCode> sector = new HashSet<>();
		    
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_subtype",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="subtype_id", referencedColumnName="id"))
     private Set<LookupCode> subType = new HashSet<>();
		    
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_investment_stage",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="investment_stage_id", referencedColumnName="id"))
     private Set<LookupCode> investmentStage = new HashSet<>();
		    
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_investment_round",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="investment_round_id", referencedColumnName="id"))
     private Set<LookupCode> investmentRound = new HashSet<>();
		    
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_typical_check_size",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="typical_check_size_id", referencedColumnName="id"))
     private Set<LookupCode> typicalCheckSize = new HashSet<>();
		    
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_equities_market_cap",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="equities_market_cap_id", referencedColumnName="id"))
     private Set<LookupCode> equitiesMarketCap = new HashSet<>();

	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_equities_style",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="equities_style_id", referencedColumnName="id"))
     private Set<LookupCode> equitiesStyle = new HashSet<>();

	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_investing_timeframe",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="investing_timeframe_id", referencedColumnName="id"))
     private Set<LookupCode> investingTimeFrame = new HashSet<>();
		    
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_research_style",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="research_style_id", referencedColumnName="id"))
     private Set<LookupCode> researchStyle = new HashSet<>();
		    
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_security_type",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="security_type_id", referencedColumnName="id"))
     private Set<LookupCode> securityType = new HashSet<>();
		  
	 @ManyToMany(fetch=FetchType.EAGER)
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_investor_industry_sector",
         joinColumns = @JoinColumn(name="investor_managers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="industry_sector_id", referencedColumnName="id"))
     private Set<LookupCode> industrySectorFocus = new HashSet<>();
		    
	 @Lob
     @Column(name = "notes")
     private String notes;
		     
	 @Column(name = "total_aum_in_millions")
     private Double totalAumInMillions;

	 @JsonAdapter(DateToStringTypeAdapter.class)
     @Column(name = "first_raised_date")
     private LocalDate firstRaisedDate;
		   
	 @Column(name = "first_raised_year")
     private Integer firstRaisedYear;
		    
	 @Column(name = "number_of_funds")
     private Integer numberOfFunds;
		    
	 @ManyToOne
     @JoinColumn(name="dominant_style_id")
     private LookupCode dominantStyle;
		    
	 @ManyToOne
     @JoinColumn(name="activity_level_id")
     private LookupCode activityLevel;
		    
	 @Column(name = "user_permission")
     private String userPermission;
		    
	 @ManyToOne
     @JoinColumn(name="side_id")
     private LookupCode side;
		    
	 @ManyToOne
     @JoinColumn(name="primary_type_id")
     private LookupCode primaryType;
		    
	 @ManyToOne
     @JoinColumn(name="primary_subtype_id")
     private LookupCode primarySubtype;
		    
	 @ManyToOne
     @JoinColumn(name="main_research_style_id")
     private LookupCode mainResearchStyle;
		   
	 @ManyToOne
     @JoinColumn(name="total_aum_range_id")
     private LookupCode totalAumRange;
		   
	 @ManyToOne
     @JoinColumn(name="total_aum_currency_id")
     private LookupCode totalAumCurrency;

	 @Column(name="exclusions_text")
     private String exclusionsText;
		   
	 @Column(name="portfolio_companies_text")
     private String portfolioCompaniesText;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public LocalDate getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(LocalDate lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public Set<DataOrganization> getDataOrganizationInvestor() {
        return dataOrganizationInvestor;
    }

    public void setDataOrganizationInvestor(Set<DataOrganization> dataOrganizationInvestor) {
        this.dataOrganizationInvestor = dataOrganizationInvestor;
    }

    public Set<LookupCode> getDataTypes() {
        return dataTypes;
    }

    public void setDataTypes(Set<LookupCode> dataTypes) {
        this.dataTypes = dataTypes;
    }

  

    public Set<LookupCode> getGeographicalFoci() {
        return geographicalFoci;
    }

    public void setGeographicalFoci(Set<LookupCode> geographicalFoci) {
        this.geographicalFoci = geographicalFoci;
    }

    public Set<LookupCode> getInvestmentStrategies() {
        return investmentStrategies;
    }

    public void setInvestmentStrategies(Set<LookupCode> investmentStrategies) {
        this.investmentStrategies = investmentStrategies;
    }

    public Set<LookupCode> getAssetClass() {
        return assetClass;
    }

    public void setAssetClass(Set<LookupCode> assetClass) {
        this.assetClass = assetClass;
    }

    public Set<LookupCode> getSector() {
        return sector;
    }

    public void setSector(Set<LookupCode> sector) {
        this.sector = sector;
    }

    public Set<LookupCode> getSubType() {
        return subType;
    }

    public void setSubType(Set<LookupCode> subType) {
        this.subType = subType;
    }

    public Set<LookupCode> getInvestmentStage() {
        return investmentStage;
    }

    public void setInvestmentStage(Set<LookupCode> investmentStage) {
        this.investmentStage = investmentStage;
    }

    public Set<LookupCode> getInvestmentRound() {
        return investmentRound;
    }

    public void setInvestmentRound(Set<LookupCode> investmentRound) {
        this.investmentRound = investmentRound;
    }

    public Set<LookupCode> getTypicalCheckSize() {
        return typicalCheckSize;
    }

    public void setTypicalCheckSize(Set<LookupCode> typicalCheckSize) {
        this.typicalCheckSize = typicalCheckSize;
    }

    public Set<LookupCode> getEquitiesMarketCap() {
        return equitiesMarketCap;
    }

    public void setEquitiesMarketCap(Set<LookupCode> equitiesMarketCap) {
        this.equitiesMarketCap = equitiesMarketCap;
    }

    public Set<LookupCode> getEquitiesStyle() {
        return equitiesStyle;
    }

    public void setEquitiesStyle(Set<LookupCode> equitiesStyle) {
        this.equitiesStyle = equitiesStyle;
    }

    public Set<LookupCode> getInvestingTimeFrame() {
        return investingTimeFrame;
    }

    public void setInvestingTimeFrame(Set<LookupCode> investingTimeFrame) {
        this.investingTimeFrame = investingTimeFrame;
    }

    public Set<LookupCode> getResearchStyle() {
        return researchStyle;
    }

    public void setResearchStyle(Set<LookupCode> researchStyle) {
        this.researchStyle = researchStyle;
    }

    public Set<LookupCode> getSecurityType() {
        return securityType;
    }

    public void setSecurityType(Set<LookupCode> securityType) {
        this.securityType = securityType;
    }

    public Set<LookupCode> getIndustrySectorFocus() {
        return industrySectorFocus;
    }

    public void setIndustrySectorFocus(Set<LookupCode> industrySectorFocus) {
        this.industrySectorFocus = industrySectorFocus;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Double getTotalAumInMillions() {
        return totalAumInMillions;
    }

    public void setTotalAumInMillions(Double totalAumInMillions) {
        this.totalAumInMillions = totalAumInMillions;
    }

    public LocalDate getFirstRaisedDate() {
        return firstRaisedDate;
    }

    public void setFirstRaisedDate(LocalDate firstRaisedDate) {
        this.firstRaisedDate = firstRaisedDate;
    }

    public Integer getFirstRaisedYear() {
        return firstRaisedYear;
    }

    public void setFirstRaisedYear(Integer firstRaisedYear) {
        this.firstRaisedYear = firstRaisedYear;
    }

    public Integer getNumberOfFunds() {
        return numberOfFunds;
    }

    public void setNumberOfFunds(Integer numberOfFunds) {
        this.numberOfFunds = numberOfFunds;
    }

    public LookupCode getDominantStyle() {
        return dominantStyle;
    }

    public void setDominantStyle(LookupCode dominantStyle) {
        this.dominantStyle = dominantStyle;
    }

    public LookupCode getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(LookupCode activityLevel) {
        this.activityLevel = activityLevel;
    }

    public String getUserPermission() {
        return userPermission;
    }

    public void setUserPermission(String userPermission) {
        this.userPermission = userPermission;
    }

    public LookupCode getSide() {
        return side;
    }

    public void setSide(LookupCode side) {
        this.side = side;
    }

    public LookupCode getPrimaryType() {
        return primaryType;
    }

    public void setPrimaryType(LookupCode primaryType) {
        this.primaryType = primaryType;
    }

    public LookupCode getPrimarySubtype() {
        return primarySubtype;
    }

    public void setPrimarySubtype(LookupCode primarySubtype) {
        this.primarySubtype = primarySubtype;
    }

    public LookupCode getMainResearchStyle() {
        return mainResearchStyle;
    }

    public void setMainResearchStyle(LookupCode mainResearchStyle) {
        this.mainResearchStyle = mainResearchStyle;
    }

    public LookupCode getTotalAumRange() {
        return totalAumRange;
    }

    public void setTotalAumRange(LookupCode totalAumRange) {
        this.totalAumRange = totalAumRange;
    }

    public LookupCode getTotalAumCurrency() {
        return totalAumCurrency;
    }

    public void setTotalAumCurrency(LookupCode totalAumCurrency) {
        this.totalAumCurrency = totalAumCurrency;
    }

    public String getExclusionsText() {
        return exclusionsText;
    }

    public void setExclusionsText(String exclusionsText) {
        this.exclusionsText = exclusionsText;
    }

    public String getPortfolioCompaniesText() {
        return portfolioCompaniesText;
    }

    public void setPortfolioCompaniesText(String portfolioCompaniesText) {
        this.portfolioCompaniesText = portfolioCompaniesText;
    }

    @Override
    public String toString() {
        return "DataInvestmentManager{" +
            "id=" + id +
            ", recordID='" + recordID + '\'' +
            ", lastUpdatedDate=" + lastUpdatedDate +
            ", name='" + name + '\'' +
            ", createdDate=" + createdDate +
            ", dataOrganizationInvestor=" + dataOrganizationInvestor +
            ", dataTypes=" + dataTypes +
            ", headQuartersLocation=" + headQuartersLocation +
            ", geographicalFoci=" + geographicalFoci +
            ", investmentStrategies=" + investmentStrategies +
            ", assetClass=" + assetClass +
            ", sector=" + sector +
            ", subType=" + subType +
            ", investmentStage=" + investmentStage +
            ", investmentRound=" + investmentRound +
            ", typicalCheckSize=" + typicalCheckSize +
            ", equitiesMarketCap=" + equitiesMarketCap +
            ", equitiesStyle=" + equitiesStyle +
            ", investingTimeFrame=" + investingTimeFrame +
            ", researchStyle=" + researchStyle +
            ", securityType=" + securityType +
            ", industrySectorFocus=" + industrySectorFocus +
            ", notes='" + notes + '\'' +
            ", totalAumInMillions='" + totalAumInMillions + '\'' +
            ", firstRaisedDate=" + firstRaisedDate +
            ", firstRaisedYear='" + firstRaisedYear + '\'' +
            ", numberOfFunds=" + numberOfFunds +
            ", dominantStyle=" + dominantStyle +
            ", activityLevel=" + activityLevel +
            ", userPermission='" + userPermission + '\'' +
            ", side=" + side +
            ", primaryType=" + primaryType +
            ", primarySubtype=" + primarySubtype +
            ", mainResearchStyle=" + mainResearchStyle +
            ", totalAumRange=" + totalAumRange +
            ", totalAumCurrency=" + totalAumCurrency +
            ", exclusionsText='" + exclusionsText + '\'' +
            ", portfolioCompaniesText='" + portfolioCompaniesText + '\'' +
            '}';
    }
}
