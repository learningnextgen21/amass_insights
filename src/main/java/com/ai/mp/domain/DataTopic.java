package com.ai.mp.domain;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "data_topic")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "datatopic")
public class DataTopic implements Serializable {

    public DataTopic(long id, String topicName) {
        this.id = id;
        this.topicName = topicName;
    }

    public DataTopic()
    {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String topicName;

    @Column(name = "record_id")
    private String recordID;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @NotNull
    @Column(name = "created_date", nullable = false)
    private LocalDate createdDate;

    private int sortOrder;

    @Column(name="hashtag")
    private String hashtag;

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public int getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(int articleCount) {
        this.articleCount = articleCount;
    }

    private int articleCount;

    private int eventCount;

    public int getEventCount() {
        return eventCount;
    }

    public void setEventCount(int eventCount) {
        this.eventCount = eventCount;
    }

    public Long getId() {
    return id;
    }

    public void setId(Long id) {
    this.id = id;
    }

    public String getTopicName() {
    return topicName;
    }

    public void setTopicName(String topicName) {
    this.topicName = topicName;
    }

    public String getRecordID() {
    return recordID;
    }

    public void setRecordID(String recordID) {
    this.recordID = recordID;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public DataTopic createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public int getSortOrder() {
    return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
    this.sortOrder = sortOrder;
    }
}
