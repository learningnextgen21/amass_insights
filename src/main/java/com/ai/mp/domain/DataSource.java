package com.ai.mp.domain;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DataSource.
 */
@Entity
@Table(name = "data_source")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "datasource")
public class DataSource implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "record_id", length = 50)
    private String recordID;

    @NotNull
    @Size(max = 100)
    @Column(name = "data_source", length = 100, nullable = false)
    private String dataSource;

    @Lob
    @Column(name = "explanation")
    private String explanation;

    private int sortOrder;

    public int getSortorder() {
        return sortOrder;
    }

    public void setSortorder(int sortorder) {
        this.sortOrder = sortorder;
    }

    public int getProviderCount() {
        return providerCount;
    }

    public void setProviderCount(int providerCount) {
        this.providerCount = providerCount;
    }

    private int providerCount;

    @NotNull
    @Max(value = 2)
    @Column(name = "inactive", nullable = false)
    private Integer inactive;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @NotNull
    @Column(name = "created_date", nullable = false)
    private LocalDate createdDate;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "updated_date")
    private Instant updatedDate;

    @ManyToMany(mappedBy = "sources")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerSources = new HashSet<>();

    @ManyToMany(mappedBy = "dataProviderSources")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> dataSources = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public DataSource recordID(String recordID) {
        this.recordID = recordID;
        return this;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getDataSource() {
        return dataSource;
    }

    public DataSource dataSource(String dataSource) {
        this.dataSource = dataSource;
        return this;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public String getExplanation() {
        return explanation;
    }

    public DataSource explanation(String explanation) {
        this.explanation = explanation;
        return this;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public Integer getInactive() {
        return inactive;
    }

    public DataSource inactive(Integer inactive) {
        this.inactive = inactive;
        return this;
    }

    public void setInactive(Integer inactive) {
        this.inactive = inactive;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public DataSource createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public DataSource updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Set<DataProvider> getProviderSources() {
        return providerSources;
    }

    public DataSource providerSources(Set<DataProvider> dataProviders) {
        this.providerSources = dataProviders;
        return this;
    }

    public DataSource addProviderSource(DataProvider dataProvider) {
        this.providerSources.add(dataProvider);
//        dataProvider.getSources().add(this);
        return this;
    }

    public DataSource removeProviderSource(DataProvider dataProvider) {
        this.providerSources.remove(dataProvider);
        dataProvider.getSources().remove(this);
        return this;
    }

    public void setProviderSources(Set<DataProvider> dataProviders) {
        this.providerSources = dataProviders;
    }


    /********************************For Providers form Page*********************/
    public Set<DataProvider> getDataSources() {
        return dataSources;
    }

    public DataSource dataSources(Set<DataProvider> dataProviders) {
        this.dataSources = dataProviders;
        return this;
    }

    public DataSource addDataSource(DataProvider dataProvider) {
        this.dataSources.add(dataProvider);
        //dataProvider.getFeatures().add(this);
        return this;
    }

    public DataSource removeDataSources(DataProvider dataProvider) {
        this.dataSources.remove(dataProvider);
        dataProvider.getDataProviderSources().remove(this);
        return this;
    }

    public void setDataSources(Set<DataProvider> dataProviders) {
        this.dataSources = dataProviders;
    }

    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataSource dataSource = (DataSource) o;
        if (dataSource.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataSource.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataSource{" +
            "id=" + getId() +
            ", recordID='" + getRecordID() + "'" +
            ", dataSource='" + getDataSource() + "'" +
            ", explanation='" + getExplanation() + "'" +
            ", inactive='" + getInactive() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
