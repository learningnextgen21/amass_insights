package com.ai.mp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ClientRole.
 */
@Entity
@Table(name = "mp_client_role")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "clientrole")
public class ClientRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Max(value = 11)
    @Column(name = "investor_type", nullable = false)
    private Integer investorType;

    @NotNull
    @Max(value = 11)
    @Column(name = "assets", nullable = false)
    private Integer assets;

    @NotNull
    @Size(max = 36)
    @Column(name = "asset_classes", length = 36, nullable = false)
    private String assetClasses;

    @NotNull
    @Size(max = 36)
    @Column(name = "equities", length = 36, nullable = false)
    private String equities;

    @NotNull
    @Lob
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Size(max = 20)
    @Column(name = "paid", length = 20, nullable = false)
    private String paid;

    @NotNull
    @Max(value = 11)
    @Column(name = "approval_status", nullable = false)
    private Integer approvalStatus;

    @NotNull
    @Max(value = 11)
    @Column(name = "subscription_type", nullable = false)
    private Integer subscriptionType;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getInvestorType() {
        return investorType;
    }

    public ClientRole investorType(Integer investorType) {
        this.investorType = investorType;
        return this;
    }

    public void setInvestorType(Integer investorType) {
        this.investorType = investorType;
    }

    public Integer getAssets() {
        return assets;
    }

    public ClientRole assets(Integer assets) {
        this.assets = assets;
        return this;
    }

    public void setAssets(Integer assets) {
        this.assets = assets;
    }

    public String getAssetClasses() {
        return assetClasses;
    }

    public ClientRole assetClasses(String assetClasses) {
        this.assetClasses = assetClasses;
        return this;
    }

    public void setAssetClasses(String assetClasses) {
        this.assetClasses = assetClasses;
    }

    public String getEquities() {
        return equities;
    }

    public ClientRole equities(String equities) {
        this.equities = equities;
        return this;
    }

    public void setEquities(String equities) {
        this.equities = equities;
    }

    public String getDescription() {
        return description;
    }

    public ClientRole description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPaid() {
        return paid;
    }

    public ClientRole paid(String paid) {
        this.paid = paid;
        return this;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public Integer getApprovalStatus() {
        return approvalStatus;
    }

    public ClientRole approvalStatus(Integer approvalStatus) {
        this.approvalStatus = approvalStatus;
        return this;
    }

    public void setApprovalStatus(Integer approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Integer getSubscriptionType() {
        return subscriptionType;
    }

    public ClientRole subscriptionType(Integer subscriptionType) {
        this.subscriptionType = subscriptionType;
        return this;
    }

    public void setSubscriptionType(Integer subscriptionType) {
        this.subscriptionType = subscriptionType;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ClientRole clientRole = (ClientRole) o;
        if (clientRole.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientRole.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientRole{" +
            "id=" + getId() +
            ", investorType='" + getInvestorType() + "'" +
            ", assets='" + getAssets() + "'" +
            ", assetClasses='" + getAssetClasses() + "'" +
            ", equities='" + getEquities() + "'" +
            ", description='" + getDescription() + "'" +
            ", paid='" + getPaid() + "'" +
            ", approvalStatus='" + getApprovalStatus() + "'" +
            ", subscriptionType='" + getSubscriptionType() + "'" +
            "}";
    }
}
