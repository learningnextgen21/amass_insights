package com.ai.mp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.internal.bind.DateTypeAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A LookupCode.
 */
@Entity
@Table(name = "mp_lookup_code")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "lookupcode")
public class LookupCode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "lookupmodule", length = 20, nullable = false)
    private String lookupmodule;

    @Size(max = 36)
    @Column(name = "lookupcode", length = 36)
    private String lookupcode;

    @Size(max = 36)
    @Column(name = "description", length = 36)
    private String description;

    @Max(value = 11)
    @Column(name = "sortorder")
    private Integer sortorder;

    public LookupCode(String description) {
        this.description = description;
    }
    public LookupCode() {
    }
    public Integer getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(Integer recordCount) {
        this.recordCount = recordCount;
    }

    private Integer recordCount;

    @JsonIgnore
    @Column(name = "lastupdatedby")
    private Integer lastupdatedby;

    @JsonAdapter(DateTypeAdapter.class)
    @JsonIgnore
    @Column(name = "createdon", nullable = false)
    private Date createdon;

    @JsonAdapter(DateTypeAdapter.class)
    @JsonIgnore
    @Column(name = "lastupdate")
    private Date lastupdate;

    @Column(name = "createdby")
    private Integer createdby;

    
	public Long getMatchingId() {
		return matchingId;
	}
	public void setMatchingId(Long matchingId) {
		this.matchingId = matchingId;
	}

	
    @Column(name = "matching_id")
    private Long matchingId;
    
    
   /* @ManyToMany(mappedBy = "geographicalFoci")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerGeographicalFoci = new HashSet<>();*/

  /*  @ManyToMany(mappedBy = "assetClasses")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerAssetClasses = new HashSet<>();*/

    /*@ManyToMany(mappedBy = "investorTypes")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerInvestorTypes = new HashSet<>();
*/
  /*  @ManyToMany(mappedBy = "securityTypes")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerSecurityTypes = new HashSet<>();
*/
   /* @ManyToMany(mappedBy = "deliveryMethods")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerDeliveryMethods = new HashSet<>();
*/
   /* @ManyToMany(mappedBy = "researchMethods")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> researchMethodsCode = new HashSet<>();
*/
   /* public Set<DataProvider> getResearchMethodsCode() {
		return researchMethodsCode;
	}
	public void setResearchMethodsCode(Set<DataProvider> researchMethodsCode) {
		this.researchMethodsCode = researchMethodsCode;
	}
*/

  /*  @ManyToMany(mappedBy = "researchMethods")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataOrganization> researchMethodsOrganization = new HashSet<>();

	*/
	
	/*public Set<DataOrganization> getResearchMethodsOrganization() {
		return researchMethodsOrganization;
	}
	public void setResearchMethodsOrganization(Set<DataOrganization> researchMethodsOrganization) {
		this.researchMethodsOrganization = researchMethodsOrganization;
	}
*/
	/*@ManyToMany(mappedBy = "deliveryFormats")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerDeliveryFormats = new HashSet<>();*/

   /* @ManyToMany(mappedBy = "deliveryFrequencies")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerDeliveryFrequencies = new HashSet<>();
*/
   /* @ManyToMany(mappedBy = "pricingModels")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerPricingModels = new HashSet<>();
*/
   /* @ManyToMany(mappedBy = "relevantSectors")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerSectors = new HashSet<>();
*/
    /*@ManyToMany(mappedBy = "dataProvidersRelevantSectors")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providersRelevantSectors= new HashSet<>();
*/
  /*  @ManyToMany(mappedBy = "dataProvidersSecurityTypes")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> dataSecurityTypes= new HashSet<>();*/

    /*******************New Fields Stat **********************/
   /* @ManyToMany(mappedBy = "equitiesStyle",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerEquitiesStyle = new HashSet<>();*/

   /* public Set<DataProvider> getProviderEquitiesStyle() {
        return providerEquitiesStyle;
    }

*/
   /* public LookupCode addProviderEquitiesStyle(DataProvider dataProvider) {
        this.providerEquitiesStyle.add(dataProvider);
        dataProvider.getEquitiesStyle().add(this);
        return this;
    }

    public LookupCode removeProviderEquitiesStyle(DataProvider dataProvider) {
        this.providerEquitiesStyle.remove(dataProvider);
        dataProvider.getEquitiesStyle().remove(this);
        return this;
    }*/


  /*  public void setProviderEquitiesStyle(Set<DataProvider> providerEquitiesStyles) {
        this.providerEquitiesStyle = providerEquitiesStyles;
    }*/
/*
    @ManyToMany(mappedBy = "identifiersAvailable",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerIdentifiersAvailable = new HashSet<>();*/

  /*  public Set<DataProvider> getProviderIdentifiersAvailable() {
        return providerIdentifiersAvailable;
    }

    public void setProviderIdentifiersAvailable(Set<DataProvider> providerIdentifiersAvailable) {
        this.providerIdentifiersAvailable = providerIdentifiersAvailable;
    }*/
/*
    @ManyToMany(mappedBy = "dataTypes",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerDataTypes = new HashSet<>();*/

    /*public Set<DataProvider> getProviderDataTypes() {
        return providerDataTypes;
    }

    public void setProviderDataTypes(Set<DataProvider> providerDataTypes) {
        this.providerDataTypes = providerDataTypes;
    }*/

   /* @ManyToMany(mappedBy = "dataProductTypes",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerDataProductTypes = new HashSet<>();
*/
   /* public Set<DataProvider> getProviderDataProductTypes() {
        return providerDataProductTypes;
    }

    public void setProviderDataProductTypes(Set<DataProvider> providerDataProductTypes) {
        this.providerDataProductTypes = providerDataProductTypes;
    }*/

  /*  @ManyToMany(mappedBy = "accessOffered",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerAccessOffered = new HashSet<>();*/

  /*  public Set<DataProvider> getProviderAccessOffered() {
        return providerAccessOffered;
    }

    public void setProviderAccessOffered(Set<DataProvider> providerAccessOffered) {
        this.providerAccessOffered = providerAccessOffered;
    }
*/
    /*@ManyToMany(mappedBy = "investingTimeFrame",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerInvestingTimeFrame = new HashSet<>();*/

    /*public Set<DataProvider> getProviderInvestingTimeFrame() {
        return providerInvestingTimeFrame;
    }

    public void setProviderInvestingTimeFrame(Set<DataProvider> providerInvestingTimeFrame) {
        this.providerInvestingTimeFrame = providerInvestingTimeFrame;
    }
*/
  /*  @ManyToMany(mappedBy = "equitiesMarketCap",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerEquitiesMarketCap = new HashSet<>();*/

  /*  public Set<DataProvider> getProviderEquitiesMarketCap() {
        return providerEquitiesMarketCap;
    }

    public void setProviderEquitiesMarketCap(Set<DataProvider> providerEquitiesMarketCap) {
        this.providerEquitiesMarketCap = providerEquitiesMarketCap;
    }*/

   /* @ManyToMany(mappedBy = "strategies",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerStrategies = new HashSet<>();
*/
  /*  public Set<DataProvider> getProviderStrategies() {
        return providerStrategies;
    }

    public void setProviderStrategies(Set<DataProvider> providerStrategies) {
        this.providerStrategies = providerStrategies;
    }
*/
 /*   @ManyToMany(mappedBy = "researchStyles",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerResearchStyles = new HashSet<>();*/

  /*  public Set<DataProvider> getProviderResearchStyles() {
        return providerResearchStyles;
    }

    public void setProviderResearchStyles(Set<DataProvider> providerResearchStyles) {
        this.providerResearchStyles = providerResearchStyles;
    }*/

  /*  @ManyToMany(mappedBy = "managerType",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerManagerType = new HashSet<>();*/

  /*  public Set<DataProvider> getProviderManagerType() {
        return providerManagerType;
    }

    public void setProviderManagerType(Set<DataProvider> providerManagerType) {
        this.providerManagerType = providerManagerType;
    }*/

  /*  @ManyToMany(mappedBy = "organizationType",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerOrganizationType = new HashSet<>();*/

 /*   public Set<DataProvider> getProviderOrganizationType() {
        return providerOrganizationType;
    }

    public void setProviderOrganizationType(Set<DataProvider> providerOrganizationType) {
        this.providerOrganizationType = providerOrganizationType;
    }*/
/*
    @ManyToMany(mappedBy = "languagesAvailable",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerLanguagesAvailable = new HashSet<>();*/

   /* public Set<DataProvider> getProviderLanguagesAvailable() {
        return providerLanguagesAvailable;
    }

    public void setProviderLanguagesAvailable(Set<DataProvider> providerLanguagesAvailable) {
        this.providerLanguagesAvailable = providerLanguagesAvailable;
    }
*/
   /* @ManyToMany(mappedBy = "paymentMethodsOffered",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerPaymentMethodsOffered = new HashSet<>();
*/
   /* public Set<DataProvider> getProviderPaymentMethodsOffered() {
        return providerPaymentMethodsOffered;
    }

    public void setProviderPaymentMethodsOffered(Set<DataProvider> providerPaymentMethodsOffered) {
        this.providerPaymentMethodsOffered = providerPaymentMethodsOffered;
    }*/

/*
    @ManyToMany(mappedBy = "dataGapsReasons",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerDataGapsReasons = new HashSet<>();
*/
    /*public Set<DataProvider> getProviderDataGapsReasons() {
        return providerDataGapsReasons;
    }

    public void setProviderDataGapsReasons(Set<DataProvider> providerDataGapsReason) {
        this.providerDataGapsReasons = providerDataGapsReason;
    }*/

   /* @ManyToMany(mappedBy = "biases",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerBiases = new HashSet<>();*/

  /*  public Set<DataProvider> getProviderBiases() {
        return providerBiases;
    }

    public void setProviderBiases(Set<DataProvider> providerBiase) {
        this.providerBiases = providerBiase;
    }*/

  /*  @ManyToMany(mappedBy = "outlierReasons",fetch = FetchType.LAZY)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerOutlierReasons = new HashSet<>();
*/
    /*public Set<DataProvider> getProviderOutlierReasons() {
        return providerOutlierReasons;
    }

    public void setProviderOutlierReasons(Set<DataProvider> provideroutlierReason) {
        this.providerOutlierReasons = provideroutlierReason;
    }*/
    /*******************New Fields End **********************/

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLookupmodule() {
        return lookupmodule;
    }

    public LookupCode lookupmodule(String lookupmodule) {
        this.lookupmodule = lookupmodule;
        return this;
    }

    public void setLookupmodule(String lookupmodule) {
        this.lookupmodule = lookupmodule;
    }

    public String getLookupcode() {
        return lookupcode;
    }

    public LookupCode lookupcode(String lookupcode) {
        this.lookupcode = lookupcode;
        return this;
    }

    public void setLookupcode(String lookupcode) {
        this.lookupcode = lookupcode;
    }

    public String getDescription() {
        return description;
    }

    public LookupCode description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSortorder() {
        return sortorder;
    }

    public LookupCode sortorder(Integer sortorder) {
        this.sortorder = sortorder;
        return this;
    }

    public void setSortorder(Integer sortorder) {
        this.sortorder = sortorder;
    }

    public Integer getLastupdatedby() {
        return lastupdatedby;
    }

    public LookupCode lastupdatedby(Integer lastupdatedby) {
        this.lastupdatedby = lastupdatedby;
        return this;
    }

    public void setLastupdatedby(Integer lastupdatedby) {
        this.lastupdatedby = lastupdatedby;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public LookupCode createdon(Date createdon) {
        this.createdon = createdon;
        return this;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public Date getLastupdate() {
        return lastupdate;
    }

    public LookupCode lastupdate(Date lastupdate) {
        this.lastupdate = lastupdate;
        return this;
    }

    public void setLastupdate(Date lastupdate) {
        this.lastupdate = lastupdate;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public LookupCode createdby(Integer createdby) {
        this.createdby = createdby;
        return this;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

/*    public Set<DataProvider> getProviderGeographicalFoci() {
        return providerGeographicalFoci;
    }

    public LookupCode providerGeographicalFoci(Set<DataProvider> dataProviders) {
        this.providerGeographicalFoci = dataProviders;
        return this;
    }*/

   /* public LookupCode addProviderGeographicalFocus(DataProvider dataProvider) {
        this.providerGeographicalFoci.add(dataProvider);
        dataProvider.getGeographicalFoci().add(this);
        return this;
    }

    public LookupCode removeProviderGeographicalFocus(DataProvider dataProvider) {
        this.providerGeographicalFoci.remove(dataProvider);
        dataProvider.getGeographicalFoci().remove(this);
        return this;
    }*/

 /*   public void setProviderGeographicalFoci(Set<DataProvider> dataProviders) {
        this.providerGeographicalFoci = dataProviders;
    }
*/
   /* public Set<DataProvider> getProviderAssetClasses() {
        return providerAssetClasses;
    }

    public LookupCode providerAssetClasses(Set<DataProvider> dataProviders) {
        this.providerAssetClasses = dataProviders;
        return this;
    }*/

   /* public LookupCode addProviderAssetClass(DataProvider dataProvider) {
        this.providerAssetClasses.add(dataProvider);
//        dataProvider.getAssetClasses().add(this);
        return this;
    }

    public LookupCode removeProviderAssetClass(DataProvider dataProvider) {
        this.providerAssetClasses.remove(dataProvider);
        dataProvider.getAssetClasses().remove(this);
        return this;
    }*/

   /* public void setProviderAssetClasses(Set<DataProvider> dataProviders) {
        this.providerAssetClasses = dataProviders;
    }
*/
    /*public Set<DataProvider> getProviderInvestorTypes() {
        return providerInvestorTypes;
    }

    public LookupCode providerInvestorTypes(Set<DataProvider> dataProviders) {
        this.providerInvestorTypes = dataProviders;
        return this;
    }

    public LookupCode addProviderInvestorType(DataProvider dataProvider) {
        this.providerInvestorTypes.add(dataProvider);
        dataProvider.getInvestorTypes().add(this);
        return this;
    }

    public LookupCode removeProviderInvestorType(DataProvider dataProvider) {
        this.providerInvestorTypes.remove(dataProvider);
        dataProvider.getInvestorTypes().remove(this);
        return this;
    }

    public void setProviderInvestorTypes(Set<DataProvider> dataProviders) {
        this.providerInvestorTypes = dataProviders;
    }
*/
  /*  public Set<DataProvider> getProviderSecurityTypes() {
        return providerSecurityTypes;
    }

    public LookupCode providerSecurityTypes(Set<DataProvider> dataProviders) {
        this.providerSecurityTypes = dataProviders;
        return this;
    }

    public LookupCode addProviderSecurityType(DataProvider dataProvider) {
        this.providerSecurityTypes.add(dataProvider);
//        dataProvider.getSecurityTypes().add(this);
        return this;
    }

    public LookupCode removeProviderSecurityType(DataProvider dataProvider) {
        this.providerSecurityTypes.remove(dataProvider);
        dataProvider.getSecurityTypes().remove(this);
        return this;
    }
*/
  /*  public void setProviderSecurityTypes(Set<DataProvider> dataProviders) {
        this.providerSecurityTypes = dataProviders;
    }*/

   /* public Set<DataProvider> getProviderDeliveryMethods() {
        return providerDeliveryMethods;
    }

    public LookupCode providerDeliveryMethods(Set<DataProvider> dataProviders) {
        this.providerDeliveryMethods = dataProviders;
        return this;
    }*/

    /*public LookupCode addProviderDeliveryMethod(DataProvider dataProvider) {
        this.providerDeliveryMethods.add(dataProvider);
//        dataProvider.getDeliveryMethods().add(this);
        return this;
    }

    public LookupCode removeProviderDeliveryMethod(DataProvider dataProvider) {
        this.providerDeliveryMethods.remove(dataProvider);
        dataProvider.getDeliveryMethods().remove(this);
        return this;
    }

    public void setProviderDeliveryMethods(Set<DataProvider> dataProviders) {
        this.providerDeliveryMethods = dataProviders;
    }
*/
    /***********Start***********/
    /*public Set<DataProvider> getDataProvidersDeliveryFormats() {
        return providerDeliveryFormats;
    }
    public void setDataProvidersDeliveryFormats(Set<DataProvider> dataProviders) {
        this.providerDeliveryFormats = dataProviders;
    }*/
   /* public Set<DataProvider> getDataProviderDeliveryMethods() {
        return providerDeliveryMethods;
    }

    public void setDataProviderDeliveryMethods(Set<DataProvider> dataProviders) {
        this.providerDeliveryMethods = dataProviders;
    }*/

   /* public Set<DataProvider> getDataProvidersPricingModels() {
        return providerPricingModels;
    }
    public void setDataProviderPricingModels(Set<DataProvider> dataProviders) {
        this.providerPricingModels = dataProviders;
    }
*/
   /* public Set<DataProvider> getDataProviderAssetClasses() {
        return providerAssetClasses;
    }
    public void setDataProviderAssetClasses(Set<DataProvider> dataProviders) {
        this.providerAssetClasses = dataProviders;
    }
*/
    /****************End***************/
 /*   public Set<DataProvider> getProviderDeliveryFormats() {
        return providerDeliveryFormats;
    }


    public LookupCode providerDeliveryFormats(Set<DataProvider> dataProviders) {
        this.providerDeliveryFormats = dataProviders;
        return this;
    }

    public LookupCode addProviderDeliveryFormat(DataProvider dataProvider) {
        this.providerDeliveryFormats.add(dataProvider);
//        dataProvider.getDeliveryFormats().add(this);
        return this;
    }

    public LookupCode removeProviderDeliveryFormat(DataProvider dataProvider) {
        this.providerDeliveryFormats.remove(dataProvider);
        dataProvider.getDeliveryFormats().remove(this);
        return this;
    }

    public void setProviderDeliveryFormats(Set<DataProvider> dataProviders) {
        this.providerDeliveryFormats = dataProviders;
    }
*/
 /*   public Set<DataProvider> getProviderDeliveryFrequencies() {
        return providerDeliveryFrequencies;
    }

    public LookupCode providerDeliveryFrequencies(Set<DataProvider> dataProviders) {
        this.providerDeliveryFrequencies = dataProviders;
        return this;
    }
*/
   /* public LookupCode addProviderDeliveryFrequency(DataProvider dataProvider) {
        this.providerDeliveryFrequencies.add(dataProvider);
        dataProvider.getDeliveryFrequencies().add(this);
        return this;
    }

    public LookupCode removeProviderDeliveryFrequency(DataProvider dataProvider) {
        this.providerDeliveryFrequencies.remove(dataProvider);
        dataProvider.getDeliveryFrequencies().remove(this);
        return this;
    }

    public void setProviderDeliveryFrequencies(Set<DataProvider> dataProviders) {
        this.providerDeliveryFrequencies = dataProviders;
    }
*/
   /* public Set<DataProvider> getProviderPricingModels() {
        return providerPricingModels;
    }

    public LookupCode providerPricingModels(Set<DataProvider> dataProviders) {
        this.providerPricingModels = dataProviders;
        return this;
    }

    public LookupCode addProviderPricingModel(DataProvider dataProvider) {
        this.providerPricingModels.add(dataProvider);
//        dataProvider.getPricingModels().add(this);
        return this;
    }
*/
  /*  public LookupCode removeProviderPricingModel(DataProvider dataProvider) {
        this.providerPricingModels.remove(dataProvider);
        dataProvider.getPricingModels().remove(this);
        return this;
    }

    public void setProviderPricingModels(Set<DataProvider> dataProviders) {
        this.providerPricingModels = dataProviders;
    }
*/
   /* public Set<DataProvider> getProviderSectors() {
        return providerSectors;
    }

    public LookupCode providerSectors(Set<DataProvider> dataProviders) {
        this.providerSectors = dataProviders;
        return this;
    }

    public LookupCode addProviderSector(DataProvider dataProvider) {
        this.providerSectors.add(dataProvider);
//        dataProvider.getRelevantSectors().add(this);
        return this;
    }

    public LookupCode removeProviderSector(DataProvider dataProvider) {
        this.providerSectors.remove(dataProvider);
        dataProvider.getRelevantSectors().remove(this);
        return this;
    }

    public void setProviderSectors(Set<DataProvider> dataProviders) {
        this.providerSectors = dataProviders;
    }
*/
    /********************************ProvidersRelevantSectors For Providers form Page*********************/
   /* public Set<DataProvider> getProvidersRelevantSectors() {
        return providersRelevantSectors;
    }

    public LookupCode providersRelevantSectors(Set<DataProvider> dataProviders) {
        this.providersRelevantSectors = dataProviders;
        return this;
    }

    public LookupCode addProvidersRelevantSector(DataProvider dataProvider) {
        this.providersRelevantSectors.add(dataProvider);
        //dataProvider.getFeatures().add(this);
        return this;
    }

    public LookupCode removeProvidersRelevantSector(DataProvider dataProvider) {
        this.providersRelevantSectors.remove(dataProvider);
        dataProvider.getDataProvidersRelevantSectors().remove(this);
        return this;
    }

    public void setProvidersRelevantSectors(Set<DataProvider> dataProviders) {
        this.providersRelevantSectors = dataProviders;
    }*/
    /********************************DataProvidersSecurityTypes For Providers form Page*********************/
    /*public Set<DataProvider> getDataSecurityTypes() {
        return dataSecurityTypes;
    }

    public LookupCode dataSecurityTypes(Set<DataProvider> dataProviders) {
        this.dataSecurityTypes = dataProviders;
        return this;
    }

    public LookupCode addDataSecurityType(DataProvider dataProvider) {
        this.dataSecurityTypes.add(dataProvider);
        //dataProvider.getFeatures().add(this);
        return this;
    }

    public LookupCode removeDataSecurityType(DataProvider dataProvider) {
        this.dataSecurityTypes.remove(dataProvider);
        dataProvider.getDataProvidersSecurityTypes().remove(this);
        return this;
    }

    public void setDataSecurityTypes(Set<DataProvider> dataProviders) {
        this.dataSecurityTypes = dataProviders;
    }*/
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LookupCode lookupCode = (LookupCode) o;
        if (lookupCode.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), lookupCode.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "LookupCode{" +
            "id=" + getId() +
            ", lookupmodule='" + getLookupmodule() + "'" +
            ", lookupcode='" + getLookupcode() + "'" +
            ", description='" + getDescription() + "'" +
            ", sortorder='" + getSortorder() + "'" +
            ", lastupdatedby='" + getLastupdatedby() + "'" +
            //   ", createdon='" + getCreatedon() + "'" +
            //  ", lastupdate='" + getLastupdate() + "'" +
            ", createdby='" + getCreatedby() + "'" +
            "}";
    }
}
