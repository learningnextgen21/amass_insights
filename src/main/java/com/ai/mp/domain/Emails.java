package com.ai.mp.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.google.gson.annotations.JsonAdapter;

@Entity
@Table(name="data_emails")
@Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="emails")
public class Emails implements Serializable{
	 private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
    @Column(name = "email_key")
	private String emailKey;
	
    public String getEmailKey() {
		return emailKey;
	}

	public void setEmailKey(String emailKey) {
		this.emailKey = emailKey;
	}

	public String getResourceRecordID() {
		return resourceRecordID;
	}

	public void setResourceRecordID(String resourceRecordID) {
		this.resourceRecordID = resourceRecordID;
	}

	@Column(name = "resource_recordid")
	private String resourceRecordID;
	
    @Column(name = "from_name")
	private String fromName;
	
    @Column(name = "from_email")
	private String fromEmail;
	
    @Column(name = "from_email_address_recordid")
	private String fromEmailAddressRecordId;
	
    @Column(name = "to_names")
	private String toNames;
	
	 @ManyToMany(fetch=FetchType.EAGER)
	  @JoinTable(name="data_to_email_addresses",joinColumns=@JoinColumn(name="email_id",referencedColumnName="id"),
	   inverseJoinColumns=@JoinColumn(name="to_email_address_id",referencedColumnName="id"))
	  private Set<EmailAddress> toEmailAddresses;
	
	@Column(name = "cc_names")
	private String ccNames;
	
	 @ManyToMany(fetch=FetchType.EAGER)
	  @JoinTable(name="data_cc_email_addresses",joinColumns=@JoinColumn(name="email_id",referencedColumnName="id"),
	   inverseJoinColumns=@JoinColumn(name="cc_email_address_id",referencedColumnName="id"))
	  private Set<EmailAddress> ccEmailAddresses;
	  
	  @Column(name = "reply_to_name")
	  private String replyToName;
	  

	@Column(name = "reply_to_email_address_recordid")
	  private  String replyToEmailAddressRecordId;
	  
	  @JsonAdapter(DateToStringTypeAdapter.class)
	  @NotNull
	  @Column(name = "date_formula", nullable = false)
	  private LocalDate dateFormula;
	  
	  @Column(name = "subject")
	  private String subject;
	  
	  @ManyToMany(fetch=FetchType.EAGER)
	  @JoinTable(name="data_email_labels",joinColumns=@JoinColumn(name="email_id",referencedColumnName="id"),
	   inverseJoinColumns=@JoinColumn(name="labels_id",referencedColumnName="id"))
	  private Set<LookupCode> labels;
	  
	  @Column(name = "body_plain")
	  private String bodyPlain;
	  
	 /* @Column(name = "body_html")
	  private String bodyHtml;*/
	  
	  @Column(name = "body_html_length")
	  private Integer bodyHtmlLength;
	  
	  @Column(name = "html_file")
	  private String htmlFile;
	  
	  @ManyToMany(fetch=FetchType.EAGER)
	    @JoinTable(name="data_emails_purposes",joinColumns=@JoinColumn(name="email_id",referencedColumnName="id"),
	        inverseJoinColumns=@JoinColumn(name="purpose_id",referencedColumnName="id"))
	    private Set<DataResourcePurpose> purposes;

	    @ManyToMany(fetch=FetchType.EAGER)
	    @JoinTable(name="data_emails_purpose_type",joinColumns=@JoinColumn(name="email_id",referencedColumnName="id"),
	        inverseJoinColumns=@JoinColumn(name="purpose_type_id",referencedColumnName="id"))
	    private Set<DataResourcePurposeType> purposeTypes;


	    @ManyToMany(fetch=FetchType.EAGER)
	    @JoinTable(name="data_emails_topic",joinColumns=@JoinColumn(name="email_id",referencedColumnName="id"),
	        inverseJoinColumns=@JoinColumn(name="topic_id",referencedColumnName="id"))
	    private Set<DataTopic> topics;
	    
		@Column(name = "message_url")
	    private String messageUrl;
	    
		@Column(name = "file_location")
	    private String fileLocation;
	    
		@Column(name = "notes")
	    private String notes;
	    
	    @JsonAdapter(DateToStringTypeAdapter.class)
	    @Column(name = "updated_date")
	    private LocalDate updatedDate;
	  
	    @JsonAdapter(DateToStringTypeAdapter.class)
	    @NotNull
	    @Column(name = "created_time", nullable = false)
	    private LocalDate createdTime;
	    
		@Column(name = "record_id")
        private String recordId;
	    
		@Column(name = "added_by")
	    private String adddedBy;
	   
	    
	    public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getFromName() {
			return fromName;
		}

		public void setFromName(String fromName) {
			this.fromName = fromName;
		}

		public String getFromEmail() {
			return fromEmail;
		}

		public void setFromEmail(String fromEmail) {
			this.fromEmail = fromEmail;
		}

		public String getFromEmailAddressRecordId() {
			return fromEmailAddressRecordId;
		}

		public void setFromEmailAddressRecordId(String fromEmailAddressRecordId) {
			this.fromEmailAddressRecordId = fromEmailAddressRecordId;
		}

		public String getToNames() {
			return toNames;
		}

		public void setToNames(String toNames) {
			this.toNames = toNames;
		}

		public Set<EmailAddress> getToEmailAddresses() {
			return toEmailAddresses;
		}

		public void setToEmailAddresses(Set<EmailAddress> toEmailAddresses) {
			this.toEmailAddresses = toEmailAddresses;
		}

		public String getCcNames() {
			return ccNames;
		}

		public void setCcNames(String ccNames) {
			this.ccNames = ccNames;
		}

		public Set<EmailAddress> getCcEmailAddresses() {
			return ccEmailAddresses;
		}

		public void setCcEmailAddresses(Set<EmailAddress> ccEmailAddresses) {
			this.ccEmailAddresses = ccEmailAddresses;
		}

		public String getReplyToName() {
			return replyToName;
		}

		public void setReplyToName(String replyToName) {
			this.replyToName = replyToName;
		}

		public LocalDate getDateFormula() {
			return dateFormula;
		}

		public void setDateFormula(LocalDate dateFormula) {
			this.dateFormula = dateFormula;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public Set<LookupCode> getLabels() {
			return labels;
		}

		public void setLabels(Set<LookupCode> labels) {
			this.labels = labels;
		}

		public String getBodyPlain() {
			return bodyPlain;
		}

		public void setBodyPlain(String bodyPlain) {
			this.bodyPlain = bodyPlain;
		}

		/*public String getBodyHtml() {
			return bodyHtml;
		}

		public void setBodyHtml(String bodyHtml) {
			this.bodyHtml = bodyHtml;
		}*/

		public Integer getBodyHtmlLength() {
			return bodyHtmlLength;
		}

		public void setBodyHtmlLength(Integer bodyHtmlLength) {
			this.bodyHtmlLength = bodyHtmlLength;
		}

		public String getHtmlFile() {
			return htmlFile;
		}

		public void setHtmlFile(String htmlFile) {
			this.htmlFile = htmlFile;
		}

		public Set<DataResourcePurpose> getPurposes() {
			return purposes;
		}

		public void setPurposes(Set<DataResourcePurpose> purposes) {
			this.purposes = purposes;
		}

		public Set<DataResourcePurposeType> getPurposeTypes() {
			return purposeTypes;
		}

		public void setPurposeTypes(Set<DataResourcePurposeType> purposeTypes) {
			this.purposeTypes = purposeTypes;
		}

		public Set<DataTopic> getTopics() {
			return topics;
		}

		public void setTopics(Set<DataTopic> topics) {
			this.topics = topics;
		}

		public String getMessageUrl() {
			return messageUrl;
		}

		public void setMessageUrl(String messageUrl) {
			this.messageUrl = messageUrl;
		}

		public String getFileLocation() {
			return fileLocation;
		}

		public void setFileLocation(String fileLocation) {
			this.fileLocation = fileLocation;
		}

		public String getNotes() {
			return notes;
		}

		public void setNotes(String notes) {
			this.notes = notes;
		}

		public LocalDate getUpdatedDate() {
			return updatedDate;
		}

		public void setUpdatedDate(LocalDate updatedDate) {
			this.updatedDate = updatedDate;
		}

		public LocalDate getCreatedTime() {
			return createdTime;
		}

		public void setCreatedTime(LocalDate createdTime) {
			this.createdTime = createdTime;
		}

	public String getReplyToEmailAddressRecordId() {
				return replyToEmailAddressRecordId;
			}

     public void setReplyToEmailAddressRecordId(String replyToEmailAddressRecordId) {
				this.replyToEmailAddressRecordId = replyToEmailAddressRecordId;
			}
			
		public String getRecordId() {
			return recordId;
		}

		public void setRecordId(String recordId) {
			this.recordId = recordId;
		}

		public String getAdddedBy() {
			return adddedBy;
		}

		public void setAdddedBy(String adddedBy) {
			this.adddedBy = adddedBy;
		}

		 @Override
		    public String toString() {
		        return "DataEvent{" +
		            "id=" + id +
		            ", emailKey='" + emailKey + '\'' +
		            ", resourceRecordID='" + resourceRecordID + '\'' +
		            ", fromName='" + fromName + '\'' +
		            ", fromEmail='" + fromEmail + '\'' +
		            ", fromEmailAddressRecordId='" + fromEmailAddressRecordId + '\'' +
		            ", toNames='" + toNames + '\'' +
		            ", toEmailAddresses='" + toEmailAddresses + '\'' +
		            ", ccNames='" + ccNames + '\'' +
		            ", ccEmailAddresses=" + ccEmailAddresses +
		            ", replyToName=" + replyToName +
		            ", dateFormula='" + dateFormula + '\'' +
		            ", subject='" + subject + '\'' +
		            ", labels='" + labels + '\'' +
		            ", bodyPlain='" + bodyPlain + '\'' +
		           // ", bodyHtml='" + bodyHtml + '\'' +
		            ", bodyHtmlLength='" + bodyHtmlLength + '\'' +
		            ", htmlFile='" + htmlFile + '\'' +
		            ", purposes='" + purposes + '\'' +
		            ", purposeTypes='" + purposeTypes + '\'' +
		            ", topics='" + topics + '\'' +
		            ", messageUrl=" + messageUrl +
		            ", createdTime=" + createdTime +
		            ", fileLocation='" + fileLocation + '\'' +
		            ", notes=" + notes +
		            ", updatedDate=" + updatedDate +
		            ", createdTime=" + createdTime +
		            ", recordId=" + recordId +
		            ", adddedBy=" + adddedBy +
		             ", replyToEmailAddressRecordId=" + replyToEmailAddressRecordId +
		            '}';
		    }

	
}
