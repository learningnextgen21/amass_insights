package com.ai.mp.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;

@Entity
@Table(name = "mp_user_geographies")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "UserGeographies")
public class UserGeographies implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "user_id", nullable = false)
    private Long userID;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	/*public String getGeographies() {
		return geographies;
	}

	public void setGeographies(String geographies) {
		this.geographies = geographies;
	}
*/
	/*public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	

	public LocalDate getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(LocalDate updatedDate) {
		this.updatedDate = updatedDate;
	}*/
	
	@Column(name = "geographies_id", nullable = false)
    private Long geographiesId;


	/*@Column(name = "geographies", nullable = false)
    private String geographies;*/


    
	/*@JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "created_date")
    private LocalDate createdDate=LocalDate.now();
    */
    public Long getGeographiesId() {
		return geographiesId;
	}

	public void setGeographiesId(Long geographiesId) {
		this.geographiesId = geographiesId;
	}
/*
	@JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "updated_date")
    private LocalDate updatedDate;*/
	private LocalDateTime createdDate;
	  public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public Instant getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Instant updatedDate) {
		this.updatedDate = updatedDate;
	}

	private Instant updatedDate;

	
    
    @Override
    public String toString() {
        return "UserDataCategory{" +
            "id=" + getId() +
            ", userID='" + getUserID() + "'" +
            //", geographies='" + getGeographies() + "'" +
           ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", geographiesId='" + getGeographiesId() + "'" +
            "}";
    }
}
