package com.ai.mp.domain;

import com.ai.core.Constants;
import com.ai.mp.utils.BasicBean;
import com.ai.mp.utils.DateToStringTypeAdapter;
import com.ai.mp.utils.SetToBeanConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.*;

/**
 * A DataProvider.
 */
@Entity
@Table(name = "data_provider")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dataprovider")
public class DataProvider implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // @Id
    @Size(max = 50)
    @Column(name = "record_id", length = 50)
    private String recordID;

    //private int cdataupdatefrequency;

    @NotNull
    @Column(name = "provider_name", length = 256, nullable = false)
    private String providerName;

    @Size(max = 255)
    @Column(name = "delivery_frequency_notes")
    private String deliveryFrequencyNotes;

    @Column(name="data_update_frequency")
    private String dataUpdateFrequency;

    @Column(name="inactive")
    private Boolean inactive;

    public Boolean isInactive() {
        return inactive;
    }

    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }
    @Column(name="tag_added")
    private Boolean tagAdded;
/*
    @ManyToOne
     @Column(name="data_update_frequency_id")
    private LookupCode dataUpdateFrequency;*/
   /* @ManyToOne
    private LookupCode priority;*/

    public Boolean getTagAdded() {
		return tagAdded;
	}

	public void setTagAdded(Boolean tagAdded) {
		this.tagAdded = tagAdded;
	}

	public String getDataUpdateFrequency() {
        return dataUpdateFrequency;
    }

    public void setDataUpdateFrequency(String dataUpdateFrequency) {
        this.dataUpdateFrequency = dataUpdateFrequency;
    }
    @Column(name="priority")
    private String priority;

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    @ManyToOne
    private LookupCode ieiStatus;


  /*  @Column(name = "planned_research_date")
    private Date plannedResearchDate;
    */

/*	public Date getPlannedResearchDate() {
		return plannedResearchDate;
	}

	public void setPlannedResearchDate(Date plannedResearchDate) {
		this.plannedResearchDate = plannedResearchDate;
	}
*/
	/*public LookupCode getPriority() {
		return priority;
	}

	public void setPriority(LookupCode priority) {
		this.priority = priority;
	}
*/

    public LookupCode getIeiStatus() {
        return ieiStatus;
    }

    public void setIeiStatus(LookupCode ieiStatus) {
        this.ieiStatus = ieiStatus;
    }

    public int getSortorder() {
        return sortOrder;
    }

   /* public LookupCode getDataUpdateFrequency() {
		return dataUpdateFrequency;
	}

    public DataProvider dataUpdateFrequency(LookupCode dataUpdateFrequency) {
        this.dataUpdateFrequency = dataUpdateFrequency;
        return this;
    }

    public void setDataUpdateFrequency(LookupCode dataUpdateFrequency) {
        this.dataUpdateFrequency = dataUpdateFrequency;
    }*/

    public void setSortorder(int sortorder) {
        this.sortOrder = sortorder;
    }

    private int sortOrder;

    public ProviderLogo getLogo() {
        return logo;
    }

    public void setLogo(ProviderLogo logo) {
        this.logo = logo;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "record_ID", insertable = false, updatable = false, referencedColumnName = "provider_record_id")
    private ProviderLogo logo;

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    @Transient
    @JsonSerialize
    private boolean locked = false;

    public List<Integer> getUserProviderStatus() {
        return userProviderStatus;
    }

    public void setUserProviderStatus( List<Integer> userProviderStatus) {
        this.userProviderStatus = userProviderStatus;
    }

   
	@Transient
    @JsonSerialize
    private List<Integer> userProviderStatus;


    public Long getPartnersCount() {
		return partnersCount;
	}

	public void setPartnersCount(Long partnersCount) {
		this.partnersCount = partnersCount;
	}
	@Transient
    @JsonSerialize
    private Long partnersCount;

    public Long getProviderTagCount() {
		return providerTagCount;
	}

	public void setProviderTagCount(Long providerTagCount) {
		this.providerTagCount = providerTagCount;
	}
	@Transient
    @JsonSerialize
    private Long providerTagCount;
	
	@Transient
    @JsonSerialize
    private Long commentsCount;
	
	@Transient
	@JsonSerialize
	@JsonDeserialize
    private List<String> user;
	
	 @Transient
	 @JsonSerialize
	 @JsonDeserialize
	private Set<EmailAddress> emailAddress = new HashSet<>();
	
	public Set<EmailAddress> getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(Set<EmailAddress> emailAddress) {
		this.emailAddress = emailAddress;
	}

	public List<String> getUser() {
		return user;
	}

	public void setUser(List<String> user) {
		this.user = user;
	}

	public Long getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Long commentsCount) {
		this.commentsCount = commentsCount;
	}

	public Long getRelavantResourceArticle() {
		return relavantResourceArticle;
	}

	public void setRelavantResourceArticle(Long relavantResourceArticle) {
		this.relavantResourceArticle = relavantResourceArticle;
	}
	@Transient
    @JsonSerialize
    private Long relavantResourceArticle;
    
    public float getCompleteness() {
        return completeness;
    }

    public void setCompleteness(float completeness) {
        this.completeness = completeness;
    }

    private float completeness;

    public Set<DataArticle> getAuthoredArticles() {
        return authoredArticles;
    }

    public void setAuthoredArticles(Set<DataArticle> authoredArticles) {
        this.authoredArticles = authoredArticles;
    }

    public boolean isFollowed() {
        return followed;
    }

    public void setFollowed(boolean followed) {
        this.followed = followed;
    }

    @Transient
    @JsonSerialize
    private boolean followed = false;


    public boolean isCategoryPermissionOverride() {
        return categoryPermissionOverride;
    }

    public void setCategoryPermissionOverride(boolean permissionOverride) {
        this.categoryPermissionOverride = permissionOverride;
    }

    @Transient
    @JsonSerialize
    private boolean categoryPermissionOverride = false;

    public boolean isProviderPermissionOverride() {
        return providerPermissionOverride;
    }

    public void setProviderPermissionOverride(boolean providerPermissionOverride) {
        this.providerPermissionOverride = providerPermissionOverride;
    }

    @Transient
    @JsonSerialize
    private boolean providerPermissionOverride = false;

    public Authority getPermission() {
        return permission;
    }

    public void setPermission(Authority permission) {
        this.permission = permission;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    private Authority permission;

    @Column(name = "feature", insertable = false, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> features = new ArrayList<BasicBean>();

    @Column(name = "category", insertable = false, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> categories = new ArrayList<BasicBean>();

    @Column(name = "industry", insertable = false, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> industries = new ArrayList<BasicBean>();


    @Column(name = "tag", insertable = false, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> providerTags = new ArrayList<BasicBean>();

    @Column(name = "source", insertable = false, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> sources = new ArrayList<BasicBean>();

    @Column(name = "website")
    private String website;


    @Column(name = "short_description")
    private String shortDescription;


    @Column(name = "long_description")
    private String longDescription;

    /*@ManyToOne
    private LookupCode mainAssetClass;*/

    @Column(name = "main_asset_class")
    private String mainAssetClass;

    public String getMainAssetClass() {
        return mainAssetClass;
    }

    public void setMainAssetClass(String mainAssetClass) {
        this.mainAssetClass = mainAssetClass;
    }

    @Lob
    @Column(name = "notes")
    private String notes;


   /* @Max(value = 2)
    @Column(name = "live")
    private Integer live;*/

    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "created_date")
    private LocalDate createdDate;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "updated_date")
    private LocalDate updatedDate;

    @CreatedDate
    @Column(name = "web_created_date")
    @JsonIgnore
    private Instant webCreatedDate = Instant.now();

    @LastModifiedDate
    @Column(name = "web_updated_date")
    @JsonIgnore
    private Instant webUpdateDate = Instant.now();

    //  @NotNull
    @Column(name = "creator_user_id")
    private Long creatorUserId;

    // @NotNull
    @Column(name = "editor_user_id")
    private Long editorUserId;

    public Instant getWebCreatedDate() {
        return webCreatedDate;
    }

    public void setWebCreatedDate(Instant webCreatedDate) {
        this.webCreatedDate = webCreatedDate;
    }

    public Instant getWebUpdateDate() {
        return webUpdateDate;
    }

    public void setWebUpdateDate(Instant webUpdateDate) {
        this.webUpdateDate = webUpdateDate;
    }

    public Long getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(Long creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public Long getEditorUserId() {
        return editorUserId;
    }

    public void setEditorUserId(Long editorUserId) {
        this.editorUserId = editorUserId;
    }
    
    @Size(max = 512)
    @Column(name = "data_update_frequency_notes", length = 512)
    private String dataUpdateFrequencyNotes;

    public LookupCode getScoreOverall() {
        return scoreOverall;
    }

    public void setScoreOverall(LookupCode scoreOverall) {
        this.scoreOverall = scoreOverall;
    }

    public String getOrgRelationshipStatus() {
        return orgRelationshipStatus;
    }

    public void setOrgRelationshipStatus(String orgRelationshipStatus) {
        this.orgRelationshipStatus = orgRelationshipStatus;
    }

    public DataProvider orgRelationshipStatus(String OrgRelationshipStatus)
    {
        this.orgRelationshipStatus = orgRelationshipStatus;
        return  this;
    }

    public DataProvider overAllScore(LookupCode scoreOverall)
    {
        this.scoreOverall = scoreOverall;
        return this;
    }
    @Size(max = 64)
    @Column(name = "org_relationship_status", length = 64)
    private String orgRelationshipStatus;

    @ManyToOne
    private LookupCode scoreOverall;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "date_collection_began")
    private LocalDate dateCollectionBegan;

    @Size(max = 256)
    @Column(name = "date_collection_range_explanation", length = 256)
    private String dateCollectionRangeExplanation;

    @Lob
    @Column(name = "collection_methods_explanation")
    private String collectionMethodsExplanation;

    @Size(max = 512)
    @Column(name = "potential_drawbacks", length = 512)
    private String potentialDrawbacks;

    @Size(max = 1024)
    @Column(name = "unique_value_props", length = 1024)
    private String uniqueValueProps;

    @Size(max = 50)
    @Column(name = "historical_date_range", length = 50)
    private String historicalDateRange;

    @Size(max = 512)
    @Column(name = "key_data_fields", length = 512)
    private String keyDataFields;

    @Size(max = 1024)
    @Column(name = "summary", length = 1024)
    private String summary;

   /* @Size(max = 512)
    @Column(name = "research_methods_completed", length = 512)
    private String researchMethodsCompleted;*/



    @Column(name = "research_methods", insertable = false, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> researchMethods = new ArrayList<BasicBean>();

    @ManyToMany(fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_research_methods",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="research_methods_id", referencedColumnName="id"))
    private Set<LookupCode> researchMethodsCompleted  = new HashSet<>();


    @ManyToOne
    private LookupCode scoreInvestorsWillingness;


    @Column(name = "investor_clients")
    private String investor_clients;
/*
    @Size(max = 50)
    @Column(name = "historical_date_range_estimate", length = 50)
    private String historicalDateRangeEstimate;*/


    @Column(name = "public_companies_covered")
    private String publicCompaniesCovered;

    @ManyToOne
    private LookupCode scoreUniqueness;

    @ManyToOne
    private LookupCode scoreValue;

    /**************** New fields Start**************/

    @ManyToOne
    private LookupCode secRegulated;

    public LookupCode getSecRegulated() {
        return secRegulated;
    }
    public DataProvider secRegulated(LookupCode secRegulated)
    {
        this.secRegulated=secRegulated;
        return this;
    }

    public void setSecRegulated(LookupCode secRegulated) {
        this.secRegulated = secRegulated;
    }

    @ManyToOne
    private LookupCode profileVerificationStatus;

    public LookupCode getProfileVerificationStatus() {
        return profileVerificationStatus;
    }
    public DataProvider profileVerificationStatus(LookupCode profileVerificationStatus)
    {
        this.profileVerificationStatus=profileVerificationStatus;
        return this;
    }
    public void setProfileVerificationStatus(LookupCode profileVerificationStatus) {
        this.profileVerificationStatus = profileVerificationStatus;
    }

    @Column(name = "public_equities_covered_count")
    private Integer publicEquitiesCoveredCount;

    public Integer getPublicEquitiesCoveredCount() {
        return publicEquitiesCoveredCount;
    }
    public DataProvider publicEquitiesCoveredCount(Integer publicEquitiesCoveredCount)
    {
        this.publicEquitiesCoveredCount=publicEquitiesCoveredCount;
        return this;
    }
    public void setPublicEquitiesCoveredCount(Integer publicEquitiesCoveredCount) {
        this.publicEquitiesCoveredCount = publicEquitiesCoveredCount;
    }
    @Column(name="public_equities_covered_range")
    private String publicEquitiesCoveredRange;


    public String getPublicEquitiesCoveredRange() {
        return publicEquitiesCoveredRange;
    }

    public void setPublicEquitiesCoveredRange(String publicEquitiesCoveredRange) {
        this.publicEquitiesCoveredRange = publicEquitiesCoveredRange;
    }

    @ManyToOne
    private LookupCode trialPricingModel;

    public LookupCode getTrialPricingModel() {
        return trialPricingModel;
    }
    public DataProvider secTrialPricingModel(LookupCode trialPricingModel)
    {
        this.trialPricingModel=trialPricingModel;
        return this;
    }

    public void setTrialPricingModel(LookupCode trialPricingModel) {
        this.trialPricingModel = trialPricingModel;
    }

    @Column(name = "trial_duration")
    private String trialDuration;

    public String getTrialDuration() {
        return trialDuration;
    }
    public DataProvider trialDuration(String trialDuration)
    {
        this.trialDuration=trialDuration;
        return this;
    }

    public void setTrialDuration(String trialDuration) {
        this.trialDuration = trialDuration;
    }

    @ManyToOne
    private LookupCode trialAgreementType;

    public LookupCode getTrialAgreementType() {
        return trialAgreementType;
    }
    public DataProvider trialAgreementType(LookupCode trialAgreementType)
    {
        this.trialAgreementType=trialAgreementType;
        return this;
    }

    public void setTrialAgreementType(LookupCode trialAgreementType) {
        this.trialAgreementType = trialAgreementType;
    }

    @Column(name = "trial_pricing_details")
    private String trialPricingDetails;

    public String getTrialPricingDetails() {
        return trialPricingDetails;
    }

    public DataProvider trialPricingDetail(String trialPricingDetail) {
        this.trialPricingDetails = trialPricingDetail;
        return this;
    }

    public void setTrialPricingDetails(String trialPricingDetails) {
        this.trialPricingDetails = trialPricingDetails;
    }


    @Column(name = "lag_time")
    private String lagTime;

    public String getLagTime() {
        return lagTime;
    }
    public DataProvider lagTime(String lagTime)
    {
        this.lagTime=lagTime;
        return this;
    }
    public void setLagTime(String lagTime) {
        this.lagTime = lagTime;
    }

    @Column(name = "competitive_differentiators")
    private String competitiveDifferentiators;

    public String getCompetitiveDifferentiators() {
        return competitiveDifferentiators;
    }

    public DataProvider competitiveDifferentiator(String competitiveDifferentiator)
    {
        this.competitiveDifferentiators=competitiveDifferentiator;
        return this;
    }

    public void setCompetitiveDifferentiators(String competitiveDifferentiators) {
        this.competitiveDifferentiators = competitiveDifferentiators;
    }

    @Column(name = "discussion_analytics")
    private String discussionAnalytics;

    public String getDiscussionAnalytics() {
        return discussionAnalytics;
    }
    public DataProvider discussionAnalytics(String discussionAnalytics)
    {
        this.discussionAnalytics=discussionAnalytics;
        return this;
    }

    public void setDiscussionAnalytics(String discussionAnalytics) {
        this.discussionAnalytics = discussionAnalytics;
    }

    @Column(name = "discussion_cleanliness")
    private String discussionCleanliness;

    public String getDiscussionCleanliness() {
        return discussionCleanliness;
    }
    public DataProvider discussionCleanlines(String discussionCleanlines)
    {
        this.discussionCleanliness=discussionCleanlines;
        return this;
    }

    public void setDiscussionCleanliness(String discussionCleanliness) {
        this.discussionCleanliness = discussionCleanliness;
    }

    @Column(name = "sample_or_panel_biases")
    private String sampleOrPanelBiases;

    public String getSampleOrPanelBiases() {
        return sampleOrPanelBiases;
    }
    public DataProvider sampleOrPanelBiase(String sampleOrPanelBiase)
    {
        this.sampleOrPanelBiases=sampleOrPanelBiase;
        return this;
    }

    public void setSampleOrPanelBiases(String sampleOrPanelBiases) {
        this.sampleOrPanelBiases = sampleOrPanelBiases;
    }
    @Column(name="dataset_size")
    private String datasetSize;

   /* @ManyToOne
    private LookupCode datasetSize;*/

    public String getDatasetSize() {
        return datasetSize;
    }

    public void setDatasetSize(String datasetSize) {
        this.datasetSize = datasetSize;
    }

    /*  public LookupCode getDatasetSize() {
        return datasetSize;
    }
    public DataProvider datasetSize(LookupCode datasetSize)
    {
        this.datasetSize=datasetSize;
        return this;
    }
    public void setDatasetSize(LookupCode datasetSize) {
        this.datasetSize = datasetSize;
    }
*/
    @Column(name="dataset_number_of_rows")
    private String datasetNumberOfRows;

    public String getDatasetNumberOfRows() {
        return datasetNumberOfRows;
    }

    public void setDatasetNumberOfRows(String datasetNumberOfRows) {
        this.datasetNumberOfRows = datasetNumberOfRows;
    }
    /*  @ManyToOne
    private LookupCode datasetNumberOfRows;
*/
    /*public LookupCode getDatasetNumberOfRows() {
        return datasetNumberOfRows;
    }
    public DataProvider datasetNumberOfRow(LookupCode datasetNumberOfRow)
    {
        this.datasetNumberOfRows=datasetNumberOfRow;
        return this;
    }

    public void setDatasetNumberOfRows(LookupCode datasetNumberOfRows) {
        this.datasetNumberOfRows = datasetNumberOfRows;
    }
*/
    @Column(name = "number_of_analyst_employees")
    private Long numberOfAnalystEmployees;

    public Long getNumberOfAnalystEmployees() {
        return numberOfAnalystEmployees;
    }
    public DataProvider numberOfAnalystEmployee(Long numberOfAnalystEmployee) {
        this.numberOfAnalystEmployees = numberOfAnalystEmployee;
        return this;
    }
    public void setNumberOfAnalystEmployees(Long numberOfAnalystEmployees) {
        this.numberOfAnalystEmployees = numberOfAnalystEmployees;
    }

    @Column(name = "investor_clients_count")
    private Long investorClientsCount;

    public Long getInvestorClientsCount() {
        return investorClientsCount;
    }

    public DataProvider investorClientsCount(Long investorClientsCounts) {
        this.investorClientsCount = investorClientsCounts;
        return this;
    }
    public void setInvestorClientsCount(Long investorClientsCount) {
        this.investorClientsCount = investorClientsCount;
    }




    @ManyToOne
    private LookupCode pointInTimeAccuracy;

    public LookupCode getPointInTimeAccuracy() {
        return pointInTimeAccuracy;
    }
    public DataProvider pointInTimeAccuracy(LookupCode pointInTimeAccuracy) {
        this.pointInTimeAccuracy = pointInTimeAccuracy;
        return this;
    }

    public void setPointInTimeAccuracy(LookupCode pointInTimeAccuracy) {
        this.pointInTimeAccuracy = pointInTimeAccuracy;
    }

    @Column(name = "data_sources_details")
    private String dataSourcesDetails;

    public String getDataSourcesDetails() {
        return dataSourcesDetails;
    }

    public DataProvider dataSourcesDetail(String dataSourcesDetail) {
        this.dataSourcesDetails = dataSourcesDetail;
        return this;
    }

    public void setDataSourcesDetails(String dataSourcesDetails) {
        this.dataSourcesDetails = dataSourcesDetails;
    }

    @ManyToOne
    private LookupCode dataGaps;

    public LookupCode getDataGaps() {
        return dataGaps;
    }
    public DataProvider dataGap(LookupCode dataGap) {
        this.dataGaps = dataGap;
        return this;
    }

    public void setDataGaps(LookupCode dataGaps) {
        this.dataGaps = dataGaps;
    }

    @ManyToOne
    private LookupCode includesOutliers;

    public LookupCode getIncludesOutliers() {
        return includesOutliers;
    }
    public DataProvider includesOutlier(LookupCode includesOutlier) {
        this.includesOutliers = includesOutlier;
        return this;
    }
    public void setIncludesOutliers(LookupCode includesOutliers) {
        this.includesOutliers = includesOutliers;
    }

    @ManyToOne
    private LookupCode normalized;

    public LookupCode getNormalized() {
        return normalized;
    }

    public DataProvider normalized(LookupCode normalized) {
        this.normalized = normalized;
        return this;
    }
    public void setNormalized(LookupCode normalized) {
        this.normalized = normalized;
    }

    @ManyToOne
    private LookupCode duplicatesCleaned;

    public LookupCode getDuplicatesCleaned() {
        return duplicatesCleaned;
    }
    public DataProvider duplicatesCleaned(LookupCode duplicatesCleaned) {
        this.duplicatesCleaned = duplicatesCleaned;
        return this;
    }

    public void setDuplicatesCleaned(LookupCode duplicatesCleaned) {
        this.duplicatesCleaned = duplicatesCleaned;
    }

    @Column(name="investor_client_bucket")
    private String investorClientBucket;

  /*  @ManyToOne
    private LookupCode investorClientBucket;*/

    /*public LookupCode getInvestorClientBucket() {
        return investorClientBucket;
    }
    public DataProvider investorClientBucket(LookupCode investorClientBucket) {
        this.investorClientBucket = investorClientBucket;
        return this;
    }
    public void setInvestorClientBucket(LookupCode investorClientBucket) {
        this.investorClientBucket = investorClientBucket;
    }
*/


    public String getInvestorClientBucket() {
        return investorClientBucket;
    }

    public void setInvestorClientBucket(String investorClientBucket) {
        this.investorClientBucket = investorClientBucket;
    }

    @Column(name = "pricing_details")
    private String pricingDetails;


    public String getPricingDetails() {
        return pricingDetails;
    }

    public DataProvider pricingDetail(String pricingDetail)
    {
        this.pricingDetails=pricingDetail;
        return this;
    }
    public void setPricingDetails(String pricingDetails) {
        this.pricingDetails = pricingDetails;
    }

    @ManyToOne
    private LookupCode subscriptionTimePeriod;

    public LookupCode getSubscriptionTimePeriod() {
        return subscriptionTimePeriod;
    }

    public DataProvider subscriptionTimePeriod(LookupCode subscriptionTimePeriod) {
        this.subscriptionTimePeriod = subscriptionTimePeriod;
        return this;
    }

    public void setSubscriptionTimePeriod(LookupCode subscriptionTimePeriod) {
        this.subscriptionTimePeriod = subscriptionTimePeriod;
    }

    @Column(name = "year_first_data_product_launched")
    private Integer yearFirstDataProductLaunched;

    public Integer getYearFirstDataProductLaunched() {
        return yearFirstDataProductLaunched;
    }

    public DataProvider yearFirstDataProductLaunched(Integer yearFirstDataProductLaunched) {
        this.yearFirstDataProductLaunched = yearFirstDataProductLaunched;
        return this;
    }

    public void setYearFirstDataProductLaunched(Integer yearFirstDataProductLaunched) {
        this.yearFirstDataProductLaunched = yearFirstDataProductLaunched;
    }

   /* @Column(name = "compliance_score_personal")
    private String complianceScorePersonal;*/

    /*public String getComplianceScorePersonal() {
        return complianceScorePersonal;
    }

    public DataProvider complianceScorePersonal(String complianceScorePersonal) {
        this.complianceScorePersonal = complianceScorePersonal;
        return this;
    }

    public void setComplianceScorePersonal(String complianceScorePersonal) {
        this.complianceScorePersonal = complianceScorePersonal;
    }
*/
    /*@Column(name = "compliance_score_public")
    private String complianceScorePublic;

    public String getComplianceScorePublic() {
        return complianceScorePublic;
    }

    public DataProvider complianceScorePublic(String complianceScorePublic) {
        this.complianceScorePublic = complianceScorePublic;
        return this;
    }

    public void setComplianceScorePublic(String complianceScorePublic) {
        this.complianceScorePublic = complianceScorePublic;
    }
*/
    @ManyToOne
    private LookupCode monthlyPriceRange;

    public LookupCode getMonthlyPriceRange() {
        return monthlyPriceRange;
    }

    public DataProvider monthlyPriceRange(LookupCode monthlyPriceRange) {
        this.monthlyPriceRange = monthlyPriceRange;
        return this;
    }

    public void setMonthlyPriceRange(LookupCode monthlyPriceRange) {
        this.monthlyPriceRange = monthlyPriceRange;
    }

    @ManyToOne
    private LookupCode subscriptionModel;

    public LookupCode getSubscriptionModel() {
        return subscriptionModel;
    }
    public DataProvider subscriptionModel(LookupCode subscriptionModel) {
        this.subscriptionModel = subscriptionModel;
        return this;
    }
    public void setSubscriptionModel(LookupCode subscriptionModel) {
        this.subscriptionModel = subscriptionModel;
    }

    @Column(name = "minimum_yearly_price")
    private String minimumYearlyPrice;

    public String getMinimumYearlyPrice() {
        return minimumYearlyPrice;
    }

    public DataProvider minimumYearlyPrice(String minimumYearlyPrice) {
        this.minimumYearlyPrice = minimumYearlyPrice;
        return this;
    }

    public void setMinimumYearlyPrice(String minimumYearlyPrice) {
        this.minimumYearlyPrice = minimumYearlyPrice;
    }

    @Column(name = "maximum_yearly_price")
    private String maximumYearlyPrice;

    public String getMaximumYearlyPrice() {
        return maximumYearlyPrice;
    }

    public DataProvider maximumYearlyPrice(String maximumYearlyPrice) {
        this.maximumYearlyPrice = maximumYearlyPrice;
        return this;
    }

    public void setMaximumYearlyPrice(String maximumYearlyPrice) {
        this.maximumYearlyPrice = maximumYearlyPrice;
    }

    @ManyToOne
    private LookupCode numberOfDataSources;


    public LookupCode getNumberOfDataSources() {
        return numberOfDataSources;
    }

    public DataProvider numberOfDataSource(LookupCode numberOfDataSource) {
        this.numberOfDataSources = numberOfDataSource;
        return this;
    }

    public void setNumberOfDataSources(LookupCode numberOfDataSources) {
        this.numberOfDataSources = numberOfDataSources;
    }


    @Column(name = "privacy")
    private String privacy;


    public String getPrivacy() {
        return privacy;
    }
    public DataProvider privacy(String privacy) {
        this.privacy = privacy;
        return this;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }


    @ManyToOne
    private LookupCode marketplaceStatus;

    public LookupCode getMarketplaceStatus() {
        return marketplaceStatus;
    }

    public void setMarketplaceStatus(LookupCode marketplaceStatus) {
        this.marketplaceStatus = marketplaceStatus;
    }

    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_identifiers",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_provider_identifier_id", referencedColumnName="id"))
    private Set<LookupCode> identifiersAvailable = new HashSet<>();


    public Set<LookupCode> getidentifiersAvailable() {
        return identifiersAvailable;
    }

    public DataProvider identifiersAvailable(Set<LookupCode> lookupCodes) {
        this.identifiersAvailable = lookupCodes;
        return this;
    }

    /*public DataProvider addIdentifiersAvailable(LookupCode lookupCode) {
        this.identifiersAvailable.add(lookupCode);
        lookupCode.getProviderIdentifiersAvailable().add(this);
        return this;
    }

    public DataProvider removeIdentifiersAvailable(LookupCode lookupCode) {
        this.identifiersAvailable.remove(lookupCode);
        lookupCode.getProviderIdentifiersAvailable().remove(this);
        return this;
    }*/

    public void setIdentifiersAvailable(Set<LookupCode> identifiersAvailables) {
        this.identifiersAvailable = identifiersAvailables;
    }

    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_equities_style",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_provider_equities_style_id", referencedColumnName="id"))
    private Set<LookupCode> equitiesStyle = new HashSet<>();


    public Set<LookupCode> getEquitiesStyle() {
        return equitiesStyle;
    }

    public DataProvider equitiesStyle(Set<LookupCode> lookupCodes) {
        this.equitiesStyle = lookupCodes;
        return this;
    }

    /*public DataProvider addEquitiesStyle(LookupCode lookupCode) {
        this.equitiesStyle.add(lookupCode);
        lookupCode.getProviderEquitiesStyle().add(this);
        return this;
    }

    public DataProvider removeEquitiesStyle(LookupCode lookupCode) {
        this.equitiesStyle.remove(lookupCode);
        lookupCode.getProviderEquitiesStyle().remove(this);
        return this;
    }
*/
    public void setEquitiesStyle(Set<LookupCode> equitiesStyles) {
        this.equitiesStyle = equitiesStyles;
    }



    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_data_types",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_type_id", referencedColumnName="id"))
    private Set<LookupCode> dataTypes = new HashSet<>();


    public Set<LookupCode> getDataTypes() {
        return dataTypes;
    }

    public DataProvider dataTypes(Set<LookupCode> lookupCodes) {
        this.dataTypes = lookupCodes;
        return this;
    }

   /* public DataProvider addDataTypes(LookupCode lookupCode) {
        this.dataTypes.add(lookupCode);
        lookupCode.getProviderDataTypes().add(this);
        return this;
    }

    public DataProvider removeDataTypes(LookupCode lookupCode) {
        this.dataTypes.remove(lookupCode);
        lookupCode.getProviderDataTypes().remove(this);
        return this;
    }*/

    public void setDataTypes(Set<LookupCode> dataType) {
        this.dataTypes = dataType;
    }


    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_data_product_types",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_product_type_id", referencedColumnName="id"))
    private Set<LookupCode> dataProductTypes = new HashSet<>();


    public Set<LookupCode> getDataProductTypes() {
        return dataProductTypes;
    }

    public DataProvider dataProductTypes(Set<LookupCode> lookupCodes) {
        this.dataProductTypes = lookupCodes;
        return this;
    }

    /*public DataProvider addDataProductTypes(LookupCode lookupCode) {
        this.dataProductTypes.add(lookupCode);
        lookupCode.getProviderDataProductTypes().add(this);
        return this;
    }

    public DataProvider removeDataProductTypes(LookupCode lookupCode) {
        this.dataProductTypes.remove(lookupCode);
        lookupCode.getProviderDataProductTypes().remove(this);
        return this;
    }
*/
    public void setDataProductTypes(Set<LookupCode> dataProductType) {
        this.dataProductTypes = dataProductType;
    }


    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_access",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_provider_access_id", referencedColumnName="id"))
    private Set<LookupCode> accessOffered = new HashSet<>();


    public Set<LookupCode> getAccessOffered() {
        return accessOffered;
    }

    public DataProvider accessOffered(Set<LookupCode> lookupCodes) {
        this.accessOffered = lookupCodes;
        return this;
    }

 /*   public DataProvider addAccessOffered(LookupCode lookupCode) {
        this.accessOffered.add(lookupCode);
        lookupCode.getProviderAccessOffered().add(this);
        return this;
    }

    public DataProvider removeAccessOffered(LookupCode lookupCode) {
        this.accessOffered.remove(lookupCode);
        lookupCode.getProviderAccessOffered().remove(this);
        return this;
    }*/

    public void setAccessOffered(Set<LookupCode> accessOffereds) {
        this.accessOffered = accessOffereds;
    }

    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_time_frame",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_provider_time_frame_id", referencedColumnName="id"))
    private Set<LookupCode> investingTimeFrame = new HashSet<>();


    public Set<LookupCode> getInvestingTimeFrame() {
        return investingTimeFrame;
    }

    public DataProvider investingTimeFrame(Set<LookupCode> lookupCodes) {
        this.investingTimeFrame = lookupCodes;
        return this;
    }

    /*  public DataProvider addInvestingTimeFrame(LookupCode lookupCode) {
          this.investingTimeFrame.add(lookupCode);
          lookupCode.getProviderInvestingTimeFrame().add(this);
          return this;
      }

      public DataProvider removeInvestingTimeFrame(LookupCode lookupCode) {
          this.investingTimeFrame.remove(lookupCode);
          lookupCode.getProviderInvestingTimeFrame().remove(this);
          return this;
      }
  */
    public void setInvestingTimeFrame(Set<LookupCode> investingTimeFrames) {
        this.investingTimeFrame = investingTimeFrames;
    }

    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_market_cap",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_provider_market_cap_id", referencedColumnName="id"))
    private Set<LookupCode> equitiesMarketCap = new HashSet<>();


    public Set<LookupCode> getEquitiesMarketCap() {
        return equitiesMarketCap;
    }

    public DataProvider equitiesMarketCap(Set<LookupCode> lookupCodes) {
        this.equitiesMarketCap = lookupCodes;
        return this;
    }

   /* public DataProvider addEquitiesMarketCap(LookupCode lookupCode) {
        this.equitiesMarketCap.add(lookupCode);
        lookupCode.getProviderEquitiesMarketCap().add(this);
        return this;
    }

    public DataProvider removeEquitiesMarketCap(LookupCode lookupCode) {
        this.equitiesMarketCap.remove(lookupCode);
        lookupCode.getProviderEquitiesMarketCap().remove(this);
        return this;
    }*/

    public void setEquitiesMarketCap(Set<LookupCode> equitiesMarketCaps) {
        this.equitiesMarketCap = equitiesMarketCaps;
    }

    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_investment_strategies",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_provider_investment_strategies_id", referencedColumnName="id"))
    private Set<LookupCode> strategies = new HashSet<>();


    public Set<LookupCode> getStrategies() {
        return strategies;
    }

    public DataProvider strategies(Set<LookupCode> lookupCodes) {
        this.strategies = lookupCodes;
        return this;
    }

  /*  public DataProvider addStrategies(LookupCode lookupCode) {
        this.strategies.add(lookupCode);
        lookupCode.getProviderStrategies().add(this);
        return this;
    }

    public DataProvider removeStrategies(LookupCode lookupCode) {
        this.strategies.remove(lookupCode);
        lookupCode.getProviderStrategies().remove(this);
        return this;
    }*/

    public void setStrategies(Set<LookupCode> strategie) {
        this.strategies = strategie;
    }

    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_research_style",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_provider_research_style_id", referencedColumnName="id"))
    private Set<LookupCode> researchStyles = new HashSet<>();


    public Set<LookupCode> getResearchStyles() {
        return researchStyles;
    }

    public DataProvider researchStyles(Set<LookupCode> lookupCodes) {
        this.researchStyles = lookupCodes;
        return this;
    }

   /* public DataProvider addResearchStyles(LookupCode lookupCode) {
        this.researchStyles.add(lookupCode);
        lookupCode.getProviderResearchStyles().add(this);
        return this;
    }

    public DataProvider removeResearchStyles(LookupCode lookupCode) {
        this.researchStyles.remove(lookupCode);
        lookupCode.getProviderResearchStyles().remove(this);
        return this;
    }*/

    public void setResearchStyles(Set<LookupCode> researchStyle) {
        this.researchStyles = researchStyle;
    }

   
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_manager_type",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_provider_manager_type_id", referencedColumnName="id"))
    private Set<LookupCode> managerType = new HashSet<>();


    public Set<LookupCode> getManagerType() {
        return managerType;
    }

    public DataProvider managerType(Set<LookupCode> lookupCodes) {
        this.managerType = lookupCodes;
        return this;
    }

 /*   public DataProvider addManagerType(LookupCode lookupCode) {
        this.managerType.add(lookupCode);
        lookupCode.getProviderManagerType().add(this);
        return this;
    }

    public DataProvider removeManagerType(LookupCode lookupCode) {
        this.managerType.remove(lookupCode);
        lookupCode.getProviderManagerType().remove(this);
        return this;
    }*/

    public void setManagerType(Set<LookupCode> managerTypes) {
        this.managerType = managerTypes;
    }

    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_target_organization",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_provider_target_organization_id", referencedColumnName="id"))
    private Set<LookupCode> organizationType = new HashSet<>();


    public Set<LookupCode> getOrganizationType() {
        return organizationType;
    }

    public DataProvider organizationType(Set<LookupCode> lookupCodes) {
        this.organizationType = lookupCodes;
        return this;
    }

   /* public DataProvider addOrganizationType(LookupCode lookupCode) {
        this.organizationType.add(lookupCode);
        lookupCode.getProviderOrganizationType().add(this);
        return this;
    }

    public DataProvider removeOrganizationType(LookupCode lookupCode) {
        this.organizationType.remove(lookupCode);
        lookupCode.getProviderOrganizationType().remove(this);
        return this;
    }*/

    public void setOrganizationType(Set<LookupCode> organizationTypes) {
        this.organizationType = organizationTypes;
    }

    //@JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_languages",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_provider_languages_id", referencedColumnName="id"))
    private Set<LookupCode> languagesAvailable = new HashSet<>();


    public Set<LookupCode> getLanguagesAvailable() {
        return languagesAvailable;
    }

    public DataProvider languagesAvailable(Set<LookupCode> lookupCodes) {
        this.languagesAvailable = lookupCodes;
        return this;
    }

    /* public DataProvider addLanguagesAvailable(LookupCode lookupCode) {
         this.languagesAvailable.add(lookupCode);
         lookupCode.getProviderLanguagesAvailable().add(this);
         return this;
     }

     public DataProvider removeLanguagesAvailable(LookupCode lookupCode) {
         this.languagesAvailable.remove(lookupCode);
         lookupCode.getProviderLanguagesAvailable().remove(this);
         return this;
     }
 */
    public void setLanguagesAvailable(Set<LookupCode> languagesAvailables) {
        this.languagesAvailable = languagesAvailables;
    }

    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_payment_method",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_provider_payment_method_id", referencedColumnName="id"))
    private Set<LookupCode> paymentMethodsOffered = new HashSet<>();

    public Set<LookupCode> getPaymentMethodsOffered() {
        return paymentMethodsOffered;
    }

    public DataProvider paymentMethodsOffered(Set<LookupCode> lookupCodes) {
        this.paymentMethodsOffered = lookupCodes;
        return this;
    }

    /*public DataProvider addPaymentMethodsOffered(LookupCode lookupCode) {
        this.paymentMethodsOffered.add(lookupCode);
        lookupCode.getProviderPaymentMethodsOffered().add(this);
        return this;
    }

    public DataProvider removePaymentMethodsOffered(LookupCode lookupCode) {
        this.paymentMethodsOffered.remove(lookupCode);
        lookupCode.getProviderPaymentMethodsOffered().remove(this);
        return this;
    }
*/
    public void setPaymentMethodsOffered(Set<LookupCode> paymentMethodsOffereds) {
        this.paymentMethodsOffered = paymentMethodsOffereds;
    }



    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_gaps_reason",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_gaps_reason_id", referencedColumnName="id"))
    private Set<LookupCode> dataGapsReasons = new HashSet<>();

    public Set<LookupCode> getDataGapsReasons() {
        return dataGapsReasons;
    }

    public DataProvider dataGapsReasons(Set<LookupCode> lookupCodes) {
        this.dataGapsReasons = lookupCodes;
        return this;
    }

    /*  public DataProvider addDataGapsReasons(LookupCode lookupCode) {
          this.dataGapsReasons.add(lookupCode);
          lookupCode.getProviderDataGapsReasons().add(this);
          return this;
      }

      public DataProvider removeDataGapsReasons(LookupCode lookupCode) {
          this.dataGapsReasons.remove(lookupCode);
          lookupCode.getProviderDataGapsReasons().remove(this);
          return this;
      }
  */
    public void setDataGapsReasons(Set<LookupCode> dataGapsReason) {
        this.dataGapsReasons = dataGapsReason;
    }


    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_outlier_reasons",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_outlier_reasons_id", referencedColumnName="id"))
    private Set<LookupCode> outlierReasons = new HashSet<>();

    public Set<LookupCode> getOutlierReasons() {
        return outlierReasons;
    }

    public DataProvider outlierReasons(Set<LookupCode> lookupCodes) {
        this.outlierReasons = lookupCodes;
        return this;
    }

    /*public DataProvider addOutlierReasons(LookupCode lookupCode) {
        this.outlierReasons.add(lookupCode);
        lookupCode.getProviderOutlierReasons().add(this);
        return this;
    }

    public DataProvider removeOutlierReasons(LookupCode lookupCode) {
        this.outlierReasons.remove(lookupCode);
        lookupCode.getProviderOutlierReasons().remove(this);
        return this;
    }
*/
    public void setOutlierReasons(Set<LookupCode> outlierReason) {
        this.outlierReasons = outlierReason;
    }

    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_biases",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="data_biases_id", referencedColumnName="id"))
    private Set<LookupCode> biases = new HashSet<>();

    public Set<LookupCode> getBiases() {
        return biases;
    }

    public DataProvider biases(Set<LookupCode> lookupCodes) {
        this.biases = lookupCodes;
        return this;
    }

  /*  public DataProvider addBiases(LookupCode lookupCode) {
        this.biases.add(lookupCode);
        lookupCode.getProviderBiases().add(this);
        return this;
    }

    public DataProvider removeBiases(LookupCode lookupCode) {
        this.biases.remove(lookupCode);
        lookupCode.getProviderBiases().remove(this);
        return this;
    }*/

    public void setBiases(Set<LookupCode> biase) {
        this.biases = biase;
    }

    /**************** New fields End**************/
    @ManyToOne
    private LookupCode scoreReadiness;

    /*@Size(max = 50)
    @Column(name = "free_trial_available", length = 50)
    private String freeTrialAvailable;*/

    @Column(name = "product_details")
    private String productDetails;

    @Size(max = 20)
    @Column(name = "permission_name", updatable = false, insertable = false)
    private String userPermission;

    public String getSampleOrPanelSize() {
        return sampleOrPanelSize;
    }

    public void setSampleOrPanelSize(String sampleOrPanelSize) {
        this.sampleOrPanelSize = sampleOrPanelSize;
    }

    public String getUseCasesOrQuestionsAddressed() {
        return useCasesOrQuestionsAddressed;
    }

    public void setUseCasesOrQuestionsAddressed(String useCasesOrQuestionsAddressed) {
        this.useCasesOrQuestionsAddressed = useCasesOrQuestionsAddressed;
    }

    /*public String getDataLicenseType() {
        return dataLicenseType;
    }

    public void setDataLicenseType(String dataLicenseType) {
        this.dataLicenseType = dataLicenseType;
    }*/

    public String getDataLicenseTypeDetails() {
        return dataLicenseTypeDetails;
    }

    public void setDataLicenseTypeDetails(String dataLicenseTypeDetails) {
        this.dataLicenseTypeDetails = dataLicenseTypeDetails;
    }
    public String getLegalAndComplianceIssues() {
        return legalAndComplianceIssues;
    }

    public void setLegalAndComplianceIssues(String legalAndComplianceIssues) {
        this.legalAndComplianceIssues = legalAndComplianceIssues;
    }

    public String getDataRoadmap() {
        return dataRoadmap;
    }

    public void setDataRoadmap(String dataRoadmap) {
        this.dataRoadmap = dataRoadmap;
    }

    @Column (name ="Sample_or_Panel_Size")
    private String sampleOrPanelSize;

    @Column(name="Use_Cases_or_Questions_Addressed")
    private String useCasesOrQuestionsAddressed;

    /*@Column(name="Data_License_Type")
    private String dataLicenseType;*/

    @ManyToOne
    private LookupCode dataLicenseType;

    public LookupCode getDataLicenseType() {
        return dataLicenseType;
    }

    public void setDataLicenseType(LookupCode dataLicenseType) {
        this.dataLicenseType = dataLicenseType;
    }

    @Column(name="Data_License_Type_Details")
    private String dataLicenseTypeDetails;

    @Column(name ="Legal_and_Compliance_Issues")
    private String legalAndComplianceIssues;

    @Column(name="Data_Roadmap")
    private String dataRoadmap;

    @Column(name="out_of_sample_data")
    private String outOfSampleData;

    /*@ManyToOne
    private LookupCode pricingModel;*/
    
    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_pricing_model",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="pricing_models_id", referencedColumnName="id"))
    private Set<LookupCode> dataProvidersPricingModels = new HashSet<>();
    
    public String getOutOfSampleData() {
        return outOfSampleData;
    }

    public Set<LookupCode> getdataProvidersPricingModels() {
		return dataProvidersPricingModels;
	}

	public void setdataProvidersPricingModels(Set<LookupCode> dataProvidersPricingModels) {
		this.dataProvidersPricingModels = dataProvidersPricingModels;
	}

	public void setOutOfSampleData(String outOfSampleData) {
        this.outOfSampleData = outOfSampleData;
    }

   /* public LookupCode getPricingModel() {
        return pricingModel;
    }

    public void setPricingModel(LookupCode pricingModel) {
        this.pricingModel = pricingModel;
    }*/

    public String getUserPermission() {
        return userPermission;
    }

    public void setUserPermission(String permission) {
        this.userPermission = permission;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private DataCategory mainDataCategory;

    @ManyToOne(fetch = FetchType.LAZY)
    private DataOrganization ownerOrganization;

    @ManyToOne(fetch = FetchType.LAZY)
    private DataIndustry mainDataIndustry;

    @ManyToOne(fetch = FetchType.LAZY)
    private DataProviderType providerType;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_geographical_focus",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="geographical_foci_id", referencedColumnName="id"))
    private Set<LookupCode> geographicalFoci = new HashSet<>();

    /*@ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_partner", joinColumns =@JoinColumn(name="data_providers_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="partners_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "partner_type = " + Constants.LOOKUP_PROVIDER_DATA_PARTNER)
    @SQLInsert(sql = "insert into data_provider_partner (data_providers_id,partners_id,partner_type) values (?, ?, 16)")
    private Set<DataProvider> providerPartners = new HashSet<>();*/

    @ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_partner", joinColumns =@JoinColumn(name="data_providers_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="partners_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "partner_type = " + Constants.LOOKUP_PROVIDER_DATA_PARTNER)
    @SQLInsert(sql = "insert into data_provider_partner (data_providers_id,partners_id,partner_type) values (?, ?, 16)")
    private Set<DataProvider> providerPartners = new HashSet<>();

    public Set<DataProvider> getDistributionPartners() {
        return distributionPartners;
    }

    public void setDistributionPartners(Set<DataProvider> distributionPartners) {
        this.distributionPartners = distributionPartners;
    }

    public Set<DataProvider> getConsumerPartners() {
        return consumerPartners;
    }

    public void setConsumerPartners(Set<DataProvider> consumerPartners) {
        this.consumerPartners = consumerPartners;
    }

    /*@ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_partner",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="partners_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "partner_type = " + Constants.LOOKUP_DISTRIBUTION_DATA_PARTNER)
    @SQLInsert(sql = "insert into data_provider_partner (data_providers_id,partners_id,partner_type) values (?, ?, 17)")
    private Set<DataProvider> distributionPartners = new HashSet<>();*/


    /*@ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_partner",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="partners_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "partner_type = " + Constants.LOOKUP_DISTRIBUTION_DATA_PARTNER)
    @SQLInsert(sql = "insert into data_provider_partner (data_providers_id,partners_id,partner_type) values (?, ?, 17)")
    private Set<DataProvider> distributionPartners = new HashSet<>();*/

    @ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_partner",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="partners_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "partner_type = " + Constants.LOOKUP_DISTRIBUTION_DATA_PARTNER)
    @SQLInsert(sql = "insert into data_provider_partner (data_providers_id,partners_id,partner_type) values (?, ?, 17)")
    private Set<DataProvider> distributionPartners = new HashSet<>();



    @ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_partner",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="partners_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "partner_type = " + Constants.LOOKUP_COMPLEMENTARY_DATA_PARTNER)
    @SQLInsert(sql = "insert into data_provider_partner (data_providers_id,partners_id,partner_type) values (?, ?, 15)")
    private Set<DataProvider> complementaryProviders = new HashSet<>();


    @ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_partner",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="partners_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "partner_type = " + Constants.LOOKUP_COMPETITORS)
    @SQLInsert(sql = "insert into data_provider_partner (data_providers_id,partners_id,partner_type) values (?, ?, 1720)")
    private Set<DataProvider> competitors = new HashSet<>();

    public Set<DataProvider> getComplementaryProviders() {
        return complementaryProviders;
    }

    public void setComplementaryProviders(Set<DataProvider> complementaryProviders) {
        this.complementaryProviders = complementaryProviders;
    }

    public Set<DataProvider> getCompetitors() {
        return competitors;
    }

    public void setCompetitors(Set<DataProvider> competitors) {
        this.competitors = competitors;
    }

    /*@ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_partner",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="partners_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "partner_type = " + Constants.LOOKUP_CONSUMER_DATA_PARTNER)
    @SQLInsert(sql = "insert into data_provider_partner (data_providers_id,partners_id,partner_type) values (?, ?, 18)")
    private Set<DataProvider> consumerPartners = new HashSet<>();*/

    @ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_partner",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="partners_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "partner_type = " + Constants.LOOKUP_CONSUMER_DATA_PARTNER)
    @SQLInsert(sql = "insert into data_provider_partner (data_providers_id,partners_id,partner_type) values (?, ?, 18)")
    private Set<DataProvider> consumerPartners = new HashSet<>();

    @ManyToMany
    //@JsonIgnore
    @OrderBy("publishedDate DESC")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_articles",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="articles_id", referencedColumnName = "id"))
    private Set<DataArticle> relevantArticles = new HashSet<>();

    @Column(name = "asset_class", insertable = false, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List assetClasses = new ArrayList<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_investor_type",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="investor_types_id", referencedColumnName="id"))
    private Set<LookupCode> investorTypes = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_industry",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="industries_id", referencedColumnName="id"))
    private Set<DataIndustry> providerIndustries = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_category",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="categories_id", referencedColumnName="id"))
    private Set<DataCategory> dataProviderCategories = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_feature",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="features_id", referencedColumnName="id"))
    private Set<DataFeature> dataProviderFeatures = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_source",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="sources_id", referencedColumnName="id"))
    private Set<DataSource> dataProviderSources = new HashSet<>();

    @Column(name = "security_type", insertable = false, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> securityTypes = new ArrayList<BasicBean>();

    @Column(name = "delivery_method", insertable = false, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> deliveryMethods = new ArrayList<BasicBean>();

    public List<BasicBean> getResearchMethods() {
        return researchMethods;
    }

    public void setResearchMethods(List<BasicBean> researchMethods) {
        this.researchMethods = researchMethods;
    }

    @Column(name = "delivery_format", insertable = false, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> deliveryFormats = new ArrayList<BasicBean>();

    @Column(name = "pricing_model", insertable = false, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> pricingModels = new ArrayList<BasicBean>();


    public List<BasicBean> getPricingModels() {
		return pricingModels;
	}

	public void setPricingModels(List<BasicBean> pricingModels) {
		this.pricingModels = pricingModels;
	}
	@ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_delivery_frequency",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="delivery_frequencies_id", referencedColumnName="id"))
    private Set<LookupCode> deliveryFrequencies = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_delivery_format",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="delivery_formats_id", referencedColumnName="id"))
    private Set<LookupCode> dataProvidersDeliveryFormats = new HashSet<>();


    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_delivery_method",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="delivery_methods_id", referencedColumnName="id"))
    private Set<LookupCode> dataProvidersDeliveryMethods = new HashSet<>();

    /* @ManyToMany
     @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
     @JoinTable(name = "data_provider_pricing_model",
         joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
         inverseJoinColumns = @JoinColumn(name="pricing_models_id", referencedColumnName="id"))
     private Set<LookupCode> dataProvidersPricingModels = new HashSet<>();
 */
    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_asset_class",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="asset_classes_id", referencedColumnName="id"))
    private Set<LookupCode> dataProvidersAssetClasses = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_relevant_sector",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="relevant_sectors_id", referencedColumnName="id"))
    private Set<LookupCode> dataProvidersRelevantSectors = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_security_type",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="security_types_id", referencedColumnName="id"))
    private Set<LookupCode> dataProvidersSecurityTypes = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_provider_tag",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="provider_tags_id", referencedColumnName="id"))
    private Set<DataProviderTag> dataProviderTags = new HashSet<>();


   /* @ManyToMany(mappedBy = "tagProviders")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProviderTag> tagsProvider = new HashSet<>();
*/
    /*public Set<DataProviderTag> getTagsProvider() {
        return tagsProvider;
    }


    public DataProvider tagsProvider(Set<DataProviderTag> dataProviderTags) {
        this.tagsProvider = dataProviderTags;
        return this;
    }*/

   /* public DataProvider addTagsProvider(DataProviderTag dataProviderTag) {
        this.tagsProvider.add(dataProviderTag);
        dataProviderTag.getTagProviders().add(this);
        return this;
    }

    public DataProvider removeTagsProvider(DataProviderTag dataProviderTag) {
        this.tagsProvider.remove(dataProviderTag);
        dataProviderTag.getTagProviders().remove(this);
        return this;
    }

    public void setTagsProvider(Set<DataProviderTag> tagsProvider) {
        this.tagsProvider = tagsProvider;
    }
*/
   /* @Column(name = "pricing_model", insertable = false, updatable = false)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> pricingModels = new ArrayList<BasicBean>();*/

    public Set<DataProviderTag> getDataProviderTags() {
        return dataProviderTags;
    }

    public void setDataProviderTags(Set<DataProviderTag> dataProviderTags) {
        this.dataProviderTags = dataProviderTags;
    }
    @Column(name = "relevant_sector", insertable = false, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> relevantSectors = new ArrayList<BasicBean>();

    @JsonIgnore
    @OneToMany(mappedBy = "authorDataProvider")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataArticle> authoredArticles = new HashSet<>();

   /* public Set<DataLinks> getLink() {
        return link;
    }*/

    /*public void setLink(Set<DataLinks> link) {
        this.link = link;
    }*/
    //  @JsonIgnore
    /*@ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name="data_provider_links",
        joinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="links_id", referencedColumnName="id"))
    private Set<DataLinks> link = new HashSet<>();*/

    public Set<DocumentsMetadata> getDocumentsMetadata() {
        return documentsMetadata;
    }

    public void setDocumentsMetadata(Set<DocumentsMetadata> documentsMetadata) {
        this.documentsMetadata = documentsMetadata;
    }

    @OneToMany(mappedBy = "dataProvider",fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @Where(clause = "published = 1")
    @JsonIgnore
    private Set<DocumentsMetadata> documentsMetadata = new HashSet<>();
    
    
    @Transient
    @JsonSerialize
    private String editProviderDataPartner;
    
    @Transient
    @JsonSerialize
    private String editDistributionDataPartner;
    
    @Transient
    @JsonSerialize
    private String editConsumerDataPartner;
    
    public String getEditProviderDataPartner() {
		return editProviderDataPartner;
	}

	public String getEditDistributionDataPartner() {
		return editDistributionDataPartner;
	}

	public String getEditConsumerDataPartner() {
		return editConsumerDataPartner;
	}

	public String getEditComplementaryDataProviders() {
		return editComplementaryDataProviders;
	}

	public String getEditCompetitorProvider() {
		return editCompetitorProvider;
	}

	public void setEditProviderDataPartner(String editProviderDataPartner) {
		this.editProviderDataPartner = editProviderDataPartner;
	}

	public void setEditDistributionDataPartner(String editDistributionDataPartner) {
		this.editDistributionDataPartner = editDistributionDataPartner;
	}

	public void setEditConsumerDataPartner(String editConsumerDataPartner) {
		this.editConsumerDataPartner = editConsumerDataPartner;
	}

	public void setEditComplementaryDataProviders(String editComplementaryDataProviders) {
		this.editComplementaryDataProviders = editComplementaryDataProviders;
	}

	public void setEditCompetitorProvider(String editCompetitorProvider) {
		this.editCompetitorProvider = editCompetitorProvider;
	}
	@Transient
    @JsonSerialize
    private String editComplementaryDataProviders;
    
    @Transient
    @JsonSerialize
    private String editCompetitorProvider;
    
    public Set<DataProvider> getProviderPartners() {
        return providerPartners;
    }

    public void setProviderPartners(Set<DataProvider> providerPartners) {
        this.providerPartners = providerPartners;
    }


    public Set<User> getFollowingUsers() {
        return followingUsers;
    }

    public void setFollowingUsers(Set<User> followingUsers) {
        this.followingUsers = followingUsers;
    }

    public String getDeliveryFrequencyNotes() {
        return deliveryFrequencyNotes;
    }

    public void setDeliveryFrequencyNotes(String deliveryFrequencyNotes) {
        this.deliveryFrequencyNotes = deliveryFrequencyNotes;
    }

    @ManyToMany(mappedBy = "providerOrganizer")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataEvent> providerEvent = new HashSet<>();

    @ManyToMany(mappedBy = "sponsorProvider")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataEvent> sponsorProviderEvent = new HashSet<>();

    @ManyToMany(mappedBy = "releventProviders")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataComment> releventProviderComment = new HashSet<>();

    @ManyToMany(mappedBy = "providersFollowed")
    @JsonIgnore

    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<User> followingUsers = new HashSet<>();


    @Column(name="free_trial_available")
    private Boolean freeTrialAvailable;


    public Boolean getFreeTrialAvailable() {
        return freeTrialAvailable;
    }

    public void setFreeTrialAvailable(Boolean freeTrialAvailable) {
        this.freeTrialAvailable = freeTrialAvailable;
    }

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public DataProvider recordID(String recordID) {
        this.recordID = recordID;
        return this;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getProviderName() {
        return providerName;
    }

    public DataProvider providerName(String providerName) {
        this.providerName = providerName;
        return this;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getWebsite() {
        return website;
    }

    public DataProvider website(String website) {
        this.website = website;
        return this;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public DataProvider shortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
        return this;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public DataProvider longDescription(String longDescription) {
        this.longDescription = longDescription;
        return this;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    /* public LookupCode getMainAssetClass() {
         return mainAssetClass;
     }

     public DataProvider mainAssetClass(LookupCode mainAssetClass) {
         this.mainAssetClass = mainAssetClass;
         return this;
     }

     public void setMainAssetClass(LookupCode mainAssetClass) {
         this.mainAssetClass = mainAssetClass;
     }
 */
    public String getNotes() {
        return notes;
    }

    public DataProvider notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    /*public Integer getLive() {
        return live;
    }

    public DataProvider live(Integer live) {
        this.live = live;
        return this;
    }

    public void setLive(Integer live) {
        this.live = live;
    }*/

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public DataProvider createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public DataProvider updatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

   /* public String getDataUpdateFrequency() {
        return dataUpdateFrequency;
    }

    public DataProvider dataUpdateFrequency(String dataUpdateFrequency) {
        this.dataUpdateFrequency = dataUpdateFrequency;
        return this;
    }

    public void setDataUpdateFrequency(String dataUpdateFrequency) {
        this.dataUpdateFrequency = dataUpdateFrequency;
    }*/

    public String getDataUpdateFrequencyNotes() {
        return dataUpdateFrequencyNotes;
    }

    public DataProvider dataUpdateFrequencyNotes(String dataUpdateFrequencyNotes) {
        this.dataUpdateFrequencyNotes = dataUpdateFrequencyNotes;
        return this;
    }

    public void setDataUpdateFrequencyNotes(String dataUpdateFrequencyNotes) {
        this.dataUpdateFrequencyNotes = dataUpdateFrequencyNotes;
    }



    public String getDateCollectionRangeExplanation() {
        return dateCollectionRangeExplanation;
    }

    public DataProvider dateCollectionRangeExplanation(String dateCollectionRangeExplanation) {
        this.dateCollectionRangeExplanation = dateCollectionRangeExplanation;
        return this;
    }

    public void setDateCollectionRangeExplanation(String dateCollectionRangeExplanation) {
        this.dateCollectionRangeExplanation = dateCollectionRangeExplanation;
    }

    public String getCollectionMethodsExplanation() {
        return collectionMethodsExplanation;
    }

    public DataProvider collectionMethodsExplanation(String collectionMethodsExplanation) {
        this.collectionMethodsExplanation = collectionMethodsExplanation;
        return this;
    }

    public void setCollectionMethodsExplanation(String collectionMethodsExplanation) {
        this.collectionMethodsExplanation = collectionMethodsExplanation;
    }

    public String getPotentialDrawbacks() {
        return potentialDrawbacks;
    }

    public DataProvider potentialDrawbacks(String potentialDrawbacks) {
        this.potentialDrawbacks = potentialDrawbacks;
        return this;
    }

    public void setPotentialDrawbacks(String potentialDrawbacks) {
        this.potentialDrawbacks = potentialDrawbacks;
    }

    public String getUniqueValueProps() {
        return uniqueValueProps;
    }

    public DataProvider uniqueValueProps(String uniqueValueProps) {
        this.uniqueValueProps = uniqueValueProps;
        return this;
    }

    public void setUniqueValueProps(String uniqueValueProps) {
        this.uniqueValueProps = uniqueValueProps;
    }

    public String getHistoricalDateRange() {
        return historicalDateRange;
    }

    public DataProvider historicalDateRange(String historicalDateRange) {
        this.historicalDateRange = historicalDateRange;
        return this;
    }

    public void setHistoricalDateRange(String historicalDateRange) {
        this.historicalDateRange = historicalDateRange;
    }

    public String getKeyDataFields() {
        return keyDataFields;
    }

    public DataProvider keyDataFields(String keyDataFields) {
        this.keyDataFields = keyDataFields;
        return this;
    }

    public void setKeyDataFields(String keyDataFields) {
        this.keyDataFields = keyDataFields;
    }

    public String getSummary() {
        return summary;
    }

    public DataProvider summary(String summary) {
        this.summary = summary;
        return this;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

   /* public String getResearchMethodsCompleted() {
        return researchMethodsCompleted;
    }

    public DataProvider researchMethodsCompleted(String researchMethodsCompleted) {
        this.researchMethodsCompleted = researchMethodsCompleted;
        return this;
    }

    public void setResearchMethodsCompleted(String researchMethodsCompleted) {
        this.researchMethodsCompleted = researchMethodsCompleted;
    }*/

    public LookupCode getScoreInvestorsWillingness() {
        return scoreInvestorsWillingness;
    }

    public DataProvider investorsScoreWillingness(LookupCode investorsScoreWillingness) {
        this.scoreInvestorsWillingness = investorsScoreWillingness;
        return this;
    }

    public void setScoreInvestorsWillingness(LookupCode scoreInvestorsWillingness) {
        this.scoreInvestorsWillingness = scoreInvestorsWillingness;
    }

    public String getInvestor_clients() {
        return investor_clients;
    }

    public DataProvider investor_clients(String investor_clients) {
        this.investor_clients = investor_clients;
        return this;
    }

    public void setInvestor_clients(String investor_clients) {
        this.investor_clients = investor_clients;
    }

    /*  public String getHistoricalDateRangeEstimate() {
          return historicalDateRangeEstimate;
      }

      public DataProvider historicalDateRangeEstimate(String historicalDateRangeEstimate) {
          this.historicalDateRangeEstimate = historicalDateRangeEstimate;
          return this;
      }

      public void setHistoricalDateRangeEstimate(String historicalDateRangeEstimate) {
          this.historicalDateRangeEstimate = historicalDateRangeEstimate;
      }
  */
    public String getPublicCompaniesCovered() {
        return publicCompaniesCovered;
    }

    public DataProvider publicCompaniesCovered(String publicCompaniesCovered) {
        this.publicCompaniesCovered = publicCompaniesCovered;
        return this;
    }

    public void setPublicCompaniesCovered(String publicCompaniesCovered) {
        this.publicCompaniesCovered = publicCompaniesCovered;
    }

    public LookupCode getScoreUniqueness() {
        return scoreUniqueness;
    }

    public DataProvider uniquenessScore(LookupCode uniquenessScore) {
        this.scoreUniqueness = uniquenessScore;
        return this;
    }

    public void setScoreUniqueness(LookupCode scoreUniqueness) {
        this.scoreUniqueness = scoreUniqueness;
    }

    public LookupCode getScoreValue() {
        return scoreValue;
    }

    public DataProvider valueScore(LookupCode valueScore) {
        this.scoreValue = valueScore;
        return this;
    }

    public void setScoreValue(LookupCode scoreValue) {
        this.scoreValue = scoreValue;
    }

    //*******************New Fields Start*********************************//



    //*******************New Fields End*********************************//

    public LookupCode getScoreReadiness() {
        return scoreReadiness;
    }

    public DataProvider readinessScore(LookupCode readinessScore) {
        this.scoreReadiness = readinessScore;
        return this;
    }

    public void setScoreReadiness(LookupCode scoreReadiness) {
        this.scoreReadiness = scoreReadiness;
    }


    /*public String getFreeTrialAvailable() {
        return freeTrialAvailable;
    }

    public DataProvider freeTrialAvailable(String freeTrialAvailable) {
        this.freeTrialAvailable = freeTrialAvailable;
        return this;
    }

    public void setFreeTrialAvailable(String freeTrialAvailable) {
        this.freeTrialAvailable = freeTrialAvailable;
    }
*/
    public String getProductDetails() {
        return productDetails;
    }

    public DataProvider productDetails(String productDetails) {
        this.productDetails = productDetails;
        return this;
    }

    public void setProductDetails(String productDetails) {
        this.productDetails = productDetails;
    }

    public DataCategory getMainDataCategory() {
        return mainDataCategory;
    }

    public DataProvider mainDataCategory(DataCategory dataCategory) {
        this.mainDataCategory = dataCategory;
        return this;
    }

    public void setMainDataCategory(DataCategory dataCategory) {
        this.mainDataCategory = dataCategory;
    }

    public DataOrganization getOwnerOrganization() {
        return ownerOrganization;
    }

    public DataProvider ownerOrganization(DataOrganization dataOrganization) {
        this.ownerOrganization = dataOrganization;
        return this;
    }

    public void setOwnerOrganization(DataOrganization dataOrganization) {
        this.ownerOrganization = dataOrganization;
    }

    public DataIndustry getMainDataIndustry() {
        return mainDataIndustry;
    }

    public DataProvider mainDataIndustry(DataIndustry dataIndustry) {
        this.mainDataIndustry = dataIndustry;
        return this;
    }

    public void setMainDataIndustry(DataIndustry dataIndustry) {
        this.mainDataIndustry = dataIndustry;
    }

    public DataProviderType getProviderType() {
        return providerType;
    }

    public DataProvider providerType(DataProviderType dataProviderType) {
        this.providerType = dataProviderType;
        return this;
    }

    public void setProviderType(DataProviderType dataProviderType) {
        this.providerType = dataProviderType;
    }

    public Set<LookupCode> getGeographicalFoci() {
        return geographicalFoci;
    }

    public DataProvider geographicalFoci(Set<LookupCode> lookupCodes) {
        this.geographicalFoci = lookupCodes;
        return this;
    }

    /*public DataProvider addGeographicalFocus(LookupCode lookupCode) {
        this.geographicalFoci.add(lookupCode);
        lookupCode.getProviderGeographicalFoci().add(this);
        return this;
    }

    public DataProvider removeGeographicalFocus(LookupCode lookupCode) {
        this.geographicalFoci.remove(lookupCode);
        lookupCode.getProviderGeographicalFoci().remove(this);
        return this;
    }*/

    public void setGeographicalFoci(Set<LookupCode> lookupCodes) {
        this.geographicalFoci = lookupCodes;
    }

    public List<BasicBean> getIndustries() {
        return industries;
    }

    public DataProvider industries(Set<DataIndustry> dataIndustries) {
//        this.industries = dataIndustries;
        return this;
    }

    public DataProvider addIndustry(DataIndustry dataIndustry) {
//        this.industries.add(dataIndustry);
        dataIndustry.getAssociatedProviders().add(this);
        return this;
    }

    public DataProvider removeIndustry(DataIndustry dataIndustry) {
        this.industries.remove(dataIndustry);
        dataIndustry.getAssociatedProviders().remove(this);
        return this;
    }

    public void setIndustries(List dataIndustries) {
        this.industries = dataIndustries;
    }

    public List<BasicBean> getCategories() {
        return categories;
    }

    public DataProvider categories(List dataCategories) {
        this.categories = dataCategories;
        return this;
    }

    public DataProvider addCategory(DataCategory dataCategory) {
//        this.categories.add(dataCategory);
        dataCategory.getCategoryProviders().add(this);
        return this;
    }

    public DataProvider removeCategory(DataCategory dataCategory) {
        this.categories.remove(dataCategory);
        dataCategory.getCategoryProviders().remove(this);
        return this;
    }

    public void setCategories(List dataCategories) {
        this.categories = dataCategories;
    }

    public List<BasicBean> getFeatures() {
        return features;
    }

    public DataProvider features(List<BasicBean> dataFeatures) {
        this.features = dataFeatures;
        return this;
    }

    public DataProvider addFeature(BasicBean dataFeature) {
        this.features.add(dataFeature);
        //dataFeature.getFeatureProviders().add(this);
        return this;
    }

    public DataProvider removeFeature(String dataFeature) {
        this.features.remove(dataFeature);
        //dataFeature.getFeatureProviders().remove(this);
        return this;
    }

    public void setFeatures(List<BasicBean> dataFeatures) {
        this.features = dataFeatures;
    }

    public Set<DataArticle> getRelevantArticles() {
        return relevantArticles;
    }

    public DataProvider articles(Set<DataArticle> dataArticles) {
        this.relevantArticles = dataArticles;
        return this;
    }

    public DataProvider addArticles(DataArticle dataArticle) {
        this.relevantArticles.add(dataArticle);
        dataArticle.getRelevantProviders().add(this);
        return this;
    }

    public DataProvider removeArticles(DataArticle dataArticle) {
        this.relevantArticles.remove(dataArticle);
        dataArticle.getRelevantProviders().remove(this);
        return this;
    }

    public void setRelevantArticles(Set<DataArticle> dataArticles) {
        this.relevantArticles = dataArticles;
    }

    public List<BasicBean> getProviderTags() {
        return providerTags;
    }

    public DataProvider providerTags(List dataProviderTags) {
        this.providerTags = dataProviderTags;
        return this;
    }

    public DataProvider addProviderTag(BasicBean dataProviderTag) {
        this.providerTags.add(dataProviderTag);
//        dataProviderTag.getProviderTags().add(this);
        return this;
    }

    public DataProvider removeProviderTag(String dataProviderTag) {
        this.providerTags.remove(dataProviderTag);
//        dataProviderTag.getProviderTags().remove(this);
        return this;
    }

    public void setProviderTags(List dataProviderTags) {
        this.providerTags = dataProviderTags;
    }

    public List<BasicBean> getSources() {
        return sources;
    }

    public DataProvider sources(List<BasicBean> dataSources) {
        this.sources = dataSources;
        return this;
    }

    public DataProvider addSource(BasicBean dataSource) {
        this.sources.add(dataSource);
//        dataSource.getProviderSources().add(this);
        return this;
    }

    public DataProvider removeSource(DataSource dataSource) {
        this.sources.remove(dataSource);
        dataSource.getProviderSources().remove(this);
        return this;
    }

    public void setSources(List<BasicBean> dataSources) {
        this.sources = dataSources;
    }

    public List<BasicBean> getAssetClasses() {
        return assetClasses;
    }

    public DataProvider assetClasses(List lookupCodes) {
        this.assetClasses = lookupCodes;
        return this;
    }

    public DataProvider addAssetClass(String lookupCode) {
        this.assetClasses.add(lookupCode);
//        lookupCode.getProviderAssetClasses().add(this);
        return this;
    }

    public DataProvider removeAssetClass(String lookupCode) {
        this.assetClasses.remove(lookupCode);
//        lookupCode.getProviderAssetClasses().remove(this);
        return this;
    }

    public void setAssetClasses(List lookupCodes) {
        this.assetClasses = lookupCodes;
    }

    public Set<LookupCode> getInvestorTypes() {
        return investorTypes;
    }

    public DataProvider investorTypes(Set<LookupCode> lookupCodes) {
        this.investorTypes = lookupCodes;
        return this;
    }
  
    public void setInvestorTypes(Set<LookupCode> lookupCodes) {
        this.investorTypes = lookupCodes;
    }

    /*******************************ProviderIndustries start*********************************/
    public Set<DataIndustry> getProviderIndustries() {
        return providerIndustries;
    }
    public DataProvider providerIndustries(Set<DataIndustry> dataIndustries) {
        this.providerIndustries = dataIndustries;
        return this;
    }
    public DataProvider addProviderIndustry(DataIndustry dataIndustry) {
        this.providerIndustries.add(dataIndustry);
        dataIndustry.getProviderIndustries().add(this);
        return this;
    }
    public DataProvider removeProviderIndustry(DataIndustry dataIndustry) {
        this.providerIndustries.remove(dataIndustry);
        dataIndustry.getProviderIndustries().remove(this);
        return this;
    }
    public void setProviderIndustries(Set<DataIndustry> dataIndustries) {
        this.providerIndustries = dataIndustries;
    }

    /*******************************DataProviderCategories start*********************************/
    public Set<DataCategory> getDataProviderCategories() {
        return dataProviderCategories;
    }
    public DataProvider dataProviderCategories(Set<DataCategory> dataCategories) {
        this.dataProviderCategories = dataCategories;
        return this;
    }
    public DataProvider addDataProviderCategory(DataCategory dataCategory) {
        this.dataProviderCategories.add(dataCategory);
        dataCategory.getProviderCategories().add(this);
        return this;
    }
    public DataProvider removeDataProviderCategory(DataCategory dataCategory) {
        this.dataProviderCategories.remove(dataCategory);
        dataCategory.getProviderCategories().remove(this);
        return this;
    }
    public void setDataProviderCategories(Set<DataCategory> dataCategories) {
        this.dataProviderCategories = dataCategories;
    }
    /*******************************DataProviderTags start*********************************/
    /*public Set<DataProviderTag> getDataProviderTags() {
        return dataProviderTags;
    }
    public DataProvider dataProviderTags(Set<DataProviderTag> dataProviderTags) {
        this.dataProviderTags = dataProviderTags;
        return this;
    }
    public DataProvider addDataProviderTag(DataProviderTag dataProviderTag) {
        this.dataProviderTags.add(dataProviderTag);
        dataProviderTag.getDataTags().add(this);
        return this;
    }
    public DataProvider removeDataProviderTag(DataProviderTag dataProviderTag) {
        this.dataProviderTags.remove(dataProviderTag);
        dataProviderTag.getDataTags().remove(this);
        return this;
    }
    public void setDataProviderTags(Set<DataProviderTag> dataProviderTags) {
        this.dataProviderTags = dataProviderTags;
    }*/
    /*******************************DataProviderFeatures start*********************************/
    public Set<DataFeature> getDataProviderFeatures() {
        return dataProviderFeatures;
    }
    public DataProvider dataProviderFeatures(Set<DataFeature> dataFeatures) {
        this.dataProviderFeatures = dataFeatures;
        return this;
    }
    public DataProvider addDataProviderFeature(DataFeature dataFeature) {
        this.dataProviderFeatures.add(dataFeature);
        dataFeature.getProviderFeatures().add(this);
        return this;
    }
    public DataProvider removeDataProviderFeature(DataFeature dataFeature) {
        this.dataProviderFeatures.remove(dataFeature);
        dataFeature.getProviderFeatures().remove(this);
        return this;
    }
    public void setDataProviderFeatures(Set<DataFeature> dataFeatures) {
        this.dataProviderFeatures = dataFeatures;
    }

    /*******************************DataProviderSources start*********************************/
    public Set<DataSource> getDataProviderSources() {
        return dataProviderSources;
    }
    public DataProvider dataProviderSources(Set<DataSource> dataSources) {
        this.dataProviderSources = dataSources;
        return this;
    }
    public DataProvider addDataProviderSource(DataSource dataSource) {
        this.dataProviderSources.add(dataSource);
        dataSource.getDataSources().add(this);
        return this;
    }
    public DataProvider removeDataProviderSource(DataSource dataSource) {
        this.dataProviderSources.remove(dataSource);
        dataSource.getDataSources().remove(this);
        return this;
    }
    public void setDataProviderSources(Set<DataSource> dataSources) {
        this.dataProviderSources = dataSources;
    }

    /*******************************DataProvidersDeliveryFormats start*********************************/

    public Set<LookupCode> getDataProvidersDeliveryFormats() {
        return dataProvidersDeliveryFormats;
    }
    public DataProvider dataProvidersDeliveryFormats(Set<LookupCode> lookupCodes) {
        this.dataProvidersDeliveryFormats = lookupCodes;
        return this;
    }
    /* public DataProvider addDataProvidersDeliveryFormat(LookupCode lookupCode) {
         this.dataProvidersDeliveryFormats.add(lookupCode);
         lookupCode.getProviderDeliveryFormats().add(this);
         return this;
     }
     public DataProvider removeDataProvidersDeliveryFormat(LookupCode lookupCode) {
         this.dataProvidersDeliveryFormats.remove(lookupCode);
         lookupCode.getProviderDeliveryFormats().remove(this);
         return this;
     }*/
    public void setDataProvidersDeliveryFormats(Set<LookupCode> lookupCodes) {
        this.dataProvidersDeliveryFormats = lookupCodes;
    }

    /*******************************dataProvidersDeliveryMethods start*********************************/

    public Set<LookupCode> getDataProvidersDeliveryMethods() {
        return dataProvidersDeliveryMethods;
    }
    public DataProvider dataProvidersDeliveryMethods(Set<LookupCode> lookupCodes) {
        this.dataProvidersDeliveryMethods = lookupCodes;
        return this;
    }
    /* public DataProvider addDataProvidersDeliveryMethod(LookupCode lookupCode) {
         this.dataProvidersDeliveryMethods.add(lookupCode);
         lookupCode.getDataProviderDeliveryMethods().add(this);
         return this;
     }
     public DataProvider removeDataProvidersDeliveryMethods(LookupCode lookupCode) {
         this.dataProvidersDeliveryMethods.remove(lookupCode);
         lookupCode.getDataProviderDeliveryMethods().remove(this);
         return this;
     }*/
    public void setDataProvidersDeliveryMethods(Set<LookupCode> lookupCodes) {
        this.dataProvidersDeliveryMethods = lookupCodes;
    }

    /*******************************researchmethod start*********************************/

    public Set<LookupCode> getResearchMethodsCompleted() {
        return researchMethodsCompleted;
    }

    public void setResearchMethodsCompleted(Set<LookupCode> researchMethodsCompleted) {
        this.researchMethodsCompleted = researchMethodsCompleted;
    }
    public DataProvider dataProvidersResearchMethods(Set<LookupCode> lookupCodes) {
        this.researchMethodsCompleted = lookupCodes;
        return this;
    }
	 /*   public DataProvider adddataProvidersResearchMethods(LookupCode lookupCode) {
	        this.researchMethodsCompleted.add(lookupCode);
	        lookupCode.getResearchMethodsCode().add(this);
	        return this;
	    }
	    public DataProvider removedataProvidersResearchMethods(LookupCode lookupCode) {
	        this.researchMethodsCompleted.remove(lookupCode);
	        lookupCode.getResearchMethodsCode().remove(this);
	        return this;
	    }

    */
    /*******************************dataProvidersAssetClasses Start*********************************/

    public Set<LookupCode> getDataProvidersAssetClasses() {
        return dataProvidersAssetClasses;
    }


    public DataProvider dataProvidersAssetClasses(Set<LookupCode> lookupCodes) {
        this.dataProvidersAssetClasses = lookupCodes;
        return this;
    }
    /* public DataProvider addDataProvidersAssetClasses(LookupCode lookupCode) {
         this.dataProvidersAssetClasses.add(lookupCode);
         lookupCode.getDataProviderDeliveryMethods().add(this);
         return this;
     }
     public DataProvider removeDataProvidersAssetClasses(LookupCode lookupCode) {
         this.dataProvidersAssetClasses.remove(lookupCode);
         lookupCode.getDataProviderDeliveryMethods().remove(this);
         return this;
     }*/
    public void setDataProvidersAssetClasses(Set<LookupCode> lookupCodes) {
        this.dataProvidersAssetClasses = lookupCodes;
    }


    /*******************************DataProvidersRelevantSectors Start*********************************/

    public Set<LookupCode> getDataProvidersRelevantSectors() {
        return dataProvidersRelevantSectors;
    }
    public DataProvider dataProvidersRelevantSectors(Set<LookupCode> lookupCodes) {
        this.dataProvidersRelevantSectors = lookupCodes;
        return this;
    }
    /* public DataProvider addDataProvidersRelevantSector(LookupCode lookupCode) {
         this.dataProvidersRelevantSectors.add(lookupCode);
         lookupCode.getProvidersRelevantSectors().add(this);
         return this;
     }
     public DataProvider removeDataProvidersRelevantSector(LookupCode lookupCode) {
         this.dataProvidersRelevantSectors.remove(lookupCode);
         lookupCode.getProvidersRelevantSectors().remove(this);
         return this;
     }*/
    public void setDataProvidersRelevantSectors(Set<LookupCode> lookupCodes) {
        this.dataProvidersRelevantSectors = lookupCodes;
    }

    /*******************************DataProvidersSecurityTypes Start*********************************/

    public Set<LookupCode> getDataProvidersSecurityTypes() {
        return dataProvidersSecurityTypes;
    }
    public DataProvider dataProvidersSecurityTypes(Set<LookupCode> lookupCodes) {
        this.dataProvidersSecurityTypes = lookupCodes;
        return this;
    }
    /* public DataProvider addDataProvidersSecurityType(LookupCode lookupCode) {
         this.dataProvidersSecurityTypes.add(lookupCode);
         lookupCode.getDataSecurityTypes().add(this);
         return this;
     }
     public DataProvider removeDataProvidersSecurityType(LookupCode lookupCode) {
         this.dataProvidersSecurityTypes.remove(lookupCode);
         lookupCode.getDataSecurityTypes().remove(this);
         return this;
     }*/
    public void setDataProvidersSecurityTypes(Set<LookupCode> lookupCodes) {
        this.dataProvidersSecurityTypes = lookupCodes;
    }

    /*******************************dataProvidersPricingModels*********************************/

  /*  public Set<LookupCode> getDataProvidersPricingModels() {
        return dataProvidersPricingModels;
    }
    public DataProvider dataProvidersPricingModels(Set<LookupCode> lookupCodes) {
        this.dataProvidersPricingModels = lookupCodes;
        return this;
    }*/
    /*public DataProvider addDataProvidersPricingModel(LookupCode lookupCode) {
        this.dataProvidersPricingModels.add(lookupCode);
        lookupCode.getDataProviderDeliveryMethods().add(this);
        return this;
    }
    public DataProvider removeDataProvidersPricingModel(LookupCode lookupCode) {
        this.dataProvidersPricingModels.remove(lookupCode);
        lookupCode.getDataProviderDeliveryMethods().remove(this);
        return this;
    }*/
  /*  public void setDataProvidersPricingModels(Set<LookupCode> lookupCodes) {
        this.dataProvidersPricingModels = lookupCodes;
    }*/

    ////////////////


    public List<BasicBean> getSecurityTypes() {
        return securityTypes;
    }

    public DataProvider securityTypes(List lookupCodes) {
        this.securityTypes = lookupCodes;
        return this;
    }

    public DataProvider addSecurityType(BasicBean lookupCode) {
        this.securityTypes.add(lookupCode);
//        lookupCode.getProviderSecurityTypes().add(this);
        return this;
    }

   /* public DataProvider removeSecurityType(LookupCode lookupCode) {
        this.securityTypes.remove(lookupCode);
        lookupCode.getProviderSecurityTypes().remove(this);
        return this;
    }*/

    public void setSecurityTypes(List lookupCodes) {
        this.securityTypes = lookupCodes;
    }

    public List<BasicBean> getDeliveryMethods() {
        return deliveryMethods;
    }

    public DataProvider deliveryMethods(List lookupCodes) {
        this.deliveryMethods = lookupCodes;
        return this;
    }

    public DataProvider addDeliveryMethod(BasicBean lookupCode) {
        this.deliveryMethods.add(lookupCode);
//        lookupCode.getProviderDeliveryMethods().add(this);
        return this;
    }

    /* public DataProvider removeDeliveryMethod(LookupCode lookupCode) {
         this.deliveryMethods.remove(lookupCode);
         lookupCode.getProviderDeliveryMethods().remove(this);
         return this;
     }
 */
    public void setDeliveryMethods(List lookupCodes) {
        this.deliveryMethods = lookupCodes;
    }

    public List<BasicBean> getDeliveryFormats() {
        return deliveryFormats;
    }

    public DataProvider deliveryFormats(List lookupCodes) {
        this.deliveryFormats = lookupCodes;
        return this;
    }

    public DataProvider addDeliveryFormat(BasicBean lookupCode) {
        this.deliveryFormats.add(lookupCode);
//        lookupCode.getProviderDeliveryFormats().add(this);
        return this;
    }

    /* public DataProvider removeDeliveryFormat(LookupCode lookupCode) {
         this.deliveryFormats.remove(lookupCode);
         lookupCode.getProviderDeliveryFormats().remove(this);
         return this;
     }
 */
    public void setDeliveryFormats(List lookupCodes) {
        this.deliveryFormats = lookupCodes;
    }

    public Set<LookupCode> getDeliveryFrequencies() {
        return deliveryFrequencies;
    }

    public DataProvider deliveryFrequencies(Set<LookupCode> lookupCodes) {
        this.deliveryFrequencies = lookupCodes;
        return this;
    }

    /*public DataProvider addDeliveryFrequency(LookupCode lookupCode) {
        this.deliveryFrequencies.add(lookupCode);
        lookupCode.getProviderDeliveryFrequencies().add(this);
        return this;
    }

    public DataProvider removeDeliveryFrequency(LookupCode lookupCode) {
        this.deliveryFrequencies.remove(lookupCode);
        lookupCode.getProviderDeliveryFrequencies().remove(this);
        return this;
    }*/

    public void setDeliveryFrequencies(Set<LookupCode> lookupCodes) {
        this.deliveryFrequencies = lookupCodes;
    }
/*
    public List<BasicBean> getPricingModels() {
        return pricingModels;
    }

    public DataProvider pricingModels(List lookupCodes) {
        this.pricingModels = lookupCodes;
        return this;
    }

    public DataProvider addPricingModel(BasicBean lookupCode) {
        this.pricingModels.add(lookupCode);
//        lookupCode.getProviderPricingModels().add(this);
        return this;
    }

    public DataProvider removePricingModel(String lookupCode) {
        this.pricingModels.remove(lookupCode);
//        lookupCode.getProviderPricingModels().remove(this);
        return this;
    }*/

    public Set<DataComment> getReleventProviderComment() {
        return releventProviderComment;
    }

    public DataProvider releventProviderComment(Set<DataComment> dataComments) {
        this.releventProviderComment = dataComments;
        return this;
    }

    public DataProvider addReleventProviderComment(DataComment dataComment) {
        this.releventProviderComment.add(dataComment);
        dataComment.getReleventProviders().add(this);
        return this;
    }

    public DataProvider removeReleventProviderComment(DataComment dataComment) {
        this.releventProviderComment.remove(dataComment);
        dataComment.getReleventProviders().remove(this);
        return this;
    }

    public void setReleventProviderComment(Set<DataComment> releventProviderComment) {
        this.releventProviderComment = releventProviderComment;
    }


    public Set<DataEvent> getSponsorProviderEvent() {
        return sponsorProviderEvent;
    }

    public DataProvider sponsorProviderEvent(Set<DataEvent> dataEvents) {
        this.sponsorProviderEvent = dataEvents;
        return this;
    }

    public DataProvider addSponsorProviderEvent(DataEvent dataEvent) {
        this.sponsorProviderEvent.add(dataEvent);
        dataEvent.getProviderOrganizer().add(this);
        return this;
    }

    public DataProvider removeSponsorProviderEvent(DataEvent dataEvent) {
        this.sponsorProviderEvent.remove(dataEvent);
        dataEvent.getProviderOrganizer().remove(this);
        return this;
    }

    public void setSponsorProviderEvent(Set<DataEvent> sponsorProviderEvent) {
        this.sponsorProviderEvent = sponsorProviderEvent;
    }



    public Set<DataEvent> getProviderEvent() {
        return providerEvent;
    }

    public DataProvider providerEvent(Set<DataEvent> dataEvents) {
        this.providerEvent = dataEvents;
        return this;
    }

    public DataProvider addProviderEvent(DataEvent dataEvent) {
        this.providerEvent.add(dataEvent);
        dataEvent.getProviderOrganizer().add(this);
        return this;
    }

    public DataProvider removeProviderEvent(DataEvent dataEvent) {
        this.providerEvent.remove(dataEvent);
        dataEvent.getProviderOrganizer().remove(this);
        return this;
    }

    public void setProviderEvent(Set<DataEvent> providerEvent) {
        this.providerEvent = providerEvent;
    }


   /* public void setPricingModels(List lookupCodes) {
        this.pricingModels = lookupCodes;
    }*/

    public List getRelevantSectors() {
        return relevantSectors;
    }

    public DataProvider relevantSectors(List lookupCodes) {
        this.relevantSectors = lookupCodes;
        return this;
    }

    public DataProvider addRelevantSector(BasicBean lookupCode) {
        this.relevantSectors.add(lookupCode);
//        lookupCode.getProviderSectors().add(this);
        return this;
    }

    public DataProvider removeRelevantSector(String lookupCode) {
        this.relevantSectors.remove(lookupCode);
//        lookupCode.getProviderSectors().remove(this);
        return this;
    }

    public void setRelevantSectors(List lookupCodes) {
        this.relevantSectors = lookupCodes;
    }

  /*  public Date getDateCollectionBegan() {
        return dateCollectionBegan;
    }

    public void setDateCollectionBegan(Date dateCollectionBegan) {
        this.dateCollectionBegan = dateCollectionBegan;
    }*/

    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataProvider dataProvider = (DataProvider) o;
        if (dataProvider.getRecordID() == null || getRecordID() == null) {
            return false;
        }
        return Objects.equals(getRecordID(), dataProvider.getRecordID());
    }

    public LocalDate getDateCollectionBegan() {
        return dateCollectionBegan;
    }

    public void setDateCollectionBegan(LocalDate dateCollectionBegan) {
        this.dateCollectionBegan = dateCollectionBegan;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getRecordID());
    }

    @Override
    public String toString() {
        return "DataProvider{" +
            "id=" + getId() +
            ", recordID='" + getRecordID() + "'" +
            ", providerName='" + getProviderName() + "'" +
            ", website='" + getWebsite() + "'" +
            ", shortDescription='" + getShortDescription() + "'" +
            ", longDescription='" + getLongDescription() + "'" +
            ", mainAssetClass='" + getMainAssetClass() + "'" +
            ", notes='" + getNotes() + "'" +
            //  ", live='" + getLive() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", dataUpdateFrequency='" + getDataUpdateFrequency() + "'" +
            ", dataUpdateFrequencyNotes='" + getDataUpdateFrequencyNotes() + "'" +
            ", dateCollectionBegan='" + getDateCollectionBegan() + "'" +
            ", dateCollectionRangeExplanation='" + getDateCollectionRangeExplanation() + "'" +
            ", collectionMethodsExplanation='" + getCollectionMethodsExplanation() + "'" +
            ", potentialDrawbacks='" + getPotentialDrawbacks() + "'" +
            ", uniqueValueProps='" + getUniqueValueProps() + "'" +
            ", historicalDateRange='" + getHistoricalDateRange() + "'" +
            ", keyDataFields='" + getKeyDataFields() + "'" +
            ", summary='" + getSummary() + "'" +
            //     ", researchMethodsCompleted='" + getResearchMethodsCompleted() + "'" +
            ", scoreInvestorsWillingness='" + getScoreInvestorsWillingness() + "'" +
            ", investor_clients='" + getInvestor_clients() + "'" +
            // ", historicalDateRangeEstimate='" + getHistoricalDateRangeEstimate() + "'" +
            ", publicCompaniesCovered='" + getPublicCompaniesCovered() + "'" +
            ", scoreUniqueness='" + getScoreUniqueness() + "'" +
            ", scoreValue='" + getScoreValue() + "'" +
            ", scoreReadiness='" + getScoreReadiness() + "'" +
            ", marketplaceStatus='" + getMarketplaceStatus() + "'" +
            ", freeTrialAvailable='" + getFreeTrialAvailable() + "'" +
            ", productDetails='" + getProductDetails() + "'" +
            ", webCreatedDate='" + getWebCreatedDate() + "'" +
            ", webUpdateDate='" + getWebUpdateDate() + "'" +
            ", creatorUserId='" + getCreatorUserId() + "'" +
            ", editorUserId='" + getEditorUserId() + "'" +
            ", deliveryFrequencyNotes='" + getDeliveryFrequencyNotes() + "'" +
            ", consumerPartners='" + getConsumerPartners() + "'" +
            ", distributionPartners='" + getDistributionPartners() + "'" +
            ", providerPartners='" + getProviderPartners() + "'" +
            ", providerIndustries='" + getProviderIndustries() + "'" +
            ", dataProvidersDeliveryFormats='" + getDataProvidersDeliveryFormats() + "'" +
            ", dataProvidersDeliveryMethods='" + getDataProvidersDeliveryMethods() + "'" +
            //   ", dataProvidersPricingModels='" + getDataProvidersPricingModels() + "'" +
            ", dataProvidersAssetClasses='" + getDataProvidersAssetClasses() + "'" +
            ", dataProviderFeatures='" + getDataProviderFeatures() + "'" +
            ", dataProvidersRelevantSectors='" + getDataProvidersRelevantSectors() + "'" +
            ", dataProvidersSecurityTypes='" + getDataProvidersSecurityTypes() + "'" +
            ", dataProviderSources='" + getDataProviderSources() + "'" +
            // ", tagsProvider='" + getTagsProvider() + "'" +
            ", dataProviderTags='" + getDataProviderTags() + "'" +
            ", deliveryFrequencies='" + getDeliveryFrequencies() + "'" +
            ", secRegulated='" + getSecRegulated() + "'"+
            ", profileVerificationStatus='" + getProfileVerificationStatus() + "'" +
            ", publicEquitiesCoveredCount='" + getPublicEquitiesCoveredCount() + "'" +
            ", publicEquitiesCoveredRange='" + getPublicEquitiesCoveredRange() + "'" +
            ", trialPricingModel='" + getTrialPricingModel() + "'" +
            ", trialDuration='" + getTrialDuration() + "'" +
            ", trialAgreementType='" + getTrialAgreementType() + "'" +
            ", trialPricingDetails='" + getTrialPricingDetails() + "'" +
            ", lagTime='" + getLagTime() + "'" +
            ", competitiveDifferentiators='" + getCompetitiveDifferentiators() + "'" +
            ", discussionAnalytics='" + getDiscussionAnalytics() + "'" +
            ", discussionCleanliness='" + getDiscussionCleanliness() + "'"+
            ", sampleOrPanelBiases='" + getSampleOrPanelBiases() + "'" +
            ", datasetSize='" + getDatasetSize() + "'" +
            ", datasetNumberOfRows='" + getDatasetNumberOfRows() + "'" +
            ", numberOfAnalystEmployees='" + getNumberOfAnalystEmployees() + "'" +
            ", pointInTimeAccuracy='" + getPointInTimeAccuracy() + "'" +
            ", dataSourcesDetails='" + getDataSourcesDetails() + "'" +
            ", dataGaps='" + getDataGaps() + "'"+
            ", includesOutliers='" + getIncludesOutliers() + "'" +
            ", outlierReasons='" + getOutlierReasons() + "'" +
            ", normalized='" + getNormalized() + "'" +
            ", duplicatesCleaned='" + getDuplicatesCleaned() + "'" +
            ", investorClientBucket='" + getInvestorClientBucket() + "'"+
            ", dataGapsReasons='" + getDataGapsReasons() + "'" +
            ", biases='" + getBiases() + "'" +
            ", pricingDetails='" + getPricingDetails() + "'" +
            ", subscriptionTimePeriod='" + getSubscriptionTimePeriod() + "'"+
            ", yearFirstDataProductLaunched='" + getYearFirstDataProductLaunched() + "'" +
            //", complianceScorePersonal='" + getComplianceScorePersonal() + "'"+
            // ", complianceScorePublic='" + getComplianceScorePublic() + "'" +
            ", monthlyPriceRange='" + getMonthlyPriceRange() + "'" +
            ", subscriptionModel='" + getSubscriptionModel() + "'" +
            ", minimumYearlyPrice='" + getMinimumYearlyPrice() + "'"+
            ", maximumYearlyPrice='" + getMaximumYearlyPrice() + "'" +
            ", numberOfDataSources='" + getNumberOfDataSources() + "'" +
            ", privacy='" + getPrivacy() + "'"+
            ", equitiesStyle='" + getEquitiesStyle() + "'"+
            ", investorClientsCount='" + getInvestorClientsCount() + "'"+
            ", dataTypes='" + getDataTypes() + "'"+
            ", dataProductTypes='" + getDataProductTypes() + "'" +
            ", accessOffered='" + getAccessOffered() + "'" +
            ", investingTimeFrame='" + getInvestingTimeFrame() + "'" +
            ", equitiesMarketCap='" + getEquitiesMarketCap() + "'"+
            ", strategies='" + getStrategies() + "'" +
            ", researchStyles='" + getResearchStyles() + "'" +
            ", managerType='" + getManagerType() + "'"+
            ", organizationType='" + getOrganizationType() + "'"+
            ", languagesAvailable='" + getLanguagesAvailable() + "'"+
            ", paymentMethodsOffered='" + getPaymentMethodsOffered() + "'"+
            ", identifiersAvailable='" + getidentifiersAvailable() + "'"+
            ", competitors='" + getCompetitors() + "'"+
            ", researchMethodsCompleted='" + getResearchMethodsCompleted() + "'" +
            ", priority='"+getPriority()+"'"+
            ", ieiStatus='"+getIeiStatus()+"'"+
            //", plannedResearchDate='"+getPlannedResearchDate()+"'"+
            ", inactive='" + isInactive() + "'"+
            //", plannedResearchDate='"+getPlannedResearchDate()+"'"+
            ", dataLicenseType='"+getDataLicenseType()+"'"+
            ", complementaryProviders='" + getComplementaryProviders() + "'"+
            ", partnersCount='" + getPartnersCount() + "'"+
            ", providerTagCount='" + getProviderTagCount() + "'"+
            ", commentsCount='" + getCommentsCount() + "'"+
            ", dataProvidersPricingModels='" + getdataProvidersPricingModels() + "'" +
            ", tagAdded='" + getTagAdded() + "'" +
            ", pricingModels='" + getPricingModels()+ "'" +
            ", industries='" + getIndustries() + "'" +

            "}";
    }


}
