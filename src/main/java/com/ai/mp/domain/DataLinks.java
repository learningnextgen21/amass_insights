package com.ai.mp.domain;

import com.ai.mp.utils.BasicBean;
import com.ai.mp.utils.DateToStringTypeAdapter;
import com.ai.mp.utils.SetToBeanConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.*;

/**
 * A DataFeature.
 */
@Entity
@Table(name = "data_links")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "datalinks")
public class DataLinks implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "record_id", length = 50)
    private String recordID;

    @NotNull
    @Size(max = 100)
    @Column(name = "link", length = 100, nullable = false)
    private String link;


    @Size(max = 100)
    @Column(name = "link_title", length = 100, nullable = false)
    private String linkTitle;

    @Column(name="resource_name_formula")
    private String resourceNameFormula;

    public String getResourceNameFormula() {
        return resourceNameFormula;
    }

    public void setResourceNameFormula(String resourceNameFormula) {
        this.resourceNameFormula = resourceNameFormula;
    }

    @JsonIgnore
    @Column(name = "last_updated_date")
    private Instant lastUpdatedDate;


    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "created_time")
    private LocalDate createdTime;

    public Instant getWebCreatedDate() {
        return webCreatedDate;
    }

    public void setWebCreatedDate(Instant webCreatedDate) {
        this.webCreatedDate = webCreatedDate;
    }

    public Instant getWebUpdateDate() {
        return webUpdateDate;
    }

    public void setWebUpdateDate(Instant webUpdateDate) {
        this.webUpdateDate = webUpdateDate;
    }

    public Long getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(Long creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public Long getEditorUserId() {
        return editorUserId;
    }

    public void setEditorUserId(Long editorUserId) {
        this.editorUserId = editorUserId;
    }

    @CreatedDate
    @Column(name = "web_created_date")
    @JsonIgnore
    private Instant webCreatedDate = Instant.now();

    @LastModifiedDate
    @Column(name = "web_updated_date")
    @JsonIgnore
    private Instant webUpdateDate = Instant.now();

    //  @NotNull
    @Column(name = "creator_user_id")
    private Long creatorUserId;

    // @NotNull
    @Column(name = "editor_user_id")
    private Long editorUserId;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }


    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "published_date")
    private LocalDate publisedDate;


    @Column(name = "link_category", insertable = false, updatable = false)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> linkCategory = new ArrayList<>();

    @ManyToMany(fetch=FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name="data_link_category",joinColumns=@JoinColumn(name="link_id",referencedColumnName="id"),
        inverseJoinColumns=@JoinColumn(name="link_category_id",referencedColumnName="id"))
    private Set<LookupCode> dataLinksCategory;

    public Set<LookupCode> getDataLinksCategory() {
        return dataLinksCategory;
    }

    public DataLinks dataLinksCategory(Set<LookupCode> lookupCodes) {
        this.dataLinksCategory = lookupCodes;
        return this;
    }


    public void setDataLinksCategory(Set<LookupCode> lookupCodes) {
        this.dataLinksCategory = lookupCodes;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getLinkTitle() {
        return linkTitle;
    }

    public List<BasicBean> getLinkCategory() {
        return linkCategory;
    }

    public void setLinkCategory(List<BasicBean> linkCategory) {
        this.linkCategory = linkCategory;
    }

    public void setLinkTitle(String linkTitle) {
        this.linkTitle = linkTitle;
    }


    public Instant getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Instant lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public LocalDate getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDate createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDate getPublisedDate() {
        return publisedDate;
    }

    public void setPublisedDate(LocalDate publisedDate) {
        this.publisedDate = publisedDate;
    }



    /*@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataLinks dataLinks = (DataLinks) o;
        return Objects.equals(id, dataLinks.id) &&
            Objects.equals(recordID, dataLinks.recordID) &&
            Objects.equals(link, dataLinks.link) &&
            Objects.equals(linkTitle, dataLinks.linkTitle) &&
            Objects.equals(linkCategory, dataLinks.linkCategory) &&
            Objects.equals(lastUpdatedDate, dataLinks.lastUpdatedDate) &&
            Objects.equals(createdTime, dataLinks.createdTime) &&
            Objects.equals(resourceNameFormula, dataLinks.resourceNameFormula) &&
            Objects.equals(publisedDate, dataLinks.publisedDate)&&
            Objects.equals(dataLinksCategory,dataLinks.dataLinksCategory);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, recordID, link, linkTitle, linkCategory,   lastUpdatedDate, createdTime, resourceNameFormula,publisedDate,dataLinksCategory);
    }
*/

    @Override
    public String toString() {
        return "DataLinks{" +
            "id=" + id +
            ", recordID='" + recordID + '\'' +
            ", link='" + link + '\'' +
            ", linkTitle='" + linkTitle + '\'' +
            ", resourceNameFormula='" + resourceNameFormula + '\'' +
            ", lastUpdatedDate=" + lastUpdatedDate +
            ", createdTime=" + createdTime +
            ", webCreatedDate=" + webCreatedDate +
            ", webUpdateDate=" + webUpdateDate +
            ", creatorUserId=" + creatorUserId +
            ", editorUserId=" + editorUserId +
            ", publisedDate=" + publisedDate +
            ", linkCategory=" + linkCategory +
            ", dataLinksCategory=" + dataLinksCategory +
            '}';
    }
}
