package com.ai.mp.domain;

import com.ai.mp.utils.ByteArrayToBase64TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "data_provider_logo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProviderLogo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public ProviderLogo(byte[] logo, String providerRecordID) {
        this.logo = logo;
        this.providerRecordID = providerRecordID;
    }

    public ProviderLogo() {
    }

    @JsonAdapter(ByteArrayToBase64TypeAdapter.class)
    @Lob
    @Column(name = "logo")
    private byte[] logo;

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    private boolean disabled = false;

    public String getProviderRecordID() {
        return providerRecordID;
    }

    public void setProviderRecordID(String providerRecordID) {
        this.providerRecordID = providerRecordID;
    }

    //@Id
    @Column(name = "provider_record_id")
    private String providerRecordID;
    public byte[] getLogo() {
        return logo;
    }
    public void setLogo(byte[] logo) {
        this.logo = logo;
    }
}
