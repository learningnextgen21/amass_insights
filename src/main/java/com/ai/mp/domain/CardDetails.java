package com.ai.mp.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "card_details")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "carddetails")
public class CardDetails implements Serializable {
	private static final long serialVersionUID = 1L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

    @Column(name = "card_id")
	private String cardId;
	
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Instant getWebCreatedDate() {
		return webCreatedDate;
	}

	public void setWebCreatedDate(Instant webCreatedDate) {
		this.webCreatedDate = webCreatedDate;
	}


	@Column(name = "customer_id")
   private String customerId;
	
    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	@ManyToOne
   private Payment payment;
    
    @ManyToOne
    private User user;
   
    @CreatedDate
    @Column(name = "created_date")
    private Instant webCreatedDate = Instant.now();
    
    @Transient
    @JsonSerialize
    private String name;
    
    @Transient
    @JsonSerialize
    private String addressZip;
    
    @Transient
    @JsonSerialize
    private String cardNumber;
    
    
    public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	@JsonGetter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@JsonGetter
	public String getAddressZip() {
		return addressZip;
	}

	public void setAddressZip(String addressZip) {
		this.addressZip = addressZip;
	}
	@JsonGetter
	public Long getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(Long expMonth) {
		this.expMonth = expMonth;
	}
	@JsonGetter
	public Long getExpYear() {
		return expYear;
	}

	public void setExpYear(Long expYear) {
		this.expYear = expYear;
	}


	@Transient
	@JsonSerialize
    private Long expMonth;
    
    @Transient
    @JsonSerialize
    private Long expYear;
    
	 @Override
	    public String toString() {
	        return "User{" +
	            "id='" + id + '\'' +
	            ",cardId='" + cardId + '\'' +
	            ", customerId='" + customerId + '\'' +
	            ", payment='" + payment + '\'' +
	            ", webCreatedDate='" + webCreatedDate + '\'' +
	             ", expMonth='" + expMonth + '\'' +
	            ", expYear='" + expYear + '\'' +
	             ", addressZip='" + addressZip + '\'' +
	            ", name='" + name + '\'' +
	        ", cardNumber='" + cardNumber + '\'' +
	            "}";
	    }
	
	
}
