package com.ai.mp.domain;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DataProviderType.
 */
@Entity
@Table(name = "data_provider_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dataprovidertype")
public class DataProviderType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "record_id", length = 50)
    private String recordID;

    @NotNull
    @Lob
    @Column(name = "explanation", nullable = false)
    private String explanation;

    @NotNull
    @Size(max = 50)
    @Column(name = "provider_type", length = 50, nullable = false)
    private String providerType;

    @NotNull
    @Max(value = 2)
    @Column(name = "inactive", nullable = false)
    private Integer inactive;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @NotNull
    @Column(name = "created_date", nullable = false)
    private LocalDate createdDate;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "updated_date")
    private Instant updatedDate;

    @OneToMany(mappedBy = "providerType")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> dataProviders = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public DataProviderType recordID(String recordID) {
        this.recordID = recordID;
        return this;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getExplanation() {
        return explanation;
    }

    public DataProviderType explanation(String explanation) {
        this.explanation = explanation;
        return this;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getProviderType() {
        return providerType;
    }

    public DataProviderType providerType(String providerType) {
        this.providerType = providerType;
        return this;
    }

    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public Integer getInactive() {
        return inactive;
    }

    public DataProviderType inactive(Integer inactive) {
        this.inactive = inactive;
        return this;
    }

    public void setInactive(Integer inactive) {
        this.inactive = inactive;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public DataProviderType createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public DataProviderType updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Set<DataProvider> getDataProviders() {
        return dataProviders;
    }

    public DataProviderType dataProviders(Set<DataProvider> dataProviders) {
        this.dataProviders = dataProviders;
        return this;
    }

    public DataProviderType addDataProvider(DataProvider dataProvider) {
        this.dataProviders.add(dataProvider);
        dataProvider.setProviderType(this);
        return this;
    }

    public DataProviderType removeDataProvider(DataProvider dataProvider) {
        this.dataProviders.remove(dataProvider);
        dataProvider.setProviderType(null);
        return this;
    }

    public void setDataProviders(Set<DataProvider> dataProviders) {
        this.dataProviders = dataProviders;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataProviderType dataProviderType = (DataProviderType) o;
        if (dataProviderType.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataProviderType.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataProviderType{" +
            "id=" + getId() +
            ", recordID='" + getRecordID() + "'" +
            ", explanation='" + getExplanation() + "'" +
            ", providerType='" + getProviderType() + "'" +
            ", inactive='" + getInactive() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            "}";
    }
}
