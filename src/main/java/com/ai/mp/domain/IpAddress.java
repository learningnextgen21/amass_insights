package com.ai.mp.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;

@Entity
@Table(name = "ip_address")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "ipaddress")
public class IpAddress implements Serializable {

    private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "ip_address")
	private String ipAddress;
	
	public int getCount() {
		return count;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Column(name = "count")
	private int count;
	
	@Column(name = "is_active")
	private boolean isActive;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	
	
	    public Instant getWebCreatedDate() {
		return webCreatedDate;
	}

	public void setWebCreatedDate(Instant webCreatedDate) {
		this.webCreatedDate = webCreatedDate;
	}

		@CreatedDate
	    @Column(name = "web_created_date")
	    @JsonIgnore
	    private Instant webCreatedDate;
	 
		 @LastModifiedDate
		 @Column(name = "web_updated_date")
		 @JsonIgnore
		 private Instant webUpdateDate;
		 
	  public Instant getWebUpdateDate() {
			return webUpdateDate;
		}

		public void setWebUpdateDate(Instant webUpdateDate) {
			this.webUpdateDate = webUpdateDate;
		}

	@Override
	    public String toString() {
	        return "IpAddress{" +
	            "id=" + getId() +
	            ", ipAddress='" + getIpAddress() + "'" +
	            ", isActive='" + isActive() + "'" +
	            ", count='" + getCount() + "'" +
	            ", webCreatedDate='" + getWebCreatedDate() + "'" +
	            ", webUpdateDate='" + getWebUpdateDate() + "'" +
	            "}";
	    }
	
}
