package com.ai.mp.domain;


import com.ai.mp.utils.BasicBean;
import com.ai.mp.utils.DateToStringTypeAdapter;
import com.ai.mp.utils.SetToBeanConverter;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "data_provider_edits")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dataprovideredits")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataProviderEdit implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "record_id")
    private String recordID;

   @Column(name = "provider_name")
    private String providerName;

    @Column(name = "website")
    private String website;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "long_description")
    private String longDescription;

    @Column(name = "completeness")
    private float completeness;

    @Column(name = "notes")
    private String notes;

    /*@Max(value = 2)
    @Column(name = "live")
    private Integer live;*/

    /*@JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "created_date")
    private LocalDate createdDate;*/

    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "updated_date")
    private LocalDate updatedDate;

  /*  @Column(name = "data_update_frequency_id")
    private Long dataUpdateFrequency;*/

    @Column(name="data_update_frequency")
    private String dataUpdateFrequency;


    @Column(name = "data_update_frequency_notes")
    private String dataUpdateFrequencyNotes;

    @Column(name = "date_collection_began")
    private String dateCollectionBegan;

   /* @Column(name = "score_overall_id")
    private Long scoreOverall;
*//*
    @Column(name = "score_readiness_id")
    private Long scoreReadiness;*/

    @Column(name = "date_collection_range_explanation")
    private String dateCollectionRangeExplanation;

    @Column(name = "collection_methods_explanation")
    private String collectionMethodsExplanation;

    @Column(name = "potential_drawbacks")
    private String potentialDrawbacks;

  /*  @Column(name = "unique_value_props")
    private String uniqueValueProps;*/

    /*   @Column(name = "historical_date_range")
       private String historicalDateRange;
   */
    @Column(name = "key_data_fields")
    private String keyDataFields;

    @Column(name = "summary")
    private String summary;

 /*   @Column(name = "research_methods_completed", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> researchMethodsCompleted;*/

    @Column(name = "investor_clients")
    private String investor_clients;

/*    @Column(name = "historical_date_range_estimate")
    private String historicalDateRangeEstimate;*/

    @Column(name = "public_companies_covered")
    private String publicCompaniesCovered;

   /* @Column (name ="score_uniqueness_id")
    private Long scoreUniqueness;*/

   /* @Column (name ="score_value_id")
    private Long scoreValue;*/
/*
    @Column(name = "marketplace_status")
    private String marketplaceStatus;*/

    @Column(name = "free_trial_available")
    private String freeTrialAvailable;

    @Column(name = "product_details")
    private String productDetails;

    @ManyToOne(fetch = FetchType.EAGER)
    private DataCategory mainDataCategory;

    @ManyToOne(fetch = FetchType.LAZY)
    private DataOrganization ownerOrganization;

    @ManyToOne(fetch = FetchType.EAGER)
    private DataIndustry mainDataIndustry;

    @ManyToOne(fetch = FetchType.EAGER)
    private DataProviderType providerType ;

    @Column(name = "feature", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> features = new ArrayList<BasicBean>();

    @Column(name = "geographical_focus", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> geographicalFoci = new ArrayList<BasicBean>();

    @Column(name = "provider_partners", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> providerPartners = new ArrayList<BasicBean>();

    @Column(name = "distribution_partners", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> distributionPartners = new ArrayList<BasicBean>();
/*
    @Column(name = "relevant_articles", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> relevantArticles;*/

    @Column(name = "consumer_partners", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> consumerPartners = new ArrayList<BasicBean>();

    @Column(name = "category", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> categories= new ArrayList<BasicBean>();

    @Column(name = "industry", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> industries= new ArrayList<BasicBean>();


    @Column(name = "tag", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> providerTags = new ArrayList<BasicBean>();

    @Column(name = "source", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> sources = new ArrayList<BasicBean>();

    @Column(name = "asset_class", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> assetClasses = new ArrayList<BasicBean>();

    /*@Column(name = "security_type", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> securityType;*/

    @Column(name = "delivery_format", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> deliveryFormats = new ArrayList<BasicBean>();

    @Column(name = "pricing_model", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> dataProvidersPricingModels = new ArrayList<BasicBean>();

    

	public List<BasicBean> getDataProvidersPricingModels() {
		return dataProvidersPricingModels;
	}

	public void setDataProvidersPricingModels(List<BasicBean> dataProvidersPricingModels) {
		this.dataProvidersPricingModels = dataProvidersPricingModels;
	}


	@Column(name = "score_investors_willingness_id")
    private Integer scoreInvestorsWillingness;

    /*@Column(name = "pricing_model_id")
    private Long pricingModel;*/

    @Column(name = "relevant_sector", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> relevantSectors = new ArrayList<BasicBean>();;

    @Column(name = "delivery_method", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> deliveryMethods = new ArrayList<BasicBean>();;

    @Size(max = 20)
    @Column(name = "permission_name", updatable = true, insertable = true)
    private String userPermission;

    /*@Size(max = 64)
    @Column(name = "org_relationship_status", length = 64)
    private String orgRelationshipStatus;*/

    @Column (name ="Sample_or_Panel_Size")
    private String sampleOrPanelSize;

    @Column(name="Use_Cases_or_Questions_Addressed")
    private String useCasesOrQuestionsAddressed;

    // @Column(name="Data_License_Type")
    // private String dataLicenseType;

    @Column(name="Data_License_Type_Details")
    private String dataLicenseTypeDetails;

   /* @Column(name ="Data_Languages_Available")
    private String dataLanguagesAvailable;*/

    @Column(name ="email")
    private String emails;

   /* @Column(name ="Legal_and_Compliance_Issues")
    private String legalAndComplianceIssues;*/

    @Column(name="Data_Roadmap")
    private String dataRoadmap;

    @Column(name = "delivery_frequency_notes")
    private String deliveryFrequencyNotes;

  /*  @Column(name = "public_companies_covered_num")
    private Integer publicCompaniesCoveredNum;*/

    @Column(name = "out_of_sample_data")
    private String outOfSampleData;

    @Column(name = "trial_duration")
    private String trialDuration;

    @Column (name ="creator_user_id")
    private Long creatorUserId;

    @Column (name ="editor_user_id")
    private Long editorUserId;

    @Column (name ="edited_date")
    private Timestamp editedDate=Timestamp.from(Instant.now());

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "country")
    private String country;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "year_founded")
    private Integer yearFounded;

    @Column(name = "headcount")
    private String headCount;

    @Column(name = "data_provider_investor_type", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> investorTypes = new ArrayList<BasicBean>();

    @Column(name = "delivery_frequency", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> deliveryFrequencies = new ArrayList<BasicBean>();

    @Column(name = "relevant_security_types", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> securityTypes = new ArrayList<BasicBean>();

   /* @Column(name = "public_equites_covered")
    private Integer publicEquitesCovered;*/
    
    @Column(name = "public_equities_covered")
    private Long publicEquitiesCoveredCount;
    
/*    @Column(name = "main_asset_class_id")
    private Long mainAssetClass;
*/

    @Column(name = "main_asset_class")
    private String mainAssetClass;

    public String getMainAssetClass() {
        return mainAssetClass;
    }

    public void setMainAssetClass(String mainAssetClass) {
        this.mainAssetClass = mainAssetClass;
    }


    @Column(name = "record_updated")
    private Boolean recordUpdated;

    @Column(name = "sec_regulated_id")
    private Long secRegulated;

   /* @Column(name = "profile_verification_status_id")
    private Long profileVerificationStatus;*/

    /*@Column(name = "public_equities_covered_count")
    private Long publicEquitiesCoveredCount;*/

    @Column(name = "investor_clients_count")
    private Long investorClientsCount;

    // @Column(name = "public_equities_covered_range_id")
    // private Long publicEquitiesCoveredRange;

    @Column(name = "trial_agreement_type_id")
    private Long trialAgreementType;

    @Column(name = "trial_pricing_model_id")
    private Long trialPricingModel;

    @Column(name="partnership_id")
    private Long partnership;

    @Column(name = "trial_pricing_details")
    private String trialPricingDetails;

    @Column(name = "pricing_details")
    private String pricingDetails;

    @Column(name = "lag_time")
    private String lagTime;

    @Column(name = "competitive_differentiators")
    private String competitiveDifferentiators;

    @Column(name = "discussion_analytics")
    private String discussionAnalytics;

    @Column(name = "discussion_cleanliness")
    private String discussionCleanliness;

    @Column(name = "sample_or_panel_biases")
    private String sampleOrPanelBiases;

    @Column(name = "dataset_size_id")
    private String datasetSize;

    @Column(name = "dataset_number_of_rows_id")
    private String datasetNumberOfRows;

    @Column(name = "number_of_analyst_employees")
    private Long numberOfAnalystEmployees;

    @Column(name = "point_in_time_accuracy_id")
    private Long pointInTimeAccuracy;

    @Column(name = "data_sources_details")
    private String dataSourcesDetails;

    @Column(name = "data_gaps_id")
    private Long dataGaps;

    @Column(name = "includes_outliers_id")
    private Long includesOutliers;

    @Column(name = "normalized_id")
    private Long normalized;

    @Column(name = "duplicates_cleaned_id")
    private Long duplicatesCleaned;

    /*@Column(name = "investor_client_bucket_id")
    private Long investorClientBucket;
*/

    @Column(name="investor_client_bucket")
    private String investorClientBucket;

    @Column(name = "subscription_time_period_id")
    private Long subscriptionTimePeriod;

    @Column(name = "year_first_data_product_launched")
    private Long yearFirstDataProductLaunched;

  /*  @Column(name = "compliance_score_personal")
    private String complianceScorePersonal;
*/
   /* @Column(name = "compliance_score_public")
    private String complianceScorePublic;*/

    @Column(name = "monthly_price_range_id")
    private Long monthlyPriceRange;

    @Column(name = "subscription_model_id")
    private Long subscriptionModel;

    @Column(name = "minimum_yearly_price")
    private String minimumYearlyPrice;

    @Column(name = "maximum_yearly_price")
    private String maximumYearlyPrice;

    @Column(name = "number_of_data_sources_id")
    private Long numberOfDataSources;

    @Column(name = "privacy")
    private String privacy;

    @Column(name = "biases", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> biases = new ArrayList<BasicBean>();

    @Column(name = "outlier_reasons", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> outlierReasons = new ArrayList<BasicBean>();

    @Column(name = "data_gaps_reasons", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> dataGapsReasons = new ArrayList<BasicBean>();

    @Column(name = "organization_type", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> organizationType = new ArrayList<BasicBean>();

    @Column(name = "manager_type", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> managerType = new ArrayList<BasicBean>();

    @Column(name = "strategies", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> strategies = new ArrayList<BasicBean>();

    @Column(name = "research_styles", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> researchStyles = new ArrayList<BasicBean>();

    @Column(name = "investing_time_frame", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> investingTimeFrame = new ArrayList<BasicBean>();

    @Column(name = "equities_market_cap", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> equitiesMarketCap = new ArrayList<BasicBean>();

    @Column(name = "equities_style", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> equitiesStyle = new ArrayList<BasicBean>();

    @Column(name = "identifiers_available", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> identifiersAvailable = new ArrayList<BasicBean>();

    @Column(name = "access_offered", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> accessOffered = new ArrayList<BasicBean>();

    @Column(name = "data_product_types", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> dataProductTypes = new ArrayList<BasicBean>();

    @Column(name = "data_types", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> dataTypes = new ArrayList<BasicBean>();

    @Column(name = "languages_available", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> languagesAvailable = new ArrayList<BasicBean>();

    @Column(name = "payment_methods_offered", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> paymentMethodsOffered = new ArrayList<BasicBean>();

    @Column(name = "complementary_providers", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> complementaryProviders= new ArrayList<BasicBean>();

    @Column(name = "competitors", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> competitors = new ArrayList<BasicBean>();

    @Column(name = "year_exit")
    private Integer yearExit;

    @Column(name = "financial_position")
    private String financialPosition;

    @Column(name = "client_base")
    private String clientBase;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "headcount_number")
    private String headcountNumber;

    @Column(name = "headcount_bucket_id")
    private Long headcountBucket;

 /*   @Column(name = "subsidiary_organization", insertable = true, updatable = true)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> subsidiaryOrganization;*/

    // @Column(name = "inactive")
    // private Integer inactive;

    /* @Column(name="headcount_details")
     private String headcountDetails;
 */

    @Column(name = "provider_data_partner")
    private String providerDataPartner;

    @Column(name = "distribution_data_partner")
    private String distributionDataPartner;

    @Column(name = "consumer_data_partner")
    private String consumerDataPartner;

    @Column(name = "complementary_data_providers")
    private String complementaryDataProviders;

    @Column(name = "competitor_provider")
    private String competitorProvider;

   @Column(name="visibility")
    private String visibility;

   public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getProviderDataPartner() {
        return providerDataPartner;
    }

    public void setProviderDataPartner(String providerDataPartner) {
        this.providerDataPartner = providerDataPartner;
    }

    public String getDistributionDataPartner() {
        return distributionDataPartner;
    }

    public void setDistributionDataPartner(String distributionDataPartner) {
        this.distributionDataPartner = distributionDataPartner;
    }

    public String getConsumerDataPartner() {
        return consumerDataPartner;
    }

    public void setConsumerDataPartner(String consumerDataPartner) {
        this.consumerDataPartner = consumerDataPartner;
    }

    public String getComplementaryDataProviders() {
        return complementaryDataProviders;
    }

    public void setComplementaryDataProviders(String complementaryDataProviders) {
        this.complementaryDataProviders = complementaryDataProviders;
    }

    public String getCompetitorProvider() {
        return competitorProvider;
    }

    public void setCompetitorProvider(String competitorProvider) {
        this.competitorProvider = competitorProvider;
    }

    public Boolean getConsent() {
        return consent;
    }

    public void setConsent(Boolean consent) {
        this.consent = consent;
    }


    @Column(name="consent")
    private Boolean consent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

  /*  public Integer getLive() {
        return live;
    }

    public void setLive(Integer live) {
        this.live = live;
    }
*/
  /*  public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }*/

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getDataUpdateFrequencyNotes() {
        return dataUpdateFrequencyNotes;
    }

    public void setDataUpdateFrequencyNotes(String dataUpdateFrequencyNotes) {
        this.dataUpdateFrequencyNotes = dataUpdateFrequencyNotes;
    }

    public String getDateCollectionBegan() {
        return dateCollectionBegan;
    }

    public void setDateCollectionBegan(String dateCollectionBegan) {
        this.dateCollectionBegan = dateCollectionBegan;
    }

    public String getDateCollectionRangeExplanation() {
        return dateCollectionRangeExplanation;
    }

    public void setDateCollectionRangeExplanation(String dateCollectionRangeExplanation) {
        this.dateCollectionRangeExplanation = dateCollectionRangeExplanation;
    }

    public String getCollectionMethodsExplanation() {
        return collectionMethodsExplanation;
    }

    public void setCollectionMethodsExplanation(String collectionMethodsExplanation) {
        this.collectionMethodsExplanation = collectionMethodsExplanation;
    }

    public String getPotentialDrawbacks() {
        return potentialDrawbacks;
    }

    public void setPotentialDrawbacks(String potentialDrawbacks) {
        this.potentialDrawbacks = potentialDrawbacks;
    }

  /*  public String getUniqueValueProps() {
        return uniqueValueProps;
    }

    public void setUniqueValueProps(String uniqueValueProps) {
        this.uniqueValueProps = uniqueValueProps;
    }

    public String getHistoricalDateRange() {
        return historicalDateRange;
    }

    public void setHistoricalDateRange(String historicalDateRange) {
        this.historicalDateRange = historicalDateRange;
    }*/

    public String getKeyDataFields() {
        return keyDataFields;
    }

    public void setKeyDataFields(String keyDataFields) {
        this.keyDataFields = keyDataFields;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    /* public List<BasicBean> getResearchMethodsCompleted() {
         return researchMethodsCompleted;
     }

     public void setResearchMethodsCompleted(List<BasicBean> researchMethodsCompleted) {
         this.researchMethodsCompleted = researchMethodsCompleted;
     }
 */
    public String getInvestor_clients() {
        return investor_clients;
    }

    public void setInvestor_clients(String investor_clients) {
        this.investor_clients = investor_clients;
    }

   /* public String getHistoricalDateRangeEstimate() {
        return historicalDateRangeEstimate;
    }

    public void setHistoricalDateRangeEstimate(String historicalDateRangeEstimate) {
        this.historicalDateRangeEstimate = historicalDateRangeEstimate;
    }*/

    public String getPublicCompaniesCovered() {
        return publicCompaniesCovered;
    }

    public void setPublicCompaniesCovered(String publicCompaniesCovered) {
        this.publicCompaniesCovered = publicCompaniesCovered;
    }

    /*   public String getMarketplaceStatus() {
           return marketplaceStatus;
       }

       public void setMarketplaceStatus(String marketplaceStatus) {
           this.marketplaceStatus = marketplaceStatus;
       }
   */
    public String getFreeTrialAvailable() {
        return freeTrialAvailable;
    }

    public void setFreeTrialAvailable(String freeTrialAvailable) {
        this.freeTrialAvailable = freeTrialAvailable;
    }

    public String getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(String productDetails) {
        this.productDetails = productDetails;
    }

    public DataCategory getMainDataCategory() {
        return mainDataCategory;
    }

    public void setMainDataCategory(DataCategory mainDataCategory) {
        this.mainDataCategory = mainDataCategory;
    }

    public DataOrganization getOwnerOrganization() {
        return ownerOrganization;
    }

    public void setOwnerOrganization(DataOrganization ownerOrganization) {
        this.ownerOrganization = ownerOrganization;
    }

    public DataIndustry getMainDataIndustry() {
        return mainDataIndustry;
    }

    public void setMainDataIndustry(DataIndustry mainDataIndustry) {
        this.mainDataIndustry = mainDataIndustry;
    }

    public DataProviderType getProviderType() {
        return providerType;
    }

    public void setProviderType(DataProviderType providerType) {
        this.providerType = providerType;
    }

    public List<BasicBean> getFeatures() {
        return features;
    }

    public void setFeatures(List<BasicBean> features) {
        this.features = features;
    }

    public List<BasicBean> getGeographicalFoci() {
        return geographicalFoci;
    }

    public void setGeographicalFoci(List<BasicBean> geographicalFoci) {
        this.geographicalFoci = geographicalFoci;
    }

    public List<BasicBean> getCategories() {
        return categories;
    }

    public void setCategories(List<BasicBean> categories) {
        this.categories = categories;
    }

    public List<BasicBean> getIndustries() {
        return industries;
    }

    public void setIndustries(List<BasicBean> industries) {
        this.industries = industries;
    }

    public List<BasicBean> getProviderTags() {
        return providerTags;
    }

    public void setProviderTags(List<BasicBean> providerTags) {
        this.providerTags = providerTags;
    }

    public List<BasicBean> getSources() {
        return sources;
    }

    public void setSources(List<BasicBean> sources) {
        this.sources = sources;
    }

    public List<BasicBean> getAssetClasses() {
        return assetClasses;
    }

    public void setAssetClasses(List<BasicBean> assetClasses) {
        this.assetClasses = assetClasses;
    }

   /* public List<BasicBean> getSecurityType() {
        return securityType;
    }

    public void setSecurityType(List<BasicBean> securityType) {
        this.securityType = securityType;
    }*/

    public List<BasicBean> getDeliveryFormats() {
        return deliveryFormats;
    }

    public void setDeliveryFormats(List<BasicBean> deliveryFormats) {
        this.deliveryFormats = deliveryFormats;
    }

    public Integer getScoreInvestorsWillingness() {
        return scoreInvestorsWillingness;
    }

    public void setScoreInvestorsWillingness(Integer scoreInvestorsWillingness) {
        this.scoreInvestorsWillingness = scoreInvestorsWillingness;
    }

   /* public Long getPricingModels() {
        return pricingModel;
    }

    public void setPricingModels(Long pricingModel) {
        this.pricingModel = pricingModel;
    }*/

    public List<BasicBean> getRelevantSectors() {
        return relevantSectors;
    }

    public void setRelevantSectors(List<BasicBean> relevantSectors) {
        this.relevantSectors = relevantSectors;
    }

    public List<BasicBean> getDeliveryMethods() {
        return deliveryMethods;
    }

    public void setDeliveryMethods(List<BasicBean> deliveryMethods) {
        this.deliveryMethods = deliveryMethods;
    }

    public String getUserPermission() {
        return userPermission;
    }

    public void setUserPermission(String userPermission) {
        this.userPermission = userPermission;
    }

    /*  public String getOrgRelationshipStatus() {
          return orgRelationshipStatus;
      }

      public void setOrgRelationshipStatus(String orgRelationshipStatus) {
          this.orgRelationshipStatus = orgRelationshipStatus;
      }
  */
    public String getSampleOrPanelSize() {
        return sampleOrPanelSize;
    }

    public void setSampleOrPanelSize(String sampleOrPanelSize) {
        this.sampleOrPanelSize = sampleOrPanelSize;
    }

    public String getUseCasesOrQuestionsAddressed() {
        return useCasesOrQuestionsAddressed;
    }

    public void setUseCasesOrQuestionsAddressed(String useCasesOrQuestionsAddressed) {
        this.useCasesOrQuestionsAddressed = useCasesOrQuestionsAddressed;
    }

    // public String getDataLicenseType() {
    //     return dataLicenseType;
    // }

    // public void setDataLicenseType(String dataLicenseType) {
    //     this.dataLicenseType = dataLicenseType;
    // }

    @Column(name = "data_license_type_id")
    private Long dataLicenseType;

    public Long getDataLicenseType() {
        return dataLicenseType;
    }

    public void setDataLicenseType(Long dataLicenseType) {
        this.dataLicenseType = dataLicenseType;
    }

    public String getDataLicenseTypeDetails() {
        return dataLicenseTypeDetails;
    }

    public void setDataLicenseTypeDetails(String dataLicenseTypeDetails) {
        this.dataLicenseTypeDetails = dataLicenseTypeDetails;
    }

  /*  public String getDataLanguagesAvailable() {
        return dataLanguagesAvailable;
    }

    public void setDataLanguagesAvailable(String dataLanguagesAvailable) {
        this.dataLanguagesAvailable = dataLanguagesAvailable;
    }*/

   

    public String getDataRoadmap() {
        return dataRoadmap;
    }

    public void setDataRoadmap(String dataRoadmap) {
        this.dataRoadmap = dataRoadmap;
    }

    public String getDeliveryFrequencyNotes() {
        return deliveryFrequencyNotes;
    }

    public void setDeliveryFrequencyNotes(String deliveryFrequencyNotes) {
        this.deliveryFrequencyNotes = deliveryFrequencyNotes;
    }

   

    public String getOutOfSampleData() {
        return outOfSampleData;
    }

    public void setOutOfSampleData(String outOfSampleData) {
        this.outOfSampleData = outOfSampleData;
    }

    public Long getTrialPricingModel() {
        return trialPricingModel;
    }

    public void setTrialPricingModel(Long trialPricingModel) {
        this.trialPricingModel = trialPricingModel;
    }

    public String getTrialDuration() {
        return trialDuration;
    }

    public void setTrialDuration(String trialDuration) {
        this.trialDuration = trialDuration;
    }

    public Long getEditorUserId() {
        return editorUserId;
    }

    public void setEditorUserId(Long editorUserId) {
        this.editorUserId = editorUserId;
    }

    public Timestamp getEditedDate() {
        return editedDate;
    }

    public void setEditedDate(Timestamp editedDate) {
        this.editedDate = editedDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Integer getYearFounded() {
        return yearFounded;
    }

    public void setYearFounded(Integer yearFounded) {
        this.yearFounded = yearFounded;
    }

    public String getHeadCount() {
        return headCount;
    }

    public void setHeadCount(String headCount) {
        this.headCount = headCount;
    }

    public List<BasicBean> getInvestorTypes() {
        return investorTypes;
    }

    public void setInvestorTypes(List<BasicBean> investorTypes) {
        this.investorTypes = investorTypes;
    }

    public List<BasicBean> getDeliveryFrequencies() {
        return deliveryFrequencies;
    }

    public void setDeliveryFrequencies(List<BasicBean> deliveryFrequencies) {
        this.deliveryFrequencies = deliveryFrequencies;
    }

    public List<BasicBean> getSecurityTypes() {
        return securityTypes;
    }

    public void setSecurityTypes(List<BasicBean> securityTypes) {
        this.securityTypes = securityTypes;
    }

   
    public Long getPublicEquitiesCoveredCount() {
		return publicEquitiesCoveredCount;
	}

	public void setPublicEquitiesCoveredCount(Long publicEquitiesCoveredCount) {
		this.publicEquitiesCoveredCount = publicEquitiesCoveredCount;
	}

	public Boolean getRecordUpdated() {
        return recordUpdated;
    }

    public void setRecordUpdated(Boolean recordUpdated) {
        this.recordUpdated = recordUpdated;
    }

    public Long getSecRegulated() {
        return secRegulated;
    }

    public void setSecRegulated(Long secRegulated) {
        this.secRegulated = secRegulated;
    }

    

   /* public Long getPublicEquitiesCoveredCount() {
        return publicEquitiesCoveredCount;
    }

    public void setPublicEquitiesCoveredCount(Long publicEquitiesCoveredCount) {
        this.publicEquitiesCoveredCount = publicEquitiesCoveredCount;
    }*/

    public Long getInvestorClientsCount() {
        return investorClientsCount;
    }

    public void setInvestorClientsCount(Long investorClientsCount) {
        this.investorClientsCount = investorClientsCount;
    }

    // public Long getPublicEquitiesCoveredRange() {
    //     return publicEquitiesCoveredRange;
    // }

    // public void setPublicEquitiesCoveredRange(Long publicEquitiesCoveredRange) {
    //     this.publicEquitiesCoveredRange = publicEquitiesCoveredRange;
    // }

    @Column(name="public_equities_covered_range")
    private String publicEquitiesCoveredRange;

    public String getPublicEquitiesCoveredRange() {
        return publicEquitiesCoveredRange;
    }

    public void setPublicEquitiesCoveredRange(String publicEquitiesCoveredRange) {
        this.publicEquitiesCoveredRange = publicEquitiesCoveredRange;
    }

    public Long getTrialAgreementType() {
        return trialAgreementType;
    }

    public void setTrialAgreementType(Long trialAgreementType) {
        this.trialAgreementType = trialAgreementType;
    }

    public String getTrialPricingDetails() {
        return trialPricingDetails;
    }

    public void setTrialPricingDetails(String trialPricingDetails) {
        this.trialPricingDetails = trialPricingDetails;
    }

    public String getLagTime() {
        return lagTime;
    }

    public void setLagTime(String lagTime) {
        this.lagTime = lagTime;
    }

    public String getCompetitiveDifferentiators() {
        return competitiveDifferentiators;
    }

    public void setCompetitiveDifferentiators(String competitiveDifferentiators) {
        this.competitiveDifferentiators = competitiveDifferentiators;
    }

    public String getDiscussionAnalytics() {
        return discussionAnalytics;
    }

    public void setDiscussionAnalytics(String discussionAnalytics) {
        this.discussionAnalytics = discussionAnalytics;
    }

    public String getDiscussionCleanliness() {
        return discussionCleanliness;
    }

    public void setDiscussionCleanliness(String discussionCleanliness) {
        this.discussionCleanliness = discussionCleanliness;
    }

    public String getSampleOrPanelBiases() {
        return sampleOrPanelBiases;
    }

    public void setSampleOrPanelBiases(String sampleOrPanelBiases) {
        this.sampleOrPanelBiases = sampleOrPanelBiases;
    }

    public String getDatasetSize() {
        return datasetSize;
    }

    public void setDatasetSize(String datasetSize) {
        this.datasetSize = datasetSize;
    }

    public String getDatasetNumberOfRows() {
        return datasetNumberOfRows;
    }

    public void setDatasetNumberOfRows(String datasetNumberOfRows) {
        this.datasetNumberOfRows = datasetNumberOfRows;
    }

    public Long getNumberOfAnalystEmployees() {
        return numberOfAnalystEmployees;
    }

    public void setNumberOfAnalystEmployees(Long numberOfAnalystEmployees) {
        this.numberOfAnalystEmployees = numberOfAnalystEmployees;
    }

    public Long getPointInTimeAccuracy() {
        return pointInTimeAccuracy;
    }

    public void setPointInTimeAccuracy(Long pointInTimeAccuracy) {
        this.pointInTimeAccuracy = pointInTimeAccuracy;
    }

    public String getDataSourcesDetails() {
        return dataSourcesDetails;
    }

    public void setDataSourcesDetails(String dataSourcesDetails) {
        this.dataSourcesDetails = dataSourcesDetails;
    }

    public Long getDataGaps() {
        return dataGaps;
    }

    public void setDataGaps(Long dataGaps) {
        this.dataGaps = dataGaps;
    }

    public Long getIncludesOutliers() {
        return includesOutliers;
    }

    public void setIncludesOutliers(Long includesOutliers) {
        this.includesOutliers = includesOutliers;
    }

    public Long getNormalized() {
        return normalized;
    }

    public void setNormalized(Long normalized) {
        this.normalized = normalized;
    }

    public Long getDuplicatesCleaned() {
        return duplicatesCleaned;
    }

    public void setDuplicatesCleaned(Long duplicatesCleaned) {
        this.duplicatesCleaned = duplicatesCleaned;
    }

    /*public Long getInvestorClientBucket() {
        return investorClientBucket;
    }

    public void setInvestorClientBucket(Long investorClientBucket) {
        this.investorClientBucket = investorClientBucket;
    }*/

    public Long getSubscriptionTimePeriod() {
        return subscriptionTimePeriod;
    }

    public String getInvestorClientBucket() {
        return investorClientBucket;
    }

    public void setInvestorClientBucket(String investorClientBucket) {
        this.investorClientBucket = investorClientBucket;
    }

    public void setSubscriptionTimePeriod(Long subscriptionTimePeriod) {
        this.subscriptionTimePeriod = subscriptionTimePeriod;
    }

    public Long getYearFirstDataProductLaunched() {
        return yearFirstDataProductLaunched;
    }

    public void setYearFirstDataProductLaunched(Long yearFirstDataProductLaunched) {
        this.yearFirstDataProductLaunched = yearFirstDataProductLaunched;
    }

    /*public String getComplianceScorePersonal() {
        return complianceScorePersonal;
    }

    public void setComplianceScorePersonal(String complianceScorePersonal) {
        this.complianceScorePersonal = complianceScorePersonal;
    }

    public String getComplianceScorePublic() {
        return complianceScorePublic;
    }

    public void setComplianceScorePublic(String complianceScorePublic) {
        this.complianceScorePublic = complianceScorePublic;
    }
*/
    public Long getMonthlyPriceRange() {
        return monthlyPriceRange;
    }

    public void setMonthlyPriceRange(Long monthlyPriceRange) {
        this.monthlyPriceRange = monthlyPriceRange;
    }

    public Long getSubscriptionModel() {
        return subscriptionModel;
    }

    public void setSubscriptionModel(Long subscriptionModel) {
        this.subscriptionModel = subscriptionModel;
    }

    public String getMinimumYearlyPrice() {
        return minimumYearlyPrice;
    }

    public void setMinimumYearlyPrice(String minimumYearlyPrice) {
        this.minimumYearlyPrice = minimumYearlyPrice;
    }

    public String getMaximumYearlyPrice() {
        return maximumYearlyPrice;
    }

    public void setMaximumYearlyPrice(String maximumYearlyPrice) {
        this.maximumYearlyPrice = maximumYearlyPrice;
    }

    public Long getNumberOfDataSources() {
        return numberOfDataSources;
    }

    public void setNumberOfDataSources(Long numberOfDataSources) {
        this.numberOfDataSources = numberOfDataSources;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public List<BasicBean> getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(List<BasicBean> organizationType) {
        this.organizationType = organizationType;
    }

    public List<BasicBean> getManagerType() {
        return managerType;
    }

    public void setManagerType(List<BasicBean> managerType) {
        this.managerType = managerType;
    }

    public List<BasicBean> getStrategies() {
        return strategies;
    }

    public void setStrategies(List<BasicBean> strategies) {
        this.strategies = strategies;
    }

    public List<BasicBean> getResearchStyles() {
        return researchStyles;
    }

    public void setResearchStyles(List<BasicBean> researchStyles) {
        this.researchStyles = researchStyles;
    }

    public List<BasicBean> getInvestingTimeFrame() {
        return investingTimeFrame;
    }

    public void setInvestingTimeFrame(List<BasicBean> investingTimeFrame) {
        this.investingTimeFrame = investingTimeFrame;
    }

    public List<BasicBean> getEquitiesMarketCap() {
        return equitiesMarketCap;
    }

    public void setEquitiesMarketCap(List<BasicBean> equitiesMarketCap) {
        this.equitiesMarketCap = equitiesMarketCap;
    }

    public List<BasicBean> getEquitiesStyle() {
        return equitiesStyle;
    }

    public void setEquitiesStyle(List<BasicBean> equitiesStyle) {
        this.equitiesStyle = equitiesStyle;
    }

    public List<BasicBean> getIdentifiersAvailable() {
        return identifiersAvailable;
    }

    public void setIdentifiersAvailable(List<BasicBean> identifiersAvailable) {
        this.identifiersAvailable = identifiersAvailable;
    }

    public List<BasicBean> getAccessOffered() {
        return accessOffered;
    }

    public void setAccessOffered(List<BasicBean> accessOffered) {
        this.accessOffered = accessOffered;
    }

    public List<BasicBean> getDataProductTypes() {
        return dataProductTypes;
    }

    public void setDataProductTypes(List<BasicBean> dataProductTypes) {
        this.dataProductTypes = dataProductTypes;
    }

    public List<BasicBean> getDataTypes() {
        return dataTypes;
    }

    public void setDataTypes(List<BasicBean> dataTypes) {
        this.dataTypes = dataTypes;
    }

    public List<BasicBean> getLanguagesAvailable() {
        return languagesAvailable;
    }

    public void setLanguagesAvailable(List<BasicBean> languagesAvailable) {
        this.languagesAvailable = languagesAvailable;
    }

    public List<BasicBean> getPaymentMethodsOffered() {
        return paymentMethodsOffered;
    }

    public void setPaymentMethodsOffered(List<BasicBean> paymentMethodsOffered) {
        this.paymentMethodsOffered = paymentMethodsOffered;
    }

    public List<BasicBean> getBiases() {
        return biases;
    }

    public void setBiases(List<BasicBean> biases) {
        this.biases = biases;
    }

    public List<BasicBean> getOutlierReasons() {
        return outlierReasons;
    }

    public void setOutlierReasons(List<BasicBean> outlierReasons) {
        this.outlierReasons = outlierReasons;
    }

    public List<BasicBean> getDataGapsReasons() {
        return dataGapsReasons;
    }

    public void setDataGapsReasons(List<BasicBean> dataGapsReasons) {
        this.dataGapsReasons = dataGapsReasons;
    }

/*    public Long getScoreUniqueness() {
        return scoreUniqueness;
    }

    public void setScoreUniqueness(Long scoreUniqueness) {
        this.scoreUniqueness = scoreUniqueness;
    }

    public Long getScoreOverall() {
        return scoreOverall;
    }

    public void setScoreOverall(Long scoreOverall) {
        this.scoreOverall = scoreOverall;
    }

    public Long getScoreValue() {
        return scoreValue;
    }

    public void setScoreValue(Long scoreValue) {
        this.scoreValue = scoreValue;
    }*/

    public Long getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(Long creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

   /* public Long getMainAssetClass() {
        return mainAssetClass;
    }

    public void setMainAssetClass(Long mainAssetClass) {
        this.mainAssetClass = mainAssetClass;
    }
*/
  /*  public Long getDataUpdateFrequency() {
        return dataUpdateFrequency;
    }

    public void setDataUpdateFrequency(Long dataUpdateFrequency) {
        this.dataUpdateFrequency = dataUpdateFrequency;
    }*/

    /*public Long getScoreReadiness() {
        return scoreReadiness;
    }*/

    public String getDataUpdateFrequency() {
        return dataUpdateFrequency;
    }

    public void setDataUpdateFrequency(String dataUpdateFrequency) {
        this.dataUpdateFrequency = dataUpdateFrequency;
    }

    /*public void setScoreReadiness(Long scoreReadiness) {
        this.scoreReadiness = scoreReadiness;
    }
*/
    public List<BasicBean> getProviderPartners() {
        return providerPartners;
    }

    public void setProviderPartners(List<BasicBean> providerPartners) {
        this.providerPartners = providerPartners;
    }

    public List<BasicBean> getDistributionPartners() {
        return distributionPartners;
    }

    public void setDistributionPartners(List<BasicBean> distributionPartners) {
        this.distributionPartners = distributionPartners;
    }

/*    public List<BasicBean> getRelevantArticles() {
        return relevantArticles;
    }

    public void setRelevantArticles(List<BasicBean> relevantArticles) {
        this.relevantArticles = relevantArticles;
    }*/

    public List<BasicBean> getConsumerPartners() {
        return consumerPartners;
    }

    public void setConsumerPartners(List<BasicBean> consumerPartners) {
        this.consumerPartners = consumerPartners;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public String getPricingDetails() {
        return pricingDetails;
    }

    public void setPricingDetails(String pricingDetails) {
        this.pricingDetails = pricingDetails;
    }

    public Integer getYearExit() {
        return yearExit;
    }

    public void setYearExit(Integer yearExit) {
        this.yearExit = yearExit;
    }

    public String getFinancialPosition() {
        return financialPosition;
    }

    public void setFinancialPosition(String financialPosition) {
        this.financialPosition = financialPosition;
    }

    public String getClientBase() {
        return clientBase;
    }

    public void setClientBase(String clientBase) {
        this.clientBase = clientBase;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getHeadcountNumber() {
        return headcountNumber;
    }

    public void setHeadcountNumber(String headcountNumber) {
        this.headcountNumber = headcountNumber;
    }

    public Long getHeadcountBucket() {
        return headcountBucket;
    }

    public void setHeadcountBucket(Long headcountBucket) {
        this.headcountBucket = headcountBucket;
    }

    /*public List<BasicBean> getSubsidiaryOrganization() {
        return subsidiaryOrganization;
    }

    public void setSubsidiaryOrganization(List<BasicBean> subsidiaryOrganization) {
        this.subsidiaryOrganization = subsidiaryOrganization;
    }*/

    // public Integer getInactive() {
    //     return inactive;
    // }

    // public void setInactive(Integer inactive) {
    //     this.inactive = inactive;
    // }

    /*@Column(name="inactive")
    private Boolean inactive;*/

    /*public Boolean isInactive() {
        return inactive;
    }

    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }*/
/*
    public String getHeadcountDetails() {
        return headcountDetails;
    }

    public void setHeadcountDetails(String headcountDetails) {
        this.headcountDetails = headcountDetails;
    }*/

    public List<BasicBean> getComplementaryProviders() {
        return complementaryProviders;
    }

    public void setComplementaryProviders(List<BasicBean> complementaryProviders) {
        this.complementaryProviders = complementaryProviders;
    }

    public List<BasicBean> getCompetitors() {
        return competitors;
    }

    public void setCompetitors(List<BasicBean> competitors) {
        this.competitors = competitors;
    }

    public float getCompleteness() {
        return completeness;
    }

    public void setCompleteness(float completeness) {
        this.completeness = completeness;
    }

    public Long getPartnership() {
        return partnership;
    }

    public void setPartnership(Long partnership) {
        this.partnership = partnership;
    }

    @Override
    public String toString() {
        return "DataProviderEdit{" +
            "id=" + id +
            ", recordID='" + recordID + '\'' +
           ", providerName='" + providerName + '\'' +
            ", website='" + website + '\'' +
            ", shortDescription='" + shortDescription + '\'' +
            ", longDescription='" + longDescription + '\'' +
            ", notes='" + notes + '\'' +
            // ", live=" + live +
            //  ", createdDate=" + createdDate +
            ", updatedDate=" + updatedDate +
            ", dataUpdateFrequency=" + dataUpdateFrequency +
            ", dataUpdateFrequencyNotes='" + dataUpdateFrequencyNotes + '\'' +
            ", dateCollectionBegan='" + dateCollectionBegan + '\'' +
            //  ", scoreOverall=" + scoreOverall +
            // ", scoreReadiness=" + scoreReadiness +
            ", dateCollectionRangeExplanation='" + dateCollectionRangeExplanation + '\'' +
            ", collectionMethodsExplanation='" + collectionMethodsExplanation + '\'' +
            ", potentialDrawbacks='" + potentialDrawbacks + '\'' +
            //  ", uniqueValueProps='" + uniqueValueProps + '\'' +
            //  ", historicalDateRange='" + historicalDateRange + '\'' +
            ", keyDataFields='" + keyDataFields + '\'' +
            ", summary='" + summary + '\'' +
            //  ", researchMethodsCompleted='" + researchMethodsCompleted + '\'' +
            ", investor_clients='" + investor_clients + '\'' +
            //  ", historicalDateRangeEstimate='" + historicalDateRangeEstimate + '\'' +
            ", publicCompaniesCovered='" + publicCompaniesCovered + '\'' +
            //  ", scoreUniqueness=" + scoreUniqueness +
            //   ", scoreValue=" + scoreValue +
            //   ", marketplaceStatus='" + marketplaceStatus + '\'' +
            ", freeTrialAvailable='" + freeTrialAvailable + '\'' +
            ", productDetails='" + productDetails + '\'' +
            ", mainDataCategory=" + mainDataCategory +
            ", ownerOrganization=" + ownerOrganization +
            ", mainDataIndustry=" + mainDataIndustry +
            ", providerType=" + providerType +
            ", features=" + features +
            ", geographicalFoci=" + geographicalFoci +
            ", providerPartners=" + providerPartners +
            ", distributionPartners=" + distributionPartners +
            //  ", relevantArticles=" + relevantArticles +
            ", consumerPartners=" + consumerPartners +
            ", categories=" + categories +
            ", industries=" + industries +
            ", providerTags=" + providerTags +
            ", sources=" + sources +
            ", assetClasses=" + assetClasses +
            // ", securityType=" + securityType +
            ", deliveryFormats=" + deliveryFormats +
            ", scoreInvestorsWillingness=" + scoreInvestorsWillingness +
            ", dataProvidersPricingModels=" + dataProvidersPricingModels +
            ", relevantSectors=" + relevantSectors +
            ", deliveryMethods=" + deliveryMethods +
            ", userPermission='" + userPermission + '\'' +
            //", orgRelationshipStatus='" + orgRelationshipStatus + '\'' +
            ", sampleOrPanelSize='" + sampleOrPanelSize + '\'' +
            ", useCasesOrQuestionsAddressed='" + useCasesOrQuestionsAddressed + '\'' +
            ", dataLicenseType='" + dataLicenseType + '\'' +
            ", dataLicenseTypeDetails='" + dataLicenseTypeDetails + '\'' +
            //  ", dataLanguagesAvailable='" + dataLanguagesAvailable + '\'' +
            ", emails='" + emails + '\'' +
            ", dataRoadmap='" + dataRoadmap + '\'' +
            ", deliveryFrequencyNotes='" + deliveryFrequencyNotes + '\'' +
            ", outOfSampleData='" + outOfSampleData + '\'' +
            ", trialDuration='" + trialDuration + '\'' +
            ", creatorUserId=" + creatorUserId +
            ", editorUserId=" + editorUserId +
            ", editedDate=" + editedDate +
            ", city='" + city + '\'' +
            ", state='" + state + '\'' +
            ", country='" + country + '\'' +
            ", zipCode='" + zipCode + '\'' +
            ", yearFounded=" + yearFounded +
            ", headCount='" + headCount + '\'' +
            ", investorTypes=" + investorTypes +
            ", deliveryFrequencies=" + deliveryFrequencies +
            ", securityTypes=" + securityTypes +
            ", publicEquitiesCoveredCount=" + publicEquitiesCoveredCount +
            ", mainAssetClass=" + mainAssetClass +
            ", recordUpdated=" + recordUpdated +
            ", secRegulated=" + secRegulated +
            //", publicEquitiesCoveredCount=" + publicEquitiesCoveredCount +
            ", investorClientsCount=" + investorClientsCount +
            ", publicEquitiesCoveredRange=" + publicEquitiesCoveredRange +
            ", trialAgreementType=" + trialAgreementType +
            ", trialPricingModel=" + trialPricingModel +
            ", trialPricingDetails='" + trialPricingDetails + '\'' +
            ", pricingDetails='" + pricingDetails + '\'' +
            ", lagTime='" + lagTime + '\'' +
            ", competitiveDifferentiators='" + competitiveDifferentiators + '\'' +
            ", discussionAnalytics='" + discussionAnalytics + '\'' +
            ", discussionCleanliness='" + discussionCleanliness + '\'' +
            ", sampleOrPanelBiases='" + sampleOrPanelBiases + '\'' +
            ", datasetSize=" + datasetSize +
            ", datasetNumberOfRows=" + datasetNumberOfRows +
            ", numberOfAnalystEmployees=" + numberOfAnalystEmployees +
            ", pointInTimeAccuracy=" + pointInTimeAccuracy +
            ", dataSourcesDetails='" + dataSourcesDetails + '\'' +
            ", dataGaps=" + dataGaps +
            ", includesOutliers=" + includesOutliers +
            ", normalized=" + normalized +
            ", duplicatesCleaned=" + duplicatesCleaned +
            ", investorClientBucket=" + investorClientBucket +
            ", subscriptionTimePeriod=" + subscriptionTimePeriod +
            ", yearFirstDataProductLaunched=" + yearFirstDataProductLaunched +
            // ", complianceScorePersonal='" + complianceScorePersonal + '\'' +
            // ", complianceScorePublic='" + complianceScorePublic + '\'' +
            ", monthlyPriceRange=" + monthlyPriceRange +
            ", subscriptionModel=" + subscriptionModel +
            ", minimumYearlyPrice='" + minimumYearlyPrice + '\'' +
            ", maximumYearlyPrice='" + maximumYearlyPrice + '\'' +
            ", numberOfDataSources='" + numberOfDataSources + '\'' +
            ", privacy='" + privacy + '\'' +
            ", biases=" + biases +
            ", outlierReasons=" + outlierReasons +
            ", dataGapsReasons=" + dataGapsReasons +
            ", organizationType=" + organizationType +
            ", managerType=" + managerType +
            ", strategies=" + strategies +
            ", researchStyles=" + researchStyles +
            ", investingTimeFrame=" + investingTimeFrame +
            ", equitiesMarketCap=" + equitiesMarketCap +
            ", equitiesStyle=" + equitiesStyle +
            ", identifiersAvailable=" + identifiersAvailable +
            ", accessOffered=" + accessOffered +
            ", dataProductTypes=" + dataProductTypes +
            ", dataTypes=" + dataTypes +
            ", languagesAvailable=" + languagesAvailable +
            ", paymentMethodsOffered=" + paymentMethodsOffered +
            ", complementaryProviders=" + complementaryProviders +
            ", competitors=" + competitors +
            ", yearExit=" + yearExit +
            ", financialPosition='" + financialPosition + '\'' +
            ", clientBase='" + clientBase + '\'' +
            ", phoneNumber='" + phoneNumber + '\'' +
            ", headcountNumber='" + headcountNumber + '\'' +
            ", headcountBucket=" + headcountBucket +
            // ", subsidiaryOrganization=" + subsidiaryOrganization +
            //    ", inactive='" + isInactive() + "'"+
            //  ", headcountDetails='" + headcountDetails + '\'' +
            ", completeness='" + completeness + '\'' +
            ", providerDataPartner=" + providerDataPartner +
            ", distributionDataPartner=" + distributionDataPartner +
            ", consumerDataPartner=" + consumerDataPartner +
            ", complementaryDataProviders=" + complementaryDataProviders +
            ", competitorProvider=" + competitorProvider +
            ", consent=" + consent +
           ", visibility=" + visibility +
           ", partnership=" + partnership +
            '}';

    }
}
