package com.ai.mp.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.annotations.JsonAdapter;

@Entity
@Table(name="data_resource")
@Cache(usage=CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="dataresources")
public class DataResources implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "record_id", length = 50)
    private String recordID;

    @Column(name = "name")
    private String name;

    @Size(max = 36)
    @Column(name = "description", length = 36)
    private String description;

    @Lob
    @Column(name = "notes")
    private String notes;

    @ManyToOne
    private LookupCode priority;

    @ManyToOne
    private LookupCode marketplaceStatus;

    @ManyToOne
    private LookupCode ieiStatus;

    @Size(max = 20)
    @Column(name = "user_permission")
    private String userPermission;
    
    public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	@Transient
    @JsonSerialize
    private boolean locked = false;

    @LastModifiedDate
    @Column(name = "last_updated_date")
    @JsonIgnore
    private Instant lastUpdatedDate = Instant.now();

    @Column(name = "created_time_formula")
    private LocalDateTime createdTimeFormula=LocalDateTime.now();

    @Column(name = "creator_user_id")
    private Long creatorUserId;

    @Column(name = "editor_user_id")
    private Long editorUserId;

    @ManyToOne(fetch=FetchType.LAZY)
    private DataProvider authorDataProvider;

    @ManyToOne(fetch=FetchType.EAGER)
    private DataOrganization authorOrganization;

    @ManyToOne
    private DataLinks link;

    @ManyToOne
    private DocumentsMetadata file;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name="data_resources_workflows",joinColumns=@JoinColumn(name="resource_id",referencedColumnName="id"),
        inverseJoinColumns=@JoinColumn(name="workflow_id",referencedColumnName="id"))
    private Set<LookupCode> workflows;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name="data_resources_purposes",joinColumns=@JoinColumn(name="resource_id",referencedColumnName="id"),
        inverseJoinColumns=@JoinColumn(name="purpose_id",referencedColumnName="id"))
    private Set<DataResourcePurpose> purposes;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name="data_resources_purpose_type",joinColumns=@JoinColumn(name="resource_id",referencedColumnName="id"),
        inverseJoinColumns=@JoinColumn(name="purpose_type_id",referencedColumnName="id"))
    private Set<DataResourcePurposeType> purposeTypes;


    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name="data_resources_topic",joinColumns=@JoinColumn(name="resource_id",referencedColumnName="id"),
        inverseJoinColumns=@JoinColumn(name="topic_id",referencedColumnName="id"))
    private Set<DataTopic> topics;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "data_resources_research_methods",
        joinColumns = @JoinColumn(name="resource_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="research_method_id ", referencedColumnName="id"))
    private Set<LookupCode> researchMethodsCompleted  = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_resources_relevant_organization",
        joinColumns = @JoinColumn(name="resource_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="organization_id  ", referencedColumnName="id"))
    private Set<DataOrganization> relavantOrganizations  = new HashSet<>();


    @ManyToMany(fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_resources_data_providers",
        joinColumns = @JoinColumn(name="resource_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="provider_id  ", referencedColumnName="id"))
    private Set<DataProvider> relavantDataProviders  = new HashSet<>();

    @CreatedDate
    @Column(name = "web_created_date")
    @JsonIgnore
    private Instant webCreatedDate = Instant.now();


    @LastModifiedDate
    @Column(name = "web_updated_date")
    @JsonIgnore
    private Instant webUpdateDate = Instant.now();


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public LookupCode getPriority() {
        return priority;
    }

    public void setPriority(LookupCode priority) {
        this.priority = priority;
    }

    public LookupCode getMarketplaceStatus() {
        return marketplaceStatus;
    }

    public void setMarketplaceStatus(LookupCode marketplaceStatus) {
        this.marketplaceStatus = marketplaceStatus;
    }

    public LookupCode getIeiStatus() {
        return ieiStatus;
    }

    public void setIeiStatus(LookupCode ieiStatus) {
        this.ieiStatus = ieiStatus;
    }

    public String getUserPermission() {
        return userPermission;
    }

    public void setUserPermission(String userPermission) {
        this.userPermission = userPermission;
    }

    public Instant getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Instant lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }



    public LocalDateTime getCreatedTimeFormula() {
        return createdTimeFormula;
    }

    public void setCreatedTimeFormula(LocalDateTime createdTimeFormula) {
        this.createdTimeFormula = createdTimeFormula;
    }

    public Long getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(Long creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public Long getEditorUserId() {
        return editorUserId;
    }

    public void setEditorUserId(Long editorUserId) {
        this.editorUserId = editorUserId;
    }

    public DataProvider getAuthorDataProvider() {
        return authorDataProvider;
    }

    public void setAuthorDataProvider(DataProvider authorDataProvider) {
        this.authorDataProvider = authorDataProvider;
    }

    public DataOrganization getAuthorOrganization() {
        return authorOrganization;
    }

    public void setAuthorOrganization(DataOrganization authorOrganization) {
        this.authorOrganization = authorOrganization;
    }

    public DataLinks getLink() {
        return link;
    }

    public void setLink(DataLinks link) {
        this.link = link;
    }

    public DocumentsMetadata getFile() {
        return file;
    }

    public void setFile(DocumentsMetadata file) {
        this.file = file;
    }

    public Set<LookupCode> getWorkflows() {
        return workflows;
    }

    public void setWorkflows(Set<LookupCode> workflows) {
        this.workflows = workflows;
    }

    public Set<DataResourcePurpose> getPurposes() {
        return purposes;
    }

    public void setPurposes(Set<DataResourcePurpose> purposes) {
        this.purposes = purposes;
    }

    public Set<DataResourcePurposeType> getPurposeTypes() {
        return purposeTypes;
    }

    public void setPurposeTypes(Set<DataResourcePurposeType> purposeTypes) {
        this.purposeTypes = purposeTypes;
    }

    public Set<DataTopic> getTopics() {
        return topics;
    }

    public void setTopics(Set<DataTopic> topics) {
        this.topics = topics;
    }

    public Set<LookupCode> getResearchMethodsCompleted() {
        return researchMethodsCompleted;
    }

    public void setResearchMethodsCompleted(Set<LookupCode> researchMethodsCompleted) {
        this.researchMethodsCompleted = researchMethodsCompleted;
    }

    public Set<DataOrganization> getRelavantOrganizations() {
        return relavantOrganizations;
    }

    public void setRelavantOrganizations(Set<DataOrganization> relavantOrganizations) {
        this.relavantOrganizations = relavantOrganizations;
    }

    public Set<DataProvider> getRelavantDataProviders() {
        return relavantDataProviders;
    }

    public void setRelavantDataProviders(Set<DataProvider> relavantDataProviders) {
        this.relavantDataProviders = relavantDataProviders;
    }

    public Instant getWebCreatedDate() {
        return webCreatedDate;
    }

    public void setWebCreatedDate(Instant webCreatedDate) {
        this.webCreatedDate = webCreatedDate;
    }

    public Instant getWebUpdateDate() {
        return webUpdateDate;
    }

    public void setWebUpdateDate(Instant webUpdateDate) {
        this.webUpdateDate = webUpdateDate;
    }

    @Override
    public String toString() {
        return "DataResources{" +
            "id=" + id +
            ", recordID='" + recordID + '\'' +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", notes='" + notes + '\'' +
            ", priority=" + priority +
            ", marketplaceStatus=" + marketplaceStatus +
            ", ieiStatus=" + ieiStatus +
            ", userPermission='" + userPermission + '\'' +
            ", lastUpdatedDate=" + lastUpdatedDate +
            ", createdTimeFormula=" + createdTimeFormula +
            ", creatorUserId=" + creatorUserId +
            ", editorUserId=" + editorUserId +
            ", authorDataProvider=" + authorDataProvider +
            ", authorOrganization=" + authorOrganization +
            ", link=" + link +
            ", file=" + file +
            ", workflows=" + workflows +
            ", purposes=" + purposes +
            ", purposeTypes=" + purposeTypes +
            ", topics=" + topics +
            ", researchMethodsCompleted=" + researchMethodsCompleted +
            ", relavantOrganizations=" + relavantOrganizations +
            ", relavantDataProviders=" + relavantDataProviders +
            ", webCreatedDate=" + webCreatedDate +
            ", webUpdateDate=" + webUpdateDate +
             ", locked=" + locked +
            '}';
    }
}
