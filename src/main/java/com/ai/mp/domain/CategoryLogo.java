package com.ai.mp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.ai.mp.utils.ByteArrayToBase64TypeAdapter;
import com.google.gson.annotations.JsonAdapter;



@SuppressWarnings("serial")
@Entity
@Table(name = "data_category_logo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CategoryLogo implements Serializable {
	
	

	 
	
	private Long id;
	
	
	
	
	 public CategoryLogo(byte[] logo,String categoryRecordID) {
		this.logo = logo;
		this.categoryRecordID = categoryRecordID;
	}

	public CategoryLogo() {
	
	}
    
	 
    


	@JsonAdapter(ByteArrayToBase64TypeAdapter.class)
	 @Lob
	 @Column(name = "logo")
	private byte[] logo;
	
	
	 public boolean isDisabled() {
	        return disabled;
	    }

	    public void setDisabled(boolean disabled) {
	        this.disabled = disabled;
	    }

	private boolean disabled=false;
	
	
	
	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryRecordID() {
		return categoryRecordID;
	}

	public void setCategoryRecordID(String categoryRecordID) {
		this.categoryRecordID = categoryRecordID;
	}



	 @Id
	 
	 @Column(name = "category_record_id")
	private String categoryRecordID;
	public byte[] getLogo() {
		return logo;
	}
	 public void setLogo(byte[] logo) {
	        this.logo = logo;
	    }
	
	
   	
}
