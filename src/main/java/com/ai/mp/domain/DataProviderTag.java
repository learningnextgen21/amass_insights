package com.ai.mp.domain;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.ai.mp.utils.SetToBeanConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;
import com.ai.mp.utils.BasicBean;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Objects;

/**
 * A DataProviderTag.
 */
@Entity
@Table(name = "data_provider_tag")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dataprovidertag")
public class DataProviderTag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "record_id", length = 50)
    private String recordID;

    @NotNull
    @Size(max = 64)
    @Column(name = "provider_tag", length = 64, nullable = false)
    private String providerTag;

    public int getSortorder() {
        return sortOrder;
    }

    public void setSortorder(int sortorder) {
        this.sortOrder = sortorder;
    }

    private int sortOrder;

    @Lob
    @Column(name = "explanation")
    private String explanation;

    @Column(name = "inactive")
    private Boolean inactive;

    @Column(name = "receive_notifications")
    private Boolean receiveNotifications;

    @Column(name = "privacy")
    private String privacy;

    @ManyToOne
//    @JsonIgnore
    private User byUser;

    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "type")
    private String type;
    

    @JsonAdapter(DateToStringTypeAdapter.class)
    //@NotNull
    @Column(name = "created_date")
    private LocalDate createdDate;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "updated_date")
    private Instant updatedDate;

    @ManyToMany(mappedBy = "dataProviderTags")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerTags = new HashSet<>();
    
    @Transient
    @JsonSerialize
    private List<Long> interested;

    @Transient
    @JsonSerialize
    private List<Long> notInterested;
    
    @Transient
    @JsonSerialize
    private List<Long> inUse;
    
    @Transient
    @JsonSerialize
    private List<Long> testing;
	public List<Long> getTesting() {
		return testing;
	}

	public void setTesting(List<Long> testing) {
		this.testing = testing;
	}

	public List<Long> getInUse() {
		return inUse;
	}

	public void setInUse(List<Long> inUse) {
		this.inUse = inUse;
	}

	public List<Long> getInterested() {
		return interested;
	}

	public List<Long> getNotInterested() {
		return notInterested;
	}

	public void setNotInterested(List<Long> notInterested) {
		this.notInterested = notInterested;
	}

	public void setInterested(List<Long> interested) {
		this.interested = interested;
	}

	public Set<DataProvider> getProviderTags() {
        return providerTags;
    }


    public void setProviderTags(Set<DataProvider> dataProviders) {
        this.providerTags = dataProviders;
    }

    @Column(name = "tag_providers", insertable = false, updatable = false)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> tagProviders = new ArrayList<BasicBean>();


    /*@ManyToMany(mappedBy = "dataProviderTags")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> dataTags = new HashSet<>();*/


   /* public DataProviderTag addTagProviders(DataProvider dataProvider) {
        this.tagProviders.add(dataProvider);
        dataProvider.getTagsProvider().add(this);
        return this;
    }

    public DataProviderTag removeCategory(DataProvider dataProvider) {
        this.tagProviders.remove(dataProvider);
        dataProvider.getTagsProvider().remove(this);
        return this;
    }*/



    public Boolean getInactive() {
        return inactive;
    }

    public List<BasicBean> getTagProviders() {
		return tagProviders;
	}

	public void setTagProviders(List<BasicBean> tagProviders) {
		this.tagProviders = tagProviders;
	}

	public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }

    public Boolean getReceiveNotifications() {
        return receiveNotifications;
    }

    public void setReceiveNotifications(Boolean receiveNotifications) {
        this.receiveNotifications = receiveNotifications;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public User getByUser() {
        return byUser;
    }

    public void setByUser(User byUser) {
        this.byUser = byUser;
    }

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public DataProviderTag recordID(String recordID) {
        this.recordID = recordID;
        return this;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getProviderTag() {
        return providerTag;
    }

    public DataProviderTag providerTag(String providerTag) {
        this.providerTag = providerTag;
        return this;
    }

    public void setProviderTag(String providerTag) {
        this.providerTag = providerTag;
    }

    public String getExplanation() {
        return explanation;
    }

    public DataProviderTag explanation(String explanation) {
        this.explanation = explanation;
        return this;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }



    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public DataProviderTag createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public DataProviderTag updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }




    /********************************For Providers form Page*********************/
    /*public Set<DataProvider> getDataTags() {
        return dataTags;
    }

    public DataProviderTag dataTags(Set<DataProvider> dataProviders) {
        this.dataTags = dataProviders;
        return this;
    }

    public DataProviderTag addDataTag(DataProvider dataProvider) {
        this.dataTags.add(dataProvider);
        //dataProvider.getFeatures().add(this);
        return this;
    }

    public DataProviderTag removeDataTag(DataProvider dataProvider) {
        this.dataTags.remove(dataProvider);
        dataProvider.getDataProviderTags().remove(this);
        return this;
    }

    public void setDataTags(Set<DataProvider> dataProviders) {
        this.dataTags = dataProviders;
    }*/
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataProviderTag dataProviderTag = (DataProviderTag) o;
        if (dataProviderTag.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataProviderTag.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataProviderTag{" +
            "id=" + getId() +
            ", recordID='" + getRecordID() + "'" +
            ", providerTag='" + getProviderTag() + "'" +
            ", explanation='" + getExplanation() + "'" +
            ", inactive='" + getInactive() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", byUser='" + getByUser() + "'" +
            ", receiveNotifications='" + getReceiveNotifications() + "'" +
            ", privacy='" + getPrivacy() + "'" +
            ", providerTags'" + getProviderTags()+"'"+
            ", tagProviders'" + getTagProviders()+"'"+
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
