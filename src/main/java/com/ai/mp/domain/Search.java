package com.ai.mp.domain;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import com.ai.mp.utils.SetToBeanConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "search")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "search")
public class Search {

	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
    @Column(name = "search_terms")
	private String searchTerms;
	
    @Column(name="elasticsearch_query")
    private String elasticSearch;
    
    @JoinColumn(name = "user_id")
    @ManyToOne
	private User user;
	
    @CreatedDate
    @Column(name = "created_date")
    @JsonIgnore
    private Instant CreatedDate = Instant.now();


	public Instant getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(Instant createdDate) {
		CreatedDate = createdDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSearchTerms() {
		return searchTerms;
	}

	public void setSearchTerms(String searchTerms) {
		this.searchTerms = searchTerms;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@Column(name="saved")
	private boolean saved;
	
	
	@Override
    public String toString() {
        return "DataInvestor{" +
            "id=" + getId() +
            ", searchTerms='" + getSearchTerms() + "'" +
            ", user='" + getUser() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", elasticSearch='" + getElasticSearch() + "'" +
            ", saved='" + isSaved() + "'" +
            "}";
 }

	public String getElasticSearch() {
		return elasticSearch;
	}

	public void setElasticSearch(String elasticSearch) {
		this.elasticSearch = elasticSearch;
	}

	public boolean isSaved() {
		return saved;
	}

	public void setSaved(boolean saved) {
		this.saved = saved;
	}

}
