package com.ai.mp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AdminRole.
 */
@Entity
@Table(name = "mp_admin_role")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "adminrole")
public class AdminRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Max(value = 11)
    @Column(name = "user_id", nullable = false)
    private Integer userID;

    @NotNull
    @Size(max = 20)
    @Column(name = "admin_role", length = 20, nullable = false)
    private String adminRole;

    @NotNull
    @Lob
    @Column(name = "description", nullable = false)
    private String description;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUserID() {
        return userID;
    }

    public AdminRole userID(Integer userID) {
        this.userID = userID;
        return this;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getAdminRole() {
        return adminRole;
    }

    public AdminRole adminRole(String adminRole) {
        this.adminRole = adminRole;
        return this;
    }

    public void setAdminRole(String adminRole) {
        this.adminRole = adminRole;
    }

    public String getDescription() {
        return description;
    }

    public AdminRole description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AdminRole adminRole = (AdminRole) o;
        if (adminRole.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), adminRole.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdminRole{" +
            "id=" + getId() +
            ", userID='" + getUserID() + "'" +
            ", adminRole='" + getAdminRole() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
