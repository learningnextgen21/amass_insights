package com.ai.mp.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;

@Entity
@Table(name = "user_checkout")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "usercheckout")
public class UserCheckout implements Serializable{

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="subscription")
    private String subscription;

    @OneToOne
    private User user;


    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "created_date")
    private LocalDate createdDate = LocalDate.now();

    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Column(name="is_subscribed")
    private boolean subscribed;

    private Integer amount;

    @Column(name="payment")
    private boolean payment;

    public boolean isPayment() {
        return payment;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public void setPayment(boolean payment) {
        this.payment = payment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscription(String subscription) {
        this.subscription = subscription;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }
    @Override
    public String toString() {
        return "UserCheckout{" +
            "id=" + getId() +
            ", subscription='" + getSubscription() + "'" +
            ", user='" + getUser() + "'" +
            ", subscribed='" + isSubscribed() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", amount='" + getAmount() + "'" +
            ", expirationDate='" + getExpirationDate() + "'" +
            "}";
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getSubscription() {
        return subscription;
    }
}
