package com.ai.mp.domain;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A UserDataCategory.
 */
@Entity
@Table(name = "mp_user_data_category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "userdatacategory")
@DiscriminatorColumn
public class UserDataCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull

    @Column(name = "user_id", nullable = false)
    private Long userID;



    @Column(name = "data_category_id", nullable = false)
    private Long dataCategoryID;


    @Max(value = 2)
    @Column(name = "get_notifications", nullable = false)
    private Integer getNotifications;


   /* public String getCategoryRecordID() {
        return categoryRecordID;
    }

    public void setCategoryRecordID(String categoryRecordID) {
        this.categoryRecordID = categoryRecordID;
    }

    @Column(name = "category_record_id")
    private String categoryRecordID;*/



    @Size(max = 20)
    @Column(name = "notification_type", length = 20, nullable = false)
    private String notificationType;


    public String getDeletionKey() {
        return deletionKey;
    }

    public void setDeletionKey(String deletionKey) {
        this.deletionKey = deletionKey;
    }

    private String deletionKey;

    /*@NotNull*/
    @Column(name = "interest_type")
    private Integer interestType;

    /* @NotNull
     @Max(value = 2)
     @Column(name = "inactive", nullable = false)
     private Integer inactive;*/
  /* @NotNull
    @Max(value = 2)
    @Column(name = "inactive", nullable = false)*/
    private boolean inactive;

    @NotNull
    @Column(name = "created_date", nullable = false)
    private LocalDateTime createdDate;

    @Column(name = "updated_date")
    private Instant updatedDate;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserID() {
        return userID;
    }

    public UserDataCategory userID(Long userID) {
        this.userID = userID;
        return this;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public Long getDataCategoryID() {
        return dataCategoryID;
    }

    public UserDataCategory dataCategoryID(Long dataCategoryID) {
        this.dataCategoryID = dataCategoryID;
        return this;
    }

    public void setDataCategoryID(Long dataCategoryID) {
        this.dataCategoryID = dataCategoryID;
    }

    public Integer getGetNotifications() {
        return getNotifications;
    }

    public UserDataCategory getNotifications(Integer getNotifications) {
        this.getNotifications = getNotifications;
        return this;
    }

    public void setGetNotifications(Integer getNotifications) {
        this.getNotifications = getNotifications;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public UserDataCategory notificationType(String notificationType) {
        this.notificationType = notificationType;
        return this;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public Integer getInterestType() {
        return interestType;
    }

    public UserDataCategory interestType(int Integer) {
        this.interestType = interestType;
        return this;
    }

    public void setInterestType(Integer interestType) {
        this.interestType = interestType;
    }

    public boolean getInactive() {
        return inactive;
    }

    public UserDataCategory inactive(boolean inactive) {
        this.inactive = inactive;
        return this;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public UserDataCategory createdDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public UserDataCategory updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }





    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserDataCategory amassUserDataCategory = (UserDataCategory) o;
        if (amassUserDataCategory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), amassUserDataCategory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserDataCategory{" +
            "id=" + getId() +
            ", userID='" + getUserID() + "'" +
            ", dataCategoryID='" + getDataCategoryID() + "'" +
            ", getNotifications='" + getGetNotifications() + "'" +
            ", notificationType='" + getNotificationType() + "'" +
            ", interestType='" + getInterestType() + "'" +
            ", inactive='" + getInactive() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +

            "}";
    }
}
