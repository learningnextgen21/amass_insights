package com.ai.mp.domain;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DataIndustry.
 */
@Entity
@Table(name = "data_industry")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dataindustry")
public class DataIndustry implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "record_id", length = 50)
    private String recordID;

    @NotNull
    @Size(max = 64)
    @Column(name = "data_industry", length = 64, nullable = false)
    private String dataIndustry;

    @Lob
    @Column(name = "explanation")
    private String explanation;

    public int getSortorder() {
        return sortOrder;
    }

    public void setSortorder(int sortorder) {
        this.sortOrder = sortorder;
    }

    private int sortOrder;

    public int getRelevantDataIndustryProviderCount() {
        return relevantDataIndustryProviderCount;
    }

    public void setRelevantDataIndustryProviderCount(int relevantDataIndustryProviderCount) {
        this.relevantDataIndustryProviderCount = relevantDataIndustryProviderCount;
    }

    public int getMainDataIndustryProviderCount() {
        return mainDataIndustryProviderCount;
    }

    public void setMainDataIndustryProviderCount(int mainDataIndustryProviderCount) {
        this.mainDataIndustryProviderCount = mainDataIndustryProviderCount;
    }

    private int relevantDataIndustryProviderCount;

    private int eventIndustryCount;

    private int mainDataIndustryProviderCount;

    public int getEventIndustryCount() {
        return eventIndustryCount;
    }

    public void setEventIndustryCount(int eventIndustryCount) {
        this.eventIndustryCount = eventIndustryCount;
    }

    @Lob
    @Column(name = "data_tools")
    private String dataTools;

    public String getDataTools() {
		return dataTools;
	}

	public void setDataTools(String dataTools) {
		this.dataTools = dataTools;
	}

	@NotNull
    @Max(value = 2)
    @Column(name = "inactive", nullable = false)
    private Integer inactive;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @NotNull
    @Column(name = "created_date", nullable = false)
    private LocalDate createdDate;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "updated_date")
    private Instant updatedDate;

    @OneToMany(mappedBy = "mainDataIndustry")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> dataProviders = new HashSet<>();

    @ManyToMany(mappedBy = "industries")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> associatedProviders = new HashSet<>();

    @ManyToMany(mappedBy = "providerIndustries")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> dataProviderIndustries = new HashSet<>();

    @CreatedDate
    @Column(name = "web_created_date")
    @JsonIgnore
    private Instant webCreatedDate = Instant.now();

    @LastModifiedDate
    @Column(name = "web_updated_date")
    @JsonIgnore
    private Instant webUpdateDate = Instant.now();

    public Instant getWebCreatedDate() {
		return webCreatedDate;
	}

	public void setWebCreatedDate(Instant webCreatedDate) {
		this.webCreatedDate = webCreatedDate;
	}

	public Instant getWebUpdateDate() {
		return webUpdateDate;
	}

	public void setWebUpdateDate(Instant webUpdateDate) {
		this.webUpdateDate = webUpdateDate;
	}

	public Long getCreatorUserId() {
		return creatorUserId;
	}

	public void setCreatorUserId(Long creatorUserId) {
		this.creatorUserId = creatorUserId;
	}

	public Long getEditorUserId() {
		return editorUserId;
	}

	public void setEditorUserId(Long editorUserId) {
		this.editorUserId = editorUserId;
	}

	//  @NotNull
    @Column(name = "creator_user_id")
    private Long creatorUserId;

    // @NotNull
    @Column(name = "editor_user_id")
    private Long editorUserId;


    @Column(name="hashtag")
    private String hashtag;

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }
    
    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public DataIndustry recordID(String recordID) {
        this.recordID = recordID;
        return this;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getDataIndustry() {
        return dataIndustry;
    }

    public DataIndustry dataIndustry(String dataIndustry) {
        this.dataIndustry = dataIndustry;
        return this;
    }

    public void setDataIndustry(String dataIndustry) {
        this.dataIndustry = dataIndustry;
    }

    public String getExplanation() {
        return explanation;
    }

    public DataIndustry explanation(String explanation) {
        this.explanation = explanation;
        return this;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public Integer getInactive() {
        return inactive;
    }

    public DataIndustry inactive(Integer inactive) {
        this.inactive = inactive;
        return this;
    }

    public void setInactive(Integer inactive) {
        this.inactive = inactive;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public DataIndustry createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public DataIndustry updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Set<DataProvider> getDataProviders() {
        return dataProviders;
    }

    public DataIndustry dataProviders(Set<DataProvider> dataProviders) {
        this.dataProviders = dataProviders;
        return this;
    }

    public DataIndustry addDataProvider(DataProvider dataProvider) {
        this.dataProviders.add(dataProvider);
        dataProvider.setMainDataIndustry(this);
        return this;
    }

    public DataIndustry removeDataProvider(DataProvider dataProvider) {
        this.dataProviders.remove(dataProvider);
        dataProvider.setMainDataIndustry(null);
        return this;
    }

    public void setDataProviders(Set<DataProvider> dataProviders) {
        this.dataProviders = dataProviders;
    }

    public Set<DataProvider> getAssociatedProviders() {
        return associatedProviders;
    }

    public DataIndustry associatedProviders(Set<DataProvider> dataProviders) {
        this.associatedProviders = dataProviders;
        return this;
    }

    public DataIndustry addAssociatedProvider(DataProvider dataProvider) {
        this.associatedProviders.add(dataProvider);
//        dataProvider.getIndustries().add(this);
        return this;
    }

    public DataIndustry removeAssociatedProvider(DataProvider dataProvider) {
        this.associatedProviders.remove(dataProvider);
        dataProvider.getIndustries().remove(this);
        return this;
    }

    public Set<DataProvider> getProviderIndustries() {
        return dataProviderIndustries;
    }

    public DataIndustry dataProviderIndustries(Set<DataProvider> dataProviders) {
        this.dataProviderIndustries = dataProviders;
        return this;
    }

    public DataIndustry addDataProviderIndustries(DataProvider dataProvider) {
        this.dataProviderIndustries.add(dataProvider);
        dataProvider.getProviderIndustries().add(this);
        return this;
    }

    public DataIndustry removeDataProviderIndustries(DataProvider dataProvider) {
        this.dataProviderIndustries.remove(dataProvider);
        dataProvider.getProviderIndustries().remove(this);
        return this;
    }

    public void setDataProviderIndustries(Set<DataProvider> dataProviders) {
        this.dataProviderIndustries = dataProviders;
    }


    public void setAssociatedProviders(Set<DataProvider> dataProviders) {
        this.associatedProviders = dataProviders;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataIndustry dataIndustry = (DataIndustry) o;
        if (dataIndustry.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataIndustry.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataIndustry{" +
            "id=" + getId() +
            ", recordID='" + getRecordID() + "'" +
            ", dataIndustry='" + getDataIndustry() + "'" +
            ", explanation='" + getExplanation() + "'" +
            ", inactive='" + getInactive() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", webCreatedDate='" + getWebCreatedDate() + "'" +
            ", webUpdateDate='" + getWebUpdateDate() + "'" +
            ", creatorUserId='" + getCreatorUserId() + "'" +
            ", editorUserId='" + getEditorUserId() + "'" +
            ", hashtag='" + getHashtag() + "'" +
            "}";
    }
}
