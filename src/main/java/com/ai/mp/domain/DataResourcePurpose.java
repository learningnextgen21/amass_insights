package com.ai.mp.domain;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

@Entity
@Document(indexName = "dataresourcepurpose")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DataResourcePurpose implements Serializable {
    private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "record_id")
  private String recordID;
  private String purpose;
  private String explanation;
  private Long sortOrder;
    @JsonAdapter(DateToStringTypeAdapter.class)
    @NotNull
    @Column(name = "created_date", nullable = false)
    private LocalDate createdDate;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "updated_date")
    private Instant updatedDate;


    public int getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(int articleCount) {
        this.articleCount = articleCount;
    }

    private int articleCount;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getRecordID() {
    return recordID;
  }

  public void setRecordID(String record_id) {
    this.recordID = record_id;
  }

  public String getPurpose() {
    return purpose;
  }

  public void setPurpose(String purpose) {
    this.purpose = purpose;
  }

  public String getExplanation() {
    return explanation;
  }

  public void setExplanation(String explanation) {
    this.explanation = explanation;
  }

  public Long getSortorder() {
    return sortOrder;
  }

  public void setSortorder(Long sort_order) {
    this.sortOrder = sort_order;
  }

  public LocalDate getCreated_date() {
    return createdDate;
  }

  public void setCreatedDate(LocalDate createdDate) {
    this.createdDate = createdDate;
  }
}
