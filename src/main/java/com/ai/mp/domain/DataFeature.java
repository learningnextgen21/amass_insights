package com.ai.mp.domain;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DataFeature.
 */
@Entity
@Table(name = "data_feature")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "datafeature")
public class DataFeature implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "record_id", length = 50)
    private String recordID;

    @NotNull
    @Size(max = 100)
    @Column(name = "data_feature", length = 100, nullable = false)
    private String dataFeature;

    public int getSortorder() {
        return sortOrder;
    }

    public void setSortorder(int sortorder) {
        this.sortOrder = sortorder;
    }

    private int sortOrder;

    public int getProviderCount() {
        return providerCount;
    }

    public void setProviderCount(int providerCount) {
        this.providerCount = providerCount;
    }

    private int providerCount;

    @Lob
    @Column(name = "explanation")
    private String explanation;

    @NotNull
    @Max(value = 2)
    @Column(name = "inactive", nullable = false)
    private Integer inactive;

  //  @JsonIgnore
    @JsonAdapter(DateToStringTypeAdapter.class)
    @NotNull
    @Column(name = "created_date", nullable = false)
    private LocalDate createdDate;

   // @JsonIgnore
    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "updated_date")
    private Instant updatedDate;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_feature_category",
               joinColumns = @JoinColumn(name="data_features_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="categories_id", referencedColumnName="id"))
    private Set<DataCategory> categories = new HashSet<>();

    @ManyToMany(mappedBy = "features")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> featureProviders = new HashSet<>();


    @ManyToMany(mappedBy = "dataProviderFeatures")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerFeatures = new HashSet<>();
    
    @CreatedDate
    @Column(name = "web_created_date")
    @JsonIgnore
    private Instant webCreatedDate = Instant.now();

    @LastModifiedDate
    @Column(name = "web_updated_date")
    @JsonIgnore
    private Instant webUpdateDate = Instant.now();

    public Instant getWebCreatedDate() {
		return webCreatedDate;
	}

	public void setWebCreatedDate(Instant webCreatedDate) {
		this.webCreatedDate = webCreatedDate;
	}

	public Instant getWebUpdateDate() {
		return webUpdateDate;
	}

	public void setWebUpdateDate(Instant webUpdateDate) {
		this.webUpdateDate = webUpdateDate;
	}

	public Long getCreatorUserId() {
		return creatorUserId;
	}

	public void setCreatorUserId(Long creatorUserId) {
		this.creatorUserId = creatorUserId;
	}

	public Long getEditorUserId() {
		return editorUserId;
	}

	public void setEditorUserId(Long editorUserId) {
		this.editorUserId = editorUserId;
	}

	//  @NotNull
    @Column(name = "creator_user_id")
    private Long creatorUserId;

    // @NotNull
    @Column(name = "editor_user_id")
    private Long editorUserId;
    

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public DataFeature recordID(String recordID) {
        this.recordID = recordID;
        return this;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getDataFeature() {
        return dataFeature;
    }

    public DataFeature dataFeature(String dataFeature) {
        this.dataFeature = dataFeature;
        return this;
    }

    public void setDataFeature(String dataFeature) {
        this.dataFeature = dataFeature;
    }

    public String getExplanation() {
        return explanation;
    }

    public DataFeature explanation(String explanation) {
        this.explanation = explanation;
        return this;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public Integer getInactive() {
        return inactive;
    }

    public DataFeature inactive(Integer inactive) {
        this.inactive = inactive;
        return this;
    }

    public void setInactive(Integer inactive) {
        this.inactive = inactive;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public DataFeature createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public DataFeature updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Set<DataCategory> getCategories() {
        return categories;
    }

    public DataFeature categories(Set<DataCategory> dataCategories) {
        this.categories = dataCategories;
        return this;
    }

    public DataFeature addCategory(DataCategory dataCategory) {
        this.categories.add(dataCategory);
        dataCategory.getCategoryFeatures().add(this);
        return this;
    }

    public DataFeature removeCategory(DataCategory dataCategory) {
        this.categories.remove(dataCategory);
        dataCategory.getCategoryFeatures().remove(this);
        return this;
    }

    public void setCategories(Set<DataCategory> dataCategories) {
        this.categories = dataCategories;
    }

    public Set<DataProvider> getFeatureProviders() {
        return featureProviders;
    }

    public DataFeature featureProviders(Set<DataProvider> dataProviders) {
        this.featureProviders = dataProviders;
        return this;
    }

    public DataFeature addFeatureProvider(DataProvider dataProvider) {
        this.featureProviders.add(dataProvider);
        //dataProvider.getFeatures().add(this);
        return this;
    }

    public DataFeature removeFeatureProvider(DataProvider dataProvider) {
        this.featureProviders.remove(dataProvider);
        dataProvider.getFeatures().remove(this);
        return this;
    }

    public void setFeatureProviders(Set<DataProvider> dataProviders) {
        this.featureProviders = dataProviders;
    }


    /********************************For Providers form Page*********************/
    public Set<DataProvider> getProviderFeatures() {
        return providerFeatures;
    }

    public DataFeature providerFeatures(Set<DataProvider> dataProviders) {
        this.providerFeatures = dataProviders;
        return this;
    }

    public DataFeature addProviderFeature(DataProvider dataProvider) {
        this.providerFeatures.add(dataProvider);
        //dataProvider.getFeatures().add(this);
        return this;
    }

    public DataFeature removeProviderFeature(DataProvider dataProvider) {
        this.providerFeatures.remove(dataProvider);
        dataProvider.getDataProviderFeatures().remove(this);
        return this;
    }

    public void setProviderFeatures(Set<DataProvider> dataProviders) {
        this.providerFeatures = dataProviders;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataFeature dataFeature = (DataFeature) o;
        if (dataFeature.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataFeature.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataFeature{" +
            "id=" + getId() +
            ", recordID='" + getRecordID() + "'" +
            ", dataFeature='" + getDataFeature() + "'" +
            ", explanation='" + getExplanation() + "'" +
            ", inactive='" + getInactive() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", webCreatedDate='" + getWebCreatedDate() + "'" +
            ", webUpdateDate='" + getWebUpdateDate() + "'" +
            ", creatorUserId='" + getCreatorUserId() + "'" +
            ", editorUserId='" + getEditorUserId() + "'" +
            "}";
    }
}
