package com.ai.mp.domain;

import com.ai.core.Constants;
import com.ai.mp.utils.BasicBean;
import com.ai.mp.utils.DateToStringTypeAdapter;
import com.ai.mp.utils.SetToBeanConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.SQLInsert;
import org.hibernate.annotations.WhereJoinTable;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * A DataOrganization.
 */
@Entity
@Table(name = "data_organization")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dataorganization")
public class DataOrganization implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //  @Size(max = 50)
    @Column(name = "record_id")
    private String recordID;

    //  @Size(max = 100)
    @Column(name = "name")
    private String name;

    //  @Size(max = 256)
 /*   @Column(name = "website")
    private String website;*/

    @Lob
    @Column(name = "description")
    private String description;

    //  @Size(max = 50)
    @Column(name = "city")
    private String city;

    //  @Size(max = 50)
    @Column(name = "state")
    private String state;

    // @Size(max = 50)
    @Column(name = "country")
    private String country;

    //  @Size(max = 50)
    @Column(name = "zip_code")
    private String zipCode;

    // @Size(max = 100)
   /* @Column(name = "facebook")
    private String facebook;
*/
    // @Size(max = 50)
   /* @Column(name = "github")
    private String github;*/

    // @Size(max = 64)
    /*@Column(name = "twitter")
    private String twitter;*/

    // @Max(value = 11)
    @Column(name = "year_founded")
    private Integer yearFounded;

    //@Size(max = 64)
    @Column(name = "headcount")
    private String headCount;

    //@Size(max = 128)
    @Column(name = "financial_position")
    private String financialPosition;

     //@Size(max = 50)
    @Column(name = "client_base")
    private String clientBase;

  /*  @Lob
    @Column(name = "notes")
    private String notes;*/

    /*  @Max(value = 2)
      @Column(name = "inactive")
      private Integer inactive;

      @Max(value = 2)
      @Column(name = "live")
      private Integer live;
  */
    @Column(name = "year_exit")
    private Integer yearExit;

   /* @Column(name = "phone_number")
    private String phoneNumber;*/

    @Column(name = "headcount_number")
    private String headcountNumber;

    @ManyToOne
    private LookupCode headcountBucket;

    public LookupCode getHeadcountBucket() {
        return headcountBucket;
    }
    public DataOrganization headcountBucket(LookupCode headcountBucket)
    {
        this.headcountBucket=headcountBucket;
        return this;
    }
    public void setHeadcountBucket(LookupCode headcountBucket) {
        this.headcountBucket = headcountBucket;
    }




    @JsonAdapter(DateToStringTypeAdapter.class)
    //@NotNull
    @Column(name = "created_date")
    private LocalDate createdDate;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @Column(name = "updateddate")
    private LocalDate updateddate;

    @ManyToOne
    private DataOrganization parentOrganization;

   /* @OneToMany(mappedBy = "authorOrganization")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataArticle> dataArticles = new HashSet<>();*/

   /* @OneToMany(mappedBy = "ownerOrganization")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> owners = new HashSet<>();
*/
    /*@OneToMany(mappedBy = "parentOrganization")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataOrganization> dataOrganizations = new HashSet<>();
*/
  /*  @ManyToMany(mappedBy = "organizationOrganizer")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataEvent> organizationEvent = new HashSet<>();

    @ManyToMany(mappedBy = "sponsorOrganization")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataEvent> sponsorOrganizationEvent = new HashSet<>();*/


    @ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_subsidiary_organization",
        joinColumns = @JoinColumn(name="organization_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="subsidiary_id", referencedColumnName="id"))
    private Set<DataOrganization> subsidiaryOrganization = new HashSet<>();

    @ManyToOne
    private LookupCode marketplaceStatus;

  /*  @ManyToOne
    private LookupCode researchMethodsCompleted;
    */

   /* @Column(name = "research_methods", insertable = false, updatable = false)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> researchMethods = new ArrayList<BasicBean>();*/

    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_organization_research_methods",
        joinColumns = @JoinColumn(name="organization_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="research_methods_id", referencedColumnName="id"))
    private Set<LookupCode> researchMethodsCompleted  = new HashSet<>();



    public Set<LookupCode> getResearchMethodsCompleted() {
        return researchMethodsCompleted;
    }
    public void setResearchMethodsCompleted(Set<LookupCode> researchMethodsCompleted) {
        this.researchMethodsCompleted = researchMethodsCompleted;
    }


	/* public DataOrganization dataOrganizationResearchMethods(Set<LookupCode> lookupCodes) {
	        this.researchMethodsCompleted = lookupCodes;
	        return this;
	    }
	    public DataOrganization adddataOrganizationResearchMethods(LookupCode lookupCode) {
	        this.researchMethodsCompleted.add(lookupCode);
	        lookupCode.getResearchMethodsOrganization().add(this);
	        return this;
	    }
	    public DataOrganization removedataOrganizationResearchMethods(LookupCode lookupCode) {
	        this.researchMethodsCompleted.remove(lookupCode);
	        lookupCode.getResearchMethodsOrganization().remove(this);
	        return this;
	    }
 */




    /*	public List<BasicBean> getResearchMethods() {
            return researchMethods;
        }
        public void setResearchMethods(List<BasicBean> researchMethods) {
            this.researchMethods = researchMethods;
        }*/
    public LookupCode getMarketplaceStatus() {
        return marketplaceStatus;
    }
    public void setMarketplaceStatus(LookupCode marketplaceStatus) {
        this.marketplaceStatus = marketplaceStatus;
    }
    /*public LookupCode getResearchMethodsCompleted() {
        return researchMethodsCompleted;
    }
    public void setResearchMethodsCompleted(LookupCode researchMethodsCompleted) {
        this.researchMethodsCompleted = researchMethodsCompleted;
    }*/
    public LookupCode getIeiStatus() {
        return ieiStatus;
    }
    public void setIeiStatus(LookupCode ieiStatus) {
        this.ieiStatus = ieiStatus;
    }
    public Instant getLastUpdatedDate() {
        return lastUpdatedDate;
    }
    public void setLastUpdatedDate(Instant lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }



    @ManyToOne
    private LookupCode ieiStatus;

    @LastModifiedDate
    @Column(name = "last_updated_date")
    @JsonIgnore
    private Instant lastUpdatedDate = Instant.now();


    public Set<DataOrganization> getSubsidiaryOrganization() {
        return subsidiaryOrganization;
    }

    public void setSubsidiaryOrganization(Set<DataOrganization> subsidiaryOrganization) {
        this.subsidiaryOrganization = subsidiaryOrganization;
    }

    public Integer getYearExit() {
        return yearExit;
    }

    public void setYearExit(Integer yearExit) {
        this.yearExit = yearExit;
    }

    /* public String getPhoneNumber() {
         return phoneNumber;
     }

     public void setPhoneNumber(String phoneNumber) {
         this.phoneNumber = phoneNumber;
     }
 */
    public String getHeadcountNumber() {
        return headcountNumber;
    }

    public void setHeadcountNumber(String headcountNumber) {
        this.headcountNumber = headcountNumber;
    }

    @CreatedDate
    @Column(name = "web_created_date")
    @JsonIgnore
    private Instant webCreatedDate = Instant.now();

    @LastModifiedDate
    @Column(name = "web_updated_date")
    @JsonIgnore
    private Instant webUpdateDate = Instant.now();

    //  @NotNull
    @Column(name = "creator_user_id")
    private Long creatorUserId;

    // @NotNull
    @Column(name = "editor_user_id")
    private Long editorUserId;

    public Instant getWebCreatedDate() {
        return webCreatedDate;
    }

    public void setWebCreatedDate(Instant webCreatedDate) {
        this.webCreatedDate = webCreatedDate;
    }

    public Instant getWebUpdateDate() {
        return webUpdateDate;
    }

    public void setWebUpdateDate(Instant webUpdateDate) {
        this.webUpdateDate = webUpdateDate;
    }

    public Long getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(Long creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public Long getEditorUserId() {
        return editorUserId;
    }

    public void setEditorUserId(Long editorUserId) {
        this.editorUserId = editorUserId;
    }

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public DataOrganization recordID(String recordID) {
        this.recordID = recordID;
        return this;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getName() {
        return name;
    }

    public DataOrganization name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

  /*  public String getWebsite() {
        return website;
    }

    public DataOrganization website(String website) {
        this.website = website;
        return this;
    }

    public void setWebsite(String website) {
        this.website = website;
    }*/

    public String getDescription() {
        return description;
    }

    public DataOrganization description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public DataOrganization city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public DataOrganization state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public DataOrganization country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public DataOrganization zipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /*public String getFacebook() {
        return facebook;
    }*/

    /*   public DataOrganization facebook(String facebook) {
           this.facebook = facebook;
           return this;
       }

       public void setFacebook(String facebook) {
           this.facebook = facebook;
       }

       public String getGithub() {
           return github;
       }

       public DataOrganization github(String github) {
           this.github = github;
           return this;
       }

       public void setGithub(String github) {
           this.github = github;
       }

       public String getTwitter() {
           return twitter;
       }

       public DataOrganization twitter(String twitter) {
           this.twitter = twitter;
           return this;
       }

       public void setTwitter(String twitter) {
           this.twitter = twitter;
       }*/
    public Integer getYearFounded() {
        return yearFounded;
    }

    public DataOrganization yearFounded(Integer yearFounded) {
        this.yearFounded = yearFounded;
        return this;
    }

    public void setYearFounded(Integer yearFounded) {
        this.yearFounded = yearFounded;
    }

    public String getHeadCount() {
        return headCount;
    }

    public DataOrganization ceo(String ceo) {
        this.headCount = ceo;
        return this;
    }

    public void setHeadCount(String headCount) {
        this.headCount = headCount;
    }

    public String getFinancialPosition() {
        return financialPosition;
    }

    public DataOrganization financialPosition(String financialPosition) {
        this.financialPosition = financialPosition;
        return this;
    }

    public void setFinancialPosition(String financialPosition) {
        this.financialPosition = financialPosition;
    }

     
 
   /* public String getNotes() {
        return notes;
    }

    public DataOrganization notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
*/
   /* public Integer getInactive() {
        return inactive;
    }

    public DataOrganization inactive(Integer inactive) {
        this.inactive = inactive;
        return this;
    }

    public void setInactive(Integer inactive) {
        this.inactive = inactive;
    }

    public Integer getLive() {
        return live;
    }

    public DataOrganization live(Integer live) {
        this.live = live;
        return this;
    }

    public void setLive(Integer live) {
        this.live = live;
    }
*/
    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public String getClientBase() {
		return clientBase;
	}
	public void setClientBase(String clientBase) {
		this.clientBase = clientBase;
	}
	public DataOrganization createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getUpdateddate() {
        return updateddate;
    }

    public DataOrganization updateddate(LocalDate updateddate) {
        this.updateddate = updateddate;
        return this;
    }

    public void setUpdateddate(LocalDate updateddate) {
        this.updateddate = updateddate;
    }
    public DataOrganization getParentOrganization() {
        return parentOrganization;
    }

    public DataOrganization parentOrganization(DataOrganization dataOrganization) {
        this.parentOrganization = dataOrganization;
        return this;
    }

    public void setParentOrganization(DataOrganization dataOrganization) {
        this.parentOrganization = dataOrganization;
    }

  /*  public Set<DataArticle> getDataArticles() {
        return dataArticles;
    }

    public DataOrganization dataArticles(Set<DataArticle> dataArticles) {
        this.dataArticles = dataArticles;
        return this;
    }

    public DataOrganization addDataArticle(DataArticle dataArticle) {
        this.dataArticles.add(dataArticle);
        dataArticle.setAuthorOrganization(this);
        return this;
    }

    public DataOrganization removeDataArticle(DataArticle dataArticle) {
        this.dataArticles.remove(dataArticle);
        dataArticle.setAuthorOrganization(null);
        return this;
    }

    public void setDataArticles(Set<DataArticle> dataArticles) {
        this.dataArticles = dataArticles;
    }

    public Set<DataEvent> getOrganizationEvent() {
        return organizationEvent;
    }

    public DataOrganization organizationEvent(Set<DataEvent> dataEvents) {
        this.organizationEvent = dataEvents;
        return this;
    }

    public DataOrganization addOrganizationEvent(DataEvent dataEvent) {
        this.organizationEvent.add(dataEvent);
        dataEvent.getOrganizationOrganizer().add(this);
        return this;
    }

    public DataOrganization removeOrganizationEvent(DataEvent dataEvent) {
        this.organizationEvent.remove(dataEvent);
        dataEvent.getOrganizationOrganizer().remove(this);
        return this;
    }

    public void setOrganizationEvent(Set<DataEvent> organizationEvent) {
        this.organizationEvent = organizationEvent;
    }



    public Set<DataEvent> getSponsorOrganizationEvent() {
        return sponsorOrganizationEvent;
    }

    public DataOrganization sponsorOrganizationEvent(Set<DataEvent> dataEvents) {
        this.sponsorOrganizationEvent = dataEvents;
        return this;
    }

    public DataOrganization addSponsorOrganizationEvent(DataEvent dataEvent) {
        this.sponsorOrganizationEvent.add(dataEvent);
        dataEvent.getOrganizationOrganizer().add(this);
        return this;
    }

    public DataOrganization removeSponsorOrganizationEvent(DataEvent dataEvent) {
        this.sponsorOrganizationEvent.remove(dataEvent);
        dataEvent.getOrganizationOrganizer().remove(this);
        return this;
    }

    public void setSponsorOrganizationEvent(Set<DataEvent> sponsorOrganizationEvent) {
        this.sponsorOrganizationEvent = sponsorOrganizationEvent;
    }




    public Set<DataProvider> getOwners() {
        return owners;
    }

    public DataOrganization owners(Set<DataProvider> dataProviders) {
        this.owners = dataProviders;
        return this;
    }

    public DataOrganization addOwner(DataProvider dataProvider) {
        this.owners.add(dataProvider);
        dataProvider.setOwnerOrganization(this);
        return this;
    }

    public DataOrganization removeOwner(DataProvider dataProvider) {
        this.owners.remove(dataProvider);
        dataProvider.setOwnerOrganization(null);
        return this;
    }

    public void setOwners(Set<DataProvider> dataProviders) {
        this.owners = dataProviders;
    }

    public Set<DataOrganization> getDataOrganizations() {
        return dataOrganizations;
    }

    public DataOrganization dataOrganizations(Set<DataOrganization> dataOrganizations) {
        this.dataOrganizations = dataOrganizations;
        return this;
    }

    public DataOrganization addDataOrganization(DataOrganization dataOrganization) {
        this.dataOrganizations.add(dataOrganization);
        dataOrganization.setParentOrganization(this);
        return this;
    }

    public DataOrganization removeDataOrganization(DataOrganization dataOrganization) {
        this.dataOrganizations.remove(dataOrganization);
        dataOrganization.setParentOrganization(null);
        return this;
    }

    public void setDataOrganizations(Set<DataOrganization> dataOrganizations) {
        this.dataOrganizations = dataOrganizations;
    }*/
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataOrganization dataOrganization = (DataOrganization) o;
        if (dataOrganization.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataOrganization.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataOrganization{" +
            "id=" + getId() +
            ", recordID='" + getRecordID() + "'" +
            ", name='" + getName() + "'" +
            //    ", website='" + getWebsite() + "'" +
            ", description='" + getDescription() + "'" +
            ", city='" + getCity() + "'" +
            ", state='" + getState() + "'" +
            ", country='" + getCountry() + "'" +
            ", zipCode='" + getZipCode() + "'" +
           /* ", facebook='" + getFacebook() + "'" +
            ", github='" + getGithub() + "'" +
            ", twitter='" + getTwitter() + "'" +
*/            ", yearFounded='" + getYearFounded() + "'" +
            ", headCount='" + getHeadCount() + "'" +
            ", financialPosition='" + getFinancialPosition() + "'" +
            ", clientBase='" + getClientBase() + "'" +
            //   ", notes='" + getNotes() + "'" +
           /* ", inactive='" + getInactive() + "'" +
            ", live='" + getLive() + "'" +*/
            ", createdDate='" + getCreatedDate() + "'" +
            ", webCreatedDate='" + getWebCreatedDate() + "'" +
            ", webUpdateDate='" + getWebUpdateDate() + "'" +
            ", creatorUserId='" + getCreatorUserId() + "'" +
            ", editorUserId='" + getEditorUserId() + "'" +
            ", updateddate='" + getUpdateddate() + "'" +
            ", subsidiaryOrganization='" + getSubsidiaryOrganization() + "'" +
            ", yearExit='" + getYearExit() + "'" +
            ", headcountNumber='" + getHeadcountNumber() + "'" +
            //    ", phoneNumber='" + getPhoneNumber() + "'" +
            ", researchMethodsCompleted='" + getResearchMethodsCompleted() + "'" +
            //  ", researchMethods='"+getResearchMethods()+"'"+
            ", ieiStatus='"+getIeiStatus()+"'"+
            ", marketplaceStatus='"+getMarketplaceStatus()+"'"+
            //  ", ieiStatus='"+getIeiStatus()+"'"+
            ", lastUpdatedDate='"+getLastUpdatedDate()+"'"+
            ", headcountBucket='" + getHeadcountBucket() + "'" +
            "}";
    }
}
