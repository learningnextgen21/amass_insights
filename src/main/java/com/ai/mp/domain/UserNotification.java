package com.ai.mp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A UserNotification.
 */
@Entity
@Table(name = "mp_user_notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "usernotification")
public class UserNotification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Max(value = 11)
    @Column(name = "notification_id", nullable = false)
    private Integer notificationID;

    @NotNull
    @Max(value = 11)
    @Column(name = "user_id", nullable = false)
    private Integer userID;

    @NotNull
    @Max(value = 11)
    @Column(name = "user_provider_id", nullable = false)
    private Integer userProviderID;

    @NotNull
    @Max(value = 11)
    @Column(name = "user_data_category_id", nullable = false)
    private Integer userDataCategoryID;

    @NotNull
    @Max(value = 11)
    @Column(name = "user_product_id", nullable = false)
    private Integer userProductID;

    @NotNull
    @Max(value = 11)
    @Column(name = "user_comment_id", nullable = false)
    private Integer userCommentID;

    @NotNull
    @Column(name = "created_date", nullable = false)
    private LocalDate createdDate;

    @NotNull
    @Size(max = 20)
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @NotNull
    @Size(max = 20)
    @Column(name = "notification_type", length = 20, nullable = false)
    private String notificationType;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNotificationID() {
        return notificationID;
    }

    public UserNotification notificationID(Integer notificationID) {
        this.notificationID = notificationID;
        return this;
    }

    public void setNotificationID(Integer notificationID) {
        this.notificationID = notificationID;
    }

    public Integer getUserID() {
        return userID;
    }

    public UserNotification userID(Integer userID) {
        this.userID = userID;
        return this;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Integer getUserProviderID() {
        return userProviderID;
    }

    public UserNotification userProviderID(Integer userProviderID) {
        this.userProviderID = userProviderID;
        return this;
    }

    public void setUserProviderID(Integer userProviderID) {
        this.userProviderID = userProviderID;
    }

    public Integer getUserDataCategoryID() {
        return userDataCategoryID;
    }

    public UserNotification userDataCategoryID(Integer userDataCategoryID) {
        this.userDataCategoryID = userDataCategoryID;
        return this;
    }

    public void setUserDataCategoryID(Integer userDataCategoryID) {
        this.userDataCategoryID = userDataCategoryID;
    }

    public Integer getUserProductID() {
        return userProductID;
    }

    public UserNotification userProductID(Integer userProductID) {
        this.userProductID = userProductID;
        return this;
    }

    public void setUserProductID(Integer userProductID) {
        this.userProductID = userProductID;
    }

    public Integer getUserCommentID() {
        return userCommentID;
    }

    public UserNotification userCommentID(Integer userCommentID) {
        this.userCommentID = userCommentID;
        return this;
    }

    public void setUserCommentID(Integer userCommentID) {
        this.userCommentID = userCommentID;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public UserNotification createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return status;
    }

    public UserNotification status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public UserNotification notificationType(String notificationType) {
        this.notificationType = notificationType;
        return this;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserNotification amassUserNotification = (UserNotification) o;
        if (amassUserNotification.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), amassUserNotification.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserNotification{" +
            "id=" + getId() +
            ", notificationID='" + getNotificationID() + "'" +
            ", userID='" + getUserID() + "'" +
            ", userProviderID='" + getUserProviderID() + "'" +
            ", userDataCategoryID='" + getUserDataCategoryID() + "'" +
            ", userProductID='" + getUserProductID() + "'" +
            ", userCommentID='" + getUserCommentID() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", status='" + getStatus() + "'" +
            ", notificationType='" + getNotificationType() + "'" +
            "}";
    }
}
