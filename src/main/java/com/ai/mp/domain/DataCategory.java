package com.ai.mp.domain;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DataCategory.
 */
@Entity
@Table(name = "data_category")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "datacategory")
public class DataCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "record_id")
    private String recordID;

    public int getSortorder() {
        return sortOrder;
    }

    public void setSortorder(int sortorder) {
        this.sortOrder = sortorder;
    }

    private int sortOrder;

    private int mainDataCategoryProviderCount;

    private int relevantDataCategoryProviderCount;
    private int eventCategoryCount;

    @CreatedDate
    @Column(name = "web_created_date")
    @JsonIgnore
    private Instant webCreatedDate = Instant.now();

    @LastModifiedDate
    @Column(name = "web_updated_date")
    @JsonIgnore
    private Instant webUpdateDate = Instant.now();

    //  @NotNull
    @Column(name = "creator_user_id")
    private Long creatorUserId;

    // @NotNull
    @Column(name = "editor_user_id")
    private Long editorUserId;

    @Column(name="hashtag")
    private String hashtag;

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public Instant getWebCreatedDate() {
		return webCreatedDate;
	}

	public void setWebCreatedDate(Instant webCreatedDate) {
		this.webCreatedDate = webCreatedDate;
	}

	public Instant getWebUpdateDate() {
		return webUpdateDate;
	}

	public void setWebUpdateDate(Instant webUpdateDate) {
		this.webUpdateDate = webUpdateDate;
	}

	public Long getCreatorUserId() {
		return creatorUserId;
	}

	public void setCreatorUserId(Long creatorUserId) {
		this.creatorUserId = creatorUserId;
	}

	public Long getEditorUserId() {
		return editorUserId;
	}

	public void setEditorUserId(Long editorUserId) {
		this.editorUserId = editorUserId;
	}

	public int getMainDataCategoryProviderCount() {
        return mainDataCategoryProviderCount;
    }

    public void setMainDataCategoryProviderCount(int mainDataCategoryProviderCount) {
        this.mainDataCategoryProviderCount = mainDataCategoryProviderCount;
    }

    public int getRelevantDataCategoryProviderCount() {
        return relevantDataCategoryProviderCount;
    }

    public void setRelevantDataCategoryProviderCount(int relevantDataCategoryProviderCount) {
        this.relevantDataCategoryProviderCount = relevantDataCategoryProviderCount;
    }

    public int getEventCategoryCount() {
        return eventCategoryCount;
    }

    public void setEventCategoryCount(int eventCategoryCount) {
        this.eventCategoryCount = eventCategoryCount;
    }

    @Size(max = 20)
    @Column(name = "marketplace_status")
    private String marketplaceStatus;


    public boolean isFollowed() {
        return followed;
    }

    public void setFollowed(boolean followed) {
        this.followed = followed;
    }

    @Transient
    @JsonSerialize
    private boolean followed = false;
//    @Column(name = "new_main_provider_id")
//    private DataProvider newMainProviderID;
//
//    public DataProvider getNewMainProviderID() {
//        return newMainProviderID;
//    }
//
//    public void setNewMainProviderID(DataProvider newMainProviderID) {
//        this.newMainProviderID = newMainProviderID;
//    }
//
//    public DataProvider getNewRelevantProviderID() {
//        return newRelevantProviderID;
//    }
//
//    public void setNewRelevantProviderID(DataProvider newRelevantProviderID) {
//        this.newRelevantProviderID = newRelevantProviderID;
//    }

    public LocalDate getNewMainProviderDate() {
        return newMainProviderDate;
    }

    public void setNewMainProviderDate(LocalDate newMainProviderDate) {
        this.newMainProviderDate = newMainProviderDate;
    }

    public LocalDate getNewRelevantProviderDate() {
        return newRelevantProviderDate;
    }

    public void setNewRelevantProviderDate(LocalDate newRelevantProviderDate) {
        this.newRelevantProviderDate = newRelevantProviderDate;
    }


//    @Column(name = "new_relevant_provider_id")
//    private DataProvider newRelevantProviderID;

    @Column(name = "new_main_provider_date")
    @JsonAdapter(DateToStringTypeAdapter.class)
    private LocalDate newMainProviderDate;

    @Column(name = "new_relevant_provider_date")
    @JsonAdapter(DateToStringTypeAdapter.class)
    private LocalDate newRelevantProviderDate;

    @Size(max = 20)
    @Column(name = "permission_name", insertable=false,updatable = false)
    private String userPermission;

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public CategoryLogo getLogo() {
        return logo;
    }

    public void setLogo(CategoryLogo logo) {
        this.logo = logo;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "record_ID", insertable = false, updatable = false, referencedColumnName = "category_record_id")
    private CategoryLogo logo;

    @Transient
    @JsonSerialize
    private boolean locked;

    public String getUserPermission() {
        return userPermission;
    }

    public void setUserPermission(String permission) {
        this.userPermission = permission;
    }

    public String getMarketplaceStatus() {
        return marketplaceStatus;
    }

    public void setMarketplaceStatus(String marketplaceStatus) {
        this.marketplaceStatus = marketplaceStatus;
    }

    @NotNull
    @Column(name = "datacategory", nullable = false)
    private String datacategory;

    @Lob
    @Column(name = "explanation")
    private String explanation;

    @NotNull
    @Max(value = 2)
    @Column(name = "inactive", nullable = false)
    private Integer inactive;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @NotNull
    @Column(name = "created_date", nullable = false)
    private LocalDate createdDate;

    @JsonIgnore
    @Column(name = "updated_date")
    private Instant updatedDate;

    @OneToMany(mappedBy = "mainDataCategory")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> dataProviders = new HashSet<>();

    @ManyToMany(mappedBy = "categories")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataFeature> categoryFeatures = new HashSet<>();

    @ManyToMany(mappedBy = "categories")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> categoryProviders = new HashSet<>();

    @ManyToMany(mappedBy = "dataProviderCategories")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> providerCategories = new HashSet<>();

    public Set<User> getCategoryUsers() {
        return categoryUsers;
    }

    public void setCategoryUsers(Set<User> categoryUsers) {
        this.categoryUsers = categoryUsers;
    }

    @ManyToMany(mappedBy = "categories")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<User> categoryUsers = new HashSet<>();

    public Authority getPermission() {
        return permission;
    }

    public void setPermission(Authority permission) {
        this.permission = permission;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    private Authority permission;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public DataCategory recordID(String recordID) {
        this.recordID = recordID;
        return this;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getDatacategory() {
        return datacategory;
    }

    public DataCategory datacategory(String datacategory) {
        this.datacategory = datacategory;
        return this;
    }

    public void setDatacategory(String datacategory) {
        this.datacategory = datacategory;
    }

    public String getExplanation() {
        return explanation;
    }

    public DataCategory explanation(String explanation) {
        this.explanation = explanation;
        return this;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public Integer getInactive() {
        return inactive;
    }

    public DataCategory inactive(Integer inactive) {
        this.inactive = inactive;
        return this;
    }

    public void setInactive(Integer inactive) {
        this.inactive = inactive;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public DataCategory createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public DataCategory updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Set<DataProvider> getDataProviders() {
        return dataProviders;
    }

    public DataCategory dataProviders(Set<DataProvider> dataProviders) {
        this.dataProviders = dataProviders;
        return this;
    }

    public DataCategory addDataProvider(DataProvider dataProvider) {
        this.dataProviders.add(dataProvider);
        dataProvider.setMainDataCategory(this);
        return this;
    }

    public DataCategory removeDataProvider(DataProvider dataProvider) {
        this.dataProviders.remove(dataProvider);
        dataProvider.setMainDataCategory(null);
        return this;
    }

    public void setDataProviders(Set<DataProvider> dataProviders) {
        this.dataProviders = dataProviders;
    }

    public Set<DataFeature> getCategoryFeatures() {
        return categoryFeatures;
    }

    public DataCategory categoryFeatures(Set<DataFeature> dataFeatures) {
        this.categoryFeatures = dataFeatures;
        return this;
    }

    public DataCategory addCategoryFeature(DataFeature dataFeature) {
        this.categoryFeatures.add(dataFeature);
        dataFeature.getCategories().add(this);
        return this;
    }

    public DataCategory removeCategoryFeature(DataFeature dataFeature) {
        this.categoryFeatures.remove(dataFeature);
        dataFeature.getCategories().remove(this);
        return this;
    }

    public void setCategoryFeatures(Set<DataFeature> dataFeatures) {
        this.categoryFeatures = dataFeatures;
    }

    public Set<DataProvider> getCategoryProviders() {
        return categoryProviders;
    }

    public DataCategory categoryProviders(Set<DataProvider> dataProviders) {
        this.categoryProviders = dataProviders;
        return this;
    }

    public DataCategory addCategoryProvider(DataProvider dataProvider) {
        this.categoryProviders.add(dataProvider);
//        dataProvider.getCategories().add(this);
        return this;
    }

    public DataCategory removeCategoryProvider(DataProvider dataProvider) {
        this.categoryProviders.remove(dataProvider);
        dataProvider.getCategories().remove(this);
        return this;
    }

    public void setCategoryProviders(Set<DataProvider> dataProviders) {
        this.categoryProviders = dataProviders;
    }

    /********************************For Providers form Page*********************/
    public Set<DataProvider> getProviderCategories() {
        return providerCategories;
    }

    public DataCategory providerCategories(Set<DataProvider> dataProviders) {
        this.providerCategories = dataProviders;
        return this;
    }

    public DataCategory addProviderCategory(DataProvider dataProvider) {
        this.providerCategories.add(dataProvider);
//        dataProvider.getCategories().add(this);
        return this;
    }

    public DataCategory removeProviderCategory(DataProvider dataProvider) {
        this.providerCategories.remove(dataProvider);
        dataProvider.getDataProviderCategories().remove(this);
        return this;
    }

    public void setProviderCategories(Set<DataProvider> dataProviders) {
        this.providerCategories = dataProviders;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataCategory dataCategory = (DataCategory) o;
        if (dataCategory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataCategory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataCategory{" +
            "id=" + getId() +
            ", recordID='" + getRecordID() + "'" +
            ", datacategory='" + getDatacategory() + "'" +
            ", explanation='" + getExplanation() + "'" +
            ", inactive='" + getInactive() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", webCreatedDate='" + getWebCreatedDate() + "'" +
            ", webUpdateDate='" + getWebUpdateDate() + "'" +
            ", creatorUserId='" + getCreatorUserId() + "'" +
            ", editorUserId='" + getEditorUserId() + "'" +
            ", hashtag='" + getHashtag() + "'" +
            "}";
    }
}
