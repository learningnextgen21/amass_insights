package com.ai.mp.domain;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.internal.bind.DateTypeAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DataArticle.
 */
@Entity
@Table(name = "data_article")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dataarticle")
public class DataArticle implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    private String stream;

    public boolean isIncludeInNewsletter() {
        return includeInNewsletter;
    }

    public void setIncludeInNewsletter(boolean includeInNewsletter) {
        this.includeInNewsletter = includeInNewsletter;
    }

    private boolean includeInNewsletter;

    @NotNull
    @Size(max = 512)
    @Column(name = "articleuniquekey", length = 512, nullable = false)
    private String articleuniquekey;

    @Size(max = 50)
    @Column(name = "record_id", length = 50)
    private String recordID;

    @NotNull
    @Size(max = 256)
    @Column(name = "title", length = 256, nullable = false)
    private String title;

    public String getVisualUrl() {
        return visualUrl;
    }

    public void setVisualUrl(String visualUrl) {
        this.visualUrl = visualUrl;
    }

    private String visualUrl;

    @NotNull
    @Size(max = 512)
    @Column(name = "url", length = 512, nullable = false)
    private String url;

  
    @JsonAdapter(DateToStringTypeAdapter.class)
    @NotNull
    @Column(name = "published_date")
    private LocalDate publishedDate;
    
    @NotNull
    @Lob
    @Column(name = "content", nullable = false)
    private String content;

    @NotNull
    @Lob
    @Column(name = "content_html", nullable = false)
    private String contentHTML;

    @NotNull
    @Lob
    @Column(name = "summary", nullable = false)
    private String summary;

    @NotNull
    @Size(max = 128)
    @Column(name = "author", length = 128, nullable = false)
    private String author;

    @NotNull
    @Size(max = 36)
    @Column(name = "publisher", length = 36, nullable = false)
    private String publisher;

    @NotNull
    @Lob
    @Column(name = "summary_html", nullable = false)
    private String summaryHTML;

    @NotNull
    @Max(value = 11)
    @Column(name = "relevancy", nullable = false)
    private Integer relevancy;

    public Set<DataResourcePurpose> getPurpose() {
        return purpose;
    }

    public void setPurpose(Set<DataResourcePurpose> purpose) {
        this.purpose = purpose;
    }

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_article_purpose",
        joinColumns = @JoinColumn(name="articles_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="purposes_id", referencedColumnName="id"))
    private Set<DataResourcePurpose> purpose;

    public Set<DataResourcePurposeType> getPurposeType() {
        return purposeType;
    }

    public void setPurposeType(Set<DataResourcePurposeType> purposeType) {
        this.purposeType = purposeType;
    }

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_article_purpose_type",
        joinColumns = @JoinColumn(name="articles_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="purpose_types_id", referencedColumnName="id"))
    private Set<DataResourcePurposeType> purposeType;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @NotNull
    @Column(name = "created_date", nullable = false)
    private LocalDate createdDate;

   /* @JsonAdapter(DateTypeAdapter.class)
    @Column(name = "updated_date")
    private Date updatedDate;*/

    @JsonAdapter(DateToStringTypeAdapter.class)
    @NotNull
    @Column(name = "updated_date")
    private LocalDate updatedDate;
    
    @ManyToOne
    private DataOrganization authorOrganization;

    @ManyToOne
    //@JsonIgnore
    private DataProvider authorDataProvider;

    @ManyToMany(mappedBy = "relevantArticles", fetch = FetchType.LAZY)
    //@JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProvider> relevantProviders = new HashSet<>();

    public Set<DataTopic> getArticleTopics() {
        return articleTopics;
    }

    public void setArticleTopics(Set<DataTopic> articleTopics) {
        this.articleTopics = articleTopics;
    }

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_article_topic",
        joinColumns = @JoinColumn(name="articles_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="topics_id", referencedColumnName="id"))
    private Set<DataTopic> articleTopics = new HashSet<>();
    
    @CreatedDate
    @Column(name = "web_created_date")
    @JsonIgnore
    private Instant webCreatedDate = Instant.now();

    @LastModifiedDate
    @Column(name = "web_updated_date")
    @JsonIgnore
    private Instant webUpdateDate = Instant.now();


    @Column(name = "post_date")
    @JsonAdapter(DateToStringTypeAdapter.class)
    private LocalDate postDate;

    public LocalDate getPostDate() {
        return postDate;
    }

    public void setPostDate(LocalDate postDate) {
        this.postDate = postDate;
    }

    public Instant getWebCreatedDate() {
		return webCreatedDate;
	}

	public void setWebCreatedDate(Instant webCreatedDate) {
		this.webCreatedDate = webCreatedDate;
	}

	public Instant getWebUpdateDate() {
		return webUpdateDate;
	}

	public void setWebUpdateDate(Instant webUpdateDate) {
		this.webUpdateDate = webUpdateDate;
	}

	public Long getCreatorUserId() {
		return creatorUserId;
	}

	public void setCreatorUserId(Long creatorUserId) {
		this.creatorUserId = creatorUserId;
	}

	public Long getEditorUserId() {
		return editorUserId;
	}

	public void setEditorUserId(Long editorUserId) {
		this.editorUserId = editorUserId;
	}

	//  @NotNull
    @Column(name = "creator_user_id")
    private Long creatorUserId;

    // @NotNull
    @Column(name = "editor_user_id")
    private Long editorUserId;

    @Column(name="linked_In")
    private Boolean linkedIn;

    @Column(name="twitter")
    private Boolean twitter;

    public Boolean getSent() {
        return sent;
    }

    public void setSent(Boolean sent) {
        this.sent = sent;
    }

    @Column(name="sent")
    private Boolean sent;

    public Boolean getMailSent() {
        return mailSent;
    }

    public void setMailSent(Boolean mailSent) {
        this.mailSent = mailSent;
    }

    @Column(name="mail_sent")
    private Boolean mailSent;

    public Boolean getLinkedIn() {
        return linkedIn;
    }

    public void setLinkedIn(Boolean linkedIn) {
        this.linkedIn = linkedIn;
    }

    public Boolean getTwitter() {
        return twitter;
    }

    public void setTwitter(Boolean twitter) {
        this.twitter = twitter;
    }
    
    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArticleuniquekey() {
        return articleuniquekey;
    }

    public DataArticle articleuniquekey(String articleuniquekey) {
        this.articleuniquekey = articleuniquekey;
        return this;
    }

    public void setArticleuniquekey(String articleuniquekey) {
        this.articleuniquekey = articleuniquekey;
    }

    public String getRecordID() {
        return recordID;
    }

    public DataArticle recordID(String recordID) {
        this.recordID = recordID;
        return this;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getTitle() {
        return title;
    }

    public DataArticle title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public DataArticle url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

  

    public LocalDate getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(LocalDate publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getContent() {
        return content;
    }

    public DataArticle content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentHTML() {
        return contentHTML;
    }

    public DataArticle contentHTML(String contentHTML) {
        this.contentHTML = contentHTML;
        return this;
    }

    public void setContentHTML(String contentHTML) {
        this.contentHTML = contentHTML;
    }

    public String getSummary() {
        return summary;
    }

    public DataArticle summary(String summary) {
        this.summary = summary;
        return this;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getAuthor() {
        return author;
    }

    public DataArticle author(String author) {
        this.author = author;
        return this;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public DataArticle publisher(String publisher) {
        this.publisher = publisher;
        return this;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getSummaryHTML() {
        return summaryHTML;
    }

    public DataArticle summaryHTML(String summaryHTML) {
        this.summaryHTML = summaryHTML;
        return this;
    }

    public void setSummaryHTML(String summaryHTML) {
        this.summaryHTML = summaryHTML;
    }

    public Integer getRelevancy() {
        return relevancy;
    }

    public DataArticle relevancy(Integer relevancy) {
        this.relevancy = relevancy;
        return this;
    }

    public void setRelevancy(Integer relevancy) {
        this.relevancy = relevancy;
    }

//    public String getPurpose() {
//        return purpose;
//    }
//
//    public DataArticle purpose(String purpose) {
//        this.purpose = purpose;
//        return this;
//    }
//
//    public void setPurpose(String purpose) {
//        this.purpose = purpose;
//    }
//
//    public String getPurposeType() {
//        return purposeType;
//    }
//
//    public DataArticle purposeType(String purposeType) {
//        this.purposeType = purposeType;
//        return this;
//    }
//
//    public void setPurposeType(String purposeType) {
//        this.purposeType = purposeType;
//    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public DataArticle createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(LocalDate updatedDate) {
		this.updatedDate = updatedDate;
	}

	public DataOrganization getAuthorOrganization() {
        return authorOrganization;
    }

    public DataArticle authorOrganization(DataOrganization dataOrganization) {
        this.authorOrganization = dataOrganization;
        return this;
    }

    public void setAuthorOrganization(DataOrganization dataOrganization) {
        this.authorOrganization = dataOrganization;
    }

    public DataProvider getAuthorDataProvider() {
        return authorDataProvider;
    }

    public DataArticle authorDataProvider(DataProvider dataProvider) {
        this.authorDataProvider = dataProvider;
        return this;
    }

    public void setAuthorDataProvider(DataProvider dataProvider) {
        this.authorDataProvider = dataProvider;
    }

    public Set<DataProvider> getRelevantProviders() {
        return relevantProviders;
    }

    public void setRelevantProviders(Set<DataProvider> relevantProviders) {
        this.relevantProviders = relevantProviders;
    }

    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataArticle dataArticle = (DataArticle) o;
        if (dataArticle.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataArticle.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataArticle{" +
            "id=" + getId() +
            ", articleuniquekey='" + getArticleuniquekey() + "'" +
            ", recordID='" + getRecordID() + "'" +
            ", title='" + getTitle() + "'" +
            ", url='" + getUrl() + "'" +
            ", publishedDate='" + getPublishedDate() + "'" +
            ", content='" + getContent() + "'" +
            ", contentHTML='" + getContentHTML() + "'" +
            ", summary='" + getSummary() + "'" +
            ", author='" + getAuthor() + "'" +
            ", publisher='" + getPublisher() + "'" +
            ", summaryHTML='" + getSummaryHTML() + "'" +
            ", relevancy='" + getRelevancy() + "'" +
            ", purpose='" + getPurpose() + "'" +
            ", purposeType='" + getPurposeType() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", webCreatedDate='" + getWebCreatedDate() + "'" +
            ", authorDataProvider='" + getAuthorDataProvider() + "'" +
            ", webUpdateDate='" + getWebUpdateDate() + "'" +
            ", postDate='" + getPostDate() + "'" +
            ", creatorUserId='" + getCreatorUserId() + "'" +
            ", editorUserId='" + getEditorUserId() + "'" +
            ", twitter='" + getTwitter() + "'" +
            ", linkedIn='" + getLinkedIn() + "'" +
            ", sent='" + getSent() + "'" +
            "}";
    }
}
