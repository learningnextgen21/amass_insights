package com.ai.mp.domain;

import com.ai.core.Constants;
import com.ai.mp.utils.BasicBean;
import com.ai.mp.utils.DateToStringTypeAdapter;
import com.ai.mp.utils.SetToBeanConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.JsonAdapter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.WhereJoinTable;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.*;

@Entity
@Table(name = "data_event")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dataevent")
public class DataEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;

    @Id
    @Size(max = 50)
    @Column(name = "record_id", length = 50)
    private String recordID;

    @NotNull
    @Size(max = 100)
    @Column(name = "event_edition_name", length = 100, nullable = false)
    private String eventEditionNames;

    @NotNull
    @Size(max = 512)
    @Column(name = "link", length = 512, nullable = false)
    private String link;

    @NotNull
    @Lob
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Lob
    @Column(name = "details", nullable = false)
    private String details;

    @NotNull
    @Lob
    @Column(name = "agenda", nullable = false)
    private String agenda;

    @NotNull
    @Column(name = "start_time", nullable = false)
    private Date startTime;


    @NotNull
    @Column(name = "end_time", nullable = false)
    private Date endTime;


    @Size(max = 50)
    @Column(name = "length_in_days", length = 50, nullable = false)
    private String lenthInDays;

    @Size(max = 512)
    @Column(name = "price", length = 512, nullable = false)
    private String price;

    @Size(max = 100)
    @Column(name = "venue", length = 100, nullable = false)
    private String venue;

    @Size(max = 150)
    @Column(name = "address", length = 150, nullable = false)
    private String address;

    @Size(max = 100)
    @Column(name = "city", length = 100, nullable = false)
    private String city;

    @Size(max = 100)
    @Column(name = "state", length = 100, nullable = false)
    private String state;

    @Size(max = 100)
    @Column(name = "country", length = 100, nullable = false)
    private String country;

    @Size(max = 50)
    @Column(name = "zip_code", length = 50, nullable = false)
    private String zipCode;

    @Size(max = 50)
    @Column(name = "user_permission", length = 50, nullable = false)
    private String userPermission;


    @Column(name = "last_updated", nullable = false)
    private Date lastUpdated;

    @JsonAdapter(DateToStringTypeAdapter.class)
    @NotNull
    @Column(name = "created_time", nullable = false)
    private LocalDate createdTime;

    @Size(max = 50)
    @Column(name = "authored_articles", length = 50, nullable = false)
    private String authoredArticles;

    @Column(name = "relevent_articles", nullable = false)
    private Integer releventArticles;

    private float completeness;

    @Column(name = "industry", insertable = false, updatable = false)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> industries = new ArrayList<BasicBean>();

    @Column(name = "pricing_model", insertable = false, updatable = false)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> pricingModels = new ArrayList<BasicBean>();

    @Column(name = "topic", insertable = false, updatable = false)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> topics = new ArrayList<BasicBean>();

    @Column(name = "category", insertable = false, updatable = false)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> categories = new ArrayList<BasicBean>();

    @Column(name = "event_type", insertable = false, updatable = false)
    @Convert(converter = SetToBeanConverter.class)
    private List<BasicBean> eventTypes = new ArrayList<BasicBean>();

    @ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_organization_events",
        joinColumns = @JoinColumn(name="event_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="organization_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "event_type = " + Constants.LOOKUP_EVENT_ORGANIZATION_TYPE)
    private Set<DataOrganization> organizationOrganizer = new HashSet<>();

    @ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_organization_events",
        joinColumns = @JoinColumn(name="event_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="organization_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "event_type = " + Constants.LOOKUP_EVENT_SPONSOR_ORGANIZATION_TYPE)
    private Set<DataOrganization> sponsorOrganization = new HashSet<>();

    @ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_events",
        joinColumns = @JoinColumn(name="event_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "event_type = " + Constants.LOOKUP_EVENT_PROVIDER_TYPE)
    private Set<DataProvider> providerOrganizer = new HashSet<>();

    @ManyToMany (fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_provider_events",
        joinColumns = @JoinColumn(name="event_id", referencedColumnName="id") , inverseJoinColumns = @JoinColumn(name="data_providers_id", referencedColumnName="id"))
    @WhereJoinTable(clause = "event_type = " + Constants.LOOKUP_EVENT_SPONSOR_PROVIDER_TYPE)
    private Set<DataProvider> sponsorProvider = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "data_events_topic",
        joinColumns = @JoinColumn(name="data_events_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="topics_id", referencedColumnName="id"))
    private Set<DataTopic> eventTopics = new HashSet<>();

    @CreatedDate
    @Column(name = "web_created_date")
    @JsonIgnore
    private Instant webCreatedDate = Instant.now();

    @LastModifiedDate
    @Column(name = "web_updated_date")
    @JsonIgnore
    private Instant webUpdateDate = Instant.now();

    public Instant getWebCreatedDate() {
		return webCreatedDate;
	}

	public void setWebCreatedDate(Instant webCreatedDate) {
		this.webCreatedDate = webCreatedDate;
	}

	public Instant getWebUpdateDate() {
		return webUpdateDate;
	}

	public void setWebUpdateDate(Instant webUpdateDate) {
		this.webUpdateDate = webUpdateDate;
	}

	public Long getCreatorUserId() {
		return creatorUserId;
	}

	public void setCreatorUserId(Long creatorUserId) {
		this.creatorUserId = creatorUserId;
	}

	public Long getEditorUserId() {
		return editorUserId;
	}

	public void setEditorUserId(Long editorUserId) {
		this.editorUserId = editorUserId;
	}

	//  @NotNull
    @Column(name = "creator_user_id")
    private Long creatorUserId;

    // @NotNull
    @Column(name = "editor_user_id")
    private Long editorUserId;
    
    
    
    public Set<DataTopic> getEventTopics() {
        return eventTopics;
    }

    public void setEventTopics(Set<DataTopic> eventTopics) {
        this.eventTopics = eventTopics;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordID() {
        return recordID;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getEventEditionNames() {
        return eventEditionNames;
    }

    public void setEventEditionNames(String eventEditionNames) {
        this.eventEditionNames = eventEditionNames;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getAgenda() {
        return agenda;
    }

    public void setAgenda(String agenda) {
        this.agenda = agenda;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getLenthInDays() {
        return lenthInDays;
    }

    public void setLenthInDays(String lenthInDays) {
        this.lenthInDays = lenthInDays;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getUserPermission() {
        return userPermission;
    }

    public void setUserPermission(String userPermission) {
        this.userPermission = userPermission;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public LocalDate getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDate createdTime) {
        this.createdTime = createdTime;
    }

    public String getAuthoredArticles() {
        return authoredArticles;
    }

    public void setAuthoredArticles(String authoredArticles) {
        this.authoredArticles = authoredArticles;
    }

    public Integer getReleventArticles() {
        return releventArticles;
    }

    public void setReleventArticles(Integer releventArticles) {
        this.releventArticles = releventArticles;
    }

    public float getCompleteness() {
        return completeness;
    }

    public void setCompleteness(float completeness) {
        this.completeness = completeness;
    }

    public List<BasicBean> getIndustries() {
        return industries;
    }

    public void setIndustries(List<BasicBean> industries) {
        this.industries = industries;
    }

    public List<BasicBean> getTopics() {
        return topics;
    }

    public void setTopics(List<BasicBean> topics) {
        this.topics = topics;
    }

    public List<BasicBean> getCategories() {
        return categories;
    }

    public void setCategories(List<BasicBean> categories) {
        this.categories = categories;
    }

    public List<BasicBean> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<BasicBean> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public Set<DataOrganization> getOrganizationOrganizer() {
        return organizationOrganizer;
    }

    public void setOrganizationOrganizer(Set<DataOrganization> organizationOrganizer) {
        this.organizationOrganizer = organizationOrganizer;
    }

    public Set<DataOrganization> getSponsorOrganization() {
        return sponsorOrganization;
    }

    public void setSponsorOrganization(Set<DataOrganization> sponsorOrganization) {
        this.sponsorOrganization = sponsorOrganization;
    }

    public Set<DataProvider> getProviderOrganizer() {
        return providerOrganizer;
    }

    public void setProviderOrganizer(Set<DataProvider> providerOrganizer) {
        this.providerOrganizer = providerOrganizer;
    }

    public Set<DataProvider> getSponsorProvider() {
        return sponsorProvider;
    }

    public void setSponsorProvider(Set<DataProvider> sponsorProvider) {
        this.sponsorProvider = sponsorProvider;
    }

    public List<BasicBean> getPricingModels() {
        return pricingModels;
    }

    public void setPricingModels(List<BasicBean> pricingModels) {
        this.pricingModels = pricingModels;
    }
/* public Set<DataEvent> getSponsorOrganizer() {
        return sponsorOrganizer;
    }

    public void setSponsorOrganizer(Set<DataEvent> sponsorOrganizer) {
        this.sponsorOrganizer = sponsorOrganizer;
    } */

    @Override
    public String toString() {
        return "DataEvent{" +
            "id=" + id +
            ", recordID='" + recordID + '\'' +
            ", eventEditionNames='" + eventEditionNames + '\'' +
            ", link='" + link + '\'' +
            ", description='" + description + '\'' +
            ", details='" + details + '\'' +
            ", agenda='" + agenda + '\'' +
            ", startTime=" + startTime +
            ", endTime=" + endTime +
            ", lenthInDays='" + lenthInDays + '\'' +
            ", pricingModels='" + pricingModels + '\'' +
            ", price='" + price + '\'' +
            ", venue='" + venue + '\'' +
            ", address='" + address + '\'' +
            ", city='" + city + '\'' +
            ", state='" + state + '\'' +
            ", country='" + country + '\'' +
            ", zipCode='" + zipCode + '\'' +
            ", userPermission='" + userPermission + '\'' +
            ", lastUpdated=" + lastUpdated +
            ", createdTime=" + createdTime +
            ", authoredArticles='" + authoredArticles + '\'' +
            ", releventArticles=" + releventArticles +
            ", completeness=" + completeness +
            ", industries=" + industries +
            ", topics=" + topics +
            ", categories=" + categories +
            ", eventTypes=" + eventTypes +
            ", webCreatedDate='" + getWebCreatedDate() + "'" +
            ", webUpdateDate='" + getWebUpdateDate() + "'" +
            ", creatorUserId='" + getCreatorUserId() + "'" +
            ", editorUserId='" + getEditorUserId() + "'" +
            '}';
    }
}
