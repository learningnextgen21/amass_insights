package com.ai.mp.domain;

import com.ai.mp.utils.DateToStringTypeAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.JsonAdapter;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;
@Entity
@Table(name = "payment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Payment implements Serializable{

	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
/*
	    @NotNull
	    @Column(name = "jhi_date", nullable = false)
	    private ZonedDateTime date;
*/
	    //@NotNull
	    @Column(name = "token", nullable = false)
	    private String token;

	    @NotNull
	    @Column(name = "currency", nullable = false)
	    private String currency;

	    @NotNull
	    @Min(value = 0)
	    @Column(name = "amount", nullable = false)
	    private Integer amount;

	    @Column(name = "description")
	    private String description;

	    @NotNull
	    @Column(name = "capture", nullable = false)
	    private Boolean capture;

	    @Lob
	    @Column(name = "receipt")
	    private String receipt;

	   /* public Instant getWebCreatedDate() {
			return webCreatedDate;
		}

		public void setWebCreatedDate(Instant webCreatedDate) {
			this.webCreatedDate = webCreatedDate;
		}*/

		@ManyToOne
	    @JsonIgnoreProperties("payments")
	    private User user;

	    public LocalDate getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(LocalDate createdDate) {
			this.createdDate = createdDate;
		}

		@JsonAdapter(DateToStringTypeAdapter.class)
	    @Column(name = "created_date")
	    private LocalDate createdDate = LocalDate.now();

		/*
	    @CreatedDate
	    @Column(name = "created_date")
	    private Instant webCreatedDate = Instant.now();*/
	    
	    @JsonAdapter(DateToStringTypeAdapter.class)
	    @Column(name = "expiration_date")
	    private LocalDate expirationDate;
	    
	    @Column(name = "name")
	    private String name;
	    
	    @Column(name = "email")
	    private String email;
	    

	    public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public LocalDate getExpirationDate() {
	        return expirationDate;
	    }

	    public void setExpirationDate(LocalDate expirationDate) {
	        this.expirationDate = expirationDate;
	    }

	    
	    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
	    public Long getId() {
	        return id;
	    }

	    public void setId(Long id) {
	        this.id = id;
	    }

	/*    public ZonedDateTime getDate() {
	        return date;
	    }

	    public Payment date(ZonedDateTime date) {
	        this.date = date;
	        return this;
	    }

	    public void setDate(ZonedDateTime date) {
	        this.date = date;
	    }*/

	    public String getToken() {
	        return token;
	    }

	    public Payment token(String token) {
	        this.token = token;
	        return this;
	    }

	    public void setToken(String token) {
	        this.token = token;
	    }

	    public String getCurrency() {
	        return currency;
	    }

	    public Payment currency(String currency) {
	        this.currency = currency;
	        return this;
	    }

	    public void setCurrency(String currency) {
	        this.currency = currency;
	    }

	    public Integer getAmount() {
	        return amount;
	    }

	    public Payment amount(Integer amount) {
	        this.amount = amount;
	        return this;
	    }

	    public void setAmount(Integer amount) {
	        this.amount = amount;
	    }

	    public String getDescription() {
	        return description;
	    }

	    public Payment description(String description) {
	        this.description = description;
	        return this;
	    }

	    public void setDescription(String description) {
	        this.description = description;
	    }

	    public Boolean isCapture() {
	        return capture;
	    }

	    public Payment capture(Boolean capture) {
	        this.capture = capture;
	        return this;
	    }

	    public void setCapture(Boolean capture) {
	        this.capture = capture;
	    }

	    public String getReceipt() {
	        return receipt;
	    }

	    public Payment receipt(String receipt) {
	        this.receipt = receipt;
	        return this;
	    }

	    public void setReceipt(String receipt) {
	        this.receipt = receipt;
	    }

	    public User getUser() {
	        return user;
	    }

	    public Payment user(User user) {
	        this.user = user;
	        return this;
	    }

	    public void setUser(User user) {
	        this.user = user;
	    }
	    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

	    @Override
	    public boolean equals(Object o) {
	        if (this == o) {
	            return true;
	        }
	        if (o == null || getClass() != o.getClass()) {
	            return false;
	        }
	        Payment payment = (Payment) o;
	        if (payment.getId() == null || getId() == null) {
	            return false;
	        }
	        return Objects.equals(getId(), payment.getId());
	    }

	    @Override
	    public int hashCode() {
	        return Objects.hashCode(getId());
	    }

	    @Override
	    public String toString() {
	        return "Payment{" +
	            "id=" + getId() +
	         //   ", date='" + getDate() + "'" +
	            ", token='" + getToken() + "'" +
	            ", currency='" + getCurrency() + "'" +
	            ", amount=" + getAmount() +
	            ", description='" + getDescription() + "'" +
	            ", capture='" + isCapture() + "'" +
	            ", receipt='" + getReceipt() + "'" +
	            ", createdDate='" + getCreatedDate() + "'" +
	            ", expirationDate='" + getExpirationDate() + "'" +
	            ", name='" + getName() + "'" +
	            ", email='" + getEmail() + "'" +
	            "}";
	    }
}
