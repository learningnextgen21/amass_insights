package com.ai.mp.domain;

import com.ai.mp.config.Constants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Email;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.util.*;

import static com.ai.core.Constants.INTEREST_TYPE_FOLLOW;
import static com.ai.core.Constants.INTEREST_TYPE_UNLOCK;

/**
 * A user.
 */
@Entity
@Table(name = "jhi_user")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "user")
public class User extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 100)
    @Column(length = 100, unique = true, nullable = false)
    private String login;


    @NotNull
    @Size(min = 6, max = 60)
    @Column(name = "password_hash",length = 60)
    private String password;

    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;

    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 100)
    @Column(length = 100, unique = true)
    private String email;

    @NotNull
    @Column(nullable = false)
    private boolean activated = false;

    @Transient
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    @ManyToOne
    private LookupCode rolePermission;

    public LookupCode getPermission() {
		return rolePermission;
	}

	public void setPermission(LookupCode permission) {
		this.rolePermission = permission;
	}

	public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    private boolean confirmed = false;

    public boolean isProfessional_investor() {
		return professional_investor;
	}

	public void setProfessional_investor(boolean professional_investor) {
		this.professional_investor = professional_investor;
	}

	public boolean isInterest_sell_data_product() {
		return interest_sell_data_product;
	}

	public void setInterest_sell_data_product(boolean interest_sell_data_product) {
		this.interest_sell_data_product = interest_sell_data_product;
	}

	public boolean isNotify_news_update() {
		return notify_news_update;
	}

	public void setNotify_news_update(boolean notify_news_update) {
		this.notify_news_update = notify_news_update;
	}

	@NotNull
    @Column(nullable = false)
    private boolean professional_investor = false;

    @NotNull
    @Column(nullable = false)
    private boolean interest_sell_data_product = false;

    @NotNull
    @Column(nullable = false)
    private boolean notify_news_update = false;

    @Size(min = 2, max = 5)
    @Column(name = "lang_key", length = 5)
    private String langKey;

    @Size(max = 256)
    @Column(name = "image_url", length = 256)
    private String imageUrl;

    @Size(max = 20)
    @Column(name = "activation_key", length = 20)
    @JsonIgnore
    private String activationKey;

    public String getConfirmationKey() {
        return confirmationKey;
    }

    public void setConfirmationKey(String confirmationKey) {
        this.confirmationKey = confirmationKey;
    }

    @Size(max = 20)
    @Column(name = "confirmation_key", length = 20)
    @JsonIgnore
    private String confirmationKey;


    @Size(max = 20)
    @Column(name = "reset_key", length = 20)
    @JsonIgnore
    private String resetKey;

    @Column(name = "reset_date")
    private Instant resetDate = null;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    private String company;


    @ManyToMany
    @JoinTable(
        name = "jhi_user_authority",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    private Set<Authority> authorities = new HashSet<>();

    public Set<DataCategory> getCategories() {
        return categories;
    }

    public void setCategories(Set<DataCategory> categories) {
        this.categories = categories;
    }

    @ManyToMany
    @JoinTable(
        name = "mp_user_data_category",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "data_category_id", referencedColumnName = "id")})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    private Set<DataCategory> categories = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "mp_user_asset_classes",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "asset_class_id", referencedColumnName = "id")})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    private Set<LookupCode> assetClasses = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "mp_user_sectors",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "sectors_id", referencedColumnName = "id")})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    private Set<LookupCode> sectors = new HashSet<>();


    @ManyToMany
    @JoinTable(
        name = "mp_user_geographies",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "geographies_id", referencedColumnName = "id")})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    private Set<LookupCode> geographies = new HashSet<>();

    @Column(name = "fund_name")
    private String fundName;

   public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

public Set<LookupCode> getAssetClasses() {
		return assetClasses;
	}

	public void setAssetClasses(Set<LookupCode> assetClasses) {
		this.assetClasses = assetClasses;
	}

	public Set<LookupCode> getSectors() {
		return sectors;
	}

	public void setSectors(Set<LookupCode> sectors) {
		this.sectors = sectors;
	}

public Set<UserProvider> getProvidersFollowed() {
        return providersFollowed;
    }

    public void setProvidersFollowed(Set<UserProvider> providersFollowed) {
        this.providersFollowed = providersFollowed;
    }

    public Set<UserProvider> getProvidersUnlocked() {
        return providersUnlocked;
    }

    public void setProvidersUnlocked(Set<UserProvider> providersUnlocked) {
        this.providersUnlocked = providersUnlocked;
    }

    @OneToMany(mappedBy = "provider")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @Where(clause = "interest_type = " + INTEREST_TYPE_FOLLOW)
    private Set<UserProvider> providersFollowed = new HashSet<>();

    @OneToMany(mappedBy = "provider")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @Where(clause = "interest_type = " + INTEREST_TYPE_UNLOCK)
    private Set<UserProvider> providersUnlocked = new HashSet<>();
    
    @Transient
	@JsonSerialize
	@JsonDeserialize
	private Set<DataProvider> providerPartners = new HashSet<>();
    
    @Transient
   	@JsonSerialize
   	@JsonDeserialize
   	private Set<DataProvider> unlockedPartners = new HashSet<>();

    public Set<DataProvider> getProviderPartners() {
		return providerPartners;
	}

	public Set<DataProvider> getUnlockedPartners() {
		return unlockedPartners;
	}

	public void setProviderPartners(Set<DataProvider> providerPartners) {
		this.providerPartners = providerPartners;
	}

	public void setUnlockedPartners(Set<DataProvider> unlockedPartners) {
		this.unlockedPartners = unlockedPartners;
	}

	@JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PersistentToken> persistentTokens = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "byUser")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataComment> byUser = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "byUser")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<DataProviderTag> byUsers = new HashSet<>();

    @Column(name="sent")
    private Boolean sent;

    @Column(name="privacy")
    private String privacy;

    @Column(name="data_requirements")
    private String dataRequirements;

    @Column(name="mndatrial")
    private boolean mndatrial;

    @Column(name="consent")
    private boolean consent;

    @Column(name="mail_sent")
    private boolean mailSent;

    @Column(name="email_tour_sub")
    private boolean emailTourSub;

    @Column(name="user_role_onboarding")
    private boolean userRoleOnboarding;

    @Column(name="data_profile")
    private boolean dataProfile;

    @Column(name="compliance")
    private boolean compliance;

    @Column(name="investors_page")
    private boolean investorsPage;

    @Column(name="trial_agreement")
    private boolean trialAgreement;

    @Column(name="license_agreement")
    private boolean licenseAgreement;

    @Column(name="support_agreement")
    private boolean supportAgreement;

    @Column(name="sell_my_data")
    private boolean sellMyData;

    @Column(name="providers_page")
    private boolean providersPage;

    public boolean isEmailTourSub() {
		return emailTourSub;
	}

	public void setEmailTourSub(boolean emailTourSub) {
		this.emailTourSub = emailTourSub;
	}

	public boolean isOnBoarding() {
		return onBoarding;
	}

	public boolean isMyMatchesCount() {
		return myMatchesCount;
	}

	public void setMyMatchesCount(boolean myMatchesCount) {
		this.myMatchesCount = myMatchesCount;
	}

	public void setOnBoarding(boolean onBoarding) {
		this.onBoarding = onBoarding;
	}

	@Column(name="on_boardings")
    private boolean onBoarding;

	@Transient
	@JsonSerialize
	private boolean myMatchesCount;

    public boolean isMailSent() {
		return mailSent;
	}

	public void setMailSent(boolean mailSent) {
		this.mailSent = mailSent;
	}
	@ManyToOne
	@JoinColumn(name="partnership_id")
    private LookupCode partnership;

    public boolean isMndatrial() {
		return mndatrial;
	}

	public void setMndatrial(boolean mndatrial) {
		this.mndatrial = mndatrial;
	}

	public LookupCode getPartnership() {
		return partnership;
	}

	public void setPartnership(LookupCode partnership) {
		this.partnership = partnership;
	}

	public String getIntroductions() {
		return introductions;
	}

	public void setIntroductions(String introductions) {
		this.introductions = introductions;
	}

	public String getOrgvisibility() {
		return orgvisibility;
	}

	public boolean isConsent() {
		return consent;
	}

	public void setConsent(boolean consent) {
		this.consent = consent;
	}

	public void setOrg_visibility(String orgvisibility) {
		this.orgvisibility = orgvisibility;
	}

	@Column(name="introductions")
    private String introductions;

    @Column(name="org_visibility")
    private String orgvisibility;

    public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	@Column(name="visibility")
    private String visibility;

	@Column(name="provider_name")
	private String providerName;

	@Column(name="provider_website")
	private String providerWebsite;

    public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProviderWebsite() {
		return providerWebsite;
	}

	public void setProviderWebsite(String providerWebsite) {
		this.providerWebsite = providerWebsite;
	}

	public String getPrivacy() {
		return privacy;
	}

	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}

	public String getDataRequirements() {
		return dataRequirements;
	}

	public void setDataRequirements(String dataRequirements) {
		this.dataRequirements = dataRequirements;
	}

	public String getDataRequests() {
		return dataRequests;
	}

	public void setDataRequests(String dataRequests) {
		this.dataRequests = dataRequests;
	}

	public Boolean getUserOnboarding() {
		return userOnboarding;
	}

	public void setUserOnboarding(Boolean userOnboarding) {
		this.userOnboarding = userOnboarding;
	}

	public Set<LookupCode> getGeographies() {
		return geographies;
	}

	public Boolean getMymatchSearch() {
		return mymatchSearch;
	}

	public void setMymatchSearch(Boolean mymatchSearch) {
		this.mymatchSearch = mymatchSearch;
	}

	public void setGeographies(Set<LookupCode> geographies) {
		this.geographies = geographies;
	}

	@Column(name="data_requests")
    private String dataRequests;

    @Column(name="user_onboarding")
    private Boolean userOnboarding;

    @Column(name="mymatch_search")
    private Boolean mymatchSearch;

    public boolean isUserRoleOnboarding() {
		return userRoleOnboarding;
	}

	public boolean isDataProfile() {
		return dataProfile;
	}

	public boolean isCompliance() {
		return compliance;
	}

	public boolean isInvestorsPage() {
		return investorsPage;
	}

	public boolean isTrialAgreement() {
		return trialAgreement;
	}

	public boolean isLicenseAgreement() {
		return licenseAgreement;
	}

	public boolean isSupportAgreement() {
		return supportAgreement;
	}

	public boolean isSellMyData() {
		return sellMyData;
	}

	public boolean isProvidersPage() {
		return providersPage;
	}

	public void setUserRoleOnboarding(boolean userRoleOnboarding) {
		this.userRoleOnboarding = userRoleOnboarding;
	}

	public void setDataProfile(boolean dataProfile) {
		this.dataProfile = dataProfile;
	}

	public void setCompliance(boolean compliance) {
		this.compliance = compliance;
	}

	public void setInvestorsPage(boolean investorsPage) {
		this.investorsPage = investorsPage;
	}

	public void setTrialAgreement(boolean trialAgreement) {
		this.trialAgreement = trialAgreement;
	}

	public void setLicenseAgreement(boolean licenseAgreement) {
		this.licenseAgreement = licenseAgreement;
	}

	public void setSupportAgreement(boolean supportAgreement) {
		this.supportAgreement = supportAgreement;
	}

	public void setSellMyData(boolean sellMyData) {
		this.sellMyData = sellMyData;
	}

	public void setProvidersPage(boolean providersPage) {
		this.providersPage = providersPage;
	}

	public void setOrgvisibility(String orgvisibility) {
		this.orgvisibility = orgvisibility;
	}

	public Boolean getSent() {
        return sent;
    }

    public void setSent(Boolean sent) {
        this.sent = sent;
    }


    public Set<DataProviderTag> getByUsers() {
        return byUsers;
    }

    public void setByUsers(Set<DataProviderTag> byUsers) {
        this.byUsers = byUsers;
    }

    public Set<DataComment> getByUser() {
        return byUser;
    }

    public void setByUser(Set<DataComment> byUser) {
        this.byUser = byUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    //Lowercase the login before saving it in database
    public void setLogin(String login) {
        this.login = StringUtils.lowerCase(login, Locale.ENGLISH);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean getActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public String getResetKey() {
        return resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public Instant getResetDate() {
       return resetDate;
    }

    public void setResetDate(Instant resetDate) {
       this.resetDate = resetDate;
    }
    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public Set<PersistentToken> getPersistentTokens() {
        return persistentTokens;
    }

    public void setPersistentTokens(Set<PersistentToken> persistentTokens) {
        this.persistentTokens = persistentTokens;
    }

    @Column(name="partnership")
    private boolean partnersCheckList;

    public boolean isPartnersCheckList() {
		return partnersCheckList;
	}

	public void setPartnersCheckList(boolean partnersCheckList) {
		this.partnersCheckList = partnersCheckList;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        User user = (User) o;
        return !(user.getId() == null || getId() == null) && Objects.equals(getId(), user.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "User{" +
            "id='" + id + '\'' +
            ",login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", company='" + company + '\'' +
            ", role='" + role + '\'' +
            ", authorities='" + authorities + '\'' +
            ", email='" + email + '\'' +
            ", imageUrl='" + imageUrl + '\'' +
            ", activated='" + activated + '\'' +
            ", langKey='" + langKey + '\'' +
            ", professional_investor=" + professional_investor +
            ", interest_sell_data_product=" + interest_sell_data_product +
            ", notify_news_update=" + notify_news_update +
            ", resetKey=" + resetKey +
            ", activationKey='" + activationKey + '\'' +
            ", sent='" + sent + '\'' +
             ", privacy='" + privacy + '\'' +
             ", dataRequests='" + dataRequests + '\'' +
             ", dataRequirements='" + dataRequirements + '\'' +
              ", userOnboarding='" + userOnboarding + '\'' +
              ", assetClasses='" + assetClasses + '\'' +
              ", sectors='" + sectors + '\'' +
               ", geographies='" + geographies + '\'' +
               ", fundName='" + fundName + '\'' +
                ", mndatrial ='" + mndatrial  + '\'' +
                ", partnership ='" + partnership  + '\'' +
                ", introductions ='" + introductions  + '\'' +
                ",orgvisibility ='" +orgvisibility  + '\'' +
                ",visibility ='" +visibility + '\'' +
                 ",providerName ='" +providerName + '\'' +
                 ",providerWebsite ='" +providerWebsite + '\'' +
                  ",consent ='" +consent + '\'' +
                   ",mailSent ='" +mailSent + '\'' +
                   ",onBoarding ='" +onBoarding + '\'' +
                   ",emailTourSub ='" +emailTourSub + '\'' +
                   ",myMatchesCount ='" +myMatchesCount + '\'' +
                   ",mymatchSearch ='" +mymatchSearch + '\'' +
                   ",userRoleOnboarding ='" +userRoleOnboarding + '\'' +
                   ",dataProfile ='" +dataProfile + '\'' +
                   ",compliance ='" +compliance + '\'' +
                   ",investorsPage ='" +investorsPage + '\'' +
                   ",trialAgreement ='" +trialAgreement + '\'' +
                   ",licenseAgreement ='" +licenseAgreement + '\'' +
                   ",supportAgreement ='" +supportAgreement + '\'' +
                   ",sellMyData ='" +sellMyData + '\'' +
                   ",providersPage ='" +providersPage + '\'' +
                   ",partnersCheckList ='" +partnersCheckList + '\'' +


            "}";
    }
}
