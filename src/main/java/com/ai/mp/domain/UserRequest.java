package com.ai.mp.domain;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "mp_user_request")
public class UserRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
  private Long userId;
  private String userName;
  private String userEmail;
  private String company;
  private String message;

  @Column(name = "confirmation_key")
  private String confirmationkey;

  private int requestType;
  private LocalDateTime createdDate;
  private Instant updatedDate;




    public String getConfirmationkey() {
	return confirmationkey;
}

public void setConfirmationkey(String confirmationkey) {
	this.confirmationkey = confirmationkey;
}

    public long getProviderID() {
        return providerID;
    }

    public void setProviderID(long providerID) {
        this.providerID = providerID;
    }

    public long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(long categoryID) {
        this.categoryID = categoryID;
    }

    @Column(name = "provider_id")
    private long providerID;

    @Column(name = "category_id")
    private long categoryID;

    public String getRequestTopic() {
        return requestTopic;
    }

    public void setRequestTopic(String requestTopic) {
        this.requestTopic = requestTopic;
    }

    private String requestTopic;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserEmail() {
    return userEmail;
  }

  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public int getRequestType() {
    return requestType;
  }

  public void setRequestType(int interestType) {
    this.requestType = interestType;
  }

  public LocalDateTime getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(LocalDateTime createdDate) {
    this.createdDate = createdDate;
  }

  public Instant getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Instant updatedDate) {
    this.updatedDate = updatedDate;
  }

    @Override
    public String toString() {
        return "UserRequest{" +
            "id=" + id +
            ", userId=" + userId +
            ", userName='" + userName + '\'' +
            ", userEmail='" + userEmail + '\'' +
            ", company='" + company + '\'' +
            ", message='" + message + '\'' +
            ", confirmationkey='" + confirmationkey + '\'' +
            ", requestType=" + requestType +
            ", createdDate=" + createdDate +
            ", updatedDate=" + updatedDate +
            ", providerID=" + providerID +
            ", categoryID=" + categoryID +
            ", requestTopic='" + requestTopic + '\'' +
            '}';
    }
}
