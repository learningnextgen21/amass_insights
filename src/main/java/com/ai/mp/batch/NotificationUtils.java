package com.ai.mp.batch;

import static com.ai.core.Constants.BASE_URL;
import static com.ai.core.Constants.INTEREST_TYPE_FOLLOW;
import static com.mailjet.client.resource.Emailv31.Message.NAME;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.ai.core.StrUtils;
import com.ai.mp.config.ApplicationProperties;
import com.ai.mp.domain.DataArticle;
import com.ai.mp.domain.DataEvent;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.Email;
import com.ai.mp.domain.EmailAddress;
import com.ai.mp.domain.User;
import com.ai.mp.repository.DataEventRepository;
import com.ai.mp.repository.EmailAddressRepository;
import com.ai.mp.repository.EmailRepository;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.repository.search.DataArticleSearchRepository;
import com.ai.mp.repository.search.DataEventSearchRepository;
import com.ai.mp.repository.search.DataProviderSearchRepository;
import com.ai.mp.service.ElasticsearchGenericService;
import com.ai.mp.service.MailJetService;
import com.ai.mp.service.UserProviderService;
import com.codahale.metrics.annotation.Timed;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.github.jhipster.config.JHipsterProperties;

@Service
public class NotificationUtils {
    private final JHipsterProperties jHipsterProperties;
    private final UserRepository userRepository;
    private final ApplicationProperties applicationProperties;
    private final ElasticsearchGenericService elasticsearchService;
    private final DataProviderSearchRepository dataProviderSearchRepository;
    private final DataArticleSearchRepository dataArticleSearchRepository;
    private final UserProviderService userProviderService;
    private final MessageSource messageSource;
    private final MailJetService mailJetClient;
    private final DataEventSearchRepository dataEventSearchRepository;
    private final DataEventRepository dataEventRepository;
    private final Environment environment;
    private final EmailAddressRepository emailAddressRepository;
    private final EmailRepository emailRepository;

    public NotificationUtils(JHipsterProperties jHipsterProperties, ElasticsearchGenericService elasticsearchGenericService, UserRepository userService, DataProviderSearchRepository dataProviderSearchRepository, DataArticleSearchRepository dataArticleSearchRepository, UserProviderService userProviderService, MessageSource messageSource, MailJetService client, ApplicationProperties applicationProperties, DataEventSearchRepository dataEventSearchRepository, DataEventRepository dataEventRepository, Environment environment, EmailAddressRepository emailAddressRepository,EmailRepository emailRepository) {
        this.jHipsterProperties = jHipsterProperties;
        this.userRepository = userService;
        this.dataProviderSearchRepository = dataProviderSearchRepository;
        this.dataArticleSearchRepository = dataArticleSearchRepository;
        this.userProviderService = userProviderService;
        this.messageSource = messageSource;
        this.applicationProperties = applicationProperties;
        this.mailJetClient = client;
        this.dataEventSearchRepository = dataEventSearchRepository;
        this.dataEventRepository = dataEventRepository;
        this.environment = environment;
        this.elasticsearchService = elasticsearchGenericService;
        this.emailAddressRepository = emailAddressRepository;
        this.emailRepository=emailRepository;
    }

    @Timed
    public void generateGenericNotifications(String fullName,int pastDays,String mailTo)
    {
        //Get all active users

        int past=pastDays-1;
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -1*past - 1);

        //Get New Articles
        double newArticleCount = dataArticleSearchRepository.countByPublishedDateAfter(calendar.getTime());

        //Get New Events
        double newEventCount = dataEventSearchRepository.countByCreatedTimeAfter(calendar.getTime());

        //Get New providers
        double newProviderCount = dataProviderSearchRepository.countByCreatedDateAfter(calendar.getTime());

        //Get Changed providers
        double updatedProviderCount = dataProviderSearchRepository.countByUpdatedDateAfter(calendar.getTime());

        //Get Changed providers
        double updatedEventCount = dataEventSearchRepository.countByLastUpdatedAfter(calendar.getTime());

        //Get Changed providers
        //  double updatedEventCount = dataEventSearchRepository.countByLastUpdatedAfter(calendar.getTime());

        List<DataArticle> alternateDataArticlesList = dataArticleSearchRepository.getByArticleTopicsIdEqualsAndPublishedDateGreaterThanEqualOrderByIncludeInNewsletterDesc(2, calendar.getTime());
        List<DataEvent> alternateDataEventCount = dataEventRepository.getAllByEventTopicsId(past);
        List<DataEvent> updatedEventCountList = dataEventRepository.getAllByEventTopicsIdAndLastupdatedDate();
        List<DataEvent> futureAlternativeDataEventsCount = dataEventRepository.getAllByEventTopicsIdAndAfterNow();
        JSONArray alternateDataArticles = getArticlesJSon(alternateDataArticlesList);
        JSONArray alternateDataEvents = getEventsJSon(alternateDataEventCount);
        JSONArray upcomingEvents = getEventsJSon(updatedEventCountList);

        calendar.add(Calendar.DATE, 1);
        double fd = calendar.getTime().getTime();
        long fDate=(long) fd;
        // System.out.println("first date "+ fDate);
        String filterDate=String.valueOf(fDate);

        Date currentDate = new Date();
        Calendar cal = Calendar.getInstance();
        calendar.setTime(currentDate);
        double cd = cal.getTime().getTime();
        long cDate=(long)cd;
        // System.out.println("Current date "+ cDate);
        String curDate=String.valueOf(cDate);

        cal.add(Calendar.DATE, +7);
        double od = cal.getTime().getTime();
        long oDate=(long)od;
        // System.out.println("On week date "+ oDate);
        String owfDate=String.valueOf(oDate);

        //System.out.println("futureAlternativeDataEventsCount =" +futureAlternativeDataEventsCount.size());
        String subject = messageSource.getMessage("email.weekly.subject", new Double[]{newProviderCount, newArticleCount, newEventCount}, Locale.US);
        JSONObject variables = new JSONObject();

        /*********Recent Start*********/
        JsonObject result = elasticsearchService.providerSearch("recent");
        Gson gson = new Gson();
        JsonArray hitsArray = result.getAsJsonObject("hits").getAsJsonArray("hits");
        JSONArray recentProvidersArray = new JSONArray();
        for (JsonElement e : hitsArray) {
            JsonObject obj = e.getAsJsonObject();
            JsonObject sourceObject = obj.getAsJsonObject("_source");
            DataProvider recentProviders = gson.fromJson(sourceObject, DataProvider.class);
            String cat = recentProviders.getMainDataCategory().getDatacategory();
            recentProvidersArray.put(new JSONObject()
                    .put("recordID", recentProviders.getRecordID())
                    .put("providerName", recentProviders.getProviderName())
                    .put("shortDescription", recentProviders.getShortDescription())
                    .put("mainDataCategory", cat)
                //      .put("logo", logo)
            );
        }
        /*********Recent End*********/

        /*********Featured Start*********/

        JsonObject result2 = elasticsearchService.providerSearch("featured");
        Gson gson2 = new Gson();
        JsonArray hitsArray2 = result2.getAsJsonObject("hits").getAsJsonArray("hits");
        JSONArray featuredProvidersArray = new JSONArray();
        for (JsonElement e : hitsArray2) {
            JsonObject obj2 = e.getAsJsonObject();
            JsonObject sourceObject = obj2.getAsJsonObject("_source");
            DataProvider featuredProviders = gson2.fromJson(sourceObject, DataProvider.class);
            String cat2 = featuredProviders.getMainDataCategory().getDatacategory();
            featuredProvidersArray.put(new JSONObject()
                    .put("recordID", featuredProviders.getRecordID())
                    .put("providerName", featuredProviders.getProviderName())
                    .put("shortDescription", featuredProviders.getShortDescription())
                    .put("mainDataCategory", cat2)
                //      .put("logo", logo)
            );
        }
        /*********Featured End*********/

        variables.put("userName", fullName);
        variables.put("pastDays", Double.valueOf(pastDays));
        variables.put("newProviderCount", newProviderCount);
        variables.put("updatedProviderCount", updatedProviderCount);
        variables.put("newArticleCount", newArticleCount);
        variables.put("alternateDataArticles", alternateDataArticles);
        variables.put("alternateDataArticlesCount", alternateDataArticlesList.size());
        variables.put("newEventCount", newEventCount);
        variables.put("updatedEventCount", updatedEventCount);
        variables.put("alternateDataEventCount", alternateDataEventCount.size());
        variables.put("alternateDataEvents", alternateDataEvents);
        variables.put("upcomingEventsCount", updatedEventCountList.size());
        variables.put("upcomingEvents", upcomingEvents);
        variables.put("recentProviders", recentProvidersArray);
        variables.put("featuredProviders", featuredProvidersArray);
        variables.put(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        variables.put("filterDate", filterDate);
        variables.put("oneWeekFromToday", owfDate);
        variables.put("futureAlternativeDataEventsCount", futureAlternativeDataEventsCount.size());
        variables.put("currentDate", curDate);
        //System.out.println("Mail to " +mailTo);
        mailJetClient.sendMail(mailTo, fullName, 484653, subject, variables); //REST APIT CALL
        String body= mailJetClient.getMail(484653);
        Email email=new Email();
        email.setSubject(subject);
        email.setEmailAdress(mailTo);
        email.setNotificationStatus(1);
        LocalDateTime now = LocalDateTime.now();  
        email.setSentDate(now);
        email.setCreatedDate(now);
        email.setTemplateId(484653);
        if(null!=body)
        {
        	email.setBody(body);
        }
        emailRepository.save(email);
    }

/*
    @Timed
    public void sendallNotifications(int pastDays,boolean test)
    {
        String mailTo = "admin@amassinsights.com";
        List<User> users = userRepository.findByActivatedIsTrue();

        for(User user : users) {
            if (user.isNotify_news_update() && !test) {
                generateNotifications(pastDays, user.getLogin(), user.getLogin());
            }
            else if (user.isNotify_news_update() && test) {
                generateNotifications(pastDays, user.getLogin(), mailTo);
            }
        }


    }*/


    @Timed
    public void sendallNotifications(int pastDays,boolean test)
    {
        String mailTo = "admin@amassinsights.com";
        //List<EmailAddress> emailAddress = emailAddressRepository.findByActiveIsTrue();

        List<EmailAddress> emailAddress = emailAddressRepository.findEmail();

        // EmailAddress emailAddress=emailAddressRepository.findEmailAddressByEmailAddressEqualsAndActiveIsTrue(mailTo);
        for(EmailAddress email : emailAddress) {
            User user = userRepository.findUserByLoginEqualsAndActivatedIsTrue(email.getEmailAddress());
            if (user != null) {
                if (!test) {
                    generateNotifications(pastDays, user.getLogin(), user.getLogin());
                } else if (test) {
                    generateNotifications(pastDays, user.getLogin(), mailTo);
                }
            } else {
               /* EmailAddress activeEmail = emailAddressRepository.findEmailAddressByEmailAddressEqualsAndActiveIsTrue(email.getEmailAddress());
                if (activeEmail != null) {
                    generateGenericNotifications("", pastDays, activeEmail.getEmailAddress());
                }*/
                generateGenericNotifications("", pastDays, email.getEmailAddress());
            }

        }
    }

    @Timed
    public void generateNotifications(int pastDays, String loginID, String mailTo)
    {
        //Get all active users


        if(StrUtils.isBlank(mailTo))
        {
            mailTo = loginID;
        }
        int past=pastDays-1;
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -1*past - 1);
        // calendar.add(Calendar.DATE, -1*(pastDays-1));
        //Get New Articles
        double newArticleCount = dataArticleSearchRepository.countByPublishedDateAfter(calendar.getTime());

        //Get New Events
        double newEventCount = dataEventSearchRepository.countByCreatedTimeAfter(calendar.getTime());

        //Get New providers
        double newProviderCount = dataProviderSearchRepository.countByCreatedDateAfter(calendar.getTime());

        //Get Changed providers
        double updatedProviderCount = dataProviderSearchRepository.countByUpdatedDateAfter(calendar.getTime());

        //Get Changed providers
        double updatedEventCount = dataEventSearchRepository.countByLastUpdatedAfter(calendar.getTime());

        List<String> emailIDs = new ArrayList<>();//mailJetClient.getSubscriberList();
        emailIDs.add(loginID);
        for(String email : emailIDs) {
            User user = userRepository.findUserByLoginEqualsAndActivatedIsTrue(email);
            if (user != null) {

//            List<Long> cids = user.getCategories().stream().map(DataCategory::getId).collect(Collectors.toList());
                List<Long> followedProviderIDs = userProviderService.getProviderIDsByInterestTypeByUser(user.getId(), INTEREST_TYPE_FOLLOW);

                JSONArray followedProviderUpdates = new JSONArray();
                List<DataProvider> followedProvidersList = new ArrayList<>();
                if (followedProviderIDs.size() > 0) {
                    List<DataProvider> followedProviderUpdatesList = dataProviderSearchRepository.getByUpdatedDateAfterAndIdIn(calendar.getTime(), followedProviderIDs);


                    for(DataProvider p : followedProviderUpdatesList)
                    {
                        followedProviderUpdates.put(new JSONObject()
                            .put("ID", p.getRecordID())
                            .put(NAME, p.getProviderName()));
                    }
                    followedProvidersList = dataProviderSearchRepository.getByIdIn(followedProviderIDs);
                }

                JSONArray followedProviderArticles = new JSONArray();
                double followedProvidersArticleCount = 0;
                for (DataProvider dp : followedProvidersList) {
                    List<DataArticle> articles = dataArticleSearchRepository.getByRelevantProvidersIdEqualsAndPublishedDateGreaterThanEqualOrderByIncludeInNewsletterDesc(dp.getId(), calendar.getTime());
                    if (articles.size() > 0) {
                        JSONArray articlesArray = getArticlesJSon(articles);
                        followedProviderArticles
                            .put(new JSONObject().put("ID", dp.getRecordID())
                                .put(NAME, dp.getProviderName())
                                .put("articles", articlesArray));
                    }
                    followedProvidersArticleCount = followedProvidersArticleCount + articles.size();
                }

                List<DataArticle> alternateDataArticlesList = dataArticleSearchRepository.getByArticleTopicsIdEqualsAndPublishedDateGreaterThanEqualOrderByIncludeInNewsletterDesc(2, calendar.getTime());
                List<DataEvent> alternateDataEventCount= dataEventRepository.getAllByEventTopicsId(past);
                List<DataEvent> updatedEventCountList= dataEventRepository.getAllByEventTopicsIdAndLastupdatedDate();
                List<DataEvent> futureAlternativeDataEventsCount= dataEventRepository.getAllByEventTopicsIdAndAfterNow();
                //System.out.println("futureAlternativeDataEventsCount =" +futureAlternativeDataEventsCount.size());


                JSONArray alternateDataArticles = getArticlesJSon(alternateDataArticlesList);
                JSONArray alternateDataEvents = getEventsJSon(alternateDataEventCount);
                JSONArray upcomingEvents = getEventsJSon(updatedEventCountList);

                calendar.add(Calendar.DATE, 1 );
                double fd = calendar.getTime().getTime();
                long fDate=(long)fd;
                String filterDate=String.valueOf(fDate);

                Date currentDate = new Date();
                Calendar cal = Calendar.getInstance();
                calendar.setTime(currentDate);
                double cd= cal.getTime().getTime();
                long cDate=(long)cd;
                String curDate=String.valueOf(cDate);

                cal.add(Calendar.DATE, +7);
                double od= cal.getTime().getTime();
                long oDate=(long)od;
                String owfDate=String.valueOf(oDate);
                /*System.out.println("oneWeekFromToday =" +owfDate);
               // System.out.println("currentDate =" +curDate);*/
                String subject = messageSource.getMessage("email.weekly.subject", new Double[]{newProviderCount, newArticleCount,newEventCount}, Locale.US);
                JSONObject variables = new JSONObject();

                /*********Recent Start*********/
                JsonObject result = elasticsearchService.providerSearch("recent");
                Gson gson = new Gson();
                JsonArray hitsArray = result.getAsJsonObject("hits").getAsJsonArray("hits");
                JSONArray recentProvidersArray = new JSONArray();
                for (JsonElement e : hitsArray) {
                    JsonObject obj = e.getAsJsonObject();
                    JsonObject sourceObject = obj.getAsJsonObject("_source");
                    DataProvider recentProviders = gson.fromJson(sourceObject, DataProvider.class);
                    String cat=recentProviders.getMainDataCategory().getDatacategory();
                    recentProvidersArray.put(new JSONObject()
                            .put("recordID", recentProviders.getRecordID())
                            .put("providerName", recentProviders.getProviderName())
                            .put("shortDescription", recentProviders.getShortDescription())
                            .put("mainDataCategory", cat)
                        //      .put("logo", logo)
                    );
                }
                /*********Recent End*********/

                /*********Featured Start*********/

                JsonObject result2 = elasticsearchService.providerSearch("featured");
                Gson gson2 = new Gson();
                JsonArray hitsArray2 = result2.getAsJsonObject("hits").getAsJsonArray("hits");
                JSONArray featuredProvidersArray = new JSONArray();
                for (JsonElement e : hitsArray2) {
                    JsonObject obj2 = e.getAsJsonObject();
                    JsonObject sourceObject = obj2.getAsJsonObject("_source");
                    DataProvider featuredProviders = gson2.fromJson(sourceObject, DataProvider.class);
                    String cat2=featuredProviders.getMainDataCategory().getDatacategory();
                    featuredProvidersArray.put(new JSONObject()
                            .put("recordID", featuredProviders.getRecordID())
                            .put("providerName", featuredProviders.getProviderName())
                            .put("shortDescription", featuredProviders.getShortDescription())
                            .put("mainDataCategory", cat2)
                        //      .put("logo", logo)
                    );
                }

                /*********Featured End*********/

                variables.put("userName", user.getFirstName());
                variables.put("pastDays", Double.valueOf(pastDays));
                variables.put("newProviderCount", newProviderCount);
                variables.put("updatedProviderCount", updatedProviderCount);
                variables.put("newArticleCount", newArticleCount);
                variables.put("newEventCount", newEventCount);
                variables.put("updatedEventCount", updatedEventCount);
                variables.put("alternateDataEventCount", alternateDataEventCount.size());
                variables.put("alternateDataEvents", alternateDataEvents);
                variables.put("upcomingEventsCount", updatedEventCountList.size());
                variables.put("upcomingEvents", upcomingEvents);
                variables.put("alternateDataArticles", alternateDataArticles);
                variables.put("alternateDataArticlesCount", alternateDataArticlesList.size());
                variables.put("followedProviderUpdateCount", followedProviderUpdates.length());
                variables.put("followedProviderUpdates", followedProviderUpdates);
                variables.put("followedProviderArticles", followedProviderArticles);
                variables.put("followedProvidersArticleCount", followedProvidersArticleCount);
                variables.put("userName", user.getFirstName());
                variables.put("recentProviders", recentProvidersArray);
                variables.put("featuredProviders", featuredProvidersArray);
                variables.put(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
                variables.put("filterDate", filterDate);
                variables.put("oneWeekFromToday", owfDate);
                variables.put("currentDate", curDate);
                variables.put("futureAlternativeDataEventsCount", futureAlternativeDataEventsCount.size());
                int template = Integer.parseInt(environment.getProperty("spring.mailjet.notification-templateID"));
                //System.out.println("Mail to " +mailTo);
               mailJetClient.sendMail(mailTo, user.getFirstName(), template, subject, variables); //REST APIT CALL
              String body= mailJetClient.getMail(template);
                Email emailValue=new Email();
                emailValue.setSubject(subject);
                emailValue.setEmailAdress(mailTo);
                emailValue.setNotificationStatus(1);
                LocalDateTime now = LocalDateTime.now();  
                emailValue.setSentDate(now);
                emailValue.setCreatedDate(now);
                emailValue.setTemplateId(template);
                if(null!=body)
                {
                	emailValue.setBody(body);
                }
                emailRepository.save(emailValue);
            }
        }
    }



    private JSONArray getArticlesJSon(List<DataArticle> articles)
    {
        JSONArray articlesArray = new JSONArray();
        for(int i = 0; i < articles.size(); i++)
        {
            DataArticle article = articles.get(i);
            String author = "";
            if (StrUtils.isNotBlank(article.getAuthorDataProvider())) {
                if (!"RECORD_NULL".equals(article.getAuthorDataProvider().getProviderName())) {
                    author = article.getAuthorDataProvider().getProviderName();
                }
            }
            if(StrUtils.isBlank(author))
            {
                if (StrUtils.isNotBlank(article.getAuthorOrganization())) {
                    if (!"NAME_NULL".equals(article.getAuthorOrganization().getName())) {
                        author = article.getAuthorOrganization().getName();
                    }
                }
            }
            if (StrUtils.isBlank(author)) {
                author = article.getStream();
            }
            if (StrUtils.isBlank(author)) {
                author = article.getAuthor();
            }
            article.setAuthor(author);

            articlesArray.put(new JSONObject()
                .put("TITLE", article.getTitle())
                .put("AUTHOR", article.getAuthor())
                .put("URL", article.getUrl())
            );
        }
        return articlesArray;
    }
//********************************Commented by Ranjith***********************
    /*private JSONArray getEventsJSon(List<DataEvent> events)
    {
        JSONArray eventsArray = new JSONArray();
        for(int i = 0; i < events.size(); i++)
        {
            DataEvent event = events.get(i);
            String eventEditionName = "";

            eventsArray.put(new JSONObject()
                .put("eventEditionName", event.getEventEditionNames())
                .put("startTime", event.getStartTime())
                .put("city", event.getCity())
                .put("url",event.getLink())
                .put("eventRecordId",event.getRecordID())
              //  .put("organizationOrganizer ",event.getOrganizationOrganizer())
            );
        }
        return eventsArray;
    }*/

    private JSONArray getEventsJSon(List<DataEvent> events)
    {
        JSONArray eventsArray = new JSONArray();
        for(int i = 0; i < events.size(); i++)
        {
            String today="";
            DataEvent event = events.get(i);
            Date date=event.getStartTime();
            DateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
            if (date!=null) {
                today = formatter.format(date);
            }
            eventsArray.put(new JSONObject()
                    .put("eventEditionName", event.getEventEditionNames())
                    .put("startTime", today)
                    .put("city", event.getCity())
                    .put("url",event.getLink())
                    .put("eventRecordId",event.getRecordID())
                //  .put("organizationOrganizer ",event.getOrganizationOrganizer())
            );
        }
        return eventsArray;
    }

    public void sendmailjetnotification(int mailjetTemplateID, String emailAddressQuery) {
        // TODO Auto-generated method stub
        List<EmailAddress> emailAddress = emailAddressRepository.findEmail();

        // EmailAddress emailAddress=emailAddressRepository.findEmailAddressByEmailAddressEqualsAndActiveIsTrue(mailTo);
        for(EmailAddress email : emailAddress) {
            User user = userRepository.findUserByLoginEqualsAndActivatedIsTrue(email.getEmailAddress());
            if (user != null) {
                generateNotifications(mailjetTemplateID, user.getLogin(), user.getLogin(),emailAddressQuery);

            }

        }
    }
    @Timed
    public void generateNotifications(int mailjetTemplateID, String loginID, String mailTo,String emailAddressQuery)
    {
        //Get all active users

        int pastDays=7;
        if(StrUtils.isBlank(mailTo))
        {
            mailTo = loginID;
        }
        int past=pastDays-1;
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -1*past - 1);
        // calendar.add(Calendar.DATE, -1*(pastDays-1));
        //Get New Articles
        double newArticleCount = dataArticleSearchRepository.countByPublishedDateAfter(calendar.getTime());

        //Get New Events
        double newEventCount = dataEventSearchRepository.countByCreatedTimeAfter(calendar.getTime());

        //Get New providers
        double newProviderCount = dataProviderSearchRepository.countByCreatedDateAfter(calendar.getTime());

        //Get Changed providers
        double updatedProviderCount = dataProviderSearchRepository.countByUpdatedDateAfter(calendar.getTime());

        //Get Changed providers
        double updatedEventCount = dataEventSearchRepository.countByLastUpdatedAfter(calendar.getTime());

        List<String> emailIDs = new ArrayList<>();//mailJetClient.getSubscriberList();
        emailIDs.add(loginID);
        for(String email : emailIDs) {
            User user = userRepository.findUserByLoginEqualsAndActivatedIsTrue(email);
            if (user != null) {

//	            List<Long> cids = user.getCategories().stream().map(DataCategory::getId).collect(Collectors.toList());
                List<Long> followedProviderIDs = userProviderService.getProviderIDsByInterestTypeByUser(user.getId(), INTEREST_TYPE_FOLLOW);

                JSONArray followedProviderUpdates = new JSONArray();
                List<DataProvider> followedProvidersList = new ArrayList<>();
                if (followedProviderIDs.size() > 0) {
                    List<DataProvider> followedProviderUpdatesList = dataProviderSearchRepository.getByUpdatedDateAfterAndIdIn(calendar.getTime(), followedProviderIDs);


                    for(DataProvider p : followedProviderUpdatesList)
                    {
                        followedProviderUpdates.put(new JSONObject()
                            .put("ID", p.getRecordID())
                            .put(NAME, p.getProviderName()));
                    }
                    followedProvidersList = dataProviderSearchRepository.getByIdIn(followedProviderIDs);
                }

                JSONArray followedProviderArticles = new JSONArray();
                double followedProvidersArticleCount = 0;
                for (DataProvider dp : followedProvidersList) {
                    List<DataArticle> articles = dataArticleSearchRepository.getByRelevantProvidersIdEqualsAndPublishedDateGreaterThanEqualOrderByIncludeInNewsletterDesc(dp.getId(), calendar.getTime());
                    if (articles.size() > 0) {
                        JSONArray articlesArray = getArticlesJSon(articles);
                        followedProviderArticles
                            .put(new JSONObject().put("ID", dp.getRecordID())
                                .put(NAME, dp.getProviderName())
                                .put("articles", articlesArray));
                    }
                    followedProvidersArticleCount = followedProvidersArticleCount + articles.size();
                }

                List<DataArticle> alternateDataArticlesList = dataArticleSearchRepository.getByArticleTopicsIdEqualsAndPublishedDateGreaterThanEqualOrderByIncludeInNewsletterDesc(2, calendar.getTime());
                List<DataEvent> alternateDataEventCount= dataEventRepository.getAllByEventTopicsId(past);
                List<DataEvent> updatedEventCountList= dataEventRepository.getAllByEventTopicsIdAndLastupdatedDate();
                List<DataEvent> futureAlternativeDataEventsCount= dataEventRepository.getAllByEventTopicsIdAndAfterNow();
                //System.out.println("futureAlternativeDataEventsCount =" +futureAlternativeDataEventsCount.size());


                JSONArray alternateDataArticles = getArticlesJSon(alternateDataArticlesList);
                JSONArray alternateDataEvents = getEventsJSon(alternateDataEventCount);
                JSONArray upcomingEvents = getEventsJSon(updatedEventCountList);

                calendar.add(Calendar.DATE, 1 );
                double fd = calendar.getTime().getTime();
                long fDate=(long)fd;
                String filterDate=String.valueOf(fDate);

                Date currentDate = new Date();
                Calendar cal = Calendar.getInstance();
                calendar.setTime(currentDate);
                double cd= cal.getTime().getTime();
                long cDate=(long)cd;
                String curDate=String.valueOf(cDate);

                cal.add(Calendar.DATE, +7);
                double od= cal.getTime().getTime();
                long oDate=(long)od;
                String owfDate=String.valueOf(oDate);
	                /*System.out.println("oneWeekFromToday =" +owfDate);
	                System.out.println("currentDate =" +curDate);*/
                String subject = messageSource.getMessage("email.weekly.subject", new Double[]{newProviderCount, newArticleCount,newEventCount}, Locale.US);
                JSONObject variables = new JSONObject();

                /*********Recent Start*********/
                JsonObject result = elasticsearchService.providerSearch("recent");
                Gson gson = new Gson();
                JsonArray hitsArray = result.getAsJsonObject("hits").getAsJsonArray("hits");
                JSONArray recentProvidersArray = new JSONArray();
                for (JsonElement e : hitsArray) {
                    JsonObject obj = e.getAsJsonObject();
                    JsonObject sourceObject = obj.getAsJsonObject("_source");
                    DataProvider recentProviders = gson.fromJson(sourceObject, DataProvider.class);
                    String cat=recentProviders.getMainDataCategory().getDatacategory();
                    recentProvidersArray.put(new JSONObject()
                            .put("recordID", recentProviders.getRecordID())
                            .put("providerName", recentProviders.getProviderName())
                            .put("shortDescription", recentProviders.getShortDescription())
                            .put("mainDataCategory", cat)
                        //      .put("logo", logo)
                    );
                }
                /*********Recent End*********/

                /*********Featured Start*********/

                JsonObject result2 = elasticsearchService.providerSearch("featured");
                Gson gson2 = new Gson();
                JsonArray hitsArray2 = result2.getAsJsonObject("hits").getAsJsonArray("hits");
                JSONArray featuredProvidersArray = new JSONArray();
                for (JsonElement e : hitsArray2) {
                    JsonObject obj2 = e.getAsJsonObject();
                    JsonObject sourceObject = obj2.getAsJsonObject("_source");
                    DataProvider featuredProviders = gson2.fromJson(sourceObject, DataProvider.class);
                    String cat2=featuredProviders.getMainDataCategory().getDatacategory();
                    featuredProvidersArray.put(new JSONObject()
                            .put("recordID", featuredProviders.getRecordID())
                            .put("providerName", featuredProviders.getProviderName())
                            .put("shortDescription", featuredProviders.getShortDescription())
                            .put("mainDataCategory", cat2)
                        //      .put("logo", logo)
                    );
                }

                /*********Featured End*********/

                variables.put("userName", user.getFirstName());
                variables.put("pastDays", Double.valueOf(pastDays));
                variables.put("newProviderCount", newProviderCount);
                variables.put("updatedProviderCount", updatedProviderCount);
                variables.put("newArticleCount", newArticleCount);
                variables.put("newEventCount", newEventCount);
                variables.put("updatedEventCount", updatedEventCount);
                variables.put("alternateDataEventCount", alternateDataEventCount.size());
                variables.put("alternateDataEvents", alternateDataEvents);
                variables.put("upcomingEventsCount", updatedEventCountList.size());
                variables.put("upcomingEvents", upcomingEvents);
                variables.put("alternateDataArticles", alternateDataArticles);
                variables.put("alternateDataArticlesCount", alternateDataArticlesList.size());
                variables.put("followedProviderUpdateCount", followedProviderUpdates.length());
                variables.put("followedProviderUpdates", followedProviderUpdates);
                variables.put("followedProviderArticles", followedProviderArticles);
                variables.put("followedProvidersArticleCount", followedProvidersArticleCount);
                variables.put("userName", user.getFirstName());
                variables.put("recentProviders", recentProvidersArray);
                variables.put("featuredProviders", featuredProvidersArray);
                variables.put(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
                variables.put("filterDate", filterDate);
                variables.put("oneWeekFromToday", owfDate);
                variables.put("currentDate", curDate);
                variables.put("futureAlternativeDataEventsCount", futureAlternativeDataEventsCount.size());
                //      int template = Integer.parseInt(environment.getProperty("spring.mailjet.notification-templateID"));
                //System.out.println("Mail to " +mailTo);
               mailJetClient.sendMail(mailTo, user.getFirstName(), mailjetTemplateID, "", variables); //REST APIT CALL
               String body=mailJetClient.getMail(mailjetTemplateID);
                Email emailValue=new Email();
                emailValue.setSubject(subject);
                emailValue.setEmailAdress(mailTo);
                emailValue.setNotificationStatus(1);
                LocalDateTime now = LocalDateTime.now();  
                emailValue.setSentDate(now);
                emailValue.setCreatedDate(now);
                emailValue.setTemplateId(mailjetTemplateID);
                if(null!=body)
                {
                	emailValue.setBody(body);
                }
                emailRepository.save(emailValue);
            }
        }
    }
    public void sendmailjetnotification(int mailjetTemplateID, List<EmailAddress> emailAddress) {
        // TODO Auto-generated method stub

        // EmailAddress emailAddress=emailAddressRepository.findEmailAddressByEmailAddressEqualsAndActiveIsTrue(mailTo);
        for(EmailAddress email : emailAddress) {
            User user = userRepository.findUserByLoginEqualsAndActivatedIsTrue(email.getEmailAddress());
            if (user != null) {
               generateNotifications(mailjetTemplateID, user.getLogin(), user.getLogin(),email.getEmailAddress());

            }

        }
    }
}
