package com.ai.mp.batch;

import com.ai.core.Constants;
import com.ai.mp.config.ApplicationProperties;
import com.ai.core.StrUtils;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.service.ElasticsearchIndexService;
import com.ai.mp.service.MailService;
import com.ai.mp.utils.FileUtils;
import com.codahale.metrics.annotation.Timed;
import org.jooq.*;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.*;
import java.sql.*;
import java.util.*;

import static com.ai.core.Constants.*;
import static java.nio.file.StandardWatchEventKinds.*;
import static org.jooq.impl.DSL.table;

@Service
public class DBUtils {
    private static DSLContext ctx = null;
    private static final Logger log = LoggerFactory.getLogger(DBUtils.class);
    private final Environment environment;
    private final MailService mailService;
    private final ApplicationProperties app;
    private final UserRepository userRepository;
    private final ElasticsearchIndexService elasticsearchIndexService;
    private final DownloadLogo downloadLogo;
    public DBUtils(Environment environment, MailService mailService, ApplicationProperties app, UserRepository userRepository, ElasticsearchIndexService elasticsearchIndexService, DownloadLogo downloadLogo)
    {
        this.app = app;
        this.environment = environment;
        this.mailService = mailService;
        this.userRepository = userRepository;
        this.elasticsearchIndexService = elasticsearchIndexService;
        ctx = DSL.using(environment.getProperty("spring.datasource.url"), environment.getProperty("spring.datasource.username"), environment.getProperty("spring.datasource.password"));
        this.downloadLogo = downloadLogo;
    }

    @PostConstruct
    protected void init() {
        if(System.getProperty("schedule") == null)
        {
            log.info("Skipping File Watcher for Feed...");
            return;
        }
        Runnable fileWatcher = () -> {
            Path dir = Paths.get(app.getDatafolder());
            if(!dir.toFile().exists())
            {
                log.info("File Watcher for Feed not found: {}", app.getDatafolder());
                return;
            }
            try {
                WatchService watcher = FileSystems.getDefault().newWatchService();
                dir.register(watcher, ENTRY_CREATE, ENTRY_MODIFY);
                log.info("Watching: " + dir);
                while (true) {
                    WatchKey key;
                    try {
                        key = watcher.take();
                    } catch (InterruptedException x) {
                        return;
                    }
                    for (WatchEvent<?> event : key.pollEvents()) {
                        WatchEvent.Kind<?> kind = event.kind();

                        // This key is registered only
                        // for ENTRY_CREATE events,
                        // but an OVERFLOW event can
                        // occur regardless if events
                        // are lost or discarded.
                        if (kind == OVERFLOW) {
                            continue;
                        }

                        // The filename is the
                        // context of the event.
                        WatchEvent<Path> ev = (WatchEvent<Path>) event;
                        Path folder = ev.context();
                        Path child = dir.resolve(folder);
                        if (Files.isDirectory(child)) {
                            log.info("New folder: " + child.toFile());
                            try {
                                process(child.toFile());
                            }
                            catch (Exception e)
                            {
                                log.error(e.getMessage(), e);
                            }
                        }
                        boolean valid = key.reset();
                        if (!valid) {
                            log.error("STOPPED watching folder: {}", folder);
                            break;
                        }
                    }
                }
            } catch (Exception x) {
                log.error(x.getMessage(), x);
            }
        };
        Thread t = new Thread(fileWatcher);
        t.setDaemon(true);
        t.start();
    }

    @Timed
    public boolean process(File folder) {
        boolean success = loadData(folder);
        if(success) {
            success =  processStagingData(folder);
        }
        else {
            log.error("Error loading data from folder: " + folder + " data processing STOPPED");
        }
        if(success) {
            success = populateDataModel(folder);
        }
        if(success)
        {
            elasticsearchIndexService.reindexAll();
        }
        mailService.sendEmail("rahul@amassinsights.com", "Processing complete for folder: " + folder, success ? "SUCCESS" : "FAILED", false, false , null);

//        Authority admin = new Authority(AuthoritiesConstants.ADMIN);
//        List<User> admins = userRepository.findAllByAuthoritiesEquals(admin);
//        for(User user : admins)
//        {
//            log.info("User email:" + user.getEmail());
//        }

        return success;
    }
    @Timed
    protected void updateProcessingStatus(String status, File folder)
    {
        ctx.insertInto(Constants.TABLE_FEED_STATUS).set(Constants.FEED_NAME, folder.getAbsolutePath()).set(Constants.FEED_STATUS, status).execute();
    }

    @Timed
    public boolean populateDataModel(File folder) {
        //updateProcessingStatus("PROCESSING_START", folder);
        Connection c = ctx.configuration().connectionProvider().acquire();
        // File dir = new File("E:\\");
        InputStream is=null;
        /* if(dir.compareTo(folder)==0){
             is = FileUtils.getResourceAsStream("scripts/db/update_json_column.ctl");
        }else {
             is = FileUtils.getResourceAsStream("scripts/db/populate_scripts.ctl");
        } */
        is = FileUtils.getResourceAsStream("scripts/db/populate_scripts.ctl");
        String line;
        if (is != null) {
            BufferedReader bufferedReader;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(is));
                while ((line = bufferedReader.readLine()) != null) {
                    line = line.trim();
                    if (!line.startsWith("#")) {
                        log.info("START script: " + line);
                        InputStream script = FileUtils.getResourceAsStream("scripts/db/populate/" + line);
                        Scanner s = new Scanner(script).useDelimiter(";");
                        String query = null;
                        try {
                            while (s.hasNext()) {
                                query = s.next();
                                if(query.startsWith("--"))
                                    continue;
                                if (StrUtils.isNotBlank(query)) {
                                    int updateCount = 0;
                                    Statement stmt = c.createStatement();
                                    if (!stmt.execute(query)) {
                                        updateCount = stmt.getUpdateCount();
                                        log.info("Records updated: " + updateCount);
                                    }
                                    SQLWarning w = stmt.getWarnings();
                                    while (w != null) {
                                        log.info("WARN: " + line + " \nQuery:" + query);
                                        log.info(w.getMessage());
                                        throw new RuntimeException("WARN executing script: " + line + "\n" + w.getMessage());
                                    }
                                }
                            }
                        }
                        catch (DataAccessException | SQLException e)
                        {
                            log.info("Script: " + line + " \nQuery:" + query);
                            log.info(e.getMessage());
                            throw new RuntimeException("ERROR executing script: " + line + "\n" + e.getMessage());
                        }
                        log.info("END script: " + line);
                    }
                }
                updateProcessingStatus("PROCESSING_COMPLETE", folder);
            } catch (IOException e) {
                updateProcessingStatus("PROCESSING_ERROR", folder);
                log.error(e.getMessage(), e);
                return false;
            }
            finally
            {
                if(c != null)
                    ctx.configuration().connectionProvider().release(c);
            }
            log.info("FINISHED processing scripts.");
            log.info("Initiating logo processing...");
            downloadLogo.processFileLogos();
            downloadLogo.downloadLogo();
            log.info("Completed logo processing...");
        }
        return true;
    }

    @Timed
    public boolean populateDataModel() {
        Connection c = ctx.configuration().connectionProvider().acquire();
        InputStream is=null;
        is = FileUtils.getResourceAsStream("scripts/db/update_json_column.ctl");
        /*if (type.equals("provider")){
        is = FileUtils.getResourceAsStream("scripts/db/update_json_column.ctl");
        }else
        {
            is = FileUtils.getResourceAsStream("scripts/db/update_json_column_tag_organization.ctl");
        }*/
        String line;
        if (is != null) {
            BufferedReader bufferedReader;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(is));
                while ((line = bufferedReader.readLine()) != null) {
                    line = line.trim();
                    if (!line.startsWith("#")) {
                        log.info("START script: " + line);
                        InputStream script = FileUtils.getResourceAsStream("scripts/db/populate/" + line);
                        Scanner s = new Scanner(script).useDelimiter(";");
                        String query = null;
                        try {
                            while (s.hasNext()) {
                                query = s.next();
                                if(query.startsWith("--"))
                                    continue;
                                if (StrUtils.isNotBlank(query)) {
                                    int updateCount = 0;
                                    Statement stmt = c.createStatement();
                                    if (!stmt.execute(query)) {
                                        updateCount = stmt.getUpdateCount();
                                        log.info("Records updated: " + updateCount);
                                    }
                                    SQLWarning w = stmt.getWarnings();
                                    while (w != null) {
                                        log.info("WARN: " + line + " \nQuery:" + query);
                                        log.info(w.getMessage());
                                        throw new RuntimeException("WARN executing script: " + line + "\n" + w.getMessage());
                                    }
                                }
                            }
                        }
                        catch (DataAccessException | SQLException e)
                        {
                            log.info("Script: " + line + " \nQuery:" + query);
                            log.info(e.getMessage());
                            throw new RuntimeException("ERROR executing script: " + line + "\n" + e.getMessage());
                        }
                        log.info("END script: " + line);
                    }
                }
                System.out.println("PROCESSING_COMPLETE");
            } catch (IOException e) {
                System.out.println("PROCESSING_ERROR");
                log.error(e.getMessage(), e);
                return false;
            }
            finally
            {
                if(c != null)
                    ctx.configuration().connectionProvider().release(c);
            }
            log.info("FINISHED processing scripts.");
        }
        return true;
    }
    @Timed
    public boolean populateDataModelProvider() {
        Connection c = ctx.configuration().connectionProvider().acquire();
        InputStream ist=null;
        ist= FileUtils.getResourceAsStream("scripts/db/update_json_columns.ctl");
        String lines;
        if (ist != null) {
            BufferedReader bufferedReader;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(ist));
                while ((lines = bufferedReader.readLine()) != null) {
                    lines = lines.trim();
                    if (!lines.startsWith("#")) {
                        log.info("START script: " + lines);
                        InputStream script = FileUtils.getResourceAsStream("scripts/db/populate/" + lines);
                        Scanner s = new Scanner(script).useDelimiter(";");
                        String query = null;
                        try {
                            while (s.hasNext()) {
                                query = s.next();
                                if(query.startsWith("--"))
                                    continue;
                                if (StrUtils.isNotBlank(query)) {
                                    int updateCount = 0;
                                    Statement stmt = c.createStatement();
                                    if (!stmt.execute(query)) {
                                        updateCount = stmt.getUpdateCount();
                                        log.info("Records updated: " + updateCount);
                                    }
                                    SQLWarning w = stmt.getWarnings();
                                    while (w != null) {
                                        log.info("WARN: " + lines + " \nQuery:" + query);
                                        log.info(w.getMessage());
                                        throw new RuntimeException("WARN executing script: " + lines + "\n" + w.getMessage());
                                    }
                                }
                            }
                        }
                        catch (DataAccessException | SQLException e)
                        {
                            log.info("Script: " + lines + " \nQuery:" + query);
                            log.info(e.getMessage());
                            throw new RuntimeException("ERROR executing script: " + lines + "\n" + e.getMessage());
                        }
                        log.info("END script: " + lines);
                    }
                }
                System.out.println("PROCESSING_COMPLETE");
            } catch (IOException e) {
                System.out.println("PROCESSING_ERROR");
                log.error(e.getMessage(), e);
                return false;
            }
            finally
            {
                if(c != null)
                    ctx.configuration().connectionProvider().release(c);
            }
            log.info("FINISHED processing scripts.");
        }
        return true;
  
    }
    @Timed
    public boolean populateDataModelProviderEdit() {
        Connection c = ctx.configuration().connectionProvider().acquire();
        InputStream is=null;
        is = FileUtils.getResourceAsStream("scripts/db/update_provider_edit_columns.ctl");
        /*if (type.equals("provider")){
        is = FileUtils.getResourceAsStream("scripts/db/update_json_column.ctl");
        }else
        {
            is = FileUtils.getResourceAsStream("scripts/db/update_json_column_tag_organization.ctl");
        }*/
        String line;
        if (is != null) {
            BufferedReader bufferedReader;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(is));
                while ((line = bufferedReader.readLine()) != null) {
                    line = line.trim();
                    if (!line.startsWith("#")) {
                        log.info("START script: " + line);
                        InputStream script = FileUtils.getResourceAsStream("scripts/db/populate/" + line);
                        Scanner s = new Scanner(script).useDelimiter(";");
                        String query = null;
                        try {
                            while (s.hasNext()) {
                                query = s.next();
                                if(query.startsWith("--"))
                                    continue;
                                if (StrUtils.isNotBlank(query)) {
                                    int updateCount = 0;
                                    Statement stmt = c.createStatement();
                                    if (!stmt.execute(query)) {
                                        updateCount = stmt.getUpdateCount();
                                        log.info("Records updated: " + updateCount);
                                    }
                                    SQLWarning w = stmt.getWarnings();
                                    while (w != null) {
                                        log.info("WARN: " + line + " \nQuery:" + query);
                                        log.info(w.getMessage());
                                        throw new RuntimeException("WARN executing script: " + line + "\n" + w.getMessage());
                                    }
                                }
                            }
                        }
                        catch (DataAccessException | SQLException e)
                        {
                            log.info("Script: " + line + " \nQuery:" + query);
                            log.info(e.getMessage());
                            throw new RuntimeException("ERROR executing script: " + line + "\n" + e.getMessage());
                        }
                        log.info("END script: " + line);
                    }
                }
                System.out.println("PROCESSING_COMPLETE");
            } catch (IOException e) {
                System.out.println("PROCESSING_ERROR");
                log.error(e.getMessage(), e);
                return false;
            }
            finally
            {
                if(c != null)
                    ctx.configuration().connectionProvider().release(c);
            }
            log.info("FINISHED processing scripts.");
        }
        return true;
    }
    @Timed
    public boolean populateDataModelJson() {
        Connection c = ctx.configuration().connectionProvider().acquire();
        InputStream is=null;
        is = FileUtils.getResourceAsStream("scripts/db/update_json_column_tag_organization.ctl");
        /*if (type.equals("provider")){
        is = FileUtils.getResourceAsStream("scripts/db/update_json_column.ctl");
        }else
        {
            is = FileUtils.getResourceAsStream("scripts/db/update_json_column_tag_organization.ctl");
        }*/
        String line;
        if (is != null) {
            BufferedReader bufferedReader;
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(is));
                while ((line = bufferedReader.readLine()) != null) {
                    line = line.trim();
                    if (!line.startsWith("#")) {
                        log.info("START script: " + line);
                        InputStream script = FileUtils.getResourceAsStream("scripts/db/populate/" + line);
                        Scanner s = new Scanner(script).useDelimiter(";");
                        String query = null;
                        try {
                            while (s.hasNext()) {
                                query = s.next();
                                if(query.startsWith("--"))
                                    continue;
                                if (StrUtils.isNotBlank(query)) {
                                    int updateCount = 0;
                                    Statement stmt = c.createStatement();
                                    if (!stmt.execute(query)) {
                                        updateCount = stmt.getUpdateCount();
                                        log.info("Records updated: " + updateCount);
                                    }
                                    SQLWarning w = stmt.getWarnings();
                                    while (w != null) {
                                        log.info("WARN: " + line + " \nQuery:" + query);
                                        log.info(w.getMessage());
                                        throw new RuntimeException("WARN executing script: " + line + "\n" + w.getMessage());
                                    }
                                }
                            }
                        }
                        catch (DataAccessException | SQLException e)
                        {
                            log.info("Script: " + line + " \nQuery:" + query);
                            log.info(e.getMessage());
                            throw new RuntimeException("ERROR executing script: " + line + "\n" + e.getMessage());
                        }
                        log.info("END script: " + line);
                    }
                }
                System.out.println("PROCESSING_COMPLETE");
            } catch (IOException e) {
                System.out.println("PROCESSING_ERROR");
                log.error(e.getMessage(), e);
                return false;
            }
            finally
            {
                if(c != null)
                    ctx.configuration().connectionProvider().release(c);
            }
            log.info("FINISHED processing scripts.");
        }
        return true;
    }

    @Timed
    public boolean loadData(File folder) {
        Connection c = ctx.configuration().connectionProvider().acquire();

        boolean success = true;
        updateProcessingStatus("LOAD_START", folder);
        String line, loadQuery = null;
        try {
            String currentCatalog = c.getCatalog();
            c.setCatalog("staging_airtable");
            InputStream is = FileUtils.getResourceAsStream("scripts/db/load_files.ctl");

            int tableCount = 0;
            if (is != null) {
                Set<String> files = new HashSet<>(Arrays.asList(folder.list()));
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

                while ((line = bufferedReader.readLine()) != null) {
                    String[] array = line.split("=");
                    String tableName = array[0].trim();
                    String fileName = array[1].trim();
                    String sourceFile = folder + "/" + fileName;
                    File file = new File(sourceFile);
                    if(!file.exists())
                    {
                        log.warn("Skipping file: " + fileName);
                        continue;
                    }
                    files.remove(fileName);
                    if (!tableName.startsWith("#")) {
                        if(!valid(c, tableName, sourceFile))
                        {
                            success = false;
                            continue;
                        }
                        ++tableCount;
                        Table table = table(tableName);
                        ctx.truncate(table).execute();
                        loadQuery = "load data local infile '" + sourceFile + "' into table " + table + " CHARACTER SET utf8mb4 fields terminated by ',' optionally enclosed by '\"' ignore 1 lines";

                        Statement stmt = c.createStatement();
                        stmt.execute("SET NAMES 'utf8mb4'");
                        stmt.execute(loadQuery);
                        log.info("Table : " + table  + " rows loaded : " + stmt.getUpdateCount());
                        SQLWarning w = stmt.getWarnings();
                        while (w != null)
                        {
                            if(w.toString().contains("Data truncated"))
                            {
                                success = false;
                            }
                            log.info("Table: " + table + " :" + w);
                            w = w.getNextWarning();
                        }
                    }
                    else
                    {
                        log.info("Skipped table: " + array[0]);
                    }
                }
                log.info("Total tables loaded: " + tableCount);
                log.info("Files skipped: " + files);
                bufferedReader.close();
            }
            updateProcessingStatus("LOAD_COMPLETE", folder);
            c.setCatalog(currentCatalog);
        } catch(Exception e){
            updateProcessingStatus("LOAD_ERROR", folder);
            log.info(loadQuery);
            log.error(e.getMessage(), e);
            success = false;
        }
        finally
        {
            if(c != null)
                ctx.configuration().connectionProvider().release(c);
        }
        return success;
    }

    @Timed
    protected boolean valid(Connection c, String tableName, String sourceFile) {

        boolean returnValue = true;
        try {
            ResultSet rs = c.getMetaData().getColumns(c.getCatalog(), null, tableName, null);
            String firstLine = new BufferedReader(new FileReader(sourceFile)).readLine();
            if(StrUtils.isBlank(firstLine))
            {
                throw new RuntimeException("Failed to read file: "+ sourceFile);
            }
            String[] fileFields = firstLine.split(",");
            int fieldCount = fileFields.length;
            int i = 0;
            List<String> columns = new ArrayList<String>();
            while(rs.next())
            {
                String o = rs.getString("COLUMN_NAME");
                columns.add(o);
            }
            int columnCount = columns.size();
            if(fieldCount > columnCount)
            {
                log.info("File contains more fields than table");
                log.info("File : " + sourceFile + " table: " + tableName);
                log.info("File field count: " + fieldCount + " Table field count: " + columnCount);
                returnValue = false;
            }
            for(int k = 0; k < (columnCount < fieldCount ? columnCount : fieldCount) ; k++)
            {
                String columnName = columns.get(k).replaceAll("\\P{Print}", "") ;
                String fieldName = fileFields[k].replaceAll("\\P{Print}", "");
                if(!columnName.equalsIgnoreCase(fieldName))
                {
                    log.info("Column name mismatch table: " + tableName + " File field name: \"" + fieldName + "\" Table field name: \"" + columnName + "\"");
                    returnValue = false;
                }
            }
        } catch (SQLException | IOException e) {
            returnValue = false;
            log.error(e.getMessage(), e);
        }
        return returnValue;
    }

    @Timed
    public boolean processStagingData(File folder) {
        boolean success = true;
        updateProcessingStatus("STAGING_START", folder);
        Connection c = ctx.configuration().connectionProvider().acquire();
        try
        {

            String currentCatalog = c.getCatalog();
            c.setCatalog("staging_airtable");

            //Split Article Topic
            success = splitColumnToRows(TABLE_STG_DATA_ARTICLE, RECORD_ID, ARTICLE_TOPICS, TABLE_IM_ARTICLE_TOPIC, ARTICLE_RECORD_ID, TOPIC, c, true);

            //Split Resource Purpose
            success = splitColumnToRows(Constants.TABLE_STG_DATA_ARTICLE, Constants.RECORD_ID, Constants.PURPOSE_RECORDID, Constants.TABLE_IM_RESOURCE_PURPOSE, Resource_RecordID, IM_PURPOSE_RECORDID, c, true);

            //Split Resource Purpose Type
            success = splitColumnToRows(Constants.TABLE_STG_DATA_ARTICLE, Constants.RECORD_ID, Constants.PURPOSE_TYPE_RECORDID, TABLE_IM_RESOURCE_PURPOSE_TYPE, Resource_RecordID, IM_PURPOSE_TYPE_RECORDID, c, true);

            // Split Industry Associated Providers
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.PROVIDER_INDUSTRIES,
                    Constants.TABLE_IM_INDUSTRY_ASSOCIATED_PROVIDER, Constants.PROVIDER_RECORD_ID, Constants.INDUSTRY_RECORD_ID, c, true);

            // split Source Providers
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.PROVIDER_SOURCES,
                    Constants.TABLE_IM_SOURCE_PROVIDER, Constants.SOURCE_RECORD_ID, Constants.PROVIDER_RECORD_ID, c, true);

            //
            // // split Provider To Tags
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.PROVIDER_TAGS,
                    Constants.TABLE_IM_PROVIDER_TO_TAG, Constants.PROVIDER_RECORD_ID, Constants.TAG_RECORD_ID, c, true);
            //
            //// //// split Product Features //Phase 3
            // splitColumnToRows();
            //

            // Split relevant Sectors
            splitColumnToRows(TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, RELEVANT_SECTORS, TABLE_IM_RELEVANT_SECTORS, Constants.PROVIDER_RECORD_ID, SECTOR, null,false);

            //// split Feature Categories
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_FEATURE, Constants.RECORD_ID, Constants.ASSOCIATED_DATA_CATEGORIES,
                    Constants.TABLE_IM_FEATURE_CATEGORY, Constants.FEATURE_RECORD_ID, Constants.CATEGORY, null, false);
            //
            //// split Provider Feature
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.DATA_FEATURE,
                    Constants.TABLE_IM_PROVIDER_FEATURE, Constants.PROVIDER_RECORD_ID, Constants.FEATURE, c, true);
            //
            //// split Provider Article
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_ARTICLE, Constants.RECORD_ID, Constants.LINKED_DATA_PROVIDER,
                    Constants.TABLE_IM_PROVIDER_ARTICLE, Constants.ARTICLE_RECORD_ID, Constants.PROVIDER, c, true);
            //
            //// split Provider Category
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.DATA_CATEGORY,
                    Constants.TABLE_IM_PROVIDER_CATEGORY, Constants.PROVIDER_RECORD_ID, Constants.CATEGORY, c, true);

            // // split Geographical Focus
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.GEOGRAPHICAL_FOCUS,
                    Constants.TABLE_IM_GEOGRAPHICAL_FOCUS, Constants.PROVIDER_RECORD_ID, Constants.IM_GEOGRAPHICAL_FOCUS, null, false);
            //
            // // split RelevantAsset Classes
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.RELEVANT_ASSET_CLASSES,
                    Constants.TABLE_IM_RELEVANT_ASSET_CLASS, Constants.PROVIDER_RECORD_ID, Constants.ASSET_CLASS, c, true);
            //
            // //split Relevant Investor Type
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.RELEVANT_INVESTORY_TYPES,
                    Constants.TABLE_IM_RELEVANT_INVESTOR_TYPE, Constants.PROVIDER_RECORD_ID, Constants.InvestorType, c, true);
            //
            //// split Relevant Security Type
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.RELEVANT_SECURITY_TYPES,
                    Constants.TABLE_IM_RELEVANT_SECURITY_TYPE, Constants.PROVIDER_RECORD_ID, Constants.SECURITY_TYPE, c, true);

            // Split Provider Partner
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.PROVIDER_DATA_PARTNER,
                    Constants.TABLE_IM_PROVIDER_PARTNER, Constants.PROVIDER_RECORD_ID, Constants.PARTNER, null, false);

            // Split Data Provider
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.DISTRIBUTION_DATA_PARTNER,
                    Constants.TABLE_IM_DISTRIBUTION_PARTNER, Constants.PROVIDER_RECORD_ID, Constants.PARTNER, null, false);

            //Split Consumer Partner
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.CONSUMER_DATA_PARTNER,
                    Constants.TABLE_IM_CONSUMER_PARTNER, Constants.PROVIDER_RECORD_ID, Constants.PARTNER, null, false);

            //Split Delivery Format
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.DELIVERY_FORMATS,
                    Constants.TABLE_IM_PROVIDER_DELIVERY_FORMAT, Constants.PROVIDER_RECORD_ID, Constants.DELIVERY_FORMAT, c, true);

            //Split Delivery Method
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.DELIVERY_METHODS,
                    Constants.TABLE_IM_PROVIDER_DELIVERY_METHOD, Constants.PROVIDER_RECORD_ID, Constants.DELIVERY_METHOD, c, true);

            //Split provider pricing model
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.PRICING_MODELS_OFFERED,
                    Constants.TABLE_IM_PROVIDER_PRICING_MODEL, Constants.PROVIDER_RECORD_ID, Constants.PRICING_MODELS_OFFERED, c, true);

            //Split provider delivery frequency
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.DELIVERY_FREQUENCIES,
                    Constants.TABLE_IM_PROVIDER_DELIVERY_FREQUENCY, Constants.PROVIDER_RECORD_ID, Constants.DELIVERY_FREQUENCY, c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_LINK, Constants.RECORD_ID, Constants.LINK_CATEGORIES,
                    Constants.TABLE_IM_LINK_CATEGORY,Constants.LINK_RECORD_ID, Constants.LINK_CATEGORY, c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EVENT, Constants.RECORD_ID, Constants.EVENT_DATA_PROVIDER,
                    Constants.TABLE_IM_EVENT_TOPIC,Constants.IM_EVENT_RECORD_ID, Constants.TOPIC,  c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EVENT, Constants.RECORD_ID, Constants.EVENT_DATA_INDUSTRIES,
                    Constants.TABLE_IM_EVENT_INDUSTRIES,Constants.IM_EVENT_INDUSTRIES_RECORD_ID, Constants.INDUSTRIES,  c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EVENT, Constants.RECORD_ID, Constants.EVENT_DATA_CATEGORY,
                    Constants.TABLE_IM_EVENT_CATEGORIES,Constants.IM_EVENT_CATEGORIES_RECORD_ID, Constants.CATEGORIES,  c, true);

            if(success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EVENT, Constants.RECORD_ID, Constants.EVENT_TYPES,
                    Constants.TABLE_IM_EVENT_TYPE,Constants.EVENT_RECORD_ID, Constants.EVENT_TYPE, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EVENT, Constants.RECORD_ID, Constants.EVENT_DATA_PROVIDER_ORGANIZER,
                    Constants.TABLE_IM_EVENT_DATA_PROVIDER_ORGANIZATION,Constants.EVENT_DATA_PROVIDER_RECORD_ID, Constants.EVENT_PROVIDER_ORGANIZATION, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EVENT, Constants.RECORD_ID, Constants.EVENT_SPONSER_DATA_PROVIDER,
                    Constants.TABLE_IM_EVENT_SPONSER_DATA_PROVIDER,Constants.EVENT_SPONSER_DATA_PROVIDER_RECORD_ID, Constants.EVENT_SPONSER_DATA_PROVIDERS, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EVENT, Constants.RECORD_ID, Constants.EVENT_DATA_ORGANIZATION,
                    Constants.TABLE_IM_EVENT_ORGANIZATION,Constants.IM_EVENT_ORGANIZATION_RECORD_ID, Constants.ORGANIZATION,  null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EVENT, Constants.RECORD_ID, Constants.EVENT_SPONSER_DATA_ORGANIZATION,
                    Constants.TABLE_IM_EVENT_SPONSER_DATA_ORGANIZATIONS,Constants.EVENT_SPONSER_ORGANIZATION_RECORD_ID, Constants.ORGANIZATION, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EVENT, Constants.RECORD_ID, Constants.PRICING_MODELS,
                    Constants.TABLE_IM_EVENT_PRICING_MODEL, Constants.IM_EVENT_RECORD_ID, Constants.PRICING_MODEL, c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_COMMENT, Constants.RECORD_ID, Constants.COMMENT_PARTICIPANT_ORGANIZATION,
                    Constants.TABLE_IM_COMMENT_PARTICIPANT_ORGANIZATION,Constants.IM_COMMENT_RECORD_ID, Constants.ORGANIZATION_RECORDID,  null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_COMMENT, Constants.RECORD_ID, Constants.COMMENT_RELEVENT_ORGANIZATION,
                    Constants.TABLE_IM_COMMENT_RELEVENT_ORGANIZATION,Constants.IM_COMMENT_RECORD_ID, Constants.ORGANIZATION_RECORDID,  null, false);
            
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_COMMENT, Constants.RECORD_ID, Constants.COMMENT_PARTICIPANT_PROVIDER,
                    Constants.TABLE_IM_COMMENT_PARTICIPANT_PROVIDER,Constants.IM_COMMENT_RECORD_ID, Constants.PROVIDER_RECORDID,  null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_COMMENT, Constants.RECORD_ID, Constants.COMMENT_RELEVENT_PROVIDER,
                    Constants.TABLE_IM_COMMENT_RELEVENT_PROVIDER,Constants.IM_COMMENT_RECORD_ID, Constants.PROVIDER_RECORDID,  null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_COMMENT, Constants.RECORD_ID, Constants.COMMENT_PARTICIPANT_EVENT,
                    Constants.TABLE_IM_COMMENT_PARTICIPANT_EVENT,Constants.IM_COMMENT_RECORD_ID, Constants.EVENT_RECORDID,  null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_COMMENT, Constants.RECORD_ID, Constants.COMMENT_RELEVENT_EVENT,
                    Constants.TABLE_IM_COMMENT_RELEVENT_EVENT,Constants.IM_COMMENT_RECORD_ID, Constants.EVENT_RECORDID,  null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_COMMENT, Constants.RECORD_ID, Constants.COMMENT_PURPOSE,
                    Constants.TABLE_IM_COMMENT_PURPOSE,Constants.IM_COMMENT_RECORD_ID, Constants.IM_PURPOSE_RECORDID,  null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_COMMENT, Constants.RECORD_ID, Constants.COMMENT_PURPOSE_TYPE,
                    Constants.TABLE_IM_COMMENT_PURPOSE_TYPE,Constants.IM_COMMENT_RECORD_ID, Constants.IM_PURPOSE_TYPE_RECORD,  null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_IDENTIFIERS,
                    Constants.TABLE_IM_PROVIDER_IDENTIFIER, Constants.PROVIDER_RECORD_ID, Constants.IDENTIFIERS, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_DATA_TYPE,
                    Constants.TABLE_IM_PROVIDER_DATA_TYPE, Constants.PROVIDER_RECORD_ID, Constants.DATATYPES, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_PRODUCT_TYPE,
                    Constants.TABLE_IM_DATA_PRODUCT_TYPE, Constants.PROVIDER_RECORD_ID, Constants.PRODUCT_TYPE, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_ACCESS_OFFERED,
                    Constants.TABLE_IM_DATA_PROVIDER_ACCESS, Constants.PROVIDER_RECORD_ID, Constants.ACCESS_OFFERED, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_TIME_FRAME,
                    Constants.TABLE_IM_PROVIDER_TIME_FRAME, Constants.PROVIDER_RECORD_ID, Constants.TIME_FRAME, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_MARKET_CAP,
                    Constants.TABLE_IM_PROVIDER_MARKET_CAP, Constants.PROVIDER_RECORD_ID, Constants.MARKET_CAP, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_MANAGER_STRATERGY,
                    Constants.TABLE_IM_PROVIDER_STRATEGRY, Constants.PROVIDER_RECORD_ID, Constants.STRATERGY, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_MANAGER_RESEARCH,
                    Constants.TABLE_IM_PROVIDER_RESEARCH, Constants.PROVIDER_RECORD_ID, Constants.RESEARCH, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_MANAGER_TYPE,
                    Constants.TABLE_IM_PROVIDER_MANAGER_TYPE, Constants.PROVIDER_RECORD_ID, Constants.MANAGER_TYPE, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_ORGANIZATION_TYPE,
                    Constants.TABLE_IM_PROVIDER_TARGET_ORGANIZATION, Constants.PROVIDER_RECORD_ID, Constants.ORGANIZATION_TYPE, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_COMPETITORS,
                    Constants.TABLE_IM_PROVIDER_COMPETITORS, Constants.PROVIDER_RECORD_ID, Constants.COMPETITORS, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_COMPLEMENTARY,
                    Constants.TABLE_IM_PROVIDER_COMPLEMENTARY, Constants.PROVIDER_RECORD_ID, Constants.COMPLEMENTARY, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_EQUITIES,
                    Constants.TABLE_IM_PROVIDER_EQUITIES, Constants.PROVIDER_RECORD_ID, Constants.EQUITIES, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_LANGUAGE,
                    Constants.TABLE_IM_PROVIDER_LANGUAGE, Constants.PROVIDER_RECORD_ID, Constants.LANGUAGE, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_OUTLIER_REASONS,
                    Constants.TABLE_IM_OUTLIER_REASONS, Constants.PROVIDER_RECORD_ID, Constants.DATA_OUTLIER_REASONS, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_BIASES,
                    Constants.TABLE_IM_BIASES, Constants.PROVIDER_RECORD_ID, Constants.DATA_BIASES, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_ORGANIZATION, Constants.RECORD_ID, Constants.IM_ORGANIZATION_SUBSIDIARY,
                    Constants.TABLE_IM_SUBSIDIARY, Constants.IM_EVENT_ORGANIZATION_RECORD_ID, Constants.SUBSIDIARY, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_PAYMENT,
                    Constants.TABLE_IM_PROVIDER_PAYMENT, Constants.PROVIDER_RECORD_ID, Constants.PAYMENT, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.IM_PROVIDER_DATA_GAPS_REASONS,
                    Constants.TABLE_IM_DATA_GAPS_REASONS, Constants.PROVIDER_RECORD_ID, Constants.DATA_GAPS_REASONS, null, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_PROVIDER, Constants.RECORD_ID, Constants.RESEARCH_METHODS,
                    Constants.TABLE_IM_RESEARCH_METHODS, Constants.PROVIDER_RECORD_ID, Constants.RESEARCH_METHOD, c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_ORGANIZATION, Constants.RECORD_ID, Constants.RESEARCH_METHODS,
                    Constants.TABLE_IM_ORGANIZATION_RESEARCH_METHODS, Constants.IM_ORGANIZATION_RECORD_ID, Constants.RESEARCH_METHOD, c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_RESOURCES, Constants.RECORD_ID, Constants.IM_WORKFLOW,
                    Constants.TABLE_IM_RESOURCE_WORKFLOW, Constants.RESOURCE_RECORD_ID, Constants.RESOURCE_WORKFLOW, c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_RESOURCES, Constants.RECORD_ID, Constants.IM_RESOURCES_PURPOSES,
                    Constants.TABLE_IM_RESOURCE_PURPOSES, Constants.RESOURCE_RECORD_ID, Constants.RESOURCE_PURPOSE, c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_RESOURCES, Constants.RECORD_ID, Constants.IM_RESOURCES_PURPOSE_TYPE,
                    Constants.TABLE_IM_RESOURCE_PURPOSES_TYPE, Constants.RESOURCE_RECORD_ID, Constants.RESOURCE_PURPOSE_TYPE, c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_RESOURCES, Constants.RECORD_ID, Constants.IM_RESOURCES_TOPICS,
                    Constants.TABLE_IM_RESOURCE_TOPIC, Constants.RESOURCE_RECORD_ID, Constants.RESOURCE_TOPIC, c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_RESOURCES, Constants.RECORD_ID, Constants.IM_RESOURCES_RELEVENT_ORGANIZATIONS,
                    Constants.TABLE_IM_RESOURCE_RELEVANT_ORGANIZATION, Constants.RESOURCE_RECORD_ID, Constants.RESOURCE_RELEVANT_ORGANIZATION, c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_RESOURCES, Constants.RECORD_ID, Constants.IM_RESOURCES_RELEVENT_DATA_PROVIDERS,
                    Constants.TABLE_IM_RESOURCE_RELEVANT_DATA_PROVIDERS, Constants.RESOURCE_RECORD_ID, Constants.RESOURCE_RELEVANT_DATA_PROVIDER, c, true);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_RESOURCES, Constants.RECORD_ID, Constants.RESEARCH_METHODS,
                    Constants.TABLE_IM_RESOURCE_RESEARCH_METHODS, Constants.RESOURCE_RECORD_ID, Constants.RESEARCH_METHOD, c, true);
           if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_ORGANIZATION_RECORDIDS,
                    Constants.TABLE_IM_INVESTOR_ORGANIZATION,Constants.INVESTMENT_RECORD_IDS, Constants.INVESTOR_ORGANIZATION_RECORDID, c, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_TYPES,
                    Constants.TABLE_IM_INVESTOR_TYPES,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_TYPE, c, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_GEOGRAPHICAL_FOCUS_TEXT,
                    Constants.TABLE_IM_INVESTOR_GEOGRAPHICAL_FOCUS,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_GEOGRAPHICAL_FOCUS, c, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_STRATEGIES,
                    Constants.TABLE_IM_INVESTOR_STRATEGIES,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_STRATEGIES, c, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_ASSET_CLASSES,
                    Constants.TABLE_IM_INVESTOR_ASSET_CLASSES,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_ASSET_CLASSES, c, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_SECTORS,
                    Constants.TABLE_IM_INVESTOR_SECTORS,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_SECTORS, c, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_SUBTYPES,
                    Constants.TABLE_IM_INVESTOR_SUBTYPES,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_SUBTYPE, c, false);

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_STAGES,
                    Constants.TABLE_IM_INVESTOR_INVESTMENT_STAGES,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_STAGE, c, false);
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_ROUNDS,
                    Constants.TABLE_IM_INVESTOR_INVESTMENT_ROUNDS,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_ROUND, c, false);
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_TYPICAL_SIZE,
                    Constants.TABLE_IM_INVESTOR_TYPICAL_SIZE_CHECK,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_TYPICAL_SIZE, c, false);
         
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_EQUITIES_MARKET_CAP,
                    Constants.TABLE_IM_INVESTOR_EQUITIES_MARKET_CAP,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_EQUITIES_MARKET_CAP, c, false);
            

            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_EQUITIES_STYLE,
                    Constants.TABLE_IM_INVESTOR_EQUITIES_STYLE,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_EQUITIES_STYLE, c, false);
            
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_INVESTING_TIME_FRAME,
                    Constants.TABLE_IM_INVESTOR_INVESTING_TIMEFRAME,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_INVESTING_TIMEFRAME, c, false);
            
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_RESEARCH_STYLES,
                    Constants.TABLE_IM_INVESTOR_RESEARCH_STYLE,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_RESEARCH_STYLE, c, false);
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_SECURITY_TYPES,
                    Constants.TABLE_IM_INVESTOR_SECURITY_TYPES,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_SECURITY_TYPE, c, false);
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_INVESTMENT_MANAGERS, Constants.RECORD_ID, Constants.INVESTOR_INDUSTRY_SECTOR,
                    Constants.TABLE_IM_INVESTOR_INDUSTRY_SECTOR,Constants.INVESTMENT_RECORD_ID, Constants.INVESTOR_INVESTMENT_INDUSTRY_SECTOR, c, false);
          
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EMAILS, Constants.RECORD_ID, Constants.DATA_TO_EMAIL_ADDRESSES,
                    Constants.TABLE_IM_TO_EMAIL_ADDRESSES,Constants.EMAILS_RECORD_ID, Constants.TO_EMAIL_ADDRESSES_RECORDID, c, false);
            
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EMAILS, Constants.RECORD_ID, Constants.DATA_CC_EMAIL_ADDRESSES,
                    Constants.TABLE_IM_CC_EMAIL_ADDRESSES,Constants.EMAILS_RECORD_ID, Constants.CC_EMAIL_ADDRESSES_RECORDID, c, false);
            
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EMAILS, Constants.RECORD_ID, Constants.DATA_LABELS,
                    Constants.TABLE_IM_LABELS,Constants.EMAILS_RECORD_ID, Constants.LABELS, c, false);
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EMAILS, Constants.RECORD_ID, Constants.DATA_PURPOSES,
                    Constants.TABLE_IM_EMAILS_PURPOSES,Constants.EMAILS_RECORD_ID, Constants.PURPOESES_RECORDID, c, false);
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EMAILS, Constants.RECORD_ID, Constants.DATA_PURPOSES_TYPES,
                    Constants.TABLE_IM_EMAILS_PURPOSE_TYPES,Constants.EMAILS_RECORD_ID, Constants.PURPOESE_TYPES_RECORDID, c, false);
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_EMAILS, Constants.RECORD_ID, Constants.DATA_TOPICS,
                    Constants.TABLE_IM_EMAILS_TOPICS,Constants.EMAILS_RECORD_ID, Constants.TOPICS_RECORDID, c, false);
            
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_DOMAINS, Constants.RECORD_ID, Constants.DOMAIN_INVESTMENT_MANAGER,
                    Constants.TABLE_IM_DOMAIN_INVESTMENT_MANAGER,Constants.DOMAINS_RECORD_ID, Constants.DOMAIN_INVESTMENT_RECORDID, c, false);
            
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_DOMAINS, Constants.RECORD_ID, Constants.DOMAIN_DATA_PROVIDER,
                    Constants.TABLE_IM_DOMAIN_DATA_PROVIDER,Constants.DOMAINS_RECORD_ID, Constants.DOMAIN_DATA_PROVIDER_RECORDID, c, false);
            
            if (success)
                splitColumnToRows(Constants.TABLE_STG_DATA_DOMAINS, Constants.RECORD_ID, Constants.DOMAIN_DATA_EMAIL_ADDRESS,
                    Constants.TABLE_IM_DOMAIN_EMAIL_ADDRESS,Constants.DOMAINS_RECORD_ID, Constants.DOMAIN_EMAIL_ADDRESS_RECORDID, c, false);
            
            
            if (success)
                updateProcessingStatus("STAGING_COMPLETE", folder);
            else {
                updateProcessingStatus("STAGING_ERROR", folder);
            }
            c.setCatalog(currentCatalog);
        }
        catch(Exception e) {
            updateProcessingStatus("SPLITTING_ERROR", folder);
        }
        finally
        {
            if(c != null)
                ctx.configuration().connectionProvider().release(c);
        }
        log.info("FINISHED splitting fields.");
        return success;
    }

    @Timed
    protected boolean splitColumnToRows(Table<Record> sourceTable, Field<String> sourceIDColumn,
                                        Field<String> sourceSplitField, Table<Record> targetTable, Field<String> targetIDColumn,
                                        Field<String> targetSplitField, Connection c, boolean spCall) {
        log.info("Starting splitting table: " + sourceTable + " Field: " + sourceSplitField.getName());
        long startTime = System.currentTimeMillis();
        Result<Record2<String, String>> result = ctx.select(sourceIDColumn, sourceSplitField).from(sourceTable)
            .where("trim(" + sourceSplitField + ") <> ''").fetch();
        ctx.truncate(targetTable).execute();
        if(spCall)
        {
            if(c != null)
            {
                try {
                    CallableStatement stmt = c.prepareCall("{call split_value_into_multiple_rows(?, ?, ?, ?, ?)}");
                    stmt.setString(1, sourceTable.getName());

                    stmt.setString(2, sourceIDColumn.getName());
                    stmt.setString(3, sourceSplitField.getName());
                    stmt.setString(4, ",");
                    stmt.setString(5, targetTable.getName());
                    //split_value_into_multiple_rows('stg_article', '`Record ID`', '`Linked Data Providers - Record IDs`', ',', 'im_provider_article_new'
                    stmt.execute();
                }
                catch (SQLException e) {
                    log.error(e.getMessage(), e);
                    return false;
                }
            }
        }
        else {
            for (Record r : result) {
                Object valueToSplit = r.getValue(sourceSplitField);
                if (StrUtils.isNotBlank(valueToSplit)) {
                    String[] splittedValue = StrUtils.splitByComma(valueToSplit.toString());
                    InsertValuesStep2 q = ctx.insertInto(targetTable, targetIDColumn, targetSplitField);
                    for (int i = 0; i < splittedValue.length; i++) {
                        q.values(r.getValue(sourceIDColumn).toString(), splittedValue[i]);
                    }
                    q.execute();
                }
            }
        }
        long execTime = (System.currentTimeMillis() - startTime) / 1000;
        log.info("Split " + sourceTable + "[" + sourceSplitField + "] Execution Time (sec): " + execTime);
        return true;
    }
    //
    // private static void splitProductFeatures() {
    // LazyList<Stg_DataProduct> l = Stg_DataProduct.findAll();
    // Im_ProductFeature.deleteAll();
    // for (Stg_DataProduct dp : l) {
    // String pf = dp.getString(Stg_DataProduct.DATA_FEATURES);
    // if (StrUtils.isNotBlank(pf)) {
    // String[] features = StrUtils.splitByComma(pf);
    // for (String feature : features) {
    // Im_ProductFeature ipf = new Im_ProductFeature();
    // ipf.set(Im_ProductFeature.FEATURE, feature,
    // Im_ProductFeature.PRODUCT_RECORD_ID,
    // dp.getString(Stg_DataProduct.PRODUCT_NAME)).saveIt();
    // }
    // }
    // }
    // }
    //
}
