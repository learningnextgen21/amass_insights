package com.ai.mp.batch;

import com.ai.mp.config.ApplicationProperties;
import com.ai.core.StrUtils;
import com.ai.mp.domain.CategoryLogo;
import com.ai.mp.domain.DataCategory;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.ProviderLogo;
import com.ai.mp.repository.CategoryLogoRepository;
import com.ai.mp.repository.DataCategoryRepository;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.repository.ProviderLogoRepository;
import com.ai.mp.repository.search.DataCategorySearchRepository;
import com.ai.mp.repository.search.DataProviderSearchRepository;
import com.codahale.metrics.annotation.Timed;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

@Service
public class DownloadLogo {
    private static final Logger log = LoggerFactory.getLogger(DownloadLogo.class);
    private final ApplicationProperties app;
	@Autowired
    private DataProviderRepository dataProviderRepository;
	@Autowired
    private ProviderLogoRepository providerLogoRepository;
    @Autowired
	private DataProviderSearchRepository providerSearchRepository;

    @Autowired
    private DataCategoryRepository dataCategoryRepository;
    
    @Autowired
    private CategoryLogoRepository categoryLogoRepository;
    @Autowired
    private DataCategorySearchRepository categorySearchRepository;
    
    public DownloadLogo(ApplicationProperties app) {
        this.app = app;
    }

    @Timed
    @Transactional
    @Scheduled(initialDelay = 10000, fixedDelay = 1000*60*60*24)
    protected void processFileLogos() {
        if (!process()) {
        	return;

        	
        }
                        processProviderFileLogo();
            DatacategoryFileLogo();
      
    }

    private boolean process()
    {
        if(System.getProperty("schedule") == null)
        {
            log.info("Skipping Scheduler for Logo Download...");
            return false;
        }
        Path dir = Paths.get(app.getProviderLogoFolder());
        if(!dir.toFile().exists())
        {
            log.info("Logos folder not found: {}", app.getproviderLogoFolder());
            return false;
        }
        return true;
    }

    @Timed
    @Transactional
	@Scheduled(initialDelay = 100000, fixedDelay = 1000*60*60*24*7)
    protected void downloadLogo()
    {
        if(!process())
        {
            return;
        }
        CloseableHttpClient httpclient = HttpClients.createDefault();
        log.info("Download Logo Scheduler Called........................................");
        Set<DataProvider> provider = dataProviderRepository.findByLogoIsNullAndWebsiteIsNotNull();
        for(final DataProvider p : provider)
        {
            final String website = p.getWebsite();
            final long id = p.getId();
            if (StrUtils.isBlank(website) || "WEBSITE_NULL".equals(website)) {
                continue;
            }

            log.info(p.getId() + " " + p.getWebsite());

            String tempSite = website;
            if(tempSite.indexOf("//") != -1)
            {
                tempSite = website.split("/")[2];
                log.info(id + " website: " + website + " Extracted: " + tempSite);
            }
            else if(tempSite.indexOf("/") != -1)
            {
                tempSite = website.split("/")[0];
                log.info(id + " website: " + website + " Extracted: " + tempSite);
            }
            else
            {
                log.info(id + " record: " + website);
            }
            final String value = tempSite;

            // Create a custom response handler
            try {
                HttpGet httpget = new HttpGet("https://logo.clearbit.com/" + value);
                log.info("Executing request: " + httpget.getRequestLine());
                ResponseHandler<Boolean> responseHandler = (final HttpResponse response) ->
                        {
                        int status = response.getStatusLine().getStatusCode();
                        if (status >= 200 && status < 300) {
                            HttpEntity entity = response.getEntity();
                            byte[] image = EntityUtils.toByteArray(entity);
                            if(image != null) {
                                ProviderLogo pl = new ProviderLogo(image, p.getRecordID());
                                providerLogoRepository.save(pl);
                                DataProvider fullProvider = dataProviderRepository.findOneWithEagerRelationships(p.getId());
                                providerSearchRepository.save(fullProvider);
                                log.info("Updated id: " + id + " website: " + website);
                                return true;
                            }
                        }
                        else if (status != 404)
                        {
                            throw new ClientProtocolException("Unexpected response status: " + status + " for: " + website);
                        }
                        return false;
                    };
                httpclient.execute(httpget, responseHandler);
            } catch (Exception e) {
                log.info(e.getMessage());
            }
        }
        log.info("Providers without logos: " + provider.size() + " Scheduler finished........................................");
    }
    
    private void processProviderFileLogo() {
    	
    	  Path dir = Paths.get(app.getProviderLogoFolder());
          if (dir.toFile().isDirectory()) {
              File[] logos = dir.toFile().listFiles((fileDir, name) -> name.endsWith(".png"));
              log.info("STARTED processing logo files.");
              for (File logo : logos) {
                  String recordID = logo.getName().split("-")[0].trim();
                  DataProvider p = dataProviderRepository.findByRecordIDEqualsAndLogoIsNull(recordID);
                  if (p != null) {
                      try {
                          byte[] image = Files.readAllBytes(Paths.get(logo.toURI()));
                          if (image != null) {
                              ProviderLogo pl = new ProviderLogo(image, p.getRecordID());
                              providerLogoRepository.save(pl);
                              DataProvider fullProvider = dataProviderRepository.findOneWithEagerRelationships(p.getId());
                              providerSearchRepository.save(fullProvider);
                              log.info("Updated provider with logo id: {} website: {}", p.getId(), p.getWebsite());
                          }
                      }
                      catch (IOException e) {
                          e.printStackTrace();
                      }
                  }
              }
              log.info("FINISHED processing logo files.");
          }
    	
    	
    	
    	
    }
    
    public void DatacategoryFileLogo() {
    	
    	  Path dir = Paths.get(app.getCategoryLogoFolder());
          if (dir.toFile().isDirectory()) {
              File[] logos = dir.toFile().listFiles((fileDir, name) -> name.endsWith(".png"));
              log.info("STARTED processing logo files.");
              for (File logo : logos) {
            	  if(!logo.getName().startsWith("rec")) {
            		  continue;
            	  }
                  String recordID = logo.getName().split("-")[0].trim();
                  DataCategory dp=dataCategoryRepository.findByRecordIDEqualsAndLogoIsNull(recordID);
                
                  
                  if (dp != null) {
                      try {
                          byte[] image = Files.readAllBytes(Paths.get(logo.toURI()));
                          if (image != null) {
                        	 
                        	  
                        	  CategoryLogo cl = new CategoryLogo(image,dp.getRecordID());
                              categoryLogoRepository.save(cl);
                             
                              DataCategory dataCategory=dataCategoryRepository.findOne(dp.getId());
                           
                              categorySearchRepository.save(dataCategory);
                             
                              log.info("Updated provider with logo id: {} website: {}", dp.getId(), dp.getDatacategory());
                          }
                      }
                      catch (IOException e) {
                          e.printStackTrace();
                      }
                  }
              }
              log.info("FINISHED processing logo files.");
          }
    	
    	
    }
    
}
