package com.ai.mp.security.social;

import com.ai.mp.security.UserNotActivatedException;
import io.github.jhipster.config.JHipsterProperties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ai.mp.config.ApplicationProperties;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.web.context.request.NativeWebRequest;

public class CustomSignInAdapter implements SignInAdapter {

    @SuppressWarnings("unused")
    private final Logger log = LoggerFactory.getLogger(CustomSignInAdapter.class);

    private final UserDetailsService userDetailsService;

    private final JHipsterProperties jHipsterProperties;

    private final ApplicationProperties applicationProperties;

    public CustomSignInAdapter(UserDetailsService userDetailsService, JHipsterProperties jHipsterProperties, ApplicationProperties applicationProperties) {
        this.userDetailsService = userDetailsService;
        this.jHipsterProperties = jHipsterProperties;
        this.applicationProperties = applicationProperties;
    }
    @Override
    public String signIn(String userId, Connection<?> connection, NativeWebRequest request) {
        UserDetails user = null;
        try {
             user = userDetailsService.loadUserByUsername(userId);
        }
        catch
            (UserNotActivatedException ex){
            return "/#/social-register/linkedin?success=true&active=0";
        }
        Authentication newAuth = new UsernamePasswordAuthenticationToken(
            user,
            null,
            user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(newAuth);
        return applicationProperties.getRedirectUrl(); // jHipsterProperties.getSocial().getRedirectAfterSignIn();
    }
}
