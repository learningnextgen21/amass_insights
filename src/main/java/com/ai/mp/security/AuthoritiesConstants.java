package com.ai.mp.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String GOLD = "ROLE_GOLD";
    public static final String SILVER = "ROLE_SILVER";
    public static final String BRONZE = "ROLE_BRONZE";

    private AuthoritiesConstants() {
    }
}
