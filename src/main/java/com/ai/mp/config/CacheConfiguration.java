package com.ai.mp.config;

import com.ai.mp.domain.*;
import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.ai.mp.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.User.class.getName() + ".categories", jcacheConfiguration);
            //cm.createCache(com.ai.mp.domain.User.class.getName() + ".category", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.User.class.getName() + ".providersFollowed", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.User.class.getName() + ".providersUnlocked", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.User.class.getName() + ".assetClasses", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.User.class.getName() + ".sectors", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.User.class.getName() + ".geographies", jcacheConfiguration);


            cm.createCache(com.ai.mp.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.User.class.getName() + ".persistentTokens", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.User.class.getName() + ".byUser", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.User.class.getName() + ".byUsers", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.UserRequest.class.getName(), jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.Authority.class.getName(), jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.PersistentToken.class.getName(), jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.SocialUserConnection.class.getName(), jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataArticle.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataArticle.class.getName() + ".relevantProviders", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataArticle.class.getName() + ".articleTopics", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataArticle.class.getName() + ".purpose", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataArticle.class.getName() + ".purposeType", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DocumentsMetadata.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataTopic.class.getName(), jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataProviderTag.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProviderTag.class.getName() + ".providerTags", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProviderTag.class.getName() + ".tagProviders", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProviderTag.class.getName() + ".dataTags", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataProviderEdit.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProviderEdit.class.getName() + ".providerEdits", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataProviderType.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProviderType.class.getName() + ".dataProviders", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataComment.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataComment.class.getName() + ".dataComment", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataComment.class.getName() + ".releventProviders", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataEvent.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataEvent.class.getName() + ".dataevent", jcacheConfiguration);
            /* cm.createCache(com.ai.mp.domain.DataEvent.class.getName() + ".sponsorOrganizer", jcacheConfiguration); */
            cm.createCache(com.ai.mp.domain.DataEvent.class.getName() + ".organizationOrganizer", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataEvent.class.getName() + ".sponsorOrganization", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataEvent.class.getName() + ".providerOrganizer", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataEvent.class.getName() + ".sponsorProvider", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataEvent.class.getName() + ".eventTopics", jcacheConfiguration);
            cm.createCache(DataOrganization.class.getName() + ".organizationEvent", jcacheConfiguration);
            cm.createCache(DataOrganization.class.getName() + ".sponsorOrganizationEvent", jcacheConfiguration);
            cm.createCache(DataOrganization.class.getName() + ".subsidiaryOrganization", jcacheConfiguration);
            cm.createCache(DataProvider.class.getName() + ".providerEvent", jcacheConfiguration);
            cm.createCache(DataProvider.class.getName() + ".sponsorProviderEvent", jcacheConfiguration);




            cm.createCache(com.ai.mp.domain.DataProvider.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".geogrpahicalFoci", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".providerPartners", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".distributionPartners", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".consumerPartners", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".competitors", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".complementaryProviders", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".industries", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".categories", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".features", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".relevantArticles", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".providerTags", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".sources", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".assetClasses", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".investorTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".securityTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".deliveryMethods", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".deliveryFormats", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".researchMethods", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataOrganization.class.getName() + ".researchMethods", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".deliveryFrequencies", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".pricingModels", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".relevantSectors", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".authoredArticles", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".followingUsers" , jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".logo" , jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".geographicalFoci", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".documentsMetadata", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".link", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".releventProviderComment", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".providerIndustries", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataProvidersDeliveryFormats", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".researchMethodsCompleted", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataOrganization.class.getName() + ".researchMethodsCompleted", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataProvidersDeliveryMethods", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataProvidersPricingModels", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataProvidersAssetClasses", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataProviderCategories", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataProviderTags", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataProviderFeatures", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".tagsProvider", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataProvidersRelevantSectors", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataProvidersSecurityTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataProviderSources", jcacheConfiguration);
            /*****************************New fields start**************************************/
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".equitiesStyle", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataProductTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".accessOffered", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".investingTimeFrame", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".equitiesMarketCap", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".strategies", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".researchStyles", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".managerType", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".organizationType", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".languagesAvailable", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".paymentMethodsOffered", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".identifiersAvailable", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".outlierReasons", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".biases", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataProvider.class.getName() + ".dataGapsReasons", jcacheConfiguration);

            /***************************New fields end**************************************/

            cm.createCache(com.ai.mp.domain.DataOrganization.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataOrganization.class.getName() + ".dataArticles", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataLinks.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataLinks.class.getName() + ".dataLinksCategory", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.ExternalClient.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataOrganization.class.getName() + ".owners", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataOrganization.class.getName() + ".dataOrganizations", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataCategory.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataCategory.class.getName() + ".dataProviders", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataCategory.class.getName() + ".providerCategories", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataCategory.class.getName() + ".categoryFeatures", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataCategory.class.getName() + ".categoryProviders", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataCategory.class.getName() + ".categoryUsers", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataCategory.class.getName() + ".categoryAuthorities", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataCategory.class.getName() + ".logo" , jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataFeature.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataFeature.class.getName() + ".categories", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataFeature.class.getName() + ".featureProviders", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataFeature.class.getName() + ".providerFeatures", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataIndustry.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataIndustry.class.getName() + ".dataProviders", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataIndustry.class.getName() + ".associatedProviders", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataIndustry.class.getName() + ".dataProviderIndustries", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataSource.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataSource.class.getName() + ".providerSources", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataSource.class.getName() + ".dataSources", jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.AdminRole.class.getName(), jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.ClientRole.class.getName(), jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.ProviderLogo.class.getName(), jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.CategoryLogo.class.getName(), jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataResourcePurpose.class.getName(), jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.DataResourcePurposeType.class.getName(), jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.EmailAddress.class.getName(), jcacheConfiguration);

            cm.createCache(com.ai.mp.domain.LookupCode.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerGeographicalFoci", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerAssetClasses", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerInvestorTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerSecurityTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerDeliveryMethods", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".researchMethodsCode", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".researchMethodsOrganization", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerDeliveryFormats", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerDeliveryFrequencies", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerPricingModels", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerSectors", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providersRelevantSectors", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".dataSecurityTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerEquitiesStyle", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerDataTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerDataProductTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerAccessOffered", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerInvestingTimeFrame", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerEquitiesMarketCap", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerStrategies", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerResearchStyles", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerManagerType", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerOrganizationType", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerLanguagesAvailable", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerPaymentMethodsOffered", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerIdentifiersAvailable", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerDataGapsReasons", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerBiases", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.LookupCode.class.getName() + ".providerOutlierReasons", jcacheConfiguration);

//DataResources
            cm.createCache(com.ai.mp.domain.DataResources.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataResources.class.getName() + ".workflows", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataResources.class.getName() + ".purposes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataResources.class.getName() + ".purposeTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataResources.class.getName() + ".topics", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataResources.class.getName() + ".researchMethodsCompleted", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataResources.class.getName() + ".relavantOrganizations", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataResources.class.getName() + ".relavantDataProviders", jcacheConfiguration);

            //DataInvestorMangers
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".dataOrganizationInvestor", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".dataTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".headquartersLocation", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".geographicalFoci", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".investmentStrategies", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".assetClass", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".sector", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".subType", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".investmentStage", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".investmentRound", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".typicalCheckSize", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".researchStyle", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".securityType", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".equitiesMarketCap", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".equitiesStyle", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".investingTimeFrame", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.DataInvestmentManager.class.getName() + ".industrySectorFocus", jcacheConfiguration);

            //Emails
            cm.createCache(com.ai.mp.domain.Emails.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.Emails.class.getName() + ".toEmailAddresses", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.Emails.class.getName() + ".ccEmailAddresses", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.Emails.class.getName() + ".labels", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.Emails.class.getName() + ".purposes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.Emails.class.getName() + ".purposeTypes", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.Emails.class.getName() + ".topics", jcacheConfiguration);
            
            //Domains
            cm.createCache(com.ai.mp.domain.Domains.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.Domains.class.getName() + ".ownerInvestmentManager", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.Domains.class.getName() + ".ownerDataProvider", jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.Domains.class.getName() + ".EmailAddresses", jcacheConfiguration);
            
            
            //search
            cm.createCache(com.ai.mp.domain.Search.class.getName(), jcacheConfiguration);

            
 //cardDetails          
            cm.createCache(com.ai.mp.domain.CardDetails.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.Products.class.getName(), jcacheConfiguration);
            
            cm.createCache(com.ai.mp.domain.IpAddress.class.getName(), jcacheConfiguration);


            cm.createCache(Email.class.getName(), jcacheConfiguration);
            cm.createCache(UserProvider.class.getName(), jcacheConfiguration);
            cm.createCache(UserDataCategory.class.getName(), jcacheConfiguration);
            cm.createCache(UserNotification.class.getName(), jcacheConfiguration);
            cm.createCache(com.ai.mp.domain.Payment.class.getName(), jcacheConfiguration);

            cm.createCache(org.hibernate.cache.spi.UpdateTimestampsCache.class.getName(), jcacheConfiguration);
            cm.createCache(org.hibernate.cache.internal.StandardQueryCache.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
            
            cm.createCache(com.ai.mp.domain.UserCheckout.class.getName(), jcacheConfiguration);
            cm.createCache(UserAsset.class.getName(), jcacheConfiguration);
            cm.createCache(UserSectors.class.getName(), jcacheConfiguration);
            cm.createCache(UserGeographies.class.getName(), jcacheConfiguration);

        };
    }
}
