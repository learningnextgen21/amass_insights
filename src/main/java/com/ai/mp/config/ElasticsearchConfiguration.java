package com.ai.mp.config;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.elasticsearch.client.Client;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.EntityMapper;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class ElasticsearchConfiguration {

    @Bean
    public ElasticsearchTemplate elasticsearchTemplate(Client client, Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder) {
        return new ElasticsearchTemplate(client, new CustomEntityMapper(jackson2ObjectMapperBuilder.createXmlMapper(false).build()));
    }

    public class CustomEntityMapper implements EntityMapper {

        private ObjectMapper objectMapper;

        public CustomEntityMapper(ObjectMapper objectMapper) {
            this.objectMapper = objectMapper;
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        }

        @Override
        public String mapToString(Object object) throws IOException {
            return objectMapper.writeValueAsString(object);
        }

        @Override
        public <T> T mapToObject(String source, Class<T> clazz) throws IOException {
            return objectMapper.readValue(source, clazz);
        }
    }
    public static final String INDEX_DATA_CATEGORY = "datacategory";
    public static final String INDEX_DATA_PROVIDER = "dataprovider";
    public static final String INDEX_DATA_TAG = "dataprovidertag";
    public static final String INDEX_DATA_EVENT = "dataevent";
    public static final String INDEX_DATA_ORGANIZATION = "dataorganization";
    public static final String MASKED_TEXT = "locked";
    public static final String INDEX_DATA_ARTICLE = "dataarticle";
    public static final String INDEX_DATA_INVESTMENT_MANAGERS = "datainvestmentmanager";
    public static final String INDEX_DATA_EMAILS = "emails";
    public static final String INDEX_DATA_USERS = "user";

    public static final String INDEX_DATA_DOMAINS = "domains";
    public static final String INDEX_DATA_RESOURCE = "dataresource";
    public static final String INDEX_EMAIL_ADDRESS="emailaddress";
    public static Set<String> RESTRICTED_INDEXES = new HashSet<String>(Arrays.asList(new String[]{INDEX_DATA_PROVIDER,INDEX_DATA_CATEGORY,INDEX_DATA_EVENT,INDEX_DATA_ARTICLE,INDEX_DATA_RESOURCE,INDEX_DATA_TAG,INDEX_DATA_INVESTMENT_MANAGERS,INDEX_DATA_EMAILS}));
}
