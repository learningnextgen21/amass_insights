package com.ai.mp.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.stereotype.Component;



@Component
public class CustomAuthenticationFailureHandler extends ExceptionMappingAuthenticationFailureHandler {


	 @Override
	  public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {

		// System.out.println("auth");
	     if(exception.getClass().isAssignableFrom(BadCredentialsException.class)) {
	    //	 System.out.println("1");
	         response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authentication failed");
	      }
	      else if (exception.getClass().isAssignableFrom(DisabledException.class)) {
	     //     System.out.println("2");
		         response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authentication failed");
	      }
	      else if (exception.getClass().isAssignableFrom(SessionAuthenticationException.class)) {
	     //     System.out.println("3");
		         response.sendError(HttpServletResponse.SC_CONFLICT, "you are already login");
	      }

	      super.onAuthenticationFailure(request, response, exception);

	    }



}
