package com.ai.mp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to JHipster.
 * <p>
 * Properties are configured in the application.yml file.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
	public String getDatafolder() {
		return datafolder;
	}

	public void setDatafolder(String datafolder) {
		this.datafolder = datafolder;
	}

	private String datafolder = "";

    private String categoryLogoFolder="";

    private String documentsMetadataFolder = "";
    
    private String documentsMetadataUserFolder = "";

    private String downloadUserFile = "";

    public String getStripeKey() {
		return stripeKey;
	}

	public void setStripeKey(String stripeKey) {
		this.stripeKey = stripeKey;
	}

	private String downloadFile = "";

    private Boolean article;

    private String stripeKey = "";
    
    private String planKey = "";
     
    public String getPlanKey() {
		return planKey;
	}

	public void setPlanKey(String planKey) {
		this.planKey = planKey;
	}

	public Boolean getDev() {
		return dev;
	}

	public void setDev(Boolean dev) {
		this.dev = dev;
	}

	private Boolean dev;

    public String getCategoryLogoFolder() {
		return categoryLogoFolder;
	}

	public void setCategoryLogoFolder(String categoryLogoFolder) {
		this.categoryLogoFolder = categoryLogoFolder;
	}

    public String getDocumentsMetadataFolder() {
        return documentsMetadataFolder;
    }

    public void setDocumentsMetadataFolder(String documentsMetadataFolder) {
        this.documentsMetadataFolder = documentsMetadataFolder;
    }

	public String getProviderLogoFolder() {
		return providerLogoFolder;
	}

	public void setProviderLogoFolder(String providerLogoFolder) {
		this.providerLogoFolder = providerLogoFolder;
	}

	public String getproviderLogoFolder() {
		return providerLogoFolder;
	}

	public void setproviderLogoFolder(String logofolder) {
		this.providerLogoFolder = logofolder;
	}

	private String providerLogoFolder = "";

	public String getDocumentsFolder() {
		return documentsFolder;
	}

	public void setDocumentsFolder(String documentsFolder) {
		this.documentsFolder = documentsFolder;
	}

	private String documentsFolder = "";

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getReplyToEmail() {
		return replyToEmail;
	}

	public void setReplyToEmail(String replyToEmail) {
		this.replyToEmail = replyToEmail;
	}

	private String adminEmail;

	private String replyToEmail;

	public String getMailFromName() {
		return mailFromName;
	}

	public void setMailFromName(String mailFromName) {
		this.mailFromName = mailFromName;
	}

	private String mailFromName;

	private String redirectUrl;

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getContactUsEmail() {
		return contactUsEmail;
	}

	public void setContactUsEmail(String contactUsEmail) {
		this.contactUsEmail = contactUsEmail;
	}

	private String contactUsEmail;

    public String getDownloadFile() {
        return downloadFile;
    }

    public void setDownloadFile(String downloadFile) {
        this.downloadFile = downloadFile;
    }

    public String getProviderPartners() {
        return providerPartners;
    }

    public void setProviderPartners(String providerPartners) {
        this.providerPartners = providerPartners;
    }

    private  String distributionPartners;

    private String providerPartners;

    private String consumerPartners;
    
    private String RequestHelpFindingData;
    
    private String  RequestHelpEngineeringData;
    
    private Integer interestTypeMoreInfo;
    
    private Integer interestTypeUnlockPending;
     
    private Integer cancelDataSample;
    
    private Integer cancelDirectContactProvider;
    public Integer getCancelDataSample() {
		return cancelDataSample;
	}

	public void setCancelDataSample(Integer cancelDataSample) {
		this.cancelDataSample = cancelDataSample;
	}

	public Integer getCancelDirectContactProvider() {
		return cancelDirectContactProvider;
	}

	public void setCancelDirectContactProvider(Integer cancelDirectContactProvider) {
		this.cancelDirectContactProvider = cancelDirectContactProvider;
	}

	public Integer getEmailSubscription() {
		return emailSubscription;
	}

	public void setEmailSubscription(Integer emailSubscription) {
		this.emailSubscription = emailSubscription;
	}

	public Integer getEmailUnsubscription() {
		return emailUnsubscription;
	}

	public void setEmailUnsubscription(Integer emailUnsubscription) {
		this.emailUnsubscription = emailUnsubscription;
	}

	private Integer emailSubscription;
    
    private Integer emailUnsubscription;
    
	public Integer getInterestTypeUnlockPending() {
		return interestTypeUnlockPending;
	}

	public void setInterestTypeUnlockPending(Integer interestTypeUnlockPending) {
		this.interestTypeUnlockPending = interestTypeUnlockPending;
	}

	public Integer getInterestTypeMoreInfo() {
		return interestTypeMoreInfo;
	}

	public void setInterestTypeMoreInfo(Integer interestTypeMoreInfo) {
		this.interestTypeMoreInfo = interestTypeMoreInfo;
	}

	public String getRequestHelpFindingData() {
		return RequestHelpFindingData;
	}

	public void setRequestHelpFindingData(String requestHelpFindingData) {
		RequestHelpFindingData = requestHelpFindingData;
	}

	public String getRequestHelpEngineeringData() {
		return RequestHelpEngineeringData;
	}

	public void setRequestHelpEngineeringData(String requestHelpEngineeringData) {
		RequestHelpEngineeringData = requestHelpEngineeringData;
	}

	public String getRequestHelpSellingData() {
		return RequestHelpSellingData;
	}

	public void setRequestHelpSellingData(String requestHelpSellingData) {
		RequestHelpSellingData = requestHelpSellingData;
	}

	private String RequestHelpSellingData;
    
    public String getConsumerPartners() {
        return consumerPartners;
    }

    public void setConsumerPartners(String consumerPartners) {
        this.consumerPartners = consumerPartners;
    }

    public String getDistributionPartners() {
        return distributionPartners;
    }

    public void setDistributionPartners(String distributionPartners) {
        this.distributionPartners = distributionPartners;
    }

    public Boolean getArticle() {
        return article;
    }

    public void setArticle(Boolean article) {


        this.article = article;
    }

	public String getDocumentsMetadataUserFolder() {
		return documentsMetadataUserFolder;
	}

	public void setDocumentsMetadataUserFolder(String documentsMetadataUserFolder) {
		this.documentsMetadataUserFolder = documentsMetadataUserFolder;
	}

	public String getDownloadUserFile() {
		return downloadUserFile;
	}

	public void setDownloadUserFile(String downloadUserFile) {
		this.downloadUserFile = downloadUserFile;
	}


}
