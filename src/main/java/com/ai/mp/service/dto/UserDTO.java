package com.ai.mp.service.dto;

import com.ai.mp.config.Constants;
import com.ai.mp.domain.Authority;
import com.ai.mp.domain.DataCategory;
import com.ai.mp.domain.LookupCode;
import com.ai.mp.domain.User;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserDTO {

    private Long id;

    @NotBlank
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 100)
    private String login;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 100)
    private String email;

    @Size(max = 256)
    private String imageUrl;

    private boolean activated = false;
    private boolean professional_investor = false;
    private boolean interest_sell_data_product = false;
    private boolean notify_news_update = false;

    @Size(min = 2, max = 5)
    private String langKey;

    private String createdBy;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    private String company;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

    private Set<String> authorities;

    private LookupCode rolePermission;
    
    public LookupCode getRolePermission() {
		return rolePermission;
	}

	public void setRolePermission(LookupCode rolePermission) {
		this.rolePermission = rolePermission;
	}

	public Set<DataCategory> getCategories() {
        return categories;
    }

    public void setCategories(Set<DataCategory> categories) {
        this.categories = categories;
    }

    private Set<DataCategory> categories = new HashSet<DataCategory>();

    private Set<Integer> permissions;

    public UserDTO() {
        // Empty constructor needed for Jackson.
    }

    public UserDTO(User user) {
        this(user.getId(), user.getLogin(), user.getFirstName(), user.getLastName(), user.getCompany(),
            user.getEmail(), user.getActivated(), user.isInterest_sell_data_product(), user.isNotify_news_update(), user.isProfessional_investor(), user.getImageUrl(), user.getLangKey(),
            user.getCreatedBy(), user.getCreatedDate(), user.getLastModifiedBy(), user.getLastModifiedDate(),user.getPermission(),
            user.getAuthorities().stream().map(Authority::getName)
                .collect(Collectors.toSet()), user.getCategories(), user.getAuthorities().stream().map(Authority::getRoleLevel).collect(Collectors.toSet()));
    }

    public UserDTO(Long id, String login, String firstName, String lastName, String company,
                   String email, boolean activated, boolean interest_sell_data_product, boolean notify_news_update, boolean professional_investor, String imageUrl, String langKey,
                   String createdBy, Instant createdDate, String lastModifiedBy, Instant lastModifiedDate,LookupCode rolePermission,
                   Set<String> authorities, Set<DataCategory> categories, Set<Integer> permissions) {

        this.id = id;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.company= company;
        this.email = email;
        this.activated = activated;
        this.interest_sell_data_product = interest_sell_data_product;
        this.notify_news_update = notify_news_update;
        this.professional_investor = professional_investor;
        this.imageUrl = imageUrl;
        this.langKey = langKey;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.lastModifiedBy = lastModifiedBy;
        this.lastModifiedDate = lastModifiedDate;
        this.authorities = authorities;
        this.categories = categories;
        this.permissions = permissions;
        this.rolePermission=rolePermission;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public boolean isActivated() {
        return activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
            "id='" + id + '\'' +
            ",login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", company='" + company + '\''+
            ", email='" + email + '\'' +
            ", imageUrl='" + imageUrl + '\'' +
            ", activated=" + activated +
            ", professional_investor=" + professional_investor +
            ", interest_sell_data_product=" + interest_sell_data_product +
            ", notify_news_update=" + notify_news_update +
            ", langKey='" + langKey + '\'' +
            ", createdBy=" + createdBy +
            ", createdDate=" + createdDate +
            ", lastModifiedBy='" + lastModifiedBy + '\'' +
            ", lastModifiedDate=" + lastModifiedDate +
            ", authorities=" + authorities +
            ", permissions=" + permissions +
            "}";
    }

	public boolean isProfessional_investor() {
		return professional_investor;
	}

	public void setProfessional_investor(boolean professional_investor) {
		this.professional_investor = professional_investor;
	}

	public boolean isInterest_sell_data_product() {
		return interest_sell_data_product;
	}

	public void setInterest_sell_data_product(boolean interest_sell_data_product) {
		this.interest_sell_data_product = interest_sell_data_product;
	}

	public boolean isNotify_news_update() {
		return notify_news_update;
	}

	public void setNotify_news_update(boolean notify_news_update) {
		this.notify_news_update = notify_news_update;
    }

    /**
     * @return the permissions
     */
    public Set<Integer> getPermissions() {
        return permissions;
    }

    /**
     * @param permissions the permissions to set
     */
    public void setPermissions(Set<Integer> permissions) {
        this.permissions = permissions;
    }
}
