package com.ai.mp.service;

import java.util.Date;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.EmailAddress;
import com.ai.mp.repository.EmailAddressRepository;
import com.ai.mp.utils.RandomUtil;
import com.ai.util.Utils;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.resource.Contact;
import com.mailjet.client.resource.ContactManagemanycontacts;
import com.mailjet.client.resource.Contactdata;
import com.mailjet.client.resource.ContactslistManageManyContacts;

@Service
@Transactional
public class EmailAddressService {
    private final Logger log = LoggerFactory.getLogger(EmailAddressService.class);
    private final MailJetService mailJetService;
    private final EmailAddressRepository emailAddressRepository;
    private final Environment environment;
    private final MailService mailService;
    public EmailAddressService(MailJetService mailJetService, EmailAddressRepository emailAddressRepository, Environment environment,MailService mailService) {
        this.mailJetService = mailJetService;
        this.emailAddressRepository = emailAddressRepository;
        this.environment = environment;
        this.mailService=mailService;
    }
    public EmailAddress createEmailAddress(EmailAddress emailAddress) {
        emailAddress.setSubscribedNewsletter(true);
        Date date = new Date();
        emailAddress.setRecordID("rece"+ Utils.randomString(13,13));
        emailAddress.setEmailAddress(emailAddress.getEmailAddress());
        emailAddress.setConfirmationkey(RandomUtil.generateConfirmationKey());
        EmailAddress result = emailAddressRepository.save(emailAddress);
        emailAddress.setCreatedDate(date);
    //    System.out.println("RandomUtil.generateConfirmationKey()"+result);
        return result;

    }


    public void subscribeMailingList(String key)
    {
        EmailAddress ur = emailAddressRepository.findOneByconfirmationkeyEquals(key);
        if(ur == null)
        {
            return;
        }

        int mailListID = Integer.parseInt(environment.getProperty("spring.mailjet.subscription-list"));
        MailjetClient client = new MailjetClient(environment.getProperty("spring.mail.username"), environment.getProperty("spring.mail.password"));

        MailjetRequest newContactRequest = new MailjetRequest(ContactManagemanycontacts.resource)
            .property(ContactManagemanycontacts.CONTACTSLISTS,
                new JSONArray()
                    .put(new JSONObject()
                        .put("ListID", environment.getProperty("spring.mailjet.subscription-list")))
                    .put(new JSONObject().put("Action", "addforce")))
            .property(ContactManagemanycontacts.CONTACTS,
                new JSONArray()
                    .put(new JSONObject()
                        .put(Contact.EMAIL, ur.getEmailAddress())
                        .put(Contact.NAME, "Null")));
        new MailjetRequest(Contactdata.resource)
            .property(Contact.NAME, "Null")
            .property(Contact.EMAIL, ur.getEmailAddress());

        MailjetResponse response = null;
        try {
            response = client.post(newContactRequest);
        } catch (MailjetException e) {
            log.error(e.getMessage(), e);
        } catch (MailjetSocketTimeoutException e) {
            log.error(e.getMessage(), e);
        }
        log.info("Response status: " + response.getStatus());
        log.info("Response data: " + response.getData());

        try {
            MailjetRequest addUserToMailingListRequest = new MailjetRequest(ContactslistManageManyContacts.resource, mailListID)
                .property(ContactslistManageManyContacts.ACTION, "addforce")
                .property(ContactslistManageManyContacts.CONTACTS, new JSONArray()
                    .put(new JSONObject()
                        .put(Contact.EMAIL, ur.getEmailAddress())
                        .put(Contact.NAME, "Null")));
            response = client.post(addUserToMailingListRequest);
            //System.out.println(response.getStatus());
            //System.out.println(response.getData());
        } catch (MailjetException e) {
            log.error(e.getMessage(), e);
        } catch (MailjetSocketTimeoutException e) {
            log.error(e.getMessage(), e);
        }
        log.info("Response status: " + response.getStatus());
        log.info("Response data: " + response.getData());
        Date date = new Date();
        ur.setActive(true);
        ur.setDateConfirmedSubscription(date);
        ur.setConfirmationkey(null);
        emailAddressRepository.save(ur);
        mailService.newsLetterSubscriptionAdmin(ur);
    }

    public void unSubscriptionMailingList(String key)
    {
        EmailAddress ur = emailAddressRepository.findOneByconfirmationkeyEquals(key);

        if(ur == null)
        {
            return;
        }
        int mailListID = Integer.parseInt(environment.getProperty("spring.mailjet.subscription-list"));
        MailjetClient client = new MailjetClient(environment.getProperty("spring.mail.username"), environment.getProperty("spring.mail.password"));

        MailjetRequest newContactRequest = new MailjetRequest(ContactManagemanycontacts.resource)
            .property(ContactManagemanycontacts.CONTACTSLISTS,
                new JSONArray()
                    .put(new JSONObject()
                        .put("ListID", environment.getProperty("spring.mailjet.subscription-list")))
                    .put(new JSONObject().put("Action", "remove")))
            .property(ContactManagemanycontacts.CONTACTS,
                new JSONArray()
                    .put(new JSONObject()
                        .put(Contact.EMAIL, ur.getEmailAddress())
                        .put(Contact.NAME, "Null")));
        new MailjetRequest(Contactdata.resource)
            .property(Contact.NAME, "Null")
            .property(Contact.EMAIL, ur.getEmailAddress());

        MailjetResponse response = null;
        try {
            response = client.post(newContactRequest);
        } catch (MailjetException e) {
            log.error(e.getMessage(), e);
        } catch (MailjetSocketTimeoutException e) {
            log.error(e.getMessage(), e);
        }
        log.info("Response status: " + response.getStatus());
        log.info("Response data: " + response.getData());

        try {
            MailjetRequest addUserToMailingListRequest = new MailjetRequest(ContactslistManageManyContacts.resource, mailListID)
                .property(ContactslistManageManyContacts.ACTION, "remove")
                .property(ContactslistManageManyContacts.CONTACTS, new JSONArray()
                    .put(new JSONObject()
                        .put(Contact.EMAIL, ur.getEmailAddress())
                        .put(Contact.NAME, "Null")));
            response = client.post(addUserToMailingListRequest);
            /*System.out.println(response.getStatus());
            System.out.println(response.getData());*/
        } catch (MailjetException e) {
            log.error(e.getMessage(), e);
        } catch (MailjetSocketTimeoutException e) {
            log.error(e.getMessage(), e);
        }
        log.info("Response status: " + response.getStatus());
        log.info("Response data: " + response.getData());

        Date date = new Date();
        ur.setActive(false);
        ur.setDateConfirmedUnubscription(date);
        emailAddressRepository.save(ur);
    }
	public void updateEmailAddress(DataProvider newProvider) {
		// TODO Auto-generated method stub
		//System.out.println("called"+newProvider.getEmailAddress());
		Set<EmailAddress> email=emailAddressRepository.findByEmailAdrressProviderId(newProvider.getId());
		if(null!=newProvider.getEmailAddress()) {
			for(EmailAddress emailAddress:newProvider.getEmailAddress()) {
				EmailAddress emailValue=emailAddressRepository.findByEmailAddress(emailAddress.getEmailAddress());
				if(null!=emailValue)
				{
					emailValue.setDataProvider(newProvider.getId());
					emailAddressRepository.save(email);
					email.remove(emailValue);
				}
				
				
			}
			if(!email.isEmpty()) {
				for(EmailAddress emailAddress:email) {
					Long value=(long) 0;
					emailAddress.setDataProvider(value);
					emailAddressRepository.save(emailAddress);
				}
			}
		}
		
	}

}
