package com.ai.mp.service;

  import com.ai.mp.domain.DataComment;
import com.ai.mp.domain.DataEvent;
    import com.ai.mp.repository.DataEventRepository;
    import com.ai.mp.repository.search.DataEventSearchRepository;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.data.domain.Page;
    import org.springframework.data.domain.Pageable;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    import org.springframework.stereotype.Service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import javax.transaction.Transactional;

@Service
@org.springframework.transaction.annotation.Transactional
public class DataEventService {
    private final Logger log = LoggerFactory.getLogger(DataEventService.class);
    @Autowired
    private final DataEventSearchRepository dataEventSearchRepository;
   
    public DataEventService(DataEventSearchRepository dataEventSearchRepository){
        this.dataEventSearchRepository=dataEventSearchRepository;
    }



    @Transactional()
    public Page<DataEvent> searchAll(Pageable pageable) {
        log.debug("Request to get a page of DataEvents");
        Page<DataEvent> result = dataEventSearchRepository.findAll(pageable);
        return result;
    }

    @Transactional()
    public Page<DataEvent> findAll(Pageable pageable) {
        log.debug("Request to get all DataEvents");
        return dataEventSearchRepository.findAll(pageable);
    }
    
    @Transactional()
    public Page<DataEvent> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DataEvent for query {}", query);
        Page<DataEvent> result = dataEventSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
    
    
}
