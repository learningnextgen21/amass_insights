package com.ai.mp.service;

import com.ai.mp.config.ApplicationProperties;
import com.ai.mp.domain.*;
import com.ai.mp.repository.DataArticleRepository;
import com.ai.mp.repository.EmailRepository;
import com.ai.mp.repository.LookupCodeRepository;
import com.ai.mp.repository.UserCheckoutRepository;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.repository.UserRequestRepository;
import com.ai.mp.utils.RandomUtil;
import com.ai.mp.web.rest.ElasticsearchIndexResource;
import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.config.JHipsterProperties;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import org.apache.commons.lang3.CharEncoding;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import twitter4j.conf.ConfigurationBuilder;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.social.connect.Connection;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.mail.internet.MimeMessage;
import javax.naming.LinkException;
import javax.net.ssl.HttpsURLConnection;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import static com.ai.core.Constants.*;

/**
 * Service for sending emails.
 * <p>
 * We use the @Async annotation to send emails asynchronously.
 */
@Service
public class MailService {
	private final Logger log = LoggerFactory.getLogger(MailService.class);

    private final JHipsterProperties jHipsterProperties;
    private final ApplicationProperties applicationProperties;
    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;
    private final UserRepository userRepository;
    private final SpringTemplateEngine templateEngine;
    private final EmailRepository emailRepository;
    private final Environment environment;
    private final UserRequestRepository userRequestRepository;
    private final MailJetService mailJetService;
    private final DataArticleRepository dataArticleRepository;
    private final UserCheckoutRepository userCheckoutRepository;
    private final LookupCodeRepository lookupCodeRepository;
    public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
                       MessageSource messageSource, UserRepository userRepository, SpringTemplateEngine templateEngine, EmailRepository emailRepository,
                       ApplicationProperties applicationProperties, Environment environment, UserRequestRepository userRequestRepository, MailJetService mailJetService, DataArticleRepository dataArticleRepository,UserCheckoutRepository userCheckoutRepository,LookupCodeRepository lookupCodeRepository) {
        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.userRepository = userRepository;
        this.templateEngine = templateEngine;
        this.emailRepository = emailRepository;
        this.applicationProperties = applicationProperties;
        this.environment = environment;
        this.userRequestRepository = userRequestRepository;
        this.mailJetService = mailJetService;
        this.dataArticleRepository = dataArticleRepository;
        this.userCheckoutRepository=userCheckoutRepository;
        this.lookupCodeRepository=lookupCodeRepository;
    }



    @Timed
    @Transactional
    protected void sendEmails(List<Email> emails) {
        for (Email email : emails) {
            // Prepare message using a Spring helper
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            try {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, email.isMultipart(), CharEncoding.UTF_8);
                message.setTo(email.getEmailAdress());
                message.setReplyTo(applicationProperties.getReplyToEmail());
                message.setFrom(jHipsterProperties.getMail().getFrom(), applicationProperties.getMailFromName());
                message.setSubject(email.getSubject());
                message.setText(email.getBody(), email.isHtml());
                javaMailSender.send(mimeMessage);
                log.debug("Sent email to User '{}'", email.getEmailAdress());
                email.setSentDate(LocalDateTime.now());
                email.setNotificationStatus(NOTIFICATION_STATUS_SENT);
            } catch (Exception e) {
                if (log.isDebugEnabled()) {
                    log.warn("Email could not be sent to user '{}'", email.getEmailAdress(), e);
                } else {
                    log.warn("Email could not be sent to user '{}': {}", email.getEmailAdress(), e.getMessage());
                }
                email.setRetrycount(email.getRetrycount() + 1);
                email.setNotificationStatus(NOTIFICATION_STATUS_FAILED);
            }
            emailRepository.save(email);
        }
    }

    @Timed
    @Transactional
    @Scheduled(initialDelay = 10000, fixedDelay = 1000 * 60 * 60 * 12)
    protected void processFailedEmails() {
        if (!process())
            return;
        List<Email> emails = emailRepository.findAllByNotificationStatusEqualsAndRetrycountIsLessThanEqual(NOTIFICATION_STATUS_FAILED, 3);
        sendEmails(emails);
    }

    @Scheduled(initialDelay = 10000, fixedDelay = 1000 * 15)
    protected void processEmails() {
        if (!process())
            return;
        List<Email> emails = emailRepository.findAllByNotificationStatusEquals(NOTIFICATION_STATUS_NEW);
        sendEmails(emails);
    }

    private boolean process() {
        if (System.getProperty("processEmails") == null) {
            log.info("Skipping Scheduler for Email processing...");
            return false;
        }
        return true;
    }

    @Async
    @Transactional
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        sendEmail(to, subject, content, isMultipart, isHtml, null);
    }




    public void sendContactUsEmail(String subject, String content, boolean isMultipart, boolean isHtml) {
        sendEmail(applicationProperties.getContactUsEmail(), subject, content, isMultipart, isHtml, null);
    }

    @Transactional
    public void sendUnfollowProviderEmail(User user, DataProvider provider, String deletionKey)
    {
        Context context = new Context(Locale.US);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(DATA_PROVIDER, provider);
        context.setVariable("deletionKey", deletionKey);
        String content = templateEngine.process("unfollowProviderEmail", context);
        sendEmail(user.getEmail(), "You have unfollowed all news and updates about " + provider.getProviderName(), content, false, true, user);
    }

    @Transactional
    public void sendFollowProviderEmail(User user, DataProvider provider, String deletionKey)
    {
        Context context = new Context(Locale.US);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(DATA_PROVIDER, provider);
        context.setVariable("deletionKey", deletionKey);
        String content = templateEngine.process("followProviderEmail", context);
        sendEmail(user.getEmail(), "You are now following news and updates about " + provider.getProviderName(), content, false, true, user);
    }

    @Transactional
    public void sendProviderEditEmail(User user, DataProviderEdit dataProviderEdit,DataProviderEdit edit,DataProvider provider,DataProviderEdit editChanged)
    {
        Context context = new Context(Locale.US);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(DATA_PROVIDER_EDIT, dataProviderEdit);
        // context.setVariable("deletionKey", deletionKey);
        context.setVariable(DATA_EDIT, edit);
        context.setVariable(DATA_PROVIDER, provider);
        context.setVariable(DATA_INDUSTRIES, editChanged.getIndustries());
        context.setVariable(DATA_FEATURESLIST, editChanged.getFeatures());
        context.setVariable(DATA_GEO, editChanged.getGeographicalFoci());
        context.setVariable(DATA_PROVIDERPARTNERS, dataProviderEdit.getProviderDataPartner());
        context.setVariable(DATA_DISTRIBUTIONPARTNERS, dataProviderEdit.getDistributionDataPartner());
        context.setVariable(DATA_CONSUMERPARTNER, dataProviderEdit.getConsumerDataPartner());
        context.setVariable(DATA_COMPLIMENTARYPARTNER, dataProviderEdit.getComplementaryDataProviders());
        context.setVariable(DATA_CATEGORIES, editChanged.getCategories());
        //context.setVariable(DATA_PROVIDERTAGS, dataProviderEdit.getProviderTags());
        context.setVariable(DATA_SOURCES, editChanged.getSources());
        context.setVariable(DATA_ASSETCLASS, editChanged.getAssetClasses());
        context.setVariable(DATA_DELIVERYFORMAT, editChanged.getDeliveryFormats());
        context.setVariable(DATA_PRICING, editChanged.getDataProvidersPricingModels());
        context.setVariable(DATA_SECTOR, editChanged.getRelevantSectors());
        context.setVariable(DATA_DELIVERY, editChanged.getDeliveryMethods());
        //context.setVariable(DATA_INVESTOR, dataProviderEdit.getInvestorTypes());
        context.setVariable(DATA_FREQUENCIES, editChanged.getDeliveryFrequencies());
        context.setVariable(DATA_SECURITY, editChanged.getSecurityTypes());
        context.setVariable(DATA_BIASE, editChanged.getBiases());
        context.setVariable(DATA_OUTLIER, editChanged.getOutlierReasons());
        context.setVariable(DATA_GAP, editChanged.getDataGapsReasons());
        context.setVariable(DATA_ORGANIZATIONTYPE, editChanged.getOrganizationType());
        context.setVariable(DATA_MANAGERTYPE, editChanged.getManagerType());
        context.setVariable(DATA_INVESTORTYPE, editChanged.getInvestorTypes());
        context.setVariable(DATA_STRATEGIES, editChanged.getStrategies());
        context.setVariable(DATA_RESEARCHSTYLES, editChanged.getResearchStyles());
        context.setVariable(DATA_INVESTING, editChanged.getInvestingTimeFrame());
        context.setVariable(DATA_FOCUS, editChanged.getEquitiesMarketCap());
        context.setVariable(DATA_STYLE, editChanged.getEquitiesStyle());
        context.setVariable(DATA_IDENTIFIERS, editChanged.getIdentifiersAvailable());
        context.setVariable(DATA_ACCESS, editChanged.getAccessOffered());
        context.setVariable(DATA_PRODUCT, editChanged.getDataProductTypes());
        context.setVariable(DATA_TYPES, editChanged.getDataTypes());
        context.setVariable(DATA_LANGUAGE, editChanged.getLanguagesAvailable());
        context.setVariable(DATA_PAYMENT, editChanged.getPaymentMethodsOffered());
        context.setVariable(DATA_COMPETITORS, dataProviderEdit.getCompetitorProvider());
        context.setVariable(DATA_PRICINGDETAILS, dataProviderEdit.getPricingDetails());
        context.setVariable(DATA_PRICINGMODELS, editChanged.getDataProvidersPricingModels());
        if(null!=dataProviderEdit.getPartnership())
        {
            LookupCode lookupcode=lookupCodeRepository.findOne(dataProviderEdit.getPartnership());
            if(null!=lookupcode && null!=lookupcode.getLookupcode())
            {
                context.setVariable(DATA_PARTNERSHIP, lookupcode.getDescription());
            }

        }
      /*  if(null!=dataProviderEdit.getHeadcountBucket())
        {
            LookupCode lookupcode=lookupCodeRepository.findOne(dataProviderEdit.getHeadcountBucket());
            if(null!=lookupcode && null!=lookupcode.getLookupcode())
            {
                context.setVariable(DATA_HEADCOUNTBUCKET, lookupcode.getDescription());
            }
        }*/

        if(null!=edit.getProviderType() && null!=edit.getProviderType().getProviderType())
        {
            context.setVariable(DATA_PROVIDERTYPE, edit.getProviderType().getProviderType());
        }
        context.setVariable(DATA_DATECOLLECTION, dataProviderEdit.getDateCollectionBegan());
        context.setVariable(DATA_FREETRIAL, dataProviderEdit.getFreeTrialAvailable());
        if(null!=editChanged.getMainDataIndustry() && null!=editChanged.getMainDataIndustry().getDataIndustry())
        {
            context.setVariable(DATA_INDUSTRY, editChanged.getMainDataIndustry().getDataIndustry());

        }
        if(null!= editChanged.getMainDataCategory() && null!=editChanged.getMainDataCategory().getDatacategory())
        {
            context.setVariable(DATA_MAINCATEGORY, editChanged.getMainDataCategory().getDatacategory());
        }

         String content = templateEngine.process("providerEdit", context);
        // System.out.println("content"+content);
        sendEmail(user.getEmail(), "Your Data Profile Edits Have Been Received And Are Pending Approval", content, false, true, user);

    }

    @Transactional
    public void sendAdminEmailProviderEdit(User user, DataProviderEdit dataProviderEdit,DataProviderEdit edit,DataProvider provider,DataProviderEdit editChanged)
    {
        Context context = new Context(Locale.US);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(DATA_PROVIDER_EDIT, dataProviderEdit);
        // context.setVariable("deletionKey", deletionKey);
        context.setVariable(DATA_EDIT, edit);
        context.setVariable(DATA_PROVIDER, provider);
        context.setVariable(DATA_INDUSTRIES, editChanged.getIndustries());
        context.setVariable(DATA_FEATURESLIST, editChanged.getFeatures());
        context.setVariable(DATA_GEO, editChanged.getGeographicalFoci());
        context.setVariable(DATA_PROVIDERPARTNERS, dataProviderEdit.getProviderDataPartner());
        context.setVariable(DATA_DISTRIBUTIONPARTNERS, dataProviderEdit.getDistributionDataPartner());
        context.setVariable(DATA_CONSUMERPARTNER, dataProviderEdit.getConsumerDataPartner());
        context.setVariable(DATA_COMPLIMENTARYPARTNER, dataProviderEdit.getComplementaryDataProviders());
        context.setVariable(DATA_CATEGORIES, editChanged.getCategories());
       // context.setVariable(DATA_PROVIDERTAGS, dataProviderEdit.getProviderTags());
        context.setVariable(DATA_SOURCES, editChanged.getSources());
        context.setVariable(DATA_ASSETCLASS, editChanged.getAssetClasses());
        context.setVariable(DATA_DELIVERYFORMAT, editChanged.getDeliveryFormats());
        context.setVariable(DATA_PRICING, editChanged.getDataProvidersPricingModels());
        context.setVariable(DATA_SECTOR, editChanged.getRelevantSectors());
        context.setVariable(DATA_DELIVERY, editChanged.getDeliveryMethods());
        context.setVariable(DATA_INVESTOR, editChanged.getInvestorTypes());
        context.setVariable(DATA_FREQUENCIES, editChanged.getDeliveryFrequencies());
        context.setVariable(DATA_SECURITY, editChanged.getSecurityTypes());
        context.setVariable(DATA_BIASE, editChanged.getBiases());
        context.setVariable(DATA_OUTLIER, editChanged.getOutlierReasons());
        context.setVariable(DATA_GAP, editChanged.getDataGapsReasons());
        context.setVariable(DATA_ORGANIZATIONTYPE, editChanged.getOrganizationType());
        context.setVariable(DATA_MANAGERTYPE, editChanged.getManagerType());
        context.setVariable(DATA_INVESTORTYPE, editChanged.getInvestorTypes());
        context.setVariable(DATA_STRATEGIES, editChanged.getStrategies());
        context.setVariable(DATA_RESEARCHSTYLES, editChanged.getResearchStyles());
        context.setVariable(DATA_INVESTING, editChanged.getInvestingTimeFrame());
        context.setVariable(DATA_FOCUS, editChanged.getEquitiesMarketCap());
        context.setVariable(DATA_STYLE, editChanged.getEquitiesStyle());
        context.setVariable(DATA_IDENTIFIERS, editChanged.getIdentifiersAvailable());
        context.setVariable(DATA_ACCESS, editChanged.getAccessOffered());
        context.setVariable(DATA_PRODUCT, editChanged.getDataProductTypes());
        context.setVariable(DATA_TYPES, editChanged.getDataTypes());
        context.setVariable(DATA_LANGUAGE, editChanged.getLanguagesAvailable());
        context.setVariable(DATA_PAYMENT, editChanged.getPaymentMethodsOffered());
        context.setVariable(DATA_COMPETITORS, dataProviderEdit.getCompetitorProvider());
        context.setVariable(DATA_PRICINGDETAILS, dataProviderEdit.getPricingDetails());
        context.setVariable(DATA_PRICINGMODELS, editChanged.getDataProvidersPricingModels());
        if(null!=dataProviderEdit.getPartnership())
        {
            LookupCode lookupcode=lookupCodeRepository.findOne(dataProviderEdit.getPartnership());
            if(null!=lookupcode && null!=lookupcode.getLookupcode())
            {
                context.setVariable(DATA_PARTNERSHIP, lookupcode.getDescription());
            }

        }
        
     /*   if(null!=dataProviderEdit.getHeadcountBucket())
        {
            LookupCode lookupcode=lookupCodeRepository.findOne(dataProviderEdit.getHeadcountBucket());
            if(null!=lookupcode && null!=lookupcode.getLookupcode())
            {
                context.setVariable(DATA_HEADCOUNTBUCKET, lookupcode.getDescription());
            }
            
        }*/
        if(null!=edit.getProviderType() && null!=edit.getProviderType().getProviderType())
        {
            context.setVariable(DATA_PROVIDERTYPE, edit.getProviderType().getProviderType());

        }
        context.setVariable(DATA_DATECOLLECTION, dataProviderEdit.getDateCollectionBegan());
        context.setVariable(DATA_FREETRIAL, dataProviderEdit.getFreeTrialAvailable());
        if(null!=editChanged.getMainDataIndustry() && null!=editChanged.getMainDataIndustry().getDataIndustry())
        {
            context.setVariable(DATA_INDUSTRY, editChanged.getMainDataIndustry().getDataIndustry());

        }
        if(null!= editChanged.getMainDataCategory() && null!=editChanged.getMainDataCategory().getDatacategory())
        {
            context.setVariable(DATA_MAINCATEGORY, editChanged.getMainDataCategory().getDatacategory());

        }
        
        String content = templateEngine.process("providerEditToAdmin", context);
        
        sendEmail(applicationProperties.getAdminEmail(), "Provider Profile Edits Need to be Reviewed", content, false, true, user);
    }

    @Async
    @Transactional
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml, User user) {
        log.debug("Saving email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);
        Email email = new Email();
        email.setEmailAdress(to);
        email.setSubject(subject);
        email.setBody(content);
        email.setMultipart(isMultipart);
        email.setHtml(isHtml);
        email.setCreatedDate(LocalDateTime.now());
        if (user != null) {
            email.setUser(user);
        }
        emailRepository.save(email);
    }


    @Async
    @Transactional
    public void sendEmailEmailAddress(String to, String subject, String content, boolean isMultipart, boolean isHtml, EmailAddress emailAddress) {
        log.debug("Saving email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);
        Email email = new Email();
        email.setEmailAdress(to);
        email.setSubject(subject);
        email.setBody(content);
        email.setMultipart(isMultipart);
        email.setHtml(isHtml);
        email.setCreatedDate(LocalDateTime.now());
        emailRepository.save(email);
    }

    public void sendAdminEmail(String subject, String content, boolean isMultipart, boolean isHtml) {
        sendEmail(applicationProperties.getAdminEmail(), subject, content, isMultipart, isHtml);
    }

    public void sendRequestAdminEmail(String subject, String content, boolean isMultipart, boolean isHtml) {
        sendEmail(applicationProperties.getAdminEmail(), subject, content, isMultipart, isHtml);
    }

    public void sendAdminEmailFollowProvider(User user, DataProvider provider, boolean follow)
    {

        StringBuffer content = new StringBuffer();
        String title = follow ? "Details of follow:" : "Details of unfollow:";
        content.append("<p>")
            .append(title)
            .append("<br>")
            .append("<b>User Information</b><br>")
            .append("Name: ")
            .append(user.getFirstName())
            .append(" ")
            .append(user.getLastName())
            .append("<br>")
            .append("Email: ")
            .append(user.getLogin())
            .append("<br>");

        if(user.getCompany() != null)
        {
            content.append("Company Name: ")
                .append(user.getCompany())
                .append("<br>");
        }

        content.append("</p><p><b>Provider Information</b><br>")
            .append("Provider Name: ")
            .append("<a href='")
            .append(jHipsterProperties.getMail().getBaseUrl())
            .append("/#/providers/")
            .append(provider.getRecordID())
            .append("'>")
            .append(provider.getProviderName())
            .append("</a><br>")
            .append("Provider Website: ")
            .append("<a href='")
            .append(provider.getWebsite())
            .append("'>")
            .append(provider.getWebsite())
            .append("</a></p>");
        sendAdminEmail(user.getFirstName() + " " + user.getLastName() + (follow ? " followed " : " unfollowed ") + provider.getProviderName(), content.toString(), false, true);
    }

    public void sendAdminEmailFromTemplate(String subject, String templateName, boolean isMultipart, boolean isHtml, User relevantUser)
    {
        sendAdminEmailFromTemplate(subject, templateName,isMultipart,isHtml, relevantUser,null )   ;
    }

    public void sendAdminEmailFromTemplate(String subject, String templateName, boolean isMultipart, boolean isHtml, User relevantUser,Context context)
    {
        if(context==null){
            context = new Context(Locale.US);
        }

        context.setVariable(USER, relevantUser);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        sendEmail(applicationProperties.getAdminEmail(), subject, content, isMultipart, isHtml);
    }




    public void sendRegistrationEmailToAdminFromTemplate(String subject, String templateName, boolean isMultipart, boolean isHtml, User relevantUser, Connection connection)
    {
        Context context = new Context(Locale.US);
        context.setVariable(USER, relevantUser);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        if(connection != null)
        {
            context.setVariable(SOCIAL_CONNECTION, connection);
        }
        String content = templateEngine.process(templateName, context);
        sendEmail(applicationProperties.getAdminEmail(), subject, content, isMultipart, isHtml);
    }

    // LinkedIn API V2
    public void sendLinkedInRegistrationEmailToAdminFromTemplate(String subject, String templateName, boolean isMultipart, boolean isHtml, User relevantUser, SocialUserConnection connection)
    {
        Context context = new Context(Locale.US);
        context.setVariable(USER, relevantUser);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        if(connection != null)
        {
            context.setVariable(SOCIAL_CONNECTION, connection);
        }
        String content = templateEngine.process(templateName, context);
        sendEmail(applicationProperties.getAdminEmail(), subject, content, isMultipart, isHtml);
    }

    @Async
    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
     //   System.out.println("content"+content);
        String subject = messageSource.getMessage(titleKey, null, locale);
    //    System.out.println("subject"+subject);
        sendEmail(user.getEmail(), subject, content, false, true, user);
    }

    @Async
    public void sendEmailFromEmailAddressTemplate(EmailAddress emailAddress, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag("en");
        Context context = new Context(Locale.US);
        context.setVariable(EMAILADDRESS, emailAddress);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable("confirmationkey", emailAddress.getConfirmationkey());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        log.debug("Sending activation email to '{}'", emailAddress.getEmailAddress());
        sendEmailEmailAddress(emailAddress.getEmailAddress(), subject, content, false, true, emailAddress);
    }


    @Async
    public void sendUpdateEmailFromTemplate(User user, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, user);
    }

    @Async
    public void sendActivationEmail(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "activationEmail", "email.activation.title");
    }

    @Async
    public void sendEmailAddressCreationEmail(EmailAddress emailAddress) {
        sendEmailFromEmailAddressTemplate(emailAddress, "registrationEmailAddress", "email.emailaddressconfirmation.text4");
    }


    @Async
    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        //  sentSubscriptionConfirmationEmailForLinkedIn(user);
        sendEmailFromTemplate(user, "registrationEmail", "email.creation.title");

        log.debug("Sending creation email to admins");
    }
    @Async
    public void sendUpdationEmail(User user) {
        log.debug("Sending updation email to '{}'", user.getEmail());
        //  sentSubscriptionConfirmationEmailForLinkedIn(user);
        sendEmailFromTemplate(user, "updationEmailAccount", "email.updation.title");

        log.debug("Sending updation email to admins");
    }

    @Async
    public void sendUpdateEmail(User user) {
        log.debug("Sending Email updation email to '{}'", user.getEmail());
        //  sentSubscriptionConfirmationEmailForLinkedIn(user);
        sendUpdateEmailFromTemplate(user, "updationEmail", "email.updation.title");

        log.debug("Sending creation email to admins");
    }

    @Async
    public void sendCreationEmailToAdmin(User user)
    {
        String adminSubject = messageSource.getMessage("email.creation.admin.title", null, Locale.US);
        sendAdminEmailFromTemplate(adminSubject + " " + user.getFirstName() + " " + user.getLastName(), "registrationEmailToAdmin", false, true, user);
    }
    @Async
    public void sendUpdationnEmailToAdmin(User user)
    {
        String adminSubject = messageSource.getMessage("email.updation.admin.title", null, Locale.US);
        sendAdminEmailFromTemplate(adminSubject, "updationEmailToAdmin", false, true, user);
    }

    @Async
    public void sendPasswordResetMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "passwordResetEmail", "email.reset.title");
    }

    @Async
    public void sendPasswordResetConfirmationMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        StringBuffer content = new StringBuffer();
        content.append("Dear ")
            .append(user.getFirstName())
            .append(",<p>You have successfully reset and changed your password for Insights.<br>If this was not done by you, please reply to this email and let us know.</p>")
            .append("<p><em>Regards,<br>")
            .append("Amass Insights Team</em></p>");
        sendEmail(user.getEmail(), "Insights Password Reset", content.toString(),false, true);

    }

    @Async
    public void sendSocialRegistrationValidationEmail(User user, String provider) {
        sendSocialRegistrationValidationEmail(user, provider, null);
    }

    @Async
    public void sendSocialRegistrationValidationEmail(User user, String provider, Connection<?> connection) {
        log.debug("Sending social registration validation email to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable("provider", StringUtils.capitalize(provider));
        String content = templateEngine.process("socialRegistrationValidationEmail", context);
        String subject = messageSource.getMessage("email.creation.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, user);
        String adminSubject = messageSource.getMessage("email.creation.admin.title", null, locale);
        sendRegistrationEmailToAdminFromTemplate(adminSubject + " " + user.getFirstName() + " " + user.getLastName(), "registrationEmailToAdmin", false, true, user, connection);
    }

    @Async
    public void sendLinkedInRegistrationValidationEmail(User user, String provider, SocialUserConnection connection) {
        log.debug("Sending social registration validation email to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable("provider", StringUtils.capitalize(provider));
        String content = templateEngine.process("socialRegistrationValidationEmail", context);
        String subject = messageSource.getMessage("email.creation.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, user);
        String adminSubject = messageSource.getMessage("email.creation.admin.title", null, locale);
        sendLinkedInRegistrationEmailToAdminFromTemplate(adminSubject + " " + user.getFirstName() + " " + user.getLastName(), "registrationLinkedInEmailToAdmin", false, true, user, connection);
    }

    public void sendWeeklyNotifications(Context context, double newProviders, double newArticles, String emailID, String subject)
    {
        DateFormatter formatter = new DateFormatter();
        formatter.setStyle(DateFormat.LONG);
        String today = formatter.print(new Date(), Locale.US);
        context.setVariable("today", today);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process("weeklyNotifcationEmail", context);
        //sendEmail(applicationProperties.getAdminEmail(), subject, content, false, true);
        sendEmail(emailID, subject, content, false, true);
    }

    public void sendFollowCategoryEmail(User user, DataCategory category, String deletionKey) {
        Context context = new Context(Locale.US);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(DATA_CATEGORY_FOLLOW, category);
        context.setVariable("deletionKey", deletionKey);
        String content = templateEngine.process("followCategoryEmail", context);
        sendEmail(user.getEmail(), "You are now following news and updates for the " + category.getDatacategory()+" data category", content, false, true, user);

    }

    public void sendAdminEmailFollowCategory(User user, DataCategory category, boolean b) {
        Context context = new Context(Locale.US);
        context.setVariable(DATA_CATEGORY_FOLLOW, category);
        String adminSubject = messageSource.getMessage("email.category.follow.title", null, Locale.US);
        sendAdminEmailFromTemplate(user.getFirstName() + " " + user.getLastName()+" followed "+category.getDatacategory()+ " data category", "followcategoryToAdmin", false, true, user,context);

    }

    public void sendUnfollowCategoryEmail(User user, DataCategory category, String deletionKey) {
        Context context = new Context(Locale.US);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(DATA_CATEGORY_FOLLOW, category);
        context.setVariable("deletionKey", deletionKey);
        String content = templateEngine.process("unfollowCategoryEmail", context);
        sendEmail(user.getEmail(), "You have unfollowed all news and updates about " + category.getDatacategory(), content, false, true, user);

    }

    public void sendAdminEmailUnFollowCategory(User user, DataCategory category, boolean b) {
        Context context = new Context(Locale.US);
        context.setVariable(DATA_CATEGORY_FOLLOW, category);
        String adminSubject = messageSource.getMessage("email.category.unfollow.title", null, Locale.US);
        sendAdminEmailFromTemplate(user.getFirstName() + " " + user.getLastName() + " unfollowed " + category.getDatacategory() + " data category", "unfollowedcategoryToAdmin", false, true, user, context);
    }


    public void sentSubscriptionConfirmationEmailForLinkedIn(User user)
    {
        UserRequest userRequest=new UserRequest();
        userRequest.setUserName(user.getFirstName() + " " + user.getLastName());
        userRequest.setUserEmail(user.getEmail());
       // userRequest.setRequestType(REQUEST_TYPE_EMAIL_SUBSCRIPTION);
        userRequest.setRequestType(applicationProperties.getEmailSubscription());
        userRequest.setUserId(user.getId());
        String confirmationKey = RandomUtil.generateConfirmationKey();
        userRequest.setConfirmationkey(confirmationKey);
        userRequestRepository.save(userRequest);
        JSONObject variables = new  JSONObject().put(BASE_URL, jHipsterProperties.getMail().getBaseUrl()+"/#/email-action?api=newsletter-subscription&key="+confirmationKey);
        int templateID = Integer.parseInt(environment.getProperty("spring.mailjet.subscription-confirmation-templateID"));
        mailJetService.sendMail(userRequest.getUserEmail(), userRequest.getUserName(), templateID, "Insights Newsletter: Please Confirm Subscription", variables);
        String body=mailJetService.getMail(templateID);
        Email email=new Email();
        email.setSubject("Insights Newsletter: Please Confirm Subscription");
        email.setEmailAdress(userRequest.getUserEmail());
        email.setNotificationStatus(1);
        LocalDateTime now = LocalDateTime.now();  
        email.setSentDate(now);
        email.setCreatedDate(now);
        email.setTemplateId(templateID);
        if(null!=body)
        {
        	email.setBody(body);
        }
        emailRepository.save(email);
    }


    /* @Timed
     @Transactional
     @Scheduled(initialDelay = 10000, fixedDelay = 10000)
     protected void remainderEmail() {
         if (!process())
             return;

         List<User> emails =userRepository.findRemainderEmail();
         sendRemainderMail(emails);

     }

     @Timed
     @Transactional
     protected void sendRemainderMail(List<User> emails) {
         // TODO Auto-generated method stub

         for(User user:emails)
         {
             sendRemainderUserMail(user);
         }
     }


     protected void sendRemainderUserMail(User user) {
         // TODO Auto-generated method stub
         log.debug("Sending remainder email to '{}'", user.getEmail());
         if(!user.getSent()) {
         sendEmailFromTemplate(user, "remainderEmail", "email.remainder.title");
         user.setSent(true);
         userRepository.save(user);
         }
     }
 */
    @Async
    public void sendUserAccountActivation(User user,String inviteType) {
        log.debug("Sending Account Activation email to '{}'", user.getEmail());
        //  sentSubscriptionConfirmationEmailForLinkedIn(user);
        sendEmailFromTemplate(user, "userAccountActivation", "email.accountactivation.title");
        inviteType="General";
        sendAdminProviderActivationFromTemplate(user,"userProviderActivationAdmin","email.providerActivationAdmin.title",inviteType);

    }

    @Async
    public void sendUserProviderActivation(User user,String inviteType) {
        log.debug("Sending Account Activation email to '{}'", user.getEmail());
        //  sentSubscriptionConfirmationEmailForLinkedIn(user);
        sendEmailFromTemplate(user, "userProviderActivation", "email.providerActivation.title");
        inviteType="Provider";
        sendAdminProviderActivationFromTemplate(user,"userProviderActivationAdmin","email.providerActivationAdmin.title",inviteType);
    }

    @Async
    public void sendInvestor(User user,String inviteType) {
        log.debug("Sending Account Activation email to '{}'", user.getEmail());
        //  sentSubscriptionConfirmationEmailForLinkedIn(user);
        sendEmailFromTemplate(user, "investorActivation", "email.investorActivation.title");
        inviteType="Investor";
        sendAdminProviderActivationFromTemplate(user,"investorActivationAdmin","email.investorActivationAdmin.title",inviteType);
    }


    @Async
    public void sendAdminProviderActivationFromTemplate(User user, String templateName, String titleKey,String inviteType) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(INVITETYPE, inviteType);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
     //   System.out.println("content"+content);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(applicationProperties.getAdminEmail(), subject, content, false, true, user);
    }


    @Async
    public void inviteUserAccount(User user) {
        user.setFirstName("User");
        user.setLastName(user.getEmail());
        // TODO Auto-generated method stub
        sendAdminEmailFromTemplate(user, "inviteUserAccountAdmin", "email.inviteUserAccount.title");
        sendUserEmailFromTemplate(user, "inviteUserAccount", "email.inviteUserAccountUser.title");

    }

    @Async
    public void newsLetterSubscriptionAdmin(EmailAddress user) {
        // TODO Auto-generated method stub
    	sendAdminEmailFromTemplateNewsLetter(user, "newsLetterSignUpAdmin", "email.newsLetterSignUpAccount.title");

    }
    @Async
    public void sendAdminEmailFromTemplateNewsLetter(EmailAddress user, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag("en");
        Context context = new Context(locale);
        User users=new User();
        users.setEmail(user.getEmailAddress());
        context.setVariable(USER, users);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
     //   System.out.println("content"+content);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(applicationProperties.getAdminEmail(), subject, content, false, true, null);
    }
    
    @Async
    public void sendAdminEmailFromTemplate(User user, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
     //   System.out.println("content"+content);
        String subject = messageSource.getMessage(titleKey, null, locale);
        String subjectAdmin=subject+" "+user.getFirstName()+" "+user.getLastName();
        sendEmail(applicationProperties.getAdminEmail(), subjectAdmin, content, false, true, user);
    }
    @Async
    public void sendAdminEmailFromTemplateUserOnboarding(User user, String templateName, String titleKey,boolean value) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable("investor", value);
        context.setVariable("geographies", user.getGeographies());
        context.setVariable("assetClasses", user.getAssetClasses());
        context.setVariable("categories", user.getCategories());
        context.setVariable("sectors", user.getSectors());
        if(null!=user.getPartnership() && null!=user.getPartnership().getId())
        {
            LookupCode lookupcode=lookupCodeRepository.findOne(user.getPartnership().getId());
            if(null!=lookupcode && null!=lookupcode.getLookupcode())
            {
                context.setVariable(DATA_PARTNERSHIP, lookupcode.getDescription());
            }

        }
    //    System.out.println("valueeee"+value);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(applicationProperties.getAdminEmail(), subject, content, false, true, user);
    }

    @Async
    public void sendAdminEmailFromTemplateproviderOnboarding(User user, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
       // context.setVariable("investor", value);
        context.setVariable("geographies", user.getGeographies());
        context.setVariable("assetClasses", user.getAssetClasses());
        context.setVariable("categories", user.getCategories());
        context.setVariable("sectors", user.getSectors());
    //    System.out.println("valueeee"+value);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(applicationProperties.getAdminEmail(), subject, content, false, true, user);
    }

    @Async
    public void sendUserEmailFromTemplate(User user, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, user);
    }




    @Timed
    @Transactional
    public DataArticle getArticleApi() {
        // TODO Auto-generated method stub
        DataArticle article=dataArticleRepository.getSocialArticle();
     //   System.out.println("!!!!!"+article.getId());
        if(article.getId()!=null)
        {
            dataArticleRepository.update(article.getId());

        }
        DataArticle da = dataArticleRepository.findOneWithEagerRelationships(article.getId());
     //   System.out.println("########"+da.getId());
        DataTopic dataTopic=new DataTopic();
        for (DataTopic topic : article.getArticleTopics()) {
            dataTopic.setTopicName(topic.getTopicName());
            dataTopic.setHashtag(topic.getHashtag());

        }

        String adminSubject = messageSource.getMessage("email.articlesubject.title", null, Locale.US);
        sendAdminArticleEmailFromTemplate(adminSubject, "articleAdminEmail", false, true, article,dataTopic);
        return da;
    }

    public void sendAdminArticleEmailFromTemplate(String subject, String templateName, boolean isMultipart, boolean isHtml, DataArticle article, DataTopic dataTopic)
    {
        sendAdminArticleEmailTemplate(subject, templateName,isMultipart,isHtml, article,null,dataTopic )   ;
    }

    public void sendAdminArticleEmailTemplate(String subject, String templateName, boolean isMultipart, boolean isHtml, DataArticle article,Context context,DataTopic topic)
    {
        if(context==null){
            context = new Context(Locale.US);
        }
        context.setVariable(DATA_ARTICLE, article);
        context.setVariable(Data_Topic, topic);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        context.setVariable(URL, article.getUrl());
        String content = templateEngine.process(templateName, context);
        sendArticleAdminEmail(applicationProperties.getAdminEmail(), subject, content, isMultipart, isHtml);
    }

    public void sendArticleAdminEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        sendArticleEmail(to, subject, content, isMultipart, isHtml, null);
    }

    public void sendArticleEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml,
                                 DataProvider provider) {
        // TODO Auto-generated method stub
        Email email = new Email();
        email.setEmailAdress(to);
        email.setSubject(subject);
        email.setBody(content);
        email.setMultipart(isMultipart);
        email.setHtml(isHtml);
        email.setCreatedDate(LocalDateTime.now());
        emailRepository.save(email);
    }

    @Timed
    @Transactional
    public DataArticle sendlinkedIn() throws LinkException {
        // TODO Auto-generated method stub
        //	findOneByUserIDEqualsAndProviderID
        DataArticle article=dataArticleRepository.findBySentEqualsAndLinkedInEquals(true,false);
        if(article!=null)
        {
            String check=linkedIn(article);
            if(check!=null)
            {
                dataArticleRepository.updateLinkedInSent(article.getId());
            }
        }
        return article;

    }



    @Timed
    @Transactional
    public DataArticle sendTwitter() throws OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException, UnsupportedOperationException, IOException {
        // TODO Auto-generated method stub
        DataArticle article=dataArticleRepository.findBySent(true);
      /*  if(article.getTwitter()&&article.getLinkedIn())
        {
            dataArticleRepository.updateSentOnly(article.getId());
        }
*/
        if(!article.getTwitter())
        {
            try {
                String s=createTweet(article);
                if(s!=null)
                {
                    /*if(article.getLinkedIn())
                    {

                        dataArticleRepository.updateTwitter(article.getId());
                    }
                    else
                    {*/
                        dataArticleRepository.updateTwitterOnly(article.getId());

                    //}
                }

            } catch (TwitterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        }
        DataArticle da = dataArticleRepository.findOneWithEagerRelationships(article.getId());
        return da;
    }

    public Twitter getTwitterinstance() {
        // TODO Auto-generated method stub

        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
            .setOAuthConsumerKey("T4LqOa1pxsZKPk4nZpxVs9Va3")
            .setOAuthConsumerSecret("9q4UYR0nYOl3CROxIO0qMF0duP0ULd7ZBglsnKS4QClns7pu5k")
            .setOAuthAccessToken("3350754329-PwEh2ixQHluXCX3RtAWV7PQQr9uP20TJw2pek4W")
            .setOAuthAccessTokenSecret("omhVn1HiBhiRzD2JyXoYPjPijSIBie1UyBJhQSyT6QGpE");
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        return twitter;

    }
    public String createTweet(DataArticle article) throws TwitterException {
        Twitter twitter = getTwitterinstance();
        String hashTag=null;
        for(DataTopic topic:article.getArticleTopics())
        {
            if(null!=topic.getHashtag())
            {
                hashTag=topic.getHashtag();
            }
        }
    //    System.out.println("!!!!"+article.getUrl());
        String url=article.getUrl();
        String hashtag="#AmassInsights";
        String tweet=url+" "+hashTag+" "+hashtag;
        Status status = twitter.updateStatus(tweet);
        return status.getText();
    }


    @Async
    public void inviteUserLinkedInAccount(User user) {
        // TODO Auto-generated method stub
        sendAdminEmailFromTemplate(user, "inviteUserAccountLinkedInAdmin", "email.inviteUserAccount.title");
    }

    @Async
    public void inviteUseronboardingadmin(User user,boolean value) {

    	sendAdminEmailFromTemplateUserOnboarding(user, "userOnboardingAdmin", "email.userOnboarding.title", value);

    }
    @Async
    public void inviteProvideronboardingAdmin(User user) {

    	sendAdminEmailFromTemplateproviderOnboarding(user, "providerOnboardingAdmin", "email.providerOnboarding.title");

    }

    @Async
    public void sendUnsubscriptionEmail(User user) {
        // TODO Auto-generated method stub
        sendUnsubscriptionEmail(user,"unSubscriptionEmail","email.unSubscription.title");

    }



    private void sendUnsubscriptionEmail(User user, String templateName, String titleKey) {
        // TODO Auto-generated method stub
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
     //   System.out.println("content"+content);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, user);

    }
    public  String linkedIn(DataArticle article)
    {
        String hashTag=null;
        for(DataTopic topic:article.getArticleTopics())
        {
            if(null!=topic.getHashtag())
            {
                hashTag=topic.getHashtag();
            }
        }

        String profileUrl = "https://api.linkedin.com/v2/ugcPosts";
        String check=null;
        // Access token for the r_liteprofile permission
        String accessToken = "AQVQaHiftDdRE8OlRkuTV5Ek6JNtojfT24CzvNq7BrYDKLuS3DIetBvD9_FPsOKdLnq-fGkcW-zpvjyPeyQv55rgmz3vVz9X1O4gvFUvNU5zwDZjxrZ_qjkbHPQPNqB-o7OCv1lqJPJHUXsBAqFZD_zDtbBiPAiya6dzlRomIHs4M_14AOioCsFMDyv9Yj7AFwOdDCl9IxRkkCkVzex30VKNwMY4nr-zz9SvRTK3rWPa-a8dbfW7d22OCrI1GZ_4aJ7enJVD7s6ROitta23HdA8-7VFnZQLa3IESJCcyoUo14cCPa1qwrCJ3g--_883vVEg9B8U99dYgGlOGQrxBfWmgGRhopQ";
        String url=jHipsterProperties.getMail().getBaseUrl()+"/#/news/"+article.getRecordID();

        String json="{\r\n" +
            "    \"author\": \"urn:li:person:peCiRzAYR-\",\r\n" +
            "    \"lifecycleState\": \"PUBLISHED\",\r\n" +
            "    \"specificContent\": {\r\n" +
            "        \"com.linkedin.ugc.ShareContent\": {\r\n" +
            "            \"shareCommentary\": {\r\n" +
            "                \"text\": \""+hashTag+" "+"#AmassInsights"+"\"\r\n" +

            "            },\r\n" +
            "            \"shareMediaCategory\": \"ARTICLE\",\r\n" +
            "            \"media\": [\r\n" +
            "                {\r\n" +
            "                    \"status\": \"READY\",\r\n" +
            "                    \"originalUrl\": \""+url+"\",\r\n" +
            "                    \"title\": {\r\n" +
            "                        \"text\": \"Official LinkedIn Blog\"\r\n" +
            "                    }\r\n" +
            "                }\r\n" +
            "            ]\r\n" +
            "        }\r\n" +
            "    },\r\n" +
            "    \"visibility\": {\r\n" +
            "        \"com.linkedin.ugc.MemberNetworkVisibility\": \"CONNECTIONS\"\r\n" +
            "    }\r\n" +
            "}";

        try {
            JsonObject profileData = MailService.sendGetRequest(profileUrl, accessToken,json);
            check= profileData.toString();
         //   System.out.println(profileData.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }
    private static JsonObject sendGetRequest(String urlString, String accessToken,String json) throws Exception {
        URL url = new URL(urlString);
        HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Authorization", "Bearer " + accessToken);
        con.setRequestProperty("cache-control", "no-cache");
        con.setRequestProperty("X-Restli-Protocol-Version", "2.0.0");
        con.setRequestProperty("Content-Type", "application/json");
        OutputStream os = con.getOutputStream();
        os.write(json.getBytes());
        os.flush();

        if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
            throw new RuntimeException("Failed : HTTP error code : "
                + con.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
            (con.getInputStream())));

        String output;
        StringBuilder jsonString = new StringBuilder();

    //    System.out.println("Output from Server .... \n");
        while ((output = br.readLine()) != null) {
     //       System.out.println(output);
            jsonString.append(output);

        }

        br.close();
        JsonReader jsonReader = Json.createReader(new StringReader(jsonString.toString()));
        JsonObject jsonObject = jsonReader.readObject();
        return jsonObject;

          /*BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
          StringBuilder jsonString = new StringBuilder();
          String line;
          while ((line = br.readLine()) != null) {
            jsonString.append(line);
          }
          br.close();
      */
          /*JsonReader jsonReader = Json.createReader(new StringReader(jsonString.toString()));
          JsonObject jsonObject = jsonReader.readObject();
      */
    }


    @Async
    public void sendUpgradeConfirmation(UserCheckout checkOut) {
        // TODO Auto-generated method stub
        sendAdminUpgradeConfirmationEmail(checkOut, "upgradeConfirmationAdmin", "mail.admin.upgrade.title");

    }


    @Async
    public void sendAdminUpgradeConfirmationEmail(UserCheckout checkOut, String templateName, String titleKey) {
        UserCheckout userCheckout= userCheckoutRepository.finduser(checkOut.getUser().getId());
        User user=userCheckout.getUser();
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(USER_CHECKOUT, userCheckout);
    //    System.out.println("!!!"+userCheckout.getSubscription());
        if(checkOut.getSubscription().equalsIgnoreCase("Starter"))
        {
            context.setVariable("subscriptionPeriod", "Monthly");
        }
        else
        {
            context.setVariable("subscriptionPeriod", "yearly");

        }
        context.setVariable("localDateTime", userCheckout.getCreatedDate().toString());
        context.setVariable("localDate", userCheckout.getExpirationDate().toString());
        context.setVariable("amount",  userCheckout.getAmount().toString());
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(applicationProperties.getAdminEmail(), subject, content, false, true, user);
    }

    @Async
    public void sendUpgradeConfirmationUser(UserCheckout checkOut) {
        // TODO Auto-generated method stub
        sendUserUpgradeConfirmationEmail(checkOut, "upgradeConfirmationUser", "mail.admin.upgrade.User");

    }
    @Async
    public void sendUserUpgradeConfirmationEmail(UserCheckout checkOut, String templateName, String titleKey) {
        UserCheckout userCheckout= userCheckoutRepository.finduser(checkOut.getUser().getId());
        User user=userCheckout.getUser();
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(USER_CHECKOUT, userCheckout);
     //   System.out.println("!!!"+userCheckout.getSubscription());
        if(checkOut.getSubscription().equalsIgnoreCase("Starter"))
        {
            context.setVariable("subscriptionPeriod", "Monthly");
        }
        else
        {
            context.setVariable("subscriptionPeriod", "Yearly");

        }
        context.setVariable("localDateTime", userCheckout.getCreatedDate().toString());
        context.setVariable("localDate", userCheckout.getExpirationDate().toString());
        context.setVariable("amount",  userCheckout.getAmount().toString());
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, user);
    }


    @Async
	public void sendMyMatchesProvider(List<DataProvider> providerList, User user) {
		// TODO Auto-generated method stub
    	sendMyMatchesProviderEmail(providerList, "mymatches", "mail.admin.mymatches.provider",user);

	}

    @Async
   	public void sendNoMyMatchesProvider(User user) {
   		// TODO Auto-generated method stub
       	sendNoMyMatchesProviderEmail("nomymatches", "mail.admin.mymatches.provider",user);

   	}
    @Async
	private void sendNoMyMatchesProviderEmail(String templateName, String titleKey, User user) {
		// TODO Auto-generated method stub
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, user);
	}

    @Async
	private void sendMyMatchesProviderEmail(List<DataProvider> providerList, String templateName, String titleKey, User user) {
		// TODO Auto-generated method stub
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable("providerList", providerList);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true, user);
	}



	public void sendMailToUserProvider(DataProvider dataProvider,Long user) {
		// TODO Auto-generated method stub
		User users=userRepository.findOneByIdEquals(user);
        sendMailToUserProviderApproved(users,"adminApprovedProviderEdits","email.adminApprovedProvider.title",dataProvider);
       
	}


	@Async
	private void sendMailToUserProviderApproved(User user, String templateName, String titleKey, DataProvider dataProvider) {
		// TODO Auto-generated method stub
        Locale locale = Locale.forLanguageTag(user.getLangKey());
		 Context context = new Context(Locale.US);
	        context.setVariable(USER, user);
	        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
	        context.setVariable(DATA_PROVIDER, dataProvider);
	        String content = templateEngine.process(templateName, context);
	        String subject = messageSource.getMessage(titleKey, null, locale);
	        sendEmail(user.getEmail(), subject, content, false, true, user);
	}



	public void sendOperatorPermission(DataProvider dataProvider, User user) {
		// TODO Auto-generated method stub
        sendMailTosendOperatorPermission(user,"operatorPermissionApproved","email.operatorPermissionApproved.title",dataProvider);

	}



	private void sendMailTosendOperatorPermission(User user, String templateName, String titleKey, DataProvider dataProvider) {
		// TODO Auto-generated method stub
		  Locale locale = Locale.forLanguageTag(user.getLangKey());
			 Context context = new Context(Locale.US);
		        context.setVariable(USER, user);
		        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
		        context.setVariable(DATA_PROVIDER, dataProvider);
		        String content = templateEngine.process(templateName, context);
		        String subject = messageSource.getMessage(titleKey, null, locale);
		        sendEmail(user.getEmail(), subject, content, false, true, user);
	}
}



