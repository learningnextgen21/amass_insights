package com.ai.mp.service;
import com.ai.mp.domain.Authority;
import com.ai.mp.domain.EmailAddress;
import com.ai.mp.domain.SocialUserConnection;
import com.ai.mp.domain.User;
import com.ai.mp.repository.AuthorityRepository;
import com.ai.mp.repository.EmailAddressRepository;
import com.ai.mp.repository.SocialUserConnectionRepository;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.repository.search.UserSearchRepository;
import com.ai.mp.utils.RandomUtil;

import com.ai.util.Utils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

@Service
@Transactional
public class SocialService {

    private final Logger log = LoggerFactory.getLogger(SocialService.class);

    private final UsersConnectionRepository usersConnectionRepository;
    public final SocialUserConnectionRepository socialUserConnectionRepository;

    private final AuthorityRepository authorityRepository;

    private final EmailAddressRepository emailAddressRepository;

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final MailService mailService;

    private final UserSearchRepository userSearchRepository;
    private final Environment environment;

    public SocialService(UsersConnectionRepository usersConnectionRepository, AuthorityRepository authorityRepository,
                         PasswordEncoder passwordEncoder, UserRepository userRepository,
                         MailService mailService, UserSearchRepository userSearchRepository, Environment environment,
                         SocialUserConnectionRepository socialUserConnectionRepository, EmailAddressRepository emailAddressRepository) {

        this.usersConnectionRepository = usersConnectionRepository;
        this.authorityRepository = authorityRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.mailService = mailService;
        this.userSearchRepository = userSearchRepository;
        this.environment = environment;
        this.socialUserConnectionRepository = socialUserConnectionRepository;
        this.emailAddressRepository = emailAddressRepository;
    }

    public void deleteUserSocialConnection(String login) {
        ConnectionRepository connectionRepository = usersConnectionRepository.createConnectionRepository(login);
        connectionRepository.findAllConnections().keySet().stream()
            .forEach(providerId -> {
                connectionRepository.removeConnections(providerId);
                log.debug("Delete user social connection providerId: {}", providerId);
            });
    }

    public boolean createSocialUser(Connection<?> connection, String langKey, String role) {
        if (connection == null) {
            log.error("Cannot create social user because connection is null");
            throw new IllegalArgumentException("Connection cannot be null");
        }
        UserProfile userProfile = connection.fetchUserProfile();
        String providerId = connection.getKey().getProviderId();
        String imageUrl = connection.getImageUrl();
        boolean userExists = userRepository.findOneByEmail(userProfile.getEmail()).isPresent();
        User user = createUserIfNotExist(userProfile, langKey, providerId, imageUrl, null,role);
        createSocialConnection(user.getLogin(), connection);
        if(!userExists)
        {


            if (role.equalsIgnoreCase("ROLE_INVESTOR")) {
                user.setRole(" Asset Manager");
            }else {
                user.setRole(" Data Provider");
            }
            mailService.sendSocialRegistrationValidationEmail(user, providerId, connection);
            mailService.sentSubscriptionConfirmationEmailForLinkedIn(user);
        }
        return userExists;
    }

    private User createUserIfNotExist(UserProfile userProfile, String langKey, String providerId, String imageUrl, String company, String role) {
        String email = userProfile.getEmail();
        String userName = userProfile.getUsername();
        if (!StringUtils.isBlank(userName)) {
            userName = userName.toLowerCase(Locale.ENGLISH);
        }
        if (StringUtils.isBlank(email) && StringUtils.isBlank(userName)) {
            log.error("Cannot create social user because email and login are null");
            throw new IllegalArgumentException("Email and login cannot be null");
        }
        if (StringUtils.isBlank(email) && userRepository.findOneByLogin(userName).isPresent()) {
            log.error("Cannot create social user because email is null and login already exist, login -> {}", userName);
            throw new IllegalArgumentException("Email cannot be null with an existing login");
        }
        if (!StringUtils.isBlank(email)) {
            User user = userRepository.findUserByLogin(email);

            if (user != null) {
                if(user.getImageUrl() == null) {
                    user.setImageUrl(imageUrl);
                    userSearchRepository.save(user);
                    userRepository.save(user);
                }
                log.info("User already exist associate the connection to this account");
                return user;
            }
        }

        String login = getLoginDependingOnProviderId(userProfile, providerId);
        String encryptedPassword = passwordEncoder.encode(RandomStringUtils.random(10));
        Set<Authority> authorities = new HashSet<>(1);
        authorities.add(authorityRepository.findOne("ROLE_USER"));
        authorities.add(authorityRepository.findOne(role));

        User newUser = new User();
        newUser.setLogin(login);
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(userProfile.getFirstName());
        newUser.setLastName(userProfile.getLastName());
        newUser.setEmail(email);
        newUser.setCompany("your company name");
        newUser.setActivated(false);
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        newUser.setAuthorities(authorities);
        newUser.setLangKey(langKey);
        newUser.setImageUrl(imageUrl);
        newUser.setNotify_news_update(true);
        userSearchRepository.save(newUser);
        return userRepository.save(newUser);
    }
    // LinkedIn API V2
    public boolean createSocialLinkedinUser(SocialUserConnection userProfile, String firstName, String lastName, String langKey, String role,boolean linkedin,boolean mainUserExists,boolean socialUserExists,boolean news) {
        boolean userExists = userRepository.findOneByEmail(userProfile.getUserId()).isPresent();
        String providerId = userProfile.getProviderId();
        String imageUrl = userProfile.getImageURL();
        userProfile.setRank((long) 1);
        User user = createLinkedinUserIfNotExist(userProfile, providerId, imageUrl, null, firstName, lastName, langKey, role,linkedin,news);
        if(!userExists)
        {
            if (role.equalsIgnoreCase("ROLE_INVESTOR")) {
                user.setRole(" Asset Manager");
            }else {
                user.setRole(" Data Provider");
            }
        }
        if(role==null)
        {
        	user.setRole("\"Unsure->Please Input\"");
        }
        if(linkedin)
        {
        //    System.out.println("Linked ");
            mailService.inviteUserLinkedInAccount(user);
        }
        else if(!mainUserExists && !socialUserExists ){
            mailService.sendLinkedInRegistrationValidationEmail(user, providerId, userProfile);
            if(news)
            {
            mailService.sentSubscriptionConfirmationEmailForLinkedIn(user);
            }
        }

        EmailAddress emailAddress=emailAddressRepository.findByEmailAddress(user.getEmail());
        if (emailAddress!=null)
        {
            emailAddress.setUser(user.getId());
            EmailAddress result =emailAddressRepository.save(emailAddress);
        }else
        {
            EmailAddress newEmail=new EmailAddress();
            newEmail.setRecordID("rece"+ Utils.randomString(13,13));
            newEmail.setEmailAddress(user.getEmail());
            newEmail.setUser(user.getId());
            EmailAddress result =emailAddressRepository.save(newEmail);
        }
        createSocialUserConnection(userProfile);
        return userExists;
    }

    // LinkedIn API V2
    private User createLinkedinUserIfNotExist(SocialUserConnection userProfile, String providerId, String imageUrl, String company, String firstName, String lastName, String langKey, String role, boolean linkedin,boolean news) {
        String email = userProfile.getUserId();
        if (StringUtils.isBlank(email) && StringUtils.isBlank(email)) {
            log.error("Cannot create social user because email and login are null");
            throw new IllegalArgumentException("Email and login cannot be null");
        }
        if (StringUtils.isBlank(email) && userRepository.findOneByLogin(email).isPresent()) {
            log.error("Cannot create social user because email is null and login already exist, login -> {}", email);
            throw new IllegalArgumentException("Email cannot be null with an existing login");
        }
        if (!StringUtils.isBlank(email)) {
            User user = userRepository.findUserByLogin(email);

            if (!linkedin) {
                if (user != null) {
                    if (user.getImageUrl() == null) {
                        user.setImageUrl(imageUrl);
                        userSearchRepository.save(user);
                        userRepository.save(user);
                    }
                    log.info("User already exist associate the connection to this account");
                    return user;
                }
            }
        }
        String encryptedPassword = passwordEncoder.encode(RandomStringUtils.random(10));
        Set<Authority> authorities = new HashSet<>(1);
        authorities.add(authorityRepository.findOne("ROLE_USER"));
        if (role!=null){
            authorities.add(authorityRepository.findOne(role));
        }


        User newUser = new User();
        newUser.setLogin(email);
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setCompany("your company name");
        newUser.setActivated(false);
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        newUser.setAuthorities(authorities);
        newUser.setLangKey(langKey);
        newUser.setImageUrl(imageUrl);
        newUser.setNotify_news_update(news);
        if(role==null)
        {
        	newUser.setRole("\"Unsure->Please Input\"");

        }
    //    System.out.println("User 2 : " +newUser);
        if (linkedin){
            User uid=userRepository.findByEmail(email);
            newUser.setActivated(true);
            newUser.setActivationKey(null);
            newUser.setId(uid.getId());
        }
    //    System.out.println("User 2 : " +newUser);
        userSearchRepository.save(newUser);
        return userRepository.save(newUser);
    }

    public SocialUserConnection createSocialUserConnection(SocialUserConnection userConnection) {
        SocialUserConnection user = socialUserConnectionRepository.findOneByUserId(userConnection.getUserId());
        if (user != null) {
            log.info("Social User already exist associate the connection to this account");
            return user;
        } else {
            log.info("Social User created successfully!");
            return socialUserConnectionRepository.save(userConnection);
        }
    }

    /**
     * @return login if provider manage a login like Twitter or GitHub otherwise email address.
     *         Because provider like Google or Facebook didn't provide login or login like "12099388847393"
     */
    private String getLoginDependingOnProviderId(UserProfile userProfile, String providerId) {
        switch (providerId) {
            case "twitter":
                return userProfile.getUsername().toLowerCase();
            default:
                return userProfile.getEmail();
        }
    }

    private void createSocialConnection(String login, Connection<?> connection) {
        ConnectionRepository connectionRepository = usersConnectionRepository.createConnectionRepository(login);
        connectionRepository.addConnection(connection);
    }

    // LinkedIn API V2
    public String getAccessToken(String code, String state) {
        String linkedInClientId = environment.getProperty("spring.social.linkedin.client-id");
        String linkedInClientSecret = environment.getProperty("spring.social.linkedin.client-secret");
        String redirectUrl = environment.getProperty("spring.application.url") + environment.getProperty("jhipster.social.redirect-after-sign-in");
        String urlParameters = "grant_type=authorization_code&client_id="+linkedInClientId+"&client_secret="+linkedInClientSecret+"&code="+code+"&state="+state+"&redirect_uri="+redirectUrl;
        String urlParametersEncoded = "";
        try {
            urlParametersEncoded = URLEncoder.encode(urlParameters, "UTF-8");
        } catch(Exception e) {
         //   System.out.println("URL Encode error.." + e);
        }

        String url = "https://www.linkedin.com/oauth/v2/accessToken?"+urlParametersEncoded;
        String access_token = "null";
        try {
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("Host", "www.linkedin.com");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            /*System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + urlParameters);
            System.out.println("Response Code : " + responseCode);*/

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            //System.out.println(response.toString());

            JSONObject jsonObj = new JSONObject(response.toString());
            access_token = jsonObj.getString("access_token");
            //System.out.println(access_token);
        } catch(Exception e) {
            System.out.println("error token.." + e);
        }

        return access_token;
    }

    // LinkedIn API V2
    public JSONObject getUserProfile(String access_token) {

        String url = "https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))";
        JSONObject jsonObj = null;
        try {
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("GET");
            con.setRequestProperty("Host", "api.linkedin.com");
            con.setRequestProperty("Connection", "Keep-Alive");
            con.setRequestProperty("Authorization", "Bearer " + access_token);
            int responseCode = con.getResponseCode();
          //  System.out.println("GET Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                //System.out.println(response.toString());
                //print result
               // System.out.println(response.toString());

                jsonObj = new JSONObject(response.toString());
                return jsonObj;
            } else {
                System.out.println("GET request not worked");

            }
        } catch(Exception e) {
            System.out.println("User profile API error...");
            return null;
        }
        return jsonObj;
    }

    // LinkedIn API V2
    public JSONObject getUserEmail(String access_token) {

        String url = "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))";
        JSONObject jsonObj = null;
        try {
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("GET");
            con.setRequestProperty("Host", "api.linkedin.com");
            con.setRequestProperty("Connection", "Keep-Alive");
            con.setRequestProperty("Authorization", "Bearer " + access_token);
            int responseCode = con.getResponseCode();
           // System.out.println("GET Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
               // System.out.println(response.toString());

                jsonObj = new JSONObject(response.toString());
              //  System.out.println("EMAIL..." + jsonObj);
                return jsonObj;
            } else {
                System.out.println("GET request not worked");
            }
        } catch(Exception e) {
            System.out.println("User Email API error...");
            return null;
        }
        return jsonObj;
    }
}
