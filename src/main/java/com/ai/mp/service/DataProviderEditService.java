package com.ai.mp.service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ai.mp.domain.DataOrganization;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.DataProviderEdit;
import com.ai.mp.domain.LookupCode;
import com.ai.mp.repository.LookupCodeRepository;
import com.ai.mp.utils.BasicBean;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@Service
@Transactional
public class DataProviderEditService {

	@Autowired
	private LookupCodeRepository lookupcodeRepository;
	
	public DataProvider checkChangedvalues(DataProviderEdit dataProviderEdit, DataProvider dataProvider) throws JsonParseException, JsonMappingException, IOException {
		// TODO Auto-generated method stub
		//DataProviderEdit providerEdit= new DataProviderEdit();
		DataProvider provider=new DataProvider();
		DataOrganization organization=new DataOrganization();
        System.out.println("proid"+dataProviderEdit.getSecRegulated());

        if(null!=dataProviderEdit.getProviderDataPartner())
        {
        	provider.setEditProviderDataPartner(dataProviderEdit.getProviderDataPartner());
        }
        if(null!=dataProviderEdit.getDistributionDataPartner())
        {
        	provider.setEditDistributionDataPartner(dataProviderEdit.getDistributionDataPartner());
        }
        if(null!=dataProviderEdit.getConsumerDataPartner())
        {
        	provider.setEditConsumerDataPartner(dataProviderEdit.getConsumerDataPartner());
        }
        if(null!=dataProviderEdit.getCompetitorProvider()) {
        	provider.setEditCompetitorProvider(dataProviderEdit.getCompetitorProvider());
        }
        if(null!=dataProviderEdit.getComplementaryDataProviders())
        {
        	provider.setEditComplementaryDataProviders(dataProviderEdit.getComplementaryDataProviders());
        }
		 boolean setShortDescription=Objects.equals(dataProvider.getShortDescription(), dataProviderEdit.getShortDescription());
         if(!setShortDescription)
         {
        	 provider.setShortDescription(dataProviderEdit.getShortDescription());

         }

         boolean setLongDescription=Objects.equals(dataProvider.getLongDescription(), dataProviderEdit.getLongDescription());
         if(!setLongDescription)
         {
         	if(null!=dataProviderEdit.getLongDescription())
         	{
         		provider.setLongDescription(dataProviderEdit.getLongDescription().replaceAll("\\<[^>]*>",""));
         	}
         }

         boolean setNotes=Objects.equals(dataProvider.getNotes(), dataProviderEdit.getNotes());
         if(!setNotes)
         {
        	 provider.setNotes(dataProviderEdit.getNotes());

         }

         boolean a=Objects.equals(dataProvider.getDataUpdateFrequency(), dataProviderEdit.getDataUpdateFrequency());
         if(!a)
         {
        	 provider.setDataUpdateFrequency(dataProviderEdit.getDataUpdateFrequency());

         }

         boolean setDataUpdateFrequencyNotes=Objects.equals(dataProvider.getDataUpdateFrequencyNotes(), dataProviderEdit.getDataUpdateFrequencyNotes());
         if(!setDataUpdateFrequencyNotes)
         {
        	 provider.setDataUpdateFrequencyNotes(dataProviderEdit.getDataUpdateFrequencyNotes());

         }
         if(null!=dataProvider.getDateCollectionBegan()) {
             boolean setDateCollection=Objects.equals(dataProvider.getDateCollectionBegan().toString(), dataProviderEdit.getDateCollectionBegan());
           if(!setDateCollection) {
        	   if(null!=dataProviderEdit.getDateCollectionBegan()) {
        		   DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        		   LocalDate  d1 = LocalDate.parse(dataProviderEdit.getDateCollectionBegan(), df);
              // LocalDate localDate = LocalDate.parse(dataProviderEdit.getDateCollectionBegan());
        	   provider.setDateCollectionBegan(d1);
        	   }
           }
         }
         else {
        	 if(null!=dataProviderEdit.getDateCollectionBegan()) {
        		 //LocalDate localDate = LocalDate.parse(dataProviderEdit.getDateCollectionBegan());
        		 DateTimeFormatter df = DateTimeFormatter.ofPattern("MM/dd/yyyy");
      		   LocalDate  d1 = LocalDate.parse(dataProviderEdit.getDateCollectionBegan(), df);
          	   provider.setDateCollectionBegan(d1);
        	 }
         }
     	/*if(!dataProvider.getDateCollectionBegan().toString().equalsIgnoreCase(dataProviderEdit.getDateCollectionBegan()))

     	{
     	providerEdit.setDateCollectionBegan(dataProviderEdit.getDateCollectionBegan());
     	}*/
         boolean setDateCollectionRangeExplanation=Objects.equals(dataProvider.getDateCollectionRangeExplanation(), dataProviderEdit.getDateCollectionRangeExplanation());
         if(!setDateCollectionRangeExplanation)
         {
        	 provider.setDateCollectionRangeExplanation(dataProviderEdit.getDateCollectionRangeExplanation());

         }

         boolean setCollectionMethodsExplanation=Objects.equals(dataProvider.getCollectionMethodsExplanation(), dataProviderEdit.getCollectionMethodsExplanation());
         if(!setCollectionMethodsExplanation)
         {
        	 provider.setCollectionMethodsExplanation(dataProviderEdit.getCollectionMethodsExplanation());

         }
         boolean setPotentialDrawbacks=Objects.equals(dataProvider.getPotentialDrawbacks(), dataProviderEdit.getPotentialDrawbacks());
         if(!setPotentialDrawbacks)
         {
        	 provider.setPotentialDrawbacks(dataProviderEdit.getPotentialDrawbacks());

         }
      if(null!=dataProvider.getKeyDataFields()) {
    	  String key=dataProvider.getKeyDataFields();    	   
          boolean setKeyDataFields=Objects.equals(key, dataProviderEdit.getKeyDataFields());
          if(!setKeyDataFields)
          {
          	if(null!=dataProviderEdit.getKeyDataFields())
          	{
          		provider.setKeyDataFields(dataProviderEdit.getKeyDataFields());
          	}

          }
         }
      else
    	  {
    	  boolean setKeyDataFields=Objects.equals(dataProvider.getKeyDataFields(), dataProviderEdit.getKeyDataFields());
         if(!setKeyDataFields)
         {
         	if(null!=dataProviderEdit.getKeyDataFields())
         	{
         		provider.setKeyDataFields(dataProviderEdit.getKeyDataFields());
         	}

         }
    	  }

         boolean setSummary=Objects.equals(dataProvider.getSummary(), dataProviderEdit.getSummary());
         if(!setSummary)
         {
        	 provider.setSummary(dataProviderEdit.getSummary());

         }

         boolean setInvestor_clients=Objects.equals(dataProvider.getInvestor_clients(), dataProviderEdit.getInvestor_clients());
         if(!setInvestor_clients)
         {
        	 provider.setInvestor_clients(dataProviderEdit.getInvestor_clients());

         }


         boolean setPublicCompaniesCovered=Objects.equals(dataProvider.getPublicCompaniesCovered(), dataProviderEdit.getPublicCompaniesCovered());
         if(!setPublicCompaniesCovered)
         {
        	 provider.setPublicCompaniesCovered(dataProviderEdit.getPublicCompaniesCovered());

         }
         if(null!=dataProviderEdit.getFreeTrialAvailable()) {
        	  boolean setFreeTrialAvailable=Objects.equals(dataProvider.getFreeTrialAvailable(), Boolean.parseBoolean(dataProviderEdit.getFreeTrialAvailable()));
              if(!setFreeTrialAvailable)
              {
             	 provider.setFreeTrialAvailable(Boolean.parseBoolean(dataProviderEdit.getFreeTrialAvailable()));

              } 
         }
       
         if(null!=dataProvider.getProductDetails()) {
        	 String product=dataProvider.getProductDetails();    
        	 boolean setProductDetails=Objects.equals(product, dataProviderEdit.getProductDetails());
             if(!setProductDetails)
             {
             	if(null!=dataProviderEdit.getProductDetails())
             	{
             		provider.setProductDetails(dataProviderEdit.getProductDetails());
             	}

             }
         }
         else {
        	 boolean setProductDetails=Objects.equals(dataProvider.getProductDetails(), dataProviderEdit.getProductDetails());
             if(!setProductDetails)
             {
             	if(null!=dataProviderEdit.getProductDetails())
             	{
             		provider.setProductDetails(dataProviderEdit.getProductDetails());
             	}

             }
         }
         
         
         if(null!=dataProvider.getMainDataCategory()) {
          	  boolean setCategory=Objects.equals(dataProvider.getMainDataCategory(), dataProviderEdit.getMainDataCategory());
                if(!setCategory)
                {
                    //providerEdit.setPointInTimeAccuracy(dataProviderEdit.getPointInTimeAccuracy());
                    if(dataProviderEdit.getMainDataCategory()!=null)
                    {
                       
                            provider.setMainDataCategory(dataProviderEdit.getMainDataCategory());
                        
                    }
                } 
           }
           else {
          	 if(dataProviderEdit.getMainDataCategory()!=null)
               {
                  
                       provider.setMainDataCategory(dataProviderEdit.getMainDataCategory());
                   
               }
           }
         
         if(null!=dataProvider.getMainDataIndustry()) {
         	  boolean setIndustry=Objects.equals(dataProvider.getMainDataIndustry(), dataProviderEdit.getMainDataIndustry());
               if(!setIndustry)
               {
                   //providerEdit.setPointInTimeAccuracy(dataProviderEdit.getPointInTimeAccuracy());
                   if(dataProviderEdit.getMainDataIndustry()!=null)
                   {
                      
                           provider.setMainDataIndustry(dataProviderEdit.getMainDataIndustry());
                       
                   }
               } 
          }
          else {
         	 if(dataProviderEdit.getMainDataIndustry()!=null)
              {
                 
                      provider.setMainDataIndustry(dataProviderEdit.getMainDataIndustry());
                  
              }
          }
         
     /*	if(!dataProvider.getProductDetails().equalsIgnoreCase(dataProviderEdit.getProductDetails()))
     	{
     		providerEdit.setProductDetails(dataProviderEdit.getProductDetails());
     	}
     	if(!dataProvider.getMainDataCategory().getId().toString().equalsIgnoreCase(dataProviderEdit.getMainDataCategory().getId().toString()))
     	{
     		providerEdit.setMainDataCategory(dataProviderEdit.getMainDataCategory());
     	}
     	if(!dataProvider.getMainDataIndustry().getId().toString().equalsIgnoreCase(dataProviderEdit.getMainDataIndustry().getId().toString()))

     	{
     	providerEdit.setMainDataIndustry(dataProviderEdit.getMainDataIndustry());
     	}*/


      /*   boolean b=Objects.equals(dataProvider.getScoreInvestorsWillingness(), dataProviderEdit.getScoreInvestorsWillingness());
         if(!b)
         {
             providerEdit.setScoreInvestorsWillingness(dataProviderEdit.getScoreInvestorsWillingness());

         }*/
         if(null!=dataProvider.getScoreInvestorsWillingness()) {
        	  boolean setscoreInvestorWillingness=Objects.equals(dataProvider.getScoreInvestorsWillingness().getId(), Long.valueOf(dataProviderEdit.getScoreInvestorsWillingness()));
              if(!setscoreInvestorWillingness)
              {
                  //providerEdit.setPointInTimeAccuracy(dataProviderEdit.getPointInTimeAccuracy());
                  if(dataProviderEdit.getScoreInvestorsWillingness()!=null)
                  {
                      LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getScoreInvestorsWillingness().longValue());
                      if(lookupcode!=null)
                      {
                          provider.setScoreInvestorsWillingness(lookupcode);
                      }
                  }
              } 
         }
         else {
        	 if(dataProviderEdit.getScoreInvestorsWillingness()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getScoreInvestorsWillingness().longValue());
                 if(lookupcode!=null)
                 {
                     provider.setScoreInvestorsWillingness(lookupcode);
                 }
             }
         }
       
         /*boolean c=Objects.equals(dataProvider.getPricingModels(), dataProviderEdit.getPricingModels());
         if(!c)
         {
             providerEdit.setDataUpdateFrequency(dataProviderEdit.getDataUpdateFrequency());

         }*/
         boolean d=Objects.equals(dataProvider.getUserPermission(), dataProviderEdit.getUserPermission());
         if(!d)
         {
        	 provider.setUserPermission(dataProviderEdit.getUserPermission());

         }
         boolean sample=Objects.equals(dataProvider.getSampleOrPanelSize(), dataProviderEdit.getSampleOrPanelSize());
         if(!sample)
         {
        	 provider.setSampleOrPanelSize(dataProviderEdit.getSampleOrPanelSize());

         }
         if(null!=dataProvider.getUseCasesOrQuestionsAddressed()) {
        	 String useCases=dataProvider.getUseCasesOrQuestionsAddressed();
        	 boolean useCase=Objects.equals(useCases, dataProviderEdit.getUseCasesOrQuestionsAddressed());
             if(!useCase)
             {
             	if(null!=dataProviderEdit.getUseCasesOrQuestionsAddressed())
             	{
             		provider.setUseCasesOrQuestionsAddressed(dataProviderEdit.getUseCasesOrQuestionsAddressed());	
             	}

             }
         }
         else {
        	 boolean useCase=Objects.equals(dataProvider.getUseCasesOrQuestionsAddressed(), dataProviderEdit.getUseCasesOrQuestionsAddressed());
             if(!useCase)
             {
             	if(null!=dataProviderEdit.getUseCasesOrQuestionsAddressed())
             	{
             		provider.setUseCasesOrQuestionsAddressed(dataProviderEdit.getUseCasesOrQuestionsAddressed());	
             	}

             } 
         }
        
         boolean dataLicense=Objects.equals(dataProvider.getDataLicenseTypeDetails(), dataProviderEdit.getDataLicenseTypeDetails());
         if(!dataLicense)
         {
        	 provider.setDataLicenseTypeDetails(dataProviderEdit.getDataLicenseTypeDetails());

         }
         boolean setDataRoadmap=Objects.equals(dataProvider.getDataRoadmap(), dataProviderEdit.getDataRoadmap());
         if(!setDataRoadmap)
         {
        	 provider.setDataRoadmap(dataProviderEdit.getDataRoadmap());

         }
         boolean setDeliveryFrequencyNotes=Objects.equals(dataProvider.getDeliveryFrequencyNotes(), dataProviderEdit.getDeliveryFrequencyNotes());
         if(!setDeliveryFrequencyNotes)
         {
        	 provider.setDeliveryFrequencyNotes(dataProviderEdit.getDeliveryFrequencyNotes());

         }
         boolean setOutOfSampleData=Objects.equals(dataProvider.getOutOfSampleData(), dataProviderEdit.getOutOfSampleData());
         if(!setOutOfSampleData)
         {
        	 provider.setOutOfSampleData(dataProviderEdit.getOutOfSampleData());

         }
         boolean setTrialDuration=Objects.equals(dataProvider.getTrialDuration(), dataProviderEdit.getTrialDuration());
         if(!setTrialDuration)
         {
        	 provider.setTrialDuration(dataProviderEdit.getTrialDuration());

         }
         boolean setCity=Objects.equals(dataProvider.getOwnerOrganization().getCity(), dataProviderEdit.getCity());
         if(!setCity)
         {
             //providerEdit.setCity(dataProviderEdit.getCity());
        	 organization.setCity(dataProviderEdit.getCity());
        	 provider.setOwnerOrganization(organization);

         }
         boolean setState=Objects.equals(dataProvider.getOwnerOrganization().getState(), dataProviderEdit.getState());
         if(!setState)
         {
            // providerEdit.setState(dataProviderEdit.getState());
        	 organization.setState(dataProviderEdit.getState());
        	 provider.setOwnerOrganization(organization);
        	 }
         boolean setCountry=Objects.equals(dataProvider.getOwnerOrganization().getCountry(), dataProviderEdit.getCountry());
         if(!setCountry)
         {
             //providerEdit.setCountry(dataProviderEdit.getCountry());
        	 organization.setCountry(dataProviderEdit.getCountry());
        	 provider.setOwnerOrganization(organization);
         }
         boolean setZipCode=Objects.equals(dataProvider.getOwnerOrganization().getZipCode(), dataProviderEdit.getZipCode());
         if(!setZipCode)
         {
            // providerEdit.setZipCode(dataProviderEdit.getZipCode());
        	 organization.setZipCode(dataProviderEdit.getZipCode());
        	 provider.setOwnerOrganization(organization);

         }
         boolean setYearFounded=Objects.equals(dataProvider.getOwnerOrganization().getYearFounded(), dataProviderEdit.getYearFounded());
         if(!setYearFounded)
         {
             //providerEdit.setYearFounded(dataProviderEdit.getYearFounded());
        	 organization.setYearFounded(dataProviderEdit.getYearFounded());
        	 provider.setOwnerOrganization(organization);         }
         boolean setHeadCount=Objects.equals(dataProvider.getOwnerOrganization().getHeadCount(), dataProviderEdit.getHeadCount());
         if(!setHeadCount)
         {
             //providerEdit.setHeadCount(dataProviderEdit.getHeadCount());
        	 organization.setHeadCount(dataProviderEdit.getHeadCount());
        	 provider.setOwnerOrganization(organization);
        	 provider.getOwnerOrganization().setHeadCount(dataProviderEdit.getHeadCount());

         }
         if(null!=dataProviderEdit.getPublicEquitiesCoveredCount()) {
        	  boolean setPublicEquitesCovered=Objects.equals(dataProvider.getPublicEquitiesCoveredCount(), dataProviderEdit.getPublicEquitiesCoveredCount().intValue());
              if(!setPublicEquitesCovered && null!=dataProviderEdit.getPublicEquitiesCoveredCount())
              {
             	 provider.setPublicEquitiesCoveredCount(Integer.valueOf(dataProviderEdit.getPublicEquitiesCoveredCount().intValue()));

              }
         }
         boolean setPublicEquitesCoveredRange=Objects.equals(dataProvider.getPublicEquitiesCoveredRange(), dataProviderEdit.getPublicEquitiesCoveredRange());
         if(!setPublicEquitesCoveredRange)
         {
        	 provider.setPublicEquitiesCoveredRange(dataProviderEdit.getPublicEquitiesCoveredRange());

         }
         boolean setMainAssetClass=Objects.equals(dataProvider.getMainAssetClass(), dataProviderEdit.getMainAssetClass());
         if(!setMainAssetClass)
         {
        	 provider.setMainAssetClass(dataProviderEdit.getMainAssetClass());

         }
       
         if(null!=dataProvider.getSecRegulated()) {
         boolean setSecRegulated=Objects.equals(dataProvider.getSecRegulated().getId(), dataProviderEdit.getSecRegulated());
        /* System.out.println("pro"+dataProvider.getSecRegulated().getId());
         System.out.println("proedit"+dataProviderEdit.getSecRegulated());
         System.out.println("sec"+setSecRegulated);*/
         if(!setSecRegulated)
         {
             //providerEdit.setSecRegulated(dataProviderEdit.getSecRegulated());
             if(dataProviderEdit.getSecRegulated()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getSecRegulated());
                 provider.setSecRegulated(lookupcode);
             }
         }
         }
         else
         {
        	 if(dataProviderEdit.getSecRegulated()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getSecRegulated());
                 provider.setSecRegulated(lookupcode);
             }
         }
         if(null!=dataProvider.getDataLicenseType())
         {
         boolean setDataLicenceType=Objects.equals(dataProvider.getDataLicenseType().getId(), dataProviderEdit.getDataLicenseType());
         if(!setDataLicenceType)
         {
             //providerEdit.setSecRegulated(dataProviderEdit.getSecRegulated());
             if(dataProviderEdit.getDataLicenseType()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getDataLicenseType());
                 provider.setDataLicenseType(lookupcode);
             }
         }
         }
         else {
        	 if(dataProviderEdit.getDataLicenseType()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getDataLicenseType());
                 provider.setDataLicenseType(lookupcode);
             }
         }
         if(null!=dataProvider.getDuplicatesCleaned())
         {
         boolean setDuplicatesCleaned=Objects.equals(dataProvider.getDuplicatesCleaned().getId(), dataProviderEdit.getDuplicatesCleaned());
         if(!setDuplicatesCleaned)
         {
             //providerEdit.setSecRegulated(dataProviderEdit.getSecRegulated());
             if(dataProviderEdit.getDuplicatesCleaned()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getDuplicatesCleaned());
                 provider.setDuplicatesCleaned(lookupcode);
             }
         }
         }
         else {
        	  if(dataProviderEdit.getDuplicatesCleaned()!=null)
              {
                  LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getDuplicatesCleaned());
                  provider.setDuplicatesCleaned(lookupcode);
              }
         }
         /*boolean setProviderType=Objects.equals(dataProvider.getProviderType().getId(), dataProviderEdit.getProviderType().getId());
         {
         	if(!setProviderType)
         	{
         		if(dataProviderEdit.getProviderType()!=null)
         		{
         			DataProviderType providers=dataProviderTypeRepository.findOne(dataProviderEdit.getProviderType().getId());
         			if(providers!=null)
         			{
         				provider.setProviderType(providers);
         			}
         		}
         	}
         }*/

         /*if(!dataProvider.getProviderType().getId().toString().equalsIgnoreCase(dataProviderEdit.getProviderType().getId().toString())) {
     		providerEdit.setProviderType(dataProviderEdit.getProviderType());
     	}*/
         boolean setInvestorClientsCount=Objects.equals(dataProvider.getInvestorClientsCount(), dataProviderEdit.getInvestorClientsCount());
         if(!setInvestorClientsCount)
         {
        	 provider.setInvestorClientsCount(dataProviderEdit.getInvestorClientsCount());

         }
         if(null!=dataProvider.getTrialAgreementType())
         {
         boolean setTrialAgreementType=Objects.equals(dataProvider.getTrialAgreementType().getId(), dataProviderEdit.getTrialAgreementType());
         if(!setTrialAgreementType)
         {
             //providerEdit.setTrialAgreementType(dataProviderEdit.getTrialAgreementType());
             if(dataProviderEdit.getTrialAgreementType()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getTrialAgreementType());
                 if(lookupcode!=null)
                 {
                	 provider.setTrialAgreementType(lookupcode);
                 }
             }
         }
         }
         else {
        	 if(dataProviderEdit.getTrialAgreementType()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getTrialAgreementType());
                 if(lookupcode!=null)
                 {
                	 provider.setTrialAgreementType(lookupcode);
                 }
             }
         }
         if(null!=dataProvider.getTrialPricingModel())
         {
         boolean setTrialPricingModel=Objects.equals(dataProvider.getTrialPricingModel().getId(), dataProviderEdit.getTrialPricingModel());
         if(!setTrialPricingModel)
         {
             //providerEdit.setTrialPricingModel(dataProviderEdit.getTrialPricingModel());
             if(dataProviderEdit.getTrialPricingModel()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getTrialPricingModel());
                 if(lookupcode!=null)
                 {

                     provider.setTrialPricingModel(lookupcode);
                 }
             }
         }
         }
         else {
        	 if(dataProviderEdit.getTrialPricingModel()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getTrialPricingModel());
                 if(lookupcode!=null)
                 {

                     provider.setTrialPricingModel(lookupcode);
                 }
             }
         }
         boolean setTrialPricingDetails=Objects.equals(dataProvider.getTrialPricingDetails(), dataProviderEdit.getTrialPricingDetails());
         if(!setTrialPricingDetails)
         {
        	 provider.setTrialPricingDetails(dataProviderEdit.getTrialPricingDetails());

         }
         boolean setPricingDetails=Objects.equals(dataProvider.getPricingDetails(), dataProviderEdit.getPricingDetails());
         if(!setPricingDetails)
         {
        	 provider.setPricingDetails(dataProviderEdit.getPricingDetails());

         }
         boolean setLagTime=Objects.equals(dataProvider.getLagTime(), dataProviderEdit.getLagTime());
         if(!setLagTime)
         {
        	 provider.setLagTime(dataProviderEdit.getLagTime());

         }
         boolean setCompetitiveDifferentiators=Objects.equals(dataProvider.getCompetitiveDifferentiators(), dataProviderEdit.getCompetitiveDifferentiators());
         if(!setCompetitiveDifferentiators)
         {
        	 provider.setCompetitiveDifferentiators(dataProviderEdit.getCompetitiveDifferentiators());

         }

         boolean setDiscussionAnalytics=Objects.equals(dataProvider.getDiscussionAnalytics(), dataProviderEdit.getDiscussionAnalytics());
         if(!setDiscussionAnalytics)
         {
        	 provider.setDiscussionAnalytics(dataProviderEdit.getDiscussionAnalytics());

         }
         boolean setDiscussionCleanliness=Objects.equals(dataProvider.getDiscussionCleanliness(), dataProviderEdit.getDiscussionCleanliness());
         if(!setDiscussionCleanliness)
         {
        	 provider.setDiscussionCleanliness(dataProviderEdit.getDiscussionCleanliness());

         }
         boolean setSampleOrPanelBiases=Objects.equals(dataProvider.getSampleOrPanelBiases(), dataProviderEdit.getSampleOrPanelBiases());
         if(!setSampleOrPanelBiases)
         {
        	 provider.setSampleOrPanelBiases(dataProviderEdit.getSampleOrPanelBiases());

         }
         boolean setDatasetSize=Objects.equals(dataProvider.getDatasetSize(), dataProviderEdit.getDatasetSize());
         if(!setDatasetSize)
         {
        	 provider.setDatasetSize(dataProviderEdit.getDatasetSize());

         }
         boolean setDatasetNumberOfRows=Objects.equals(dataProvider.getDatasetNumberOfRows(), dataProviderEdit.getDatasetNumberOfRows());
         if(!setDatasetNumberOfRows)
         {
        	 provider.setDatasetNumberOfRows(dataProviderEdit.getDatasetNumberOfRows());

         }
         boolean setNumberOfAnalystEmployees=Objects.equals(dataProvider.getNumberOfAnalystEmployees(), dataProviderEdit.getNumberOfAnalystEmployees());
         if(!setNumberOfAnalystEmployees)
         {
        	 provider.setNumberOfAnalystEmployees(dataProviderEdit.getNumberOfAnalystEmployees());

         }
         if(null!=dataProvider.getPointInTimeAccuracy())
         {
         boolean setPointInTimeAccuracy=Objects.equals(dataProvider.getPointInTimeAccuracy().getId(), dataProviderEdit.getPointInTimeAccuracy());
         if(!setPointInTimeAccuracy)
         {
             //providerEdit.setPointInTimeAccuracy(dataProviderEdit.getPointInTimeAccuracy());
             if(dataProviderEdit.getPointInTimeAccuracy()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getPointInTimeAccuracy());
                 if(lookupcode!=null)
                 {
                     provider.setPointInTimeAccuracy(lookupcode);
                 }
             }
         }
         }
         else {
        	 if(dataProviderEdit.getPointInTimeAccuracy()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getPointInTimeAccuracy());
                 if(lookupcode!=null)
                 {
                     provider.setPointInTimeAccuracy(lookupcode);
                 }
             }
         }
         boolean setDataSourcesDetails=Objects.equals(dataProvider.getDataSourcesDetails(), dataProviderEdit.getDataSourcesDetails());
         if(!setDataSourcesDetails)
         {
        	 provider.setDataSourcesDetails(dataProviderEdit.getDataSourcesDetails());

         }
         if(null!=dataProvider.getDataGaps())
         {
         boolean setDataGaps=Objects.equals(dataProvider.getDataGaps().getId(), dataProviderEdit.getDataGaps());
         if(!setDataGaps)
         {
             //providerEdit.setDataGaps(dataProviderEdit.getDataGaps());
             if(dataProviderEdit.getDataGaps()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getDataGaps());
                 if(lookupcode!=null)
                 {
                     provider.setDataGaps(lookupcode);
                 }
             }
         }
         }
         else {
        	 if(dataProviderEdit.getDataGaps()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getDataGaps());
                 if(lookupcode!=null)
                 {
                     provider.setDataGaps(lookupcode);
                 }
             }
         }
         if(null!=dataProvider.getIncludesOutliers()) {
         boolean setIncludesOutliers=Objects.equals(dataProvider.getIncludesOutliers().getId(), dataProviderEdit.getIncludesOutliers());
         if(!setIncludesOutliers)
         {
             //providerEdit.setIncludesOutliers(dataProviderEdit.getIncludesOutliers());
             if(dataProviderEdit.getIncludesOutliers()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getIncludesOutliers());
                 if(lookupcode!=null)
                 {
                     provider.setIncludesOutliers(lookupcode);
                 }
             }
         }
         }
         else {
        	 if(dataProviderEdit.getIncludesOutliers()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getIncludesOutliers());
                 if(lookupcode!=null)
                 {
                     provider.setIncludesOutliers(lookupcode);
                 }
             }
         }
         if(null!=dataProvider.getNormalized()) {
         boolean setNormalized=Objects.equals(dataProvider.getNormalized().getId(), dataProviderEdit.getNormalized());
         if(!setNormalized)
         {
             //providerEdit.setNormalized(dataProviderEdit.getNormalized());
             if(dataProviderEdit.getNormalized()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getNormalized());
                 if(lookupcode!=null)
                 {
                     provider.setNormalized(lookupcode);
                 }
             }
         }
         }
         else {
        	 if(dataProviderEdit.getNormalized()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getNormalized());
                 if(lookupcode!=null)
                 {
                     provider.setNormalized(lookupcode);
                 }
             }
         }
         boolean setInvestorClientBucket=Objects.equals(dataProvider.getInvestorClientBucket(), dataProviderEdit.getInvestorClientBucket());
         if(!setInvestorClientBucket)
         {
        	 provider.setInvestorClientBucket(dataProviderEdit.getInvestorClientBucket());

         }
         if(null!=dataProvider.getSubscriptionTimePeriod()) {
        	 boolean setSubscriptionTimePeriod=Objects.equals(dataProvider.getSubscriptionTimePeriod().getId(), dataProviderEdit.getSubscriptionTimePeriod());
             if(!setSubscriptionTimePeriod)
             {
                 //providerEdit.setSubscriptionTimePeriod(dataProviderEdit.getSubscriptionTimePeriod());
                 if(dataProviderEdit.getSubscriptionTimePeriod()!=null)
                 {
                     LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getSubscriptionTimePeriod());
                     if(lookupcode!=null)
                     {
                         provider.setSubscriptionTimePeriod(lookupcode);
                     }
                 }
             }	 
         }
         else {
        	 if(dataProviderEdit.getSubscriptionTimePeriod()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getSubscriptionTimePeriod());
                 if(lookupcode!=null)
                 {
                     provider.setSubscriptionTimePeriod(lookupcode);
                 }
             }
         }
         if(null!=dataProviderEdit.getYearFirstDataProductLaunched()) {
        	  boolean setYearFirstDataProductLaunched=Objects.equals(dataProvider.getYearFirstDataProductLaunched(), dataProviderEdit.getYearFirstDataProductLaunched().intValue());
              if(!setYearFirstDataProductLaunched)
              {
             	 provider.setYearFirstDataProductLaunched(Integer.valueOf(dataProviderEdit.getYearFirstDataProductLaunched().intValue()));

              }
         }
       
         if(null!=dataProvider.getMonthlyPriceRange()) {
        	 boolean setMonthlyPriceRange=Objects.equals(dataProvider.getMonthlyPriceRange().getId(), dataProviderEdit.getMonthlyPriceRange());
             if(!setMonthlyPriceRange)
             {
                 //providerEdit.setMonthlyPriceRange(dataProviderEdit.getMonthlyPriceRange());
                 if(dataProviderEdit.getMonthlyPriceRange()!=null)
                 {
                     LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getMonthlyPriceRange());
                     if(lookupcode!=null)
                     {
                         provider.setMonthlyPriceRange(lookupcode);
                     }
                 }
             } 
         }
         else {
        	 if(dataProviderEdit.getMonthlyPriceRange()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getMonthlyPriceRange());
                 if(lookupcode!=null)
                 {
                     provider.setMonthlyPriceRange(lookupcode);
                 }
             }
         }
         if(null!=dataProvider.getSubscriptionModel()) {
        	 boolean setSubscriptionModel=Objects.equals(dataProvider.getSubscriptionModel().getId(), dataProviderEdit.getSubscriptionModel());
             if(!setSubscriptionModel)
             {
                 //providerEdit.setSubscriptionModel(dataProviderEdit.getSubscriptionModel());
                 if(dataProviderEdit.getSubscriptionModel()!=null)
                 {
                     LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getSubscriptionModel());
                     if(lookupcode!=null)
                     {
                         provider.setSubscriptionModel(lookupcode);
                     }
                 }
             } 
         }
         else{
        	 if(dataProviderEdit.getSubscriptionModel()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getSubscriptionModel());
                 if(lookupcode!=null)
                 {
                     provider.setSubscriptionModel(lookupcode);
                 }
             }
         }
         
         boolean setMinimumYearlyPrice=Objects.equals(dataProvider.getMinimumYearlyPrice(), dataProviderEdit.getMinimumYearlyPrice());
         if(!setMinimumYearlyPrice)
         {
        	 provider.setMinimumYearlyPrice(dataProviderEdit.getMinimumYearlyPrice());

         }
         boolean setMaximumYearlyPrice=Objects.equals(dataProvider.getMaximumYearlyPrice(), dataProviderEdit.getMaximumYearlyPrice());
         if(!setMaximumYearlyPrice)
         {
        	 provider.setMaximumYearlyPrice(dataProviderEdit.getMaximumYearlyPrice());

         }
         if(null!=dataProvider.getNumberOfDataSources()) {
        	 boolean setNumberOfDataSources=Objects.equals(dataProvider.getNumberOfDataSources().getId(), dataProviderEdit.getNumberOfDataSources());
             if(!setNumberOfDataSources)
             {
                 //providerEdit.setNumberOfDataSources(dataProviderEdit.getNumberOfDataSources());
                 if(dataProviderEdit.getNumberOfDataSources()!=null)
                 {
                     LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getNumberOfDataSources());
                     if(lookupcode!=null)
                     {
                         provider.setNumberOfDataSources(lookupcode);
                     }
                 }
             }
         }
         else {
        	 if(dataProviderEdit.getNumberOfDataSources()!=null)
             {
                 LookupCode lookupcode=lookupcodeRepository.findOne(dataProviderEdit.getNumberOfDataSources());
                 if(lookupcode!=null)
                 {
                     provider.setNumberOfDataSources(lookupcode);
                 }
             }
         }

         boolean setPrivacy=Objects.equals(dataProvider.getPrivacy(), dataProviderEdit.getPrivacy());
         if(!setPrivacy)
         {
        	 provider.setPrivacy(dataProviderEdit.getPrivacy());

         }
         boolean setYearExit=Objects.equals(dataProvider.getOwnerOrganization().getYearExit(), dataProviderEdit.getYearExit());
         if(!setYearExit)
         {
            // providerEdit.setYearExit(dataProviderEdit.getYearExit());
             organization.setYearExit(dataProviderEdit.getYearExit());
        	 provider.setOwnerOrganization(organization);

         }
         boolean setFinancialPosition=Objects.equals(dataProvider.getOwnerOrganization().getFinancialPosition(), dataProviderEdit.getFinancialPosition());
         if(!setFinancialPosition)
         {
             //providerEdit.setFinancialPosition(dataProviderEdit.getFinancialPosition());
        	 organization.setFinancialPosition(dataProviderEdit.getFinancialPosition());
        	 provider.setOwnerOrganization(organization);
         }
         boolean setClientBase=Objects.equals(dataProvider.getOwnerOrganization().getClientBase(), dataProviderEdit.getClientBase());
         if(!setClientBase)
         {
             //providerEdit.setClientBase(dataProviderEdit.getClientBase());
        	 organization.setClientBase(dataProviderEdit.getClientBase());
        	 provider.setOwnerOrganization(organization);
         }

         boolean setHeadcountNumber=Objects.equals(dataProvider.getOwnerOrganization().getHeadcountNumber(), dataProviderEdit.getHeadcountNumber());
         if(!setHeadcountNumber)
         {
        	 organization.setHeadcountNumber(dataProviderEdit.getHeadcountNumber());
        	 provider.setOwnerOrganization(organization);
             //providerEdit.setHeadcountNumber(dataProviderEdit.getHeadcountNumber());

         }
        /* boolean setHeadcountBucket=Objects.equals(dataProvider.getHeadcountBucket(), dataProviderEdit.getHeadcountBucket());
         if(!setHeadcountBucket)
         {
             providerEdit.setHeadcountBucket(dataProviderEdit.getHeadcountBucket());

         }*/
        /* boolean setPublicCompaniesCoveredNum=Objects.equals(dataProvider.getPublicCompaniesCoveredNum(), dataProviderEdit.getPublicCompaniesCoveredNum());
         if(!setPublicCompaniesCoveredNum)
         {
             providerEdit.setPublicCompaniesCoveredNum(dataProviderEdit.getPublicCompaniesCoveredNum());

         }*/
       /*  boolean setProviderName=Objects.equals(dataProvider.getProviderName(), dataProviderEdit.getProviderName());
         if(!setProviderName)
         {
             providerEdit.setProviderName(dataProviderEdit.getProviderName());

         }
         boolean setWebsite=Objects.equals(dataProvider.getWebsite(), dataProviderEdit.getWebsite());
         if(!setWebsite)
         {
             providerEdit.setWebsite(dataProviderEdit.getWebsite());

         }*/
        if(!dataProvider.getProviderName().equalsIgnoreCase(dataProviderEdit.getProviderName()))
         {
             provider.setProviderName(dataProviderEdit.getProviderName());
         }
         if(!dataProvider.getWebsite().equalsIgnoreCase(dataProviderEdit.getWebsite()))
         {
             provider.setWebsite(dataProviderEdit.getWebsite());
         }
        /* if(null!=dataProviderEdit.getProviderType()&& null!=dataProviderEdit.getProviderType())
         {
         	//System.out.println("ind"+dataProviderEdit.getProviderType().getProviderType());
        	 provider.setProviderType(dataProviderEdit.getProviderType());;
         }*/
         boolean setProviderType=Objects.equals(dataProvider.getProviderType().getId(), dataProviderEdit.getProviderType().getId());
         {
         	if(!setProviderType)
         	{
         		if(dataProviderEdit.getProviderType()!=null)
         		{
         			//DataProviderType providers=dataProviderTypeRepository.findOne(dataProviderEdit.getProviderType().getId());
         			/*if(providers!=null)
         			{
         				provider.setProviderType(providers);
         			}*/
     				provider.setProviderType(dataProviderEdit.getProviderType());

         		}
         	}
         }
         //multiselect values
         
         ObjectMapper mapper = new ObjectMapper();
    	 //System.out.println("dataProviders"+dataProvider.getMainDataCategory().getDatacategory());
    	 
//sources
		 
		String sourcesJson = new Gson().toJson(dataProvider.getSources());
		 /*   ArrayList<BasicBean> sourcesListJson = mapper.readValue(sourcesJson,
		            new TypeReference<ArrayList<BasicBean>>(){});*/
		    
		    String editsourcesJson = new Gson().toJson(dataProviderEdit.getSources());
		   /* ArrayList<BasicBean> editsourcesListJson = mapper.readValue(editsourcesJson,
		            new TypeReference<ArrayList<BasicBean>>(){});
		    */
		    if(!Objects.equals(sourcesJson, editsourcesJson))
		    {
		    	//System.out.println("not ewqual");
		    	if(null!=dataProviderEdit.getSources()) {
			         provider.setSources(dataProviderEdit.getSources());
		    	}

		    }
		    
		   /* ArrayList<BasicBean> sourcesList= new ArrayList<BasicBean>();  
		    if(null!=editsourcesListJson) {
		    	 for(BasicBean b:editsourcesListJson)
				 {
					 if(null!=sourcesListJson)
					 {
						 if(!sourcesListJson.contains(b)) {
								sourcesList.add(b);
							}
					 }
					
			   
				 }
		    }
		    else {
				 provider.setSources(sourcesList);
		    }
         
         provider.setSources(dataProviderEdit.getSources());*/
    	 
    	 //geographical	   
       Set<LookupCode> geolist= new HashSet<LookupCode>();  
       List<String>geoGraphical = new ArrayList<String>();
       if(null!=dataProvider.getGeographicalFoci()) {
       geoGraphical=dataProvider.getGeographicalFoci().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
       }
       String editgeoJson = new Gson().toJson(dataProviderEdit.getGeographicalFoci());
	    ArrayList<BasicBean> editgeoListJson = mapper.readValue(editgeoJson,
	            new TypeReference<ArrayList<BasicBean>>(){});
	    if(null!=editgeoListJson) {
	    	  List<String> geoValues= editgeoListJson.stream().map(l->l.getDesc()).collect(Collectors.toList());
	   	   if(!geoGraphical.equals(geoValues)) {
	   		 for(BasicBean b:editgeoListJson)
	    	 {
	    	   LookupCode lookupCode=new LookupCode();
	    		 if(null!=geoGraphical)
	    		 {
	    				
	    					lookupCode.setId(Long.valueOf(b.getId()));
	    					lookupCode.setLookupcode(b.getDesc());
	    	    			geolist.add(lookupCode);
	    	    		
	    		 }
	   	   }
		        provider.setGeographicalFoci(geolist);
	    }
	   	   else {
	   		provider.setGeographicalFoci(geolist);
	   	   }
	    }
	   
	  /*  if(null!=editgeoListJson) {
       for(BasicBean b:editgeoListJson)
    	 {
    	   LookupCode lookupCode=new LookupCode();
    		 if(null!=geoGraphical)
    		 {
    				if(!geoGraphical.contains(b.getDesc()))
    	    		{
    					lookupCode.setId(Long.valueOf(b.getId()));
    					lookupCode.setLookupcode(b.getDesc());
    	    			geolist.add(lookupCode);
    	    		}
    		 }
    		 else {
    			 lookupCode.setId(Long.valueOf(b.getId()));
					lookupCode.setLookupcode(b.getDesc());
    			 geolist.add(lookupCode);
    		 }
    	   lookupCode.setId(Long.valueOf(b.getId()));
			lookupCode.setLookupcode(b.getDesc());
		 geolist.add(lookupCode);
    	 }
       provider.setGeographicalFoci(geolist);
	    }else {
	        provider.setGeographicalFoci(geolist);

	    }*/
    	 
    	//tags
		 String tagsJson = new Gson().toJson(dataProvider.getProviderTags());
		   /* ArrayList<BasicBean> tagsListJson = mapper.readValue(tagsJson,
		            new TypeReference<ArrayList<BasicBean>>(){});*/
		    
		    //ArrayList<BasicBean> tagsList= new ArrayList<BasicBean>();   
		    
		    String edittagJson = new Gson().toJson(dataProviderEdit.getProviderTags());
		   /* ArrayList<BasicBean> edittagListJson = mapper.readValue(edittagJson,
		            new TypeReference<ArrayList<BasicBean>>(){});*/
		    
		    if(!Objects.equals(tagsJson, edittagJson))
		    {
		    	//System.out.println("not ewqual");
		    	if(null!=dataProviderEdit.getProviderTags()) {
			         provider.setProviderTags(dataProviderEdit.getProviderTags());
		    	}

		    }
		    
		   /* if(null!=edittagListJson) {
			 for(BasicBean b:edittagListJson)
			 {
				 if(null!=tagsListJson)
				 {
					 if(!tagsListJson.contains(b)) {
							tagsList.add(b);
						} 
				 }
				 else {
						tagsList.add(b);
				 }
		   
			 }
		    }else {
				 provider.setProviderTags(tagsList);
		    }
	    provider.setProviderTags(dataProviderEdit.getProviderTags());*/
		    
//target organization type
    	 Set<LookupCode> targetOrganizationlist= new HashSet<LookupCode>(); 
    	 List<String>targetOrganization=new ArrayList<String>();
    	 if(null!=dataProvider.getOrganizationType()) {
             targetOrganization=dataProvider.getOrganizationType().stream().map(l->l.getLookupcode()).collect(Collectors.toList()); 
    	 }
         String editOrganizationTypeJson = new Gson().toJson(dataProviderEdit.getOrganizationType());
		    ArrayList<BasicBean> editOrganizationTypeListJson = mapper.readValue(editOrganizationTypeJson,
		            new TypeReference<ArrayList<BasicBean>>(){});
		    
		    if(null!=editOrganizationTypeListJson) {
		    	  List<String> orgValues= editOrganizationTypeListJson.stream().map(l->l.getDesc()).collect(Collectors.toList());
		   	   if(!targetOrganization.equals(orgValues)) {
		   		 for(BasicBean b:editOrganizationTypeListJson)
		    	 {
		    	   LookupCode lookupCode=new LookupCode();
		    			
		    					lookupCode.setId(Long.valueOf(b.getId()));
		    					lookupCode.setLookupcode(b.getDesc());
		    					targetOrganizationlist.add(lookupCode);
		    	    		
		    		 
		   	   }
	             provider.setOrganizationType(targetOrganizationlist);
		    }
		    }
		    
		    
         /*if(null!=editOrganizationTypeListJson) {
        	 for(BasicBean b:editOrganizationTypeListJson)
          	 {
            	 LookupCode lookupCode=new LookupCode();
          		 if(null!=targetOrganization)
          		 {
          				if(!targetOrganization.contains(b.getDesc()))
          	    		{
          					lookupCode.setId(Long.valueOf(b.getId()));
          					lookupCode.setLookupcode(b.getDesc());
          					targetOrganizationlist.add(lookupCode);
          	    		}
          		 }
          		 else {
          			lookupCode.setId(Long.valueOf(b.getId()));
    					lookupCode.setLookupcode(b.getDesc());
          			targetOrganizationlist.add(lookupCode);
          		 }
            	 lookupCode.setId(Long.valueOf(b.getId()));
					lookupCode.setLookupcode(b.getDesc());
   			targetOrganizationlist.add(lookupCode);
          	 }
        	 provider.setOrganizationType(targetOrganizationlist);

         }
         else {
             provider.setOrganizationType(targetOrganizationlist);

         }
      	*/
      //managerType
    	 Set<LookupCode> managerTypeList= new HashSet<LookupCode>();  
    	 List<String>managerType = new ArrayList<String>();
    	 if(null!=dataProvider.getManagerType()) {
    	      managerType=dataProvider.getManagerType().stream().map(l->l.getLookupcode()).collect(Collectors.toList()); 
    	 }
       String editManagerTypeJson = new Gson().toJson(dataProviderEdit.getManagerType());
	    ArrayList<BasicBean> editManagerTypeListJson = mapper.readValue(editManagerTypeJson,
	            new TypeReference<ArrayList<BasicBean>>(){});
       if(null!=editManagerTypeListJson) {
    	   List<String> editManagerType= editManagerTypeListJson.stream().map(l->l.getDesc()).collect(Collectors.toList());
    	   //System.out.println("manager"+editManagerType.size());
    	   if(!managerType.equals(editManagerType)) { 
    		  // System.out.println("mancalled");
    	   for(BasicBean b:editManagerTypeListJson)
      	 {
      	   LookupCode lookupCode=new LookupCode();
      	 lookupCode.setId(Long.valueOf(b.getId()));
			lookupCode.setLookupcode(b.getDesc());
		managerTypeList.add(lookupCode);
      	 }
	       provider.setManagerType(managerTypeList);
       }
       }
       else {
    	   provider.setManagerType(managerTypeList);
       }
    	
    	 //investor Type
   	 Set<LookupCode> investorTypeList= new HashSet<LookupCode>(); 
   	List<String>investorType=new ArrayList<String>();
   	if(null!=dataProvider.getInvestorTypes()) {
        investorType=dataProvider.getInvestorTypes().stream().map(l->l.getLookupcode()).collect(Collectors.toList());

   	}
   	
      String editInvestorTypeJson = new Gson().toJson(dataProviderEdit.getInvestorTypes());
	    ArrayList<BasicBean> editInvestorTypeListJson = mapper.readValue(editInvestorTypeJson,
	            new TypeReference<ArrayList<BasicBean>>(){});
      if(null!=editInvestorTypeListJson) {
    	  List<String> editInvestorType= editInvestorTypeListJson.stream().map(l->l.getDesc()).collect(Collectors.toList());
	   	   if(!investorType.equals(editInvestorType)) {
    	  for(BasicBean b:editInvestorTypeListJson)
    	   	 {
    	    	  LookupCode lookupCode=new LookupCode();
    	    	  lookupCode.setId(Long.valueOf(b.getId()));
					lookupCode.setLookupcode(b.getDesc());
	   			 investorTypeList.add(lookupCode);
    	   	 }
			 provider.setInvestorTypes(investorTypeList);
      }
      }
   	
    //target strategies	 
 	 Set<LookupCode> targetStrategieslist= new HashSet<LookupCode>();
 	List<String>targetStrategies=new ArrayList<String>();
 	if(null!=dataProvider.getStrategies()) {
 	    targetStrategies=dataProvider.getStrategies().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
 	}
    String editStrategiesTypeJson = new Gson().toJson(dataProviderEdit.getStrategies());
    ArrayList<BasicBean> editStrategiesTypeListJson = mapper.readValue(editStrategiesTypeJson,
            new TypeReference<ArrayList<BasicBean>>(){});
    if(null!=editStrategiesTypeListJson) {
    	  List<String> editStrategiesType= editStrategiesTypeListJson.stream().map(l->l.getDesc()).collect(Collectors.toList());
	   	   if(!targetStrategies.equals(editStrategiesType)) {
    	 for(BasicBean b:editStrategiesTypeListJson)
     	 {
        	LookupCode lookupCode=new LookupCode();
        	lookupCode.setId(Long.valueOf(b.getId()));
			lookupCode.setLookupcode(b.getDesc());
			targetStrategieslist.add(lookupCode);
     	 }
		 provider.setStrategies(targetStrategieslist);
    }
    }
  
 	
 	//target research styles
	 Set<LookupCode> targetResearchStylesList= new HashSet<LookupCode>();   
	 List<String>targetResearchStyles=new ArrayList<String>();
	 if(null!=dataProvider.getResearchStyles()) {
		    targetResearchStyles=dataProvider.getResearchStyles().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
	 }
    String editResearchStyleTypeJson = new Gson().toJson(dataProviderEdit.getResearchStyles());
    ArrayList<BasicBean> editResearchStyleTypeListJson = mapper.readValue(editResearchStyleTypeJson,
            new TypeReference<ArrayList<BasicBean>>(){});
    if(null!=editResearchStyleTypeListJson) {
    	  List<String> editResearchStyleType= editResearchStyleTypeListJson.stream().map(l->l.getDesc()).collect(Collectors.toList());
	   	   if(!targetResearchStyles.equals(editResearchStyleType)) {
    	for(BasicBean b:editResearchStyleTypeListJson)
    	 {
       	LookupCode lookupCode=new LookupCode();
       	lookupCode.setId(Long.valueOf(b.getId()));
			lookupCode.setLookupcode(b.getDesc());
		targetResearchStylesList.add(lookupCode);
    	 }
        provider.setResearchStyles(targetResearchStylesList);
    }
    }
    	
//targetInvestmenttimeframe
    	 Set<LookupCode> targetInvestmentlist= new HashSet<LookupCode>();  
    	 List<String>targetInvestment=new ArrayList<String>();
    	 if(null!=dataProvider.getInvestingTimeFrame()) {
             targetInvestment=dataProvider.getInvestingTimeFrame().stream().map(l->l.getLookupcode()).collect(Collectors.toList()); 
    	 }
         String editInvestingTimeFrameJson = new Gson().toJson(dataProviderEdit.getInvestingTimeFrame());
         ArrayList<BasicBean> editInvestingTimeFrameListJson = mapper.readValue(editInvestingTimeFrameJson,
                 new TypeReference<ArrayList<BasicBean>>(){});
         
         if(null!=editInvestingTimeFrameListJson)
         {
        	 List<String> editInvestingTimeFrame= editInvestingTimeFrameListJson.stream().map(l->l.getDesc()).collect(Collectors.toList());
  	   	   if(!targetInvestment.equals(editInvestingTimeFrame)) {
        	 for(BasicBean b:editInvestingTimeFrameListJson)
          	 {
        		 LookupCode lookupCode=new LookupCode();
        		 lookupCode.setId(Long.valueOf(b.getId()));
					lookupCode.setLookupcode(b.getDesc());
       			targetInvestmentlist.add(lookupCode);
          	 }
             provider.setInvestingTimeFrame(targetInvestmentlist);

         }
         }       	
      //target public equities marketcap
     	 Set<LookupCode> targetPublicEquitiesMarketCapList= new HashSet<LookupCode>();   
     	List<String>targetPublicEquitiesMarketCap=new ArrayList<String>();
     	if(null!=dataProvider.getEquitiesMarketCap())
     	{
     		targetPublicEquitiesMarketCap=dataProvider.getEquitiesMarketCap().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
     	}
        String editEquitiesMarketCapJson = new Gson().toJson(dataProviderEdit.getEquitiesMarketCap());
        ArrayList<BasicBean> editEquitiesMarketCapListJson = mapper.readValue(editEquitiesMarketCapJson,
                new TypeReference<ArrayList<BasicBean>>(){});
        if(null!=editEquitiesMarketCapListJson)
        {
        	 List<String> editEquitiesMarket= editEquitiesMarketCapListJson.stream().map(l->l.getDesc()).collect(Collectors.toList());
    	   	   if(!targetPublicEquitiesMarketCap.equals(editEquitiesMarket)) {
        	 for(BasicBean b:editEquitiesMarketCapListJson)
         	 {
        		 LookupCode lookupCode=new LookupCode();
        		 lookupCode.setId(Long.valueOf(b.getId()));
					lookupCode.setLookupcode(b.getDesc());
      			targetPublicEquitiesMarketCapList.add(lookupCode);
         	 }	
             provider.setEquitiesMarketCap(targetPublicEquitiesMarketCapList);

        }
        }
     	//target public equities style
     	 Set<LookupCode> targetPublicEquitiesStylesList= new HashSet<LookupCode>(); 
         List<String>targetPublicEquitiesStyles=new ArrayList<String>();
         if(null!=dataProvider.getEquitiesStyle()) {
             targetPublicEquitiesStyles=dataProvider.getEquitiesStyle().stream().map(l->l.getLookupcode()).collect(Collectors.toList());

         }
        String editEquitiesStyleJson = new Gson().toJson(dataProviderEdit.getEquitiesStyle());
        ArrayList<BasicBean> editEquitiesStyleListJson = mapper.readValue(editEquitiesStyleJson,
                new TypeReference<ArrayList<BasicBean>>(){});
        if(null!=editEquitiesStyleListJson)
        {
        	 List<String> editEquitiesStyle= editEquitiesStyleListJson.stream().map(l->l.getDesc()).collect(Collectors.toList());
  	   	   if(!targetPublicEquitiesStyles.equals(editEquitiesStyle)) {
        	for(BasicBean b:editEquitiesStyleListJson)
        	 {
        		LookupCode lookupCode=new LookupCode();
        		lookupCode.setId(Long.valueOf(b.getId()));
					lookupCode.setLookupcode(b.getDesc());
    			targetPublicEquitiesStylesList.add(lookupCode);
        	 }
            provider.setEquitiesStyle(targetPublicEquitiesStylesList);

        }
        }
        else {
            provider.setEquitiesStyle(targetPublicEquitiesStylesList);

        }
     	
     	//assetClass
		 String assetClassJson = new Gson().toJson(dataProvider.getAssetClasses());
		 ArrayList<BasicBean> assetClassJsonListJson= new ArrayList<BasicBean>();
		   assetClassJsonListJson = mapper.readValue(assetClassJson,
		            new TypeReference<ArrayList<BasicBean>>(){});
		    String editAssetClassJson = new Gson().toJson(dataProviderEdit.getAssetClasses());
		    ArrayList<BasicBean> editAssetClassListJson=new ArrayList<BasicBean>();
	        editAssetClassListJson = mapper.readValue(editAssetClassJson,
	                new TypeReference<ArrayList<BasicBean>>(){});
		    //ArrayList<BasicBean> assetClassList= new ArrayList<BasicBean>(); 
	        if(null!=assetClassJsonListJson) { 
		 if(!assetClassJsonListJson.equals(editAssetClassListJson))
	    {
	    	//System.out.println("not ewqual");
	    	if(null!=dataProviderEdit.getAssetClasses()) {
		         provider.setAssetClasses(dataProviderEdit.getAssetClasses());
	    	}

	    }
	        }
	        else {
	        	if(null!=dataProviderEdit.getAssetClasses()) {
			         provider.setAssetClasses(dataProviderEdit.getAssetClasses());
		    	}
	        }
	      /*  if(!Objects.equals(assetClassJson, editAssetClassJson))
		    {
		    	//System.out.println("not ewqual");
	        	if(null!=dataProviderEdit.getAssetClasses()) {
			         provider.setAssetClasses(dataProviderEdit.getAssetClasses());
		    	}

		    }
	        */
	        	
			 
			//securityTypes
			String securityTypeJson = new Gson().toJson(dataProvider.getSecurityTypes());
			 ArrayList<BasicBean> securityTypeListJson=new  ArrayList<BasicBean>();
			    securityTypeListJson = mapper.readValue(securityTypeJson,
			            new TypeReference<ArrayList<BasicBean>>(){});
			    
			    String editSecurityTypeJson = new Gson().toJson(dataProviderEdit.getSecurityTypes());
			 ArrayList<BasicBean> editSecurityTypeListJson = mapper.readValue(editSecurityTypeJson,
			            new TypeReference<ArrayList<BasicBean>>(){});
			   // ArrayList<BasicBean> securityTypeList= new ArrayList<BasicBean>(); 
                  if(null!=securityTypeListJson) {
                	  if(!securityTypeListJson.equals(editSecurityTypeListJson))
          		    {
          		    	//System.out.println("not ewqual");
          			    	 if(null!=dataProviderEdit.getSecurityTypes()) {
          						 provider.setSecurityTypes(dataProviderEdit.getSecurityTypes());
          					 }

          		    }
                  }
                  else {
                	  if(null!=dataProviderEdit.getSecurityTypes()) {
   						 provider.setSecurityTypes(dataProviderEdit.getSecurityTypes());
   					 }
                  }
				 
			/* if(!Objects.equals(securityTypeJson, editSecurityTypeJson))
			    {
			    	//System.out.println("not ewqual");
				 if(null!=dataProviderEdit.getSecurityTypes()) {
					 provider.setSecurityTypes(dataProviderEdit.getSecurityTypes());
				 }

			    }*/
			 
			 
				//sectors
				String sectorsJson = new Gson().toJson(dataProvider.getRelevantSectors());
				  ArrayList<BasicBean> sectorsListJson=new  ArrayList<BasicBean>();
				    sectorsListJson = mapper.readValue(sectorsJson,
				            new TypeReference<ArrayList<BasicBean>>(){});
				    
				    String editSectorsJson = new Gson().toJson(dataProviderEdit.getRelevantSectors());
				    ArrayList<BasicBean> editSectorsListJson = mapper.readValue(editSectorsJson,
				            new TypeReference<ArrayList<BasicBean>>(){});
				    
				    //ArrayList<BasicBean> sectorsList= new ArrayList<BasicBean>(); 
				    //System.out.println("sectorsListJson"+sectorsListJson);
				    if(null!=sectorsListJson) {
                       if(!sectorsListJson.equals(editSectorsListJson))
			    {
			    	//System.out.println("not ewqual");
				    	 if(null!=dataProviderEdit.getRelevantSectors()) {
							 provider.setRelevantSectors(dataProviderEdit.getRelevantSectors());
						 }

			    }
				    }
				    else {
				    	 if(null!=dataProviderEdit.getRelevantSectors()) {
							 provider.setRelevantSectors(dataProviderEdit.getRelevantSectors());
						 }
				    }
				   /* if(!Objects.equals(sectorsJson, editSectorsJson))
				    {
				    	//System.out.println("not ewqual");
				    	 if(null!=dataProviderEdit.getRelevantSectors()) {
							 provider.setRelevantSectors(dataProviderEdit.getRelevantSectors());
						 }

				    }*/
					
					 //identifiers Available
				   	 Set<LookupCode> identifiersAvailableList= new HashSet<LookupCode>(); 
				   	List<String>identifiersAvailable=new ArrayList<String>();
				   	if(null!=dataProvider.getidentifiersAvailable()) {
					      identifiersAvailable=dataProvider.getidentifiersAvailable().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
				   	}
				   	
				      String editIdentifiersAvailableJson = new Gson().toJson(dataProviderEdit.getIdentifiersAvailable());
					    ArrayList<BasicBean> editIdentifiersAvailableListJson = mapper.readValue(editIdentifiersAvailableJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
				      if(null!=editIdentifiersAvailableListJson) {
				    	  List<String> editIdentifiersAvailable= editIdentifiersAvailableListJson.stream().map(l->l.getDesc()).collect(Collectors.toList());
			    	   	  Collections.sort(identifiersAvailable);
			    	   	  Collections.sort(editIdentifiersAvailable);
				    	 /* System.out.println("identifiersAvailable"+identifiersAvailable);
			    	   	  System.out.println("editIdentifiersAvailable"+editIdentifiersAvailable);*/
				    	  if(!identifiersAvailable.equals(editIdentifiersAvailable)) {
				    	  for(BasicBean b:editIdentifiersAvailableListJson)
						   	 {
				    		  LookupCode lookupCode=new LookupCode();
				    		  lookupCode.setId(Long.valueOf(b.getId()));
			   					lookupCode.setLookupcode(b.getDesc());
					   			identifiersAvailableList.add(lookupCode);
						   	 }
				    	 // System.out.println("ca");
					      provider.setIdentifiersAvailable(identifiersAvailableList);
				      }
				      }
				      else {
				    	  //System.out.println("ca");
					      provider.setIdentifiersAvailable(identifiersAvailableList);

				      }
				   	
				    //Access Offered
				  	 Set<LookupCode> accessOfferedList= new HashSet<LookupCode>();
				  	List<String>accessOffered=new ArrayList<String>();
				  	if(null!=dataProvider.getAccessOffered()) {
					     accessOffered=dataProvider.getAccessOffered().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
				  	}
				  	
				     String editAccessOfferedJson = new Gson().toJson(dataProviderEdit.getAccessOffered());
					    ArrayList<BasicBean> editAccessOfferedJsonList = mapper.readValue(editAccessOfferedJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
				     if(null!=editAccessOfferedJsonList) {
				    	 List<String> editAccessOffered= editAccessOfferedJsonList.stream().map(l->l.getDesc()).collect(Collectors.toList());
				    	  Collections.sort(accessOffered);
			    	   	  Collections.sort(editAccessOffered); 
				    	 if(!accessOffered.equals(editAccessOffered)) {
				    	 for(BasicBean b:editAccessOfferedJsonList)
					  	 {
				    		 LookupCode lookupCode=new LookupCode();
					  		
				    		 lookupCode.setId(Long.valueOf(b.getId()));
			  					lookupCode.setLookupcode(b.getDesc());
					  			accessOfferedList.add(lookupCode);
					  	 }
					     provider.setAccessOffered(accessOfferedList);

				     }
				     }
				     else {
					     provider.setAccessOffered(accessOfferedList);

				     }
				  	
				  //producttypes
				  	 Set<LookupCode> productTypesList= new HashSet<LookupCode>();
				  	List<String>productTypes=new ArrayList<String>();
				  	if(null!=dataProvider.getDataProductTypes()) {
					     productTypes=dataProvider.getDataProductTypes().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
				  	}
				  	
				 	
				     String editDataProductTypesJson = new Gson().toJson(dataProviderEdit.getDataProductTypes());
					    ArrayList<BasicBean> editDataProductTypesJsonList = mapper.readValue(editDataProductTypesJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
				     
				     if(null!=editDataProductTypesJsonList) {
				    	 List<String> editDataProductTypes= editDataProductTypesJsonList.stream().map(l->l.getDesc()).collect(Collectors.toList());
			    	   	   if(!productTypes.equals(editDataProductTypes)) {
				    	 for(BasicBean b:editDataProductTypesJsonList)
					  	 {
				    		 LookupCode lookupCode=new LookupCode();
				    		 lookupCode.setId(Long.valueOf(b.getId()));
			  					lookupCode.setLookupcode(b.getDesc());
					  			productTypesList.add(lookupCode);
					  	 }
				    	 provider.setDataProductTypes(productTypesList);
				     }
				     }
				     else {
					     provider.setDataProductTypes(productTypesList);

				     }
				  	
				  	 //types
				  	 Set<LookupCode> typesList= new HashSet<LookupCode>();
				  	List<String>types=new ArrayList<String>();
				  	if(null!=types) {
					     types=dataProvider.getDataTypes().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
				  	}
				  	
				     String editDataTypesJson = new Gson().toJson(dataProviderEdit.getDataTypes());
					    ArrayList<BasicBean> editDataTypesJsonList = mapper.readValue(editDataTypesJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
				     if(null!=editDataTypesJsonList) {
				    	 List<String>  editDataTypes= editDataTypesJsonList.stream().map(l->l.getDesc()).collect(Collectors.toList());
			    	   	   if(!types.equals(editDataTypes)) {
				    	 for(BasicBean b:editDataTypesJsonList)
					  	 {
				    		 LookupCode lookupCode=new LookupCode();
				    		 lookupCode.setId(Long.valueOf(b.getId()));
			  					lookupCode.setLookupcode(b.getDesc());
					  			typesList.add(lookupCode);
					  	 }
				    	   provider.setDataTypes(typesList);
				     }
				     }
				     else {
					     provider.setDataTypes(typesList);
 
				     }
				  	
				  	
					//Language Available
			      	 Set<LookupCode> languageList= new HashSet<LookupCode>();
			      	 List<String>language=new ArrayList<String>();
			      	 if(null!=dataProvider.getLanguagesAvailable()) {
				         language=dataProvider.getLanguagesAvailable().stream().map(l->l.getLookupcode()).collect(Collectors.toList()); 
			      	 }
			         String editLanguageJson = new Gson().toJson(dataProviderEdit.getLanguagesAvailable());
					    ArrayList<BasicBean> editLanguageJsonList = mapper.readValue(editLanguageJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
			         if(null!=editLanguageJsonList) {
			        	 List<String> editLanguage= editLanguageJsonList.stream().map(l->l.getDesc()).collect(Collectors.toList());
			    	   	   if(!language.equals(editLanguage)) {
			        	 for(BasicBean b:editLanguageJsonList)
				      	 {
			        		 LookupCode lookupCode=new LookupCode();
			        		 lookupCode.setId(Long.valueOf(b.getId()));
		      					lookupCode.setLookupcode(b.getDesc());
		      					languageList.add(lookupCode);
				      	 } 
				         provider.setLanguagesAvailable(languageList);

			         }
			         }
			         else {
				         provider.setLanguagesAvailable(languageList);

			         }
			      	
			      //deliveryMethod
					 String deliveryMethodJson = new Gson().toJson(dataProvider.getDeliveryMethods());
					 ArrayList<BasicBean> deliveryMethodListJson =new ArrayList<BasicBean>();
					     deliveryMethodListJson = mapper.readValue(deliveryMethodJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
					   String editDeliveryMethodJson = new Gson().toJson(dataProviderEdit.getDeliveryMethods());
					   ArrayList<BasicBean> editDeliveryMethodListJson = mapper.readValue(editDeliveryMethodJson,
					            new TypeReference<ArrayList<BasicBean>>(){});
					    
					   // ArrayList<BasicBean> deliveryMethodTypeList= new ArrayList<BasicBean>();   
					   if(null!=deliveryMethodListJson) {
						   if(!deliveryMethodListJson.equals(editDeliveryMethodListJson))
						    {
						    	//System.out.println("not ewqual");
						    	if(null!=dataProviderEdit.getDeliveryMethods()) {
							         provider.setDeliveryMethods(dataProviderEdit.getDeliveryMethods());
						    	}

						    }
					   }
					   else {
						   if(null!=dataProviderEdit.getDeliveryMethods()) {
						         provider.setDeliveryMethods(dataProviderEdit.getDeliveryMethods());
					    	}
					   }
			       
					 /*  if(!Objects.equals(deliveryMethodJson, editDeliveryMethodJson))
					    {
					    	//System.out.println("not ewqual");
						   if(null!=dataProviderEdit.getDeliveryMethods()) {
						         provider.setDeliveryMethods(dataProviderEdit.getDeliveryMethods());
					    	}

					    }*/
					   
						 
						//deliveryFormats
						String deliveryFormatsJson = new Gson().toJson(dataProvider.getDeliveryFormats());
						 ArrayList<BasicBean> deliveryFormatsListJson = new ArrayList<BasicBean>();
						    deliveryFormatsListJson = mapper.readValue(deliveryFormatsJson,
						            new TypeReference<ArrayList<BasicBean>>(){});
						    
						    String editDeliveryFormatsJson = new Gson().toJson(dataProviderEdit.getDeliveryFormats());
						    ArrayList<BasicBean> deliveryFormatsEditListJson = mapper.readValue(editDeliveryFormatsJson,
						            new TypeReference<ArrayList<BasicBean>>(){});
						    
						    if(null!=deliveryFormatsListJson) {
						    	  if(!deliveryFormatsListJson.equals(deliveryFormatsEditListJson))
								    {
								    	//System.out.println("not ewqual");
								    	 if(null!=dataProviderEdit.getDeliveryFormats()) {
											    provider.setDeliveryFormats(dataProviderEdit.getDeliveryFormats());

										    }
												 

								    }
						    }
						    else {
						    	if(null!=dataProviderEdit.getDeliveryFormats()) {
								    provider.setDeliveryFormats(dataProviderEdit.getDeliveryFormats());

							    }
						    }
						    /*if(!Objects.equals(deliveryFormatsJson, editDeliveryFormatsJson))
						    {
						    	//System.out.println("not ewqual");
						    	 if(null!=dataProviderEdit.getDeliveryFormats()) {
									    provider.setDeliveryFormats(dataProviderEdit.getDeliveryFormats());

								    }

						    }*/
						 
//delivery Frequency
        Set<LookupCode> deliveryFrequencyList= new HashSet<LookupCode>();
        List<String>deliveryFrequency=new ArrayList<String>();
        if(null!=dataProvider.getDeliveryFrequencies()) {
        deliveryFrequency=dataProvider.getDeliveryFrequencies().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
        }
        String editDeliveryFrequencyJson = new Gson().toJson(dataProviderEdit.getDeliveryFrequencies());
	    ArrayList<BasicBean> editDeliveryFrequencyJsonList = mapper.readValue(editDeliveryFrequencyJson,
	            new TypeReference<ArrayList<BasicBean>>(){});
	    
        if(null!=editDeliveryFrequencyJsonList) {
        	List<String> editDeliveryFrequency= editDeliveryFrequencyJsonList.stream().map(l->l.getDesc()).collect(Collectors.toList());
        	
        	if(!deliveryFrequency.equals(editDeliveryFrequency)) {
        		for(BasicBean b:editDeliveryFrequencyJsonList)
        	 {
        		LookupCode lookupCode=new LookupCode();
        		lookupCode.setId(Long.valueOf(b.getId()));
				lookupCode.setLookupcode(b.getDesc());
				deliveryFrequencyList.add(lookupCode);
        	 }
            provider.setDeliveryFrequencies(deliveryFrequencyList);

        }
 	   	   }
 	   	   else {
            provider.setDeliveryFrequencies(deliveryFrequencyList);

        }
  
   	
      	 //paymentmethodsoffered
      	 Set<LookupCode> paymentMethodsOfferedList= new HashSet<LookupCode>(); 
      	List<String>paymentMethodsOffered=new ArrayList<String>();
      	if(null!=dataProvider.getPaymentMethodsOffered()) {
            paymentMethodsOffered=dataProvider.getPaymentMethodsOffered().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
      	}
      
         String editPaymentsMehtodOfferedJson = new Gson().toJson(dataProviderEdit.getPaymentMethodsOffered());
 	    ArrayList<BasicBean> editPaymentsMehtodOfferedJsonList = mapper.readValue(editPaymentsMehtodOfferedJson,
 	            new TypeReference<ArrayList<BasicBean>>(){});
 	    
         if(null!=editPaymentsMehtodOfferedJsonList) {
        	 List<String> editPaymentsMehtodOffered= editPaymentsMehtodOfferedJsonList.stream().map(l->l.getDesc()).collect(Collectors.toList());
  	   	   if(!paymentMethodsOffered.equals(editPaymentsMehtodOffered)) {
        	 for(BasicBean b:editPaymentsMehtodOfferedJsonList)
          	 {
        		 LookupCode lookupCode=new LookupCode();
        		 lookupCode.setId(Long.valueOf(b.getId()));
					lookupCode.setLookupcode(b.getDesc());
					paymentMethodsOfferedList.add(lookupCode);
          	 }
             provider.setPaymentMethodsOffered(paymentMethodsOfferedList);
         }
         }
         else {
             provider.setPaymentMethodsOffered(paymentMethodsOfferedList);
 
         }
         
      	 //datagapsreason
      	 Set<LookupCode> dataGapsReasonList= new HashSet<LookupCode>(); 
      	List<String>dataGapsReason=new ArrayList<String>();
      	if(null!=dataProvider.getDataGapsReasons()) {
            dataGapsReason=dataProvider.getDataGapsReasons().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
      	}
      	

         String editDataGapsJson = new Gson().toJson(dataProviderEdit.getDataGapsReasons());
 	    ArrayList<BasicBean> editDataGapsJsonList = mapper.readValue(editDataGapsJson,
 	            new TypeReference<ArrayList<BasicBean>>(){});
         
 	    if(null!=editDataGapsJsonList) {
 	    	 List<String> editDataGaps= editDataGapsJsonList.stream().map(l->l.getDesc()).collect(Collectors.toList());
  	   	   if(!dataGapsReason.equals(editDataGaps)) {
 	    	for(BasicBean b:editDataGapsJsonList)
 	      	 {
 	    		LookupCode lookupCode=new LookupCode();
 	    		lookupCode.setId(Long.valueOf(b.getId()));
					lookupCode.setLookupcode(b.getDesc());
	      			dataGapsReasonList.add(lookupCode);
 	      	 }
 	 	   provider.setDataGapsReasons(dataGapsReasonList);

 	    }
 	    }
 	    else {
 	 	   provider.setDataGapsReasons(dataGapsReasonList);

 	    }
      	//outlier Reason
      	 Set<LookupCode> outlierReasonList= new HashSet<LookupCode>();
      	List<String>outlierReason=new ArrayList<String>();
      	if(null!=dataProvider.getOutlierReasons()) {
            outlierReason=dataProvider.getOutlierReasons().stream().map(l->l.getLookupcode()).collect(Collectors.toList());
      	}
      	
         String editOutliersJson = new Gson().toJson(dataProviderEdit.getOutlierReasons());
  	    ArrayList<BasicBean> editOutliersJsonList = mapper.readValue(editOutliersJson,
  	            new TypeReference<ArrayList<BasicBean>>(){});
         
  	    if(null!=editOutliersJsonList) {
  	    	 List<String> editOutliers= editOutliersJsonList.stream().map(l->l.getDesc()).collect(Collectors.toList());
  	   	   if(!outlierReason.equals(editOutliers)) {
  	    	for(BasicBean b:editOutliersJsonList)
  	      	 {
  	    		LookupCode lookupCode=new LookupCode();
  	    		lookupCode.setId(Long.valueOf(b.getId()));
  				lookupCode.setLookupcode(b.getDesc());
    			outlierReasonList.add(lookupCode);
  	      	 }
  	  	  provider.setOutlierReasons(outlierReasonList);

  	    }
  	    }
  	    else {
  	  	  provider.setOutlierReasons(outlierReasonList);

  	    }
     	//Bias Reason
      	 Set<LookupCode> biasList= new HashSet<LookupCode>();     
      	List<String>bias=new ArrayList<String>();
      	if(null!=dataProvider.getBiases()) {
            bias=dataProvider.getBiases().stream().map(l->l.getLookupcode()).collect(Collectors.toList());

      	}
      	
         String editBiasesJson = new Gson().toJson(dataProviderEdit.getBiases());
   	    ArrayList<BasicBean> editBiasesJsonList = mapper.readValue(editBiasesJson,
   	            new TypeReference<ArrayList<BasicBean>>(){});
         if(null!=editBiasesJsonList) {
        	 List<String> editBiases= editBiasesJsonList.stream().map(l->l.getDesc()).collect(Collectors.toList());
  	   	   if(!bias.equals(editBiases)) {
        	 for(BasicBean b:editBiasesJsonList)
          	 {
        		 LookupCode lookupCode=new LookupCode();
        		 lookupCode.setId(Long.valueOf(b.getId()));
					lookupCode.setLookupcode(b.getDesc());
       			biasList.add(lookupCode);
          	 }
             provider.setBiases(biasList);

         }
         }
         else {
             provider.setBiases(biasList);

         }
      
      	 //pricingModel	
    	 String pricingModeljson = new Gson().toJson(dataProvider.getPricingModels());
        ArrayList<BasicBean> listFromPricingmodel= mapper.readValue(pricingModeljson,
               new TypeReference<ArrayList<BasicBean>>(){});
       
       String editPricingModeljson = new Gson().toJson(dataProviderEdit.getDataProvidersPricingModels());
       ArrayList<BasicBean> editListFromPricingmodel= mapper.readValue(editPricingModeljson,
               new TypeReference<ArrayList<BasicBean>>(){});
       
       if(null!=listFromPricingmodel) {
           if(!listFromPricingmodel.equals(editListFromPricingmodel))
    {
    	//System.out.println("not ewqual");
        	   if(null!=dataProviderEdit.getDataProvidersPricingModels()) {
            	   provider.setPricingModels(dataProviderEdit.getDataProvidersPricingModels());
               }

    }
	    }
	    else {
	    	  if(null!=dataProviderEdit.getDataProvidersPricingModels()) {
	        	   provider.setPricingModels(dataProviderEdit.getDataProvidersPricingModels());
	           }
	    }
       
      // Set<LookupCode> pricingModelList= new HashSet<LookupCode>();  
      /* if(!Objects.equals(pricingModeljson, editPricingModeljson))
	    {
	    	//System.out.println("not ewqual");
    	   if(null!=dataProviderEdit.getDataProvidersPricingModels()) {
        	   provider.setPricingModels(dataProviderEdit.getDataProvidersPricingModels());
           }

	    }*/
     
    //industries	
    	 String json = new Gson().toJson(dataProvider.getIndustries());
      ArrayList<BasicBean> listFromJackson = mapper.readValue(json,
               new TypeReference<ArrayList<BasicBean>>(){});
       
       String editjson = new Gson().toJson(dataProviderEdit.getIndustries());
       ArrayList<BasicBean> editListFromJackson = mapper.readValue(editjson,
               new TypeReference<ArrayList<BasicBean>>(){});
       
       if(null!=listFromJackson) {
    	   if(!listFromJackson.equals(editListFromJackson))
    	    {
    	    	//System.out.println("not ewqual");
    		   if(null!=dataProviderEdit.getIndustries()) {
                   provider.setIndustries(dataProviderEdit.getIndustries());

               }

    	    }
       }
       else {
    	   if(null!=dataProviderEdit.getIndustries()) {
               provider.setIndustries(dataProviderEdit.getIndustries());

           }
       }
       
      /* if(!Objects.equals(json, editjson))
	    {
	    	//System.out.println("not ewqual");
    	   if(null!=dataProviderEdit.getIndustries()) {
               provider.setIndustries(dataProviderEdit.getIndustries());

           }

	    }
     */
    	 
//categories		
   	String categoriesJson = new Gson().toJson(dataProvider.getCategories());
   ArrayList<BasicBean> categoriesListJson = mapper.readValue(categoriesJson,
            new TypeReference<ArrayList<BasicBean>>(){});
    
	String editCategoriesJson = new Gson().toJson(dataProviderEdit.getCategories());
    ArrayList<BasicBean> editCategoriesListJson = mapper.readValue(editCategoriesJson,
            new TypeReference<ArrayList<BasicBean>>(){});
    
    
   // ArrayList<BasicBean> categoriesList= new ArrayList<BasicBean>();   
    if(null!=categoriesListJson) {
    	 if(!categoriesListJson.equals(editCategoriesListJson))
 	    {
 	    	//System.out.println("not ewqual");
    		 if(null!=dataProviderEdit.getCategories()) {
    	 		    provider.setCategories(dataProviderEdit.getCategories());

    	 	 }

 	    }
    
    }
    else {
    
    	if(null!=dataProviderEdit.getCategories()) {
 		    provider.setCategories(dataProviderEdit.getCategories());

 	 }
    }
   /* if(null!=categoriesListJson)
    if(!Objects.equals(categoriesJson, editCategoriesJson))
    {
    	//System.out.println("not ewqual");
    	 if(null!=dataProviderEdit.getCategories()) {
 		    provider.setCategories(dataProviderEdit.getCategories());

 	 }*/

    
	
	//features		
	   	String featuresJson = new Gson().toJson(dataProvider.getFeatures());
	  /*  ArrayList<BasicBean> featuresListJson = mapper.readValue(featuresJson,
	            new TypeReference<ArrayList<BasicBean>>(){});
	    */
		String editFeaturesJson = new Gson().toJson(dataProviderEdit.getFeatures());
	  /*  ArrayList<BasicBean> editFeaturesListJson = mapper.readValue(editFeaturesJson,
	            new TypeReference<ArrayList<BasicBean>>(){});
	    
	    ArrayList<BasicBean> featuresList= new ArrayList<BasicBean>();  */
	    if(!Objects.equals(featuresJson, editFeaturesJson))
	    {
	    	//System.out.println("not ewqual");
	    	 if(null!=dataProviderEdit.getFeatures()) {
				    provider.setFeatures(dataProviderEdit.getFeatures());
			 }

	    }
		
        
		return provider;
	}
	

}
