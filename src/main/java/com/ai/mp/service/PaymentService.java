package com.ai.mp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ai.mp.config.ApplicationProperties;
import com.ai.mp.domain.Products;
import com.ai.mp.domain.UserCheckout;
import com.ai.mp.repository.ProductRepository;
import com.stripe.exception.StripeException;
import com.stripe.model.Plan;
import com.stripe.model.Product;
import com.stripe.model.Subscription;

@Service
@Transactional
public class PaymentService {

	private final ProductRepository productRepository;
    private final ApplicationProperties applicationProperties;


	public PaymentService(ProductRepository productRepository,ApplicationProperties applicationProperties)
	{
		this.productRepository=productRepository;
		this.applicationProperties = applicationProperties;
	}

	public String addProduct(UserCheckout checkOut,String customerId) throws StripeException {
		// TODO Auto-generated method stub
		Products addProduct=productRepository.findByProductNameEquals(checkOut.getSubscription());
		List<Object> items = new ArrayList<>();
		Map<String, Object> item1 = new HashMap<>();
		String status=null;
		if(addProduct!=null)
		{

			item1.put("plan",applicationProperties.getPlanKey());
			items.add(item1);
			Map<String, Object> paramsItem = new HashMap<>();
			paramsItem.put("customer",customerId);
			paramsItem.put("items", items);
			Subscription subscription =
			  Subscription.create(paramsItem);
			//System.out.println("subscription"+subscription.getStatus());
			status=subscription.getStatus();
		}
		else {
		Map<String, Object> params = new HashMap<>();
    	params.put("name", checkOut.getSubscription());
    	Product product = Product.create(params);
    //	System.out.println("product"+product);
    	Products products = new Products();
    	products.setProductId(product.getId());
    	products.setProductName(product.getName());

    	Map<String, Object> productss = new HashMap<>();
    	productss.put("name",product.getName());
    	Map<String, Object> paramss = new HashMap<>();
    	paramss.put("amount", checkOut.getAmount());
    	paramss.put("currency", "USD");
    	if(checkOut.getSubscription().equalsIgnoreCase("Silver"))
        {    	paramss.put("interval", "year");
        }
    	else
    	{
             	paramss.put("interval", "month");

    	}

    	paramss.put("product", productss);
    	Plan plan = Plan.create(paramss);
    	products.setPlanId(plan.getId());
    	item1.put("plan",plan.getId());
		items.add(item1);
		Map<String, Object> paramsItem = new HashMap<>();
		paramsItem.put("customer",customerId );
		paramsItem.put("items", items);
		Subscription subscription =
		  Subscription.create(paramsItem);
		//products.setSubscriptionId(subscription.getId());
		productRepository.save(products);
		status=subscription.getStatus();
		}
		return status;

	/*	item1.put("plan",plan.get);
		items.add(item1);*/


	}

}
