package com.ai.mp.service;

import com.ai.core.StrUtils;
import com.ai.mp.config.ApplicationProperties;
import com.ai.mp.config.Constants;
import com.ai.mp.domain.*;
import com.ai.mp.repository.*;
import com.ai.mp.repository.search.UserSearchRepository;
import com.ai.mp.security.AuthoritiesConstants;
import com.ai.mp.security.SecurityUtils;
import com.ai.mp.service.dto.UserDTO;
import com.ai.mp.utils.RandomUtil;
import com.ai.util.Utils;
import com.codahale.metrics.annotation.Timed;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.resource.Contact;
import com.mailjet.client.resource.ContactManagemanycontacts;
import com.mailjet.client.resource.Contactdata;
import com.mailjet.client.resource.ContactslistManageManyContacts;
import io.github.jhipster.config.JHipsterProperties;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.Null;


import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.ai.core.Constants.*;
import static com.ai.mp.security.AuthoritiesConstants.USER;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final SocialService socialService;

    private final UserProviderRepository userProviderRepository;

    private final UserSearchRepository userSearchRepository;

    private final UserRequestRepository userRequestRepository;

    private final PersistentTokenRepository persistentTokenRepository;

    private final AuthorityRepository authorityRepository;
    private final DataCategoryRepository categoryRepository;
    private final DataProviderRepository dataProviderRepository;

    private final MailService mailService;
    private final MailJetService mailJetService;

    private final Environment environment;

    private final JHipsterProperties jHipsterProperties;
    private final ApplicationProperties applicationProperties;
    private final EmailAddressRepository emailAddressRepository;

    private final LookupCodeRepository lookupCodeRepository;
    
    private final EmailRepository emailRepository;
    
    private final DataDomainsRepository DatadomainsRepository;
    
    private final DomainService domainService;
    
    

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Timed
    public List<Long> getAllCategoryIDsForCurrentUser() {
        return categoryRepository.findByCategoryUsersEqualsOrUserPermissionIn(getUser(), getLoginUserRoles()).stream().map(DataCategory::getId).collect(Collectors.toList());
    }

    public List<Long> getOverrideCategoryIDsForCurrentUser() {
        return categoryRepository.findByCategoryUsersEquals(getUser()).stream().map(DataCategory::getId).collect(Collectors.toList());
    }

    public static boolean isUser()
    {
        if(getLoginUserRoles().contains(AuthoritiesConstants.USER))
            return true;
        return false;
    }


    public static boolean isAdminUser()
    {
        if(getLoginUserRoles().contains(AuthoritiesConstants.ADMIN))
            return true;
        return false;
    }
    public static List<String> getLoginUserRoles()
    {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Collection<? extends GrantedAuthority> a = authentication.getAuthorities();
        List<String> roles = a.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        return roles;
    }
    public   List<String> getLoginUserEffectiveRoles()
    {
        User user = getUserWithAuthorities();
        int maxRole = 0;
        Set<Authority> authorities = user.getAuthorities();
        for(Authority authority : authorities)
        {
            if(authority.getRoleLevel() > maxRole)
            {
                maxRole = authority.getRoleLevel();
            }
        }
        authorities = authorityRepository.findAllByRoleLevelLessThanEqual(maxRole);
        List<String> roles = authorities.stream().map(Authority::getName).collect(Collectors.toList());
        return roles;
    }
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, SocialService socialService, UserProviderRepository userProviderRepository, UserSearchRepository userSearchRepository, PersistentTokenRepository persistentTokenRepository, AuthorityRepository authorityRepository, DataCategoryRepository categoryRepository, DataProviderRepository dataProviderRepository, MailService mailService, Environment environment, ApplicationProperties applicationProperties, JHipsterProperties jHipsterProperties, UserRequestRepository userRequestRepository, MailJetService mailJetService,EmailAddressRepository emailAddressRepository,LookupCodeRepository lookupCodeRepository,
    		EmailRepository emailRepository,DataDomainsRepository dataDomainsRepository,DomainService domainService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.socialService = socialService;
        this.userProviderRepository = userProviderRepository;
        this.userSearchRepository = userSearchRepository;
        this.persistentTokenRepository = persistentTokenRepository;
        this.authorityRepository = authorityRepository;
        this.categoryRepository = categoryRepository;
        this.dataProviderRepository = dataProviderRepository;
        this.mailService = mailService;
        this.environment = environment;
        this.applicationProperties = applicationProperties;
        this.jHipsterProperties = jHipsterProperties;
        this.userRequestRepository = userRequestRepository;
        this.mailJetService = mailJetService;
        this.emailAddressRepository = emailAddressRepository;
        this.lookupCodeRepository = lookupCodeRepository;
        this.emailRepository=emailRepository;
        this.DatadomainsRepository=dataDomainsRepository;
        this.domainService=domainService;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                user.setEmailTourSub(true);
                userSearchRepository.save(user);
                mailService.sendActivationEmail(user);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> confirmRegistration(String key,String role) {
        log.debug("Confirming user for confirmation key {}", key);
        return userRepository.findOneByConfirmationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setConfirmed(true);
                user.setConfirmationKey(null);
                String loginOld=user.getLogin();
                if(!user.getLogin().equals(user.getEmail()))
                {
                    user.setLogin(user.getEmail());
                }
                userSearchRepository.save(user);
                if((user.getLogin().equals(user.getEmail()))&&(loginOld.equalsIgnoreCase(user.getEmail())))
                {
                    mailService.sendCreationEmail(user);
                }
                if((!user.getLastModifiedBy().equalsIgnoreCase(user.getEmail()))&&(!loginOld.equalsIgnoreCase(user.getEmail())))
                {
                    mailService.sendUpdationEmail(user);
                }
                Authority authority = user.getAuthorities().stream().filter(x -> (x.getName().contains("ROLE_PROVIDER")))
                    .findAny().orElse(null);
                if (authority!=null) {
                    user.setRole(" Data Provider");
                }else{
                    user.setRole(" Asset Manager");
                }
                EmailAddress email=emailAddressRepository.findByEmailAddress(user.getEmail());
                EmailAddress emailAddress=new EmailAddress();
                if(null!=email)
                {
                    email.setActive(true);
                    email.setUser(user.getId());
                    emailAddressRepository.save(email);
                    EmailAddress address=emailAddressRepository.findByEmailAddress(user.getLastModifiedBy());
                    if(address!=null)
                    {
                        address.setUser(null);
                        emailAddressRepository.save(email);
                    }
                }
                else
                {
                    emailAddress.setEmailAddress(user.getEmail());
                    emailAddress.setUser(user.getId());
                    emailAddress.setActive(true);
                    emailAddressRepository.save(emailAddress);

                }
                if((user.getLogin().equals(user.getEmail()))&&(loginOld.equalsIgnoreCase(user.getEmail())))
                {
                    mailService.sendCreationEmailToAdmin(user);
                }
                if((!user.getLastModifiedBy().equalsIgnoreCase(user.getEmail()))&&(!loginOld.equalsIgnoreCase(user.getEmail())))
                {
                 //   System.out.println("###emailtoadmin");

                    mailService.sendUpdationnEmailToAdmin(user);
                }
                log.debug("Confirmed user: {}", user);
                return user;
            });
    }


    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);

        return userRepository.findOneByResetKey(key)
            .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
            .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                return user;
            });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmail(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(Instant.now());
                return user;
            });
    }

    public User createUser(String login, String password, String firstName, String lastName, String email,
                           String imageUrl, String langKey, String company) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne(USER);
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setImageUrl(imageUrl);
        newUser.setLangKey(langKey);
        newUser.setCompany(company);
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        userSearchRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }
    public User createUser(UserDTO userDTO)
    {
        return createUser(userDTO, null);
    }
    public User createUser(UserDTO userDTO, String password) {
        boolean activated = false;
        User user = new User();
        user.setLogin(userDTO.getLogin());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setCompany(userDTO.getCompany());
        user.setEmail(userDTO.getEmail().toLowerCase());
        user.setImageUrl(userDTO.getImageUrl());
        user.setPermission(userDTO.getRolePermission());
        if (userDTO.getLangKey() == null) {
            user.setLangKey("en"); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        if(!StrUtils.isBlank(password))
        {
            String encryptedPassword = passwordEncoder.encode(password);
            // new user gets initially a generated password
            user.setPassword(encryptedPassword);
            user.setActivationKey(RandomUtil.generateActivationKey());
            user.setConfirmationKey(RandomUtil.generateConfirmationKey());
        }
        else //Admin created user
        {
            String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
            user.setPassword(encryptedPassword);
            user.setResetKey(RandomUtil.generateResetKey());
            user.setResetDate(Instant.now());
        }
        user.setCompany(userDTO.getCompany());
        user.setActivated(activated);
        user.setInterest_sell_data_product(userDTO.isInterest_sell_data_product());
        user.setNotify_news_update(userDTO.isNotify_news_update());
        user.setProfessional_investor(userDTO.isProfessional_investor());
        user.setUserOnboarding(false);
        user.setMymatchSearch(false);
        user=userRepository.save(user);
        String emailDomain=userDTO.getEmail();
		emailDomain=emailDomain.substring(emailDomain .indexOf("@") + 1);
		Domains domain=DatadomainsRepository.findByDomain(emailDomain);
        Set<Authority> authorities = new HashSet<>();
        if(null!=domain)
        {
         if(null!= domain.getOwnerInvestmentManager() && domain.getOwnerInvestmentManager().size()>0)
         	{
        	 System.out.println(domain.getOwnerInvestmentManager().size());
         		authorities.add(authorityRepository.findOne(USER));
  	            authorities.add(authorityRepository.findOne(ROLE_INVESTOR));
  	          if (userDTO.getAuthorities() != null) {
                  userDTO.getAuthorities().forEach(
                      authority -> authorities.add(authorityRepository.findOne(authority))
                  );
                  authorities.add(authorityRepository.findOne(USER));
              }
         	}
         else if(null!= domain.getOwnerDataProvider() && domain.getOwnerDataProvider().size()>0)
        	{
        	 System.out.println(domain.getOwnerDataProvider().size());
	            authorities.add(authorityRepository.findOne(USER));
	            authorities.add(authorityRepository.findOne(ROLE_PROVIDER));
	            if (userDTO.getAuthorities() != null) {
	                userDTO.getAuthorities().forEach(
	                    authority -> authorities.add(authorityRepository.findOne(authority))
	                );
	                authorities.add(authorityRepository.findOne(USER));
	            }
	            addOperatorPermission(user,domain.getOwnerDataProvider());
        	}
         else
         {
        	   if (userDTO.getAuthorities() != null) {
	                userDTO.getAuthorities().forEach(
	                    authority -> authorities.add(authorityRepository.findOne(authority))
	                );
	                authorities.add(authorityRepository.findOne(USER));
	            }         }
           domainService.updateEmailAddress(user,domain);
        	
        }
        else
		{
        	domainService.createDomain(user);
			/* if (userDTO.getAuthorities() != null) {
		            userDTO.getAuthorities().forEach(
		                authority -> authorities.add(authorityRepository.findOne(authority))
		            );*/
        	if (userDTO.getAuthorities() != null) {
                userDTO.getAuthorities().forEach(
                    authority -> authorities.add(authorityRepository.findOne(authority))
                );
                authorities.add(authorityRepository.findOne(USER));
            }
        	else
        	{
	            authorities.add(authorityRepository.findOne(USER));
        	}
		       /* }
		        else
		        {
		            authorities.add(authorityRepository.findOne(USER));
		        }*/
		}
       
        user.setAuthorities(authorities);
        userRepository.save(user);
        //User signup
        
        
        EmailAddress email=emailAddressRepository.findByEmailAddress(user.getEmail());
        if (email!=null)
        {
            email.setUser(user.getId());
            if(user.isNotify_news_update())
            {
                email.setSubscribedNewsletter(true);
            }
            EmailAddress result =emailAddressRepository.save(email);

        }else
        {
            EmailAddress newEmail=new EmailAddress();
            newEmail.setRecordID("rece"+ Utils.randomString(13,13));
            newEmail.setEmailAddress(user.getEmail().toLowerCase());
            newEmail.setUser(user.getId());
            if(user.isNotify_news_update())
            {
                newEmail.setSubscribedNewsletter(true);
            }
            EmailAddress result =emailAddressRepository.save(newEmail);
        }


        userSearchRepository.save(user);
        addDefaultProviders(user);
        if(userDTO.isNotify_news_update())
        {
            sentSubscriptionConfirmationEmail(user);
        }
        log.debug("Created Information for User: {}", user);
        return user;
    }
    private void addDefaultProviders(User user) {
        String providers = environment.getProperty("spring.defaults.unlocked.providers");
        if(!StrUtils.isBlank(providers))
        {
            Scanner s = new Scanner(providers).useDelimiter(";");
            while(s.hasNext())
            {
                String providerRecordID = s.next();
                DataProvider dataProvider = dataProviderRepository.findByRecordIDEquals(providerRecordID);

                if(dataProvider != null)
                {
                    UserProvider userProvider = new UserProvider();
                    userProvider.setProviderRecordID(providerRecordID);
                    userProvider.setProviderID(dataProvider.getId());
                    userProvider.setUserID(user.getId());
                    Boolean dataProviderEdit=user.getAuthorities().stream().filter(o -> o.getName().equals("ROLE_PROVIDER")).findFirst().isPresent();
                    if(dataProviderEdit.equals(true))
                        userProvider.setInterestType(INTEREST_TYPE_OPERATOR);
                    else
                        userProvider.setInterestType(INTEREST_TYPE_UNLOCK);
                    userProvider.setCreatedDate(LocalDateTime.now());

                    userProvider.setInactive(false);
                    userProviderRepository.save(userProvider);
                }
            }
        }
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user
     * @param lastName last name of user
     * @param Company company Name
     * @param email email id of user
     * @param langKey language key
     * @param imageUrl image URL of user
     */
    public void updateUser(String firstName, String lastName, String Company,String email, Boolean notify_news_update,String langKey, String imageUrl) {
    	userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setCompany(Company);
            String emailid=user.getEmail();
            user.setEmail(email);
            Date dateobj = new Date(System.currentTimeMillis());


            if(user.isNotify_news_update() && !notify_news_update)
            {

                sentUnSubscriptionConfirmationEmail(user);

            }
            if(!user.isNotify_news_update() && notify_news_update)
            {

                sentSubscriptionConfirmationEmail(user);
                user.setNotify_news_update(notify_news_update);
            } user.setLangKey(langKey);
            user.setImageUrl(imageUrl);
            if(emailid.equals(email)) {

                userSearchRepository.save(user);

            }
            else{
                String confirmationKey = RandomUtil.generateConfirmationKey();
                user.setConfirmationKey(confirmationKey);
                user.setConfirmed(false);
                userSearchRepository.save(user);
                mailService.sendUpdateEmail(user);

            }

            EmailAddress changeEmail=emailAddressRepository.findByEmailAddress(user.getEmail());
            if (changeEmail!=null)
            {
                changeEmail.setUser(null);
                if(user.isNotify_news_update())
                {
                    changeEmail.setSubscribedNewsletter(false);
                    changeEmail.setActive(false);
                }
                emailAddressRepository.save(changeEmail);

            }else
            {
                EmailAddress newEmail=new EmailAddress();
                newEmail.setRecordID("rece"+ Utils.randomString(13,13));
                newEmail.setEmailAddress(user.getEmail().toLowerCase());
                newEmail.setUser(null);
                newEmail.setActive(false);
                newEmail.setCreatedDate(dateobj);
                emailAddressRepository.save(newEmail);
            }



            log.debug("Changed Information for User: {}", user);


        });
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update
     * @return updated user
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO, boolean activateUser) {
        Optional<UserDTO> updatedUser = Optional.of(userRepository
            .findOne(userDTO.getId()))
            .map(user -> {
                    user.setLogin(userDTO.getLogin());
                    user.setFirstName(userDTO.getFirstName());
                    user.setLastName(userDTO.getLastName());
                    user.setCompany(userDTO.getCompany());
                    user.setEmail(userDTO.getEmail());
                    user.setImageUrl(userDTO.getImageUrl());
                    user.setActivated(userDTO.isActivated());
                    user.setInterest_sell_data_product(userDTO.isInterest_sell_data_product());
                    user.setNotify_news_update(userDTO.isNotify_news_update());
                    user.setProfessional_investor(userDTO.isProfessional_investor());
                    user.setLangKey(userDTO.getLangKey());
                    user.setPermission(userDTO.getRolePermission());
                    Set<Authority> managedAuthorities = user.getAuthorities();
                    managedAuthorities.clear();
                    userDTO.getAuthorities().stream()
                        .map(authorityRepository::findOne)
                        .forEach(managedAuthorities::add);
                    if (activateUser) {
                        user.setActivationKey(RandomUtil.generateActivationKey());
                    }
                    userSearchRepository.save(user);
                    log.debug("Changed Information for User: {}", user);
                    return user;
                }
            )
            .map(UserDTO::new);
        if(activateUser)
        {
            User user = userRepository.findOne(userDTO.getId());
            mailService.sendActivationEmail(user);
        }
        return updatedUser;
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            socialService.deleteUserSocialConnection(user.getLogin());
            userRepository.delete(user);
            userSearchRepository.delete(user);
            log.debug("Deleted User: {}", user);
        });
    }

    public void changePassword(String password) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
            String encryptedPassword = passwordEncoder.encode(password);
            user.setPassword(encryptedPassword);
            log.debug("Changed password for User: {}", user);
            StringBuffer content = new StringBuffer();
            content.append("Dear ")
                .append(user.getFirstName())
                .append(",<p>Your Insights account password has just been successfully changed.</p>")
                .append("<p>If this was not initiated by you or if you think there is an error, please respond to this email.</p>")
                .append("<p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");
            mailService.sendEmail(user.getEmail(), "Your Insights Password Was Changed", content.toString(),false, true);
        });
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities(Long id) {
        return userRepository.findOneWithAuthoritiesById(id);
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities() {
        return userRepository.findOneWithAuthoritiesByLogin(SecurityUtils.getCurrentUserLogin()).orElse(null);
    }

    @Transactional(readOnly = true)
    public User getUser() {
        return userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).orElse(null);
    }

    @Transactional(readOnly = true)
    public long getCurrentUserID() {
        return userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).orElse(null).getId();
    }

    @Transactional(readOnly = true)
    public User getUser(long id)
    {
        return userRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    public User getUser(String loginID) {
        return userRepository.findOneByLogin(loginID).orElse(null);
    }
    /**
     * Persistent Token are used for providing automatic authentication, they should be automatically deleted after
     * 30 days.
     * <p>
     * This is scheduled to get fired everyday, at midnight.
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void removeOldPersistentTokens() {
        LocalDate now = LocalDate.now();
        persistentTokenRepository.findByTokenDateBefore(now.minusMonths(1)).forEach(token -> {
            log.debug("Deleting token {}", token.getSeries());
            User user = token.getUser();
            user.getPersistentTokens().remove(token);
            persistentTokenRepository.delete(token);
        });
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).

     @Scheduled(cron = "0 0 1 * * ?")
     public void removeNotActivatedUsers() {
     List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(30, ChronoUnit.DAYS));
     for (User user : users) {
     log.debug("Deleting not activated user {}", user.getLogin());
     userRepository.delete(user);
     userSearchRepository.delete(user);
     }
     }*/

    /**
     * @return a list of all the authorities
     */
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }
    public void sentSubscriptionConfirmationEmail(User user)
    {

        //UserRequest userRequest= userRequestRepository.findByUserIdAndRequestType(user.getId(),REQUEST_TYPE_EMAIL_UNSUBSCRIPTION);
    	UserRequest userRequest= userRequestRepository.findByUserIdAndRequestType(user.getId(),applicationProperties.getEmailUnsubscription());

        if(userRequest==null)
        {

            UserRequest userRequest1=new UserRequest();
            userRequest1.setUserName(user.getFirstName() + " " + user.getLastName());
            userRequest1.setUserEmail(user.getEmail());
            //userRequest1.setRequestType(REQUEST_TYPE_EMAIL_SUBSCRIPTION);
            userRequest1.setRequestType(applicationProperties.getEmailSubscription());
            userRequest1.setUserId(user.getId());
            String confirmationKey = RandomUtil.generateConfirmationKey();
            userRequest1.setConfirmationkey(confirmationKey);
            userRequest1.setRequestTopic("Request Email Subscription");
            userRequest1.setCreatedDate(LocalDateTime.now());
            userRequestRepository.save(userRequest1);
            EmailAddress email=emailAddressRepository.findByEmailAddress(user.getEmail());
            email.setConfirmationkey(confirmationKey);
            emailAddressRepository.save(email);
            JSONObject variables = new  JSONObject().put(BASE_URL, jHipsterProperties.getMail().getBaseUrl()+"/#/email-action?api=newsletter-subscription&key="+confirmationKey);
            int templateID = Integer.parseInt(environment.getProperty("spring.mailjet.subscription-confirmation-templateID"));
            mailJetService.sendMail(userRequest1.getUserEmail(), userRequest1.getUserName(), templateID, "Insights Newsletter: Please Confirm Subscription", variables);
           String body=mailJetService.getMail(templateID);
            Email emailAdded=new Email();
            emailAdded.setSubject("Insights Newsletter: Please Confirm Subscription");
            emailAdded.setEmailAdress(userRequest1.getUserEmail());
            emailAdded.setNotificationStatus(1);
            LocalDateTime now = LocalDateTime.now();  
            emailAdded.setSentDate(now);
            emailAdded.setCreatedDate(now);
            emailAdded.setTemplateId(templateID);
            if(null!=body)
            {
            	emailAdded.setBody(body);
            }
            emailRepository.save(emailAdded);
        } else if (userRequest.getUserId() != null && user.getId().equals(userRequest.getUserId())) {
            //userRequest.setRequestType(REQUEST_TYPE_EMAIL_SUBSCRIPTION);
        	userRequest.setRequestType(applicationProperties.getEmailSubscription());
        	userRequest.setRequestTopic("Request Email Subscription");
            userRequestRepository.save(userRequest);
            JSONObject variables = new  JSONObject().put(BASE_URL, jHipsterProperties.getMail().getBaseUrl()+"/#/email-action?api=newsletter-subscription&key="+userRequest.getConfirmationkey());
            int templateID = Integer.parseInt(environment.getProperty("spring.mailjet.subscription-confirmation-templateID"));
            mailJetService.sendMail(userRequest.getUserEmail(), userRequest.getUserName(), templateID, "Insights Newsletter: Please Confirm Subscription", variables);
            String body=mailJetService.getMail(templateID);
            Email emailAdded=new Email();
            emailAdded.setSubject("Insights Newsletter: Please Confirm Subscription");
            emailAdded.setEmailAdress(userRequest.getUserEmail());
            emailAdded.setNotificationStatus(1);
            LocalDateTime now = LocalDateTime.now();  
            emailAdded.setSentDate(now);
            emailAdded.setCreatedDate(now);
            emailAdded.setTemplateId(templateID);
            if(null!=body)
            {
            	emailAdded.setBody(body);
            }
            emailRepository.save(emailAdded);
        }

    }
    public void sentUnSubscriptionConfirmationEmail(User user)
    {
    	

       // UserRequest userRequest= userRequestRepository.findByUserIdAndRequestType(user.getId(),REQUEST_TYPE_EMAIL_SUBSCRIPTION);
    	UserRequest userRequest= userRequestRepository.findByUserIdAndRequestType(user.getId(),applicationProperties.getEmailSubscription());
        if(null!=userRequest)
        {
        userRequest.setUserName(user.getFirstName() + " " + user.getLastName());
        userRequest.setUserEmail(user.getEmail());
        userRequest.setRequestType(applicationProperties.getEmailUnsubscription());
        //userRequest.setRequestType(REQUEST_TYPE_EMAIL_UNSUBSCRIPTION);
        userRequest.setUserId(user.getId());
        userRequest.setId(userRequest.getId());
        userRequest.setRequestTopic("Request Email Unsubscription");
        userRequest.setCreatedDate(LocalDateTime.now());
        //System.out.println("user request "+user.toString());
        userRequestRepository.save(userRequest);
        JSONObject variables = new  JSONObject().put(BASE_URL, jHipsterProperties.getMail().getBaseUrl()+"/#/email-action?api=newsletter-unsubscription&key="+userRequest.getConfirmationkey());
        int templateID = Integer.parseInt(environment.getProperty("spring.mailjet.unsubscription-confirmation-templateID"));
        mailJetService.sendMail(userRequest.getUserEmail(), userRequest.getUserName(), templateID, "Insights Newsletter: Please Confirm UnSubscription", variables);
       String body= mailJetService.getMail(templateID);
        Email emailAdded=new Email();
        emailAdded.setSubject("Insights Newsletter: Please Confirm UnSubscription");
        emailAdded.setEmailAdress(userRequest.getUserEmail());
        emailAdded.setNotificationStatus(1);
        LocalDateTime now = LocalDateTime.now();  
        emailAdded.setSentDate(now);
        emailAdded.setCreatedDate(now);
        emailAdded.setTemplateId(templateID);
        if(null!=body)
        {
        	emailAdded.setBody(body);
        }
        emailRepository.save(emailAdded);
        }
        else
        {
        	UserRequest userRequests = new UserRequest();
        	userRequests.setUserName(user.getFirstName() + " " + user.getLastName());
        	userRequests.setUserEmail(user.getEmail());
        	userRequests.setRequestType(applicationProperties.getEmailUnsubscription());
             //userRequest.setRequestType(REQUEST_TYPE_EMAIL_UNSUBSCRIPTION);
        	userRequests.setUserId(user.getId());
             //userRequest.setId(userRequest.getId());
        	userRequests.setRequestTopic("Request Email Unsubscription");
        	userRequests.setCreatedDate(LocalDateTime.now());
        	String confirmationKey = RandomUtil.generateConfirmationKey();
            userRequests.setConfirmationkey(confirmationKey);
             //System.out.println("user request "+user.toString());
             userRequestRepository.save(userRequests);
             JSONObject variables = new  JSONObject().put(BASE_URL, jHipsterProperties.getMail().getBaseUrl()+"/#/email-action?api=newsletter-unsubscription&key="+userRequests.getConfirmationkey());
             int templateID = Integer.parseInt(environment.getProperty("spring.mailjet.unsubscription-confirmation-templateID"));
             mailJetService.sendMail(userRequests.getUserEmail(), userRequests.getUserName(), templateID, "Insights Newsletter: Please Confirm UnSubscription", variables);
            String body= mailJetService.getMail(templateID);
             Email emailAdded=new Email();
             emailAdded.setSubject("Insights Newsletter: Please Confirm UnSubscription");
             emailAdded.setEmailAdress(userRequests.getUserEmail());
             emailAdded.setNotificationStatus(1);
             LocalDateTime now = LocalDateTime.now();  
             emailAdded.setSentDate(now);
             emailAdded.setCreatedDate(now);
             emailAdded.setTemplateId(templateID);
             if(null!=body)
             {
            	 emailAdded.setBody(body);
             }
             emailRepository.save(emailAdded);
        }
    }

    public void subscribeMailingList(String key)
    {
        UserRequest ur = userRequestRepository.findOneByconfirmationkeyEquals(key);
        if(ur == null)
        {
            return;
        }

        int mailListID = Integer.parseInt(environment.getProperty("spring.mailjet.subscription-list"));
        MailjetClient client = new MailjetClient(environment.getProperty("spring.mail.username"), environment.getProperty("spring.mail.password"));

        MailjetRequest newContactRequest = new MailjetRequest(ContactManagemanycontacts.resource)
            .property(ContactManagemanycontacts.CONTACTSLISTS,
                new JSONArray()
                    .put(new JSONObject()
                        .put("ListID", environment.getProperty("spring.mailjet.subscription-list")))
                    .put(new JSONObject().put("Action", "addforce")))
            .property(ContactManagemanycontacts.CONTACTS,
                new JSONArray()
                    .put(new JSONObject()
                        .put(Contact.EMAIL, ur.getUserEmail())
                        .put(Contact.NAME, ur.getUserName())));
        new MailjetRequest(Contactdata.resource)
            .property(Contact.NAME, ur.getUserName())
            .property(Contact.EMAIL, ur.getUserEmail());

        MailjetResponse response = null;
        try {
            response = client.post(newContactRequest);
        } catch (MailjetException e) {
            log.error(e.getMessage(), e);
        } catch (MailjetSocketTimeoutException e) {
            log.error(e.getMessage(), e);
        }
        log.info("Response status: " + response.getStatus());
        log.info("Response data: " + response.getData());

        try {
            MailjetRequest addUserToMailingListRequest = new MailjetRequest(ContactslistManageManyContacts.resource, mailListID)
                .property(ContactslistManageManyContacts.ACTION, "addforce")
                .property(ContactslistManageManyContacts.CONTACTS, new JSONArray()
                    .put(new JSONObject()
                        .put(Contact.EMAIL, ur.getUserEmail())
                        .put(Contact.NAME, ur.getUserName())));
            response = client.post(addUserToMailingListRequest);
            //System.out.println(response.getStatus());
            //System.out.println(response.getData());
        } catch (MailjetException e) {
            log.error(e.getMessage(), e);
        } catch (MailjetSocketTimeoutException e) {
            log.error(e.getMessage(), e);
        }
        log.info("Response status: " + response.getStatus());
        log.info("Response data: " + response.getData());
        if(ur!=null)
        {
            EmailAddress emailAddress=emailAddressRepository.findByEmailAddress(ur.getUserEmail());
            emailAddress.setActive(true);
            Date dateobj = new Date(System.currentTimeMillis());
            emailAddress.setSubscribedNewsletter(true);
            emailAddress.setDateConfirmedSubscription(dateobj);
            emailAddressRepository.save(emailAddress);
        }


    }




    public void unsubscribeMailingList(String key)
    {
        UserRequest ur = userRequestRepository.findOneByconfirmationkeyEquals(key);

        if(ur == null)
        {
            return;
        }

        int mailListID = Integer.parseInt(environment.getProperty("spring.mailjet.subscription-list"));
        MailjetClient client = new MailjetClient(environment.getProperty("spring.mail.username"), environment.getProperty("spring.mail.password"));

        MailjetRequest newContactRequest = new MailjetRequest(ContactManagemanycontacts.resource)
            .property(ContactManagemanycontacts.CONTACTSLISTS,
                new JSONArray()
                    .put(new JSONObject()
                        .put("ListID", environment.getProperty("spring.mailjet.subscription-list")))
                    .put(new JSONObject().put("Action", "remove")))
            .property(ContactManagemanycontacts.CONTACTS,
                new JSONArray()
                    .put(new JSONObject()
                        .put(Contact.EMAIL, ur.getUserEmail())
                        .put(Contact.NAME, ur.getUserName())));
        new MailjetRequest(Contactdata.resource)
            .property(Contact.NAME, ur.getUserName())
            .property(Contact.EMAIL, ur.getUserEmail());

        MailjetResponse response = null;
        try {
            response = client.post(newContactRequest);
        } catch (MailjetException e) {
            log.error(e.getMessage(), e);
        } catch (MailjetSocketTimeoutException e) {
            log.error(e.getMessage(), e);
        }
        log.info("Response status: " + response.getStatus());
        log.info("Response data: " + response.getData());

        try {
            MailjetRequest addUserToMailingListRequest = new MailjetRequest(ContactslistManageManyContacts.resource, mailListID)
                .property(ContactslistManageManyContacts.ACTION, "remove")
                .property(ContactslistManageManyContacts.CONTACTS, new JSONArray()
                    .put(new JSONObject()
                        .put(Contact.EMAIL, ur.getUserEmail())
                        .put(Contact.NAME, ur.getUserName())));
            response = client.post(addUserToMailingListRequest);
            /*System.out.println(response.getStatus());
            System.out.println(response.getData());*/
        } catch (MailjetException e) {
            log.error(e.getMessage(), e);
        } catch (MailjetSocketTimeoutException e) {
            log.error(e.getMessage(), e);
        }
        log.info("Response status: " + response.getStatus());
        log.info("Response data: " + response.getData());

        if(ur.getUserEmail()!=null)
        {
         //   System.out.println("!!"+ur.getUserEmail());
            User user = userRepository.findByEmail(ur.getUserEmail());
            user.setNotify_news_update(false);
            user=userRepository.save(user);
            mailService.sendUnsubscriptionEmail(user);
        }

        if(ur!=null)
        {
            EmailAddress emailAddress=emailAddressRepository.findByEmailAddress(ur.getUserEmail());
            Date dateobj = new Date(System.currentTimeMillis());
            emailAddress.setSubscribedNewsletter(false);
            emailAddress.setDateConfirmedUnubscription(dateobj);
            emailAddressRepository.save(emailAddress);
        }

    }

    public void unSubscriptionMailingList(String key)
    {
        UserRequest ur = userRequestRepository.findOneByconfirmationkeyEquals(key);

        if(ur == null)
        {
            return;
        }
        //ur.setRequestType(REQUEST_TYPE_EMAIL_UNSUBSCRIPTION);
        ur.setRequestType(applicationProperties.getEmailUnsubscription());
        ur.setRequestTopic("Request Email Unsubscription");
        userRequestRepository.save(ur);
        userRepository.findOneByLogin(ur.getUserEmail()).ifPresent(user -> {
            user.setNotify_news_update(false);
            userRepository.save(user);
        });
        int mailListID = Integer.parseInt(environment.getProperty("spring.mailjet.subscription-list"));
        MailjetClient client = new MailjetClient(environment.getProperty("spring.mail.username"), environment.getProperty("spring.mail.password"));

        MailjetRequest newContactRequest = new MailjetRequest(ContactManagemanycontacts.resource)
            .property(ContactManagemanycontacts.CONTACTSLISTS,
                new JSONArray()
                    .put(new JSONObject()
                        .put("ListID", environment.getProperty("spring.mailjet.subscription-list")))
                    .put(new JSONObject().put("Action", "remove")))
            .property(ContactManagemanycontacts.CONTACTS,
                new JSONArray()
                    .put(new JSONObject()
                        .put(Contact.EMAIL, ur.getUserEmail())
                        .put(Contact.NAME, ur.getUserName())));
        new MailjetRequest(Contactdata.resource)
            .property(Contact.NAME, ur.getUserName())
            .property(Contact.EMAIL, ur.getUserEmail());

        MailjetResponse response = null;
        try {
            response = client.post(newContactRequest);
        } catch (MailjetException e) {
            log.error(e.getMessage(), e);
        } catch (MailjetSocketTimeoutException e) {
            log.error(e.getMessage(), e);
        }
        log.info("Response status: " + response.getStatus());
        log.info("Response data: " + response.getData());

        try {
            MailjetRequest addUserToMailingListRequest = new MailjetRequest(ContactslistManageManyContacts.resource, mailListID)
                .property(ContactslistManageManyContacts.ACTION, "remove")
                .property(ContactslistManageManyContacts.CONTACTS, new JSONArray()
                    .put(new JSONObject()
                        .put(Contact.EMAIL, ur.getUserEmail())
                        .put(Contact.NAME, ur.getUserName())));
            response = client.post(addUserToMailingListRequest);
            /*System.out.println(response.getStatus());
            System.out.println(response.getData());*/
        } catch (MailjetException e) {
            log.error(e.getMessage(), e);
        } catch (MailjetSocketTimeoutException e) {
            log.error(e.getMessage(), e);
        }
        log.info("Response status: " + response.getStatus());
        log.info("Response data: " + response.getData());
    }
    public User requestClientApi(String login,String password) {
        String encryptedPassword = passwordEncoder.encode(password);
        // System.out.println(encryptedPassword);
        User userAuthenticate =userRepository.findByLoginAndPassword(login,passwordEncoder.encode(password));
        return userAuthenticate;
    }


    public User createAccountUser(String email,String inviteType) {
        User user=new User();
        user.setEmail(email);
        user.setConfirmationKey(RandomUtil.generateConfirmationKey());
        user.setActivationKey(RandomUtil.generateActivationKey());
        user.setLogin(email);
        user.setLangKey("en");
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        if(inviteType.equalsIgnoreCase("provider"))
        {
            Set<Authority> authorities=new HashSet<>(1);
            authorities.add(authorityRepository.findOne("ROLE_PROVIDER"));
            authorities.add(authorityRepository.findOne("ROLE_USER"));
            user.setAuthorities(authorities);
            user.setUserOnboarding(false);
            user.setMymatchSearch(false);
            user=userRepository.save(user);
        }
        else if(inviteType.equalsIgnoreCase("investor"))
        {
            Set<Authority> authorities=new HashSet<>(1);
            authorities.add(authorityRepository.findOne("ROLE_INVESTOR"));
            authorities.add(authorityRepository.findOne("ROLE_USER"));
            user.setAuthorities(authorities);
            user.setUserOnboarding(false);
            user.setMymatchSearch(false);
            user=userRepository.save(user);
        }
        else if(inviteType.equalsIgnoreCase("default")){
            Set<Authority> authorities = new HashSet<>(1);
            authorities.add(authorityRepository.findOne("ROLE_USER"));
            user.setAuthorities(authorities);
            user.setUserOnboarding(false);
            user.setMymatchSearch(false);
            user=userRepository.save(user);
        }
        userSearchRepository.save(user);
        return user;
        // TODO Auto-generated method stub

    }

    public User createinviteUserAccount(User user) {
        // TODO Auto-generated method stub
        Set<Authority> authorities = new HashSet<>();
        if (user.getAuthorities() != null) {

            authorities.add(authorityRepository.findOne(USER));
        }
        else {
            authorities.add(authorityRepository.findOne(USER));
        }
        // authorities.add(authorityRepository.findOne("ROLE_PROVIDER"));
        User userAccount= userRepository.findUser(user.getId());
        for(Authority author:userAccount.getAuthorities())
        {
            authorities.add(author);
        }
        user.setAuthorities(authorities);

        if(!StrUtils.isBlank(user.getPassword()))
        {
            String encryptedPassword = passwordEncoder.encode(user.getPassword());
            // new user gets initially a generated password
            user.setPassword(encryptedPassword);
        }
        if (user.getLangKey() == null) {
            user.setLangKey("en"); // default language
        }

        user.setActivated(true);
        user.setConfirmed(true);
        user.setConfirmationKey(null);
        user.setActivationKey(null);
        User userUpdated= userRepository.save(user);
        userSearchRepository.save(user);
        Authority authority = user.getAuthorities().stream().filter(x -> (x.getName().contains("ROLE_PROVIDER")))
            .findAny().orElse(null);
        if (authority!=null) {
            userUpdated.setRole("Data Provider");
        }else{
            //userUpdated.setRole(" Asset Manager");
        	userUpdated.setRole("\"Unsure->Please Input\"");
        }
       Authority authoritiess= user.getAuthorities().stream().filter(x -> (x.getName().contains("ROLE_INVESTOR")))
               .findAny().orElse(null);
      if(authoritiess!=null)
      {
    	  userUpdated.setRole("Asset Manager");
      }
        addDefaultProviders(user);

        if(user.isNotify_news_update())
        {
            sentSubscriptionConfirmationEmail(user);
        }
        EmailAddress address=emailAddressRepository.findByEmailAddress(userUpdated.getEmail());
        if(user.isNotify_news_update())
        {
        address.setSubscribedNewsletter(true);
        }
        else
        {
        	address.setSubscribedNewsletter(false);
        }
        emailAddressRepository.save(address);
        return userUpdated;
    }

	public User getUserValues(Long id) {
		// TODO Auto-generated method stub
		User user=userRepository.findUserData(id);
		return user;
	}

	public List<LookupCode> getGeographies(Long lookupcode) {
		// TODO Auto-generated method stub
		List<LookupCode>geographiesList=lookupCodeRepository.getGeographiesValue(lookupcode);
		return geographiesList;
	}

	public List<User> getmyMatchedUser() {
		// TODO Auto-generated method stub
	List<User> user=userRepository.findMyMatchesProvider();
		return user;
	}

	public boolean findAuthority(Long id) {
		// TODO Auto-generated method stub
		boolean authority=false;
	User user=userRepository.findUser(id);
	if(null!=user)
	{
		 Authority authoritiess= user.getAuthorities().stream().filter(x -> (x.getName().contains("ROLE_INVESTOR")))
	               .findAny().orElse(null);
		 if(null!=authoritiess)
		 {
			 authority=true;
		 }
		 else
		 {
			 authority=false;
		 }
	}
		return authority;
	}

	public void sendMyMatchesUser(User usersValue) {
		// TODO Auto-generated method stub
		usersValue.setMailSent(true);
		userRepository.save(usersValue);
	}

	public void addOperatorPermission(User user, Set<DataProvider> ownerDataProvider) {
		// TODO Auto-generated method stub
		UserProvider userProvider=new UserProvider();
		userProvider.setUserID(user.getId());
		userProvider.setInterestType(INTEREST_TYPE_OPERATOR);
		for(DataProvider provider:ownerDataProvider)
		{
			UserProvider userOperatorPermission=userProviderRepository.findOneByUserIDEqualsAndProviderIDEqualsAndInterestType(user.getId(), provider.getId(), INTEREST_TYPE_OPERATOR);
			if(!Optional.ofNullable(userOperatorPermission).isPresent())
			{
				userProvider.setProviderID(provider.getId());
				userProvider.setProviderRecordID(provider.getRecordID());
				userProvider.setCreatedDate(LocalDateTime.now());
				userProvider.setInactive(false);
				userProviderRepository.save(userProvider);
			}
			
		}
	}

	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		
		return userRepository.findAllUserData();
	}
}
