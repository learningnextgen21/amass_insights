package com.ai.mp.service;

import com.ai.mp.domain.*;
import com.ai.mp.repository.*;
import com.ai.mp.repository.search.*;
import com.codahale.metrics.annotation.Timed;
import io.searchbox.client.JestClient;
import org.elasticsearch.indices.IndexAlreadyExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;

@Service
public class ElasticsearchIndexService {

    private final Logger log = LoggerFactory.getLogger(ElasticsearchIndexService.class);

    private final AdminRoleRepository adminRoleRepository;

    private final DataEventSearchRepository dataEventSearchRepository;

    private final AdminRoleSearchRepository adminRoleSearchRepository;

    private final UserDataCategoryRepository amassUserDataCategoryRepository;

    private final UserDataCategorySearchRepository amassUserDataCategorySearchRepository;

    private final UserNotificationRepository amassUserNotificationRepository;

    private final UserNotificationSearchRepository amassUserNotificationSearchRepository;

    private final UserProviderRepository amassUserProviderRepository;

    private final UserProviderSearchRepository amassUserProviderSearchRepository;

    private final ClientRoleRepository clientRoleRepository;

    private final ClientRoleSearchRepository clientRoleSearchRepository;

    private final DataArticleRepository dataArticleRepository;

    private final DataArticleSearchRepository dataArticleSearchRepository;

    private final DataCategoryRepository dataCategoryRepository;

    private final DataCategorySearchRepository dataCategorySearchRepository;

    private final DataFeatureRepository dataFeatureRepository;

    private final DataFeatureSearchRepository dataFeatureSearchRepository;

    private final DataIndustryRepository dataIndustryRepository;

    private final DataIndustrySearchRepository dataIndustrySearchRepository;

    private final DataOrganizationRepository dataOrganizationRepository;

    private final DataOrganizationSearchRepository dataOrganizationSearchRepository;

    private final DataProviderRepository dataProviderRepository;

    private final DataProviderSearchRepository dataProviderSearchRepository;

    private final DataResourceSearchRepository dataResourceSearchRepository;

    private final DataProviderTagRepository dataProviderTagRepository;

    private final DataProviderTagSearchRepository dataProviderTagSearchRepository;

    private final DataProviderTypeRepository dataProviderTypeRepository;

    private final DataProviderTypeSearchRepository dataProviderTypeSearchRepository;

    private final DataSourceRepository dataSourceRepository;

    private final DataSourceSearchRepository dataSourceSearchRepository;

    private final LookupCodeRepository lookupCodeRepository;

    private final LookupCodeSearchRepository lookupCodeSearchRepository;

    private final UserRepository userRepository;

    private final DataEventRepository dataEventRepository;

    private final UserSearchRepository userSearchRepository;

    private final ElasticsearchTemplate elasticsearchTemplate;

    private final DataProviderService dataProviderService;

    private final DataResourceService dataResourceService;
    private final DataResourceRepository dataResourceRepository;
    private final DataTopicRepository dataTopicRepository;

    private final DataTopicSearchRepository dataTopicSearchRepository;

    private final DataResourcePurposeRepository dataResourcePurposeRepository;
    private final DataResourcePurposeTypeRepository dataResourcePurposeTypeRepository;

    private final DataResourcePurposeSearchRepository dataResourcePurposeSearchRepository;
    private final DataResourcePurposeTypeSearchRepository dataResourcePurposeTypeSearchRepository;

    private final DataLinkSearchRepository dataLinkSearchRepository;
    private final DataLinksRepository dataLinksRepository;

    private final DocumentsMetadataRepository documentsMetadataRepository;
    private final DocumentsMetadataSearchRepository documentsMetadataSearchRepository;
    private final EmailAddressRepository emailAddressRepository;
    private final EmailAddressSearchRepository emailAddressSearchRepository;
    private final DataInvestmentMangersRepository dataInvestmentManagersRepository;
    private final DataInvestmentMangerSearchRepository dataInvestmentMangerSearchRepository;
    private final EmailsRepository emailsRepository;
    private final EmailsSearchRepository emailsSearchRepository;
    private final DataDomainsRepository dataDomainsRepository;
    private final DataDomainSearchRepository dataDomainSearchRepository;
    @Autowired
    JestClient client;

    public ElasticsearchIndexService(
        DataEventSearchRepository dataEventSearchRepository, DataTopicRepository dataTopicRepository,
        DataTopicSearchRepository dataTopicSearchRepository,
        UserRepository userRepository,
        UserSearchRepository userSearchRepository,
        AdminRoleRepository adminRoleRepository,
        AdminRoleSearchRepository adminRoleSearchRepository,
        UserDataCategoryRepository amassUserDataCategoryRepository,
        UserDataCategorySearchRepository amassUserDataCategorySearchRepository,
        UserNotificationRepository amassUserNotificationRepository,
        UserNotificationSearchRepository amassUserNotificationSearchRepository,
        UserProviderRepository amassUserProviderRepository,
        UserProviderSearchRepository amassUserProviderSearchRepository,
        ClientRoleRepository clientRoleRepository,
        ClientRoleSearchRepository clientRoleSearchRepository,
        DataArticleRepository dataArticleRepository,
        DataArticleSearchRepository dataArticleSearchRepository,
        DataCategoryRepository dataCategoryRepository,
        DataCategorySearchRepository dataCategorySearchRepository,
        DataFeatureRepository dataFeatureRepository,
        DataFeatureSearchRepository dataFeatureSearchRepository,
        DataIndustryRepository dataIndustryRepository,
        DataIndustrySearchRepository dataIndustrySearchRepository,
        DataOrganizationRepository dataOrganizationRepository,
        DataOrganizationSearchRepository dataOrganizationSearchRepository,
        DataProviderRepository dataProviderRepository,
        DataEventRepository dataEventRepository,
        DataProviderSearchRepository dataProviderSearchRepository,
        DataProviderTagRepository dataProviderTagRepository,
        DataProviderTagSearchRepository dataProviderTagSearchRepository,
        DataProviderTypeRepository dataProviderTypeRepository,
        DataProviderTypeSearchRepository dataProviderTypeSearchRepository,
        DataSourceRepository dataSourceRepository,
        DataSourceSearchRepository dataSourceSearchRepository,
        LookupCodeRepository lookupCodeRepository,
        LookupCodeSearchRepository lookupCodeSearchRepository,
        DataResourcePurposeRepository dataResourcePurposeRepository,
        DataResourcePurposeTypeRepository dataResourcePurposeTypeRepository,
        DataResourcePurposeSearchRepository dataResourcePurposeSearchRepository,
        DataResourcePurposeTypeSearchRepository dataResourcePurposeTypeSearchRepository,
        DataProviderService dataProviderService, DataResourceService dataResourceService, DataResourceSearchRepository dataResourceSearchRepository, DataResourceRepository dataResourceRepository,
        ElasticsearchTemplate elasticsearchTemplate, DataLinkSearchRepository dataLinkSearchRepository, DataLinksRepository dataLinksRepository, DocumentsMetadataRepository documentsMetadataRepository, DocumentsMetadataSearchRepository documentsMetadataSearchRepository,EmailAddressRepository emailAddressRepository,EmailAddressSearchRepository emailAddressSearchRepository,DataInvestmentMangersRepository dataInvestmentManagersRepository, DataInvestmentMangerSearchRepository dataInvestmentMangerSearchRepository,EmailsRepository emailsRepository,EmailsSearchRepository emailsSearchRepository
        ,DataDomainsRepository dataDomainsRepository,DataDomainSearchRepository dataDomainSearchRepository) {
        this.dataEventSearchRepository = dataEventSearchRepository;
        this.userRepository = userRepository;
        this.userSearchRepository = userSearchRepository;
        this.adminRoleRepository = adminRoleRepository;
        this.adminRoleSearchRepository = adminRoleSearchRepository;
        this.amassUserDataCategoryRepository = amassUserDataCategoryRepository;
        this.amassUserDataCategorySearchRepository = amassUserDataCategorySearchRepository;
        this.amassUserNotificationRepository = amassUserNotificationRepository;
        this.amassUserNotificationSearchRepository = amassUserNotificationSearchRepository;
        this.amassUserProviderRepository = amassUserProviderRepository;
        this.amassUserProviderSearchRepository = amassUserProviderSearchRepository;
        this.clientRoleRepository = clientRoleRepository;
        this.clientRoleSearchRepository = clientRoleSearchRepository;
        this.dataArticleRepository = dataArticleRepository;
        this.dataArticleSearchRepository = dataArticleSearchRepository;
        this.dataCategoryRepository = dataCategoryRepository;
        this.dataCategorySearchRepository = dataCategorySearchRepository;
        this.dataFeatureRepository = dataFeatureRepository;
        this.dataFeatureSearchRepository = dataFeatureSearchRepository;
        this.dataIndustryRepository = dataIndustryRepository;
        this.dataIndustrySearchRepository = dataIndustrySearchRepository;
        this.dataOrganizationRepository = dataOrganizationRepository;
        this.dataOrganizationSearchRepository = dataOrganizationSearchRepository;
        this.dataProviderRepository = dataProviderRepository;
        this.dataProviderSearchRepository = dataProviderSearchRepository;
        this.dataProviderTagRepository = dataProviderTagRepository;
        this.dataProviderTagSearchRepository = dataProviderTagSearchRepository;
        this.dataProviderTypeRepository = dataProviderTypeRepository;
        this.dataProviderTypeSearchRepository = dataProviderTypeSearchRepository;
        this.dataSourceRepository = dataSourceRepository;
        this.dataSourceSearchRepository = dataSourceSearchRepository;
        this.lookupCodeRepository = lookupCodeRepository;
        this.lookupCodeSearchRepository = lookupCodeSearchRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.dataProviderService = dataProviderService;
        this.dataTopicRepository = dataTopicRepository;
        this.dataTopicSearchRepository = dataTopicSearchRepository;
        this.dataResourcePurposeRepository = dataResourcePurposeRepository;
        this.dataResourcePurposeTypeRepository = dataResourcePurposeTypeRepository;
        this.dataResourcePurposeSearchRepository = dataResourcePurposeSearchRepository;
        this.dataResourcePurposeTypeSearchRepository = dataResourcePurposeTypeSearchRepository;
        this.dataEventRepository = dataEventRepository;
        this.dataResourceService=dataResourceService;
        this.dataResourceSearchRepository=dataResourceSearchRepository;
        this.dataResourceRepository=dataResourceRepository;
        this.dataLinkSearchRepository = dataLinkSearchRepository;
        this.dataLinksRepository = dataLinksRepository;
        this.documentsMetadataRepository = documentsMetadataRepository;
        this.documentsMetadataSearchRepository = documentsMetadataSearchRepository;
        this.emailAddressRepository=emailAddressRepository;
        this.emailAddressSearchRepository=emailAddressSearchRepository;
        this.dataInvestmentManagersRepository=dataInvestmentManagersRepository;
        this.dataInvestmentMangerSearchRepository=dataInvestmentMangerSearchRepository;
        this.emailsRepository=emailsRepository;
    	this.emailsSearchRepository=emailsSearchRepository;	
    	this.dataDomainsRepository=dataDomainsRepository;
    	this.dataDomainSearchRepository=dataDomainSearchRepository;
    }

    @Async
    @Timed
    public void reindexAll() {
        // reindexForClass(DataEvent.class, dataEventRepository, dataEventSearchRepository);
        reindexForClass(AdminRole.class, adminRoleRepository, adminRoleSearchRepository);
        reindexForClass(UserDataCategory.class, amassUserDataCategoryRepository, amassUserDataCategorySearchRepository);
        reindexForClass(UserNotification.class, amassUserNotificationRepository, amassUserNotificationSearchRepository);
        reindexForClass(UserProvider.class, amassUserProviderRepository, amassUserProviderSearchRepository);
        reindexForClass(ClientRole.class, clientRoleRepository, clientRoleSearchRepository);
        reindexForClass(DataTopic.class, dataTopicRepository, dataTopicSearchRepository);
        reindexForClass(DataLinks.class, dataLinksRepository, dataLinkSearchRepository);
       // reindexForClass(DocumentsMetadata.class, documentsMetadataRepository, documentsMetadataSearchRepository);
        reindexDocumentMetaData();
        reindexForClass(DataResourcePurpose.class, dataResourcePurposeRepository, dataResourcePurposeSearchRepository);
        reindexForClass(DataResourcePurposeType.class, dataResourcePurposeTypeRepository, dataResourcePurposeTypeSearchRepository);
        reindexForClass(DataCategory.class, dataCategoryRepository, dataCategorySearchRepository);
        reindexForClass(DataFeature.class, dataFeatureRepository, dataFeatureSearchRepository);
        reindexForClass(DataIndustry.class, dataIndustryRepository, dataIndustrySearchRepository);
        reindexForClass(DataOrganization.class, dataOrganizationRepository, dataOrganizationSearchRepository);
        reindexProviders(true);
        reindexResources();
        reindexEvents();
        reindexArticles();
        reindexForClass(DataProviderTag.class, dataProviderTagRepository, dataProviderTagSearchRepository);
        reindexForClass(DataProviderType.class, dataProviderTypeRepository, dataProviderTypeSearchRepository);
        reindexForClass(DataSource.class, dataSourceRepository, dataSourceSearchRepository);
        reindexForClass(LookupCode.class, lookupCodeRepository, lookupCodeSearchRepository);
        reindexForClass(User.class, userRepository, userSearchRepository);
        reindexForClass(DataInvestmentManager.class, dataInvestmentManagersRepository , dataInvestmentMangerSearchRepository);
        reindexEmails();
        reindexEmail();
        reindexDomain();
        log.info("Elasticsearch: Successfully performed reindexing");
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public void reindexSingleProvider(long providerID) {
        DataProvider dp = dataProviderService.findOne(providerID);
        if(null!=dp)
        {
            dataProviderSearchRepository.save(dp);
        }
    }
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Async
    public void reindexArticles() {
        if(elasticsearchTemplate.indexExists(DataArticle.class))
        {
            elasticsearchTemplate.deleteIndex(DataArticle.class);
        }
        elasticsearchTemplate.createIndex(DataArticle.class);
        elasticsearchTemplate.putMapping(DataArticle.class);
        if (dataArticleRepository.count() > 0) try {
            List<Long> ids = dataArticleRepository.getAllIDs();
            for (long id : ids) {
                try {
                    DataArticle da = dataArticleRepository.findOneWithEagerRelationships(id);
                    dataArticleSearchRepository.save(da);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            log.error("Exception in findAllWithEagerRelationships for: " + DataArticle.class.getName());
        }
        log.info("Elasticsearch: Indexed all rows for " + DataArticle.class.getSimpleName());
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Async
    public void reindexEvents() {
        if(elasticsearchTemplate.indexExists(DataEvent.class))
        {
            elasticsearchTemplate.deleteIndex(DataEvent.class);
        }
        elasticsearchTemplate.createIndex(DataEvent.class);
        elasticsearchTemplate.putMapping(DataEvent.class);
        if (dataEventRepository.count() > 0) try {
            List<Long> ids = dataEventRepository.getAllIDs();
            for (long id : ids) {
                try {
                    DataEvent da = dataEventRepository.findOneWithEagerRelationships(id);
                    dataEventSearchRepository.save(da);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            log.error("Exception in findAllWithEagerRelationships for: " + DataEvent.class.getName());
        }
        log.info("Elasticsearch: Indexed all rows for " + DataEvent.class.getSimpleName());
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Async
    public void reindexProviders(boolean rebuildMapping) {
        if(rebuildMapping)
        {
            if(elasticsearchTemplate.indexExists(DataProvider.class))
            {
                elasticsearchTemplate.deleteIndex(DataProvider.class);
            }
            elasticsearchTemplate.createIndex(DataProvider.class);
            elasticsearchTemplate.putMapping(DataProvider.class);
//                String mainDataCategoryMappingJson = "{\n" +
//                    "  \"properties\": {\n" +
//                    "    \"mainDataCategory\": {\n" +
//                    "      \"properties\": {\n" +
//                    "        \"datacategory\": { \n" +
//                    "          \"type\": \"string\",\n" +
//                    "          \"fields\" :{\n" +
//                    "                    \"raw\" :{\n" +
//                    "                        \"type\": \"string\",\n" +
//                    "                        \"index\": \"not_analyzed\"\n" +
//                    "                    }\n" +
//                    "                }\n" +
//                    "        }\n" +
//                    "      }\n" +
//                    "    }\n" +
//                    "  }\n" +
//                    "}";
//                String dataIndustryMappingJson = "{\n" +
//                    "  \"properties\": \n" +
//                    "    {\n" +
//                    "      \"mainDataIndustry\": {\n" +
//                    "        \"properties\": {\n" +
//                    "          \"dataIndustry\": {\n" +
//                    "            \"type\": \"string\",\n" +
//                    "            \"fields\": {\n" +
//                    "              \"raw\": {\n" +
//                    "                \"type\": \"string\",\n" +
//                    "                \"index\": \"not_analyzed\"\n" +
//                    "              }\n" +
//                    "            }\n" +
//                    "          }\n" +
//                    "        }\n" +
//                    "      }\n" +
//                    "    }\n" +
//                    "}\n";
//                String providerNameMappingJson = "{\n" +
//                    "  \"properties\": {\n" +
//                    "    \"providerName\": {\n" +
//                    "      \"type\": \"string\",\n" +
//                    "      \"fields\": {\n" +
//                    "        \"raw\": {\n" +
//                    "          \"type\": \"string\", \n" +
//                    "          \"index\": \"not_analyzed\"\n" +
//                    "        }\n" +
//                    "      }\n" +
//                    "    }\n" +
//                    "  }\n" +
//                    "}";
//                client.execute(new PutMapping.Builder(INDEX_DATA_PROVIDER, INDEX_DATA_PROVIDER, mainDataCategoryMappingJson).build());
//                client.execute(new PutMapping.Builder(INDEX_DATA_PROVIDER, INDEX_DATA_PROVIDER, dataIndustryMappingJson).build());
//                client.execute(new PutMapping.Builder(INDEX_DATA_PROVIDER, INDEX_DATA_PROVIDER, providerNameMappingJson).build());
        }
        if (dataProviderRepository.count() > 0) try {
            List<Long> ids = dataProviderRepository.getAllIDs();
            for (long id : ids) {
                try {
                    DataProvider dp = dataProviderService.findOne(id);
                    if(null!=dp)
                    {
                        dataProviderSearchRepository.save(dp);
                    }
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            log.error("Exception in findAllWithEagerRelationships for: " + DataProvider.class.getName());
        }
        log.info("Elasticsearch: Indexed all rows for " + DataProvider.class.getSimpleName());
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Async
    public void reindexLookupCode(boolean rebuildMapping) {
        if(rebuildMapping)
        {
            if(elasticsearchTemplate.indexExists(LookupCode.class))
            {
                elasticsearchTemplate.deleteIndex(LookupCode.class);
            }
            elasticsearchTemplate.createIndex(LookupCode.class);
            elasticsearchTemplate.putMapping(LookupCode.class);
        }
        if (lookupCodeRepository.count() > 0) try {
            lookupCodeSearchRepository.save(lookupCodeRepository.findAll());
        } catch (Exception e) {
            log.error("Exception in reindex for: " + LookupCode.class.getName());
        }
        log.info("Elasticsearch: Indexed all rows for " + LookupCode.class.getSimpleName());
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    protected <T, ID extends Serializable> void reindexForClass(Class<T> entityClass, JpaRepository<T, ID> jpaRepository,
                                                                ElasticsearchRepository<T, ID> elasticsearchRepository) {
        if(elasticsearchTemplate.indexExists(entityClass))
        {
            elasticsearchTemplate.deleteIndex(entityClass);
        }
        try {
            elasticsearchTemplate.createIndex(entityClass);
        } catch (IndexAlreadyExistsException e) {
            // Do nothing. Index was already concurrently recreated by some other service.
        }
        elasticsearchTemplate.putMapping(entityClass);
        if (jpaRepository.count() > 0)
            try {

                Method m = jpaRepository.getClass().getMethod("findAllWithEagerRelationships");
                elasticsearchRepository.save((List<T>) m.invoke(jpaRepository));
            } catch (Exception e) {
                log.error("Exception in findAllWithEagerRelationships for: " + entityClass.getName() + e.getMessage());
                elasticsearchRepository.save(jpaRepository.findAll());
            }
        log.info("Elasticsearch: Indexed all rows for " + entityClass.getSimpleName());
    }

   /* @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Async
    public void reindexdataorganization() {
        if(elasticsearchTemplate.indexExists(DataOrganization.class))
        {
            elasticsearchTemplate.deleteIndex(DataOrganization.class);
        }
        elasticsearchTemplate.createIndex(DataOrganization.class);
        elasticsearchTemplate.putMapping(DataOrganization.class);
        if (dataOrganizationRepository.count() > 0) try {
            List<Long> ids = dataOrganizationRepository.getAllIDs();
            for (long id : ids) {
                try {
                	DataOrganization da = dataOrganizationRepository.findOneWithEagerRelationships(id);
                	dataOrganizationSearchRepository.save(da);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            log.error("Exception in findAllWithEagerRelationships for: " + DataOrganization.class.getName());
        }
        log.info("Elasticsearch: Indexed all rows for " + DataOrganization.class.getSimpleName());
    }*/


    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Async
    public void reindexdataorganization() {
        if(elasticsearchTemplate.indexExists(DataOrganization.class))
        {
            elasticsearchTemplate.deleteIndex(DataOrganization.class);
        }
        elasticsearchTemplate.createIndex(DataOrganization.class);
        elasticsearchTemplate.putMapping(DataOrganization.class);
        if (dataOrganizationRepository.count() > 0) try {
            try {
                dataOrganizationSearchRepository.save(dataOrganizationRepository.findAll());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }

        } catch (Exception e) {
            log.error("Exception in findAllWithEagerRelationships for: " + DataOrganization.class.getName());
        }
        log.info("Elasticsearch: Indexed all rows for " + DataOrganization.class.getSimpleName());
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public void reindexSingleResources(Long id) {
        // TODO Auto-generated method stub
        DataResources resources=dataResourceService.findOne(id);
        dataResourceSearchRepository.save(resources);

    }
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Async
    public void reindexDocumentMetaData() {

        if(elasticsearchTemplate.indexExists(DocumentsMetadata.class))
        {
            elasticsearchTemplate.deleteIndex(DocumentsMetadata.class);
        }
        elasticsearchTemplate.createIndex(DocumentsMetadata.class);
        elasticsearchTemplate.putMapping(DocumentsMetadata.class);
        if (documentsMetadataRepository.count() > 0) try {
            // System.out.println("!!!!called");
        	documentsMetadataSearchRepository.save(documentsMetadataRepository.findAll());
         } catch (Exception e) {
             log.error("Exception in reindex for: " + DocumentsMetadata.class.getName());
         }
         log.info("Elasticsearch: Indexed all rows for " + DocumentsMetadata.class.getSimpleName());
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Async
    public void reindexResources() {

        if(elasticsearchTemplate.indexExists(DataResources.class))
        {
            elasticsearchTemplate.deleteIndex(DataResources.class);
        }
        elasticsearchTemplate.createIndex(DataResources.class);
        elasticsearchTemplate.putMapping(DataResources.class);
        if (dataResourceRepository.count() > 0) try {
            List<Long> ids = dataResourceRepository.getAllIDs();
            for (long id : ids) {
                try {
                    DataResources dp = dataResourceService.findOne(id);
                    dataResourceSearchRepository.save(dp);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            log.error("Exception in findAllWithEagerRelationships for: " + DataResources.class.getName());
        }
        log.info("Elasticsearch: Indexed all rows for " + DataResources.class.getSimpleName());
    }


    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public void reindexSingleResource(long id) {
        DataResources dp = dataResourceService.findOne(id);
        dataResourceSearchRepository.save(dp);
    }
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Async
    public void reindexEmail() {

        if(elasticsearchTemplate.indexExists(EmailAddress.class))
        {
            elasticsearchTemplate.deleteIndex(EmailAddress.class);
        }
        elasticsearchTemplate.createIndex(EmailAddress.class);
        elasticsearchTemplate.putMapping(EmailAddress.class);

        if (emailAddressRepository.count() > 0) try {
           // System.out.println("!!!!called");
            emailAddressSearchRepository.save(emailAddressRepository.findAll());
        } catch (Exception e) {
            log.error("Exception in reindex for: " + EmailAddress.class.getName());
        }
        log.info("Elasticsearch: Indexed all rows for " + EmailAddress.class.getSimpleName());


    }


    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Async
    public void reindexDataTag() {
        reindexForClass(DataProviderTag.class, dataProviderTagRepository, dataProviderTagSearchRepository);

    }

	public void reindexInvestorManagers() {
		// TODO Auto-generated method stub
        reindexForClass(DataInvestmentManager.class, dataInvestmentManagersRepository, dataInvestmentMangerSearchRepository);

	}
	
	public void reindexEmails() {
		// TODO Auto-generated method stub
        reindexForClass(Emails.class, emailsRepository, emailsSearchRepository);

	}

	public void reindexDatasources() {
		// TODO Auto-generated method stub
        reindexForClass(DataSource.class, dataSourceRepository, dataSourceSearchRepository);
	}
	
	public void reindexDomains() {
		// TODO Auto-generated method stub
        reindexForClass(Domains.class, dataDomainsRepository, dataDomainSearchRepository);

	}
	 @Transactional(readOnly = true)
	    @SuppressWarnings("unchecked")
	    @Async
	    public void reindexDomain() {

	        if(elasticsearchTemplate.indexExists(Domains.class))
	        {
	            elasticsearchTemplate.deleteIndex(Domains.class);
	        }
	        try {
	            elasticsearchTemplate.createIndex(Domains.class);
	        } catch (IndexAlreadyExistsException e) {
	            // Do nothing. Index was already concurrently recreated by some other service.
	        }	        elasticsearchTemplate.putMapping(Domains.class);
	        if (dataResourceRepository.count() > 0) try {
	            List<Long> ids = dataDomainsRepository.getAllIDs();
	            for (long id : ids) {
	                try {
	                	Domains dp = dataDomainsRepository.findOneByDomain(id);
	                	dataDomainSearchRepository.save(dp);
	                } catch (Exception e) {
	                    log.error(e.getMessage(), e);
	                }
	            }
	        } catch (Exception e) {
	            log.error("Exception in findAllWithEagerRelationships for: " + Domains.class.getName());
	        }
	        log.info("Elasticsearch: Indexed all rows for " + Domains.class.getSimpleName());
	    }
}
