package com.ai.mp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ai.mp.domain.DataArticle;
import com.ai.mp.domain.DataLinks;
import com.ai.mp.repository.DataLinksRepository;
import com.ai.mp.repository.search.DataLinkSearchRepository;

@Service
@Transactional
public class DataLinkService {
	
    private final Logger log = LoggerFactory.getLogger(DataLinkService.class);
	
private final DataLinksRepository dataLinksRepository;
private final DataLinkSearchRepository dataLinkSearchRepository;
    public DataLinkService(DataLinksRepository dataLinksRepository,DataLinkSearchRepository dataLinkSearchRepository)
    {
    	this.dataLinksRepository=dataLinksRepository;
    	this.dataLinkSearchRepository=dataLinkSearchRepository;
    }
    
	public DataLinks save(DataLinks dataLinks) {
		// TODO Auto-generated method stub
        log.debug("Request to save DataLinks : {}", dataLinks);
        DataLinks result = dataLinksRepository.save(dataLinks);
       // dataLinkSearchRepository.save(result);
		return result;
	}
	  @Transactional(readOnly = true)
	public Page<DataLinks> searchAll(Pageable pageable) {
		// TODO Auto-generated method stub
	        log.debug("Request to get a page of DataArticles");
	        Page<DataLinks> result = dataLinkSearchRepository.findAll(pageable);
		return result;
	}
	    @Transactional(readOnly = true)
	public DataLinks findOne(Long id) {
		// TODO Auto-generated method stub
	        log.debug("Request to get DataLinks : {}", id);

		return dataLinksRepository.findOne(id);
	}

		public void delete(Long id) {
			// TODO Auto-generated method stub
	        log.debug("Request to delete DataLinks : {}", id);
	        dataLinksRepository.delete(id);
	        dataLinkSearchRepository.delete(id);
		}

}
