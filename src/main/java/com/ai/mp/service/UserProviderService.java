package com.ai.mp.service;

import static com.ai.core.Constants.INTEREST_TYPE_CANCEL_MORE_INFO;
import static com.ai.core.Constants.INTEREST_TYPE_DATA_SAMPLE;
import static com.ai.core.Constants.INTEREST_TYPE_DIRECT_CONTACT_PROVIDER;
import static com.ai.core.Constants.INTEREST_TYPE_EXPAND_PENDING;
import static com.ai.core.Constants.INTEREST_TYPE_EXPAND_PENDING_USER_CONFIRM;
import static com.ai.core.Constants.INTEREST_TYPE_FOLLOW;
import static com.ai.core.Constants.INTEREST_TYPE_GENERAL_INQUIRY;
import static com.ai.core.Constants.INTEREST_TYPE_MORE_INFO;
import static com.ai.core.Constants.INTEREST_TYPE_OPERATOR;
import static com.ai.core.Constants.INTEREST_TYPE_PRESS;
import static com.ai.core.Constants.INTEREST_TYPE_TECH_SUPPORT;
import static com.ai.core.Constants.INTEREST_TYPE_UNLOCK;
import static com.ai.core.Constants.INTEREST_TYPE_UNLOCK_DENY;
import static com.ai.core.Constants.INTEREST_TYPE_UNLOCK_NEED_MORE_INFO;
import static com.ai.core.Constants.INTEREST_TYPE_UNLOCK_PENDING;
import static com.ai.core.Constants.INTEREST_TYPE_UNLOCK_PENDING_USER_CONFIRM;
import static com.ai.core.Constants.REQUEST_HELP_ENGINEERING_DATA;
import static com.ai.core.Constants.REQUEST_HELP_FINDING_DATA;
import static com.ai.core.Constants.REQUEST_HELP_SELLING_DATA;
import static com.ai.core.Constants.ROLE_INVESTOR;
import static com.ai.core.Constants.ROLE_PROVIDER;
import static com.ai.mp.security.AuthoritiesConstants.USER;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ai.core.StrUtils;
import com.ai.mp.config.ApplicationProperties;
import com.ai.mp.domain.Authority;
import com.ai.mp.domain.DataComment;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.DataProviderTag;
import com.ai.mp.domain.Domains;
import com.ai.mp.domain.User;
import com.ai.mp.domain.UserDataCategory;
import com.ai.mp.domain.UserProvider;
import com.ai.mp.domain.UserRequest;
import com.ai.mp.repository.AuthorityRepository;
import com.ai.mp.repository.DataDomainsRepository;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.repository.DataProviderTagRepository;
import com.ai.mp.repository.UserDataCategoryRepository;
import com.ai.mp.repository.UserProviderRepository;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.repository.UserRequestRepository;
import com.ai.mp.repository.search.DataProviderSearchRepository;
import com.ai.mp.utils.RandomUtil;
import com.ai.util.Utils;

import io.github.jhipster.config.JHipsterProperties;

@Service
@Transactional(readOnly=false)
public class UserProviderService {

	 @PersistenceContext
	    EntityManager entityManager;

    private final UserProviderRepository userProviderRepository;
    private final UserRepository userRepository;
    private final DataProviderSearchRepository dataProviderSearchRepository;
    private final UserService userService;
    private final MailService mailService;
    private final JHipsterProperties jHipsterProperties;
    private final UserRequestRepository userRequestRepository;
    private final UserDataCategoryRepository userDataCategoryRepository;
    private final ApplicationProperties applicationProperties;
    private final DataProviderTagRepository dataProviderTagRepository;
    private final DataProviderRepository dataProviderRepository;
    private final DataDomainsRepository dataDomainsRepository;
    private final AuthorityRepository authorityRepository;

    public UserProviderService(UserProviderRepository userProviderRepository, UserRepository userRepository, DataProviderSearchRepository dataProviderSearchRepository, UserService userService, MailService mailService, JHipsterProperties jHipsterProperties, UserRequestRepository userRequestRepository, UserDataCategoryRepository userDataCategoryRepository,ApplicationProperties applicationProperties,DataProviderTagRepository dataProviderTagRepository,DataProviderRepository dataProviderRepository,DataDomainsRepository dataDomainsRepository
    		,AuthorityRepository authorityRepository) {
        this.userProviderRepository = userProviderRepository;
        this.userRepository = userRepository;
        this.dataProviderSearchRepository = dataProviderSearchRepository;
        this.userService = userService;
        this.mailService = mailService;
        this.jHipsterProperties = jHipsterProperties;
        this.userRequestRepository = userRequestRepository;
        this.userDataCategoryRepository = userDataCategoryRepository;
        this.applicationProperties=applicationProperties;
        this.dataProviderTagRepository = dataProviderTagRepository;
        this.dataProviderRepository = dataProviderRepository;
        this.dataDomainsRepository=dataDomainsRepository;
        this.authorityRepository=authorityRepository;
    }

    public void followProvider(String key, boolean toggle) {
        UserProvider udp = userProviderRepository.findOneByDeletionKeyEquals(key);
        if (udp != null) {
            String deletionKey = RandomUtil.generateActivationKey();
            User user = userRepository.findOneByIdEquals(udp.getUserID());
            DataProvider provider = dataProviderSearchRepository.findOne(udp.getProviderID());
            saveProviderInterest(provider, INTEREST_TYPE_FOLLOW, user, deletionKey, toggle, udp);
            mailService.sendAdminEmailFollowProvider(user, provider, !toggle);
        }
    }

    public void followProvider(long providerID) {
        User user = userService.getUser();
        DataProvider provider = dataProviderSearchRepository.findOne(providerID);
        String deletionKey = RandomUtil.generateActivationKey();
        UserProvider up = userProviderRepository.findOneByUserIDEqualsAndProviderIDEqualsAndInterestTypeEquals(user.getId(), providerID, INTEREST_TYPE_FOLLOW);
        if (up != null) {
            if (up.getInactive()) {
                up.setInactive(false);
                saveProviderInterest(provider, INTEREST_TYPE_FOLLOW, user, deletionKey, false, up);
                mailService.sendFollowProviderEmail(user, provider, deletionKey);
                mailService.sendAdminEmailFollowProvider(user, provider, true);
            } else {
                up.setInactive(true);
                saveProviderInterest(provider, INTEREST_TYPE_FOLLOW, user, deletionKey, true, up);
                mailService.sendUnfollowProviderEmail(user, provider, deletionKey);
                mailService.sendAdminEmailFollowProvider(user, provider, false);
            }
        } else {
            saveProviderInterest(provider, INTEREST_TYPE_FOLLOW, user, deletionKey, false, null);
            mailService.sendFollowProviderEmail(user, provider, deletionKey);
            mailService.sendAdminEmailFollowProvider(user, provider, true);
        }
    }

    private void saveProviderInterest(DataProvider provider, int interestType, User user, String deletionKey, boolean inactive, UserProvider userProvider) {
        
    	
    	if (userProvider == null) {
            userProvider = new UserProvider();
        }
        userProvider.setProviderRecordID(provider.getRecordID());
        userProvider.setProviderID(provider.getId());
        userProvider.setUserID(user.getId());
        userProvider.setInterestType(interestType);
        userProvider.setCreatedDate(LocalDateTime.now());

        if (!StrUtils.isBlank(deletionKey)) {
            userProvider.setDeletionKey(deletionKey);
        }

        userProvider.setInactive(inactive);
        userProviderRepository.save(userProvider);
    }

    public List<Long> getProvidersByInterestTypeForCurrentUser(int interestType) {
        return userProviderRepository.findAllByUserIDEqualsAndInterestTypeEquals(userService.getUserWithAuthorities().getId(), interestType);
    }

    public List<Long> getProviderIDsByInterestTypeByUser(long userID, int interestType) {
        return userProviderRepository.findAllByUserIDEqualsAndInterestTypeEquals(userID, interestType);
    }
    public List<UserProvider> getProviderIDByInterestTypeByUser() {
        User user = userService.getUser();
        return userProviderRepository.findAllByUserIDsEqualsAndInterestTypeEquals(user.getId(), INTEREST_TYPE_OPERATOR);
    }
    public List<UserProvider> getProviderIDByInterestTypeByUsers(Long id) {
        return userProviderRepository.findAllByUserIDsEqualsAndInterestTypeEquals(id, INTEREST_TYPE_OPERATOR);
    }
    public List<DataProvider> getProvidersByInterestTypeByUser(long userID, int interestType) {
        return userProviderRepository.findAllProvidersByUserIDEqualsAndInterestTypeEquals(userID, interestType);
    }

    public List<Long> getProvidersByInterestType(int interestType) {
        return userProviderRepository.findAllByInterestTypeEquals(interestType);
    }
    public void tagProviders(){
        List<User> userList = userRepository.findByActivatedIsTrue();

        for(User user:userList){
            String record1="rec0"+ Utils.randomString(13,13);
            String record2="rec0"+ Utils.randomString(13,13);
            String record3="rec0"+ Utils.randomString(13,13);
            String record4="rec0"+ Utils.randomString(13,13);
            String record5="rec0"+ Utils.randomString(13,13);
            String record6="rec0"+ Utils.randomString(13,13);
            String record7="rec0"+ Utils.randomString(13,13);
            int result = userProviderRepository.insetTagUser(record1,record2,record3,record4,record5,record6,record7,user.getId());

        }
        System.out.println("All Record Updated");

    }
    public void processNeedMoreInfoForUnlock(String key) {
        UserProvider udp = userProviderRepository.findOneByDeletionKeyEquals(key);
        User user = userService.getUser(udp.getUserID());
        UserRequest ur = userRequestRepository.findFirstByUserIdEqualsAndProviderIDEqualsAndRequestTypeEqualsOrderByCreatedDateDesc(user.getId(), udp.getProviderID(), INTEREST_TYPE_UNLOCK_PENDING);
        if (udp != null) {
            udp.setInterestType(INTEREST_TYPE_UNLOCK_NEED_MORE_INFO);
            userProviderRepository.save(udp);
            StringBuffer content = new StringBuffer();
            content.append("Dear ")
                .append(user.getFirstName())
                .append(",<p>You recently requested ")
                .append("<a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(udp.getProviderRecordID())
                .append("'>")
                .append("this provider's profile</a> to be unlocked")
                .append(" with the associated message:<br>")
                .append(ur.getMessage())
                .append("</p><p>In order to best serve you, we would like to understand more about why you requested this provider and what data you are searching for.<br>Specifically, please answer a couple questions for us:<br>")
                .append("1. Why did you request this particular provider profile to be unlocked?<br>")
                .append("2. Are there particular data categories, geographies, or other requirements for data that you are seeking?</p>")
                .append("<p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");

            mailService.sendEmail(user.getEmail(), "Requesting More Details Regarding Your Unlock Request", content.toString(), false, true, user);
        }
    }

    public void approveExpandProviderRequest(String key) {

    }

    public void rejectUnlockProviderRequest(String key) {
        UserProvider udp = userProviderRepository.findOneByDeletionKeyEquals(key);
        if (udp != null) {
            udp.setInterestType(INTEREST_TYPE_UNLOCK_DENY);
            udp.setUpdatedDate(Instant.now());
            userProviderRepository.save(udp);
        }
    }

    public void approveUnlockProviderRequest(String key) {
        UserProvider udp = userProviderRepository.findOneByDeletionKeyEquals(key);
        DataProvider provider = dataProviderSearchRepository.findOne(udp.getProviderID());
        User user = userService.getUser(udp.getUserID());
        if (udp != null) {
            udp.setInterestType(INTEREST_TYPE_UNLOCK);
            udp.setUpdatedDate(Instant.now());
            userProviderRepository.save(udp);
            StringBuffer content = new StringBuffer();
            content.append("Dear ")
                .append(user.getFirstName())
                .append(",<p>Your recent request to unlock ")
                .append(provider.getProviderName())
                .append(" has been granted!<br>Please <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(udp.getProviderRecordID())
                .append("'>click here</a> to learn more about this provider or copy and paste the following link into your web browser:<br>")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(udp.getProviderRecordID())
                .append("</p>")
                .append("<p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");
            mailService.sendEmail(user.getEmail(), "Provider Unlock Request Granted for " + provider.getProviderName(), content.toString(), false, true, user);
        }
    }

    public List<UserProvider> findAllProviderInterestsByUserID() {
        return userProviderRepository.findAllByUserIDEqualsAndInactiveIsFalse(userService.getCurrentUserID());
    }

    public void cancelProcessProviderMoreInfo(long providerID,int interestType) {
        long userID = userService.getCurrentUserID();
        UserProvider udp = userProviderRepository.findOneByUserIDEqualsAndProviderIDEqualsAndInterestTypeEquals(userID, providerID, interestType);
        cancelProcessProviderMoreInfo(udp);
    }

    public void cancelProcessProviderMoreInfo(String key) {
        UserProvider udp = userProviderRepository.findOneByDeletionKeyEquals(key);
        cancelProcessProviderMoreInfo(udp);
    }

    private void cancelProcessProviderMoreInfo(UserProvider udp) {
        if (udp == null)
            return;
        String key = udp.getDeletionKey();
        User user = udp.getUser();
        DataProvider provider = dataProviderSearchRepository.findOne(udp.getProviderID());
        int request = udp.getInterestType();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date cdate = new Date();
       // if (request == 261) {
        if (request == applicationProperties.getInterestTypeMoreInfo()) {
            udp.setInterestType(INTEREST_TYPE_CANCEL_MORE_INFO);
            udp.setUpdatedDate(Instant.now());
            userProviderRepository.save(udp);
            StringBuffer content = new StringBuffer();
            content.append("Dear ")
                .append(user.getFirstName())
                .append(",<p>You have cancelled your request for more information about <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(udp.getProviderRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a></p>")
                .append("If you have cancelled this by mistake or would still like to receive more information about this Provider,<a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("/#/email-action?api=resubmit-contact-us-provider-more-info&key=")
                .append(key)
                .append("'>click here</a> or copy and paste the following link into your browser:<br>")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/email-action?api=resubmit-contact-us-provider-more-info&key=")
                .append(key)
                .append("<p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");
            mailService.sendEmail(user.getEmail(), "Request for More Information about  " + provider.getProviderName(), content.toString(), false, true, user);
            StringBuffer adminContent = new StringBuffer();
            INTEREST_TYPE_MORE_INFO=applicationProperties.getInterestTypeMoreInfo();
            UserRequest ur = userRequestRepository.findFirstByUserIdEqualsAndProviderIDEqualsAndRequestTypeEqualsOrderByCreatedDateDesc(user.getId(), udp.getProviderID(), INTEREST_TYPE_MORE_INFO);

            adminContent.append("<p>More Information Request Details:<p>")
                .append("Original Request Message: ")
                .append(ur.getMessage())
                .append("<br>Date Request Made: ")
                .append(cdate)
                .append("<br>Current Status of Request: Cancelled")
                .append("<br><br><b>User: </b>")
                .append("<br>Name: ")
                .append(user.getFirstName() + " " + user.getLastName())
                .append("<br>Email: ")
                .append(user.getEmail());
            if (user.getCompany() != null) {
                adminContent.append("<br>Company: ")
                    .append(user.getCompany());
            }
            adminContent.append("<br><br><b>Provider: </b>")
                .append("<br>Name: <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(provider.getRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a><br>Website: ")
                .append(provider.getWebsite())
                .append("<br>Record ID: ")
                .append(provider.getRecordID())
                .append("</p>");
            mailService.sendAdminEmail(user.getFirstName() + " " + user.getLastName() + " cancelled request for more information about " + provider.getProviderName(), adminContent.toString(), false, true);
        } else if (request == 565) {
           // udp.setInterestType(INTEREST_TYPE_CANCEL_DATA_SAMPLE);
          udp.setInterestType(applicationProperties.getCancelDataSample());
            udp.setUpdatedDate(Instant.now());
            userProviderRepository.save(udp);
            StringBuffer content = new StringBuffer();
            content.append("Dear ")
                .append(user.getFirstName())
                .append(",<p>You have cancelled your request for a data sample from <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(udp.getProviderRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a></p>")
                .append("If you have cancelled this by mistake or would still like to receive a data sample from the Provider,<a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("/#/email-action?api=resubmit-contact-us-provider-more-info&key=")
                .append(key)
                .append("'>click here</a> or copy and paste the following link into your browser:<br>")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/email-action?api=resubmit-contact-us-provider-more-info&key=")
                .append(key)
                .append("<p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");
            mailService.sendEmail(user.getEmail(), "Request for a Data Sample from  " + provider.getProviderName(), content.toString(), false, true, user);
            StringBuffer adminContent = new StringBuffer();

            UserRequest ur = userRequestRepository.findFirstByUserIdEqualsAndProviderIDEqualsAndRequestTypeEqualsOrderByCreatedDateDesc(user.getId(), udp.getProviderID(), INTEREST_TYPE_DATA_SAMPLE);

            adminContent.append("<p>Data Sample Request Details:<p>")
                .append("Original Request Message: ")
                .append(ur.getMessage())
                .append("<br>Date Request Made: ")
                .append(cdate)
                .append("<br>Current Status of Request: Cancelled")
                .append("<br><br><b>User: </b>")
                .append("<br>Name: ")
                .append(user.getFirstName() + " " + user.getLastName())
                .append("<br>Email: ")
                .append(user.getEmail());
            if (user.getCompany() != null) {
                adminContent.append("<br>Company: ")
                    .append(user.getCompany());
            }
            adminContent.append("<br><br><b>Provider: </b>")
                .append("<br>Name: <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(provider.getRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a><br>Website: ")
                .append(provider.getWebsite())
                .append("<br>Record ID: ")
                .append(provider.getRecordID())
                .append("</p>");
            mailService.sendAdminEmail(user.getFirstName() + " " + user.getLastName() + " cancelled request for a data sample from " + provider.getProviderName(), adminContent.toString(), false, true);
        }else
        {
            //udp.setInterestType(INTEREST_TYPE_CANCEL_DIRECT_CONTACT_PROVIDER);
           udp.setInterestType(applicationProperties.getCancelDirectContactProvider());
            udp.setUpdatedDate(Instant.now());
            userProviderRepository.save(udp);
            StringBuffer content = new StringBuffer();
            content.append("Dear ")
                .append(user.getFirstName())
                .append(",<p>You have cancelled your request for direct contact with <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(udp.getProviderRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a></p>")
                .append("If you have cancelled this by mistake or would still like to have direct contact with this Provider,<a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("/#/email-action?api=resubmit-contact-us-provider-more-info&key=")
                .append(key)
                .append("'>click here</a> or copy and paste the following link into your browser:<br>")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/email-action?api=resubmit-contact-us-provider-more-info&key=")
                .append(key)
                .append("<p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");
            mailService.sendEmail(user.getEmail(), "Request for Direct Contact with  " + provider.getProviderName(), content.toString(), false, true, user);
            StringBuffer adminContent = new StringBuffer();

            UserRequest ur = userRequestRepository.findFirstByUserIdEqualsAndProviderIDEqualsAndRequestTypeEqualsOrderByCreatedDateDesc(user.getId(), udp.getProviderID(), INTEREST_TYPE_DIRECT_CONTACT_PROVIDER);

            adminContent.append("<p>Direct Contact Request Details:<p>")
                .append("Original Request Message: ")
                .append(ur.getMessage())
                .append("<br>Date Request Made: ")
                .append(cdate)
                .append("<br>Current Status of Request: Cancelled")
                .append("<br><br><b>User: </b>")
                .append("<br>Name: ")
                .append(user.getFirstName() + " " + user.getLastName())
                .append("<br>Email: ")
                .append(user.getEmail());
            if (user.getCompany() != null) {
                adminContent.append("<br>Company: ")
                    .append(user.getCompany());
            }
            adminContent.append("<br><br><b>Provider: </b>")
                .append("<br>Name: <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(provider.getRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a><br>Website: ")
                .append(provider.getWebsite())
                .append("<br>Record ID: ")
                .append(provider.getRecordID())
                .append("</p>");

            mailService.sendAdminEmail(user.getFirstName() + " " + user.getLastName() + " cancelled request for direct contact with " + provider.getProviderName(), adminContent.toString(), false, true);
        }
    }


    public void processProviderUnlock(UserRequest userRequest) {
    	if(applicationProperties.getInterestTypeUnlockPending()!=null)
    	{
    		INTEREST_TYPE_UNLOCK_PENDING=applicationProperties.getInterestTypeUnlockPending();
    	}
        int interestType = INTEREST_TYPE_UNLOCK_PENDING;
        User user = userService.getUser();
        DataProvider provider = dataProviderSearchRepository.findOne(userRequest.getProviderID());
        String deletionKey = RandomUtil.generateActivationKey();
        saveProviderInterest(provider, interestType, user, deletionKey, false, null);
        if (interestType == INTEREST_TYPE_UNLOCK_PENDING || interestType == INTEREST_TYPE_EXPAND_PENDING) {
            StringBuffer content = new StringBuffer();
            String adminSubject, userName;
            if (interestType == INTEREST_TYPE_UNLOCK_PENDING) {
                content.append("Dear " + user.getFirstName() + ",")
                    .append("<p>We have received your request to access ")
                    .append("<a href='")
                    .append(jHipsterProperties.getMail().getBaseUrl())
                    .append("#/providers/")
                    .append(provider.getRecordID())
                    .append("'>")
                    .append("the provider</a>")
                    .append(" and the associated message:<br>")
                    .append(userRequest.getMessage())
                    .append("</p>We are reviewing your request and will let you know the next steps within a few days.")
                    .append("<br>In the meantime, please respond to this email if you have any more details about why you requested this Provider or about the data you are looking to find.")
                    .append("<p><em>Regards,<br>")
                    .append("Amass Insights Team</em></p>");

                mailService.sendEmail(user.getEmail(), "Insights Data Profile Access Request", content.toString(), false, true, user);
            }
            userRequest.setRequestTopic("Request Access to Provider");
            userName = saveUserRequest(userRequest, interestType);

            if (!userName.equalsIgnoreCase(user.getLastName())) {
                userName = userName + " " + user.getLastName();
            }

            adminSubject = userName + " requested Profile unlock for: " + provider.getProviderName();

            content = new StringBuffer();
            content.append("<p>Details of Unlock Request:</p>")
                .append("Message:")
                .append(userRequest.getMessage())
                .append("<p><b>User Information</b>")
                .append("<br>Name: " + userName)
                .append("<br>Email: " + user.getEmail());
            if (user.getCompany() != null) {
                content.append("<br>Company Name:")
                    .append(user.getCompany());
            }
            content.append("</p><p><b>Provider Information</b><br>Provider Name: <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(provider.getRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a><br>Short Description: ")
                .append(provider.getShortDescription())
                .append("</p><br>")
                .append("MORE INFO: ")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("/#/email-action?api=admin-request-more-info&key=")
                .append(deletionKey)
                .append(" to ask user for more information regarding this request.<br>")
                .append("APPROVE: ")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("/#/email-action?api=approve-unlock-provider&key=")
                .append(deletionKey)
                .append(" to grant this request.<br>")
                .append("DENY: ")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("/#/email-action?api=reject-unlock-provider&key=")
                .append(deletionKey)
                .append(" to deny this request.");
            mailService.sendAdminEmail(adminSubject, content.toString(), false, true);
        }
    }

    public void processProviderMoreInfo(UserRequest userRequest) {

        processProviderMoreInfo(userRequest, null);
    }

    public void requestProviderMoreInfo(UserRequest userRequest) {
        requestProviderMoreInfo(userRequest, null);
    }

    public void processProviderMoreInfo(UserRequest userRequest, User user) {

        if (user == null) {
            user = userService.getUser();
        }
        DataProvider provider = dataProviderSearchRepository.findOne(userRequest.getProviderID());
        String deletionKey = RandomUtil.generateActivationKey();
        UserProvider up = userProviderRepository.findOneByUserIDEqualsAndProviderIDEqualsAndInterestTypeEquals(user.getId(), provider.getId(), INTEREST_TYPE_CANCEL_MORE_INFO);
     // System.out.println(applicationProperties.getInterestTypeMoreInfo());
        if(applicationProperties.getInterestTypeMoreInfo()!=null)
       {
    	   INTEREST_TYPE_MORE_INFO=applicationProperties.getInterestTypeMoreInfo();
       }
        if(userRequest.getMessage().equalsIgnoreCase("I'd like more information about this data provider"))
        {
            userRequest.setRequestType(INTEREST_TYPE_MORE_INFO);

        }
        int request = userRequest.getRequestType();
        userRequest.setRequestType(INTEREST_TYPE_MORE_INFO);
        if (request == INTEREST_TYPE_MORE_INFO) {
            userRequest.setRequestTopic("Need more info on Provider");
            saveProviderInterest(provider, INTEREST_TYPE_MORE_INFO, user, deletionKey, false, up);
            StringBuffer content = new StringBuffer();
            String adminSubject, userName;
            content.append("Dear " + user.getFirstName() + ",")
                .append("<p>We have received your request for more information about  ")
                .append("<a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(provider.getRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a> and the associated message:<br>")
                .append(userRequest.getMessage())
                .append("</p>Your request has been added to our queue and we will begin gathering more details about this Provider immediately.<br> When this process is finished, normally within about one week's time, you will receive a notification that the Provider's Data Profile has been updated.</p>")
                .append("<p>Please respond to this email if you have any more specific details about what you're looking to learn about this Provider.</p>")
                .append("<p>If you no longer have a need for this information or you would like to cancel this request, <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("/#/email-action?api=cancel-contact-us-provider-more-info&key=")
                .append(deletionKey)
                .append("'>click here</a> or copy and paste the following link into your browser:<br>")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("/#/email-action?api=cancel-contact-us-provider-more-info&key=")
                .append(deletionKey)
                .append("</p><p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");

            mailService.sendEmail(user.getEmail(), " Request for More Information about " + provider.getProviderName(), content.toString(), false, true, user);

            userName = saveUserRequest(userRequest, INTEREST_TYPE_MORE_INFO);

            if (!userName.equalsIgnoreCase(user.getLastName())) {
                userName = user.getFirstName() + " " + user.getLastName();
            }

            adminSubject = userName + " requested more information about " + provider.getProviderName();

            content = new StringBuffer();
            content.append("<p>More Information Request Details:</p>")
                .append("Message: ")
                .append(userRequest.getMessage())
                .append("<p><b>User Details</b>")
                .append("<br>Name: " + userName)
                .append("<br>Email: " + user.getEmail());
            if (user.getCompany() != null) {
                content.append("<br>Company Name: ")
                    .append(user.getCompany());
            }
            content.append("</p><p><b>Provider Details</b><br>Name: <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(provider.getRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a><br>Website: ")
                .append(provider.getWebsite())
                .append("<br>Record ID: ")
                .append(provider.getRecordID())
                .append("</p>");
            mailService.sendAdminEmail(adminSubject, content.toString(), false, true);
        }else if (request == 565) {
            userRequest.setRequestTopic("Data Sample Request from a Provider");
        	UserProvider udp = userProviderRepository.findOneByUserIDEqualsAndProviderIDEqualsAndInterestTypeEquals(user.getId(), provider.getId(), applicationProperties.getCancelDataSample());
            saveProviderInterest(provider, INTEREST_TYPE_DATA_SAMPLE, user, deletionKey, false, udp);
            StringBuffer content = new StringBuffer();
            String adminSubject, userName;
            content.append("Dear " + user.getFirstName() + ",")
                .append("<p>We have received your request for a data sample from  ")
                .append("<a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(provider.getRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a> and the associated message:<br>")
                .append(userRequest.getMessage())
                .append("</p>Your request has been received by the Amass team and will be addressed soon. When this process is finished, normally within about one week's time, you will receive a notification an update on your request and the data sample if available.</p>")
                .append("<p>Please respond to this email if you have any more specific details about what you're looking to learn about this Provider.</p>")
                .append("<p>If you no longer have a need for this information or you would like to cancel this request, <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("/#/email-action?api=cancel-contact-us-provider-more-info&key=")
                .append(deletionKey)
                .append("'>click here</a> or copy and paste the following link into your browser:<br>")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("/#/email-action?api=cancel-contact-us-provider-more-info&key=")
                .append(deletionKey)
                .append("</p><p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");

            mailService.sendEmail(user.getEmail(), " Request a Data Sample from " + provider.getProviderName(), content.toString(), false, true, user);

            userName = saveUserRequest(userRequest, INTEREST_TYPE_DATA_SAMPLE);

            if (!userName.equalsIgnoreCase(user.getLastName())) {
                userName = user.getFirstName() + " " + user.getLastName();
            }

            adminSubject = userName + " requested a data sample from " + provider.getProviderName();

            content = new StringBuffer();
            content.append("<p>Data Sample Request Details:</p>")
                .append("Message: ")
                .append(userRequest.getMessage())
                .append("<p><b>User Details</b>")
                .append("<br>Name: " + userName)
                .append("<br>Email: " + user.getEmail());
            if (user.getCompany() != null) {
                content.append("<br>Company Name: ")
                    .append(user.getCompany());
            }
            content.append("</p><p><b>Provider Details</b><br>Name: <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(provider.getRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a><br>Website: ")
                .append(provider.getWebsite())
                .append("<br>Record ID: ")
                .append(provider.getRecordID())
                .append("</p>");
            mailService.sendAdminEmail(adminSubject, content.toString(), false, true);
        }else{
            userRequest.setRequestTopic("Request Direct Contact with a Provider");
        	UserProvider udp = userProviderRepository.findOneByUserIDEqualsAndProviderIDEqualsAndInterestTypeEquals(user.getId(), provider.getId(), applicationProperties.getCancelDirectContactProvider());
            saveProviderInterest(provider, INTEREST_TYPE_DIRECT_CONTACT_PROVIDER, user, deletionKey, false, udp);
            StringBuffer content = new StringBuffer();
            String adminSubject, userName;
            content.append("Dear " + user.getFirstName() + ",")
                .append("<p>We have received your request for direct contact with  ")
                .append("<a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(provider.getRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a> and the associated message:<br>")
                .append(userRequest.getMessage())
                .append("</p>Your request has been added to our queue and we will attempt to connect you directly to the Provider immediately.</p>")
                .append("<p>Please respond to this email if you have any more specific details about what you're looking to learn about this Provider.</p>")
                .append("<p>If you no longer have a need for direct contact or you would like to cancel this request, <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("/#/email-action?api=cancel-contact-us-provider-more-info&key=")
                .append(deletionKey)
                .append("'>click here</a> or copy and paste the following link into your browser:<br>")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("/#/email-action?api=cancel-contact-us-provider-more-info&key=")
                .append(deletionKey)
                .append("</p><p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");

            mailService.sendEmail(user.getEmail(), " Request for Direct Contact with " + provider.getProviderName(), content.toString(), false, true, user);

            userName = saveUserRequest(userRequest, INTEREST_TYPE_DIRECT_CONTACT_PROVIDER);

            if (!userName.equalsIgnoreCase(user.getLastName())) {
                userName = user.getFirstName() + " " + user.getLastName();
            }

            adminSubject = userName + " requested direct contact with " + provider.getProviderName();

            content = new StringBuffer();
            content.append("<p>Direct Contact Request Details:</p>")
                .append("Message: ")
                .append(userRequest.getMessage())
                .append("<p><b>User Details</b>")
                .append("<br>Name: " + userName)
                .append("<br>Email: " + user.getEmail());
            if (user.getCompany() != null) {
                content.append("<br>Company Name: ")
                    .append(user.getCompany());
            }
            content.append("</p><p><b>Provider Details</b><br>Name: <a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(provider.getRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a><br>Website: ")
                .append(provider.getWebsite())
                .append("<br>Record ID: ")
                .append(provider.getRecordID())
                .append("</p>");
            mailService.sendAdminEmail(adminSubject, content.toString(), false, true);
        }
    }

    public void requestProviderMoreInfo(UserRequest userRequest, User user) {

        if (user == null) {
            user = userService.getUser();
        }
        //DataProvider provider = dataProviderSearchRepository.findOne(userRequest.getProviderID());
        // String deletionKey = RandomUtil.generateActivationKey();
        //  UserProvider up = userProviderRepository.findOneByUserIDEqualsAndProviderIDEqualsAndInterestTypeEquals(user.getId(), provider.getId(), INTEREST_TYPE_CANCEL_MORE_INFO);
        // saveProviderInterest(null, REQUEST_HELP_FINDING_DATA, user, deletionKey, false, up);
        StringBuffer content = new StringBuffer();
        String adminSubject, userName, userSubject;
        int request = userRequest.getRequestType();
        if (request == 1) {
            userRequest.setRequestTopic("Request help Finding Data");
            userName = saveUserRequest(userRequest, REQUEST_HELP_FINDING_DATA);
            adminSubject = " Data Request Submitted by " + userName;
            content.append("<p>The Data Request form has been submitted. Here are the details:</p>")

                .append("Name: " + userName)
                .append("<br>Email: " + user.getEmail());
            if (user.getCompany() != null) {
                content.append("<br>Company Name:")
                    .append(user.getCompany());
            }
            content.append("<br>Message: ")
                .append(userRequest.getMessage());
            mailService.sendRequestAdminEmail(adminSubject, content.toString(), false, true);

            userSubject = " Your RFD from Amass Insights ";
            content = new StringBuffer();
            content.append("Dear " + userName + ",")
                .append("<p>Thank you for requesting data from Amass Insights. We will review your message and get back to you soon.</p>")
                .append("<p>The details of your communication are below:</p> ")
                .append("<p>Message: </p>")
                .append(userRequest.getMessage());

            content.append("<br><p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");
            mailService.sendEmail(user.getEmail(), " Your RFD from Amass Insights ", content.toString(), false, true, user);
        } else if (request == 2) {
            userRequest.setRequestTopic("Request help Engineering Data");
            userName = saveUserRequest(userRequest, REQUEST_HELP_ENGINEERING_DATA);
            adminSubject = " Data Engineering Request (RFQ) Submitted by " + userName;
            content.append("<p>The Data Engineering Request form has been submitted. Here are the details:</p>")

                .append("Name: " + userName)
                .append("<br>Email: " + user.getEmail());
            if (user.getCompany() != null) {
                content.append("<br>Company Name:")
                    .append(user.getCompany());
            }
            content.append("<br>Message: ")
                .append(userRequest.getMessage());
            mailService.sendRequestAdminEmail(adminSubject, content.toString(), false, true);

            userSubject = " Your RFQ from Amass Insights ";
            content = new StringBuffer();
            content.append("Dear " + userName + ",")
                .append("<p>Thank you for requesting help with data science, data cleaning, data analysis, and/or other data engineering services from Amass Insights. We will review your message and get back to you soon.</p>")
                .append("<p>The details of your communication are below:</p> ")
                .append("<p>Message: </p>")
                .append(userRequest.getMessage());

            content.append("<br><p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");
            mailService.sendEmail(user.getEmail(), " Your RFQ from Amass Insights ", content.toString(), false, true, user);
        } else if (request == 3) {
            userRequest.setRequestTopic("Request help Selling Data");
            userName = saveUserRequest(userRequest, REQUEST_HELP_SELLING_DATA);
            adminSubject = " Data Sales Request (RFS) Submitted by " + userName;
            content.append("<p>The Data Sales Request form has been submitted. Here are the details:</p>")

                .append("Name: " + userName)
                .append("<br>Email: " + user.getEmail());
            if (user.getCompany() != null) {
                content.append("<br>Company Name:")
                    .append(user.getCompany());
            }
            content.append("<br>Message: ")
                .append(userRequest.getMessage());
            mailService.sendRequestAdminEmail(adminSubject, content.toString(), false, true);

            userSubject = " Your RFS from Amass Insights ";
            content = new StringBuffer();
            content.append("Dear " + userName + ",")
                .append("<p>Thank you for requesting help with sales, business development, and distribution of your data products by Amass Insights. We will review your message and get back to you soon.</p>")
                .append("<p>The details of your communication are below:</p> ")
                .append("<p>Message: </p>")
                .append(userRequest.getMessage());

            content.append("<br><p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");
            mailService.sendEmail(user.getEmail(), " Your RFS from Amass Insights ", content.toString(), false, true, user);
        }else {
            userRequest.setRequestTopic("Suggest a Provider");
            userName = saveUserRequest(userRequest, 567);
            adminSubject = " Suggestion for a new Provider by " + userName;
            content.append("<p>The Suggest a Provider form has been submitted. Here are the details:</p>")

                .append("Name: " + userName)
                .append("<br>Email: " + user.getEmail());
            if (user.getCompany() != null) {
                content.append("<br>Company Name: ")
                    .append(user.getCompany());
            }
            content.append("<br>Message: ")
                .append(userRequest.getMessage());
            mailService.sendRequestAdminEmail(adminSubject, content.toString(), false, true);

            userSubject = " Your Suggestion for a new Provider to be Added to Insights ";
            content = new StringBuffer();
            content.append("Dear " + userName + ",")
                .append("<br>Thank you for your suggestion for a new provider to be added to the Insights platform. We will review your suggestion, consider whether the suggestion is appropriate and get back to you soon with the outcome of your suggestion.")
                .append("<br>The details of your communication are below:")
                .append("<br>Message: ")
                .append(userRequest.getMessage());

            content.append("<br><p><em>Regards,<br>")
                .append("Amass Insights Team</em></p>");
            mailService.sendEmail(user.getEmail(), userSubject, content.toString(), false, true, user);
        }


    }
   /* public void subcriptionMoreInfo(UserRequest userRequest) {
    	subcriptionMoreInfo(userRequest);
    }*/






  /*  private void subcriptionMoreInfo(UserRequest userRequest) {
    	System.out.println("ssssselcaas++++++++++++++++++");
    	  String confirmationkey = RandomUtil.generateConfirmationKey();
    	  System.out.println(confirmationkey+";;;;;;;;;;;;;;;;");
    	  //saveSubcriptionInterest(confirmationkey,userRequest);
    	 StringBuffer content = new StringBuffer();
        // String adminSubject, userName;
         content.append("<p>More Information Request subcribing:</p>")
        // .append("Message: ")
         .append(userRequest.getMessage())
         //.append("<p><b>User Details</b>")
         .append(userRequest.getUserName())
         .append(userRequest.getUserEmail());

     content.append("</p><p><b>Provider Details</b><br>Name: <a href='")
         .append(jHipsterProperties.getMail().getBaseUrl())
         .append("/#/email-action?api=subscription/new-contactlist-info&key=/")
          .append(confirmationkey);
     userService.sentSubscriptionConfirmationEmail(userRequest.getUserName(), userRequest.getUserEmail());

     //mailService.sendAdminEmail(adminSubject, content.toString(), false, true);

	}
*/
	/*private void saveSubcriptionInterest(String confirmationkey,UserRequest userRequest) {
             System.out.println("sssssssssasdjsniii");

        if(!StrUtils.isBlank(confirmationkey)) {
            userRequest.setConfirmationkey(confirmationkey);
        }

       userRequestRepository.save(userRequest);
        *///userProviderRepository.save(userProvider);

	  /* private void saveProviderInterest(DataProvider provider, int interestType, User user, String deletionKey, boolean inactive, UserProvider userProvider)
	    {
	        if(userProvider == null) {
	            userProvider = new UserProvider();
	        }
	        userProvider.setProviderRecordID(provider.getRecordID());
	        userProvider.setProviderID(provider.getId());
	        userProvider.setUserID(user.getId());
	        userProvider.setInterestType(interestType);
	        userProvider.setCreatedDate(LocalDateTime.now());

	        if(!StrUtils.isBlank(deletionKey)) {
	            userProvider.setDeletionKey(deletionKey);
	        }

	        userProvider.setInactive(inactive);
	        userProviderRepository.save(userProvider);
	    }
*/

    private String saveUserRequest(UserRequest userRequest, int interestType) {
        User user = userService.getUser();
        userRequest.setCreatedDate(LocalDateTime.now());
        userRequest.setRequestType(interestType);
        String userName;
        if (user != null) {
            userRequest.setUserId(user.getId());
            userRequest.setUserName(user.getFirstName());
            userRequest.setCompany(user.getCompany());
            userRequest.setUserEmail(user.getEmail());
            userName = user.getFirstName() + " " + user.getLastName();
        } else {
            userName = userRequest.getUserName();
        }
        userRequestRepository.save(userRequest);
        return userName;
    }

    public void contactUs(UserRequest userRequest) {
        String userName=null;
        if(userRequest.getRequestTopic()!=null)
        {
        if(userRequest.getRequestTopic().equalsIgnoreCase("Help Finding Data"))
        {
            userName = saveUserRequest(userRequest, Integer.parseInt(applicationProperties.getRequestHelpFindingData()));

        }
        if(userRequest.getRequestTopic().equalsIgnoreCase("Help Selling My Data"))
        {
            userName = saveUserRequest(userRequest, Integer.parseInt(applicationProperties.getRequestHelpSellingData()));

        }
        if(userRequest.getRequestTopic().equalsIgnoreCase("General Inquiry"))
        {
            userName = saveUserRequest(userRequest, INTEREST_TYPE_GENERAL_INQUIRY);

        }
        if(userRequest.getRequestTopic().equalsIgnoreCase("Press"))
        {
            userName = saveUserRequest(userRequest, INTEREST_TYPE_PRESS);

        }
        if(userRequest.getRequestTopic().equalsIgnoreCase("Tech Support"))
        {
            userName = saveUserRequest(userRequest, INTEREST_TYPE_TECH_SUPPORT);

        }
        }
        else
        {
        	userRequest.setRequestTopic("General Inquiry");
            userName = saveUserRequest(userRequest, INTEREST_TYPE_GENERAL_INQUIRY);

        }

        String subject = "Contact Us Form Submitted by " + userName;
        StringBuffer adminMessage = new StringBuffer();
        adminMessage.append("The Contact Us form has been submitted. Here are the details: ")
            .append("<p>Name: ")
            .append(userName)
            .append("<br>Email:")
            .append(userRequest.getUserEmail())
            .append("<br>Company: ")
            .append(userRequest.getCompany())
            .append("<br>Inquiry Type: ")
            .append(userRequest.getRequestTopic())
            .append("<br>Message: ")
            .append(userRequest.getMessage())
            .append("</p>");
        mailService.sendContactUsEmail(subject, adminMessage.toString(), false, true);
        String replyMessage = "Dear " + userRequest.getUserName() + "," + "<p>Thank you for contacting Amass Insights." +
            "We will review your message and get back to you soon.<br>" +
            "The details of your communication are below:" +
            "<p>Inquiry Type: " +
            userRequest.getRequestTopic() +
            "<br>Message: " +
            userRequest.getMessage() +
            "</p><em>Regards,<br>" +
            "Amass Insights Team</em>";
        mailService.sendEmail(userRequest.getUserEmail(), "Thank you for contacting Amass Insights", replyMessage, false, true);
    }

    public void contactUsHeader(UserRequest userRequest) {

        if (userRequest.getRequestType() == 1){
            userRequest.setRequestTopic("Request help Finding Data");
            saveUserRequest(userRequest,  Integer.parseInt(applicationProperties.getRequestHelpFindingData()));
            String subject = "Data Request Submitted by " + userRequest.getUserName();
            StringBuffer adminMessage = new StringBuffer();
            adminMessage.append("The Data Request form has been submitted. Here are the details: ")
                .append("<p>Name: ")
                .append(userRequest.getUserName())
                .append("<br>Email:")
                .append(userRequest.getUserEmail())
                .append("<br>Message: ")
                .append(userRequest.getMessage())
                .append("</p>");
            mailService.sendContactUsEmail(subject, adminMessage.toString(), false, true);
            String replyMessage = "Dear " + userRequest.getUserName() + "," + "<p>Thank you for requesting data from Amass Insights. We will review your message and get back to you soon.\n" +
                "<br>The details of your communication are below:" +
                "<br>Message:<br>" +
                userRequest.getMessage() +
                "</p><em>Regards,<br>" +
                "Amass Insights Team</em>";
            mailService.sendEmail(userRequest.getUserEmail(), "Your RFD from Amass Insights", replyMessage, false, true);
        }
        else if (userRequest.getRequestType() == 2) {
            userRequest.setRequestTopic("Request help Engineering Data");

            saveUserRequest(userRequest,Integer.parseInt(applicationProperties.getRequestHelpEngineeringData()));
            String subject = "Data Engineering Request (RFQ) Submitted by " + userRequest.getUserName();
            StringBuffer adminMessage = new StringBuffer();
            adminMessage.append("The Data Engineering Request form has been submitted. Here are the details: ")
                .append("<p>Name: ")
                .append(userRequest.getUserName())
                .append("<br>Email:")
                .append(userRequest.getUserEmail())
                .append("<br>Message: ")
                .append(userRequest.getMessage())
                .append("</p>");
            mailService.sendContactUsEmail(subject, adminMessage.toString(), false, true);
            String replyMessage = "Dear " + userRequest.getUserName() + "," + "<p>Thank you for requesting help with data science, data cleaning, data analysis, and/or other data engineering services from Amass Insights. We will review your message and get back to you soon.\n" +
                "<br>The details of your communication are below:" +
                "<br>Message:<br>" +
                userRequest.getMessage() +
                "</p><em>Regards,<br>" +
                "Amass Insights Team</em>";
            mailService.sendEmail(userRequest.getUserEmail(), "Your RFQ from Amass Insights", replyMessage, false, true);
        }
        else {
            userRequest.setRequestTopic("Request help Selling  Data");
            saveUserRequest(userRequest, Integer.parseInt(applicationProperties.getRequestHelpSellingData()));
            String subject = "Data Sales Request (RFS) Submitted by " + userRequest.getUserName();
            StringBuffer adminMessage = new StringBuffer();
            adminMessage.append("The Data Sales Request form has been submitted. Here are the details: ")
                .append("<p>Name: ")
                .append(userRequest.getUserName())
                .append("<br>Email:")
                .append(userRequest.getUserEmail())
                .append("<br>Message: ")
                .append(userRequest.getMessage())
                .append("</p>");
            mailService.sendContactUsEmail(subject, adminMessage.toString(), false, true);
            String replyMessage = "Dear " + userRequest.getUserName() + "," + "<p>Thank you for requesting help with sales, business development, and distribution of your data products by Amass Insights. We will review your message and get back to you soon.\n" +
                "<br>The details of your communication are below:" +
                "<br>Message:<br>" +
                userRequest.getMessage() +
                "</p><em>Regards,<br>" +
                "Amass Insights Team</em>";
            mailService.sendEmail(userRequest.getUserEmail(), "Your RFS from Amass Insights", replyMessage, false, true);

        }

    }

    public void processCategoryInterest(long categoryID, long providerID, int interestType) {
        User user = userService.getUser();
        UserDataCategory userDataCategory = new UserDataCategory();
        userDataCategory.setDataCategoryID(categoryID);
        userDataCategory.setUserID(user.getId());
        userDataCategory.setInterestType(interestType);
        userDataCategory.setCreatedDate(LocalDateTime.now());
        userDataCategoryRepository.save(userDataCategory);
        if (interestType == INTEREST_TYPE_UNLOCK_PENDING_USER_CONFIRM) {
            DataProvider provider = dataProviderSearchRepository.findOne(categoryID);
            StringBuffer content = new StringBuffer();

            String subject = null;

            if (interestType == INTEREST_TYPE_UNLOCK_PENDING_USER_CONFIRM) {
                subject = "Please confirm the profile unlock request";
                content.append("Dear " + user.getFirstName() + ",")
                    .append("<p>Please click on the below link to confirm your request to unlock the provider.")
                    .append("<br><a href='" + jHipsterProperties.getMail().getBaseUrl() + "/#/confirm/unlock-provider?id=" + provider.getId() + "'>Click here to confirm</a>")
                    .append("<p>The short description of the profile is as below:</p>")
                    .append(provider.getShortDescription())
                    .append("</p><br>Regards,<br><em>Amass Team</em>");
            } else if (interestType == INTEREST_TYPE_EXPAND_PENDING_USER_CONFIRM) {
                subject = "Please confirm the profile expansion request for " + provider.getProviderName();
                content.append("Dear " + user.getFirstName() + ",")
                    .append("<p>Please click on the below link to confirm you request to expand the provider.")
                    .append("<br><a href='" + jHipsterProperties.getMail().getBaseUrl() + "/#/confirm/expand-provider?id=" + provider.getId() + "'>Click here to confirm</a>")
                    .append("<p>The profile details are as below:</p>")
                    .append("Provider Name: " + provider.getProviderName())
                    .append("<br>Description: ")
                    .append(provider.getShortDescription())
                    .append("</p><br>Regards,<br><em>Amass Team</em>");
            }

            mailService.sendEmail(user.getEmail(), subject, content.toString(), false, true, user);

        }
        else if (interestType == INTEREST_TYPE_UNLOCK_PENDING) {
            DataProvider provider = dataProviderSearchRepository.findOne(providerID);
            StringBuffer content = new StringBuffer();
            String userName = user.getFirstName();
            if (interestType == INTEREST_TYPE_UNLOCK_PENDING) {
                mailService.sendEmail(user.getEmail(), "Thank you for confirming profile unlock request ", content.toString(), false, true, user);


                content.append("Profile unlock request for: " + provider.getProviderName())
                    .append("<p>Please click on the below link to approve the request to unlock the provider.")
                    .append("<br><a href='" + jHipsterProperties.getMail().getBaseUrl() + "/#/approve/unlock-provider?id='" + provider.getRecordID() + ">Click here to approve</a>");
                String    adminSubject = userName + " requested Profile expansion request for: " + provider.getProviderName();

            }
        }
    }
    //check based on key

    public void resubmitProviderMoreInfo(String key) {
        UserProvider udp = userProviderRepository.findOneByDeletionKeyEquals(key);
        if(udp == null)
            return;
        INTEREST_TYPE_MORE_INFO=applicationProperties.getInterestTypeMoreInfo();
        UserRequest ur = userRequestRepository.findFirstByUserIdEqualsAndProviderIDEqualsAndRequestTypeEqualsOrderByCreatedDateDesc(udp.getUserID(), udp.getProviderID(), INTEREST_TYPE_MORE_INFO);
        if(ur != null)
            processProviderMoreInfo(ur, udp.getUser());

    }


   /* public void subcriptionUserRequest(UserRequest userRequest) {



    	//UserRequest ur /*= userRequestRepository.findOneByconfirmationkeyEquals(key);*/

    //System.out.println(ur+";;;;;;;;;;;;;;;;;;;;;;");
    /*	 subcriptionMoreInfo(userRequest);
    	 if(ur != null)

    //}
    }*/

////


    public void processProviderComment(DataComment result) {
        int interestType = INTEREST_TYPE_UNLOCK_PENDING;
        User user = userService.getUser();
        Long dataProviderId=null;
        for(DataProvider dc:result.getReleventProviders())
        {
            dataProviderId=dc.getId();
        }
        DataProvider provider = dataProviderSearchRepository.findOne(dataProviderId);
        String message;
        if(result.getPrivacy().equalsIgnoreCase("Public"))
        {
            message="Comment has been Posted to ";
        }
        else {
            message="Note has been Saved to ";
        }
        String subject;
        if(result.getPrivacy().equalsIgnoreCase("Public"))
        {
            subject="Your Comment has been Posted in "+" "+provider.getProviderName()+"'s Profile";
        }
        else {
            subject="Your Note has been Saved in "+" "+provider.getProviderName()+"'s Profile";
        }
        String Comment;
        if(result.getPrivacy().equalsIgnoreCase("Public"))
        {
            Comment="Posted a Comment in "+provider.getProviderName()+"'s Profile";
        }
        else {
            Comment="Saved a Note in "+provider.getProviderName()+"'s Profile";
        }
        String place=result.getComment(). replaceAll("\\<[^>]*>","");

        // String deletionKey = RandomUtil.generateActivationKey();
        //saveProviderInterest(provider, interestType, user, deletionKey, false, null);
        if (interestType == INTEREST_TYPE_UNLOCK_PENDING || interestType == INTEREST_TYPE_EXPAND_PENDING) {
            StringBuffer content = new StringBuffer();
            String adminSubject, userName;
            if (interestType == INTEREST_TYPE_UNLOCK_PENDING) {
                content.append("Dear " + user.getFirstName() + ",")
                    .append("<p>There is a friendly notification that your ")
                    .append(message)
                    .append(" ")
                    .append("<a href='")
                    .append(jHipsterProperties.getMail().getBaseUrl())
                    .append("#/providers/")
                    .append(provider.getRecordID())
                    .append("'>")
                    .append(provider.getProviderName()+"'s Profile"+"</a>")
                    .append(" with the following text:<br>")
                    .append("<p>")
                    .append('"'+place+'"')
                    .append("</p>Considering your apparent interest in this provider, your account will also now follow updates on this provider's profile.")
                    //   .append("<br>In the meantime, please respond to this email if you have any more details about why you requested this Provider or about the data you are looking to find.")
                    .append("<p><em>Regards,<br>")
                    .append("Amass Insights Team</em></p>");

                mailService.sendEmail(user.getEmail(), subject, content.toString(), false, true, user);
            }


            adminSubject = user.getFirstName()+"\t"+user.getLastName()+"\t"+Comment;
            String comment=result.getComment();
            String msg=comment. replaceAll("\\<[^>]*>","");

            content = new StringBuffer();
            content.append("<p>Details of Comment:</p>")
                .append("<p><b>User Information</b>")
                .append("<br>Name: " + user.getFirstName()+"\t"+user.getLastName())
                .append("<br>Email: " + user.getEmail());
            if (user.getCompany() != null) {
                content.append("<br>Company Name: ")
                    .append(user.getCompany()+"</p>");
            }
            content.append("<p><b>Provider Information</b><br>Provider Name: ")
                .append("<a href='")
                .append(jHipsterProperties.getMail().getBaseUrl())
                .append("#/providers/")
                .append(provider.getRecordID())
                .append("'>")
                .append(provider.getProviderName())
                .append("</a><br>provider Website: ")
                .append("<a href='")
                .append(provider.getWebsite())
                .append("'>")
                .append(provider.getWebsite())
                .append("</a><br>Comment Text: "+'"' + msg+'"')
                .append("</p>");
            //.append("<br>Comment Text: "+result.getComment()+"</p>");
            mailService.sendAdminEmail(adminSubject, content.toString(), false, true);
        }
    }

    public UserProvider findOneByUserIDEqualsAndProviderID(Long id) {
        // TODO Auto-generated method stub
        User user = userService.getUser();

        return userProviderRepository.findOneByUserIDEqualsAndProviderID(user.getId(),id);
    }

    public UserProvider findOneByUserIDEqualsAndProviderIDEqualsAndInterestType(Long id, int operatorType) {
        // TODO Auto-generated method stub
        User user = userService.getUser();
        return userProviderRepository.findOneByUserIDEqualsAndProviderIDEqualsAndInterestType(user.getId(),id,operatorType);
    }
    @Transactional(readOnly=false)
	public void myMatchesValue(UserProvider userProvider) {
		// TODO Auto-generated method stub
		userProviderRepository.save(userProvider);
	}

	public UserProvider getMyMatchesValue(Long userId, Long ProviderId, int interestTypeUnlock) {
		// TODO Auto-generated method stub
		UserProvider userProvider=userProviderRepository.getMyMatchedValue(userId,ProviderId,interestTypeUnlock);
		return userProvider;
	}

	public void myMatchesProviderEmail() {
		// TODO Auto-generated method stub
       List<User> user = userService.getmyMatchedUser();
       if(null!=user && user.size()>0)
       {
    	   for(User usersValue:user)
    	   {
    		boolean authority= userService.findAuthority(usersValue.getId());
    		if(authority)
    		{
    			//System.out.println("usersValue"+usersValue.getId());
		DataProviderTag dataProviderTag=dataProviderTagRepository.findUserValue(usersValue.getId());
//System.out.println(dataProviderTag.getId());
		//List<Long> dataTag=dataProviderTagRepository.myMatchTag(dataProviderTag.getId());
    	Query q = entityManager.createNativeQuery("SELECT * FROM  datamarketplace.data_provider_provider_tag where provider_tags_id="+ dataProviderTag.getId());
    	List<?> authors = q.setMaxResults(20).getResultList();
	//	System.out.println(authors.size());
		if(null !=authors && authors.size()>0)
		{
			List<DataProvider>providerList=new ArrayList<DataProvider>();
		for(int i=0; i<authors.size();i++)
		{
			Object[] row = (Object[]) authors.get(i);
		//	System.out.println(Long.parseLong(row[1].toString()));
			UserProvider userProviderValue=getMyMatchesValue(usersValue.getId(),Long.parseLong(row[1].toString()),INTEREST_TYPE_UNLOCK);
			if(null!=userProviderValue)
			{
               DataProvider provider =dataProviderRepository.findById(userProviderValue.getProviderID());
               if(null != provider)
               {
            	   providerList.add(provider);
               }
			}
    }
		//System.out.println("providerList"+providerList.size());
		if(providerList.size()>0)
		{
			mailService.sendMyMatchesProvider(providerList,usersValue);
		}
		else
		{
			mailService.sendNoMyMatchesProvider(usersValue);

		}
		userService.sendMyMatchesUser(usersValue);
		}
		else
		{
			mailService.sendNoMyMatchesProvider(usersValue);
			userService.sendMyMatchesUser(usersValue);

		}
	}
       }
	}
	}

	public void addOperatorPermissionToUserDomain(List<User> users) {
		// TODO Auto-generated method stub
		for(User user:users)
		{
			String emailDomain=user.getEmail();
			emailDomain=emailDomain.substring(emailDomain.indexOf("@") + 1);
			Domains domain=dataDomainsRepository.findByDomain(emailDomain);
			if(null!=domain)
			{
		        //Set<Authority> authorities = new HashSet<>();
				 if(null!= domain.getOwnerInvestmentManager() && domain.getOwnerInvestmentManager().size()>0)
		         	{
		        	 System.out.println(domain.getOwnerInvestmentManager().size());
		        	 user.getAuthorities().add(authorityRepository.findOne(USER));
		        	 user.getAuthorities().add(authorityRepository.findOne(ROLE_INVESTOR));
		         	}
		         else if(null!= domain.getOwnerDataProvider() && domain.getOwnerDataProvider().size()>0)
		        	{
		        	 System.out.println(domain.getOwnerDataProvider().size());
		        	 user.getAuthorities().add(authorityRepository.findOne(USER));
		        	 user.getAuthorities().add(authorityRepository.findOne(ROLE_PROVIDER));
						 userService.addOperatorPermission(user,domain.getOwnerDataProvider());

		        	}
		         else
		         {
		        	 user.getAuthorities().add(authorityRepository.findOne(USER));
		         }
				 //user.setAuthorities(authorities);
			     userRepository.save(user);

				/* if(user.getAuthorities().stream().anyMatch(l->l.getDescription().equalsIgnoreCase("ROLE_PROVIDER"))) {
				    
					 if(null!=domain.getOwnerDataProvider() && domain.getOwnerDataProvider().size()>0)
					 {
						 List<Long>userProvider=userProviderRepository.findAllByUserIDEqualsAndInterestTypeEquals(user.getId(), INTEREST_TYPE_OPERATOR);
						 userService.addOperatorPermission(user,domain.getOwnerDataProvider());
					 }
					 
				    }
*/			}
		   
		}
	}

	public void grantOperatorPermission(Long userId, Long providerId) {
		// TODO Auto-generated method stub
		DataProvider dataProvider=dataProviderRepository.findById(providerId);
		UserProvider userProvider=new UserProvider();
		userProvider.setUserID(userId);
		userProvider.setProviderID(providerId);
		if(null!=dataProvider)
		{
			userProvider.setProviderRecordID(dataProvider.getRecordID());
		}
		userProvider.setInterestType(INTEREST_TYPE_OPERATOR);
		userProvider.setCreatedDate(LocalDateTime.now());
		userProvider.setInactive(false);
		userProviderRepository.save(userProvider);
		User user=userRepository.findOneByIdEquals(userId);
		mailService.sendOperatorPermission(dataProvider,user);
	}

	public List<UserProvider> findByProviderIdMatchesPermission(Long id) {
		// TODO Auto-generated method stub
		List<UserProvider> users=userProviderRepository.findAllProvidersByUserID(id,INTEREST_TYPE_OPERATOR);
		return users;
	}

	public DataProvider getLogo(Long providerId,Long userId) {
		// TODO Auto-generated method stub
		UserProvider users=userProviderRepository.findOneByUserIDEqualsAndProviderIDEqualsAndInterestTypeEquals(userId,providerId,INTEREST_TYPE_OPERATOR);
		DataProvider provider=dataProviderRepository.findOneWithEagerRelationships(users.getProviderID());
		return provider;
	}
	public User getProviderDetails(Long id) {
		// TODO Auto-generated method stub
		User user=new User();
		List<UserProvider> userOperatedPermission=userProviderRepository.findAllByUserIDsEqualsAndInterestTypeEquals(id,INTEREST_TYPE_OPERATOR);
		List<UserProvider> usersUnlocked=userProviderRepository.findAllByUserIDsEqualsAndInterestTypeEquals(id,INTEREST_TYPE_UNLOCK);
Set<DataProvider>provider=new HashSet<DataProvider>();
Set<DataProvider>providerUnlocked=new HashSet<DataProvider>();

if(null!=userOperatedPermission) {
		for(UserProvider d:userOperatedPermission) {
	DataProvider data=dataProviderRepository.findById(d.getProviderID());
	provider.add(data);
}
}
if(null!=usersUnlocked) {
	for(UserProvider d:usersUnlocked) {
	DataProvider data=dataProviderRepository.findById(d.getProviderID());
	providerUnlocked.add(data);
}
}
		user.setProviderPartners(provider);
		user.setUnlockedPartners(providerUnlocked);
		//DataProvider provider=dataProviderRepository.findOneWithEagerRelationships(users.getProviderID());
		return user;
	}

	public void providerAccessPermission(User user) {
		// TODO Auto-generated method stub
		if(null!=user.getProviderPartners() && user.getProviderPartners().size()>0) {
			System.out.println("cals");
			userProviderRepository.deleteProvider(user.getId(),INTEREST_TYPE_OPERATOR);
			for(DataProvider dataProvider:user.getProviderPartners())
			{
				System.out.println("cal"+dataProvider.getId());
				UserProvider userProvider=new UserProvider();
				DataProvider data=dataProviderRepository.findById(dataProvider.getId());
				userProvider.setProviderID(data.getId());
				userProvider.setUserID(user.getId());
				userProvider.setInterestType(INTEREST_TYPE_OPERATOR);
				userProvider.setCreatedDate(LocalDateTime.now());
				userProvider.setProviderRecordID(data.getRecordID());
				userProvider.setInactive(false);
				userProviderRepository.save(userProvider);
			}
		}
		else {
			userProviderRepository.deleteProvider(user.getId(),INTEREST_TYPE_OPERATOR);

		}
		if(null!=user.getUnlockedPartners() && user.getUnlockedPartners().size()>0) {
			userProviderRepository.deleteProvider(user.getId(),INTEREST_TYPE_UNLOCK);
			for(DataProvider dataProvider:user.getUnlockedPartners())
			{
				UserProvider userProvider=new UserProvider();
				DataProvider data=dataProviderRepository.findById(dataProvider.getId());
				userProvider.setProviderID(data.getId());
				userProvider.setUserID(user.getId());
				userProvider.setInterestType(INTEREST_TYPE_UNLOCK);
				userProvider.setCreatedDate(LocalDateTime.now());
				userProvider.setProviderRecordID(data.getRecordID());
				userProvider.setInactive(false);
				userProviderRepository.save(userProvider);
			}
		}
		else {
			userProviderRepository.deleteProvider(user.getId(),INTEREST_TYPE_UNLOCK);
		}
	}

	public void updateUserProvider(DataProvider newProvider) {
		// TODO Auto-generated method stub
		if(null!=newProvider.getUser()) {
			//userProviderRepository.deleteUserProvider(newProvider.getId(),INTEREST_TYPE_OPERATOR);
List<User>userList=new ArrayList<User>();
List<Long>userId=new ArrayList<Long>();
			for(String userEmail:newProvider.getUser()) {
				User user=userRepository.findByEmail(userEmail);
                if(null!=user) {
    				userList.add(user);
    				userId.add(user.getId());
                }
			}
			//System.out.println("us"+userId.size());
			List<Long>userPermission=userProviderRepository.findAllByInterestTypeEqualsProviderId(newProvider.getId(),INTEREST_TYPE_OPERATOR);
			//System.out.println("per"+userPermission.size());
			if(null!=userPermission) {
				if(!userId.equals(userPermission)) {
					//System.out.println("called");
					userProviderRepository.deleteUserProvider(newProvider.getId(), INTEREST_TYPE_OPERATOR);
					for(User user:userList) {
						 UserProvider userProviders=new UserProvider();
							userProviders.setProviderID(newProvider.getId());
							userProviders.setUserID(user.getId());
							userProviders.setInterestType(INTEREST_TYPE_OPERATOR);
							userProviders.setCreatedDate(LocalDateTime.now());
							userProviders.setProviderRecordID(newProvider.getRecordID());
							userProviders.setInactive(false);
							userProviderRepository.save(userProviders);
					}
					
				}
			}
		}
		else {
			//userProviderRepository.deleteProvider(user.getId(),INTEREST_TYPE_UNLOCK);
		}
	}
	
}
