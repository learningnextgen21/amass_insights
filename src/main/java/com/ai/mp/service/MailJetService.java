package com.ai.mp.service;

import com.ai.mp.config.ApplicationProperties;
import com.mailjet.client.ClientOptions;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.resource.Contact;
import com.mailjet.client.resource.Emailv31;
import com.mailjet.client.resource.TemplateDetailcontent;

import io.github.jhipster.config.JHipsterProperties;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.mailjet.client.resource.Emailv31.Message.EMAIL;
import static com.mailjet.client.resource.Emailv31.Message.NAME;

@Component
public class MailJetService {
    private MailjetClient client;
    private MailjetClient clientV3;
    private final Environment environment;
    private final JHipsterProperties jHipsterProperties;
    private final ApplicationProperties applicationProperties;
    private final Logger log = LoggerFactory.getLogger(MailService.class);
    public MailJetService(Environment environment, JHipsterProperties jHipsterProperties, ApplicationProperties applicationProperties)
    {
        this.environment = environment;
        this.jHipsterProperties = jHipsterProperties;
        this.applicationProperties = applicationProperties;
        client = new MailjetClient(this.environment.getProperty("spring.mail.username"), this.environment.getProperty("spring.mail.password"));
        clientV3 = new MailjetClient(this.environment.getProperty("spring.mail.username"), this.environment.getProperty("spring.mail.password"), new ClientOptions("v3.1"));
    }
    public List<String> getSubscriberList()
    {
        MailjetResponse response = null;
        MailjetRequest contacts;
        List<String> emails = new ArrayList<>();
        try {
            contacts = new MailjetRequest(Contact.resource).filter(Contact.CONTACTSLIST, environment.getProperty("spring.mailjet.subscription-list"));
            response = client.get(contacts);
            if(response.getCount() > 0)
            {
                JSONArray array = response.getData();
                for(int i = 0; i < array.length(); i++)
                {
                    JSONObject o = array.getJSONObject(i);
                    emails.add(o.getString(Contact.EMAIL));
                }
            }
        } catch (MailjetException e) {
            log.error(e.getMessage(), e);
        } catch (MailjetSocketTimeoutException e) {
            log.error(e.getMessage(), e);
        }
        return emails;
    }
    public void sendMail(String email, String userName, int templateID, String subject, JSONObject variables)
    {
        MailjetResponse response = null;
        try{
            MailjetRequest request = new MailjetRequest(Emailv31.resource)
                .property(Emailv31.MESSAGES, new JSONArray()
                    .put(new JSONObject()
                        .put(Emailv31.Message.FROM, new JSONObject()
                            .put(EMAIL, jHipsterProperties.getMail().getFrom())
                            .put(NAME, applicationProperties.getMailFromName()))
                        .put(Emailv31.Message.TO, new JSONArray()
                            .put(new JSONObject()
                                .put(EMAIL, email)
                                .put(NAME, userName)))
                        .put(Emailv31.Message.TEMPLATEID, templateID)
                        .put(Emailv31.Message.TEMPLATELANGUAGE, true)
                        .put(Emailv31.Message.SUBJECT, subject)
                        .put(Emailv31.Message.VARIABLES, variables)
                .put(Emailv31.Message.TEMPLATEERROR_DELIVERY, true)
                .put(Emailv31.Message.TEMPLATEERROR_REPORTING, new JSONObject()
                    .put(EMAIL, applicationProperties.getAdminEmail())
                    
                    .put(NAME, applicationProperties.getMailFromName()))));
         
            response = clientV3.post(request);
        }
        catch (MailjetException e) {
            log.error(e.getMessage(), e);
        }
        catch (MailjetSocketTimeoutException e) {
            log.error(e.getMessage(), e);
        }
        log.info("Response status: " + response.getStatus());
        log.info("Response data: " + response.getData());
    }
    
    public String getMail(int templateID) 
    {
         MailjetRequest request;
         MailjetResponse response;
         request = new MailjetRequest(TemplateDetailcontent.resource, templateID);
         String body=null;
         try {
			response = client.get(request);
			 if(response.getCount() > 0)
	            {
	                JSONArray array = response.getData();
	                for(int i = 0; i < array.length(); i++)
	                {
	                    JSONObject o = array.getJSONObject(i);
	                   // System.out.println((o.getString("Html-part")));
	                  // System.out.println((o.toString()));
	                    body=o.getString("Html-part");
	                    //System.out.println(body);
	                    //emails.add(o.getString(Contact.EMAIL));
	                }
	            }
			
		} catch (MailjetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MailjetSocketTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return body;
         
    }
}
