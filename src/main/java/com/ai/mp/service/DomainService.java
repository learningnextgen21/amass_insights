package com.ai.mp.service;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ai.mp.domain.Domains;
import com.ai.mp.domain.EmailAddress;
import com.ai.mp.domain.User;
import com.ai.mp.repository.DataDomainsRepository;
import com.ai.mp.repository.EmailAddressRepository;
import com.ai.util.Utils;

@Service
@Transactional
public class DomainService {

	private final DataDomainsRepository dataDomainsRepository;
	private final EmailAddressRepository emailAddressRepository;
	DomainService(DataDomainsRepository dataDomainsRepository,EmailAddressRepository emailAddressRepository)
	{
		this.dataDomainsRepository=dataDomainsRepository;
		this.emailAddressRepository=emailAddressRepository;
		
	}

	public Domains createDomain(User user) {
		// TODO Auto-generated method stub
    String emailDomain=user.getEmail();
	emailDomain=emailDomain.substring(emailDomain .indexOf("@") + 1);
	Domains domain =new Domains();
    EmailAddress email=emailAddressRepository.findByEmailAddress(user.getEmail());
    Set<EmailAddress>emailAddress=new HashSet<EmailAddress>();
    if (email!=null)
    {
    	emailAddress.add(email);
      domain.setEmailAddresses(emailAddress);

    }else
    {
        EmailAddress newEmail=new EmailAddress();
        newEmail.setRecordID("rece"+ Utils.randomString(13,13));
        newEmail.setEmailAddress(user.getEmail().toLowerCase());
        EmailAddress result =emailAddressRepository.save(newEmail);
        emailAddress.add(result);
        domain.setEmailAddresses(emailAddress);        
    }
	
	domain.setDomain(emailDomain);
	domain.setCreatedDate(LocalDate.now());
	domain.setRecordId("recd"+ Utils.randomString(13,13));
	domain.setLastUpdatedDate(LocalDate.now());
	System.out.println("domain"+domain.getDomain());
	dataDomainsRepository.save(domain);
		return domain;
	}

	public void updateEmailAddress(User user, Domains domain) {
		// TODO Auto-generated method stub
	    EmailAddress email=emailAddressRepository.findByEmailAddress(user.getEmail());
	    Set<EmailAddress>emailAddress=new HashSet<EmailAddress>();
	    if (email!=null)
	    {
	    	emailAddress.add(email);
	      domain.setEmailAddresses(emailAddress);

	    }else
	    {
	        EmailAddress newEmail=new EmailAddress();
	        newEmail.setRecordID("rece"+ Utils.randomString(13,13));
	        newEmail.setEmailAddress(user.getEmail().toLowerCase());
	        EmailAddress result =emailAddressRepository.save(newEmail);
	        emailAddress.add(result);
	        domain.setEmailAddresses(emailAddress);        
	    }
		domain.setLastUpdatedDate(LocalDate.now());
		dataDomainsRepository.save(domain);

	}
	
}
