package com.ai.mp.service;

import static com.ai.core.Constants.INTEREST_TYPE_FOLLOW;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ai.core.StrUtils;
import com.ai.mp.batch.DownloadLogo;
import com.ai.mp.domain.DataCategory;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.User;
import com.ai.mp.domain.UserDataCategory;
import com.ai.mp.domain.UserProvider;
import com.ai.mp.repository.DataCategoryRepository;
import com.ai.mp.repository.UserDataCategoryRepository;
import com.ai.mp.repository.UserProviderRepository;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.repository.UserRequestRepository;
import com.ai.mp.repository.search.DataCategorySearchRepository;
import com.ai.mp.repository.search.DataProviderSearchRepository;
import com.ai.mp.utils.RandomUtil;

import io.github.jhipster.config.JHipsterProperties;

@Service
@Transactional
public class UserDataCategoryService {

    private final UserService userService;

    private final DataCategoryRepository dataCategoryRepository;
    private final UserDataCategoryRepository userDataCategoryRepository;
    private final MailService mailService;
    private final UserRepository userRepository;
    private final DataCategorySearchRepository dataCategorySearchRepository;


    public UserDataCategoryService(UserService userService, DataProviderSearchRepository dataProviderSearchRepository, DataCategoryRepository dataCategoryRepository, UserDataCategoryRepository userDataCategoryRepository, MailService mailService, UserRepository userRepository, DataCategorySearchRepository dataCategorySearchRepository, DownloadLogo downloadLogo) {

        this.userService = userService;

        this.dataCategoryRepository = dataCategoryRepository;
        this.userDataCategoryRepository = userDataCategoryRepository;
        this.mailService = mailService;
        this.userRepository = userRepository;
        this.dataCategorySearchRepository = dataCategorySearchRepository;

    }


    public void followCategory(Long dataCategoryID)


    {
        User user = userService.getUser();
        DataCategory category = dataCategoryRepository.findOne(dataCategoryID);
        String deletionKey = RandomUtil.generateActivationKey();
        UserDataCategory udc = userDataCategoryRepository.findOneByUserIDEqualsAndDataCategoryIDEqualsAndInterestTypeEquals(user.getId(), dataCategoryID, INTEREST_TYPE_FOLLOW);
        //UserProvider up = userProviderRepository.findOneByUserIDEqualsAndProviderIDEqualsAndInterestTypeEquals(user.getId(), providerID, INTEREST_TYPE_FOLLOW);
        //try {

        if (udc != null) {
            if (udc.getInactive()) {
                udc.setInactive(false);
                saveCategoryInterest(category, INTEREST_TYPE_FOLLOW, user, false, deletionKey, udc);
                mailService.sendFollowCategoryEmail(user, category, deletionKey);
                mailService.sendAdminEmailFollowCategory(user, category, true);
            } else {
                udc.setInactive(true);
                saveCategoryInterest(category, INTEREST_TYPE_FOLLOW, user, true, deletionKey, udc);
                mailService.sendUnfollowCategoryEmail(user, category, deletionKey);
                mailService.sendAdminEmailUnFollowCategory(user, category, false);
            }
        } else {
            saveCategoryInterest(category, INTEREST_TYPE_FOLLOW, user, false, deletionKey, null);
            mailService.sendFollowCategoryEmail(user, category, deletionKey);
            mailService.sendAdminEmailFollowCategory(user, category, true);
        }


    }/*catch(NullPointerException exception){
            System.out.print("NullPointerException Caught"+udc.toString());
        }*/


    public List<UserDataCategory> findAllDatacategoryInterestsByUserID()
    {
        return userDataCategoryRepository.findAllByUserIDEqualsAndInactiveIsFalse(userService.getCurrentUserID());

    }

    public List<Long> getDataCategoryByInterestTypeForCurrentUser(int interestType)
    {
        // return userProviderRepository.findAllByUserIDEqualsAndInterestTypeEquals(userService.getUserWithAuthorities().getId(), interestType);
        return userDataCategoryRepository.findAllByUserIDEqualsAndInterestTypeEquals(userService.getUserWithAuthorities().getId(), interestType);
    }

    public List<Long> getDataCategoryIDsByInterestTypeByUser(long userID, int interestType)
    {
        return userDataCategoryRepository.findAllByUserIDEqualsAndInterestTypeEquals(userID, interestType);
    }




    public void followCategory(String key, boolean toggle)
    {
        UserDataCategory  udp = userDataCategoryRepository.findOneByDeletionKeyEquals(key);
        if(udp != null)
        {
            String deletionKey = RandomUtil.generateActivationKey();
            User user = userRepository.findOneByIdEquals(udp.getUserID());
            DataCategory category= dataCategorySearchRepository.findOne(udp.getDataCategoryID());
            // DataProvider provider = dataProviderSearchRepository.findOne(udp.getProviderID());
            saveCategoryInterest(category, INTEREST_TYPE_FOLLOW, user,toggle,deletionKey, udp);
            mailService.sendAdminEmailFollowCategory(user, category, !toggle);
        }
    }



    private void saveCategoryInterest(DataCategory category, int interestType, User user, boolean inactive,String deletionKey ,UserDataCategory userCategory)
    {
        if(userCategory == null) {
            userCategory = new UserDataCategory();
        }

        //userCategory.setCategoryRecordID(category.getRecordID());
        userCategory.setDataCategoryID(category.getId());
        userCategory.setUserID(user.getId());
        userCategory.setInterestType(interestType);
        userCategory.setCreatedDate(LocalDateTime.now());

        //wheather to add delete r not in userdatacategory
        if(!StrUtils.isBlank(deletionKey)) {
            userCategory.setDeletionKey(deletionKey);
        }
        userCategory.setInactive(inactive);
        userDataCategoryRepository.save(userCategory);
    }










}












