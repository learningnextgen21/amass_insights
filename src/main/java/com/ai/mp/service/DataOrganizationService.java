package com.ai.mp.service;

import com.ai.mp.batch.DBUtils;
import com.ai.mp.domain.DataOrganization;
import com.ai.mp.repository.DataOrganizationRepository;
import com.ai.mp.repository.search.DataOrganizationSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing DataOrganization.
 */
@Service
@Transactional
public class DataOrganizationService {

    private final Logger log = LoggerFactory.getLogger(DataOrganizationService.class);
    private final DBUtils dbutils;
    private final DataOrganizationRepository dataOrganizationRepository;

    private final DataOrganizationSearchRepository dataOrganizationSearchRepository;
    public DataOrganizationService(DBUtils dbutils, DataOrganizationRepository dataOrganizationRepository, DataOrganizationSearchRepository dataOrganizationSearchRepository) {
        this.dbutils = dbutils;
        this.dataOrganizationRepository = dataOrganizationRepository;
        this.dataOrganizationSearchRepository = dataOrganizationSearchRepository;
    }

    /**
     * Save a dataOrganization.
     *
     * @param dataOrganization the entity to save
     * @return the persisted entity
     */
    public DataOrganization save(DataOrganization dataOrganization) {
        log.debug("Request to save DataOrganization : {}", dataOrganization);
        DataOrganization result = dataOrganizationRepository.save(dataOrganization);
        dataOrganizationSearchRepository.save(dataOrganizationRepository.findOne(result.getId()));
        return result;
    }

    /*public DataOrganization save(DataOrganization dataOrganization) {
        log.debug("Request to save DataOrganization : {}", dataOrganization);
        DataOrganization result = dataOrganizationRepository.save(dataOrganization);
        dataOrganizationSearchRepository.save(result);
        return result;
    }*/


    /**
     *  Get all the dataOrganizations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DataOrganization> findAll(Pageable pageable) {
        log.debug("Request to get all DataOrganizations");
        return dataOrganizationRepository.findAll(pageable);
    }

    /**
     *  Get one dataOrganization by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DataOrganization findOne(Long id) {
        log.debug("Request to get DataOrganization : {}", id);
        return dataOrganizationRepository.findOne(id);
    }

    /**
     *  Delete the  dataOrganization by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DataOrganization : {}", id);
        dataOrganizationRepository.delete(id);
        dataOrganizationSearchRepository.delete(id);
    }

    /**
     * Search for the dataOrganization corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DataOrganization> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DataOrganizations for query {}", query);
        Page<DataOrganization> result = dataOrganizationSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    @Transactional(readOnly = true)
    public Set<String> getDistinctCities()
    {
        return dataOrganizationRepository.getDistinctCities();
    }
}
