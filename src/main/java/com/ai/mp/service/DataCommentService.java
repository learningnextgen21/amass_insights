package com.ai.mp.service;
import com.ai.mp.repository.DataCommentRepository;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ai.mp.domain.DataComment;
import com.ai.mp.domain.DataProvider;

@Service
@Transactional
public class DataCommentService {
    private final Logger log = LoggerFactory.getLogger(DataCommentService.class);
    @Autowired
    private final DataCommentRepository dataCommentRepository;
    @Autowired
  //  private final DataCommentSearchRepository dataCommentSearchRepository;

    public DataCommentService(DataCommentRepository dataCommentRepository) {
        this.dataCommentRepository = dataCommentRepository;
   // this.dataCommentSearchRepository=dataCommentSearchRepository;
    }

   /* @Transactional(readOnly = true)
    public Page<DataComment> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DataComment for query {}", query);
        Page<DataComment> result = dataCommentSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }*/
}
