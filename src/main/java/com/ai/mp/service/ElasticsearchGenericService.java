package com.ai.mp.service;

import com.ai.core.Constants;
import com.ai.core.StrUtils;
import com.ai.mp.domain.*;
import com.ai.mp.repository.DataCategoryRepository;
import com.ai.mp.repository.DataCommentRepository;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.repository.DataProviderTagRepository;
import com.ai.mp.repository.DataResourceRepository;
import com.ai.mp.repository.EmailAddressRepository;
import com.ai.mp.repository.ExternalClientRepository;
import com.ai.mp.repository.UserProviderRepository;
import com.ai.mp.repository.UserRepository;
import com.ai.mp.repository.search.*;
import com.ai.mp.utils.BasicBean;
import com.ai.mp.web.rest.DataResource;
import com.ai.util.Utils;
import com.codahale.metrics.annotation.Timed;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.searchbox.client.JestClient;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.ai.core.Constants.INTEREST_TYPE_FOLLOW;
import static com.ai.core.Constants.INTEREST_TYPE_UNLOCK;
import static com.ai.mp.config.ElasticsearchConfiguration.*;
import static org.apache.commons.lang.time.DateUtils.addDays;
import static org.apache.commons.lang.time.DateUtils.addYears;

@Service
public class ElasticsearchGenericService {

    @PersistenceContext
    EntityManager entityManager;


    private final HttpServletRequest request;

    private final UserService userService;
    private final DataCategorySearchRepository categorySearchRepository;
    private final DataCategoryRepository categoryRepository;
    private final DataProviderSearchRepository dataProviderSearchRepository;
    private final DataEventSearchRepository dataEventSearchRepository;
    private final DataArticleSearchRepository dataArticleSearchRepository;
    private final DataFeatureSearchRepository dataFeatureSearchRepository;
    private final UserProviderService userProviderService;
    private final UserDataCategoryService userDataCategoryService;
    private final ExternalClientRepository externalClientRepository;
    private final DataProviderRepository dataProviderRepository;
    private final DataProviderTagRepository dataProviderTagRepository;
    private final DataCommentRepository dataCommentRepository;
    private final EmailAddressRepository emailAddressRepository;
    public ElasticsearchGenericService(ExternalClientRepository externalClientRepository, UserService userService, DataCategorySearchRepository categorySearchRepository, DataCategoryRepository categoryRepository, DataProviderSearchRepository dataProviderSearchRepository, DataEventSearchRepository dataEventSearchRepository, DataArticleSearchRepository dataArticleSearchRepository, DataFeatureSearchRepository dataFeatureSearchRepository, UserProviderService userProviderService,
    		UserDataCategoryService userDataCategoryService, DataProviderRepository dataProviderRepository,HttpServletRequest request,
    		DataProviderTagRepository dataProviderTagRepository,DataCommentRepository dataCommentRepository,EmailAddressRepository emailAddressRepository) {
        this.userService = userService;
        this.categorySearchRepository = categorySearchRepository;
        this.categoryRepository = categoryRepository;
        this.dataProviderSearchRepository = dataProviderSearchRepository;
        this.dataEventSearchRepository = dataEventSearchRepository;
        this.dataArticleSearchRepository = dataArticleSearchRepository;
        this.dataFeatureSearchRepository = dataFeatureSearchRepository;
        this.userProviderService = userProviderService;
        this.userDataCategoryService = userDataCategoryService;
        this.externalClientRepository = externalClientRepository;
        this.dataProviderRepository=dataProviderRepository;
        this.request =request;
        this.dataProviderTagRepository = dataProviderTagRepository;
        this.dataCommentRepository = dataCommentRepository;
        this.emailAddressRepository=emailAddressRepository;
    }

    @Autowired
    JestClient client;
    private final Logger log = LoggerFactory.getLogger(ElasticsearchGenericService.class);
private Long dataProviderIdMatch = null;
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Timed
    public JsonObject getCategoryFilterForCurrentUser(String query) {
        try {
            JsonObject result = searchIndex(query, INDEX_DATA_PROVIDER);
            if (userService.isAdminUser())
                return result;
            List<Long> categoryIDs = userService.getAllCategoryIDsForCurrentUser();
            // Set<Authority> userPermissions = userService.getLoginUserEffectiveRolesWithLevel();
            List<String> userRoles = userService.getLoginUserEffectiveRoles();
            String userPermission="'" + StringUtils.join(userRoles,"','")+ "'";
            //List<Long> categories= categoryRepository.getAllCatIDs(userPermission);
            List<Long> userPermissions=new ArrayList<>();
            Query q = entityManager.createNativeQuery("SELECT * FROM datamarketplace.data_category dc WHERE dc.permission_name in("+userPermission+")");
            List<?> authors = q.getResultList();
            Long check;
            for(int i=0; i<authors.size();i++) {
                Object[] row = (Object[]) authors.get(i);
                userPermissions.add(Long.parseLong(row[0].toString()));
            }

            JsonArray hitsArray = result.getAsJsonObject("aggregations").getAsJsonObject("providerfilter").getAsJsonArray("buckets");
            for (JsonElement e : hitsArray) {
                JsonObject obj = e.getAsJsonObject();
                JsonObject hits = obj.getAsJsonObject("filterelement").getAsJsonObject("hits").getAsJsonArray("hits").get(0).getAsJsonObject();
                JsonObject sourceObject = hits.getAsJsonObject("_source");
                long categoryID = obj.get("key").getAsLong();
                if (!userPermissions.contains(categoryID)) {
                    JsonObject innerObject = new JsonObject();
                    innerObject.addProperty(INDEX_DATA_CATEGORY, Utils.randomString(5, 15));
                    innerObject.addProperty(MASKED_TEXT, true);
                    //Overwrite the current _source element
                    sourceObject.add("mainDataCategory", innerObject);
                }
            }
            return result;
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Timed
    public JsonObject search(String query, String type) {

        return search(query, type, false, false, false);
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Timed
    public JsonObject categoriesSearch(String query, String type) {
        Gson gson = new Gson();

        JsonObject result = searchIndex(query, type);
        if (result == null) {
            log.error("Failed to retrieve elasticsearch result for query: {}", query);
            return new JsonObject();
        }
        JsonArray hitsArray = result.getAsJsonObject("hits").getAsJsonArray("hits");

        if (!RESTRICTED_INDEXES.contains(type)) {
            return result;
        }
        for (JsonElement e : hitsArray) {
            JsonObject obj = e.getAsJsonObject();
            JsonObject sourceObject = obj.getAsJsonObject("_source");
            if (INDEX_DATA_CATEGORY.equals(type)) {
                if (UserService.isAdminUser()) {
                    continue;
                }
                DataCategory category = gson.fromJson(sourceObject, DataCategory.class);
                obj.add("_source", gson.toJsonTree(category));
            }
        }
        return result;
    }

    private String generateUnlockedFilter(String query, List<Long> unlockedProviders, List<Long> overrideCategoryIDs) {
        Gson gson = new Gson();
        JsonElement element = gson.fromJson(query, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        JsonElement filter = jsonObj.get("filter");
        List<String> userPermissions = userService.getLoginUserEffectiveRoles();
        if (filter != null) {
            JsonElement bool = filter.getAsJsonObject().get("bool");
            if (bool == null) {
                JsonElement queryElement = filter.getAsJsonObject().get("query");
                if (queryElement != null) {
                    bool = queryElement.getAsJsonObject().get("bool");
                }
            }
            if (bool != null) {
                JsonElement should = bool.getAsJsonObject().get("should");
                if (should != null) {
                    if (should.isJsonArray()) {
                        addUserPermissions(userPermissions, gson, should, true, true);
                        if (!unlockedProviders.isEmpty()) {
                            JsonElement providerIDs = gson.fromJson("{\"terms\": {\"id\":[" + unlockedProviders.toString() + "]}", JsonElement.class);
                            should.getAsJsonArray().add(providerIDs);
                        }
                    } else {
                        //Don't we need to add user permission in this block?????
                        if (!unlockedProviders.isEmpty()) {
                            JsonElement providerIDs = gson.fromJson("{\"id\": [" + unlockedProviders.toString() + "]}", JsonElement.class);
                            JsonArray terms = new JsonArray();
                            terms.add(providerIDs);
                            terms.add(should.getAsJsonObject());
                            bool.getAsJsonObject().add("terms", terms);
                        }
                    }
                } else {
                    JsonArray shouldElement = gson.fromJson("[]", JsonArray.class);
                    if (!unlockedProviders.isEmpty()) {
                        JsonElement terms = gson.fromJson("{\"terms\": {\"id\":" + unlockedProviders.toString() + "}}", JsonElement.class);
                        shouldElement.add(terms);
                    }

                    for (String permission : userPermissions) {
                        JsonElement providerPermissionElement = gson.fromJson("{\"match\": {\"userPermission\":" + permission + "}}", JsonElement.class);
                        shouldElement.add(providerPermissionElement);
                    }
                    //2018-02-21:RP: Only unlocked providers will be showing when this filter is selected, unlocked categories are not to be shown
                    if (!overrideCategoryIDs.isEmpty()) {
                        addCategoriesFilter(overrideCategoryIDs, shouldElement, gson);
                    }
                    bool.getAsJsonObject().add("should", shouldElement);
                }
            }
        } else {
            filter = gson.fromJson("{\"query\":{\"bool\":{\"should\":[]}}}", JsonElement.class);
            JsonArray shouldElement = filter.getAsJsonObject().get("query").getAsJsonObject().get("bool").getAsJsonObject().get("should").getAsJsonArray();
            if (!unlockedProviders.isEmpty()) {
                JsonElement terms = gson.fromJson("{\"terms\": {\"id\":" + unlockedProviders.toString() + "}}", JsonElement.class);
                shouldElement.add(terms);
            }
            if (!overrideCategoryIDs.isEmpty()) {
                addCategoriesFilter(overrideCategoryIDs, shouldElement, gson);
            }
            addUserPermissions(userPermissions, gson, shouldElement, true, false);
            jsonObj.add("filter", filter);
        }
        return jsonObj.toString();
    }

    private void addUserPermissions(List<String> userPermissions, Gson gson, JsonElement should, boolean addProviderPermission, boolean addCategoryPermission) {
        for (String permission : userPermissions) {
            if (addProviderPermission) {
                JsonElement providerPermissionElement = gson.fromJson("{\"match\": {\"userPermission\":" + permission + "}}", JsonElement.class);
                should.getAsJsonArray().add(providerPermissionElement);
            }
            if (addCategoryPermission) {
                JsonElement categoryPermissionElement = gson.fromJson("{\"match\": {\"mainDataCategory.userPermission\":" + permission + "}}", JsonElement.class);
                should.getAsJsonArray().add(categoryPermissionElement);
            }
        }
    }

    private String generateFollowedFilter(String query, List<Long> followedProviders) {
        Gson gson = new Gson();
        JsonElement element = gson.fromJson(query, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        JsonElement filter = jsonObj.get("filter");

        if (filter != null) {
            JsonElement bool = filter.getAsJsonObject().get("bool");
            if (bool == null) {
                JsonElement queryElement = filter.getAsJsonObject().get("query");
                if (queryElement != null) {
                    bool = queryElement.getAsJsonObject().get("bool");
                }
            }
            if (bool != null) {
                JsonElement must = bool.getAsJsonObject().get("must");
                if (must != null) {
                    if (must.isJsonArray()) {
                        JsonElement providerIDs = gson.fromJson("{\"terms\": {\"id\":" + followedProviders.toString() + "}}", JsonElement.class);
                        must.getAsJsonArray().add(providerIDs);
                    } else {
                        JsonElement providerIDs = gson.fromJson("{\"terms\": {\"id\":" + followedProviders.toString() + "}}", JsonElement.class);
                        JsonArray mustArray = new JsonArray();
                        mustArray.add(providerIDs);
                        mustArray.add(must.getAsJsonObject());
                        bool.getAsJsonObject().add("must", mustArray);
                    }
                } else {
                    JsonElement providerIDs = gson.fromJson("[{\"terms\": {\"id\":" + followedProviders.toString() + "}}]", JsonArray.class);
                    bool.getAsJsonObject().add("must", providerIDs);
                }
            }
        } else {
            filter = gson.fromJson("{\"query\":{\"bool\":{\"must\":[{\"terms\": {\"id\":" + followedProviders.toString() + "}}]}}}", JsonElement.class);
            jsonObj.add("filter", filter);
        }
        return jsonObj.toString();
    }
    @Transactional(readOnly = false)
    @SuppressWarnings("unchecked")
    @Timed
    public JsonObject providerTagsearch(String query, String type, boolean followedFilter, boolean unlockedProviders, boolean detail) {
    	 Gson gson = new Gson();

         JsonObject result = searchIndex(query, type);
         return result;
    }
    @Transactional(readOnly = false)
    @SuppressWarnings("unchecked")
    @Timed
    public JsonObject search(String query, String type, boolean followedFilter, boolean unlockedProviders, boolean detail) {
        Gson gson = new Gson();
        User user = userService.getUser();
        if(null!=request.getAttribute("myMatches"))
        {
       boolean myMatch = (boolean) request.getAttribute("myMatches");
        if(myMatch)
        {
        	myMatchesAdd(myMatch,user);
        }
        }
        List<UserProvider> userProviderInterests = userProviderService.findAllProviderInterestsByUserID();
        Map<Long, List<Integer>> userProviderInterestMap = new HashMap<>();
        for (UserProvider up : userProviderInterests) {
            List<Integer> it = userProviderInterestMap.get(up.getProviderID());
            if (it != null) {
                it.add(up.getInterestType());
            } else {
                it = new ArrayList<>();
                it.add(up.getInterestType());
            }
            userProviderInterestMap.put(up.getProviderID(), it);
        }
        List<Long> followedProviderIDs = new ArrayList<>(),
            unlockedProviderIDs = new ArrayList<>();
        List<Long> overrideCategoryIDs = userService.getOverrideCategoryIDsForCurrentUser();
       // System.out.println("overrideCategoryIDs"+overrideCategoryIDs);
        if (INDEX_DATA_PROVIDER.equals(type)) {
            followedProviderIDs = userProviderService.getProvidersByInterestTypeForCurrentUser(INTEREST_TYPE_FOLLOW);
            unlockedProviderIDs = userProviderService.getProvidersByInterestTypeForCurrentUser(INTEREST_TYPE_UNLOCK);
            //System.out.println("unlockedProvider"+unlockedProviderIDs);
            if (followedFilter) {
                query = generateFollowedFilter(query, followedProviderIDs);
            } else if (unlockedProviders) {
                query = generateUnlockedFilter(query, unlockedProviderIDs, overrideCategoryIDs);
            }
        }

        JsonObject result = searchIndex(query, type);
        if (result == null) {
            log.error("Failed to retrieve elasticsearch result for query: {}", query);
            return new JsonObject();
        }
       // System.out.println("query"+query);

        if (!RESTRICTED_INDEXES.contains(type)) {
            return result;
        }

        List<Long> categoryIDs = userService.getAllCategoryIDsForCurrentUser();
        List<String> userRoles = userService.getLoginUserEffectiveRoles();
       //System.out.println("userRoles"+userRoles);
     //  System.out.println("result"+result);
            JsonArray hitsArray = result.getAsJsonObject("hits").getAsJsonArray("hits");
        
        for (JsonElement e : hitsArray) {
            JsonObject obj = e.getAsJsonObject();
            JsonObject sourceObject = obj.getAsJsonObject("_source");
            long categoryID;
            if (INDEX_DATA_CATEGORY.equals(type)) {
                if (UserService.isAdminUser()) {
                    continue;
                }
                DataCategory category = gson.fromJson(sourceObject, DataCategory.class);
                categoryID = category.getId();
                String categoryPermission=category.getUserPermission();
                List<Long> unlockedDataCategoryIDs = new ArrayList<>();
                unlockedDataCategoryIDs=userDataCategoryService.getDataCategoryByInterestTypeForCurrentUser(INTEREST_TYPE_UNLOCK);
                if (!userRoles.contains(categoryPermission) && !unlockedDataCategoryIDs.contains(categoryID)) {
                    category.setDatacategory(Utils.randomString(5, 15));
                    category.setLocked(true);
                    //Overwrite the current _source element
                    obj.add("_source", gson.toJsonTree(category));
                }
                /*if (!categoryIDs.contains(categoryID)) {
                    category.setDatacategory(Utils.randomString(5, 15));
                    category.setLocked(true);
                    //Overwrite the current _source element
                    obj.add("_source", gson.toJsonTree(category));
                }*/
            }
            else if (INDEX_DATA_INVESTMENT_MANAGERS.equals(type)) {
                if (UserService.isAdminUser()) {
                    continue;
                }
                DataInvestmentManager investmentManagers = gson.fromJson(sourceObject, DataInvestmentManager.class);
                obj.add("_source", gson.toJsonTree(investmentManagers));
            }
            else if (INDEX_DATA_EMAILS.equals(type)) {
                if (UserService.isAdminUser()) {
                    continue;
                }
                Emails emails = gson.fromJson(sourceObject, Emails.class);
                obj.add("_source", gson.toJsonTree(emails));
            }
            else if (INDEX_DATA_USERS.equals(type)) {
                if (UserService.isAdminUser()) {
                    continue;
                }
                User users = gson.fromJson(sourceObject, User.class);
                obj.add("_source", gson.toJsonTree(users));
            }
            else if (INDEX_DATA_DOMAINS.equals(type)) {
                if (UserService.isAdminUser()) {
                    continue;
                }
                Domains domains = gson.fromJson(sourceObject, Domains.class);
                obj.add("_source", gson.toJsonTree(domains));
            }
            
            else if (INDEX_DATA_ARTICLE.equals(type)) {
                if (UserService.isAdminUser()) {
                    continue;
                }
                unlockedProviderIDs = userProviderService.getProvidersByInterestTypeForCurrentUser(INTEREST_TYPE_UNLOCK);
                //System.out.println(DataArticle dataArticles = gson.fromJson(sourceObject, DataArticle.class););
                DataArticle dataArticles = gson.fromJson(sourceObject, DataArticle.class);
                DataArticle article=new DataArticle();
                article.setId(dataArticles.getId());
                article.setArticleTopics(dataArticles.getArticleTopics());
                article.setAuthor(dataArticles.getAuthor());
                article.setContent(dataArticles.getContent());
                article.setContentHTML(dataArticles.getContentHTML());
                article.setCreatedDate(dataArticles.getCreatedDate());
                article.setPublishedDate(dataArticles.getPublishedDate());
                article.setPublisher(dataArticles.getPublisher());
                article.setPurpose(dataArticles.getPurpose());
                article.setPurposeType(dataArticles.getPurposeType());
                article.setRecordID(dataArticles.getRecordID());
                article.setStream(dataArticles.getStream());
                article.setSummary(dataArticles.getSummary());
                article.setSummaryHTML(dataArticles.getSummaryHTML());
                article.setTitle(dataArticles.getTitle());
                article.setUpdatedDate(dataArticles.getUpdatedDate());
                article.setUrl(dataArticles.getUrl());
                article.setArticleuniquekey(dataArticles.getArticleuniquekey());
                article.setAuthorDataProvider(dataArticles.getAuthorDataProvider());
                article.setAuthorOrganization(dataArticles.getAuthorOrganization());
                article.setIncludeInNewsletter(dataArticles.isIncludeInNewsletter());
                article.setRelevancy(dataArticles.getRelevancy());
                article.setVisualUrl(dataArticles.getVisualUrl());
                Set<DataProvider> relevantProviders = new HashSet<>();

                // System.out.println(dataArticles.getRelevantProviders());

                for (DataProvider a:dataArticles.getRelevantProviders()){
                    String providerPermission = a.getUserPermission();
                    if(!userRoles.contains(providerPermission) && !unlockedProviderIDs.contains(a.getId()))
                    {
                        a.setProviderName(Utils.randomString(5, 15));
                        a.setLocked(true);
                    }/*else if (unlockedProviderIDs.contains(a.getId())) {
                        a.setLocked(false);
                        System.out.println("relevantProviders2");
                    } else {
                        a.setLocked(false);
                        System.out.println("relevantProviders3");
                    }*/
                    relevantProviders.add(a);
                }

                article.setRelevantProviders(relevantProviders);
                dataArticles=article;
                obj.add("_source", gson.toJsonTree(dataArticles));
            }else if (INDEX_DATA_RESOURCE.equals(type)) {
                if (UserService.isAdminUser()) {
                    continue;
                }
                DataResources dataResources = gson.fromJson(sourceObject, DataResources.class);
                DataResources resources=new DataResources();
                resources.setId(dataResources.getId());
                resources.setRecordID(dataResources.getRecordID());
                resources.setName(dataResources.getName());
                resources.setDescription(dataResources.getDescription());
                resources.setNotes(dataResources.getNotes());
                resources.setPriority(dataResources.getPriority());
                resources.setMarketplaceStatus(dataResources.getMarketplaceStatus());
                resources.setIeiStatus(dataResources.getIeiStatus());
                resources.setUserPermission(dataResources.getUserPermission());
                resources.setLastUpdatedDate(dataResources.getLastUpdatedDate());
                resources.setCreatedTimeFormula(dataResources.getCreatedTimeFormula());
                resources.setCreatorUserId(dataResources.getCreatorUserId());
                resources.setEditorUserId(dataResources.getEditorUserId());
                resources.setAuthorDataProvider(dataResources.getAuthorDataProvider());
                resources.setAuthorOrganization(dataResources.getAuthorOrganization());
                resources.setLink(dataResources.getLink());
                resources.setFile(dataResources.getFile());
                resources.setWorkflows(dataResources.getWorkflows());
                resources.setPurposes(dataResources.getPurposes());
                resources.setPurposeTypes(dataResources.getPurposeTypes());
                resources.setTopics(dataResources.getTopics());
                resources.setResearchMethodsCompleted(dataResources.getResearchMethodsCompleted());
                resources.setRelavantDataProviders(dataResources.getRelavantDataProviders());
                resources.setRelavantOrganizations(dataResources.getRelavantOrganizations());
                dataResources=resources;
                obj.add("_source", gson.toJsonTree(dataResources));
            } else if (INDEX_DATA_PROVIDER.equals(type)) {
                DataProvider dataProvider = gson.fromJson(sourceObject, DataProvider.class);
                long providerID = dataProvider.getId();
                boolean followed = followedProviderIDs.contains(providerID);
                dataProvider.setFollowed(followed);
                dataProviderIdMatch = dataProvider.getId();
                if(null!=dataProvider.getConsumerPartners() && null!=dataProvider.getDistributionPartners() && null!=dataProvider.getProviderPartners())
                {
                    Long partners=(long) (dataProvider.getConsumerPartners().size()+dataProvider.getDistributionPartners().size()+dataProvider.getProviderPartners().size());
                    if(null!=partners)
                    {
                        dataProvider.setPartnersCount(partners);
                    }
                }

              if(null!= dataProvider.getProviderTags())
              {
            	  if (dataProvider.getProviderTags() != null) {
                      List<BasicBean> providersTags = dataProvider.getProviderTags();
                      Iterator<BasicBean> itr = providersTags.iterator();
                      while (itr.hasNext()) {
                          BasicBean tag = itr.next();
                          long cid = Long.parseLong(tag.getId());

                        //  List<DataProviderTag> Tags = new ArrayList<>();
                         // long tagCheck = 0;
                         // DataProviderTag tagList = new DataProviderTag();
                          Query q = entityManager.createNativeQuery("SELECT dt.id,dt.provider_tag,dt.privacy,dt.record_id FROM datamarketplace.data_provider_tag dt WHERE ((dt.by_user_id=(" + user.getId() + ") AND dt.privacy='private') OR (dt.privacy='Public')) and dt.id in(" + cid + ")");
                          List<?> authors = q.getResultList();
                          if (authors.size() == 0) {
                              itr.remove();
                              continue;
                          }


                      }
                  }
            	  Long providerTagCount = (long) dataProvider.getProviderTags().size();
            	  if(null!=providerTagCount)
            	  {
                      dataProvider.setProviderTagCount(providerTagCount);
            	  }
              }
            Long resourceCount = dataProviderRepository.findResourceCount(dataProvider.getId());
            Long ArticleCount = dataProviderRepository.findRelavantArticleCount(dataProvider.getId());
            if(null!=resourceCount && null!=ArticleCount)
            {
            	Long relavantResourceArticle = resourceCount+ ArticleCount;
                dataProvider.setRelavantResourceArticle(relavantResourceArticle);	
            }
            List<DataComment> commentsCount = dataCommentRepository.getCommentByRecordID(dataProvider.getId(),user.getId());
            if(null!=commentsCount)
            {
                dataProvider.setCommentsCount(Long.valueOf(commentsCount.size()));
            }
            if (UserService.isAdminUser()) {
            	if(detail) {
            		List<UserProvider>users=userProviderService.findByProviderIdMatchesPermission(dataProvider.getId());
                	if(null!=users)
                	{
                		List<String>usersList=new ArrayList<String>();
                		for(UserProvider userProvider:users)
                		{
                        	//System.out.println("email"+userProvider.getUser().getEmail());
                        	usersList.add(userProvider.getUser().getEmail());
                		}
                    	dataProvider.setUser(usersList);
                	}
            	}
            	
            	if(detail)
            	{
            		Set<EmailAddress> emailAddress=emailAddressRepository.findByEmailAdrressProviderId(dataProvider.getId());
                	if(null!=emailAddress) {
                		//System.out.println("emailAddress"+emailAddress.size());
                	    dataProvider.setEmailAddress(emailAddress);
                	}
            	}
            	
                obj.add("_source", gson.toJsonTree(dataProvider));
            }
              if (!UserService.isAdminUser()) {
                    //Ranjith commented below if condition
                    /*if (detail) {
                        String orgRelationshipStatus = dataProvider.getOrgRelationshipStatus();
                        if (StrUtils.isNotBlank(orgRelationshipStatus)) {
                            dataProvider.getConsumerPartners().clear();
                            dataProvider.getDistributionPartners().clear();
                            dataProvider.getProviderPartners().clear();
                        }
                    }*/
                    int operatorType= Constants.INTEREST_TYPE_OPERATOR;
                    UserProvider userProvider= userProviderService.findOneByUserIDEqualsAndProviderIDEqualsAndInterestType(dataProvider.getId(),operatorType);
                    // OLD Code   UserProvider userProvider= userProviderService.findOneByUserIDEqualsAndProviderID(dataProvider.getId());

                    if(userProvider!=null)
                    {
                        List<Integer> interestType = userProviderInterestMap.get(providerID);
                        if (interestType != null) {
                            dataProvider.setUserProviderStatus(interestType);
                        }
                        obj.add("_source", gson.toJsonTree(dataProvider));
                        continue;
                    }

                    String providerPermission = dataProvider.getUserPermission();
                    List<Long> categories = new ArrayList<>();
                    if (dataProvider.getCategories() != null) {
                        categories = dataProvider.getCategories().stream().map(BasicBean::getId).map(Long::parseLong).collect(Collectors.toList());
                    }


                    if (unlockedProviderIDs.contains(providerID)) {
                        dataProvider.setProviderPermissionOverride(true);
                    }

                    //Locked Provider
                    DataCategory mainDataCategory = dataProvider.getMainDataCategory();
                    if (mainDataCategory != null) {
                        categoryID = mainDataCategory.getId();

                        /* permission hide for my matches button
                         *  if (overrideCategoryIDs.contains(categoryID) || !Collections.disjoint(categories, overrideCategoryIDs)) {
                        	//System.out.println("true");
                            dataProvider.setCategoryPermissionOverride(true);
                            dataProvider.setProviderPermissionOverride(true);
                        }*/

/*               permission hide for my matches button
 *         if (!userRoles.contains(providerPermission) && !unlockedProviderIDs.contains(providerID) && !overrideCategoryIDs.contains(categoryID) && Collections.disjoint(categories, overrideCategoryIDs)) {
*/
                       /* System.out.println("type"+type);
                        System.out.println("permission"+providerPermission);
                        System.out.println("userroles"+userRoles);
                        System.out.println("unlockedProviderIDs"+unlockedProviderIDs);
                        System.out.println("providerid"+providerID);*/
                        if (!userRoles.contains(providerPermission) && !unlockedProviderIDs.contains(providerID)) {
                        	dataProvider.setProviderName(Utils.randomString(5, 15));
                            dataProvider.setLocked(true);

                            dataProvider.setLogo(null);
                            if (detail) {
                                DataProvider provider = new DataProvider();

                                provider.setFollowed(followed);

                                if (!categoryIDs.contains(categoryID)) {
                                    mainDataCategory.setDatacategory(Utils.randomString(5, 15));
                                    mainDataCategory.setLocked(true);
                                }
                                provider.setShortDescription(dataProvider.getShortDescription());
                                provider.setMainDataCategory(mainDataCategory);

                                if (!StrUtils.isBlank(dataProvider.getLongDescription())) {
                                    provider.setLongDescription(Utils.randomString(5, 15));
                                }

                                provider.setCategories(dataProvider.getCategories());

                                provider.setMainDataIndustry(dataProvider.getMainDataIndustry());
                                provider.setGeographicalFoci(dataProvider.getGeographicalFoci());
                                provider.setProviderName(Utils.randomString(5, 15));

                                provider.setCreatedDate(dataProvider.getCreatedDate());
                                provider.setUpdatedDate(dataProvider.getUpdatedDate());

                               /* if (!StrUtils.isBlank(dataProvider.getHistoricalDateRange())) {
                                    provider.setHistoricalDateRange(Utils.randomString(5, 15));
                                }*/
                                if (!StrUtils.isBlank(dataProvider.getHistoricalDateRange())) {
                                    provider.setHistoricalDateRange(dataProvider.getHistoricalDateRange());
                                }
                                if (!StrUtils.isBlank(dataProvider.getCollectionMethodsExplanation())) {
                                    provider.setCollectionMethodsExplanation(Utils.randomString(5, 15));
                                }

                                if (!StrUtils.isBlank(dataProvider.getDateCollectionRangeExplanation())) {
                                    provider.setDateCollectionRangeExplanation(Utils.randomString(5, 15));
                                }

                             /*   if (!StrUtils.isBlank(dataProvider.getDataUpdateFrequency())) {
                                    provider.setDataUpdateFrequency(Utils.randomString(5, 15));
                                }*/
                                if (!StrUtils.isBlank(dataProvider.getDataUpdateFrequency())) {
                                    provider.setDataUpdateFrequency(dataProvider.getDataUpdateFrequency());

                                }
                           /* if (!StrUtils.isBlank(dataProvider.getPricingModels())) {
                                List<BasicBean> pricingModel = dataProvider.getPricingModels();
                                for (BasicBean b : pricingModel) {
                                    b.setDesc(Utils.randomString(5, 15));
                                }
                                provider.setPricingModels(pricingModel);
                            }*/
/*
                                if (!StrUtils.isBlank(dataProvider.getPotentialDrawbacks())) {
                                    provider.setPotentialDrawbacks(Utils.randomString(5, 15));
                                }*/
                                if (!StrUtils.isBlank(dataProvider.getPotentialDrawbacks())) {
                                    provider.setPotentialDrawbacks(dataProvider.getPotentialDrawbacks());
                                }

                                /*if (!StrUtils.isBlank(dataProvider.getUniqueValueProps())) {
                                    provider.setUniqueValueProps(Utils.randomString(5, 15));
                                }*/
                                if (!StrUtils.isBlank(dataProvider.getUniqueValueProps())) {
                                    provider.setUniqueValueProps(dataProvider.getUniqueValueProps());
                                }

                                /*if (!StrUtils.isBlank(dataProvider.getScoreInvestorsWillingness().getDescription())) {
                                    provider.setScoreInvestorsWillingness(new LookupCode(Utils.randomString(5, 15)));
                                }*/
                                if (!StrUtils.isBlank(dataProvider.getScoreInvestorsWillingness().getDescription())) {
                                    provider.setScoreInvestorsWillingness(dataProvider.getScoreInvestorsWillingness());
                                }

                             /*   if (!StrUtils.isBlank(dataProvider.getScoreUniqueness().getDescription())) {
                                    provider.setScoreUniqueness(new LookupCode(Utils.randomString(5, 15)));
                                }*/
                                if(null!=dataProvider.getScoreUniqueness() && null!=dataProvider.getScoreUniqueness().getDescription())    
                                {
                                	if (!StrUtils.isBlank(dataProvider.getScoreUniqueness().getDescription())) {
                                        provider.setScoreUniqueness(dataProvider.getScoreUniqueness());
                                    }
                                }
                                
                               /* if (!StrUtils.isBlank(dataProvider.getScoreValue().getDescription())) {
                                    provider.setScoreValue(new LookupCode(Utils.randomString(5, 15)));
                                }*/
                                if(null!=dataProvider.getScoreValue() && null!=dataProvider.getScoreValue().getDescription())    
                                {
                                	if (!StrUtils.isBlank(dataProvider.getScoreValue().getDescription())) {
                                        provider.setScoreValue(dataProvider.getScoreValue());
                                    }
                                }
                                
                                /*if (!StrUtils.isBlank(dataProvider.getScoreReadiness().getDescription())) {
                                    provider.setScoreReadiness(new LookupCode(Utils.randomString(5, 15)));
                                }*/
                                if(null!=dataProvider.getScoreReadiness() && null!=dataProvider.getScoreReadiness().getDescription()) 
                                {
                                	 if (!StrUtils.isBlank(dataProvider.getScoreReadiness().getDescription())) {
                                         provider.setScoreReadiness(dataProvider.getScoreReadiness());
                                     }
                                }
                               
                               /* if (!StrUtils.isBlank(dataProvider.getScoreOverall().getDescription())) {
                                    provider.setScoreOverall(new LookupCode(Utils.randomString(5, 15)));
                                }*/
                                if(null!=dataProvider.getScoreOverall() && null!=dataProvider.getScoreOverall().getDescription()) 
                                {
                                	 if (!StrUtils.isBlank(dataProvider.getScoreOverall().getDescription())) {
                                         provider.setScoreOverall(dataProvider.getScoreOverall());
                                     }
                                }
                               

                                provider.setCompleteness(dataProvider.getCompleteness());
                                provider.setFreeTrialAvailable(dataProvider.getFreeTrialAvailable());
                                provider.setLocked(true);

                                //Don't show partner information for locked provider
                                provider.getConsumerPartners().clear();
                                provider.getDistributionPartners().clear();
                                provider.getProviderPartners().clear();

                                if (!StrUtils.isBlank(dataProvider.getInvestor_clients())) {
                                    provider.setInvestor_clients(Utils.randomString(5, 15));
                                }

                                DataOrganization dataOrganization = dataProvider.getOwnerOrganization();

                                if (dataOrganization != null) {
                                    DataOrganization parent = dataOrganization.getParentOrganization();

                                    if (parent != null) {
                                        if (!StrUtils.isBlank(parent.getName())) {
                                            parent.setName(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(parent.getHeadCount())) {
                                            parent.setHeadCount(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(parent.getYearFounded())) {
                                            parent.setYearFounded(1970);
                                        }

                                        if (!StrUtils.isBlank(parent.getCity())) {
                                            parent.setCity(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(parent.getState())) {
                                            parent.setState(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(parent.getZipCode())) {
                                            parent.setZipCode(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(parent.getCountry())) {
                                            parent.setCountry(Utils.randomString(5, 15));
                                        }

                                      /*  if (!StrUtils.isBlank(parent.getFinancialPosition())) {
                                            parent.setFinancialPosition(Utils.randomString(5, 15));
                                        }
                                        if (!StrUtils.isBlank(parent.getClientBase())) {
                                            parent.setClientBase(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(parent.getFacebook())) {
                                            parent.setFacebook(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(parent.getTwitter())) {
                                            parent.setTwitter(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(parent.getGithub())) {
                                            parent.setGithub(Utils.randomString(5, 15));
                                        }
                                        if (!StrUtils.isBlank(parent.getGithub())) {
                                            parent.setGithub(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(parent.getWebsite())) {
                                            parent.setWebsite(Utils.randomString(5, 15));
                                        }
                                        if (!StrUtils.isBlank(parent.getNotes())) {
                                            parent.setNotes(Utils.randomString(5, 15));
                                        }
                                    }
                                    dataOrganization.setParentOrganization(parent);*/

                                        if (!StrUtils.isBlank(dataOrganization.getHeadCount())) {
                                            dataOrganization.setHeadCount(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(dataOrganization.getName())) {
                                            dataOrganization.setName(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(dataOrganization.getYearFounded())) {
                                            dataOrganization.setYearFounded(1970);
                                        }

                                        if (!StrUtils.isBlank(dataOrganization.getCity())) {
                                            dataOrganization.setCity(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(dataOrganization.getState())) {
                                            dataOrganization.setState(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(dataOrganization.getZipCode())) {
                                            dataOrganization.setZipCode(Utils.randomString(5, 15));
                                        }

                                        if (!StrUtils.isBlank(dataOrganization.getCountry())) {
                                            dataOrganization.setCountry(Utils.randomString(5, 15));
                                        }

                                  /*  if (!StrUtils.isBlank(dataOrganization.getFinancialPosition())) {
                                        dataOrganization.setFinancialPosition(Utils.randomString(5, 15));
                                    }
                                    if (!StrUtils.isBlank(dataOrganization.getClientBase())) {
                                        dataOrganization.setClientBase(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataOrganization.getFacebook())) {
                                        dataOrganization.setFacebook(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataOrganization.getTwitter())) {
                                        dataOrganization.setTwitter(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataOrganization.getGithub())) {
                                        dataOrganization.setGithub(Utils.randomString(5, 15));
                                    }
                                    if (!StrUtils.isBlank(dataOrganization.getGithub())) {
                                        dataOrganization.setGithub(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataOrganization.getWebsite())) {
                                        dataOrganization.setWebsite(Utils.randomString(5, 15));
                                    }
                                    if (!StrUtils.isBlank(dataOrganization.getNotes())) {
                                        dataOrganization.setNotes(Utils.randomString(5, 15));
                                    }*/
                                    }
                                   /* if (!StrUtils.isBlank(dataProvider.getLegalAndComplianceIssues())) {
                                        provider.setLegalAndComplianceIssues(Utils.randomString(5, 15));
                                    }*/
                                    if (!StrUtils.isBlank(dataProvider.getLegalAndComplianceIssues())) {
                                        provider.setLegalAndComplianceIssues(dataProvider.getLegalAndComplianceIssues());
                                    }

                                    /*if (!StrUtils.isBlank(dataProvider.getNotes())) {
                                        provider.setNotes(Utils.randomString(5, 15));
                                    }*/

                                    if (!StrUtils.isBlank(dataProvider.getNotes())) {
                                        provider.setNotes(dataProvider.getNotes());
                                    }
                                  /*  if (!StrUtils.isBlank(dataProvider.getCompetitiveDifferentiators())) {
                                        provider.setCompetitiveDifferentiators(Utils.randomString(5, 15));
                                    }*/
                                    if (!StrUtils.isBlank(dataProvider.getCompetitiveDifferentiators())) {
                                        provider.setCompetitiveDifferentiators(dataProvider.getCompetitiveDifferentiators());
                                    }

                                  /*  if (!StrUtils.isBlank(dataProvider.getUseCasesOrQuestionsAddressed())) {
                                        provider.setUseCasesOrQuestionsAddressed(Utils.randomString(5, 15));
                                    }
*/
                                    if (!StrUtils.isBlank(dataProvider.getUseCasesOrQuestionsAddressed())) {
                                        provider.setUseCasesOrQuestionsAddressed(dataProvider.getUseCasesOrQuestionsAddressed());
                                    }
                                    /*if (!StrUtils.isBlank(dataProvider.getDataRoadmap())) {
                                        provider.setDataRoadmap(Utils.randomString(5, 15));
                                    }*/
                                    if (!StrUtils.isBlank(dataProvider.getDataRoadmap())) {
                                        provider.setDataRoadmap(dataProvider.getDataRoadmap());
                                    }
                                    if (dataProvider.getProfileVerificationStatus() != null && !StrUtils.isBlank(dataProvider.getProfileVerificationStatus().getDescription())) {
                                        provider.setProfileVerificationStatus(new LookupCode(Utils.randomString(5, 15)));
                                    }

                                    if (!StrUtils.isBlank(dataProvider.getDataSourcesDetails())) {
                                        provider.setDataSourcesDetails(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataProvider.getSampleOrPanelSize())) {
                                        provider.setSampleOrPanelSize(Utils.randomString(5, 15));
                                    }

                                 /*   if (!StrUtils.isBlank(dataProvider.getPublicEquitiesCoveredCount())) {
                                        provider.setPublicEquitiesCoveredCount(1970);
                                    }*/
                                    if (!StrUtils.isBlank(dataProvider.getPublicEquitiesCoveredCount())) {
                                        provider.setPublicEquitiesCoveredCount(dataProvider.getPublicEquitiesCoveredCount());
                                    }
                                    if (!StrUtils.isBlank(dataProvider.getYearFirstDataProductLaunched())) {
                                        provider.setYearFirstDataProductLaunched(1970);
                                    }

                                /*if (!StrUtils.isBlank(dataProvider.getHistoricalDateRangeEstimate())) {
                                    provider.setHistoricalDateRangeEstimate(Utils.randomString(5, 15));
                                }*/

                                    if (!StrUtils.isBlank(dataProvider.getCollectionMethodsExplanation())) {
                                        provider.setCollectionMethodsExplanation(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataProvider.getKeyDataFields())) {
                                        provider.setKeyDataFields(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataProvider.getDataUpdateFrequencyNotes())) {
                                        provider.setDataUpdateFrequencyNotes(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataProvider.getDataLicenseTypeDetails())) {
                                        provider.setDataLicenseTypeDetails(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataProvider.getMinimumYearlyPrice())) {
                                        provider.setMinimumYearlyPrice(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataProvider.getMaximumYearlyPrice())) {
                                        provider.setMaximumYearlyPrice(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataProvider.getPricingDetails())) {
                                        provider.setPricingDetails(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataProvider.getTrialDuration())) {
                                        provider.setTrialDuration(Utils.randomString(5, 15));
                                    }

                                    if (dataProvider.getTrialAgreementType() != null && !StrUtils.isBlank(dataProvider.getTrialAgreementType().getDescription())) {
                                        provider.setTrialAgreementType(new LookupCode(Utils.randomString(5, 15)));
                                    }

                                    if (!StrUtils.isBlank(dataProvider.getTrialPricingDetails())) {
                                        provider.setTrialPricingDetails(Utils.randomString(5, 15));
                                    }

                                    if (!StrUtils.isBlank(dataProvider.getProductDetails())) {
                                        provider.setProductDetails(Utils.randomString(5, 15));
                                    }
                                    provider.setSummary(dataProvider.getSummary());
                                    provider.setPublicCompaniesCovered(dataProvider.getPublicCompaniesCovered());
                                    provider.setDeliveryFrequencyNotes(dataProvider.getDeliveryFrequencyNotes());
                                    provider.setDeliveryMethods(dataProvider.getDeliveryMethods());
                                    provider.setDeliveryFormats(dataProvider.getDeliveryFormats());
                                    provider.setDeliveryFrequencies(dataProvider.getDeliveryFrequencies());
                                    //provider.setPricingModel(dataProvider.getPricingModel());
                                    provider.setdataProvidersPricingModels(dataProvider.getdataProvidersPricingModels());
                                    provider.setSecRegulated(dataProvider.getSecRegulated());
                                    provider.setPublicEquitiesCoveredRange(dataProvider.getPublicEquitiesCoveredRange());
                                    provider.setTrialPricingModel(dataProvider.getTrialPricingModel());
                                    provider.setLagTime(dataProvider.getLagTime());
                                    provider.setDataLicenseType(dataProvider.getDataLicenseType());
                                    provider.setDiscussionAnalytics(dataProvider.getDiscussionAnalytics());
                                    provider.setDiscussionCleanliness(dataProvider.getDiscussionCleanliness());
                                    provider.setSampleOrPanelBiases(dataProvider.getSampleOrPanelBiases());
                                    provider.setDatasetSize(dataProvider.getDatasetSize());
                                    provider.setDatasetNumberOfRows(dataProvider.getDatasetNumberOfRows());
                                    provider.setNumberOfAnalystEmployees(dataProvider.getNumberOfAnalystEmployees());
                                    provider.setInvestorClientsCount(dataProvider.getInvestorClientsCount());
                                    provider.setPointInTimeAccuracy(dataProvider.getPointInTimeAccuracy());
                                    provider.setDataGaps(dataProvider.getDataGaps());
                                    provider.setIncludesOutliers(dataProvider.getIncludesOutliers());
                                    provider.setNormalized(dataProvider.getNormalized());
                                    provider.setDuplicatesCleaned(dataProvider.getDuplicatesCleaned());
                                    provider.setInvestorClientBucket(dataProvider.getInvestorClientBucket());
                                    provider.setSubscriptionTimePeriod(dataProvider.getSubscriptionTimePeriod());
                                /*provider.setComplianceScorePersonal(dataProvider.getComplianceScorePersonal());
                                provider.setComplianceScorePublic(dataProvider.getComplianceScorePublic());*/
                                    provider.setMonthlyPriceRange(dataProvider.getMonthlyPriceRange());
                                    provider.setSubscriptionModel(dataProvider.getSubscriptionModel());
                                    provider.setNumberOfDataSources(dataProvider.getNumberOfDataSources());
                                    provider.setPrivacy(dataProvider.getPrivacy());
                                    provider.setIdentifiersAvailable(dataProvider.getidentifiersAvailable());
                                    provider.setEquitiesStyle(dataProvider.getEquitiesStyle());
                                    provider.setDataTypes(dataProvider.getDataTypes());
                                    provider.setDataProductTypes(dataProvider.getDataProductTypes());
                                    provider.setAccessOffered(dataProvider.getAccessOffered());
                                    provider.setInvestingTimeFrame(dataProvider.getInvestingTimeFrame());
                                    provider.setEquitiesMarketCap(dataProvider.getEquitiesMarketCap());
                                    provider.setStrategies(dataProvider.getStrategies());
                                    provider.setResearchStyles(dataProvider.getResearchStyles());
                                    provider.setOrganizationType(dataProvider.getOrganizationType());
                                    provider.setLanguagesAvailable(dataProvider.getLanguagesAvailable());
                                    provider.setPaymentMethodsOffered(dataProvider.getPaymentMethodsOffered());
                                    provider.setDataGapsReasons(dataProvider.getDataGapsReasons());
                                    provider.setOutlierReasons(dataProvider.getOutlierReasons());
                                    provider.setBiases(dataProvider.getBiases());
                                    provider.setSecRegulated(dataProvider.getSecRegulated());
                                    provider.setManagerType(dataProvider.getManagerType());
                                    provider.setResearchMethods(dataProvider.getResearchMethods());
                                    /*provider.setPlannedResearchDate(dataProvider.getPlannedResearchDate());*/
                                    provider.setIeiStatus(dataProvider.getIeiStatus());
                                    provider.setPriority(dataProvider.getPriority());


                                    provider.setOwnerOrganization(dataOrganization);
                                    provider.setMainAssetClass(dataProvider.getMainAssetClass());
                                    provider.setSecurityTypes(dataProvider.getSecurityTypes());
                                    provider.setInvestorTypes(dataProvider.getInvestorTypes());
                                    provider.setProviderType(dataProvider.getProviderType());
                                    provider.setProviderTags(dataProvider.getProviderTags());
                                    provider.setAssetClasses(dataProvider.getAssetClasses());
                                    provider.setIndustries(dataProvider.getIndustries());
                                    provider.setMarketplaceStatus(dataProvider.getMarketplaceStatus());
                                    provider.setRecordID(dataProvider.getRecordID());
                                    provider.setFeatures(dataProvider.getFeatures());
                                    provider.setSources(dataProvider.getSources());
                                    provider.setId(dataProvider.getId());
                                    dataProvider = provider;
                                } else {
                                    dataProvider.setOwnerOrganization(null);
                                }
                            }

                            if (detail) {
                                //Set provider permissions
                            	//System.out.println("permission called");
                                dataProvider.setConsumerPartners(setPartnerPermissions(dataProvider.getConsumerPartners(), userRoles, unlockedProviderIDs));
                                if (dataProvider.getDistributionPartners() != null) {
                                    dataProvider.setDistributionPartners(setPartnerPermissions(dataProvider.getDistributionPartners(), userRoles, unlockedProviderIDs));
                                }
                                dataProvider.setProviderPartners(setPartnerPermissions(dataProvider.getProviderPartners(), userRoles, unlockedProviderIDs));

                                //Set Other categories
                                if (dataProvider.getCategories() != null) {
                                    dataProvider.setCategories(getCategories(categoryIDs, dataProvider));
                                }

                            }


                            if (!categoryIDs.contains(categoryID) && dataProvider.isLocked()) {
                                mainDataCategory.setDatacategory(Utils.randomString(5, 15));
                                mainDataCategory.setLocked(true);
                            }
                        }
                    }
                    if (detail) {
                        //Set provider permissions
                    	//System.out.println("permission called");
                        if (dataProvider.getConsumerPartners() != null) {
                        dataProvider.setConsumerPartners(setPartnerPermissions(dataProvider.getConsumerPartners(), userRoles, unlockedProviderIDs));
                        }
                        if (dataProvider.getDistributionPartners() != null) {
                            dataProvider.setDistributionPartners(setPartnerPermissions(dataProvider.getDistributionPartners(), userRoles, unlockedProviderIDs));
                        }
                        if (dataProvider.getProviderPartners() != null) {
                        dataProvider.setProviderPartners(setPartnerPermissions(dataProvider.getProviderPartners(), userRoles, unlockedProviderIDs));
                        }
                    }
                    if (detail) {
                        if (dataProvider.getProviderTags() != null) {
                            List<BasicBean> providersTags = dataProvider.getProviderTags();
                            Iterator<BasicBean> itr = providersTags.iterator();
                            while (itr.hasNext()) {
                                BasicBean tag = itr.next();
                                long cid = Long.parseLong(tag.getId());
                                //  System.out.println("tag id" + tag.get);

                                /*List<DataProviderTag> Tags = new ArrayList<>();
                                long tagCheck = 0;
                                DataProviderTag tagList = new DataProviderTag();*/
                                Query q = entityManager.createNativeQuery("SELECT dt.id,dt.provider_tag,dt.privacy,dt.record_id FROM datamarketplace.data_provider_tag dt WHERE ((dt.by_user_id=(" + user.getId() + ") AND dt.privacy='private') OR (dt.privacy='Public')) and dt.id in(" + cid + ")");
                                List<?> authors = q.getResultList();
                                if (authors.size() == 0) {
                                    itr.remove();
                                    continue;
                                }


                            }
                            //System.out.println("testing 2 " + dataProvider.getProviderTags());
                        }
                    }
                  /*  if (detail) {
                        //Set provider permissions
                    	System.out.println("permission called");
                        if (dataProvider.getConsumerPartners() != null) {
                        dataProvider.setConsumerPartners(setPartnerPermissions(dataProvider.getConsumerPartners(), userRoles, unlockedProviderIDs));
                        }
                        if (dataProvider.getDistributionPartners() != null) {
                            dataProvider.setDistributionPartners(setPartnerPermissions(dataProvider.getDistributionPartners(), userRoles, unlockedProviderIDs));
                        }
                        if (dataProvider.getProviderPartners() != null) {
                        dataProvider.setProviderPartners(setPartnerPermissions(dataProvider.getProviderPartners(), userRoles, unlockedProviderIDs));
                        }

                    }*/
                    List<Integer> interestType = userProviderInterestMap.get(providerID);
                    if (interestType != null) {
                        dataProvider.setUserProviderStatus(interestType);
                    }
                    obj.add("_source", gson.toJsonTree(dataProvider));
                }
            }
        }
    
        
       
        
        return result;
    }


    private void myMatchesAdd(boolean myMatch, User user) {
		// TODO Auto-generated method stub
		if(myMatch)
		{
			DataProviderTag dataProviderTag=dataProviderTagRepository.findUserValue(user.getId());
			//System.out.println(dataProviderTag.getId());
			//List<Long> dataTag=dataProviderTagRepository.myMatchTag(dataProviderTag.getId());
	    	Query q = entityManager.createNativeQuery("SELECT * FROM  datamarketplace.data_provider_provider_tag where provider_tags_id="+ dataProviderTag.getId());
	    	List<?> authors = q.setMaxResults(20).getResultList();
			//System.out.println(authors.size());
			for(int i=0; i<authors.size();i++)
			{
				Object[] row = (Object[]) authors.get(i);
				//System.out.println(Long.parseLong(row[1].toString()));
				UserProvider userProviderValue=userProviderService.getMyMatchesValue(user.getId(),Long.parseLong(row[1].toString()),INTEREST_TYPE_UNLOCK);
				if(null == userProviderValue)
				{
				UserProvider userProvider = new UserProvider();
				// userProvider.setProviderRecordID(provider.getRecordID());
			        userProvider.setProviderID(Long.parseLong(row[1].toString()));
			        userProvider.setUserID(user.getId());
			        userProvider.setInterestType(INTEREST_TYPE_UNLOCK);
			        userProvider.setCreatedDate(LocalDateTime.now());
			        userProvider.setInactive(false);
			        userProviderService.myMatchesValue(userProvider);
				}
					//userProviderRepository.save(userProvider);
			}
		}
	}

	@Transactional(noRollbackFor = Exception.class)
    @SuppressWarnings("unchecked")
    @Timed
    public JsonObject organizationsearch(String query,String type) {
        Gson gson = new Gson();
        JsonObject result = searchIndex(query, type);
        if (result == null) {
            log.error("Failed to retrieve elasticsearch result for query: {}", query);
            return new JsonObject();
        }
        JsonArray hitsArray = result.getAsJsonObject("hits").getAsJsonArray("hits");
        if (!RESTRICTED_INDEXES.contains(type)) {
            return result;
        }
        for (JsonElement e : hitsArray) {
            JsonObject obj = e.getAsJsonObject();
            JsonObject sourceObject = obj.getAsJsonObject("_source");
            long providerOrganizerID;
            DataOrganization dataOrganization = gson.fromJson(sourceObject, DataOrganization.class);
            DataOrganization organization=new DataOrganization();
            organization.setId(organization.getId());
            organization.setName(dataOrganization.getName());
            organization.setRecordID(dataOrganization.getRecordID());
            dataOrganization=organization;
            obj.add("_source", gson.toJsonTree(dataOrganization));
        }
        return result;
    }

    @Transactional(noRollbackFor = Exception.class)
    @SuppressWarnings("unchecked")
    @Timed
    public JsonObject eventSearch(String query,String type) {
        Gson gson = new Gson();
       // boolean unlockedProviders=true;
        List<Long> unlockedProviderIDs = new ArrayList<>();
        //List<Long> overrideCategoryIDs = userService.getOverrideCategoryIDsForCurrentUser();
        unlockedProviderIDs = userProviderService.getProvidersByInterestTypeForCurrentUser(INTEREST_TYPE_UNLOCK);
        JsonObject result = searchIndex(query, type);
        if (result == null) {
            log.error("Failed to retrieve elasticsearch result for query: {}", query);
            return new JsonObject();
        }
        JsonArray hitsArray = result.getAsJsonObject("hits").getAsJsonArray("hits");
        if (!RESTRICTED_INDEXES.contains(type)) {
            return result;
        }

        // List<Long> providerOrganizerIDs=userService.getAllProviderOrganizerIDsForCurrentUser();

        List<String> userRoles = userService.getLoginUserEffectiveRoles();
        for (JsonElement e : hitsArray) {
            JsonObject obj = e.getAsJsonObject();
            JsonObject sourceObject = obj.getAsJsonObject("_source");
           // long providerOrganizerID;
            DataEvent dataEvent = gson.fromJson(sourceObject, DataEvent.class);



           // String eventPermission = dataEvent.getUserPermission();

            DataEvent event=new DataEvent();

            event.setCity(dataEvent.getCity());
            event.setCreatedTime(dataEvent.getCreatedTime());
            event.setAddress(dataEvent.getAddress());
            event.setAgenda(dataEvent.getAgenda());
            event.setAuthoredArticles(dataEvent.getAuthoredArticles());
            event.setCategories(dataEvent.getCategories());
            event.setCompleteness(dataEvent.getCompleteness());
            event.setCountry(dataEvent.getCountry());
            event.setDescription(dataEvent.getDescription());
            event.setDetails(dataEvent.getDetails());
            event.setEndTime(dataEvent.getEndTime());
            event.setEventEditionNames(dataEvent.getEventEditionNames());
            event.setEventTypes(dataEvent.getEventTypes());
            event.setId(dataEvent.getId());
            event.setIndustries(dataEvent.getIndustries());
            event.setLastUpdated(dataEvent.getLastUpdated());
            event.setLenthInDays(dataEvent.getLenthInDays());
            event.setLink(dataEvent.getLink());
            event.setPrice(dataEvent.getPrice());
            event.setRecordID(dataEvent.getRecordID());
            event.setReleventArticles(dataEvent.getReleventArticles());
            event.setStartTime(dataEvent.getStartTime());
            event.setState(dataEvent.getState());
            event.setTopics(dataEvent.getTopics());
            event.setVenue(dataEvent.getVenue());
            event.setZipCode(dataEvent.getZipCode());
            Set<DataProvider> sponsorProviders = new HashSet<>();
            for (DataProvider h : dataEvent.getSponsorProvider()) {
                String providerPermission = h.getUserPermission();
                if (!userRoles.contains(providerPermission) && !unlockedProviderIDs.contains(h.getId())) {
                    h.setProviderName(Utils.randomString(5, 15));
                    h.setLocked(true);
                    h.setLogo(null);
                    sponsorProviders.add(h);
                }
                sponsorProviders.add(h);
            }
            Set<DataProvider> providerOrganizer = new HashSet<>();
            for (DataProvider h : dataEvent.getProviderOrganizer()) {
                String providerPermission = h.getUserPermission();
                if (!userRoles.contains(providerPermission) && !unlockedProviderIDs.contains(h.getId())) {
                    h.setProviderName(Utils.randomString(5, 15));
                    h.setLocked(true);
                    h.setLogo(null);
                    providerOrganizer.add(h);
                }
                providerOrganizer.add(h);
            }
            event.setSponsorProvider(sponsorProviders);
            event.setOrganizationOrganizer(dataEvent.getOrganizationOrganizer());
            event.setSponsorOrganization(dataEvent.getSponsorOrganization());
            event.setProviderOrganizer(dataEvent.getProviderOrganizer());
            event.setSponsorProvider(dataEvent.getSponsorProvider());
            dataEvent=event;
            obj.add("_source", gson.toJsonTree(dataEvent));
        }
        return result;
    }

    @Transactional(noRollbackFor = Exception.class)
    @SuppressWarnings("unchecked")
    @Timed
    public JsonObject getDataProviderExt(String type, String recordID) {
        User user = userService.getUser();
        ExternalClient externalClient = externalClientRepository.findById(user.getId());
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date cdate = new Date();
        Date eDate = externalClient.getExpiryDate();
        Date newDate = addDays(eDate, 1);
        if (newDate.compareTo(cdate) > 0) {
            if (externalClient != null && (externalClient.getApiLimit() > 0)) {
                Integer apilimits = externalClient.getApiLimit();
                Integer hit = apilimits - 1;
                externalClient.setApiLimit(hit);
                //  externalClients.getUser().setId(user.getId());
                //externalClients.setId(userOptional.getId());
                // externalClients.getUser().setId(user.getId());
                try {
                    ExternalClient resultw = externalClientRepository.saveAndFlush(externalClient);
                } catch (Exception e) {
                    System.out.println("failed saving ext client" + e.getMessage());
                }

                String query = "{\n" +
                    "  \"_source\": {\n" +
                    "    \"include\": [\n" +
                    "      \"createdDate\",\n" +
                    "      \"completeness\",\n" +
                    "      \"features.desc\",\n" +
                    "      \"categories.desc\",\n" +
                    "      \"industries.desc\",\n" +
                    "       \"providerName\",\n" +
                    "\"pricingModels.desc\",\n" +
                    "\"assetClasses.desc\",\n" +
                    "\"investorTypes.description\",\n" +
                    "\"securityTypes.desc\",\n" +
                    "\"relevantSectors.desc\",\n" +
                    "      \"mainDataCategory.datacategory\",\n" +
                    "      \"mainDataIndustry.dataIndustry\",\n" +
                    "      \"shortDescription\",\n" +
                    "\"longDescription\",\n" +
                    "      \"updatedDate\",\n" +
                    "      \"ownerOrganization\",\n" +
                    "      \"scoreReadiness.description\",\n" +
                    "      \"scoreInvestorsWillingness.description\",\n" +
                    "      \"scoreUniqueness.description\",\n" +
                    "      \"scoreValue.description\",\n" +
                    "      \"scoreOverall.description\",\n" +
                    "      \"deliveryMethods.desc\",\n" +
                    "      \"deliveryFormats.desc\",\n" +
                    "\"dataUpdateFrequency\",\n" +
                    "\"geographicalFoci.description\",\n" +
                    "\"providerType.providerType\",\n" +
                    "            \"freeTrialAvailable\",\n" +
                    "      \"createdDate\",\n" +
                    "\"distributionPartners.recordID\",\n" +
                    "\"distributionPartners.providerName\",\n" +
                    "\"deliveryFrequencyNotes\",\n" +
                    "\"providerTags.desc\",\n" +
                    "\"sources.desc\",\n" +
                    "\"mainAssetClass.description\",\n" +
                    "\"notes\",\n" +
                    "\"dataUpdateFrequencyNotes\",\n" +
                    "\"dateCollectionBegan\",\n" +
                    "\"dateCollectionRangeExplanation\",\n" +
                    "\"collectionMethodsExplanation\",\n" +
                    "\"potentialDrawbacks\",\n" +
                    "\"uniqueValueProps\",\n" +
                    "\"historicalDateRange\",\n" +
                    "\"keyDataFields\",\n" +
                    "\"summary\",\n" +
                    "\"investor_clients\",\n" +
                    "\"historicalDateRangeEstimate\",\n" +
                    "\"publicCompaniesCovered\",\n" +
                    "\"productDetails\",\n" +
                    "\"sampleOrPanelSize\",\n" +
                    "\"useCasesOrQuestionsAddressed\",\n" +
                    "\"dataLicenseType\",\n" +
                    "\"dataLicenseTypeDetails\",\n" +
                    "\"dataLanguagesAvailable\",\n" +
                    "\"legalAndComplianceIssues\",\n" +
                    "\"dataRoadmap\",\n" +
                    "\"providerPartners.recordID\",\n" +
                    "\"providerPartners.providerName\",\n" +
                    "\"consumerPartners.recordID\",\n" +
                    "\"consumerPartners.providerName\",\n" +
                    "\"recordID\",\n" +
                    "\"deliveryFrequencies.description\"\n" +
                    "    ],\n" +
                    "\n" +
                    "\"exclude\": [\n" +
                    "\"logo\",\n" +
                    "      \"id\",\n" +
                    "\"ownerOrganization.id\",\n" +
                    "\"ownerOrganization.recordID\",\n" +
                    "\"locked\",\n" +
                    "\"userPermission\",\n" +
                    "\"live\",\n" +
                    "\"sortorder\",\n" +
                    "\"userProviderStatus\",\n" +
                    "\"followed\",\n" +
                    "\"orgRelationshipStatus\",\n" +
                    "\"providerPermissionOverride\",\n" +
                    "\"providerPermissionOverride\",\n" +
                    "\"marketplaceStatus\",\n" +
                    "\"documentsMetadata\",\n" +
                    "\"followingUsers\",\n" +
                    "\"marketplaceStatus\",\n" +
                    "\"pricingModels.id\",\n" +
                    "\"ownerOrganization.live\",\n" +
                    "\"ownerOrganization.parentOrganization.live\",\n" +
                    "\"researchMethodsCompleted\"\n" +
                    "]\n" +
                    "  },\n" +
                    "\"filter\": {\n" +
                    "   \"query\": {\n " +
                    "      \"match\": {\n " +
                    "      \"recordID\": \"" + recordID + "\"\n" +
                    "    }\n" +
                    "    }\n" +
                    "    }\n" +
                    "}";
                //System.out.println(query);
                JsonObject result = searchIndex(query, type);


                return result;
            }else {
                JsonObject apierror = new JsonObject();
                apierror = new JsonObject();
                apierror.addProperty("error", "403");
                apierror.addProperty("message", "You have exceeded your limit of API calls for the month. Please contact Amass Insights for more information!");
                return apierror;
            }
        }else
        {
            JsonObject apierror = new JsonObject();
            apierror = new JsonObject();
            apierror.addProperty("error", "403");
            apierror.addProperty("message", "Your API subscription has expired. Please contact Amass Insights for more information!");
            return apierror;
        }

    }


    @Transactional(noRollbackFor = Exception.class)
    @SuppressWarnings("unchecked")
    @Timed
    public JsonObject getDataProvidersExt(String type, Integer size, Integer page,String sort) throws Exception {
        User user = userService.getUser();
        ExternalClient externalClient=externalClientRepository.findById(user.getId());
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date cdate = new Date();
        Date eDate=externalClient.getExpiryDate();
        Date newDate = addDays(eDate, 1);
        if (newDate.compareTo(cdate)>0) {
            if (externalClient != null && (externalClient.getApiLimit() > 0)) {
                Integer apilimits = externalClient.getApiLimit();
                Integer hit = apilimits - 1;
                externalClient.setApiLimit(hit);
                //  externalClients.getUser().setId(user.getId());
                //externalClients.setId(userOptional.getId());
                // externalClients.getUser().setId(user.getId());
                try {
                    ExternalClient resultw = externalClientRepository.saveAndFlush(externalClient);
                } catch (Exception e) {
                    System.out.println("failed saving ext client" + e.getMessage());
                }
                int sBreak = 0;
                JsonObject json = new JsonObject();
                Integer pagesize = page;
                //  System.out.println("size " + size + " page " + page);
                JsonObject maxpage = new JsonObject();
                try {
                    if (page == null) {
                        pagesize = 0;
                        page = 1;
                    }
                } catch (NullPointerException b) {
                    page = 1;
                    pagesize = 0;
                }

                String filter = null;
                if (filter == null) {
                    filter = "";
                }
                if (sort.equals("{}")) {
                    sort = "{\n" +
                        "\"id\": \"asc\"\n" +
                        "}";
                }

                try {
                    if (size > 20) {

                        json = new JsonObject();
                        json.addProperty("error", "maximum size is 20");


                        //System.out.println("ErrorMessage : " + Error);
                        sBreak = 1;
                    }


                } catch (Exception e) {

                }
                if (sBreak == 0) {

                    try {
                        if (size <= 0 || size == null) {

                            size = 20;
                        }

                    } catch (NullPointerException e) {
                        size = 20;
                    }

                    int page1 = (page - 1) * size;
                    //size=(page*size);
                    page = page1 + 1;

                    String query = "{\n" +
                        "  \"_source\": {\n" +
                        "    \"include\": [\n" +
                        "      \"createdDate\",\n" +
                        "      \"completeness\",\n" +
                        "      \"features.desc\",\n" +
                        "      \"categories.desc\",\n" +
                        "      \"industries.desc\",\n" +
                        "       \"providerName\",\n" +
                        "\"pricingModels.desc\",\n" +
                        "\"assetClasses.desc\",\n" +
                        "\"investorTypes.description\",\n" +
                        "\"securityTypes.desc\",\n" +
                        "\"relevantSectors.desc\",\n" +
                        "      \"mainDataCategory.datacategory\",\n" +
                        "      \"mainDataIndustry.dataIndustry\",\n" +
                        "      \"shortDescription\",\n" +
                        "\"longDescription\",\n" +
                        "      \"updatedDate\",\n" +
                        "      \"ownerOrganization\",\n" +
                        "      \"scoreReadiness.description\",\n" +
                        "      \"scoreInvestorsWillingness.description\",\n" +
                        "      \"scoreUniqueness.description\",\n" +
                        "      \"scoreValue.description\",\n" +
                        "      \"scoreOverall.description\",\n" +
                        "      \"deliveryMethods.desc\",\n" +
                        "      \"deliveryFormats.desc\",\n" +
                        "\"dataUpdateFrequency\",\n" +
                        "\"geographicalFoci.description\",\n" +
                        "\"providerType.providerType\",\n" +
                        "            \"freeTrialAvailable\",\n" +
                        "      \"createdDate\",\n" +
                        "\"distributionPartners.recordID\",\n" +
                        "\"distributionPartners.providerName\",\n" +
                        "\"deliveryFrequencyNotes\",\n" +
                        "\"providerTags.desc\",\n" +
                        "\"sources.desc\",\n" +
                        "\"mainAssetClass.description\",\n" +
                        "\"notes\",\n" +
                        "\"dataUpdateFrequencyNotes\",\n" +
                        "\"dateCollectionBegan\",\n" +
                        "\"dateCollectionRangeExplanation\",\n" +
                        "\"collectionMethodsExplanation\",\n" +
                        "\"potentialDrawbacks\",\n" +
                        "\"uniqueValueProps\",\n" +
                        "\"historicalDateRange\",\n" +
                        "\"keyDataFields\",\n" +
                        "\"summary\",\n" +
                        "\"investor_clients\",\n" +
                        "\"historicalDateRangeEstimate\",\n" +
                        "\"publicCompaniesCovered\",\n" +
                        "\"productDetails\",\n" +
                        "\"sampleOrPanelSize\",\n" +
                        "\"useCasesOrQuestionsAddressed\",\n" +
                        "\"dataLicenseType\",\n" +
                        "\"dataLicenseTypeDetails\",\n" +
                        "\"dataLanguagesAvailable\",\n" +
                        "\"legalAndComplianceIssues\",\n" +
                        "\"dataRoadmap\",\n" +
                        "\"providerPartners.recordID\",\n" +
                        "\"providerPartners.providerName\",\n" +
                        "\"consumerPartners.recordID\",\n" +
                        "\"consumerPartners.providerName\",\n" +
                        "\"recordID\",\n" +
                        "\"deliveryFrequencies.description\"\n" +
                        "    ],\n" +
                        "\n" +
                        "\"exclude\": [\n" +
                        "\"logo\",\n" +
                        "\"ownerOrganization.id\",\n" +
                        "\"ownerOrganization.recordID\",\n" +
                        "\"locked\",\n" +
                        " \"id\",\n" +
                        "\"userPermission\",\n" +
                        "\"live\",\n" +
                        "\"sortorder\",\n" +
                        "\"userProviderStatus\",\n" +
                        "\"followed\",\n" +
                        "\"orgRelationshipStatus\",\n" +
                        "\"providerPermissionOverride\",\n" +
                        "\"providerPermissionOverride\",\n" +
                        "\"marketplaceStatus\",\n" +
                        "\"documentsMetadata\",\n" +
                        "\"followingUsers\",\n" +
                        "\"marketplaceStatus\",\n" +
                        "\"pricingModels.id\",\n" +
                        "\"ownerOrganization.live\",\n" +
                        "\"ownerOrganization.parentOrganization.live\",\n" +
                        "\"researchMethodsCompleted\"\n" +
                        "\n" +
                        "\n" +
                        "]\n" +
                        "  },\n" +
                        "  \"sort\": [" + sort +
                        "  ],\n" +
                        "\"from\":" + page +
                        " , \"size\":" + size +
                        " , \"query\": {\n" +
                        "    \"query_string\": {\n" +
                        "      \"query\": \"" + filter + "*\",\n" +
                        "      \"fields\": [\n" +
                        "        \"_all\",\n" +
                        "        \"providerName^2\"\n" +
                        "      ]\n" +
                        "    }\n" +
                        "  }\n" +
                        "}";
                    JsonObject result = searchIndex(query, type);
                    //    System.out.println(result.get("hits"));
                    JsonElement total = result.get("hits").getAsJsonObject().get("total");
                    Double actualsize = total.getAsDouble() / size;
                    actualsize = Math.ceil(actualsize);
                    if (actualsize < pagesize) {
                        maxpage = new JsonObject();
                        maxpage.addProperty("error", "403");
                        maxpage.addProperty("message", "The page number of " + Math.round(actualsize) + " with size " + size + " exceeds the total number of matching providers for this query");
                        return maxpage;
                    } else {
                        return result;
                    }
                } else {
                    return json;
                }
            } else {
                JsonObject apierror = new JsonObject();
                apierror = new JsonObject();
                apierror.addProperty("error", "403");
                apierror.addProperty("message", "You have exceeded your limit of API calls for the month. Please contact Amass Insights for more information!");
                return apierror;
            }
        }else {
            JsonObject apierror = new JsonObject();
            apierror = new JsonObject();
            apierror.addProperty("error", "403");
            apierror.addProperty("message", "Your API subscription has expired. Please contact Amass Insights for more information!");
            return apierror;
        }

    }
    @Transactional(readOnly = true)
    /*"unchecked"*/
    @Timed
    public JsonObject searchcategory(String query,boolean followedFilter, boolean unlockedDatacategory) {
        Gson gson = new Gson();

        List<UserDataCategory> userDataCategoryIntrests = userDataCategoryService.findAllDatacategoryInterestsByUserID();
        Map<Long, List<Integer>> userDatacatoryInterestMap = new HashMap<>();
        for(UserDataCategory udc : userDataCategoryIntrests)
        {
            List<Integer> it = userDatacatoryInterestMap.get(udc.getDataCategoryID());
            if(it != null)
            {
                it.add(udc.getInterestType());
            }
            else
            {
                it = new ArrayList<>();
                it.add(udc.getInterestType());
            }
            userDatacatoryInterestMap.put(udc.getDataCategoryID(), it);
        }
        List<Long>  followedDataCategoryIDs = new ArrayList<>(),
            unlockedDataCategoryIDs = new ArrayList<>();
        List<Long> overrideCategoryIDs = userService.getOverrideCategoryIDsForCurrentUser();

        followedDataCategoryIDs= userDataCategoryService.getDataCategoryByInterestTypeForCurrentUser(INTEREST_TYPE_FOLLOW);

        unlockedDataCategoryIDs=userDataCategoryService.getDataCategoryByInterestTypeForCurrentUser(INTEREST_TYPE_UNLOCK);


        if(followedFilter)
        {
            query = generateFollowedCategoryFilter(query, followedDataCategoryIDs);
        }
        else if(unlockedDatacategory)
        {
            query = generateUnlockedCategoryFilter(query, unlockedDataCategoryIDs, overrideCategoryIDs);
        }



        JsonObject result = searchIndex(query, INDEX_DATA_CATEGORY);
        if(result == null)
        {
            log.error("Failed to retrieve elasticsearch result for query: {}", query);
            return new JsonObject();
        }
        JsonArray hitsArray = result.getAsJsonObject("hits").getAsJsonArray("hits");



        List<Long> categoryIDs = userService.getAllCategoryIDsForCurrentUser();
        List<DataCategory> categories=new ArrayList<DataCategory>();
        List<String> userRoles = userService.getLoginUserEffectiveRoles();
        for(JsonElement e : hitsArray)
        {
            JsonObject obj = e.getAsJsonObject();
            JsonObject sourceObject = obj.getAsJsonObject("_source");
            DataCategory category = gson.fromJson(sourceObject, DataCategory.class);
            long datacategoryID = category.getId();
            String datacategory = category.getDatacategory();
            boolean followed = followedDataCategoryIDs.contains(datacategoryID);
            category.setFollowed(followed);
           /* System.out.println(userDataCategoryIntrests+"1 is there");
            //for(int i=0;i<categories.size();i++) {
            //if (UserService.isUser() && categories.get(i).getDatacategory().contains("DATA_CATEGORY_NULL")) {
            if(UserService.isUser() && datacategory.contains("DATA_CATEGORY_NULL")){
                categories.remove(datacategory);
                // categoryIDs.remove(datacategoryID);
                //userDataCategoryIntrests.remove(datacategoryID);
                System.out.println(userDataCategoryIntrests);
                break;

                //  }


            }
*/

            /*DataProvider dataProvider=new DataProvider();
            DataCategory mainDataCategory = dataProvider.getMainDataCategory();*/
            //List<Integer> categories = category.getC.stream().map(BasicBean::getId).map(Integer::parseInt).collect(Collectors.toList())
            if(UserService.isAdminUser()) {
                obj.add("_source", gson.toJsonTree(category));
                continue;
            }




            String categoryPermission = category.getUserPermission();
            if (!userRoles.contains(categoryPermission) && !unlockedDataCategoryIDs.contains(datacategoryID)) {
                category.setDatacategory(Utils.randomString(5,15));
                category.setLocked(true);
                category.setLogo(null);

            }





           /* if(!categoryIDs.contains(datacategoryID)) {
                category.setDatacategory(Utils.randomString(5, 15));
                category.setExplanation(Utils.randomString(5, 20));
                category.setLocked(true);
                //Overwrite the current _source element

            }*/
            obj.add("_source", gson.toJsonTree(category));




        }


        return result;
    }






    private Set<DataProvider> setPartnerPermissions(Set<DataProvider> partners, List<String> userRoles, List<Long> unlockedProviderIDs) {
        User user = userService.getUser();
        UserProvider userProvider = null;
    	if (partners != null) {
            Set p = new HashSet<DataProvider>();
            Iterator<DataProvider> dataProviderIterator = partners.iterator();
            while (dataProviderIterator.hasNext()) {
                DataProvider dataProvider = dataProviderIterator.next();
                long providerID = dataProvider.getId();
                dataProvider = dataProviderSearchRepository.findDataProviderByidEquals(providerID);
//System.out.println("dataProvider"+dataProvider);
                String providerPermission = dataProvider.getUserPermission();
                if(null!=dataProviderIdMatch)
                {
                    userProvider= userProviderService.getMyMatchesValue(user.getId(),dataProviderIdMatch, INTEREST_TYPE_UNLOCK);

                }
              /*  System.out.println(providerPermission);
                System.out.println(userRoles);*/
              /* System.out.println("userProvider"+userProvider);
               System.out.println("providerID"+dataProviderIdMatch);*/
                if (providerPermission != null) {
                	if(null !=userProvider)
                	{
                		if(providerPermission.equalsIgnoreCase("ROLE_ADMIN"))
                    	{
                			  dataProvider.setProviderName(Utils.randomString(5, 15));
                              dataProvider.setLocked(true);
                              dataProvider.setOwnerOrganization(null);
                              dataProvider.setLogo(null);
                    	}
                	}
                	else if (!userRoles.contains(providerPermission) && !unlockedProviderIDs.contains(providerID)) {
                    	/*if(providerPermission.equalsIgnoreCase("ROLE_ADMIN"))
                    	{*/
                        dataProvider.setProviderName(Utils.randomString(5, 15));
                        dataProvider.setLocked(true);
                        dataProvider.setOwnerOrganization(null);
                        dataProvider.setLogo(null);
                    }
                    p.add(dataProvider);
                }
            }
            return p;
        }
        return partners;
    }

    private List getCategories(List<Long> categoryIDs, DataProvider provider)
    {
        List<BasicBean> categories = provider.getCategories();
        Iterator<BasicBean> itr = categories.iterator();
        while (itr.hasNext()) {
            BasicBean category = itr.next();
            long cid = Long.parseLong(category.getId());
            if(cid == provider.getMainDataCategory().getId())
            {
                itr.remove();
                continue;
            }
            if (!categoryIDs.contains(cid) && provider.isLocked()) {
                category.setDesc(Utils.randomString(5, 15));
                category.setLocked(true);
            }
        }
        return categories;
    }
    @Timed
    protected  JsonObject searchIndex(String query, String type)
    {
        Search search = new Search.Builder(query)
            .addIndex(type)
            .addType(type)
            .build();
        try {
            SearchResult result = client.execute(search);
            return result.getJsonObject();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    private void addCategoriesFilter(List<Long> overrideCategoryIDs, JsonArray shouldElement, Gson gson)
    {
        for (long categoryID : overrideCategoryIDs)
        {
            JsonElement mainCategoryPermissionElement = gson.fromJson("{\"match\": {\"mainDataCategory.id\":" + categoryID + "}}", JsonElement.class);
            shouldElement.add(mainCategoryPermissionElement);
            JsonElement categoryPermissionElement = gson.fromJson("{\"match\": {\"categories.id\":" + categoryID + "}}", JsonElement.class);
            shouldElement.add(categoryPermissionElement);
        }
    }

    @Timed
    @Transactional(readOnly = true)
    public long getCategoryCount() {
        return categorySearchRepository.count();
    }

    @Timed
    @Transactional(readOnly = true)
    public long getArticleCount() {
        return dataArticleSearchRepository.count();
    }

    @Timed
    @Transactional(readOnly = true)
    public long getFeatureCount() {
        return dataFeatureSearchRepository.count();
    }

    @Timed
    @Transactional(readOnly = true)
    public long getProviderCount() {
        return dataProviderSearchRepository.count();
    }

    @Timed
    @Transactional(readOnly = true)
    public long getEventCount() {
        return dataEventSearchRepository.count();
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Timed
    public JsonObject newsSearch(String queryBody, String type,String lookupmodule, String sort) {
        Integer size = 20;
        String query = "{}";
        if (queryBody != null) {
            if (type.equalsIgnoreCase("dataarticle")) {
                query="{   \n" +
                    "\"_source\": {\n" +
                    "    \n" +
                    "\"exclude\": [      \n" +
                    "      \"includeInNewsletter\",\n" +
                    "  \"articleuniquekey\",\n" +
                    "  \"relevancy\",\n" +
                    "  \"authorOrganization\",\n" +
                    "  \"authorDataProvider\",\n" +
                    "  \"relevantProviders\"\n" +
                    "      ]\n" +
                    "},\n" +
                    " \"sort\": [     \n" +
                    "      {\n" +
                    " \"publishedDate\": \"desc\"\n" +
                    "  }   \n" +
                    "  ],   \n" +
                    "\"size\": 20,\n" +
                    "\"filter\": {\n" +
                    "\"query\": {\n" +
                    " \"bool\": { \n" +
                    " \"must\": { \n" +
                    "  \"terms\": { \n" +
                    " \"articleTopics.id\": [2]\n" +
                    "       }         \n" +
                    "  }       \n" +
                    "}     \n" +
                    "        }\n" +
                    "} \n" +
                    "}";
            } else if (type.equalsIgnoreCase("dataevent")) {
                query="{\n" +
                    "\"sort\": [\n" +
                    "{\n" +
                    "      \"publishedDate\": \"desc\"" +
                    "    }\n" +
                    "  ],\n" +
                    " \"size\":" + size +
                    "}";
            } else if (type.equalsIgnoreCase("lookupcode")) {
                query="{\n" +
                    "  \"_source\": {\n" +
                    "    \"include\": [\n" +
                    "      \"id\",\n" +
                    "      \"lookupcode\",\n" +
                    "      \"description\",\n" +
                    "      \"recordCount\"\n" +
                    "    ]\n" +
                    "  },\n" +
                    "  \"sort\": [\n" +
                    "    {\n" +
                    "      \"recordCount\": \"desc\"\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"size\": 200,\n" +
                    "  \"filter\": {\n" +
                    "    \"query\": {\n" +
                    "      \"match\": {\n" +
                    "        \"lookupmodule\": \""+lookupmodule+"\"\n" +
                    "      }\n" +
                    "    }\n" +
                    "  }\n" +
                    "}";
            } else if(type.equalsIgnoreCase("blog")){
                query="{\n" +
                    "\"filter\": {\n" +
                    "    \"query\": {\n" +
                    "      \"bool\": {\n" +
                    "        \"must\": {\n" +
                    "            \"terms\": {\n" +
                    "            \"authorOrganization.id\": [7828]\n" +
                    "            }\n" +
                    "          }\n" +
                    "      }\n" +
                    "    }\n" +
                    "  }\n" +
                    "}";
                type="dataarticle";
            }

            else {
                if (sort != null && !sort.isEmpty()) {
                    sort = "  \"sort\": [\n" +
                        "    {\n" +
                        "      \""+sort+"\": \"desc\"\n" +
                        "    }\n" +
                        "  ],\n";
                } else {
                    sort = "\n";
                }
                Integer sizes = 50;
                query = "{\n" +
                    sort +
                    "\"size\":"+ sizes +
                    "}";
            }
        } else {
            query = queryBody;
        }
        //System.out.println(" Query "+ query);
        JsonObject result = searchIndex(query, type);
        return result;
    }


    public JsonObject providerSearch(String type) {
        DataProvider dataProvider=dataProviderRepository.getProvider();
        Long providerId=dataProvider.getId();
        String query = "{}";
        JsonObject result=new JsonObject();
        if (type != null) {
            if (type.equalsIgnoreCase("recent")) {
                Integer size=3;
                query = "{\n" +
                    "  \"_source\": {\n" +
                    "    \"include\": [\n" +
                    "      \"providerName\",\n" +
                    "      \"completeness\",\n" +
                    "      \"id\",\n" +
                    "      \"updatedDate\",\n" +
                    "      \"createdDate\",\n" +
                    "      \"mainDataIndustry.dataIndustry\",\n" +
                    "       \"mainDataCategory.datacategory\",\n" +
                    "\"recordID\",\n" +
                    "\"logo.logo\",\n" +
                    "\"scoreOverall.description\",\n" +
                    "\"ownerOrganization.name\",\n" +
                    "\"shortDescription\"\n" +
                    "]\n" +
                    "  },\n" +
                    " \"size\":" + size +
                    ",  \"sort\": [\n" +
                    "    {\n" +
                    "      \"createdDate\": \"desc\"\n" +
                    "    },\n" +
                    "\t{\n" +
                    "      \"completeness\": \"desc\"\n" +
                    "    }\n" +
                    "  ]\n"+
                    ",\"filter\": {\n" +
                    "    \"query\": {\n" +
                    "      \"bool\": {\n" +
                    "        \"must\": {\n" +
                    "            \"range\": {\n" +
                    "              \"completeness\": {\n" +
                    "                \"from\": "+50+
                    ",                \"to\": "+100+
                    "              }\n" +
                    "            }\n" +
                    "          }\n" +
                    "      }\n" +
                    "    }\n" +
                    "  }\n" +
                    "}";
                result = searchIndex(query, "dataprovider");

            }else if (type.equalsIgnoreCase("featured")) {
                Integer size=1;
                query = "{\n" +
                    "  \"_source\": {\n" +
                    "    \"include\": [\n" +
                    "      \"providerName\",\n" +
                    "      \"id\",\n" +
                    "      \"completeness\",\n" +
                    "      \"updatedDate\",\n" +
                    "      \"createdDate\",\n" +
                    "      \"mainDataIndustry.dataIndustry\",\n" +
                    "       \"mainDataCategory.datacategory\",\n" +
                    "\"recordID\",\n" +
                    "\"logo.logo\",\n" +
                    "\"scoreOverall.description\",\n" +
                    "\"ownerOrganization.name\",\n" +
                    "\"scoreOverall.id\",\n" +
                    "\"shortDescription\"\n" +
                    "]\n" +
                    "  },\n" +
                    " \"size\":" + size +
                    ",\"filter\": {\n" +
                    "    \"query\": {\n" +
                    "      \"match\": {\n" +
                    "       \"id\":" + 134 +
                    "      }\n" +
                    "    }\n" +
                    "  }\n" +
                    "}";
                result = searchIndex(query, "dataprovider");
                // System.out.println(query);
            }
            else if(type.equalsIgnoreCase("unregistered"))
            {
            	Integer size=10;
                query = "{\n" +
                    "  \"_source\": {\n" +
                    "    \"include\": [\n" +
                    "      \"providerName\",\n" +
                    "      \"completeness\",\n" +
                    "      \"id\",\n" +
                    "      \"updatedDate\",\n" +
                    "      \"createdDate\",\n" +
                    "      \"scoreOverall\",\n" +
                    "      \"scoreInvestorsWillingness\",\n" +
                    "      \"scoreValue\",\n" +
                    "      \"mainDataIndustry\",\n" +
                    "       \"mainDataCategory\",\n" +
                    "\"recordID\",\n" +
                    "\"logo.logo\",\n" +
                    "\"scoreOverall.description\",\n" +
                    "\"ownerOrganization.name\",\n" +
                    "\"shortDescription\"\n" +
                    "]\n" +
                    "  },\n" +
                    " \"size\":" + size +
                    ",  \"sort\": [\n" +
                    "{\n" +
                    "      \"completeness\": \"desc\"\n" +
                    "    },\n" +
                    "\t{\n" +
                    "      \"scoreOverall.sortorder\": \"asc\"\n" +
                    "    },\n" +
                    "\t{\n" +
                    "      \"scoreInvestorsWillingness.sortorder\": \"desc\"\n" +
                    "    }\n" +
                    "  ]\n"+
                    "}";
                result = searchIndex(query, "dataprovider");
            }
            else {
                JsonObject result2 = new JsonObject();
                result2 = new JsonObject();
                result2.addProperty("error", "403");
                result2.addProperty("message", "Pass 'recent' or 'featured' in type.!");

                result=result2;
            }

        }

        return result;
    }
    public JsonObject SearchDataProvider(String sort,String type) {
    	List<Long> list = new ArrayList<Long>();
    //int dataproviderId=388;
        String query = "{}";
        JsonObject result=new JsonObject();
        if (type != null) {
             if(type.equalsIgnoreCase("Transactional"))
            {
            	 Query q = entityManager.createNativeQuery("SELECT * FROM datamarketplace.data_provider dt where dt.record_id='rec7He5XgmxgLxx3J' or dt.record_id='recvBkHZL1ljyYUhu' or dt.record_id='recOZFOMWQrfFNEvL' or dt.record_id='recx6Y6K183F115J0' or dt.record_id='recXVyx1cy6jny9Bg' or dt.record_id='recKdLQpCYJ11fQzQ' or dt.record_id='recG1LvTDWpK9pnTU'");
                 List<?> authors = q.getResultList();
                 for(int i=0; i<authors.size();i++){
                	 DataProvider dataProvider = new DataProvider();
                     Object[] row = (Object[]) authors.get(i);
                     dataProvider.setId(Long.parseLong(row[0].toString()));
                     list.add(dataProvider.getId());
                 }
            }
                 if(type.equalsIgnoreCase("jobs"))
                 {
                	 Query q = entityManager.createNativeQuery("SELECT * FROM datamarketplace.data_provider dt where dt.record_id='recY6bjgIDbVjrWzZ' or dt.record_id='recZb7lQ3Wt5PkWVm' or dt.record_id='recKNP08sL01Zjron' or dt.record_id='recxNrIhjhoXpEHsi'");
                     List<?> authors = q.getResultList();
                     for(int i=0; i<authors.size();i++){
                    	 DataProvider dataProvider = new DataProvider();
                         Object[] row = (Object[]) authors.get(i);
                         dataProvider.setId(Long.parseLong(row[0].toString()));
                         list.add(dataProvider.getId());
                 }
                 }
                     if(type.equalsIgnoreCase("location"))
                     {
                    	 Query q = entityManager.createNativeQuery("SELECT * FROM datamarketplace.data_provider dt where dt.record_id='recRke6gukyNevwWT' or dt.record_id='rec1hhV4Ai0XRo0fy' or dt.record_id='recWkc2jifNwGZQ36' or dt.record_id='recmpK8DlPlqu5lsV' or dt.record_id='recnnHGk05Di7MQXr' or dt.record_id='reccDX2GxWafdB9Gz' or dt.record_id='recAJi6lmQCh8H8Wm' or dt.record_id='recZ0s8x0OvTtoqSF' or dt.record_id='recBivhc8zyA8RqUW' or dt.record_id='reccJsFFOeRPLo8hN'or dt.record_id='recrOVJOKKKf9Sf2e'");
                         List<?> authors = q.getResultList();
                         for(int i=0; i<authors.size();i++){
                        	 DataProvider dataProvider = new DataProvider();
                             Object[] row = (Object[]) authors.get(i);
                             dataProvider.setId(Long.parseLong(row[0].toString()));
                             list.add(dataProvider.getId());
                     }
                     }
                         if(type.equalsIgnoreCase("shipping"))
                         {
                        	 Query q = entityManager.createNativeQuery("SELECT * FROM datamarketplace.data_provider dt where dt.record_id='recxRxqIxrhqUuYZ9' or dt.record_id='recoL199T9z1jHHj3' or dt.record_id='reczq7eCmlXWGt1aX' or dt.record_id='reccjP9FvVZGUZqPy' or dt.record_id='recine7Frmk01HlF6' or dt.record_id='recyhB74dgQ217miR' or dt.record_id='rechb0NhhwG2E4woY' or dt.record_id='rec9AC3o3Cw6c3cAr' or dt.record_id='recBz9TFGa4wU4VKH' or dt.record_id='recd8jlVXMRlGV5Yv'");
                             List<?> authors = q.getResultList();
                             for(int i=0; i<authors.size();i++){
                            	 DataProvider dataProvider = new DataProvider();
                                 Object[] row = (Object[]) authors.get(i);
                                 dataProvider.setId(Long.parseLong(row[0].toString()));
                                 list.add(dataProvider.getId());
                         }
                         }
                         if(type.equalsIgnoreCase("unregistered"))
                         {
                        	 Query q = entityManager.createNativeQuery("SELECT * FROM datamarketplace.data_provider dt where dt.record_id='recEFafxWv2fVQnqc' or dt.record_id='recY6bjgIDbVjrWzZ' or dt.record_id='recKNP08sL01Zjron' or dt.record_id='recxm1HvdOgfLUFOU' or dt.record_id='recxRxqIxrhqUuYZ9' or dt.record_id='recoL199T9z1jHHj3' or dt.record_id='reczq7eCmlXWGt1aX' or dt.record_id='reccjP9FvVZGUZqPy' or dt.record_id='recRke6gukyNevwWT' or dt.record_id='rec1hhV4Ai0XRo0fy' or dt.record_id='recWkc2jifNwGZQ36' or dt.record_id='rec7He5XgmxgLxx3J' or dt.record_id='recvBkHZL1ljyYUhu' or dt.record_id='recP7OAz5fIMOoR5B' or dt.record_id='recc8p83nTi8SBedB' or dt.record_id='recGWfyzrEJvQiYmQ' or dt.record_id='recbBwRBqCl3VLHgX' or dt.record_id='recaHLbrl0jb8CV1a'");
                             List<?> authors = q.getResultList();
                             for(int i=0; i<authors.size();i++){
                            	 DataProvider dataProvider = new DataProvider();
                                 Object[] row = (Object[]) authors.get(i);
                                 dataProvider.setId(Long.parseLong(row[0].toString()));
                                 list.add(dataProvider.getId());
                         }
                         }
                     //System.out.println(list);
            	Integer size=20;
            	String sortAdded="";
            	if(null!=sort && !sort.equalsIgnoreCase("null"))
            	{
            		sort = sort.substring(1);
            		sortAdded=sort;
            	}
            	else
            	{
            		sortAdded= " \"size\":" + size +
                            
                            ",  \"sort\": [\n" +
                            "{\n" +
                            "      \"completeness\": \"desc\"\n" +
                            "    },\n" +
                            "\t{\n" +
                            "      \"scoreOverall.sortorder\": \"asc\"\n" +
                            "    },\n" +
                            "\t{\n" +
                            "      \"scoreInvestorsWillingness.sortorder\": \"desc\"\n" +
                            "    }\n" +
                            "  ]\n" +
                           "  }";
            	}

                query = "{\n" +
                    "  \"_source\": {\n" +
                    "    \"include\": [\n" +
                    "      \"providerName\",\n" +
                    "      \"completeness\",\n" +
                    "      \"id\",\n" +
                    "      \"updatedDate\",\n" +
                    "      \"createdDate\",\n" +
                    "      \"scoreOverall\",\n" +
                    "      \"scoreInvestorsWillingness\",\n" +
                    "      \"scoreValue\",\n" +
                    "      \"mainDataIndustry\",\n" +
                    "       \"mainDataCategory\",\n" +
                    "       \"consumerPartners.id\",\n" +
                    "       \"distributionPartners.id\",\n" +
                    "       \"providerPartners.id\",\n" +
                    "       \"providerTags\",\n" +
                    "\"recordID\",\n" +
                    "\"logo.logo\",\n" +
                    "\"scoreOverall.description\",\n" +
                    "\"ownerOrganization.name\",\n" +
                    "\"shortDescription\"\n" +
                    "]\n" +
                    "  },\n" +
                    " \"filter\": {\n" +
                    "\"query\": {\n" +
                    "\"bool\": {\n" +
                    "\"must\": {\n" +
                    "\"terms\": {\n" +
                    "\"id\":" +
                    list+
                    "    }\n" +
                    "    }\n" +
                    "    }\n" +
                    "    }\n" +
                    "    },\n" +
                    sortAdded
                    ;
                result = searchIndex(query, "dataprovider");
                JsonArray hitsArray = result.getAsJsonObject("hits").getAsJsonArray("hits");
                Gson gson = new Gson();
                User user = userService.getUser();
                for (JsonElement e : hitsArray) {
                    JsonObject obj = e.getAsJsonObject();
                    JsonObject sourceObject = obj.getAsJsonObject("_source");
        			if (null!=sourceObject) {
        				
                        DataProvider dataProvider = gson.fromJson(sourceObject, DataProvider.class);
                        if(null!=dataProvider.getConsumerPartners() && null!=dataProvider.getDistributionPartners() && null!=dataProvider.getProviderPartners())
                        {
                            Long partners=(long) (dataProvider.getConsumerPartners().size()+dataProvider.getDistributionPartners().size()+dataProvider.getProviderPartners().size());
                            if(null!=partners)
                            {
                                dataProvider.setPartnersCount(partners);
                            }
                        }
                        if(null!= dataProvider.getProviderTags())
                        {
                        	 Long providerTagCount = (long) dataProvider.getProviderTags().size();
                       	  if(null!=providerTagCount)
                       	  {
                                 dataProvider.setProviderTagCount(providerTagCount);
                       	  }
                        }
        		    Long resourceCount = dataProviderRepository.findResourceCount(dataProvider.getId());
                    Long ArticleCount = dataProviderRepository.findRelavantArticleCount(dataProvider.getId());
                    if(null!=resourceCount && null!=ArticleCount)
                    {
                    	Long relavantResourceArticle = resourceCount+ ArticleCount;
                        dataProvider.setRelavantResourceArticle(relavantResourceArticle);	
                    }
                    /*List<DataComment> commentsCount = dataCommentRepository.getCommentByRecordID(dataProvider.getId(),user.getId());
                    if(null!=commentsCount)
                    {
                        dataProvider.setCommentsCount(Long.valueOf(commentsCount.size()));
                    }*/
        		   obj.add("_source", gson.toJsonTree(dataProvider));
        }
        }
            }
            else {
                JsonObject result2 = new JsonObject();
                result2 = new JsonObject();
                result2.addProperty("error", "403");
                result2.addProperty("message", "Pass 'recent' or 'featured' in type.!");

                result=result2;
            }
        return result;
    }
    public JsonObject searchUnrestrictedProviders(String type) {
        JsonObject result=new JsonObject();
        result = searchIndex(type, "dataprovider");
         return result;    	
    }
    public JsonObject SearchDataProviderDetail(String type) {
        String query = "{}";
        JsonObject result=new JsonObject();
        List<String>list=new ArrayList<String>();
   	 Query q = entityManager.createNativeQuery("SELECT * FROM datamarketplace.unrestricted_providers");
     List<?> authors = q.getResultList();
     for(int i=0; i<authors.size();i++){
        // Object[] row = (Object[]) authors.get(i);
         list.add((String) authors.get(i));
     }
if(list.contains(type))
{
        query = "{\n" +
                "  \"_source\": {\n" +
                "    \"exclude\": [\n" +
                "\"ownerOrganization.description\"\n" +
                "],\n" +
                "    \"include\": [\n" +
                "      \"assetClasses\",\n" +
                "      \"authoredArticles\",\n" +
                "      \"authoredArticles.publishedDate\",\n" +
                "      \"authoredArticles.purpose\",\n" +
                "      \"authoredArticles.summaryHTML\",\n" +
                "      \"authoredArticles.title\",\n" +
                "      \"authoredArticles.url\",\n" +
                "      \"categories\",\n" +
                "      \"collectionMethodsExplanation\",\n" +
                "      \"completeness\",\n" +
                "      \"consumerPartners.id\",\n" +
                "      \"consumerPartners.mainDataIndustry.dataIndustry\",\n" +
                "      \"consumerPartners.mainDataCategory.datacategory\",\n" +
                "      \"consumerPartners.logo.logo\",\n" +
                "      \"consumerPartners.ownerOrganization.name\",\n" +
                "      \"consumerPartners.shortDescription\",\n" +
                "      \"consumerPartners.providerName\",\n" +
                "      \"consumerPartners.recordID\",\n" +
                "      \"consumerPartners.completeness\",\n" +
                "      \"consumerPartners.createdDate\",\n" +
                "      \"consumerPartners.updatedDate\",\n" +
                "      \"consumerPartners.scoreOverall\",\n" +
                "      \"consumerPartners.scoreOverall.description\",\n" +
                "      \"competitors.id\",\n" +
                "      \"competitors.mainDataIndustry.dataIndustry\",\n" +
                "      \"competitors.mainDataCategory.datacategory\",\n" +
                "      \"competitors.logo.logo\",\n" +
                "      \"competitors.ownerOrganization.name\",\n" +
                "      \"competitors.shortDescription\",\n" +
                "      \"competitors.providerName\",\n" +
                "      \"competitors.recordID\",\n" +
                "      \"competitors.completeness\",\n" +
                "      \"competitors.createdDate\",\n" +
                "      \"competitors.updatedDate\",\n" +
                "      \"competitors.scoreOverall\",\n" +
                "      \"competitors.scoreOverall.description\",\n" +
                "      \"creatorUserId\",\n" +
                "      \"createdDate\",\n" +
                "      \"dataProvider\",\n" +
                "      \"dataRoadmap\",\n" +
                "      \"summary\",\n" +
                "      \"dataUpdateFrequency\",\n" +
                "      \"deliveryFrequencyNotes\",\n" +
                "      \"dataUpdateFrequencyNotes\",\n" +
                "      \"dateCollectionBegan\",\n" +
                "      \"dateCollectionRangeExplanation\",\n" +
                "      \"deliveryFormats\",\n" +
                "      \"deliveryMethods\",\n" +
                "      \"distributionPartners.id\",\n" +
                "      \"distributionPartners.mainDataIndustry.dataIndustry\",\n" +
                "      \"distributionPartners.mainDataCategory.datacategory\",\n" +
                "      \"distributionPartners.logo.logo\",\n" +
                "      \"distributionPartners.ownerOrganization.name\",\n" +
                "      \"distributionPartners.recordID\",\n" +
                "      \"distributionPartners.shortDescription\",\n" +
                "      \"distributionPartners.providerName\",\n" +
                "      \"distributionPartners.completeness\",\n" +
                "      \"distributionPartners.createdDate\",\n" +
                "      \"distributionPartners.updatedDate\",\n" +
                "      \"distributionPartners.scoreOverall\",\n" +
                "      \"distributionPartners.scoreOverall.description\",\n" +
                "      \"features\",\n" +
                "      \"freeTrialAvailable\",\n" +
                "      \"geographicalFoci.description\",\n" +
                "      \"geographicalFoci.id\",\n" +
                "      \"historicalDateRange\",\n" +
                "      \"industries\",\n" +
                "      \"investor_clients\",\n" +
                "      \"scoreInvestorsWillingness.description\",\n" +
                "      \"investorTypes.id\",\n" +
                "      \"investorTypes.description\",\n" +
                "      \"keyDataFields\",\n" +
                "      \"logo.logo\",\n" +
                "      \"longDescription\",\n" +
                "      \"mainAssetClass\",\n" +
                "      \"mainDataCategory.datacategory\",\n" +
                "      \"mainDataCategory.id\",\n" +
                "      \"mainDataCategory.explanation\",\n" +
                "      \"mainDataCategory.hashtag\",\n" +
                "      \"mainDataIndustry.dataIndustry\",\n" +
                "      \"mainDataIndustry.id\",\n" +
                "      \"mainDataIndustry.explanation\",\n" +
                "      \"mainDataIndustry.hashtag\",\n" +
                "      \"notes\",\n" +
                "      \"orgRelationshipStatus\",\n" +
                "      \"scoreOverall.description\",\n" +
                "      \"ownerOrganization\",\n" +
                "      \"potentialDrawbacks\",\n" +
                "      \"pricingModels\",\n" +
                "      \"productDetails\",\n" +
                "      \"providerName\",\n" +
                "      \"providerType.providerType\",\n" +
                "      \"providerType.id\",\n" +
                "      \"providerType.explanation\",\n" +
                "      \"providerPartners.id\",\n" +
                "      \"providerPartners.mainDataIndustry\",\n" +
                "      \"providerPartners.mainDataIndustry.id\",\n" +
                "      \"providerPartners.mainDataIndustry.dataIndustry\",\n" +
                "      \"providerPartners.mainDataCategory\",\n" +
                "      \"providerPartners.mainDataCategory.id\",\n" +
                "      \"providerPartners.mainDataCategory.datacategory\",\n" +
                "      \"providerPartners.logo.logo\",\n" +
                "      \"providerPartners.ownerOrganization.name\",\n" +
                "      \"providerPartners.providerName\",\n" +
                "      \"providerPartners.shortDescription\",\n" +
                "      \"providerPartners.completeness\",\n" +
                "      \"providerPartners.createdDate\",\n" +
                "      \"providerPartners.updatedDate\",\n" +
                "      \"providerPartners.scoreOverall\",\n" +
                "      \"providerPartners.scoreOverall.description\",\n" +
                "      \"providerTags\",\n" +
                "      \"providerPartners.recordID\",\n" +
                "      \"languagesAvailable\",\n" +
                "      \"sampleOrPanelSize\",\n" +
                "      \"deliveryFrequencies\",\n" +
                "      \"dataLicenseType\",\n" +
                "      \"dataLicenseTypeDetails\",\n" +
                "      \"useCasesOrQuestionsAddressed\",\n" +
                "      \"legalAndComplianceIssues\",\n" +
                "      \"publicCompaniesCovered\",\n" +
                "      \"scoreReadiness.description\",\n" +
                "      \"relevantArticles\",\n" +
                "      \"recordID\",\n" +
                "      \"securityTypes\",\n" +
                "      \"shortDescription\",\n" +
                "      \"sources\",\n" +
                "      \"scoreUniqueness.description\",\n" +
                "      \"uniqueValueProps\",\n" +
                "      \"updatedDate\",\n" +
                "      \"scoreValue.description\",\n" +
                "      \"website\",\n" +
                "      \"relevantSectors\",\n" +
                "      \"scoreReadiness\",\n" +
                "      \"scoreUniqueness\",\n" +
                "      \"scoreInvestorsWillingness\",\n" +
                "      \"scoreValue\",\n" +
                "      \"scoreOverall\",\n" +
                "      \"deliveryMethods\",\n" +
                "      \"deliveryFormats\",\n" +
                "      \"deliveryFrequencies\",\n" +
                "      \"freeTrialAvailable\",\n" +
                "      \"inactive\",\n" +
                "      \"link\",\n" +
                "      \"link.link\",\n" +
                "      \"relevantSectors\",\n" +
                "      \"createdDate\",\n" +
                "      \"profileVerificationStatus\",\n" +
                "      \"numberOfDataSources\",\n" +
                "      \"dataSourcesDetails\",\n" +
                "      \"numberOfAnalystEmployees\",\n" +
                "      \"ownerOrganization.name\",\n" +
                "      \"ownerOrganization.financialPosition\",\n" +
                "      \"yearFirstDataProductLaunched\",\n" +
                "      \"secRegulated\",\n" +
                "      \"investorClientBucket\",\n" +
                "      \"investorClientBucket.id\",\n" +
                "      \"investorClientBucket.description\",\n" +
                "      \"investorClientsCount\",\n" +
                "      \"organizationType\",\n" +
                "      \"managerType\",\n" +
                "      \"strategies.id\",\n" +
                "      \"strategies.description\",\n" +
                "      \"researchStyles\",\n" +
                "      \"investingTimeFrame\",\n" +
                "      \"equitiesMarketCap\",\n" +
                "      \"equitiesStyle\",\n" +
                "      \"accessOffered\",\n" +
                "      \"dataProductTypes\",\n" +
                "      \"dataTypes\",\n" +
                "      \"publicEquitiesCoveredRange\",\n" +
                "      \"identifiersAvailable\",\n" +
                "      \"pointInTimeAccuracy\",\n" +
                "      \"dataGaps\",\n" +
                "      \"dataGapsReasons\",\n" +
                "      \"duplicatesCleaned\",\n" +
                "      \"includesOutliers\",\n" +
                "      \"outlierReasons\",\n" +
                "      \"biases\",\n" +
                "      \"normalized\",\n" +
                "      \"lagTime\",\n" +
                "      \"paymentMethodsOffered\",\n" +
                "      \"subscriptionModel\",\n" +
                "      \"subscriptionTimePeriod\",\n" +
                "      \"monthlyPriceRange\",\n" +
                "      \"minimumYearlyPrice\",\n" +
                "      \"maximumYearlyPrice\",\n" +
                "      \"pricingDetails\",\n" +
                "      \"trialPricingModel\",\n" +
                "      \"trialDuration\",\n" +
                "      \"trialAgreementType\",\n" +
                "      \"trialPricingDetails\",\n" +
                "      \"competitiveDifferentiators\",\n" +
                "      \"publicEquitiesCoveredCount\",\n" +
                "      \"discussionCleanliness\",\n" +
                "      \"datasetSize\",\n" +
                "      \"datasetNumberOfRows\",\n" +
                "      \"discussionAnalytics\",\n" +
                "      \"outOfSampleData\",\n" +
                "      \"sampleOrPanelBiases\",\n" +
                "      \"pricingModel\",\n" +
                "      \"researchMethods\",\n" +
                "      \"outOfSampleData\",\n" +
                "      \"researchMethods\"\n" +
                "]\n" +
                "  },\n" +
                "\"filter\": {\n" +
                "\"query\": {\n" +
                "\"match\": {\n" +
                "\"recordID\":" +
                "\""+type+ "\""+
                "    }\n" +
                "    }\n" +
                "    }\n" +
                "}";
        result = searchIndex(query, "dataprovider");
}
else
{
	query = "{\n" +
            "  \"_source\": {\n" +
            "    \"exclude\": [\n" +
            "\"id\"\n" +
            "],\n" +
            "    \"include\": [\n" +
               "\"providerName\",\n" +
            "\"shortDescription\"\n" +
            "]\n" +
            "  },\n" +
            "\"filter\": {\n" +
            "\"query\": {\n" +
            "\"match\": {\n" +
            "\"recordID\":" +
            "\""+type+ "\""+
            "    }\n" +
            "    }\n" +
            "    }\n" +
            "}";
    result = searchIndex(query, "dataprovider");
}
		return result;
   
    }
    
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Timed
    public JsonObject eventsSearch(String type) {
        Date fromDate = new Date();
        Date toDate = addYears(fromDate, 2);
        String isoDatePattern = "yyyy-MM-dd'T'HH:mm:dd'Z'";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(isoDatePattern);
        String fDate = simpleDateFormat.format(new Date());
        String tDate = simpleDateFormat.format(toDate);
        String query="{\n" +
            "  \"_source\": {\n" +
            "    \"include\": [      \n" +
            "      \"eventEditionNames\",\n" +
            "      \"topics\",\n" +
            "      \"description\",     \n" +
            "      \"startTime\",\n" +
            "      \"endTime\",\n" +
            "      \"lenthInDays\",\n" +
            "\t  \"categories\",\n" +
            "      \"lastUpdated\",\n" +
            "      \"createdTime\",\n" +
            "      \"completeness\",\n" +
            "      \"industries\",      \n" +
            "      \"eventTypes\"\n" +
            "    ]\n" +
            "  },\n" +
            "  \"sort\": [\n" +
            "    {\n" +
            "      \"startTime\": \"asc\"\n" +
            "    },\n" +
            "\t{\n" +
            "      \"completeness\": \"desc\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"size\": 20,\n" +
            "  \"filter\": {\n" +
            "    \"query\": {\n" +
            "      \"bool\": {\n" +
            "        \"must\": [\n" +
            "          {\n" +
            "            \"terms\": {\n" +
            "              \"topics.id\": [\n" +
            "                2\n" +
            "              ]\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            \"range\": {\n" +
            "              \"startTime\": {\n" +
            "                \"from\": \""+fDate+"\",\n" +
            "                \"to\": \""+tDate+"\"\n" +
            "              }\n" +
            "            }\n" +
            "          }\n" +
            "        ]\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}";
        JsonObject result = searchIndex(query, type);
        return result;
    }

    private String generateUnlockedCategoryFilter(String query, List<Long> unlockedDataCategoryIDs, List<Long> overrideCategoryIDs ) {
        Gson gson = new Gson();
        JsonElement element = gson.fromJson (query, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        JsonElement filter = jsonObj.get("filter");
        List<String> userPermissions = userService.getLoginUserEffectiveRoles();
        if(filter != null)
        {
            JsonElement bool = filter.getAsJsonObject().get("bool");
            if(bool == null) {
                JsonElement queryElement = filter.getAsJsonObject().get("query");
                if (queryElement != null) {
                    bool = queryElement.getAsJsonObject().get("bool");
                }
            }
            if (bool != null) {
                JsonElement should = bool.getAsJsonObject().get("should");
                if (should != null) {
                    if (should.isJsonArray()) {
                        addUserPermissions(userPermissions, gson, should, true, true);
                        if(!unlockedDataCategoryIDs.isEmpty()) {
                            JsonElement providerIDs = gson.fromJson("{\"terms\": {\"id\":[" + unlockedDataCategoryIDs.toString() + "]}", JsonElement.class);
                            should.getAsJsonArray().add(providerIDs);
                        }
                    } else {
                        //Don't we need to add user permission in this block?????
                        if(!unlockedDataCategoryIDs.isEmpty()) {
                            JsonElement providerIDs = gson.fromJson("{\"id\": [" + unlockedDataCategoryIDs.toString() + "]}", JsonElement.class);
                            JsonArray terms = new JsonArray();
                            terms.add(providerIDs);
                            terms.add(should.getAsJsonObject());
                            bool.getAsJsonObject().add("terms", terms);
                        }
                    }
                }
                else {
                    JsonArray shouldElement = gson.fromJson("[]", JsonArray.class);
                    if(!unlockedDataCategoryIDs.isEmpty())
                    {
                        JsonElement terms = gson.fromJson("{\"terms\": {\"id\":" + unlockedDataCategoryIDs.toString() + "}}", JsonElement.class);
                        shouldElement.add(terms);
                    }

                    for(String permission : userPermissions)
                    {
                        JsonElement providerPermissionElement = gson.fromJson("{\"match\": {\"userPermission\":" + permission + "}}", JsonElement.class);
                        shouldElement.add(providerPermissionElement);
                    }
                    //2018-02-21:RP: Only unlocked providers will be showing when this filter is selected, unlocked categories are not to be shown
                    if(!overrideCategoryIDs.isEmpty()) {
                        addCategoriesFilter(overrideCategoryIDs, shouldElement, gson);
                    }
                    bool.getAsJsonObject().add("should", shouldElement);
                }
            }
        }
        else
        {
            filter = gson.fromJson("{\"query\":{\"bool\":{\"should\":[]}}}", JsonElement.class);
            JsonArray shouldElement = filter.getAsJsonObject().get("query").getAsJsonObject().get("bool").getAsJsonObject().get("should").getAsJsonArray();
            if(!unlockedDataCategoryIDs.isEmpty())
            {
                JsonElement terms = gson.fromJson("{\"terms\": {\"id\":" + unlockedDataCategoryIDs.toString() + "}}", JsonElement.class);
                shouldElement.add(terms);
            }
            if(!overrideCategoryIDs.isEmpty()) {
                addCategoriesFilter(overrideCategoryIDs, shouldElement, gson);
            }
            addUserPermissions(userPermissions, gson, shouldElement, true, false);
            jsonObj.add("filter", filter);
        }
        return jsonObj.toString();
    }


    private String generateFollowedCategoryFilter(String query, List<Long> followedDataCategoryIDs) {
        Gson gson = new Gson();
        JsonElement element = gson.fromJson (query, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        JsonElement filter = jsonObj.get("filter");

        if(filter != null)
        {
            JsonElement bool = filter.getAsJsonObject().get("bool");
            if(bool == null) {
                JsonElement queryElement = filter.getAsJsonObject().get("query");
                if (queryElement != null) {
                    bool = queryElement.getAsJsonObject().get("bool");
                }
            }
            if (bool != null) {
                JsonElement must = bool.getAsJsonObject().get("must");
                if (must != null) {
                    if (must.isJsonArray()) {
                        JsonElement providerIDs = gson.fromJson("{\"terms\": {\"id\":" + followedDataCategoryIDs.toString() + "}}", JsonElement.class);
                        must.getAsJsonArray().add(providerIDs);
                    } else {
                        JsonElement providerIDs = gson.fromJson("{\"terms\": {\"id\":" + followedDataCategoryIDs.toString() + "}}", JsonElement.class);
                        JsonArray mustArray = new JsonArray();
                        mustArray.add(providerIDs);
                        mustArray.add(must.getAsJsonObject());
                        bool.getAsJsonObject().add("must", mustArray);
                    }
                } else {
                    JsonElement providerIDs = gson.fromJson("[{\"terms\": {\"id\":" + followedDataCategoryIDs.toString() + "}}]", JsonArray.class);
                    bool.getAsJsonObject().add("must", providerIDs);
                }
            }
        }
        else
        {
            filter = gson.fromJson("{\"query\":{\"bool\":{\"must\":[{\"terms\": {\"id\":" + followedDataCategoryIDs.toString() + "}}]}}}", JsonElement.class);
            jsonObj.add("filter", filter);
        }
        return jsonObj.toString();
    }

    public DataProvider getProvider(DataProvider dataProvider) {
        // TODO Auto-generated method stub
        //completenessss
        DataProvider dataProviders=new DataProvider();
        dataProviders.setId(dataProvider.getId());
        dataProviders.setProviderName(dataProvider.getProviderName());
        dataProviders.setCompleteness(dataProvider.getCompleteness());
        dataProviders.setUpdatedDate(dataProvider.getUpdatedDate());
        dataProviders.setCreatedDate(dataProvider.getCreatedDate());
        dataProviders.getMainDataIndustry().setDataIndustry(dataProvider.getMainDataIndustry().getDataIndustry());
        dataProviders.getMainDataCategory().datacategory(dataProvider.getMainDataCategory().getDatacategory());
        dataProviders.setRecordID(dataProvider.getRecordID());
        dataProviders.getLogo().setLogo(dataProvider.getLogo().getLogo());
        dataProviders.getScoreOverall().setDescription(dataProvider.getScoreOverall().getDescription());
        dataProviders.getOwnerOrganization().setName(dataProvider.getOwnerOrganization().getName());
        dataProviders.getScoreOverall().setId(dataProvider.getScoreOverall().getId());
        dataProviders.setShortDescription(dataProvider.getShortDescription());
        return dataProviders;
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    @Timed
    public JsonObject pressNews(Boolean press) {
        String query = "{}";
        String type = "dataarticle";
        if (press) {
            Integer size = 20;
            query = "{   \n" +
                " \"sort\": [     \n" +
                "      {\n" +
                " \"publishedDate\": \"desc\"\n" +
                "  }   \n" +
                "  ],   \n" +
                "\"size\": 20,\n" +
                "\"filter\": {\n" +
                "\"query\": {\n" +
                " \"bool\": { \n" +
                " \"must\": { \n" +
                "  \"terms\": { \n" +
                " \"relevantProviders.id\": [211]\n" +
                "       }         \n" +
                "  }       \n" +
                "}     \n" +
                "        }\n" +
                "} \n" +
                "}";
        }
        JsonObject result = searchIndex(query, type);
        return result;
    }
    public JsonObject emailSearch(String query, String type) {
        // TODO Auto-generated method stub
        Gson gson = new Gson();
        JsonObject result = searchIndex(query, type);
        if (result == null) {
            log.error("Failed to retrieve elasticsearch result for query: {}", query);
            return new JsonObject();
        }
        JsonArray hitsArray = result.getAsJsonObject("hits").getAsJsonArray("hits");
       // System.out.println("hits"+hitsArray.size());
        if (!RESTRICTED_INDEXES.contains(type)) {
            return result;
        }
        for (JsonElement e : hitsArray) {
            JsonObject obj = e.getAsJsonObject();
            JsonObject sourceObject = obj.getAsJsonObject("_source");
            EmailAddress email = gson.fromJson(sourceObject, EmailAddress.class);
            EmailAddress emailAddress=new EmailAddress();
            email.setId(emailAddress.getId());
            emailAddress.setEmailAddress(email.getEmailAddress());
            emailAddress.setRecordID(email.getRecordID());
            email=emailAddress;
            obj.add("_source", gson.toJsonTree(email));
        }
        return result;
    }

	public String matchSearch() {
        User user = userService.getUser();
        int itemsFrom =0;
        //Long id=(long) 307;
        if(null!=request.getAttribute("itemsFrom"))
        {
           String  from =  (String) request.getAttribute("itemsFrom");
           itemsFrom = Integer.parseInt(from);
        }
        //System.out.println("itemsFrom"+itemsFrom);
       User userValue= userService.getUserValues(user.getId());
       //System.out.println("uservalue"+userValue);
	   List<Long>geographiesList=new ArrayList<Long>();
       List<Long>assetList=new ArrayList<Long>();
       List<Long>sectorsList = new ArrayList<Long>();
       List<Long>categoriesList = new ArrayList<Long>();
       String category = "";
       if(null!=userValue.getAssetClasses())
       {
       for(LookupCode lookupCode:userValue.getAssetClasses())
       {
    	   assetList.add(lookupCode.getId());
       }
       }
       if(null!=userValue.getGeographies())
       {
    	   for(LookupCode geographies:userValue.getGeographies())
    	   {
    		 List<LookupCode>lookupcode= userService.getGeographies(geographies.getId());
    		 for(LookupCode geographicRegion:lookupcode)
    		 {
    			 geographiesList.add(geographicRegion.getId());
    		 }

    	   }

       }
       if(null!=userValue.getSectors())
       {
    	   for(LookupCode sectors:userValue.getSectors())
    	   {
    		   sectorsList.add(sectors.getId());
    	   }
       }
       if(userValue.getCategories().size()>0)
       {
    	   for(DataCategory categories: userValue.getCategories())
    	   {
    		   categoriesList.add(categories.getId());
    	   }
    	   category= "          {\n" +
		            "            \"terms\": {\n" +
		            "              \"categories.id\":" +
		            categoriesList+
		            "                \n" +
		            "            }\n" +
		            "          },\n" ;
       }
       String sort ="";
      // String sort1 ="";
       if(null!= request.getAttribute("key") && null!=request.getAttribute("order"))
       {
    	   String key=(String) request.getAttribute("key");
    	   String order = (String) request.getAttribute("order");
    	   if(!key.equalsIgnoreCase("recommended"))
    	   {
    		   sort = "  \"sort\": [\n" +
   		            "    {\n" +
   		            '"' + key+ '"'+":" +'"'+ order+ '"'+"\n"+
   		            "    }\n" +
   		            "  ],\n" ;
    	   }
    	   else
    	   {
    		   sort= "  \"sort\": [\n" +
   		            "    {\n" +
   		            "      \"scoreOverall.sortorder\": \"asc\"\n" +
   		            "    },\n" +
   		            "\t{\n" +
   		            "      \"scoreInvestorsWillingness.sortorder\": \"asc\"\n" +
   		            "    },\n" +
   		            "\t{\n" +
   		            "      \"completeness\": \"desc\"\n" +
   		            "    }\n" +
   		            "  ],\n" ;
    	   }
       }
       else
       {
    	   sort= "  \"sort\": [\n" +
		            "    {\n" +
		            "      \"scoreOverall.sortorder\": \"asc\"\n" +
		            "    },\n" +
		            "\t{\n" +
		            "      \"scoreInvestorsWillingness.sortorder\": \"asc\"\n" +
		            "    },\n" +
		            "\t{\n" +
		            "      \"completeness\": \"desc\"\n" +
		            "    }\n" +
		            "  ],\n" ;
       }
      // System.out.println(sort);
		 String query="{\n" +
		            "  \"_source\": {\n" +
		            "    \"include\": [      \n" +
		            "      \"id\",\n" +
		            "      \"completeness\",\n" +
		            "      \"providerName\",     \n" +
		            "      \"scoreInvestorsWillingness\",\n" +
		            "      \"scoreOverall\",\n" +
		            "      \"userPermission\",\n" +
		            "      \"mainDataCategory\",\n" +
		            "      \"mainDataIndustry\",\n" +
		            "      \"shortDescription\",\n" +
		            "      \"updatedDate\",\n" +
		            "      \"createdDate\",\n" +
		            "      \"scoreValue\",\n" +
		            "      \"followed\",\n" +
		            "      \"categories.id\",\n" +
		            "      \"marketplaceStatus\",\n" +
		            "      \"ownerOrganization\",\n" +
		            "      \"scoreReadiness\",\n" +
		            "      \"scoreUniqueness\",\n" +
		            "      \"freeTrialAvailable\",\n" +
		            "\"recordID\",\n" +
                    "\"logo.logo\",\n" +
                    "      \"completeness\"\n" +
		            "    ]\n" +
		            "  },\n" +
		           sort+
		            "  \"size\": 20,\n" +
		            "  \"from\":" +
		            itemsFrom+",\n" +
		            "  \"filter\": {\n" +
		            "    \"query\": {\n" +
		            "      \"bool\": {\n" +
		            "        \"must\": [\n" +
		            category+
		            "          {\n" +
		            "            \"terms\": {\n" +
		            "              \"assetClasses.id\": " +
		            assetList+
		            "               \n" +
		            "            }\n" +
		            "          },\n" +
		            "          {\n" +
		            "            \"terms\": {\n" +
		            "              \"geographicalFoci.id\":" +
		            geographiesList+
		            "                \n" +
		            "            }\n" +
		            "          },\n" +
		            "          {\n" +
		            "            \"terms\": {\n" +
		            "              \"relevantSectors.id\":" +
		            sectorsList+
		            "                \n" +
		            "            }\n" +
		            "          }\n" +
		            "        ]\n" +
		            "      }\n" +
		            "    }\n" +
		            "  }\n" +
		            "}";
		return query;
		// TODO Auto-generated method stub

	}

	public List<DataResources> resourceLock(List<DataResources> resources) {
		// TODO Auto-generated method stub
        List<String> userRoles = userService.getLoginUserEffectiveRoles();
List<DataResources> listResource = new ArrayList<DataResources>();
		for(DataResources dataResource: resources)
		{
			 if (!userRoles.contains(dataResource.getUserPermission()))
     		{
				 dataResource.setLocked(true);
     		}
			 else
			 {
				 dataResource.setLocked(false);

			 }
			 listResource.add(dataResource);
		}
		return listResource;
	}
}

