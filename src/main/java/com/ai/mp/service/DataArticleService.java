package com.ai.mp.service;

import com.ai.mp.domain.DataArticle;
import com.ai.mp.repository.DataArticleRepository;
import com.ai.mp.repository.search.DataArticleSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing DataArticle.
 */
@Service
@Transactional
public class DataArticleService {

    private final Logger log = LoggerFactory.getLogger(DataArticleService.class);

    private final DataArticleRepository dataArticleRepository;

    private final DataArticleSearchRepository dataArticleSearchRepository;
    public DataArticleService(DataArticleRepository dataArticleRepository, DataArticleSearchRepository dataArticleSearchRepository) {
        this.dataArticleRepository = dataArticleRepository;
        this.dataArticleSearchRepository = dataArticleSearchRepository;
    }

    /**
     * Save a dataArticle.
     *
     * @param dataArticle the entity to save
     * @return the persisted entity
     */
    public DataArticle save(DataArticle dataArticle) {
        log.debug("Request to save DataArticle : {}", dataArticle);
        DataArticle result = dataArticleRepository.save(dataArticle);
        dataArticleSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the dataArticles.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DataArticle> findAll(Pageable pageable) {
        log.debug("Request to get all DataArticles");
        return dataArticleRepository.findAll(pageable);
    }

    /**
     *  Get one dataArticle by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DataArticle findOne(Long id) {
        log.debug("Request to get DataArticle : {}", id);
        return dataArticleRepository.findOne(id);
    }

    /**
     *  Delete the  dataArticle by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DataArticle : {}", id);
        dataArticleRepository.delete(id);
        dataArticleSearchRepository.delete(id);
    }

    /**
     * Search for the dataArticle corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DataArticle> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DataArticles for query {}", query);
        Page<DataArticle> result = dataArticleSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
    /**
     * Search for the dataArticle corresponding to the query.
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DataArticle> searchAll(Pageable pageable) {
        log.debug("Request to get a page of DataArticles");
        Page<DataArticle> result = dataArticleSearchRepository.findAll(pageable);
        return result;
    }
}
