package com.ai.mp.service;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ai.mp.domain.DataCategory;
import com.ai.mp.domain.DataFeature;
import com.ai.mp.domain.DataIndustry;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.DataProviderEdit;
import com.ai.mp.domain.DataProviderTag;
import com.ai.mp.domain.DataSource;
import com.ai.mp.domain.LookupCode;
import com.ai.mp.repository.DataArticleRepository;
import com.ai.mp.repository.DataCategoryRepository;
import com.ai.mp.repository.DataFeatureRepository;
import com.ai.mp.repository.DataIndustryRepository;
import com.ai.mp.repository.DataProviderEditRepository;
import com.ai.mp.repository.DataProviderRepository;
import com.ai.mp.repository.DataProviderTagRepository;
import com.ai.mp.repository.DataSourceRepository;
import com.ai.mp.repository.LookupCodeRepository;
import com.ai.mp.repository.search.DataProviderSearchRepository;
import com.ai.mp.utils.BasicBean;

/**
 * Service Implementation for managing DataProvider.
 */
@Service
@Transactional
public class DataProviderService {

    private final Logger log = LoggerFactory.getLogger(DataProviderService.class);

    private final DataProviderRepository dataProviderRepository;
    private final LookupCodeRepository lookupCodeRepository;
   
    private final DataProviderSearchRepository dataProviderSearchRepository;
    private final DataProviderTagRepository dataProviderTagRepository;
    private final DataFeatureRepository dataFeatureRepository;
    private final DataIndustryRepository dataIndustryRepository;
    private final DataCategoryRepository dataCategoryRepository;
    private final DataSourceRepository dataSourceRepository;
    private final MailService mailService;
    private final DataProviderEditRepository dataProviderEditRepository;
    private final UserProviderService userProviderService;
    private final EmailAddressService emailAddresService;
    public DataProviderService(DataProviderRepository dataProviderRepository, DataProviderSearchRepository dataProviderSearchRepository,DataProviderTagRepository dataProviderTagRepository,LookupCodeRepository lookupCodeRepository, DataFeatureRepository dataFeatureRepository,DataIndustryRepository dataIndustryRepository,DataCategoryRepository dataCategoryRepository
    		,DataSourceRepository dataSourceRepository,MailService mailService,DataProviderEditRepository dataProviderEditRepository,UserProviderService userProviderService,EmailAddressService emailAddresService) {
        this.dataProviderRepository = dataProviderRepository;
        this.dataProviderSearchRepository = dataProviderSearchRepository;
        this.dataProviderTagRepository=dataProviderTagRepository;
        this.lookupCodeRepository=lookupCodeRepository;
        this.dataFeatureRepository=dataFeatureRepository;
        this.dataIndustryRepository=dataIndustryRepository;
        this.dataCategoryRepository=dataCategoryRepository;
        this.dataSourceRepository=dataSourceRepository;
        this.mailService=mailService;
        this.dataProviderEditRepository=dataProviderEditRepository;
        this.userProviderService=userProviderService;
        this.emailAddresService=emailAddresService;
    }

    /**
     * Save a dataProvider.
     *
     * @param dataProvider the entity to save
     * @return the persisted entity
     */
    public DataProvider save(DataProvider dataProvider) {
        log.debug("Request to save DataProvider : {}", dataProvider);
        DataProvider result = dataProviderRepository.save(dataProvider);
        dataProviderSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the dataProviders.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DataProvider> findAll(Pageable pageable) {
        log.debug("Request to get all DataProviders");
        return dataProviderRepository.findAll(pageable);
    }

    /**
     *  Get one dataProvider by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DataProvider findOne(Long id) {
      // log.info("Request to get DataProvider : {}", id);

        DataProvider dp = dataProviderRepository.findOneWithEagerRelationships(id);
      //  System.out.println("providerRecordId"+dp.getRecordID());
        Set<DataProvider> providerPartners = dataProviderRepository.findAllProviderPartners(id);
        Set<DataProvider> distributionPartners = dataProviderRepository.findAllDistributionPartners(id);
        Set<DataProvider> consumerPartners = dataProviderRepository.findAllConsumerPartners(id);
       Set<DataProvider>competitors=dataProviderRepository.findAllCompetiors(id);
       // Set<DataProvider>complementaryProviders=dataProviderRepository.findAllComplementaryProviders(id);
      /*  log.info("Request to get Provider Partners id: {} count: {}", id, providerPartners.size());
        log.info("Request to get Distribution Partners id: {} count: {}", id, distributionPartners.size());
        log.info("Request to get Consumer Partners id: {} count: {}", id, consumerPartners.size());*/
       
        /*if (id > 1)
        {
            try {
                Set<DataArticle> da = dataProviderRepository.getRelevantArticlesForProvider(id);
               // log.info("Request to get Provider Articles id: {} count: {}", id, da.size());
                Set<DataArticle> authoredArticles = dataArticleRepository.findAllByAuthorDataProviderIdEquals(id);
                //log.info("Request to get Authored Articles id: {} count: {}", id, authoredArticles.size());
                if(da!=null)
                {
                dp.setRelevantArticles(da);
                }
                if(authoredArticles!=null)
                {
                dp.setAuthoredArticles(authoredArticles);
                }
            }
            catch (JpaObjectRetrievalFailureException e)
            {
                log.error("ID: " + id, e);
            }
        }*/
if(providerPartners!=null)
{
        dp.setProviderPartners(providerPartners);
}
if(distributionPartners!=null)
{
        dp.setDistributionPartners(distributionPartners);
}
if(consumerPartners!=null)
{
        dp.setConsumerPartners(consumerPartners);
}
if(competitors!=null)
{
	dp.setCompetitors(competitors);
}
/*if(complementaryProviders!=null)
{
	dp.setComplementaryProviders(complementaryProviders);
}*/
        return dp;
    }

    /**
     *  Get one dataProvider by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
//    @Transactional(readOnly = true)
//    public DataProvider searchOne(Long id) {
//        log.debug("Request to get DataProvider : {}", id);
//        DataProvider dataProvider = dataProviderSearchRepository.findOne(id);
//        boolean followed = false;
//        if(userService.getProvidersByInterestTypeForCurrentUser(INTEREST_TYPE_FOLLOW).contains(dataProvider))
//        {
//            followed = true;
//        }
//		dataProvider.setFollowed(followed);
//        if(userService.isAdminUser())
//            return dataProvider;
//        long categoryID = dataProvider.getMainDataCategory().getId();
//        List<Long> categoryIDs = userService.getAllCategoryIDsForCurrentUser();
//        if(!categoryIDs.contains(categoryID) && !userService.getLoginUserRoles().contains(dataProvider.getUserPermission())) {
//            DataProvider provider = new DataProvider();
//            provider.setShortDescription(dataProvider.getShortDescription());
//            DataCategory mainDataCategory = dataProvider.getMainDataCategory();
//            mainDataCategory.setLocked(true);
//            provider.setFollowed(followed);
//			mainDataCategory.setDatacategory(RandomStringUtils.randomAlphabetic(18));
//            provider.setMainDataCategory(mainDataCategory);
//            provider.setMainDataIndustry(dataProvider.getMainDataIndustry());
//            provider.setGeographicalFoci(dataProvider.getGeographicalFoci());
//            provider.setProviderName(RandomStringUtils.randomAlphabetic(dataProvider.getProviderName().length()));
//            provider.setLocked(true);
//            provider.setInvestor_clients(dataProvider.getInvestor_clients());
//            provider.setMainAssetClass(dataProvider.getMainAssetClass());
//            provider.setSecurityTypes(dataProvider.getSecurityTypes());
//            provider.setInvestorTypes(dataProvider.getInvestorTypes());
//            provider.setProviderType(dataProvider.getProviderType());
//            provider.setProviderTags(dataProvider.getProviderTags());
//            provider.setAssetClasses(dataProvider.getAssetClasses());
//            provider.setIndustries(dataProvider.getIndustries());
//
//            provider.setRecordID(dataProvider.getRecordID());
//            provider.setFeatures(dataProvider.getFeatures());
//            provider.setSources(dataProvider.getSources());
//            provider.setId(dataProvider.getId());
//            //Set Other categories
//            provider.setCategories(getCategories(categoryIDs, dataProvider.getCategories()));
//            return provider;
//        }
//        else
//        {
//            if(!categoryIDs.contains(categoryID)) {
//                DataCategory dc = dataProvider.getMainDataCategory();
//                dc.setDatacategory(RandomStringUtils.randomAlphabetic(18));
//                dc.setLocked(true);
//            }
//            dataProvider.setCategories(getCategories(categoryIDs, dataProvider.getCategories()));
//            return dataProvider;
//        }
//    }
//
//    private List getCategories(List<Long> categoryIDs, List categories)
//    {
//        Iterator<Map> itr = categories.iterator();
//        while (itr.hasNext()) {
//            Map category = itr.next();
//            long cid = Long.parseLong(category.get("id").toString());
//            if (!categoryIDs.contains(cid)) {
//                category.put("desc", RandomStringUtils.randomAlphabetic(18));
//                category.put(MASKED_TEXT, true);
//            }
//        }
//        return categories;
//    }
    /**
     *  Delete the  dataProvider by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DataProvider : {}", id);
        dataProviderRepository.delete(id);
        dataProviderSearchRepository.delete(id);
    }

    /**
     * Search for the dataProvider corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DataProvider> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DataProviders for query {}", query);
        Page<DataProvider> result = dataProviderSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    /**
     * Search for the dataProvider corresponding to the query.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DataProvider> searchAll(Pageable pageable) {
        log.debug("Request to get a page of DataProviders");
        Page<DataProvider> result = dataProviderSearchRepository.findAll(pageable);
        return result;
    }

    @Transactional(readOnly = true)
    public Set<DataProvider> getDataUpdateFrequency()
    {
        //dataProviderRepository.getDataUpdateFrequency();

        //return null;
        log.debug("Request to get  a filter from  queryupdatefrequency  ");
        return dataProviderRepository.getDataUpdateFrequency();
    }

    public void insertPartner(Long id, Long id2) {
        // TODO Auto-generated method stub
        dataProviderRepository.insertPartner(id, id2);
    }

    public void distributionPartner(DataProvider dataProvider, String distributionId) {
        // TODO Auto-generated method stub
        Long distributionPartner=Long.parseLong(distributionId);
        dataProviderRepository.deletePartner(dataProvider.getId(),distributionPartner);

        for(DataProvider dataProviderPartner:dataProvider.getDistributionPartners())
        {
            dataProviderRepository.updatePartner(dataProvider.getId(),dataProviderPartner.getId(),distributionPartner);

        }
    }

    public void providerPartners(DataProvider dataProvider, String providerPartnerId) {
        // TODO Auto-generated method stub
        Long providerPartner=Long.parseLong(providerPartnerId);
        dataProviderRepository.deletePartner(dataProvider.getId(),providerPartner);
        for(DataProvider data:dataProvider.getProviderPartners())
        {
           // System.out.println("providerPartnercalled");
            dataProviderRepository.updatePartner(dataProvider.getId(),data.getId(),providerPartner);

        }
    }

    public void consumerPartners(DataProvider dataProvider, String consumerPartnerId) {
        // TODO Auto-generated method stub
        Long consumerPartner=Long.parseLong(consumerPartnerId);
        dataProviderRepository.deletePartner(dataProvider.getId(), consumerPartner);
      //  System.out.println("consumerpartnercalled");
        for(DataProvider provider:dataProvider.getConsumerPartners())
        {
            dataProviderRepository.updatePartner(dataProvider.getId(),provider.getId(),consumerPartner);

        }
    }

    public String historicalDate(DataProvider dataProvider) {
        // TODO Auto-generated method stub
        Integer yearsBeforeToday = null;
       // Integer todayYear=LocalDate.now().getYear();
        Integer todayYear = Calendar.getInstance().get(Calendar.YEAR);
        if(null !=dataProvider.getOwnerOrganization() && null !=dataProvider.getOwnerOrganization().getYearFounded())
        {
        	Integer yearFounded=dataProvider.getOwnerOrganization().getYearFounded();
            if(yearFounded!=null)
            {
                yearsBeforeToday=todayYear-yearFounded;
            }
        }
        if(dataProvider.getDateCollectionBegan()==null && yearsBeforeToday==null)
        {
            return null;
        }
        else if(dataProvider.getDateCollectionBegan()==null)
        {
            return "up to "+yearsBeforeToday+" years";
        }
        else
        {
            Integer yearSinceCollection=todayYear-year(dataProvider.getDateCollectionBegan());
            return "up to "+yearSinceCollection+" years";
        }
    }

    private Integer year(LocalDate dateCollectionBegan) {
        // TODO Auto-generated method stub
        Integer year=dateCollectionBegan.getYear();
        return year;
    }

    
   // @Scheduled(cron = "0 0 0/3 * * ?")
   
    @Transactional
	public DataProvider addTag(DataProvider newProvider) {
		// TODO Auto-generated method stub
		if(null!=newProvider.getMainDataCategory() || null!=newProvider.getMainDataIndustry() || null!=newProvider.getMainAssetClass() || null!=newProvider.getDataUpdateFrequency())
		{
			//System.out.println("newprovider"+newProvider.getMainDataCategory().getDatacategory());
			Set<DataProviderTag> tag= dataProviderTagRepository.findProviderTag(newProvider.getMainDataCategory().getDatacategory(),newProvider.getMainDataIndustry().getDataIndustry(),newProvider.getMainAssetClass(),newProvider.getDataUpdateFrequency());
			//newProvider.setDataProviderTags(tag);
			//System.out.println("tag"+tag.getProviderTag());
			/*for(BasicBean bean:newProvider.getIndustries())
			{
				String type="Industry";
				DataProviderTag tagIndustry=dataProviderTagRepository.findProviderTag(bean.getDesc(),type);
			}*/
			//newProvider.setDataProviderTags(null);
			Set<DataProviderTag>tags=newProvider.getDataProviderTags();
			Set<DataProviderTag>providerTag=new HashSet<DataProviderTag>();
			if(null!=tags && null!=tag)
			{
				tags.addAll(tag);
				newProvider.setDataProviderTags(tags);
			}
			else
			{
				//Set<DataProviderTag> dataProviderTag= new HashSet<DataProviderTag>();
				//dataProviderTag.add(tag);
				if(null!=tag)
				{
					//System.out.println(tag.size());
					//newProvider.setDataProviderTags(tag);
					providerTag.addAll(tag);
					newProvider.setDataProviderTags(providerTag);
				}
			}
			//System.out.println("tags"+tags.size());
				if(newProvider.getdataProvidersPricingModels().size()>0)
				{
					Set<DataProviderTag> tagName = lookupcodeAddTag(newProvider.getdataProvidersPricingModels(),"Pricing Model");
					//System.out.println("tagname"+tagName.size());
					if(null!=tags)
					{
						tags.addAll(tagName);
						newProvider.setDataProviderTags(tags);

					}
					else
					{
						providerTag.addAll(tagName);
						newProvider.setDataProviderTags(providerTag);
					}
				}
                if(newProvider.getInvestorTypes().size()>0 && null!=newProvider.getInvestorTypes())
                {
					Set<DataProviderTag> tagInvestor = lookupcodeAddTag(newProvider.getInvestorTypes(),"Investor Type");
					//System.out.println("tagname"+tagInvestor.size());
					if(null!=tags)
					{
						tags.addAll(tagInvestor);
						newProvider.setDataProviderTags(tags);
					}
					else
					{
						providerTag.addAll(tagInvestor);
						newProvider.setDataProviderTags(providerTag);
					}
					
                }
            
                if(newProvider.getDataProvidersDeliveryMethods().size()>0 && null!=newProvider.getDataProvidersDeliveryMethods())
                {
					Set<DataProviderTag> tagdelivery = lookupcodeAddTag(newProvider.getDataProvidersDeliveryMethods(),"Delivery Method");
					//System.out.println("tagname"+tagdelivery.size());
					if(null!=tagdelivery)
					{
						if(null!=tags)
						{
							tags.addAll(tagdelivery);
							newProvider.setDataProviderTags(tags);
						}
						else
						{
							providerTag.addAll(tagdelivery);
							newProvider.setDataProviderTags(providerTag);
						}
					}
                }
                if(newProvider.getDeliveryFrequencies().size()>0 &&  null!=newProvider.getDataProvidersDeliveryMethods())
                {
					Set<DataProviderTag> tagfrequency = lookupcodeAddTag(newProvider.getDeliveryFrequencies(),"Delivery Frequency");
					//System.out.println("tagname"+tagfrequency.size());
					if(null!=tagfrequency)
					{
						if(null!=tags)
						{
							tags.addAll(tagfrequency);
							newProvider.setDataProviderTags(tags);
						}
						else
						{
							providerTag.addAll(tagfrequency);
							newProvider.setDataProviderTags(providerTag);
						}
					}
					
                }
                if(newProvider.getDataProvidersAssetClasses().size()>0)
                {
					Set<DataProviderTag> tagasset = lookupcodeAddTag(newProvider.getDataProvidersAssetClasses(),"Asset Class");
					if(null!=tagasset)
					{
						if(null!=tags)
						{
							tags.addAll(tagasset);
							newProvider.setDataProviderTags(tags);
						}
						else
						{
							providerTag.addAll(tagasset);
							newProvider.setDataProviderTags(providerTag);
						}
						
					}
                }
                if(newProvider.getDataProvidersRelevantSectors().size()>0)
                {
					Set<DataProviderTag> tagsector = lookupcodeAddTag(newProvider.getDataProvidersRelevantSectors(),"Sector");
					if(null!=tagsector)
					{
						if(null!=tags)
						{
							tags.addAll(tagsector);
							newProvider.setDataProviderTags(tags);
						}
						else
						{
							providerTag.addAll(tagsector);
							newProvider.setDataProviderTags(providerTag);
						}
					}
                }
                if(newProvider.getDataProvidersDeliveryFormats().size()>0)
                {
					Set<DataProviderTag> tagdeliveryformat = lookupcodeAddTag(newProvider.getDataProvidersDeliveryFormats(),"Delivery Format");
					if(null!=tagdeliveryformat)
					{
						if(null!=tags)
						{
							tags.addAll(tagdeliveryformat);
							newProvider.setDataProviderTags(tags);
						}
						else
						{
							providerTag.addAll(tagdeliveryformat);
							newProvider.setDataProviderTags(providerTag);
						}
					}
                }
                if(newProvider.getDataProvidersSecurityTypes().size()>0)
                {
					Set<DataProviderTag> tagsecuritytype = lookupcodeAddTag(newProvider.getDataProvidersSecurityTypes(),"Security Type");
					if(null!=tagsecuritytype)
					{
						if(null!=tags)
						{
							tags.addAll(tagsecuritytype);
							newProvider.setDataProviderTags(tags);
						}
						else
						{
							providerTag.addAll(tagsecuritytype);
							newProvider.setDataProviderTags(providerTag);
						}
					}
                }
                if(newProvider.getGeographicalFoci().size()>0)
                {
					Set<DataProviderTag> tagGeographicalfocus = lookupcodeAddTag(newProvider.getGeographicalFoci(),"Geographical Focus");
					if(null!=tagGeographicalfocus)
					{
						if(null!=tags)
						{
							tags.addAll(tagGeographicalfocus);
							newProvider.setDataProviderTags(tags);
						}
						else
						{
							providerTag.addAll(tagGeographicalfocus);
							newProvider.setDataProviderTags(providerTag);
						}
					}
                }
                if(newProvider.getDataProviderSources().size()>0)
                {
                	Set<DataProviderTag>dataprovidersourceTag=new HashSet<DataProviderTag>();
                	for(DataSource dataSources:newProvider.getDataProviderSources())
                	{
                    	DataSource datasource=dataSourceRepository.finById(dataSources.getId());
                		DataProviderTag data=dataProviderTagRepository.findProviderTagName(datasource.getDataSource(), "Data Source");
                		if(null!=data)
                		{
                    		dataprovidersourceTag.add(data);
                		}
                	}
                	if(null!=tags)
                	{
                		tags.addAll(dataprovidersourceTag);
    					newProvider.setDataProviderTags(tags);
                	}
                	else
                	{
                		providerTag.addAll(dataprovidersourceTag);
						newProvider.setDataProviderTags(providerTag);
                	}
                	
                }
                if(newProvider.getDataProviderFeatures().size()>0)
                {
                	Set<DataProviderTag>dataproviderfeatureTag=new HashSet<DataProviderTag>();
                	for(DataFeature dataFeatures:newProvider.getDataProviderFeatures())
                	{
                	DataFeature feature=dataFeatureRepository.finById(dataFeatures.getId());
                		DataProviderTag data=dataProviderTagRepository.findProviderTagName(feature.getDataFeature(), "Data Feature");
                		if(null!=data)
                		{
                    		dataproviderfeatureTag.add(data);
                		}
                	}
                	if(null!=tags)
                	{
                		tags.addAll(dataproviderfeatureTag);
    					newProvider.setDataProviderTags(tags);
                	}
                	else
                	{
                		providerTag.addAll(dataproviderfeatureTag);
						newProvider.setDataProviderTags(providerTag);
                	}
                	
                }
                if(newProvider.getDataProviderCategories().size()>0)
                {
                	Set<DataProviderTag>dataprovidercategoriesTag=new HashSet<DataProviderTag>();
                	for(DataCategory datacategories:newProvider.getDataProviderCategories())
                	{
                    	DataCategory category=dataCategoryRepository.finByIDEquals(datacategories.getId());
                		DataProviderTag data=dataProviderTagRepository.findProviderTagName(category.getDatacategory(), "Data Category");
                		if(null!=data)
                		{
                    		dataprovidercategoriesTag.add(data);
                		}
                	}
                	if(null!=tags)
                	{
                		tags.addAll(dataprovidercategoriesTag);
    					newProvider.setDataProviderTags(tags);
                	}
                	else
                	{
                		providerTag.addAll(dataprovidercategoriesTag);
						newProvider.setDataProviderTags(providerTag);
                	}
                }
                if(newProvider.getProviderIndustries().size()>0)
                {
                	Set<DataProviderTag>dataproviderindustriesTag=new HashSet<DataProviderTag>();
                	for(DataIndustry dataindustries:newProvider.getProviderIndustries())
                	{
                    	DataIndustry industry=dataIndustryRepository.finById(dataindustries.getId());

                		DataProviderTag data=dataProviderTagRepository.findProviderTagName(industry.getDataIndustry(), "Industry");
                		if(null!=data)
                		{
                    		dataproviderindustriesTag.add(data);
                		}
                	}
                	if(null!=tags)
                	{
                		tags.addAll(dataproviderindustriesTag);
    					newProvider.setDataProviderTags(tags);
                	}
                	else
                	{
                		providerTag.addAll(dataproviderindustriesTag);
						newProvider.setDataProviderTags(providerTag);
                	}
                }
		}
		
			return newProvider;
		
	}

	private Set<DataProviderTag> lookupcodeAddTag(Set<LookupCode> lookupcodename,String type) {
		// TODO Auto-generated method stub
		Set<DataProviderTag> tag=new HashSet<DataProviderTag>();
		for(LookupCode lookupcode:lookupcodename)
		{
			LookupCode look = lookupCodeRepository.findById(lookupcode.getId());
			DataProviderTag tagName=dataProviderTagRepository.findProviderTagName(look.getLookupcode(),type);
			if(null!=tagName)
			{
				tag.add(tagName);
			}
			
		}
		return tag;
	}

	public void updateJsonTag(DataProvider newProvider) {
		// TODO Auto-generated method stub
       /* dataProviderRepository.updateRelavantSector(newProvider.getId());
       dataProviderRepository.updateResearchMethods(newProvider.getId());
        dataProviderRepository.updateTagProviders(newProvider.getId());
       dataProviderRepository.updateFeatures(newProvider.getId());*/
        dataProviderRepository.updateTag(newProvider.getId());
        dataProviderRepository.updateTagProvider(newProvider.getId());

        /*dataProviderRepository.updateIndustry(newProvider.getId());
        dataProviderRepository.updateCategory(newProvider.getId());
        dataProviderRepository.updateSource(newProvider.getId());
        dataProviderRepository.updateAssetClass(newProvider.getId());
        dataProviderRepository.updateSecurityType(newProvider.getId());
        dataProviderRepository.updateDeliveryFormat(newProvider.getId());
        dataProviderRepository.updateDeliveryMethod(newProvider.getId());
        dataProviderRepository.updatePricingModel(newProvider.getId());*/

	}

	public void sendMailToUser(DataProvider dataProvider) {
		// TODO Auto-generated method stub
		DataProviderEdit edit=dataProviderEditRepository.findByrecordUpdatedValues(dataProvider.getRecordID());
		edit.setRecordUpdated(false);
		dataProviderEditRepository.save(edit);
		mailService.sendMailToUserProvider(dataProvider,edit.getEditorUserId());
	}

	public void updateUserProvider(DataProvider newProvider) {
		// TODO Auto-generated method stub
		userProviderService.updateUserProvider(newProvider);
		
	}

	public void updateEmailAddress(DataProvider newProvider) {
		// TODO Auto-generated method stub
		emailAddresService.updateEmailAddress(newProvider);
	}

}
