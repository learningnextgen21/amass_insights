package com.ai.mp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.DataResources;
import com.ai.mp.repository.DataResourceRepository;
import com.ai.mp.repository.search.DataResourceSearchRepository;

@Service
@Transactional
public class DataResourceService {

    private final Logger log = LoggerFactory.getLogger(DataResourceService.class);

    private final DataResourceRepository dataResourceRepository;

    private final DataResourceSearchRepository dataResourceSearchRepository;

    public DataResourceService(DataResourceRepository dataResourceRepository, DataResourceSearchRepository dataResourceSearchRepository)
    {
        this.dataResourceRepository=dataResourceRepository;
        this.dataResourceSearchRepository=dataResourceSearchRepository;
    }

    public DataResources save(DataResources dataResources) {
        // TODO Auto-generated method stub
        log.debug("Request to save DataResources : {}", dataResources);
        DataResources resources =dataResourceRepository.save(dataResources);
        //dataResourceSearchRepository.save(resources);
        return resources;
    }

    @Transactional(readOnly = true)
    public DataResources findOne(Long id) {
        // TODO Auto-generated method stub
       // log.info("Request to get DataResources : {}", id);
        DataResources resources=dataResourceRepository.findOneWithEagerRelationships(id);
     //  System.out.println("resourceRecordId"+resources.getRecordID()+resources.getId());
    //   System.out.println("resourceId"+resources.getId());
       if(id>1)
        {
            try {
                Set<DataProvider> relavantDataProviders=dataResourceRepository.findAllRelavantDataProviders(id);
                log.debug("Request to get Resources Partners id: {} count: {}", id, relavantDataProviders.size());
                resources.setRelavantDataProviders(relavantDataProviders);
            }
            catch (JpaObjectRetrievalFailureException e)
            {
                log.error("ID: " + id, e);
            }
        }
        return resources;
    }


    public List<DataResources> saveList(List<DataResources> resources) {
        // TODO Auto-generated method stub
        List<DataResources>list=new ArrayList<DataResources>();
        for(DataResources resourceList:resources)
        {
            DataResources resource=dataResourceRepository.save(resourceList);
            list.add(resource);
        }
        return list;
    }

	public void deleteResourceProviders(Long providerId, Long resourceId) {
		// TODO Auto-generated method stub
		dataResourceRepository.deleteResourceProviders(providerId,resourceId);
	}



}
