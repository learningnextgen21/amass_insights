package com.ai.mp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ai.mp.domain.Products;

@SuppressWarnings("unused")
@Repository
public interface ProductRepository extends JpaRepository<Products, Long>{

	Products findByProductNameEquals(String subscription);

}
