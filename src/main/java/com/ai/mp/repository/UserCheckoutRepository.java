package com.ai.mp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ai.mp.domain.UserCheckout;

@Repository
public interface UserCheckoutRepository extends JpaRepository<UserCheckout, Long>{
    @Query(value="select * from user_checkout where user_id=?1 order by id desc limit 1",nativeQuery=true)
    UserCheckout finduser(@Param("id") Long id);
}
