package com.ai.mp.repository;

import com.ai.mp.domain.LookupCode;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the LookupCode entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LookupCodeRepository extends JpaRepository<LookupCode, Long> {

	List<LookupCode> findBylookupmoduleEquals(String lookupmodule);
	
	@Query(value="select * from mp_lookup_code where matching_id=?1",nativeQuery=true)
	List<LookupCode> getGeographiesValue(Long lookupcode);
	
	@Query(value="SELECT * FROM datamarketplace.mp_lookup_code where lookupcode= ?1 and lookupmodule='ASSET_CLASS'",nativeQuery=true)
	LookupCode findByLookupcode(String value);

	LookupCode findById(Long id);

}
