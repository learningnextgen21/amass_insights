package com.ai.mp.repository;

import com.ai.mp.domain.DataFeature;
import com.ai.mp.domain.DataIndustry;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the DataFeature entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataFeatureRepository extends JpaRepository<DataFeature, Long> , JpaSpecificationExecutor<DataFeature>{
    @Query("select distinct data_feature from DataFeature data_feature left join fetch data_feature.categories")
    List<DataFeature> findAllWithEagerRelationships();

    @Query("select data_feature from DataFeature data_feature left join fetch data_feature.categories where data_feature.id =:id")
    DataFeature findOneWithEagerRelationships(@Param("id") Long id);

    @Query(value="select * from data_feature where id=?1",nativeQuery=true)
	DataFeature finById(Long id);

}
