package com.ai.mp.repository;

import com.ai.mp.domain.Authority;
import com.ai.mp.domain.DataCategory;
import com.ai.mp.domain.User;
import org.jooq.types.Unsigned;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByActivationKey(String activationKey);

    Optional<User> findOneByConfirmationKey(String confirmationKey);

    List<User> findAllByActivatedIsFalseAndCreatedDateBefore(Instant dateTime);

    List<User> findByActivatedIsTrue();
    //List<User> findByActivated( String pastDays,Boolean test);
    Optional<User> findOneByResetKey(String resetKey);

    Optional<User> findOneByEmail(String email);

    Optional<User> findOneByLogin(String login);

    @EntityGraph(attributePaths = "authorities")
    User findOneWithAuthoritiesById(Long id);

    User findUserByLogin(String login);
    User findByResetKey(String resetKey);

    User findUserByEmailEqualsAndActivatedIsTrue(String login);
    User findUserByLoginEqualsAndActivatedIsTrue(String login);

    @EntityGraph(attributePaths = {"authorities", "categories"})
    Optional<User> findOneWithAuthoritiesByLogin(String login);

    @EntityGraph(attributePaths = "categories")
    Page<User> findAllByLoginNot(Pageable pageable, String login);

    List<User> findAllByAuthoritiesEqualsAndActivatedIsTrue(Authority authority);

    User findOneByIdEquals(long id);

	List<DataCategory> findCategorynameById(Long id);
    User findByLoginAndPassword(String login,String password);
    User findByLogin(String login);

    User findByActivationKey(String key);

    @Query(value="SELECT * FROM jhi_user WHERE created_date < now() and confirmation_key IS NOT NULL and NOT sent;",nativeQuery=true)
    List<User> findRemainderEmail();

    User findByEmail(String email);

    @Query("select distinct jhi_user from User jhi_user left join fetch jhi_user.authorities where jhi_user.id =:id")
    User findUser(@Param("id") Long id);
    
    @Query("select distinct jhi_user from User jhi_user left join fetch jhi_user.categories left join fetch jhi_user.assetClasses left join fetch jhi_user.sectors left join fetch jhi_user.geographies left join fetch jhi_user.authorities where jhi_user.id =:id")
    User findUserData(@Param("id") Long id);
    
    @Modifying
    @Transactional
    @Query(value="insert into jhi_user_authority values(?1,?2)",nativeQuery=true)
	void updateRole(Long id,String subscription);

    @Query(value="SELECT * FROM jhi_user WHERE last_modified_date <= now() - INTERVAL 3 DAY and user_onboarding =true and mail_sent = false",nativeQuery=true)
	List<User> findMyMatchesProvider();

    @Query("select distinct jhi_user from User jhi_user left join fetch jhi_user.categories left join fetch jhi_user.assetClasses left join fetch jhi_user.sectors left join fetch jhi_user.geographies left join fetch jhi_user.authorities")
    List<User> findAllUserData();
}
