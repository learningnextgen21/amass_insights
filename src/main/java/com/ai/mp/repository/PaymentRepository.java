package com.ai.mp.repository;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import com.ai.mp.domain.Payment;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("unused")
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

    @Query("select payment from Payment payment where payment.user.login = ?#{principal.username}")
    List<Payment> findByUserIsCurrentUser();

	Optional<Payment> findById(Long id);

	void deleteById(Long id);

	List<Payment> findByUser(Long id);

}