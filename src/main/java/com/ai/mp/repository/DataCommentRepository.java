package com.ai.mp.repository;

import com.ai.mp.domain.DataComment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataCommentRepository extends JpaRepository<DataComment, Long>, JpaSpecificationExecutor<DataComment> {

    @Query(value = "SELECT dpc.comment_type,dpc.data_providers_id,dpc.data_providers_record_id,dc.* FROM data_provider_comments dpc INNER JOIN data_comments dc ON dpc.comment_id=dc.id WHERE (dc.by_user_id=?2 AND dpc.data_providers_id=?1 AND dpc.comment_type=578) OR (dc.by_user_id!=?2 AND dpc.data_providers_id=?1 AND dpc.comment_type=578 AND dc.privacy='Public')ORDER BY dc.created_date DESC;", nativeQuery = true)
    List<DataComment> getCommentByRecordID(Long providerID,Long id);

    @Query(value = "SELECT dpc.comment_type,dpc.data_providers_id,dpc.data_providers_record_id,dc.* FROM data_provider_comments dpc INNER JOIN data_comments dc ON dpc.comment_id=dc.id WHERE dpc.data_providers_id=?1 AND dpc.comment_type=578 AND dc.privacy='Public' ORDER BY dc.created_date DESC;", nativeQuery = true)
    List<DataComment> getPublic(Long providerID);

    @Query(value = "SELECT dpc.comment_type,dpc.data_providers_id,dpc.data_providers_record_id,dc.* FROM data_provider_comments dpc INNER JOIN data_comments dc ON dpc.comment_id=dc.id WHERE dc.by_user_id=?2 AND dpc.data_providers_id=?1 AND dpc.comment_type=578 AND dc.privacy='Private' ORDER BY dc.created_date DESC;", nativeQuery = true)
    List<DataComment> getPrivate(Long providerID,Long userId);
}
