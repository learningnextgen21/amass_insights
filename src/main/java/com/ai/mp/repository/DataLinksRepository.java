package com.ai.mp.repository;

import com.ai.mp.domain.DataFeature;
import com.ai.mp.domain.DataLinks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the DataFeature entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataLinksRepository extends JpaRepository<DataLinks, Long> {

//    @Query("select data_feature from DataLinks data_feature left join fetch data_links.categories where data_feature.id =:id")
//    DataLinks findOneWithEagerRelationships(@Param("id") Long id);

}
