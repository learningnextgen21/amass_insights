package com.ai.mp.repository;

import com.ai.mp.domain.DocumentsMetadata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
@Repository
public interface DocumentsMetadataRepository extends JpaRepository<DocumentsMetadata, Long> {
    DocumentsMetadata findByFileIDEquals(String fileID);
}
