package com.ai.mp.repository;

import com.ai.mp.domain.DataFeature;
import com.ai.mp.domain.DataSource;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DataSource entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataSourceRepository extends JpaRepository<DataSource, Long>, JpaSpecificationExecutor<DataSource> {

    @Query(value="select * from data_source where id=?1",nativeQuery=true)
	DataSource finById(Long id);

}
