package com.ai.mp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ai.mp.domain.IpAddress;

@SuppressWarnings("unused")
@Repository
public interface IpAddressRepository extends JpaRepository<IpAddress, Long>{

	IpAddress findByIpAddressEquals(String hostAddress);

    @Query(value="SELECT * FROM datamarketplace.ip_address WHERE web_updated_date < NOW() - INTERVAL 24 HOUR and ip_address= ?1",nativeQuery=true)
    IpAddress findIpAddressGreater(String ipAddressCheck);

}
