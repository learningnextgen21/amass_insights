package com.ai.mp.repository;

import com.ai.mp.domain.DataCategory;
import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.DataProviderTag;
import com.ai.mp.domain.User;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;


/**
 * Spring Data JPA repository for the DataProviderTag entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface DataProviderTagRepository extends JpaRepository<DataProviderTag, Long>, JpaSpecificationExecutor<DataProviderTag>{

    @Modifying
    @Query(value = "INSERT INTO data_provider_provider_tag(provider_tags_id, data_providers_id) VALUES(?1, ?2)", nativeQuery = true)
    int insetProviderTag(long tagId, long providerId);

   /* @Modifying
    @Query(value = "UPDATE data_provider SET tag = (select cast(concat('[', g`at('{\"id\":\"', dt.id, '\", \"desc\":\"', dt.provider_tag, '\"}'), ']') as json) tag from data_provider_tag dt inner join data_provider_provider_tag dpt on dt.id = dpt.provider_tags_id where dpt.data_providers_id = data_provider.id);", nativeQuery = true)
    int updateProvider();*/


    @Query(value = "SELECT * FROM datamarketplace.data_provider_tag dpt where dpt.by_user_id=?1 ORDER BY dpt.created_date ASC;", nativeQuery = true)
    List<DataProviderTag> getAllTagsByUserId(Long id);

    @Query(value = "SELECT * FROM datamarketplace.data_provider_tag  where by_user_id=?1 and provider_tag=?2", nativeQuery = true)
    DataProviderTag getTagsByUserId(Long id,String value);
    
    @Query("select distinct data_provider_tag from DataProviderTag data_provider_tag left join fetch data_provider_tag.providerTags where data_provider_tag.id =:id")
    DataProviderTag getTagProvider(@Param("id") Long id);

    DataProviderTag findOneById(Long id);

    @Modifying
    @Query(value = "DELETE FROM data_provider_provider_tag where provider_tags_id=?1 and data_providers_id=?2", nativeQuery = true)
    int deleteProviderTag(long tagId,long providerId);
    
    @Query(value = "SELECT * FROM datamarketplace.jhi_user", nativeQuery = true)
	List<User> getUserValue();

    @Query(value = "select * from data_provider_tag where provider_tag='My Matches'and by_user_id=?1", nativeQuery = true)
	DataProviderTag findUserValue(Long id);
    
    @Modifying
    @Query(value = "INSERT INTO datamarketplace.data_provider_tag(record_id, provider_tag, explanation, inactive, created_date, updated_date, sort_order, privacy, receive_notifications, by_user_id) VALUES(?1, 'My Matches', '', False, NOW(), NOW(), 0, 'Private', False, ?2)", nativeQuery = true)
    @Transactional
    int userUpdateTag(String record,Long user);

    @Modifying
    @Query(value = "DELETE FROM data_provider_tag where by_user_id=?1 and privacy='Private'", nativeQuery = true)
	void deleteByUserId(Long user);

    @Query(value = "select * from data_provider_tag where (provider_tag=?1 and type='Data Category') or (provider_tag=?2 and type='Industry') or (provider_tag=?3 and type='Asset Class') or (provider_tag=?4 and type='Data Update Frequency')", nativeQuery = true)
	Set<DataProviderTag> findProviderTag(String mainDataCategory,String mainDataIndustry,String mainAssset,String updateFrequency);
    @Query(value = "select * from data_provider_tag where (provider_tag=?1 and type=?2)", nativeQuery = true)
     DataProviderTag findProviderTagName(String name,String type);
}
