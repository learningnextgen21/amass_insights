package com.ai.mp.repository;

import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.User;
import com.ai.mp.domain.UserProvider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Spring Data JPA repository for the UserProvider entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserProviderRepository extends JpaRepository<UserProvider, Long> {
 UserProvider findOneByUserIDEqualsAndProviderIDEqualsAndInterestTypeEquals(long userID, long providerID, int interestType);

 @Query("select up.providerID from UserProvider up where up.userID = :userID and up.interestType = :interestType and up.inactive = false")
 List<Long> findAllByUserIDEqualsAndInterestTypeEquals(@Param("userID") Long userID, @Param("interestType") int interestType);

    @Query("select up from UserProvider up where up.userID = :userID and up.interestType = :interestType")
    List<UserProvider> findAllByUserIDsEqualsAndInterestTypeEquals(@Param("userID") Long userID, @Param("interestType") int interestType);

    @Query("select up.provider from UserProvider up where up.userID = :userID and up.interestType = :interestType and up.inactive = false")
    List<DataProvider> findAllProvidersByUserIDEqualsAndInterestTypeEquals(@Param("userID") Long userID, @Param("interestType") int interestType);

    @Query("select up.providerID from UserProvider up where up.interestType = :interestType")
    List<Long> findAllByInterestTypeEquals(@Param("interestType") int interestType);

    UserProvider findOneByDeletionKeyEquals(String key);

    List<UserProvider> findAllByUserIDEqualsAndInactiveIsFalse(long userID);

    @Query("select up.userID from UserProvider up where up.providerID = :providerID and up.interestType = :interestType")
    List<Long> findAllByInterestTypeEqualsProviderId(@Param("providerID") Long providerID,@Param("interestType") int interestType); 
    
    @Modifying
    @Query(value = "INSERT INTO datamarketplace.data_provider_tag(record_id, provider_tag, explanation, inactive, created_date, updated_date, sort_order, privacy, receive_notifications, by_user_id) VALUES(?1, 'Interested', '', False, NOW(), NOW(), 0, 'Private', False, ?8),(?2, 'Not Interested', '', False, NOW(), NOW(), 0, 'Private', False, ?8),(?3, 'Reviewed', '', False, NOW(), NOW(), 0, 'Private', False, ?8),(?4, 'In Touch With', '', False, NOW(), NOW(), 0, 'Private', False, ?8),(?5, 'Follow Up', '', False, NOW(), NOW(), 0, 'Private', False, ?8),(?6, 'Testing', '', False, NOW(), NOW(), 0, 'Private', False, ?8),(?7, 'In Use', '', False, NOW(), NOW(), 0, 'Private', False, ?8);", nativeQuery = true)
    @Transactional
    int insetTagUser(String record1, String record2,String record3,String record4,String record5,String record6,String record7,Long id);

    UserProvider findOneByUserIDEqualsAndProviderID(Long id, Long id2);
    UserProvider findOneByUserIDEqualsAndInterestType(Long id, Long id2);

    UserProvider findOneByUserIDEqualsAndProviderIDEqualsAndInterestType(Long id, Long id2, int operatorType);
    
    @Query(value = "SELECT * FROM mp_user_provider where user_id=?1 and provider_id=?2 and interest_type=?3", nativeQuery = true)
	UserProvider getMyMatchedValue(Long userId, Long providerId, int interestTypeUnlock);
    
    @Query("select up from UserProvider up where up.providerID = :providerID and up.interestType = :interestType and up.inactive = false")
    List<UserProvider> findAllProvidersByUserID(@Param("providerID") Long providerID, @Param("interestType") int interestType);
    @Modifying
    @Query(value = "Delete FROM mp_user_provider where user_id=?1 and interest_type=?2", nativeQuery = true)
    @Transactional
    void deleteProvider(Long id, int interestTypeOperator);

    @Modifying
    @Query(value = "Delete FROM mp_user_provider where provider_id=?1 and interest_type=?2", nativeQuery = true)
    @Transactional
    void deleteUserProvider(Long id, int interestTypeOperator);
}
