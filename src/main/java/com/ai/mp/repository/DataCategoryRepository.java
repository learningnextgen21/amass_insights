package com.ai.mp.repository;

import com.ai.mp.domain.DataCategory;
import com.ai.mp.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the DataCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataCategoryRepository extends JpaRepository<DataCategory, Long> , JpaSpecificationExecutor<DataCategory>{

	List<DataCategory> findByCategoryUsersEqualsOrUserPermissionIn(User user, List<String> roles);

    List<DataCategory> findByCategoryUsersEquals(User user);

	List<DataCategory> findCategorynameById(Long id);

	 DataCategory findByRecordIDEqualsAndLogoIsNull(String recordID);

	 @Query("select distinct data_category from DataCategory data_category left join fetch data_category.logo")
	 List<DataCategory> findAllWithEagerRelationships();
	 
	 @Query(value="select * from data_category where id=?1",nativeQuery=true)
	DataCategory finByIDEquals(Long id);

	//DataCategory findOneWithEagerRelationships(Long id);

	//DataCategory findOne(String recordID);

	/* @Query("select distinct data_provider from DataProvider data_provider left join fetch data_provider.geographicalFoci left join fetch data_provider.investorTypes left join fetch data_provider.deliveryFrequencies left join fetch data_provider.logo left join fetch data_provider.mainDataCategory left join fetch data_provider.ownerOrganization left join fetch data_provider.mainDataIndustry left join fetch data_provider.providerType left join fetch data_provider.documentsMetadata where data_provider.id =:id")
	 DataCategory findOneWithEagerRelationships(@Param("id") Long id);*/

}
