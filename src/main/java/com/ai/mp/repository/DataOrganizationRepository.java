package com.ai.mp.repository;

import com.ai.mp.domain.DataOrganization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;


/**
 * Spring Data JPA repository for the DataOrganization entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataOrganizationRepository extends JpaRepository<DataOrganization, Long>, JpaSpecificationExecutor<DataOrganization> {

    @Query("select distinct data_organization.city from DataOrganization data_organization order by city")
    Set<String> getDistinctCities();

    @Query("select data_organization.id from DataOrganization data_organization")
	List<Long> getAllIDs();

    @Query("select data_organization from DataOrganization data_organization left join fetch data_organization.subsidiaryOrganization left join fetch data_organization.researchMethodsCompleted left join fetch  data_organization.headcountBucket left join fetch data_organization.marketplaceStatus left join fetch data_organization.ieiStatus where data_organization.id =:id")
	DataOrganization findOneWithEagerRelationships(@Param("id")Long id);

}
