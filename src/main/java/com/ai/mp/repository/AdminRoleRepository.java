package com.ai.mp.repository;

import com.ai.mp.domain.AdminRole;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AdminRole entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdminRoleRepository extends JpaRepository<AdminRole, Long> {

}
