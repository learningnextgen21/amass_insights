package com.ai.mp.repository;

import com.ai.mp.domain.EmailAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
@Transactional
public interface EmailAddressRepository extends JpaRepository<EmailAddress, Long>, JpaSpecificationExecutor<EmailAddress> {

    EmailAddress findByEmailAddress(String email);

    EmailAddress findOneByconfirmationkeyEquals(String confirmationkey);
    EmailAddress findEmailAddressByEmailAddressEqualsAndActiveIsTrue(String emailAddress);

    List<EmailAddress> findByActiveIsTrue();

    @Query(value = "SELECT DISTINCT * FROM data_email_address WHERE (subscribed_to_alt_data_newsletter IS TRUE OR date_confirmed_subscription) AND (exclude_from_newsletter IS FALSE) ORDER BY email_address DESC;", nativeQuery = true)
    List<EmailAddress> findEmail();

	EmailAddress findByUser(Long id);
	
    @Query(value = "SELECT * FROM datamarketplace.data_email_address where data_provider=?1", nativeQuery = true)
	Set<EmailAddress> findByEmailAdrressProviderId(Long id);
}
