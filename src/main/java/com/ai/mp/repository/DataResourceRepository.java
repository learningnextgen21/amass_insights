package com.ai.mp.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ai.mp.domain.DataProvider;
import com.ai.mp.domain.DataResources;

@SuppressWarnings("unused")
@Repository
public interface DataResourceRepository extends JpaRepository<DataResources, Long> {

    @Query("select distinct data_resource from DataResources data_resource left join fetch data_resource.authorDataProvider where data_resource.id =:id")
    DataResources findOneWithEagerRelationships(@Param("id") Long id);

    @Query("select  data_resource.relavantDataProviders from DataResources data_resource where data_resource.id = :id")
    Set<DataProvider> findAllRelavantDataProviders(@Param("id") Long id);

    @Query("select data_resource.id from DataResources data_resource order by data_resource.id")
    List<Long> getAllIDs();

    @Modifying
    @Transactional
    @Query(value="delete from data_resources_data_providers where provider_id=?1 and resource_id=?2",nativeQuery=true)
	void deleteResourceProviders(Long providerId, Long resourceId);

}
