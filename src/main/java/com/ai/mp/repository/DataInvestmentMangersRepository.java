package com.ai.mp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ai.mp.domain.DataInvestmentManager;


public interface DataInvestmentMangersRepository extends JpaRepository<DataInvestmentManager, Long> {

}
