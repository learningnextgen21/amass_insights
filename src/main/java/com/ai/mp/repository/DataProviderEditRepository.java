package com.ai.mp.repository;

import com.ai.mp.domain.DataProviderEdit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface DataProviderEditRepository extends JpaRepository<DataProviderEdit, Long>, JpaSpecificationExecutor<DataProviderEdit> {

    DataProviderEdit findByRecordID(String recordId);
	DataProviderEdit findByRecordIDEqualsAndEditorUserIdEquals(String recordID, Long id);

    @Query(value="SELECT * FROM datamarketplace.data_provider_edits where record_id=?1 and  editor_user_id= ?2 order by id desc limit 1",nativeQuery=true)
	DataProviderEdit findByrecordUpdated(String recordID, Long id);
	DataProviderEdit findById(Long id);
    @Query(value="SELECT * FROM datamarketplace.data_provider_edits where record_id=?1 order by id desc limit 1",nativeQuery=true)
	DataProviderEdit findByrecordUpdated(String recordId);

    @Query(value="SELECT * FROM datamarketplace.data_provider_edits where record_id= ?1 and record_updated=true order by id desc limit 1",nativeQuery=true)
   	DataProviderEdit findByrecordUpdatedValues(String recordId);
}
