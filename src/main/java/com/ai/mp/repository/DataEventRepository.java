package com.ai.mp.repository;

import java.util.Date;
import java.util.List;

import com.ai.mp.domain.DataEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface DataEventRepository extends JpaRepository<DataEvent, Long>, JpaSpecificationExecutor<DataEvent> {
    @Query("select data_event.id from DataEvent data_event")
    List<Long> getAllIDs();

    @Query("select data_event from DataEvent data_event left join fetch data_event.organizationOrganizer left join fetch data_event.sponsorOrganization left join fetch data_event.providerOrganizer left join fetch data_event.sponsorProvider where data_event.id =:id")
    DataEvent findOneWithEagerRelationships(@Param("id") Long id);

    @Query(value="select det.*,de.* FROM data_events_topic det INNER JOIN data_event de ON det.data_events_id=de.id where det.topics_id=2 AND de.created_time BETWEEN NOW() - INTERVAL ?1 DAY AND NOW() ORDER BY de.created_time ASC;",nativeQuery = true)
    List<DataEvent> getAllByEventTopicsId(int countOfDays);

    @Query(value="select det.topics_id,det.data_events_id,de.* FROM data_events_topic det INNER JOIN data_event de ON det.data_events_id=de.id where det.topics_id=2 AND de.start_time BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY) ORDER BY de.start_time ASC;",nativeQuery = true)
    List<DataEvent> getAllByEventTopicsIdAndLastupdatedDate();

    @Query(value="select det.*,de.* FROM data_events_topic det INNER JOIN data_event de ON det.data_events_id=de.id where det.topics_id=2 AND NOW()- INTERVAL 1 DAY <= de.start_time;",nativeQuery = true)
    List<DataEvent> getAllByEventTopicsIdAndAfterNow();
}
