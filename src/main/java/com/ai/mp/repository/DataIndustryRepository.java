package com.ai.mp.repository;

import com.ai.mp.domain.DataCategory;
import com.ai.mp.domain.DataIndustry;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DataIndustry entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataIndustryRepository extends JpaRepository<DataIndustry, Long>, JpaSpecificationExecutor<DataIndustry> {

	 @Query(value="select * from data_industry where id=?1",nativeQuery=true)
	DataIndustry finById(Long id);

}
