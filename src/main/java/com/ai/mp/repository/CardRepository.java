package com.ai.mp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ai.mp.domain.CardDetails;

public interface CardRepository extends JpaRepository<CardDetails, Long> {

    @Query(value="SELECT * FROM datamarketplace.card_details where user_id=?1",nativeQuery=true)
	CardDetails findByUser(Long id);
    
    @Query(value="SELECT * FROM datamarketplace.card_details where user_id=?1",nativeQuery=true)
	List<CardDetails> findByUserList(Long id);

}
