package com.ai.mp.repository;

import com.ai.mp.domain.DataTopic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the ClientRole entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataTopicRepository extends JpaRepository<DataTopic, Long> {

}
