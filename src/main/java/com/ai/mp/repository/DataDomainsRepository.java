package com.ai.mp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ai.mp.domain.DataResources;
import com.ai.mp.domain.Domains;


@Repository
public interface DataDomainsRepository extends JpaRepository<Domains, Long> {

    @Query("select distinct data_domains from Domains data_domains left join fetch data_domains.ownerDataProvider left join fetch data_domains.ownerInvestmentManager left join fetch data_domains.EmailAddresses where data_domains.domain =:emailName")
	Domains findByDomain(@Param("emailName") String emailName);

    @Query("select data_domains.id from Domains data_domains order by data_domains.id")
	List<Long> getAllIDs();

    @Query("select distinct data_domains from Domains data_domains left join fetch data_domains.ownerDataProvider left join fetch data_domains.ownerInvestmentManager left join fetch data_domains.EmailAddresses where data_domains.id =:id")
	Domains findOneByDomain(@Param("id") Long id);

	Domains findById(Long id);

}
