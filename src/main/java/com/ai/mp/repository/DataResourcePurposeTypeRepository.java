package com.ai.mp.repository;

import com.ai.mp.domain.DataResourcePurposeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the DataCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataResourcePurposeTypeRepository extends JpaRepository<DataResourcePurposeType, Long> {
}
