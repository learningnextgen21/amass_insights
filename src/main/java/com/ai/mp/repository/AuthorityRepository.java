package com.ai.mp.repository;

import com.ai.mp.domain.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * Spring Data JPA repository for the Authority entity.
 */
@Repository
public interface AuthorityRepository extends JpaRepository<Authority, String> {
    Set<Authority> findAllByRoleLevelLessThanEqual(int roleLevel);
}
