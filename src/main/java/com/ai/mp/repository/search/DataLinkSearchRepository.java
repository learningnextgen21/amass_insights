package com.ai.mp.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ai.mp.domain.DataLinks;

public interface DataLinkSearchRepository extends ElasticsearchRepository<DataLinks, Long> {

}
