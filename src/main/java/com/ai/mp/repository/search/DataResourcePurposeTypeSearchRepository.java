package com.ai.mp.repository.search;

import com.ai.mp.domain.DataResourcePurposeType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ClientRole entity.
 */
@SuppressWarnings("unused")
public interface DataResourcePurposeTypeSearchRepository extends ElasticsearchRepository<DataResourcePurposeType, Long> {
}
