package com.ai.mp.repository.search;

import com.ai.mp.domain.LookupCode;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the LookupCode entity.
 */
public interface LookupCodeSearchRepository extends ElasticsearchRepository<LookupCode, Long> {
}
