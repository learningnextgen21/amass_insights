package com.ai.mp.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ai.mp.domain.Domains;


public interface DataDomainSearchRepository extends ElasticsearchRepository<Domains, Long>{

}
