package com.ai.mp.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ai.mp.domain.DataResources;

public interface DataResourceSearchRepository extends ElasticsearchRepository<DataResources,Long> {

}
