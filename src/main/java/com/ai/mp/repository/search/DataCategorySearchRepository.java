package com.ai.mp.repository.search;

import com.ai.mp.domain.DataCategory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Date;

/**
 * Spring Data Elasticsearch repository for the DataCategory entity.
 */
public interface DataCategorySearchRepository extends ElasticsearchRepository<DataCategory, Long> {
    Long countByCreatedDateAfter(Date date);
}
