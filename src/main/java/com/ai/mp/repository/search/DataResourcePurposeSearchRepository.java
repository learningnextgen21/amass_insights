package com.ai.mp.repository.search;

import com.ai.mp.domain.DataResourcePurpose;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ClientRole entity.
 */
@SuppressWarnings("unused")
public interface DataResourcePurposeSearchRepository extends ElasticsearchRepository<DataResourcePurpose, Long> {
}
