package com.ai.mp.repository.search;

import com.ai.mp.domain.UserDataCategory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UserDataCategory entity.
 */
public interface UserDataCategorySearchRepository extends ElasticsearchRepository<UserDataCategory, Long> {
}
