package com.ai.mp.repository.search;

import com.ai.mp.domain.DataProviderTag;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DataProviderTag entity.
 */
public interface DataProviderTagSearchRepository extends ElasticsearchRepository<DataProviderTag, Long> {
}
