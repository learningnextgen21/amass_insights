package com.ai.mp.repository.search;

import com.ai.mp.domain.DataProviderType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DataProviderType entity.
 */
public interface DataProviderTypeSearchRepository extends ElasticsearchRepository<DataProviderType, Long> {
}
