package com.ai.mp.repository.search;

import com.ai.mp.domain.DataFeature;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Date;

/**
 * Spring Data Elasticsearch repository for the DataFeature entity.
 */
public interface DataFeatureSearchRepository extends ElasticsearchRepository<DataFeature, Long> {
    Long countByCreatedDateAfter(Date date);
}
