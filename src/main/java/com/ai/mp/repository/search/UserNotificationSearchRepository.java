package com.ai.mp.repository.search;

import com.ai.mp.domain.UserNotification;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UserNotification entity.
 */
public interface UserNotificationSearchRepository extends ElasticsearchRepository<UserNotification, Long> {
}
