package com.ai.mp.repository.search;

import com.ai.mp.domain.DataArticle;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Date;
import java.util.List;

/**
 * Spring Data Elasticsearch repository for the DataArticle entity.
 */
public interface DataArticleSearchRepository extends ElasticsearchRepository<DataArticle, Long> {
    Long countByPublishedDateAfter(Date date);
    List<DataArticle> getByArticleTopicsIdEqualsAndPublishedDateGreaterThanEqualOrderByIncludeInNewsletterDesc(long topicID, Date publishedDate);
    List<DataArticle> getByRelevantProvidersIdEqualsAndPublishedDateGreaterThanEqualOrderByIncludeInNewsletterDesc(long id, Date cate);

}
