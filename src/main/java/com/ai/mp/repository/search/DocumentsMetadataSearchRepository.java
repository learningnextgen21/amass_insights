package com.ai.mp.repository.search;

import com.ai.mp.domain.DocumentsMetadata;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface DocumentsMetadataSearchRepository extends ElasticsearchRepository<DocumentsMetadata, Long> {
}
