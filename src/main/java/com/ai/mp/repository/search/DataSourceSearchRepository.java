package com.ai.mp.repository.search;

import com.ai.mp.domain.DataSource;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DataSource entity.
 */
public interface DataSourceSearchRepository extends ElasticsearchRepository<DataSource, Long> {
}
