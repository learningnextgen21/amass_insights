package com.ai.mp.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ai.mp.domain.EmailAddress;

public interface EmailAddressSearchRepository extends ElasticsearchRepository<EmailAddress, Long>{

}
