package com.ai.mp.repository.search;
import com.ai.mp.domain.DataEvent;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface DataEventSearchRepository extends ElasticsearchRepository<DataEvent, Long> {
    Long countByCreatedTimeAfter(Date date);
    Long countByLastUpdatedAfter(Date date);

}
