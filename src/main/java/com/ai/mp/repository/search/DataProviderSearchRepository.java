package com.ai.mp.repository.search;

import com.ai.mp.domain.DataProvider;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Date;
import java.util.List;

/**
 * Spring Data Elasticsearch repository for the DataProvider entity.
 */
public interface DataProviderSearchRepository extends ElasticsearchRepository<DataProvider, Long> {
    Long countByCreatedDateAfter(Date date);
    Long countByUpdatedDateAfter(Date date);
    List<DataProvider> getByUpdatedDateAfterAndIdIn(Date date, List<Long> providerIDs);
    List<DataProvider> getByIdIn(List<Long> providerIDs);
    DataProvider findDataProviderByidEquals(long id);
}
