package com.ai.mp.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ai.mp.domain.DataInvestmentManager;


public interface DataInvestmentMangerSearchRepository extends ElasticsearchRepository<DataInvestmentManager, Long> {

}
