package com.ai.mp.repository.search;

import com.ai.mp.domain.AdminRole;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AdminRole entity.
 */
public interface AdminRoleSearchRepository extends ElasticsearchRepository<AdminRole, Long> {
}
