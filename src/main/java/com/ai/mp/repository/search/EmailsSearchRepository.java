package com.ai.mp.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ai.mp.domain.Emails;

public interface EmailsSearchRepository  extends ElasticsearchRepository<Emails, Long>{

}
