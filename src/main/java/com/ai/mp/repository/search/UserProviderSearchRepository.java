package com.ai.mp.repository.search;

import com.ai.mp.domain.UserProvider;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UserProvider entity.
 */
public interface UserProviderSearchRepository extends ElasticsearchRepository<UserProvider, Long> {
}
