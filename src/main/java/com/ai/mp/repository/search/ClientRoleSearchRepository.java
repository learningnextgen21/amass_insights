package com.ai.mp.repository.search;

import com.ai.mp.domain.ClientRole;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ClientRole entity.
 */
public interface ClientRoleSearchRepository extends ElasticsearchRepository<ClientRole, Long> {
}
