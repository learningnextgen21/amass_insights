package com.ai.mp.repository.search;

import com.ai.mp.domain.DataOrganization;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DataOrganization entity.
 */
public interface DataOrganizationSearchRepository extends ElasticsearchRepository<DataOrganization, Long> {
}
