package com.ai.mp.repository.search;

import com.ai.mp.domain.DataIndustry;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DataIndustry entity.
 */
public interface DataIndustrySearchRepository extends ElasticsearchRepository<DataIndustry, Long> {
}
