package com.ai.mp.repository;

import com.ai.mp.domain.ClientRole;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ClientRole entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientRoleRepository extends JpaRepository<ClientRole, Long> {

}
