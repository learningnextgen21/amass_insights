package com.ai.mp.repository;

import com.ai.mp.domain.ExternalClient;
import com.ai.mp.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ExternalClientRepository extends JpaRepository<ExternalClient, Long> {

ExternalClient findById(Long id);
}
