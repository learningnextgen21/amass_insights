package com.ai.mp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ai.mp.domain.Search;


@Repository
public interface SearchRepository extends JpaRepository<Search, Long> {

	
}
