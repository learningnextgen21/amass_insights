package com.ai.mp.repository;

import com.ai.mp.domain.DataProviderType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DataProviderType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataProviderTypeRepository extends JpaRepository<DataProviderType, Long> {

}
