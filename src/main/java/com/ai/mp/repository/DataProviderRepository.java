package com.ai.mp.repository;

import com.ai.mp.domain.DataArticle;
import com.ai.mp.domain.DataProvider;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Spring Data JPA repository for the DataProvider entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataProviderRepository extends JpaRepository<DataProvider, Long>, JpaSpecificationExecutor<DataProvider> {
    @Query("select distinct data_provider from DataProvider data_provider left join fetch data_provider.geographicalFoci left join fetch data_provider.relevantArticles left join fetch data_provider.investorTypes left join fetch data_provider.deliveryFrequencies")
    List<DataProvider> findAllWithEagerRelationships();

    @Query("select distinct data_provider from DataProvider data_provider left join fetch data_provider.geographicalFoci left join fetch data_provider.investorTypes left join fetch data_provider.deliveryFrequencies  left join fetch data_provider.logo left join fetch data_provider.mainDataCategory left join fetch data_provider.ownerOrganization left join fetch data_provider.mainDataIndustry left join fetch data_provider.providerType  where data_provider.id =:id")
    DataProvider findOneWithEagerRelationships(@Param("id") Long id);
    @Query("select distinct data_provider from DataProvider data_provider left join fetch data_provider.geographicalFoci left join fetch data_provider.investorTypes left join fetch data_provider.deliveryFrequencies left join fetch data_provider.dataProvidersDeliveryMethods left join fetch data_provider.dataProvidersPricingModels left join fetch data_provider.dataProviderTags left join fetch data_provider.logo left join fetch data_provider.mainDataCategory left join fetch data_provider.ownerOrganization left join fetch data_provider.mainDataIndustry left join fetch data_provider.providerType left join fetch data_provider.dataProvidersAssetClasses left join fetch data_provider.dataProvidersRelevantSectors left join fetch data_provider.dataProvidersDeliveryFormats left join fetch data_provider.dataProvidersSecurityTypes left join fetch data_provider.providerIndustries left join fetch data_provider.dataProviderCategories left join fetch data_provider.dataProviderSources left join fetch data_provider.dataProviderFeatures  where data_provider.id =:id")
    DataProvider findOneWithEagerRelationshipsTag(@Param("id") Long id);


    @Query("select distinct data_provider.providerPartners from DataProvider data_provider inner join data_provider.providerPartners p where data_provider.id = :id")
    Set<DataProvider> findAllProviderPartners(@Param("id") Long id);


    @Query("select distinct data_provider.distributionPartners from DataProvider data_provider inner join data_provider.distributionPartners p where data_provider.id = :id")
    Set<DataProvider> findAllDistributionPartners(@Param("id") Long id);


    @Query("select distinct data_provider.consumerPartners from DataProvider data_provider inner join data_provider.consumerPartners p where data_provider.id = :id")
    Set<DataProvider> findAllConsumerPartners(@Param("id") Long id);

    @Query("select distinct data_provider.competitors from DataProvider data_provider inner join data_provider.competitors p where data_provider.id = :id")
    Set<DataProvider> findAllCompetiors(@Param("id")Long id);
    
    @Query("select distinct data_provider.complementaryProviders from DataProvider data_provider inner join data_provider.complementaryProviders p where data_provider.id = :id")
	Set<DataProvider> findAllComplementaryProviders(@Param("id")Long id);
    
    @Query("select data_provider.id from DataProvider data_provider order by data_provider.id")
    List<Long> getAllIDs();

    DataProvider findByRecordIDEqualsAndLogoIsNull(String recordID);

    DataProvider findByRecordIDEquals(String recordID);
    DataProvider findByRecordID(String recordID);

    Set<DataProvider> findByLogoIsNullAndWebsiteIsNotNull();

    @Query("select data_provider.relevantArticles from DataProvider data_provider where data_provider.id = :id")
    Set<DataArticle> getRelevantArticlesForProvider(@Param("id") long id);

    @Query("select DISTINCT COUNT(d.dataUpdateFrequency),d.dataUpdateFrequency  from DataProvider d   GROUP BY d.dataUpdateFrequency")
    Set<DataProvider> getDataUpdateFrequency();
    //select DISTINCT COUNT(d.data_update_frequency),d.data_update_frequency  from data_provider d where d.data_update_frequency IS NOT NULL and d.data_update_frequency !='' GROUP BY d.data_update_frequency;
    @Query(value="SELECT * FROM data_provider where score_overall_id=283 or score_overall_id=284 ORDER BY RAND(WEEK(NOW())) LIMIT 1",nativeQuery=true)
	DataProvider getProvider();


    @Modifying
    @Transactional
    @Query(value="insert into data_provider_partner (data_providers_id,partners_id,partner_type) values (?1, ?2, 18)",nativeQuery=true)
    void insertPartner(Long id, Long id2);

    @Modifying
    @Transactional
    @Query(value="insert into data_provider_partner (data_providers_id,partners_id,partner_type) values (?1, ?2, ?3)",nativeQuery=true)
    void updatePartner(Long id, Long id2, Long distributionPartner);

    @Modifying
    @Transactional
    @Query(value="delete from data_provider_partner where data_providers_id=?1 and partner_type=?2",nativeQuery=true)
    void deletePartner(Long id,Long partnerId);

	DataProvider findById(Long providerID);
	
    @Query(value="select count(*) FROM data_resources_data_providers where provider_id=?1",nativeQuery=true)
	Long findResourceCount(Long id);
    
    @Query(value="select count(*) FROM data_provider_articles where data_providers_id=?1",nativeQuery=true)
	Long findRelavantArticleCount(Long id);


    @Query(value="select count(*) FROM data_provider_comments where data_providers_id=?1",nativeQuery=true)
	Long findCommentsCount(Long id);

	List<DataProvider> findByTagAdded(boolean b);
	
	@Transactional
	@Modifying
	@Query(value="UPDATE data_provider_tag SET tag_providers = (select cast(concat('[', group_concat('{\"id\":\"', l.id, '\"}'), ']') as json) tag_providers\r\n" + 
			"from data_provider l inner join data_provider_provider_tag ac on l.id = ac.data_providers_id where ac.provider_tags_id = ?1)",nativeQuery=true)
	void updateTag(Long id);

	@Transactional
	@Modifying
	@Query(value="UPDATE data_provider SET tag = (select cast(concat('[', group_concat('{\"id\":\"', dt.id, '\", \"desc\":\"', dt.provider_tag, '\"}'), ']') as json) tag\r\n" + 
			"from data_provider_tag dt inner join data_provider_provider_tag dpt on dt.id = dpt.provider_tags_id where dpt.data_providers_id = ?1)",nativeQuery=true)
	void updateTagProvider(Long id);
	
	  @Query("select distinct data_provider from DataProvider data_provider left join fetch data_provider.geographicalFoci left join fetch data_provider.relevantArticles left join fetch data_provider.investorTypes left join fetch data_provider.deliveryFrequencies left join fetch data_provider.mainDataCategory left join fetch data_provider.mainDataIndustry left join fetch data_provider.ownerOrganization left join fetch data_provider.providerType where data_provider.recordID =:recordId")
	    DataProvider findOneIdWithEagerRelationships(@Param("recordId") String recordId);
}

