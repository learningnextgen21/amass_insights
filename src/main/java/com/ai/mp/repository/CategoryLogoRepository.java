
package com.ai.mp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ai.mp.domain.CategoryLogo;

@SuppressWarnings("unused")
@Repository
public interface CategoryLogoRepository extends JpaRepository<CategoryLogo, Long> {
    CategoryLogo findByCategoryRecordID(String categoryId);

}
