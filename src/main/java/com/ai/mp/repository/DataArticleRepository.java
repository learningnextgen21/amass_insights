package com.ai.mp.repository;

import com.ai.mp.domain.DataArticle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Spring Data JPA repository for the DataArticle entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataArticleRepository extends JpaRepository<DataArticle, Long>, JpaSpecificationExecutor<DataArticle> {

    @Query("select data_article from DataArticle data_article left join fetch data_article.relevantProviders where data_article.id =:id")
    DataArticle findOne(@Param("id") Long id);

    Set<DataArticle> findAllByRelevantProvidersRecordIDEquals(String recordID);

    Set<DataArticle> findAllByAuthorDataProviderIdEquals(long dataProviderID);

    @Query("select data_article from DataArticle data_article left join fetch data_article.relevantProviders  left join fetch data_article.articleTopics left join fetch data_article.purpose left join fetch  data_article.purposeType where data_article.id =:id")
    DataArticle findOneWithEagerRelationships(@Param("id") Long id);

    @Query("select data_article.id from DataArticle data_article")
    List<Long> getAllIDs();

    DataArticle findByRecordID(String key);

    @Query(value="select * from data_article where mail_sent=false and id in(select articles_id from data_article_topic t inner join data_topic t1 on t.topics_id=(select id from data_topic where topic_name='Alternative Data in Investing' or topic_name='Investment Research' or topic_name='Quantitative Investing' order by rand() limit 1)) order by published_date desc limit 1;",nativeQuery=true)
    DataArticle getSocialArticle();


    @Modifying
    @Transactional
    @Query(value="update data_article set sent=1,mail_sent=1,post_date=NOW()+ INTERVAL 1 DAY where id=?1",nativeQuery=true)
    void update(Long id);

    DataArticle findBySent(Boolean b);
    

    @Modifying
    @Transactional
    @Query(value="update data_article set sent=0,twitter=1 where id=?1",nativeQuery=true)
    void updateTwitter(Long id);

    @Modifying
    @Transactional
    @Query(value="update data_article set twitter=1 where id=?1",nativeQuery=true)
    void updateTwitterOnly(Long id);

    @Modifying
    @Transactional
    @Query(value="update data_article set linked_In=1 where id=?1",nativeQuery=true)
    void updateLinkedInSent(Long id);
    
    
    @Modifying
    @Transactional
    @Query(value="update data_article set twitter=1 where id=?1",nativeQuery=true)
    void updateTwitterCancel(Long id);

    @Modifying
    @Transactional
    @Query(value="update data_article set linked_In=1 where id=?1",nativeQuery=true)
    void updateLinkedInCancel(Long id);

    
    @Modifying
    @Transactional
    @Query(value="update data_article set sent=0 where id=?1",nativeQuery=true)
    void updateSentOnly(Long id);
    
	DataArticle findBySentEqualsAndLinkedInEquals(Boolean b, Boolean c);


}
