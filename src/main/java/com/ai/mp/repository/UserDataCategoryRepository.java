package com.ai.mp.repository;

import com.ai.mp.domain.UserDataCategory;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;


/**
 * Spring Data JPA repository for the UserDataCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserDataCategoryRepository extends JpaRepository<UserDataCategory, Long> {

	UserDataCategory findOneByUserIDEqualsAndDataCategoryIDEqualsAndInterestTypeEquals(Long userID, Long dataCategoryID, int interestType);

	UserDataCategory findOneByDeletionKeyEquals(String key);

    List <UserDataCategory> findAllByUserIDEqualsAndInactiveIsFalse(long userID);

    @Query("select udc.dataCategoryID from UserDataCategory udc where udc.userID = :userID and udc.interestType = :interestType and udc.inactive = false")
    List<Long> findAllByUserIDEqualsAndInterestTypeEquals(@Param("userID") Long userID, @Param("interestType") int interestType);


}
