package com.ai.mp.repository;

import com.ai.mp.domain.UserProvider;
import com.ai.mp.domain.UserRequest;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
@Repository
public interface UserRequestRepository extends JpaRepository<UserRequest, Long> {
    UserRequest findFirstByUserIdEqualsAndProviderIDEqualsAndRequestTypeEqualsOrderByCreatedDateDesc(long userID, long providerID, int requestType);

	List<UserRequest> findCompanyByUserId(Long userId);

	   UserRequest findOneByconfirmationkeyEquals(String confirmationkey);
	   UserRequest findOneByUserIdEquals(Long userId);
	@Query(value="SELECT * FROM datamarketplace.mp_user_request where user_id=?1 and request_type=?2 order by created_date desc limit 1",nativeQuery=true)  
    UserRequest findByUserIdAndRequestType(Long userId, int requestType);


	//void save(String confirmationkey);



}
