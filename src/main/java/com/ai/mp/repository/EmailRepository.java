package com.ai.mp.repository;

import com.ai.mp.domain.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the Authority entity.
 */
@Repository
public interface EmailRepository extends JpaRepository<Email, Long> {
    List<Email> findAllByNotificationStatusEquals(int notificationStatus);

    List<Email> findAllByNotificationStatusEqualsAndRetrycountIsLessThanEqual(int notificationStatus, int retryCount);
}
