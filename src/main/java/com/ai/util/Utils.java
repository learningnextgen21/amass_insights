package com.ai.util;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

public class Utils {
    private static Random random = new Random();
    public static void log(String log)
	{
		System.out.println(log);
	}
    public static int randomInt(int min, int max) {
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = random.nextInt((max - min) + 1) + min;
        return randomNum;
    }
    public static String randomString(int minimum, int maximum)
    {
        int randomInt = randomInt(minimum, maximum);
        return RandomStringUtils.randomAlphabetic(randomInt);
    }
}
