package com.ai.core;

public class StrUtils {
    static String otherThanQuote = " [^\"] ";
    static String quotedString = String.format(" \" %s* \" ", otherThanQuote);
    static String REGEX_FIND_COMMA_NOT_IN_QUOTE = String.format("(?x) "+ // enable comments, ignore white spaces
                    ",                         "+ // match a comma
                    "(?=                       "+ // start positive look ahead
                    "  (?:                     "+ //   start non-capturing group 1
                    "    %s*                   "+ //     match 'otherThanQuote' zero or more times
                    "    %s                    "+ //     match 'quotedString'
                    "  )*                      "+ //   end group 1 and repeat it zero or more times
                    "  %s*                     "+ //   match 'otherThanQuote'
                    "  $                       "+ // match the end of the string
                    ")                         ", // stop positive look ahead
            otherThanQuote, quotedString, otherThanQuote);
    public static String[] splitByComma(String value)
    {
        if(isBlank(value))
            return  null;
        String[] values = value.split(REGEX_FIND_COMMA_NOT_IN_QUOTE);
        for(int i = 0; i < values.length; i++)
        {
            values[i] = values[i].replaceAll("^\"|\"$", "").trim();
        }
        return values;
    }

    public static boolean isBlank(String value) {
        if(value == null || "".equals(value.trim()) || "\n".equals(value))
            return true;
        return false;
    }
    public static boolean isNotBlank(String value)
    {
        return !(isBlank(value));
    }
    public static boolean isBlank(Object value)
    {
    		if(value == null || String.valueOf(value) == null)
    			return true;
    		return "".equals(value.toString().trim()) ? true : false;
    }
    public static boolean isNotBlank(Object value)
    {
    		return !isBlank(value);
    }
}
