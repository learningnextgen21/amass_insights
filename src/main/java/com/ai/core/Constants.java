package com.ai.core;

import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

public class Constants {
    public static final Table<Record>  TABLE_STG_DATA_SOURCE                    = table("stg_data_source");
    public static final Table<Record>  TABLE_STG_DATA_PRODUCT                   = table("stg_data_product");
    public static final Table<Record>  TABLE_STG_DATA_FEATURE                   = table("stg_data_feature");
    public static final Table<Record>  TABLE_STG_DATA_ARTICLE                   = table("stg_article");
    public static final Table<Record>  TABLE_STG_DATA_PROVIDER                  = table("stg_data_provider");
    public static final Table<Record>  TABLE_STG_DATA_INDUSTRY                  = table("stg_data_industry");
    public static final Table<Record>  TABLE_STG_DATA_PROVIDER_TAG              = table("stg_data_provider_tag");
    public static final Table<Record>  TABLE_STG_DATA_LINK                   = table("stg_data_links");
    public static final Table<Record>  TABLE_STG_DATA_EVENT                   = table("stg_data_events");
    public static final Table<Record>  TABLE_STG_DATA_COMMENT                   = table("stg_data_comment");
    public static final Table<Record>  TABLE_STG_DATA_ORGANIZATION                  = table("stg_data_organization");
    public static final Table<Record>  TABLE_STG_DATA_RESOURCES                    = table("stg_data_resources");
    public static final Table<Record>  TABLE_STG_DATA_INVESTMENT_MANAGERS                  = table("stg_investment_managers");
    public static final Table<Record>  TABLE_STG_DATA_EMAILS                  = table("stg_data_emails");
    public static final Table<Record>  TABLE_STG_DATA_DOMAINS                  = table("stg_data_domains");


    public static final Table<Record>  TABLE_IM_SOURCE_PROVIDER                 = table("im_source_providers");
    public static final Table<Record>  TABLE_IM_PRODUCT_FEATURE                 = table("im_product_features");
    public static final Table<Record>  TABLE_IM_PROVIDER_TO_TAG                 = table("im_provider_to_tags");
    public static final Table<Record>  TABLE_IM_FEATURE_CATEGORY                = table("im_feature_category");
    public static final Table<Record>  TABLE_IM_PROVIDER_ARTICLE                = table("im_provider_article");
    public static final Table<Record>  TABLE_IM_PROVIDER_FEATURE                = table("im_provider_feature");
    //public static final Table<Record>  TABLE_IM_PROVIDER_LINKS                = table("im_provider_links");
    public static final Table<Record>  TABLE_IM_PROVIDER_PARTNER                = table("im_provider_partner");
    public static final Table<Record>  TABLE_IM_DISTRIBUTION_PARTNER            = table("im_distribution_partner");
    public static final Table<Record>  TABLE_IM_CONSUMER_PARTNER                = table("im_consumer_partner");
    public static final Table<Record>  TABLE_IM_PROVIDER_CATEGORY               = table("im_provider_category");
    public static final Table<Record>  TABLE_IM_PROVIDER_DELIVERY_FORMAT        = table("im_provider_delivery_format");
    public static final Table<Record>  TABLE_IM_RESEARCH_METHODS        = table("im_research_methods_completed");
    public static final Table<Record>  TABLE_IM_ORGANIZATION_RESEARCH_METHODS        = table("im_organization_research_methods");
    public static final Table<Record>  TABLE_IM_PROVIDER_DELIVERY_METHOD        = table("im_provider_delivery_method");
    public static final Table<Record>  TABLE_IM_PROVIDER_DELIVERY_FREQUENCY     = table("im_provider_delivery_frequency");
    public static final Table<Record>  TABLE_IM_LINK_CATEGORY     = table("im_link_category");
    public static final Table<Record>  TABLE_IM_PROVIDER_PRICING_MODEL          = table("im_provider_pricing_model");
    public static final Table<Record>  TABLE_IM_EVENT_PRICING_MODEL          = table("im_event_pricing_model");
    public static final Table<Record>  TABLE_IM_RESOURCE_PURPOSE                = table("im_resource_purpose");
    public static final Table<Record>  TABLE_IM_ARTICLE_TOPIC                   = table("im_article_topic");
    public static final Table<Record>  TABLE_IM_EVENT_TYPE     = table("im_event_type");
    public static final Table<Record>  TABLE_IM_EVENT_PRICE     = table("im_event_price");
    public static final Table<Record>  TABLE_IM_EVENT_DATA_PROVIDER_ORGANIZATION    = table("im_event_data_provider_organization");
    public static final Table<Record>  TABLE_IM_EVENT_SPONSER_DATA_PROVIDER    = table("im_event_sponsor_data_provider");
    public static final Table<Record>  TABLE_IM_EVENT_SPONSER_DATA_ORGANIZATIONS   = table("im_event_sponsers_organization");
    public static final Table<Record>  TABLE_IM_EVENT_TOPIC                   = table("im_event_topic");
    public static final Table<Record>  TABLE_IM_EVENT_INDUSTRIES                   = table("im_event_industries");
    public static final Table<Record>  TABLE_IM_EVENT_CATEGORIES                   = table("im_event_categories");
    public static final Table<Record>  TABLE_IM_EVENT_ORGANIZATION                   = table("im_event_organization");
    public static final Table<Record>  TABLE_IM_COMMENT_PARTICIPANT_ORGANIZATION    = table("im_comment_organization");
    public static final Table<Record>  TABLE_IM_COMMENT_RELEVENT_ORGANIZATION    = table("im_comment_relevent_organization");
    public static final Table<Record>  TABLE_IM_COMMENT_PARTICIPANT_PROVIDER    = table("im_comment_provider");
    public static final Table<Record>  TABLE_IM_COMMENT_RELEVENT_PROVIDER    = table("im_comment_relevent_provider");
    public static final Table<Record>  TABLE_IM_COMMENT_PARTICIPANT_EVENT    = table("im_comment_event");
    public static final Table<Record>  TABLE_IM_COMMENT_RELEVENT_EVENT    = table("im_comment_relevent_event");
    public static final Table<Record>  TABLE_IM_COMMENT_PURPOSE    = table("im_comment_purpose");
    public static final Table<Record>  TABLE_IM_COMMENT_PURPOSE_TYPE    = table("im_comment_purpose_type");
    public static final Table<Record>  TABLE_IM_RESOURCE_PURPOSE_TYPE           = table("im_resource_purpose_type");
    public static final Table<Record>  TABLE_IM_INDUSTRY_ASSOCIATED_PROVIDER    = table("im_industry_associated_providers");
    public static final Table<Record>  TABLE_IM_GEOGRAPHICAL_FOCUS              = table("im_geographical_focus");
    public static final Table<Record>  TABLE_IM_RELEVANT_ASSET_CLASS            = table("im_relevant_asset_class");
    public static final Table<Record>  TABLE_IM_RELEVANT_INVESTOR_TYPE          = table("im_relevant_investor_type");
    public static final Table<Record>  TABLE_IM_RELEVANT_SECURITY_TYPE          = table("im_relevant_security_type");
    public static final Table<Record>  TABLE_IM_RELEVANT_SECTORS		           = table("im_relevant_sectors");

    //New Columns
    public static final Table<Record>  TABLE_IM_PROVIDER_IDENTIFIER		           = table("im_provider_identifiers");
    public static final Table<Record>  TABLE_IM_PROVIDER_DATA_TYPE		           = table("im_provider_data_types");
    public static final Table<Record>  TABLE_IM_DATA_PRODUCT_TYPE		           = table("im_data_product_types");
    public static final Table<Record>  TABLE_IM_DATA_PROVIDER_ACCESS		           = table("im_provider_access");
    public static final Table<Record>  TABLE_IM_PROVIDER_TIME_FRAME	           = table("im_provider_time_frame");
    public static final Table<Record>  TABLE_IM_PROVIDER_MARKET_CAP	           = table("im_provider_market_cap");
    public static final Table<Record>  TABLE_IM_PROVIDER_STRATEGRY	           = table("im_provider_investment_strategies");
    public static final Table<Record>  TABLE_IM_PROVIDER_RESEARCH	           = table("im_provider_research_style");
    public static final Table<Record>  TABLE_IM_PROVIDER_MANAGER_TYPE	           = table("im_provider_manager_type");
    public static final Table<Record>  TABLE_IM_PROVIDER_TARGET_ORGANIZATION	           = table("im_provider_target_organization");
    public static final Table<Record>  TABLE_IM_PROVIDER_COMPETITORS	           = table("im_provider_competitors");
    public static final Table<Record>  TABLE_IM_PROVIDER_COMPLEMENTARY	           = table("im_provider_complementary");
    public static final Table<Record>  TABLE_IM_PROVIDER_EQUITIES	           = table("im_provider_equities_style");
    public static final Table<Record>  TABLE_IM_PROVIDER_LANGUAGE	           = table("im_provider_language");
    public static final Table<Record>  TABLE_IM_PROVIDER_PAYMENT	           = table("im_provider_payment_method");
    public static final Table<Record>  TABLE_IM_DATA_GAPS_REASONS	           = table("im_provider_data_gaps_reasons");
    public static final Table<Record>  TABLE_IM_OUTLIER_REASONS	           = table("im_provider_outlier_reasons");
    public static final Table<Record>  TABLE_IM_BIASES	           = table("im_provider_biases");
    public static final Table<Record>  TABLE_IM_SUBSIDIARY	           = table("im_organization_subsidiary");

    /*******Resources fields*******/
    public static final Table<Record>  TABLE_IM_RESOURCE_WORKFLOW                 = table("im_resources_workflows");
    public static final Table<Record>  TABLE_IM_RESOURCE_PURPOSES                = table("im_resources_purposes");
    public static final Table<Record>  TABLE_IM_RESOURCE_PURPOSES_TYPE                = table("im_resources_purpose_type");
    public static final Table<Record>  TABLE_IM_RESOURCE_TOPIC                = table("im_resources_topic");
    public static final Table<Record>  TABLE_IM_RESOURCE_RELEVANT_ORGANIZATION                = table("im_resources_relevant_organization");
    public static final Table<Record>  TABLE_IM_RESOURCE_RELEVANT_DATA_PROVIDERS                = table("im_resources_relevant_data_providers");
    public static final Table<Record>  TABLE_IM_RESOURCE_RESEARCH_METHODS                = table("im_resources_research_methods");
    public static final Table<Record>  TABLE_IM_INVESTOR_ORGANIZATION                = table("im_investor_organization");
    public static final Table<Record>  TABLE_IM_INVESTOR_TYPES                = table("im_investor_types");
    public static final Table<Record>  TABLE_IM_INVESTOR_HEADQUARTER_LOCATION        = table("im_investor_headquarter_location");
    public static final Table<Record>  TABLE_IM_INVESTOR_GEOGRAPHICAL_FOCUS        = table("im_investor_geographical_focus");
    public static final Table<Record>  TABLE_IM_INVESTOR_STRATEGIES      = table("im_investor_strategies");
    public static final Table<Record>  TABLE_IM_INVESTOR_ASSET_CLASSES     = table("im_investor_asset_class");
    public static final Table<Record>  TABLE_IM_INVESTOR_SECTORS     = table("im_investor_sectors");
    public static final Table<Record>  TABLE_IM_INVESTOR_SUBTYPES     = table("im_investor_subtypes");
    public static final Table<Record>  TABLE_IM_INVESTOR_INVESTMENT_STAGES     = table("im_investor_investment_stages");
    public static final Table<Record>  TABLE_IM_INVESTOR_INVESTMENT_ROUNDS    = table("im_investor_investment_rounds");
    public static final Table<Record>  TABLE_IM_INVESTOR_TYPICAL_SIZE_CHECK    = table("im_investor_typical_check_size");
    public static final Table<Record>  TABLE_IM_INVESTOR_EQUITIES_MARKET_CAP   = table("im_investor_equities_market_cap");
    public static final Table<Record>  TABLE_IM_INVESTOR_EQUITIES_STYLE   = table("im_investor_equities_style");
    public static final Table<Record>  TABLE_IM_INVESTOR_INVESTING_TIMEFRAME   = table("im_investor_investing_timeframe");

    public static final Table<Record>  TABLE_IM_INVESTOR_RESEARCH_STYLE    = table("im_investor_research_styles");
    public static final Table<Record>  TABLE_IM_INVESTOR_SECURITY_TYPES    = table("im_investor_security_types");
    public static final Table<Record>  TABLE_IM_INVESTOR_INDUSTRY_SECTOR    = table("im_investor_industry_sector");
    public static final Table<Record>  TABLE_IM_INVESTOR_EXCLUSION_TEXT    = table("im_investor_exclusion_text");
    public static final Table<Record>  TABLE_IM_INVESTOR_PORTFOLIO_TEXT    = table("im_portfolio_company_text");

    public static final Table<Record>  TABLE_IM_TO_EMAIL_ADDRESSES   = table("im_to_email_addresses");
    public static final Table<Record>  TABLE_IM_CC_EMAIL_ADDRESSES   = table("im_cc_email_addresses");
    public static final Table<Record>  TABLE_IM_LABELS   = table("im_labels");
    public static final Table<Record>  TABLE_IM_EMAILS_PURPOSES   = table("im_emails_purposes");
    public static final Table<Record>  TABLE_IM_EMAILS_PURPOSE_TYPES   = table("im_emails_purpose_types");
    public static final Table<Record>  TABLE_IM_EMAILS_TOPICS   = table("im_emails_topics");
    public static final Table<Record>  TABLE_IM_DOMAIN_INVESTMENT_MANAGER   = table("im_domain_investment_manager");
    public static final Table<Record>  TABLE_IM_DOMAIN_DATA_PROVIDER   = table("im_domain_data_provider");
    public static final Table<Record>  TABLE_IM_DOMAIN_EMAIL_ADDRESS   = table("im_domain_email_address");

    
    public static final Table<Record>  TABLE_DATA_PROVIDER		           	   = table("data_provider");
    public static final Table<Record>  TABLE_FEED_STATUS                        = table("datamarketplace.feed_status");

    public static final Field<String> RECORD_ID = field("`Record ID`", String.class);
    public static final Field<String> PROVIDER = field("provider", String.class);
    public static final Field<String> WEBSITE = field("website", String.class);
    public static final Field<String> TOPIC = field("topic", String.class);
    public static final Field<String> INDUSTRIES = field("industries", String.class);
    public static final Field<String> CATEGORIES = field("categories", String.class);
    public static final Field<String> ORGANIZATION = field("organization", String.class);
    public static final Field<String> ORGANIZATION_RECORDID = field("Organization_RecordID", String.class);
    public static final Field<String> PROVIDER_RECORDID = field("Provider_RecordID", String.class);
    public static final Field<String> EVENT_RECORDID = field("Event_RecordID", String.class);
    public static final Field<String> LOGO_FILE = field("logo_file", String.class);
    public static final Field<byte[]> LOGO = field("logo", byte[].class);
    public static final Field<Integer> PROVIDER_ID = field("id", Integer.class);

    public static final Field<String> PROVIDER_INDUSTRIES = field("`Data Industries - Record IDs`", String.class);

    public static final Field<String> DATA_FEATURE = field("`Data Features - Record IDs`", String.class);
    public static final Field<String> EVENT_DATA_PROVIDER = field("`Relevant Topics - Record IDs`", String.class);
    public static final Field<String> EVENT_DATA_INDUSTRIES = field("`Relevant Data Industries - Record IDs`", String.class);
    public static final Field<String> EVENT_DATA_CATEGORY = field("`Relevant Data Categories - Record IDs`", String.class);
    public static final Field<String> EVENT_DATA_ORGANIZATION = field("`Organization Organizer- Record ID`", String.class);
    public static final Field<String> COMMENT_PARTICIPANT_ORGANIZATION = field("`Participant Organizations - Record IDs`", String.class);
    public static final Field<String> COMMENT_PARTICIPANT_PROVIDER = field("`Participant Data Providers - Record IDs`", String.class);
    public static final Field<String> COMMENT_RELEVENT_ORGANIZATION = field("`Relevant Organizations - Record IDs`", String.class);
    public static final Field<String> COMMENT_RELEVENT_PROVIDER = field("`Relevant Data Providers - Record IDs`", String.class);
    public static final Field<String> COMMENT_PARTICIPANT_EVENT = field("`Participant Events - Record IDs`", String.class);
    public static final Field<String> COMMENT_RELEVENT_EVENT = field("`Relevant Events - Record IDs`", String.class);
    public static final Field<String> COMMENT_PURPOSE = field("`Purpose - Record IDs`", String.class);
    public static final Field<String> COMMENT_PURPOSE_TYPE = field("`Purpose Type - Record IDs`", String.class);

    public static final Field<String> EVENT_TYPES = field("`Event Type`", String.class);
    public static final Field<String> EVENT_PRICE = field("`Price`", String.class);
    public static final Field<String> EVENT_DATA_PROVIDER_ORGANIZER = field("`Data Provider Organizer - Record ID`", String.class);
    public static final Field<String> EVENT_SPONSER_DATA_PROVIDER = field("`Sponsor Data Providers - Record IDs`", String.class);
    public static final Field<String> EVENT_SPONSER_DATA_ORGANIZATION = field("`Sponsor Organizations - Record IDs`", String.class);
    public static final Field<String> LINK_DATA_PROVIDER = field("`Relevant Data Providers - Record IDs`", String.class);
    public static final Field<String> DATA_CATEGORY = field("`Data Categories - Record IDs`", String.class);
    public static final Field<String> PROVIDER_TAGS = field("`Tags - Record IDs`", String.class);
    public static final Field<String> ASSOCIATED_DATA_CATEGORIES = field("`Associated Data Categories Record IDs`", String.class);
    public static final Field<String> PROVIDER_DATA_PARTNER = field("`Provider Data Partners - Record IDs`", String.class);
    public static final Field<String> DISTRIBUTION_DATA_PARTNER = field("`Distribution Data Partners - Record IDs`", String.class);
    public static final Field<String> CONSUMER_DATA_PARTNER = field("`Consumer Data Partners - Record IDs`", String.class);
    public static final Field<String> DELIVERY_FORMATS = field("`Delivery Formats`", String.class);
    public static final Field<String> RESEARCH_METHODS = field("`Research Methods Completed`", String.class);
    public static final Field<String> DELIVERY_METHODS = field("`Delivery Methods`", String.class);
    public static final Field<String> DELIVERY_FREQUENCIES = field("`Delivery Frequency`", String.class);
    public static final Field<String> LINK_CATEGORIES = field("`Link Category`", String.class);
    public static final Field<String> PRICING_MODELS = field("`Pricing Model`", String.class);
    public static final Field<String> PRICING_MODELS_OFFERED = field("`Pricing Models Offered`", String.class);
    public static final Field<String> ARTICLE_TOPICS = field("`Topics Combined - Record IDs`", String.class);
    public static final Field<String> GEOGRAPHICAL_FOCUS = field("`Geographical Focus`", String.class);
    public static final Field<String> RELEVANT_ASSET_CLASSES = field("`Relevant Asset Classes`", String.class);
    public static final Field<String> RELEVANT_SECURITY_TYPES = field("`Relevant Security Types`", String.class);
    public static final Field<String> RELEVANT_INVESTORY_TYPES = field("`Relevant Investor Type`", String.class);
    public static final Field<String> RELEVANT_SECTORS = field("`Relevant Sectors`", String.class);
    public static final Field<String> LINKED_DATA_PROVIDER = field("`Linked Data Providers - Record IDs`", String.class);
    public static final Field<String> PURPOSE_RECORDID = field("`Purpose - Record ID`", String.class);
    public static final Field<String> PURPOSE_TYPE_RECORDID = field("`Purpose Type - Record ID`", String.class);
    public static final Field<String> Resource_RecordID = field("`Resource_RecordID`", String.class);
    public static final Field<String> IM_PURPOSE_RECORDID = field("`Purpose_RecordID`", String.class);
    public static final Field<String> IM_PURPOSE_TYPE_RECORDID = field("`PurposeType_Record ID`", String.class);
    public static final Field<String> IM_PURPOSE_TYPE_RECORD = field("`Purpose_Type_RecordID`", String.class);
    public static final Field<String> INVESTOR_ORGANIZATION_RECORDIDS = field("`Owner Organization - Record IDs`", String.class);
    public static final Field<String> INVESTOR_TYPES = field("`Types`", String.class);
    public static final Field<String> INVESTOR_HEADQUARTERS_LOCATION_TEXT = field("`Headquarters Location Text`", String.class);
    public static final Field<String> INVESTOR_GEOGRAPHICAL_FOCUS_TEXT = field("`Geographical Focus Text`", String.class);
    public static final Field<String> INVESTOR_STRATEGIES = field("`Strategies`", String.class);
    public static final Field<String> INVESTOR_ASSET_CLASSES = field("`Asset Classes`", String.class);
    public static final Field<String> INVESTOR_SECTORS = field("`Sectors`", String.class);
    public static final Field<String> INVESTOR_SUBTYPES = field("`Subtypes`", String.class);
    public static final Field<String> INVESTOR_STAGES = field("`Investement Stages`", String.class);
    public static final Field<String> INVESTOR_ROUNDS = field("`Investment Rounds`", String.class);
    public static final Field<String> INVESTOR_TYPICAL_SIZE = field("`Typical Check Sizes`", String.class);
    public static final Field<String> INVESTOR_EQUITIES_MARKET_CAP = field("`Public Equities Market Cap`", String.class);
    public static final Field<String> INVESTOR_EQUITIES_STYLE = field("`Public Equities Style`", String.class);
    public static final Field<String> INVESTOR_INVESTING_TIME_FRAME = field("`Investing Time Frame`", String.class);

    public static final Field<String> INVESTOR_RESEARCH_STYLES = field("`Research Styles`", String.class);
    public static final Field<String> INVESTOR_SECURITY_TYPES = field("`Security Types`", String.class);
    public static final Field<String> INVESTOR_INDUSTRY_SECTOR = field("`Industry and Sector Focus Text`", String.class);
    public static final Field<String> INVESTOR_EXCLUSIONS_TEXT = field("`Exclusions Text`", String.class);
    public static final Field<String> INVESTOR_PORTFOLIO_COMPANY_TEXT = field("`Portfolio Companies Text`", String.class);

    public static final Field<String> DATA_TO_EMAIL_ADDRESSES = field("`To Email Addresses - Record IDs`", String.class);
    public static final Field<String> DATA_CC_EMAIL_ADDRESSES = field("`CC Email Addresses - Record IDs`", String.class);
    public static final Field<String> DATA_LABELS = field("`Labels`", String.class);
    public static final Field<String> DATA_PURPOSES = field("`Purposes - Record IDs`", String.class);
    public static final Field<String> DATA_PURPOSES_TYPES = field("`Purpose Types - Record IDs`", String.class);
    public static final Field<String> DATA_TOPICS = field("`Topics - Record IDs`", String.class);
    public static final Field<String> DOMAIN_INVESTMENT_MANAGER = field("`Owner Investment Manager - Record ID`", String.class);
    public static final Field<String> DOMAIN_DATA_PROVIDER = field("`Owner Data Provider - Record ID`", String.class);
    public static final Field<String> DOMAIN_DATA_EMAIL_ADDRESS = field("`Email Addresses - Record ID`", String.class);

    
    //New Columns
    public static final Field<String> IM_PROVIDER_IDENTIFIERS = field("`Identifiers Available`", String.class);
    public static final Field<String> IM_PROVIDER_DATA_TYPE = field("`Data Types`", String.class);
    public static final Field<String> IM_PROVIDER_PRODUCT_TYPE = field("`Data Product Types`", String.class);
    public static final Field<String> IM_PROVIDER_ACCESS_OFFERED = field("`Access Offered`", String.class);
    public static final Field<String> IM_PROVIDER_TIME_FRAME = field("`Target Investment Managers Investing Time Frame`", String.class);
    public static final Field<String> IM_PROVIDER_MARKET_CAP = field("`Target Investment Manager Public Equities Market Cap`", String.class);
    public static final Field<String> IM_PROVIDER_MANAGER_STRATERGY = field("`Target Investment Manager Strategies`", String.class);
    public static final Field<String> IM_PROVIDER_MANAGER_RESEARCH = field("`Target Investment Manager Research Styles`", String.class);
    public static final Field<String> IM_PROVIDER_MANAGER_TYPE = field("`Target Investment Manager Type`", String.class);
    public static final Field<String> IM_PROVIDER_ORGANIZATION_TYPE = field("`Target Organization Type`", String.class);
    public static final Field<String> IM_PROVIDER_COMPETITORS = field("`Competitors - Record IDs`", String.class);
    public static final Field<String> IM_PROVIDER_COMPLEMENTARY = field("`Complementary Providers - Record IDs`", String.class);
    public static final Field<String> IM_PROVIDER_EQUITIES = field("`Target Investment Managers Public Equities Style`", String.class);
    public static final Field<String> IM_PROVIDER_LANGUAGE = field("`Data Languages Available`", String.class);
    public static final Field<String> IM_PROVIDER_PAYMENT = field("`Payment Methods Offered`", String.class);
    public static final Field<String> IM_PROVIDER_DATA_GAPS_REASONS = field("`Data Gaps Reasons`", String.class);
    public static final Field<String> IM_PROVIDER_OUTLIER_REASONS = field("`Outlier Reasons`", String.class);
    public static final Field<String> IM_PROVIDER_BIASES = field("`Biases`", String.class);
    public static final Field<String> IM_ORGANIZATION_SUBSIDIARY = field("`Subsidiary Organization - Record IDs`", String.class);


    /*******Resources fields*******/
    public static final Field<String> IM_WORKFLOW = field("`Workflows`", String.class);
    public static final Field<String> IM_RESOURCES_PURPOSES = field("`Purposes - Record IDs`", String.class);
    public static final Field<String> IM_RESOURCES_PURPOSE_TYPE = field("`Purpose Types - Record IDs`", String.class);
    public static final Field<String> IM_RESOURCES_TOPICS = field("`Topics - Record IDs`", String.class);
    public static final Field<String> IM_RESOURCES_RELEVENT_ORGANIZATIONS = field("`Relevant Organizations - Record IDs`", String.class);
    public static final Field<String> IM_RESOURCES_RELEVENT_DATA_PROVIDERS = field("`Relevant Data Providers - Record IDs`", String.class);


    //Product fields
    public static final Field<String> DATA_FEATURES = field("`Data Features`", String.class);
    public static final Field<String> PRODUCT_NAME = field("`Product Name`", String.class);

    public static final Field<String> SOURCE = field("Source", String.class);
    public static final Field<String> EXPLANATION = field("Explanation", String.class);
    public static final Field<String> PROVIDER_SOURCES = field("`Data Sources - Record IDs`", String.class);
    public static final Field<String> InvestorType = field("investor_type", String.class);
    public static final Field<String> CATEGORY = field("category", String.class);
    public static final Field<String> FEATURE = field("feature", String.class);
    public static final Field<String> LINKS = field("links", String.class);
    public static final Field<String> SECTOR = field("sector", String.class);
    public static final Field<String> IM_GEOGRAPHICAL_FOCUS = field("geographical_focus", String.class);
    public static final Field<String> ASSET_CLASS = field("asset_class", String.class);
    public static final Field<String> SECURITY_TYPE = field("security_type", String.class);
    public static final Field<String> PARTNER = field("Partner", String.class);
    public static final Field<String> DELIVERY_FORMAT = field("Format", String.class);
    public static final Field<String> RESEARCH_METHOD = field("research_methods", String.class);
    //investor
    public static final Field<String> INVESTOR_ORGANIZATION_RECORDID = field("`Owner_Organization_RecordID`", String.class);
    public static final Field<String> INVESTOR_TYPE = field("`type`", String.class);
    public static final Field<String> INVESTOR_HEADQUARTER = field("`headquarters_location`", String.class);
    public static final Field<String> INVESTOR_GEOGRAPHICAL_FOCUS = field("`geographical_focus`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_STRATEGIES = field("`investment_strategies`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_ASSET_CLASSES = field("`asset_class`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_SECTORS = field("`sector`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_SUBTYPE = field("`subtype`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_STAGE = field("`investment_stage`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_ROUND = field("`investment_round`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_TYPICAL_SIZE = field("`typical_check_size`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_EQUITIES_MARKET_CAP = field("`equities_market_cap`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_EQUITIES_STYLE = field("`equities_style`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_INVESTING_TIMEFRAME = field("`investing_timeframe`", String.class);

    public static final Field<String> INVESTOR_INVESTMENT_RESEARCH_STYLE = field("`research_style`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_SECURITY_TYPE = field("`security_type`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_INDUSTRY_SECTOR = field("`industry_sector`", String.class);
   // public static final Field<String> INVESTOR_INVESTMENT_EXCLUSION_TEXT = field("`exclusion_text`", String.class);
    public static final Field<String> INVESTOR_INVESTMENT_PORTFOLIO_TEXT = field("`portfolio_company_text`", String.class);

    public static final Field<String> TO_EMAIL_ADDRESSES_RECORDID= field("`to_email_addresses_recordID`", String.class);
    public static final Field<String> CC_EMAIL_ADDRESSES_RECORDID= field("`cc_email_addresses_recordID`", String.class);
    public static final Field<String> LABELS= field("`labels`", String.class);
    public static final Field<String> PURPOESES_RECORDID= field("`purposes_recordID`", String.class);
    public static final Field<String> PURPOESE_TYPES_RECORDID= field("`purposes_types_recordID`", String.class);
    public static final Field<String> TOPICS_RECORDID= field("`topics_recordID`", String.class);
    public static final Field<String> DOMAIN_INVESTMENT_RECORDID= field("`investment_manager_recordID`", String.class);
    public static final Field<String> DOMAIN_DATA_PROVIDER_RECORDID= field("`data_provider_recordID`", String.class);
    public static final Field<String> DOMAIN_EMAIL_ADDRESS_RECORDID= field("`email_address_recordID`", String.class);

    
    public static final Field<String> DELIVERY_METHOD = field("Method", String.class);
    public static final Field<String> DELIVERY_FREQUENCY = field("frequency", String.class);
    public static final Field<String> LINK_CATEGORY = field("category", String.class);
    public static final Field<String> EVENT_TYPE = field("event", String.class);
    public static final Field<String> EVENT_PRICES = field("price", String.class);
    public static final Field<String> EVENT_PROVIDER_ORGANIZATION= field("data_provider_organization", String.class);
    public static final Field<String> EVENT_SPONSER_DATA_PROVIDERS= field("sponser_data_provider", String.class);
    public static final Field<String> EVENT_SPONSER_ORGANIZATION= field("sponser_organization", String.class);
    public static final Field<String> EVENT_PRICE_RECORD_ID = field("Price_RecordID", String.class);
    public static final Field<String> LINK_RECORD_ID = field("Link_RecordID", String.class);
    public static final Field<String> EVENT_RECORD_ID = field("Event_RecordID", String.class);
    public static final Field<String> EVENT_DATA_PROVIDER_RECORD_ID = field("Data_Provider_Organization_RecordID", String.class);
    public static final Field<String> EVENT_SPONSER_DATA_PROVIDER_RECORD_ID = field("Event_RecordID", String.class);
    public static final Field<String> EVENT_SPONSER_ORGANIZATION_RECORD_ID = field("Sponser_Organization_RecordID", String.class);
    public static final Field<String> LINK = field("Link", String.class);
    public static final Field<String> PRICING_MODEL = field("pricing_model", String.class);

    //New Columns
    public static final Field<String> IDENTIFIERS = field("Identifiers", String.class);
    public static final Field<String> DATATYPES = field("DataTypes", String.class);
    public static final Field<String> PRODUCT_TYPE = field("DataProductTypes", String.class);
    public static final Field<String> ACCESS_OFFERED = field("AccessOffered", String.class);
    public static final Field<String> TIME_FRAME = field("TimeFrame", String.class);
    public static final Field<String> MARKET_CAP = field("MarketCap", String.class);
    public static final Field<String> STRATERGY = field("ManagerStrategies", String.class);
    public static final Field<String> RESEARCH = field("ResearchStyles", String.class);
    public static final Field<String> MANAGER_TYPE = field("ManagerType", String.class);
    public static final Field<String> ORGANIZATION_TYPE = field("OrganizationType", String.class);
    public static final Field<String> COMPETITORS = field("Competitors", String.class);
    public static final Field<String> COMPLEMENTARY = field("Complementary", String.class);
    public static final Field<String> EQUITIES = field("EquitiesStyle", String.class);
    public static final Field<String> LANGUAGE = field("Languages", String.class);
    public static final Field<String> PAYMENT = field("Payment", String.class);
    public static final Field<String> DATA_GAPS_REASONS = field("DataGapsReasons", String.class);
    public static final Field<String> DATA_OUTLIER_REASONS = field("OutlierReasons", String.class);
    public static final Field<String> DATA_BIASES = field("Biases", String.class);
    public static final Field<String> SUBSIDIARY = field("subsidiary", String.class);




    public static final Field<String> SOURCE_RECORD_ID = field("Source_RecordID", String.class);
    public static final Field<String> INDUSTRY_RECORD_ID = field("Industries_RecordID", String.class);
    public static final Field<String> PROVIDER_RECORD_ID = field("Provider_RecordID", String.class);
    public static final Field<String> TAG_RECORD_ID = field("Tag_RecordID", String.class);
    public static final Field<String> FEATURE_RECORD_ID = field("Feature_RecordID", String.class);
    public static final Field<String> ARTICLE_RECORD_ID = field("Article_recordID", String.class);
    public static final Field<String> IM_EVENT_RECORD_ID = field("Event_RecordID", String.class);
    public static final Field<String> IM_EVENT_INDUSTRIES_RECORD_ID = field("Industries_RecordID", String.class);
    public static final Field<String> IM_EVENT_CATEGORIES_RECORD_ID = field("Categories_RecordID", String.class);
    public static final Field<String> IM_EVENT_ORGANIZATION_RECORD_ID = field("Organization_RecordID", String.class);
    public static final Field<String> IM_ORGANIZATION_RECORD_ID = field("Organization_RecordID", String.class);
    public static final Field<String> IM_COMMENT_RECORD_ID = field("Comment_RecordID", String.class);
    public static final Field<String> PRODUCT_RECORD_ID = field("Product_RecordID", String.class);

    public static final Field<String> FEED_NAME = field("feed_name", String.class);
    public static final Field<String> FEED_STATUS = field("status", String.class);

    /*******Resources fields*******/
    public static final Field<String> RESOURCE_RECORD_ID = field("Resource_RecordID", String.class);
    public static final Field<String> RESOURCE_WORKFLOW = field("Workflows", String.class);
    public static final Field<String> RESOURCE_PURPOSE = field("purposes", String.class);
    public static final Field<String> RESOURCE_PURPOSE_TYPE = field("purpose_type", String.class);
    public static final Field<String> RESOURCE_TOPIC = field("topic", String.class);
    public static final Field<String> RESOURCE_RELEVANT_ORGANIZATION = field("relevant_organization", String.class);
    public static final Field<String> RESOURCE_RELEVANT_DATA_PROVIDER = field("data_provider", String.class);

    public static final Field<String> INVESTMENT_RECORD_ID = field("investment_recordID", String.class);

    public static final Field<String> INVESTMENT_RECORD_IDS = field("Investment_RecordID", String.class);
    
    public static final Field<String> EMAILS_RECORD_ID = field("emails_recordID", String.class);
    public static final Field<String> DOMAINS_RECORD_ID = field("domains_recordID", String.class);


    public static final int INTEREST_TYPE_FOLLOW = 252;
    public static final int INTEREST_TYPE_UNLOCK = 253;
    public static final int INTEREST_TYPE_UNLOCK_DENY = 269;
    // public static final int INTEREST_TYPE_UNLOCK_PENDING = 262;
    public static  int INTEREST_TYPE_UNLOCK_PENDING = 2087;
    public static final int INTEREST_TYPE_UNLOCK_PENDING_USER_CONFIRM = 266;
    public static final int INTEREST_TYPE_UNLOCK_NEED_MORE_INFO = 268;
    public static final int INTEREST_TYPE_EXPAND = 254;
    public static final int INTEREST_TYPE_EXPAND_PENDING = 264;
    public static final int INTEREST_TYPE_EXPAND_PENDING_USER_CONFIRM = 267;
    //public static final int INTEREST_TYPE_MORE_INFO = 261;
    public static  int INTEREST_TYPE_MORE_INFO = 2086;
    public static final int INTEREST_TYPE_CANCEL_MORE_INFO = 270;
    public static final int INTEREST_TYPE_CANCEL_DATA_SAMPLE = 271;
    public static final int INTEREST_TYPE_CANCEL_DIRECT_CONTACT_PROVIDER = 272;
    public static final int INTEREST_TYPE_OPERATOR = 561;

    public static final int REQUEST_HELP_FINDING_DATA = 562;
    public static final int REQUEST_HELP_ENGINEERING_DATA = 563;
    public static final int REQUEST_HELP_SELLING_DATA = 564;
    public static final int INTEREST_TYPE_DATA_SAMPLE = 565;
    public static final int INTEREST_TYPE_DIRECT_CONTACT_PROVIDER = 566;
    public static final int INTEREST_TYPE_GENERAL_INQUIRY = 568;
    public static final int INTEREST_TYPE_PRESS = 569;
    public static final int INTEREST_TYPE_TECH_SUPPORT = 570;

    public static final int LOOKUP_PROVIDER_DATA_PARTNER = 16;
    public static final int LOOKUP_DISTRIBUTION_DATA_PARTNER = 17;
    public static final int LOOKUP_COMPLEMENTARY_DATA_PARTNER = 15;
    public static final int LOOKUP_COMPETITORS = 1720;
    public static final int LOOKUP_EVENT_ORGANIZATION_TYPE = 571;
    public static final int LOOKUP_EVENT_SPONSOR_ORGANIZATION_TYPE = 572;
    public static final int LOOKUP_EVENT_PROVIDER_TYPE = 573;
    public static final int LOOKUP_EVENT_SPONSOR_PROVIDER_TYPE = 574;
    public static final int LOOKUP_CONSUMER_DATA_PARTNER = 18;
    public static final int LOOKUP_RELEVENT_PROVIDER_TYPE = 578;

    public static final int NOTIFICATION_STATUS_NEW = 0;
    public static final int NOTIFICATION_STATUS_SENT = 1;
    public static final int NOTIFICATION_STATUS_FAILED = 2;

    public static final int REQUEST_TYPE_CONTACT_US = 256;
    public static  int REQUEST_TYPE_EMAIL_SUBSCRIPTION = 300;
    public static  int REQUEST_TYPE_EMAIL_UNSUBSCRIPTION = 301;

    public static final String USER = "user";
    public static final String DATA_PROVIDER = "dataProvider";
    public static final String DATA_PROVIDER_EDIT = "dataProviderEdit";
    public static final String DATA_CATEGORY_FOLLOW = "dataCategory";
    public static final String EMAILADDRESS = "emailAddress";
    public static final String DATA_ARTICLE="dataArticle";
    public static final String Data_Topic="dataTopic";
    public static final String DATA_EDIT="dataEdit";
    public static final String DATA_INDUSTRIES="industries";
    public static final String INVITETYPE="inviteType";
    public static final String BASE_URL = "baseUrl";
    public static final String SOCIAL_CONNECTION = "socialConnection";
    public static final String USER_CHECKOUT="userCheckout";
    public static final String URL="url";
    public static final String DATA_FEATURESLIST = "feature";

	public static final String DATA_GEO = "geo";

	public static final String DATA_PROVIDERPARTNERS = "providerpartner";

	public static final String DATA_DISTRIBUTIONPARTNERS = "distributionpartner";

	public static final String DATA_CONSUMERPARTNER = "consumerpartner";
	public static final String DATA_COMPLIMENTARYPARTNER = "complimentarypartner";
	public static final String DATA_COMPETITORS = "competitors";

	public static final String DATA_CATEGORIES = "categories";

	public static final String DATA_PROVIDERTAGS = "providertags";

	public static final String DATA_SOURCES = "sources";

	public static final String DATA_ASSETCLASS = "assetclass";

	public static final String DATA_DELIVERYFORMAT = "deliveryformat";

	public static final String DATA_PRICING = "pricing";

	public static final String DATA_SECTOR = "sector";

	public static final String DATA_DELIVERY = "delivery";

	public static final String DATA_INVESTOR = "investor";

	public static final String DATA_FREQUENCIES = "frequencies";

	public static final String DATA_SECURITY = "security";

	public static final String DATA_BIASE = "biase";

	public static final String DATA_OUTLIER = "outlier";

	public static final String DATA_GAP = "gap";
	public static final String DATA_ORGANIZATIONTYPE = "organizationtype";
	public static final String DATA_MANAGERTYPE = "managertype";
	public static final String DATA_INVESTORTYPE = "investortype";
	public static final String DATA_STRATEGIES = "strategies";
	public static final String DATA_RESEARCHSTYLES = "research";
	public static final String DATA_INVESTING = "investing";
	public static final String DATA_FOCUS = "focus";
	public static final String DATA_STYLE = "style";
	public static final String DATA_IDENTIFIERS = "identifiers";
	public static final String DATA_ACCESS = "access";
	public static final String DATA_PRODUCT = "product";
	public static final String DATA_TYPES = "types";
	public static final String DATA_LANGUAGE = "language";
	public static final String DATA_PAYMENT = "payment";
	public static final String DATA_PRICINGDETAILS = "pricing";
	public static final String DATA_PRICINGMODELS = "pricingmodel";
	public static final String DATA_PROVIDERTYPE = "providertype";
	public static final String DATA_PARTNERSHIP = "partnership";
	public static final String DATA_HEADCOUNTBUCKET = "headcountbucket";
	public static final String DATA_DATECOLLECTION = "datecollection";
	public static final String DATA_FREETRIAL = "freetrial";
	public static final String DATA_INDUSTRY = "mainindustry";
	public static final String DATA_MAINCATEGORY = "maincategory";
	public static final String ROLE_PROVIDER = "ROLE_PROVIDER";
	public static final String ROLE_INVESTOR = "ROLE_INVESTOR";

}
