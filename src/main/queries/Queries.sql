  SET @@SESSION.sql_mode='ALLOW_INVALID_DATES';
 update stg_articles set published_date = CASE
   WHEN str_to_date(published_date_str,'%Y %m %d') is not null
   THEN
     str_to_date(published_date_str,'%Y %m %d')
    when str_to_date(published_date_str,'%M %D %Y') is not null
   THEN
     str_to_date(published_date_str,'%M %D %Y')
   else
     null
 END;

update data_provider set permission
= case permission
  when 'Registered'	then 'ROLE_USER'
  when 'Bronze'  then 'ROLE_BRONZE'
  when 'Silver'	 	then 'ROLE_SILVER'
  when 'Admin'    then 'ROLE_ADMIN'
  else permission
  end;
update data_category set permission
= case permission
  when 'Registered'	then 'ROLE_USER'
  when 'Bronze'  then 'ROLE_BRONZE'
  when 'Silver'	 	then 'ROLE_SILVER'
  when 'Admin'    then 'ROLE_ADMIN'
  else permission
  end;

  delete from jhi_social_user_connection where user_id = 'rahulapatil@gmail.com';
  delete from mp_email where user_id in (select id from jhi_user where login = 'rahulapatil@gmail.com' or email = 'rahulapatil@gmail.com');
  delete from jhi_user_authority where user_id in (select id from jhi_user where login = 'rahulapatil@gmail.com' or email = 'rahulapatil@gmail.com');
  delete from jhi_user where login = 'rahulapatil@gmail.com' or email = 'rahulapatil@gmail.com';
