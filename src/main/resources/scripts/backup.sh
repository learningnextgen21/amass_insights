#!/bin/sh
current_date=`/bin/date +%m%d%Y_%H%M%S`
BACKUP_LOCATION=/root/backup/
S3_LOCATION=s3://amassinsights-backups/`hostname`/
BACKUP_FOLDER=/home/ec2-user/
EXCLUDE=/home/ec2-user/mysql
BACKUP_PREFIX=ami_mp_
sql_backup=$BACKUP_LOCATION/all_databases_$current_date.sql.gz
find $BACKUP_LOCATION -type f -mtime +1 -name '*.gz' -execdir rm -- {} \;
mysqldump --single-transaction --routines --all-databases -umysql | gzip > $sql_backup
TAR_FILE=$BACKUP_PREFIX$current_date.tar.gz
TAR_LOCATION=$BACKUP_LOCATION$TAR_FILE
tar -czf $TAR_LOCATION $BACKUP_FOLDER $sql_backup --exclude=$EXCLUDE --exclude=/home/ec2-user/mount* --exclude=/home/ec2-user/amass_marketplace/target --exclude=/home/ec2-user/amass_marketplace/*.log --exclude=/home/ec2-user/Data* --exclude=/home/ec2-user/.*
S3_FILE=$S3_LOCATION$TAR_FILE
aws s3 cp $TAR_LOCATION $S3_FILE
