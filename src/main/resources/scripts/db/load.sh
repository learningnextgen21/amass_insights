#!/bin/sh
ctrlfile=load_files.ctl
source=../../../../staging/2017-10-16
host=localdevdb
echo `date` Starting data load script
while IFS= read -r file
do 
	IFS='=' read -r -a array <<< $file
	echo table: ${array[0]}
	table=${array[0]}
	input=${array[1]}
	comment=${array[0]:0:1}
	if [ $comment = '#' ]
	then
		printf "`date` \033[0;33mSkipping $table \n\033[0m"
	else
		echo `date` Loading Table: $table from File: $input
		sql="load data local infile '$source/$input' into table $table fields terminated by ',' optionally enclosed by '\"' ignore 1 lines;show warnings;"
		mysql -sN -umysql -pmysql -h$host staging_airtable -e"truncate table $table;$sql;show warnings;"
		echo `date` Truncated table: $table
		count=`mysql -sN -umysql -pmysql -h$host staging_airtable -e"select count(*) from $table;"`
		echo Loaded $count records in $table
		echo `date` Finished loading $input
 	fi
	echo "____________________________________________________________________________________"
done < $ctrlfile
echo `date` Finished load scripts.
