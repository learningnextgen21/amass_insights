TRUNCATE TABLE data_resources_workflows;
INSERT INTO data_resources_workflows(workflow_id,resource_id)
   SELECT lc.id, dr.id
     FROM data_resource dr
          INNER JOIN staging_airtable.im_resources_workflows rw
             ON rw.Resource_RecordID = dr.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'WORKFLOW'
                AND lc.lookupcode = rw.Workflows
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_resources_workflows pa
               WHERE     pa.workflow_id = lc.id
                     AND pa.resource_id = dr.id);


create table temp_im_resources_workflows as select mlc.id, count(*) count from data_resources_workflows dpdf inner join mp_lookup_code mlc on dpdf.workflow_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_im_resources_workflows
set record_count = temp_im_resources_workflows.count
where temp_im_resources_workflows.id = mp_lookup_code.id;

drop table if exists temp_im_resources_workflows;


TRUNCATE TABLE data_resources_purposes;
INSERT INTO data_resources_purposes(purpose_id, resource_id)
   SELECT DISTINCT df.id, dp.id
     FROM staging_airtable.im_resources_purposes pf
          INNER JOIN data_resource dp ON dp.record_id = pf.Resource_RecordID
          INNER JOIN data_resource_purpose df ON df.record_id = pf.purposes
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_resources_purposes jpf
               WHERE     jpf.purpose_id = df.id
                     AND jpf.resource_id = dp.id);

TRUNCATE TABLE data_resources_purpose_type;
INSERT INTO data_resources_purpose_type(purpose_type_id, resource_id)
   SELECT DISTINCT df.id, dp.id
     FROM staging_airtable.im_resources_purpose_type pf
          INNER JOIN data_resource dp ON dp.record_id = pf.Resource_RecordID
          INNER JOIN data_resource_purpose_type df ON df.record_id = pf.purpose_type
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_resources_purpose_type jpf
               WHERE     jpf.purpose_type_id = df.id
                     AND jpf.resource_id = dp.id);

TRUNCATE TABLE data_resources_topic;
INSERT INTO data_resources_topic(topic_id, resource_id)
   SELECT DISTINCT df.id, dp.id
     FROM staging_airtable.im_resources_topic pf
          INNER JOIN data_resource dp ON dp.record_id = pf.Resource_RecordID
          INNER JOIN data_topic df ON df.record_id = pf.topic
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_resources_topic jpf
               WHERE     jpf.topic_id = df.id
                     AND jpf.resource_id = dp.id);

TRUNCATE TABLE data_resources_relevant_organization;
INSERT INTO data_resources_relevant_organization(organization_id, resource_id)
   SELECT dc.id, dp.id
     FROM staging_airtable.im_resources_relevant_organization pc
          INNER JOIN data_resource dp ON dp.record_id = pc.Resource_RecordID
          INNER JOIN data_organization dc ON pc.relevant_organization = dc.record_id
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_resources_relevant_organization jpc
               WHERE     jpc.resource_id = dp.id
                     AND jpc.organization_id = dc.id);

DELETE FROM data_resources_data_providers 
WHERE resource_id NOT IN(
SELECT id  FROM data_resource);

TRUNCATE TABLE data_resources_research_methods;
INSERT INTO data_resources_research_methods(research_method_id,
                                          resource_id)
   SELECT lc.id, dp.id
     FROM data_resource dp
          INNER JOIN staging_airtable.im_resources_research_methods rac
             ON rac.Resource_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'RESEARCH_METHOD'
                AND lc.lookupcode = rac.research_methods
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_resources_research_methods pa
               WHERE     pa.research_method_id = lc.id
                     AND pa.resource_id = dp.id);


INSERT INTO data_resources_data_providers(resource_id, provider_id)
   SELECT dr.id, dp.id
     FROM staging_airtable.im_resources_relevant_data_providers pc
          INNER JOIN data_resource dr ON pc.Resource_RecordID = dr.Record_ID
          INNER JOIN data_provider dp ON pc.data_provider = dp.record_id
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_resources_data_providers jpc
               WHERE     jpc.resource_id = dr.id
                     AND jpc.provider_id = dp.id);
                     





