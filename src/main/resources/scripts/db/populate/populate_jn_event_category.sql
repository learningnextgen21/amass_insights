truncate table data_events_category;
delete from data_events_category where not exists(select 1 from
  data_event da, data_category dt
where da.id = data_events_category.event_id
      and dt.id = data_events_category.event_category_id
);
INSERT INTO data_events_category(event_id,event_category_id)
   SELECT dp.id, dc.id
     FROM staging_airtable.im_event_categories pc
          INNER JOIN data_event dp ON pc.Categories_RecordID = dp.Record_ID
          INNER JOIN data_category dc ON pc.categories = dc.record_id
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_events_category jpc
               WHERE     jpc.event_category_id = dc.id
                     AND jpc.event_id = dp.id);

create table temp_data_event_category_count as select drp.id, count(*) count from datamarketplace.data_events_category dap inner join datamarketplace.data_category drp ON dap.event_category_id = drp.id
group by drp.id;
update data_category, temp_data_event_category_count
set event_category_count = temp_data_event_category_count.count
where temp_data_event_category_count.id = data_category.id;

drop table if exists temp_data_event_category_count;

update data_category set event_category_count = 0 where event_category_count is null;
