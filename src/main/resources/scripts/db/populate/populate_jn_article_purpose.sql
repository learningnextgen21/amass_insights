truncate table data_article_purpose;
delete from data_article_purpose where not exists(select 1 from
  data_article da, data_resource_purpose drp
where da.id = data_article_purpose.articles_id
      and drp.id = data_article_purpose.purposes_id
);
INSERT INTO data_article_purpose(articles_id, purposes_id)
   SELECT DISTINCT  da.id, dt.id
     FROM staging_airtable.im_resource_purpose at
          INNER JOIN data_resource_purpose dt ON dt.record_id = at.Purpose_RecordID
         inner join data_article da on da.record_id = at.Resource_RecordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_article_purpose dat
               WHERE     dat.articles_id = da.id
                     AND dat.purposes_id = dt.id);

create table temp_data_article_purpose_count as select drp.id, count(*) count from datamarketplace.data_article_purpose dap inner join datamarketplace.data_resource_purpose drp ON dap.purposes_id = drp.id
group by drp.id;

update data_resource_purpose, temp_data_article_purpose_count
set article_count = temp_data_article_purpose_count.count
where temp_data_article_purpose_count.id = data_resource_purpose.id;

drop table if exists temp_data_article_purpose_count;

update data_resource_purpose set article_count = 0 where article_count is null;
