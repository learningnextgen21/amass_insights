DELETE from data_investor_assest_class
where not exists(select 1 from data_inv_managers dp, mp_lookup_code lc where dp.id = data_investor_assest_class.investor_managers_id and lc.id = data_investor_assest_class.asset_class_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'ASSET_CLASS' lookupmodule,
                    i.asset_class lookupcode,
                    i.asset_class description
               FROM staging_airtable.im_investor_asset_class i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'ASSET_CLASS'
                     AND c.lookupcode = a.lookupcode);
                     
INSERT INTO data_investor_assest_class(asset_class_id, investor_managers_id)
   SELECT lc.id, dp.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_asset_class rac
             ON rac.investment_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'ASSET_CLASS'
                AND lc.lookupcode = rac.asset_class
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_assest_class pa
               WHERE     pa.asset_class_id = lc.id
                     AND pa.investor_managers_id = dp.id);                     