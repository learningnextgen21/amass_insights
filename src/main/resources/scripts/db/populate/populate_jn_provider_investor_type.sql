DELETE from data_provider_investor_type where not exists(select 1 from data_provider dp, mp_lookup_code lc where dp.id = data_provider_investor_type.data_providers_id and lc.id = data_provider_investor_type.investor_types_id);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'RLVNT_INVESTOR_TYPE' lookupmodule,
                    i.investor_type lookupcode,
                    i.investor_type description
               FROM staging_airtable.im_relevant_investor_type  i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'RLVNT_INVESTOR_TYPE'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_provider_investor_type(investor_types_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_relevant_investor_type rit
             ON rit.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'RLVNT_INVESTOR_TYPE'
                AND lc.lookupcode = rit.investor_type
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_investor_type pa
               WHERE     pa.investor_types_id = lc.id
                     AND pa.data_Providers_ID = dp.id);

create table temp_investor_type_provider_count as select mlc.id, count(*) count from datamarketplace.data_provider_investor_type dpit inner join mp_lookup_code mlc on dpit.investor_types_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_investor_type_provider_count
set record_count = temp_investor_type_provider_count.count
where temp_investor_type_provider_count.id = mp_lookup_code.id;

drop table if exists temp_investor_type_provider_count;
