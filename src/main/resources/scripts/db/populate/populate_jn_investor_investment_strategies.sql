DELETE from data_investor_investment_strategies
where not exists(select 1 from data_inv_managers dp, mp_lookup_code lc where dp.id = data_investor_investment_strategies.investor_managers_id and lc.id = data_investor_investment_strategies.investment_strategies_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'INVESTMENT_MANAGER_STRATEGY' lookupmodule,
                    i.investment_strategies lookupcode,
                    i.investment_strategies description
               FROM staging_airtable.im_investor_strategies i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'INVESTMENT_MANAGER_STRATEGY'
                     AND c.lookupcode = a.lookupcode);
                     
INSERT INTO data_investor_investment_strategies(investment_strategies_id,
                                             investor_managers_id)
   SELECT lc.id, dp.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_strategies rac
             ON rac.investment_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'INVESTMENT_MANAGER_STRATEGY'
                AND lc.lookupcode = rac.investment_strategies
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_investment_strategies pa
               WHERE     pa.investment_strategies_id = lc.id
                     AND pa.investor_managers_id = dp.id);                     