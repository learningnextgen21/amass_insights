TRUNCATE TABLE data_subsidiary_organization;
INSERT INTO data_subsidiary_organization(organization_id, subsidiary_id)
   SELECT dp.id, dc.id
     FROM staging_airtable.im_organization_subsidiary pc
          INNER JOIN data_organization dp ON pc.Organization_RecordID = dp.Record_ID
          INNER JOIN data_organization dc ON pc.subsidiary = dc.record_id
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_subsidiary_organization jpc
               WHERE     jpc.subsidiary_id = dc.id
                     AND jpc.organization_id = dp.id);
