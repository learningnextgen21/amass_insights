DELETE from data_emails_purpose_type
where not exists(select 1 from data_emails dp, data_resource_purpose_type lc where dp.id = data_emails_purpose_type.email_id and lc.id = data_emails_purpose_type.purpose_type_id);

INSERT INTO data_emails_purpose_type(purpose_type_id, email_id)
   SELECT DISTINCT df.id, dp.id
     FROM staging_airtable.im_emails_purpose_types pf
          INNER JOIN data_emails dp ON dp.record_id = pf.emails_recordID
          INNER JOIN data_resource_purpose_type df ON df.record_id = pf.purposes_types_recordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_emails_purpose_type jpf
               WHERE     jpf.purpose_type_id = df.id
                     AND jpf.email_id = dp.id);                     