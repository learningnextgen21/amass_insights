-- truncate table data_category;
INSERT INTO datamarketplace.jhi_authority
(name, description, role_level)
select dc.`User Permissions`, dc.`User Permissions`, 0 from staging_airtable.stg_data_category dc where not exists(select 1 from datamarketplace.jhi_authority a where a.description = dc.`User Permissions`) and ifnull(`User Permissions`, '') <> '';

update data_category tc, staging_airtable.stg_data_category stg
		set tc.datacategory =stg.`Data Category`,
			tc.Explanation = stg.Explanation,
			tc.permission_name = case stg.`User Permissions`
																 when 'Registered'	then 'ROLE_USER'
																 when 'Bronze'  then 'ROLE_BRONZE'
																 when 'Silver'	 	then 'ROLE_SILVER'
																 when 'Admin'    then 'ROLE_ADMIN'
                                                                 when '' then null
																 else stg.`User Permissions`
																 end,
			tc.marketplace_status = stg.`Marketplace Status`,
			tc.Inactive = 0,
			tc.Created_Date = str_to_date(stg.`date created`, '%m/%d/%Y %h:%i %p'),
			tc.hashtag=stg.Hashtag
			where tc.record_id = stg.`Record ID`;

insert
	into
		data_category(
			ID,
			Record_ID,
			DataCategory,
			Explanation,
			Inactive,
			Created_Date
		)
	select
		1,
		'RECORD_NULL',
		'DATA_CATEGORY_NULL',
		'',
		0,
		current_timestamp from dual where not exists(select 1 from data_category where id = 1	);

insert
	into
		data_category(
			Record_ID,
			DataCategory,
			Explanation,
			permission_name,
			marketplace_status,
			inactive,
			created_date,
			hashtag
		) select
			`Record ID`,
			`Data Category`,
			Explanation,
			case `User Permissions`
				when 'Registered'	then 'ROLE_USER'
				when 'Bronze'       then 'ROLE_BRONZE'
				when 'Silver'	 	then 'ROLE_SILVER'
				when 'Admin'        then 'ROLE_ADMIN'
                when ''             then null
				else `User Permissions`
			end,
			`Marketplace Status`,
			0 inactive,
			str_to_date(`date created`, '%m/%d/%Y %h:%i %p') created_date,
			Hashtag
		from
			staging_airtable.stg_data_category sdc where ifnull(`Record ID`, '') <> '' and not exists(select 1 from data_category dc where dc.record_id = sdc.`Record ID`);

update data_category, (
    select @rownum := @rownum + 1 AS sort_order, a.*
    FROM (SELECT dc.`Record ID`, dc.`Data Category`
    FROM staging_airtable.stg_data_category dc
    ORDER BY dc.`Data Category`) a,
    (SELECT @rownum := 0) r) a
set data_category.sort_order = a.sort_order
where `Record ID` = record_id;
