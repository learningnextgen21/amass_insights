DELETE from data_email_labels
where not exists(select 1 from data_emails dp, mp_lookup_code lc where dp.id = data_email_labels.email_id and lc.id = data_email_labels.labels_id);


INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'LABELS' lookupmodule,
                    i.labels lookupcode,
                    i.labels description
               FROM staging_airtable.im_labels i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'LABELS'
                     AND c.lookupcode = a.lookupcode);
 
 INSERT INTO data_email_labels(labels_id, email_id)
   SELECT lc.id, dp.id
     FROM data_emails dp
          INNER JOIN staging_airtable.im_labels rac
             ON rac.emails_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'LABELS'
                AND lc.lookupcode = rac.labels
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_email_labels pa
               WHERE     pa.labels_id = lc.id
                     AND pa.email_id = dp.id);               