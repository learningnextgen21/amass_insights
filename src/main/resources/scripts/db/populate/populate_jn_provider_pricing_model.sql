delete from data_provider_pricing_model where not exists(select 1 from data_provider dp, mp_lookup_code lc where dp.id = data_provider_pricing_model.data_providers_id and lc.id = data_provider_pricing_model.pricing_models_id);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'PRICING_MODEL' lookupmodule,
                    i.pricing_model lookupcode,
                    i.pricing_model description
               FROM staging_airtable.im_provider_pricing_model i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'PRICING_MODEL'
                     AND c.lookupcode = lookupcode);

INSERT INTO data_provider_pricing_model(pricing_models_id, data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_pricing_model rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'PRICING_MODEL'
                AND lc.lookupcode = rac.pricing_model
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_pricing_model pa
               WHERE     pa.pricing_models_id = lc.id
                     AND pa.data_Providers_ID = dp.id);

create table temp_pricing_model_provider_count as select mlc.id, count(*) count from data_provider_pricing_model dppm inner join mp_lookup_code mlc on dppm.pricing_models_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_pricing_model_provider_count
set record_count = temp_pricing_model_provider_count.count
where temp_pricing_model_provider_count.id = mp_lookup_code.id;

drop table if exists temp_pricing_model_provider_count;
