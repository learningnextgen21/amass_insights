INSERT INTO data_event_comments(comment_id, comment_record_id, event_record_id,
                                  event_id,
                                  comment_type)
   SELECT DISTINCT p1.id comment_id, p1.record_id, p2.record_id, p2.id event_id, lc.id
        FROM staging_airtable.im_comment_event p
             INNER JOIN mp_lookup_code lc
                ON     lc.lookupmodule = 'COMMENT_EVENT_TYPE'
                   AND lc.lookupcode = 'Participant Events'
            INNER JOIN data_comments p1 ON p1.Record_ID = p.Comment_RecordID
            INNER JOIN data_event p2 ON p2.Record_ID = p.Event_RecordID
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_event_comments pp
               WHERE     pp.comment_id = p1.id
                     AND pp.event_id = p2.id
                     AND pp.comment_type = lc.id);

INSERT INTO data_event_comments(comment_id, comment_record_id, event_record_id,
                                  event_id,
                                  comment_type)
   SELECT DISTINCT p1.id comment_id, p1.record_id, p2.record_id, p2.id event_id, lc.id
        FROM staging_airtable.im_comment_relevent_event p
             INNER JOIN mp_lookup_code lc
                ON     lc.lookupmodule = 'COMMENT_EVENT_TYPE'
                   AND lc.lookupcode = 'Relevant Events'
            INNER JOIN data_comments p1 ON p1.Record_ID = p.Comment_RecordID
            INNER JOIN data_event p2 ON p2.Record_ID = p.Event_RecordID
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_event_comments pp
               WHERE     pp.comment_id = p1.id
                     AND pp.event_id = p2.id
                     AND pp.comment_type = lc.id);
