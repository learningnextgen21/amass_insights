truncate table data_article_purpose_type;
delete from data_article_purpose_type where not exists(select 1 from
    data_article da, data_resource_purpose drp
where da.id = data_article_purpose_type.articles_id
      and drp.id = data_article_purpose_type.purpose_types_id
);
INSERT INTO data_article_purpose_type(articles_id, purpose_types_id)
    SELECT DISTINCT  da.id, dt.id
    FROM staging_airtable.im_resource_purpose_type at
        INNER JOIN data_resource_purpose_type dt ON dt.record_id = at.PurposeType_RecordID
        inner join data_article da on da.record_id = at.Resource_RecordID
    WHERE NOT EXISTS
    (SELECT 1
     FROM data_article_purpose_type dat
     WHERE     dat.articles_id = da.id
               AND dat.purpose_types_id = dt.id);

create table temp_data_article_purpose_type_count as select drp.id, count(*) count from datamarketplace.data_article_purpose_type dap inner join datamarketplace.data_resource_purpose_type drp ON dap.purpose_types_id = drp.id
group by drp.id;

update data_resource_purpose_type, temp_data_article_purpose_type_count
set article_count = temp_data_article_purpose_type_count.count
where temp_data_article_purpose_type_count.id = data_resource_purpose_type.id;

drop table if exists temp_data_article_purpose_type_count;

update data_resource_purpose_type set article_count = 0 where article_count is null;
