-- TRUNCATE TABLE data_provider_feature;
DELETE from data_provider_feature where not exists(select 1 from data_provider dp, mp_lookup_code lc where dp.id = data_provider_feature.data_providers_id and lc.id = data_provider_feature.features_id);
INSERT INTO data_provider_feature(Features_ID, data_Providers_ID)
   SELECT DISTINCT df.id, dp.id
     FROM staging_airtable.im_provider_feature pf
          INNER JOIN data_provider dp ON dp.Record_ID = pf.Provider_RecordID
          INNER JOIN data_feature df ON df.record_id = pf.Feature
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_feature jpf
               WHERE     jpf.Features_ID = df.id
                     AND jpf.data_Providers_ID = dp.id);

create table temp_data_feature_provider_count as select df.id, count(*) count from data_provider_feature dpf inner join datamarketplace.data_feature df ON dpf.features_id = df.id
group by df.id;

update data_feature, temp_data_feature_provider_count
set provider_count = temp_data_feature_provider_count.count
where temp_data_feature_provider_count.id = data_feature.id;

drop table if exists temp_data_feature_provider_count;
