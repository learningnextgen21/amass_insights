DELETE from data_investor_typical_check_size
where not exists(select 1 from data_inv_managers dp, mp_lookup_code lc where dp.id = data_investor_typical_check_size.investor_managers_id and lc.id = data_investor_typical_check_size.typical_check_size_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'CHECK_SIZE' lookupmodule,
                    i.typical_check_size lookupcode,
                    i.typical_check_size description
               FROM staging_airtable.im_investor_typical_check_size i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'CHECK_SIZE'
                     AND c.lookupcode = a.lookupcode);
                 
INSERT INTO data_investor_typical_check_size(typical_check_size_id,
                                             investor_managers_id)
   SELECT lc.id, dp.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_typical_check_size rac
             ON rac.investment_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'CHECK_SIZE'
                AND lc.lookupcode = rac.typical_check_size
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_typical_check_size pa
               WHERE     pa.typical_check_size_id = lc.id
                     AND pa.investor_managers_id = dp.id);                     
                     