INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'OUTLIER_REASON' lookupmodule,
                    i.OutlierReasons lookupcode,
                    i.OutlierReasons description
               FROM staging_airtable.im_provider_outlier_reasons i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'OUTLIER_REASON'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_outlier_reasons(data_outlier_reasons_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_outlier_reasons rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'OUTLIER_REASON'
                AND lc.lookupcode = rac.OutlierReasons
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_outlier_reasons pa
               WHERE     pa.data_outlier_reasons_id = lc.id
                     AND pa.data_Providers_id = dp.id);
