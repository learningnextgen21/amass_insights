INSERT INTO data_organization_events(event_id, event_record_id, organization_record_id,
                                  organization_id,
                                  event_type)
   SELECT DISTINCT p1.id event_id, p1.record_id, p2.record_id, p2.id organization_id, lc.id
        FROM staging_airtable.im_event_organization p
             INNER JOIN mp_lookup_code lc
                ON     lc.lookupmodule = 'EVENT_ORGANIZATION_TYPE'
                   AND lc.lookupcode = 'Organization Organizer'
            INNER JOIN data_event p1 ON p1.Record_ID = p.Organization_RecordID
            INNER JOIN data_organization p2 ON p2.Record_ID = p.organization
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_organization_events pp
               WHERE     pp.event_id = p1.id
                     AND pp.organization_id = p2.id
                     AND pp.event_type = lc.id);

INSERT INTO data_organization_events(event_id, event_record_id, organization_record_id,
                                  organization_id,
                                  event_type)
   SELECT DISTINCT p1.id event_id, p1.record_id, p2.record_id, p2.id organization_id, lc.id
        FROM staging_airtable.im_event_sponsers_organization p
             INNER JOIN mp_lookup_code lc
                ON     lc.lookupmodule = 'EVENT_ORGANIZATION_TYPE'
                   AND lc.lookupcode = 'Sponsor Organizations'
            INNER JOIN data_event p1 ON p1.Record_ID = p.Sponser_Organization_RecordID
            INNER JOIN data_organization p2 ON p2.Record_ID = p.organization
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_organization_events pp
               WHERE     pp.event_id = p1.id
                     AND pp.organization_id = p2.id
                     AND pp.event_type = lc.id);
