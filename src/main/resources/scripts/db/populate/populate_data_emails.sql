update datamarketplace.data_emails de, staging_airtable.stg_data_emails sde
    set de.email_key = sde.`Email Key`,
                              de.resource_recordid=sde. `Resource - Record ID`,
                              de.from_name=sde.`From Name`,
                              de.from_email=sde.`From Email`,
                              de.from_email_address_recordid=sde.`From Email Address - Record ID`,
                              de.to_names=sde.`To Names`,
                              de.cc_names=sde.`CC Names`,
                              de.reply_to_name=sde.`Reply to Name`,
                              de.reply_to_email_address_recordid=sde.`Reply To Email Address - Record ID`,
                              de.date_formula=case when ifnull(`Date Formula`, '') <> '' then str_to_date(
                    `Date Formula`,
                     '%m/%d/%Y %h:%i%p') else null end,
                              de.subject=sde.`Subject`,
                              de.body_plain=sde.`Body Plain`,
                              de.body_html_length=sde.`Body HTML Length`,
                              de.html_file=sde.`HTML File`,
                              de.message_url=sde.`Message URL`,
                              de.file_location=sde.`File Location`,
                              de.notes=sde.`Notes`,
                               de.updated_date=case when ifnull(`Updated Date`, '') <> '' then str_to_date(
                               `Updated Date`,
                               '%m/%d/%Y') else null end,
              de.created_time=case when ifnull(`Created Time`, '') <> '' then str_to_date(
                    `Created Time`,
                     '%m/%d/%Y %h:%i%p') else null end,
                              de.record_id=sde.`Record ID`,
                              de.added_by=sde.`Added By`
        where record_id = `Record ID`;
        









INSERT INTO
 data_emails(
   email_key,
   resource_recordid,
   from_name,
   from_email,
   from_email_address_recordid,
   to_names,
   cc_names,
   reply_to_name,
   reply_to_email_address_recordid,
   date_formula,
   subject,
   body_plain,
   body_html_length,
   html_file,
   message_url,
   file_location,
   notes,
   updated_date,
   created_time,
   record_id,
   added_by
   )
  SELECT
   `Email Key`,
    `Resource - Record ID`,
    `From Name`,
    `From Email`,
     `From Email Address - Record ID`,
     `To Names`,
     `CC Names`,
     `Reply to Name`,
     `Reply To Email Address - Record ID`,
   case when ifnull(`Date Formula`, '') <> '' then str_to_date(
                    `Date Formula`,
                     '%m/%d/%Y %h:%i%p') else null end
                     AS date_formula,
   `Subject`,
   `Body Plain`,
   `Body HTML Length`,
   `HTML File`,
   `Message URL`,
   `File Location`,
   `Notes`,
    case when ifnull(`Updated Date`, '') <> '' then str_to_date(
                               `Updated Date`,
                               '%m/%d/%Y') else null end AS updated_date,
    case when ifnull(`Created Time`, '') <> '' then str_to_date(
                    `Created Time`,
                     '%m/%d/%Y %h:%i%p') else null end
                     AS created_time,                            
      `Record ID`,
      `Added By`
    
  FROM
   staging_airtable.stg_data_emails a where not exists (select 1 from data_emails da where da.record_id = a.`Record ID`);