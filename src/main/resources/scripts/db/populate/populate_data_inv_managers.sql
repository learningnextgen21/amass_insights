INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'DOMINANT_STYLE' lookupmodule,
                i.`Dominant Style` lookupcode,
                i.`Dominant Style` description
            FROM staging_airtable.stg_investment_managers i WHERE i.`Dominant Style`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'DOMINANT_STYLE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'ACTIVITY_LEVEL' lookupmodule,
                i.`Activity Level` lookupcode,
                i.`Activity Level` description
            FROM staging_airtable.stg_investment_managers i WHERE i.`Activity Level`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'ACTIVITY_LEVEL'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'SIDE' lookupmodule,
                i.`Side` lookupcode,
                i.`Side` description
            FROM staging_airtable.stg_investment_managers i WHERE i.`Side`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'SIDE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'AUM_RANGE' lookupmodule,
                i.`Total AUM Range (in millions)` lookupcode,
                i.`Total AUM Range (in millions)` description
            FROM staging_airtable.stg_investment_managers i WHERE i.`Total AUM Range (in millions)`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'AUM_RANGE'
               AND c.lookupcode = a.lookupcode);
               
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'CURRENCY' lookupmodule,
                i.`Total AUM Currency` lookupcode,
                i.`Total AUM Currency` description
            FROM staging_airtable.stg_investment_managers i WHERE i.`Total AUM Currency`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'CURRENCY'
               AND c.lookupcode = a.lookupcode);               
   
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'INVESTMENT_MANAGER_SUBTYPE' lookupmodule,
                i.`Primary Subtype` lookupcode,
                i.`Primary Subtype` description
            FROM staging_airtable.stg_investment_managers i WHERE i.`Primary Subtype`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'INVESTMENT_MANAGER_SUBTYPE'
               AND c.lookupcode = a.lookupcode);               
 
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'TYPES' lookupmodule,
                i.`Primary Type` lookupcode,
                i.`Primary Type` description
            FROM staging_airtable.stg_investment_managers i WHERE i.`Primary Type`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'TYPES'
               AND c.lookupcode = a.lookupcode);                 
               
update staging_airtable.stg_investment_managers set `Total AUM (in millions)`=replace(`Total AUM (in millions)`,'$',''); 
update staging_airtable.stg_investment_managers set `Total AUM (in millions)`=replace(`Total AUM (in millions)`,',','');                              
update staging_airtable.stg_investment_managers set `Total AUM (in millions)`=0 where `Total AUM (in millions)`is null or `Total AUM (in millions)`='';
               
update data_inv_managers di, staging_airtable.stg_investment_managers si
  set di.name = si.`Name`,
  last_updated_date = case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(`Last Updated Date`,  '%m/%d/%Y') else null end,
  created_date = case when ifnull(`Created Date`, '') <> '' then str_to_date(`Created Date`,  '%m/%d/%Y') else null end,
  di.notes = si.`Notes`,
  di.head_quarters_location = si.`Headquarters Location Text`,
  total_aum_in_millions = `Total AUM (in millions)`,
  first_raised_date = case when ifnull(`First Raised Date`, '') <> '' then str_to_date(`First Raised Date`,  '%m/%d/%Y') else null END,
  first_raised_year = `First Raised Year`,
  number_of_funds = `Number of Funds`,
  dominant_style_id = (select id from mp_lookup_code where lookupmodule = 'DOMINANT_STYLE' and description = `Dominant Style`),
  activity_level_id = (select id from mp_lookup_code where lookupmodule = 'ACTIVITY_LEVEL' and description = `Activity Level`),
  user_permission = case `User Permissions`
				when 'Registered'	then 'ROLE_USER'
				when 'Bronze' then 'ROLE_BRONZE'
				when 'Silver'	then 'ROLE_SILVER'
				when 'Admin'  then 'ROLE_ADMIN'
        when ''       then null
				else `User Permissions`
      END,
  side_id = (select id from mp_lookup_code where lookupmodule = 'SIDE' and description = `Side`),
  primary_type_id = (select id from mp_lookup_code where lookupmodule = 'TYPES' and description = `Primary Type`),
  primary_subtype_id = (select id from mp_lookup_code where lookupmodule = 'INVESTMENT_MANAGER_SUBTYPE' and description = `Primary Subtype`),
  main_research_style_id = (select id from mp_lookup_code where lookupmodule = 'INVESTMENT_MANAGER_RESEARCH_STYLE' and description = `Main Research Style`),
  total_aum_range_id = (select id from mp_lookup_code where lookupmodule = 'AUM_RANGE' and description = `Total AUM Range (in millions)`),
  total_aum_currency_id = (select id from mp_lookup_code where lookupmodule = 'CURRENCY' and description = `Total AUM Currency`),
  exclusions_text = `Exclusions Text`,
  portfolio_companies_text = `Portfolio Companies Text` 
  where record_id = `Record ID`;



INSERT INTO
 data_inv_managers(
   name,
   last_updated_date,
   created_date,
   record_id,
   head_quarters_location,
   notes,
   total_aum_in_millions,
   first_raised_date,
   first_raised_year,
   number_of_funds,
   dominant_style_id,
   activity_level_id,
   user_permission,
   side_id,
   primary_type_id,
   primary_subtype_id,
   main_research_style_id,
   total_aum_range_id,
   total_aum_currency_id,
   exclusions_text,
   portfolio_companies_text
   )
  SELECT
   `Name`,
    case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(`Last Updated Date`,  '%m/%d/%Y') else null end last_updated,
    case when ifnull(`Created Date`, '') <> '' then str_to_date(`Created Date`,  '%m/%d/%Y') else null end created_date,
    `Record ID`,
    `Headquarters Location Text`,
    `Notes`,
    `Total AUM (in millions)`,
    case when ifnull(`First Raised Date`, '') <> '' then str_to_date(`First Raised Date`,  '%m/%d/%Y') else null end first_raised_date,
     `First Raised Year`,
     `Number of Funds`,
     (select id from mp_lookup_code where lookupmodule = 'DOMINANT_STYLE' and description = `Dominant Style`),
     	(select id from mp_lookup_code where lookupmodule = 'ACTIVITY_LEVEL' and description = `Activity Level`),
     case `User Permissions`
				when 'Registered'	then 'ROLE_USER'
				when 'Bronze' then 'ROLE_BRONZE'
				when 'Silver'	then 'ROLE_SILVER'
				when 'Admin'  then 'ROLE_ADMIN'
        when ''       then null
				else `User Permissions`
      end as permission_name,
      (select id from mp_lookup_code where lookupmodule = 'SIDE' and description = `Side`),
      (select id from mp_lookup_code where lookupmodule = 'TYPES' and description = `Primary Type`),
      (select id from mp_lookup_code where lookupmodule = 'INVESTMENT_MANAGER_SUBTYPE' and description = `Primary Subtype`),
      (select id from mp_lookup_code where lookupmodule = 'INVESTMENT_MANAGER_RESEARCH_STYLE' and description = `Main Research Style`),
      (select id from mp_lookup_code where lookupmodule = 'AUM_RANGE' and description = `Total AUM Range (in millions)`),
      (select id from mp_lookup_code where lookupmodule = 'CURRENCY' and description = `Total AUM Currency`),
      `Exclusions Text`,
      `Portfolio Companies Text`
    
  FROM
   staging_airtable.stg_investment_managers a where not exists (select 1 from data_inv_managers da where da.record_id = a.`Record ID`);