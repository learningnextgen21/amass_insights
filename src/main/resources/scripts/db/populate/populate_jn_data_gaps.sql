INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'DATA_GAPS_REASON' lookupmodule,
                    i.DataGapsReasons lookupcode,
                    i.DataGapsReasons description
               FROM staging_airtable.im_provider_data_gaps_reasons i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'DATA_GAPS_REASON'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_provider_gaps_reason(data_gaps_reason_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_data_gaps_reasons rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'DATA_GAPS_REASON'
                AND lc.lookupcode = rac.DataGapsReasons
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_gaps_reason pa
               WHERE     pa.data_gaps_reason_id = lc.id
                     AND pa.data_Providers_id = dp.id);
