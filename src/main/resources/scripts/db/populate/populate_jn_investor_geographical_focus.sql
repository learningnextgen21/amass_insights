DELETE from data_investor_geographical_focus
where not exists(select 1 from data_inv_managers dp, mp_lookup_code lc where dp.id = data_investor_geographical_focus.investor_managers_id and lc.id = data_investor_geographical_focus.geographical_foci_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'INVESTOR_GEOGRAPHICAL_FOCUS' lookupmodule,
                    i.geographical_focus lookupcode,
                    i.geographical_focus description
               FROM staging_airtable.im_investor_geographical_focus i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'INVESTOR_GEOGRAPHICAL_FOCUS'
                     AND c.lookupcode = a.lookupcode);
                     
INSERT INTO data_investor_geographical_focus(geographical_foci_id,
                                             investor_managers_id)
   SELECT lc.id, dp.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_geographical_focus rac
             ON rac.investment_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'INVESTOR_GEOGRAPHICAL_FOCUS'
                AND lc.lookupcode = rac.geographical_focus
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_geographical_focus pa
               WHERE     pa.geographical_foci_id = lc.id
                     AND pa.investor_managers_id = dp.id);
                     