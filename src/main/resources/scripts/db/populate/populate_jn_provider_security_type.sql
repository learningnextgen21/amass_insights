delete from data_provider_security_type where not exists(select 1 from data_provider dp, mp_lookup_code lc where dp.id = data_provider_security_type.data_providers_id and lc.id = data_provider_security_type.security_types_id);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'RLVNT_SECURITY_TYPE' lookupmodule,
                    i.security_type lookupcode,
                    i.security_type description
               FROM staging_airtable.im_relevant_security_type i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'RLVNT_SECURITY_TYPE'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_provider_security_type(security_types_id, data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_relevant_security_type rit
             ON rit.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'RLVNT_SECURITY_TYPE'
                AND lc.lookupcode = rit.security_type
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_security_type pa
               WHERE     pa.security_types_id = lc.id
                     AND pa.data_Providers_ID = dp.id);

create table temp_security_type_provider_count as select mlc.id, count(*) count from data_provider_security_type dpst inner join mp_lookup_code mlc on dpst.security_types_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_security_type_provider_count
set record_count = temp_security_type_provider_count.count
where temp_security_type_provider_count.id = mp_lookup_code.id;

drop table if exists temp_security_type_provider_count;
