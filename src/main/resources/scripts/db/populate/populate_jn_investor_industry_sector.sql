DELETE from data_investor_industry_sector
where not exists(select 1 from data_inv_managers dp, mp_lookup_code lc where dp.id = data_investor_industry_sector.investor_managers_id and lc.id = data_investor_industry_sector.industry_sector_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'INDUSTRY_SECTOR' lookupmodule,
                    i.industry_sector lookupcode,
                    i.industry_sector description
               FROM staging_airtable.im_investor_industry_sector i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'INDUSTRY_SECTOR'
                     AND c.lookupcode = a.lookupcode);

                     
INSERT INTO data_investor_industry_sector(industry_sector_id,
                                             investor_managers_id)
   SELECT lc.id, dp.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_industry_sector rac
             ON rac.investment_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'INDUSTRY_SECTOR'
                AND lc.lookupcode = rac.industry_sector
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_industry_sector pa
               WHERE     pa.industry_sector_id = lc.id
                     AND pa.investor_managers_id = dp.id);