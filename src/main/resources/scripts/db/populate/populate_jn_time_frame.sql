INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'INVESTING_TIME_FRAME' lookupmodule,
                    i.TimeFrame lookupcode,
                    i.TimeFrame description
               FROM staging_airtable.im_provider_time_frame i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'INVESTING_TIME_FRAME'
                     AND c.lookupcode = a.lookupcode);
INSERT INTO data_provider_time_frame(data_provider_time_frame_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_time_frame rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'INVESTING_TIME_FRAME'
                AND lc.lookupcode = rac.TimeFrame
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_time_frame pa
               WHERE     pa.data_provider_time_frame_id = lc.id
                     AND pa.data_Providers_id = dp.id);
