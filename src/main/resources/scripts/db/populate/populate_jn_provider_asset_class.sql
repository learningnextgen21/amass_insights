DELETE from data_provider_asset_class
where not exists(select 1 from data_provider dp, mp_lookup_code lc where dp.id = data_provider_asset_class.data_providers_id and lc.id = data_provider_asset_class.asset_classes_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'ASSET_CLASS' lookupmodule,
                    i.asset_class lookupcode,
                    i.asset_class description
               FROM staging_airtable.im_relevant_asset_class i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'ASSET_CLASS'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_provider_asset_class(asset_classes_id, data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_relevant_asset_class rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'ASSET_CLASS'
                AND lc.lookupcode = rac.asset_class
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_asset_class pa
               WHERE     pa.asset_classes_id = lc.id
                     AND pa.data_Providers_ID = dp.id);

create table temp_asset_class_provider_count as select mlc.id, count(*) count from data_provider_asset_class dpac inner join mp_lookup_code mlc on dpac.asset_classes_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_asset_class_provider_count
set record_count = temp_asset_class_provider_count.count
where temp_asset_class_provider_count.id = mp_lookup_code.id;

drop table if exists temp_asset_class_provider_count;
