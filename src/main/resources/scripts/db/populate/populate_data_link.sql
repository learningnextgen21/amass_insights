update data_links, staging_airtable.stg_data_links
		SET data_links.link = stg_data_links.`Link`,
			data_links.inactive = 0,
			created_time = str_to_date(`Created Time`, '%m/%d/%Y %h:%i %p'),
			resource_name_formula=`Resource Name Formula`
					where record_id = `Record ID`;
insert
	into
		data_links(
			Record_ID,
			link,
			inactive,
			created_time,
			resource_name_formula


		) select
			`Record ID`,
			`Link`,
			0 inactive,
			str_to_date(`Created Time`, '%m/%d/%Y %h:%i %p') created_time,
			`Resource Name Formula`
		from
			staging_airtable.stg_data_links sdl where not exists(select 1 from data_links dl where dl.record_id = sdl.`Record ID`);

update data_links, (
                  select @rownum := @rownum + 1 AS sort_order, a.*
                  FROM (SELECT sdl.`Record ID`, sdl.`link`
                        FROM staging_airtable.stg_data_links sdl
                        ORDER BY sdl.`Link`) a,
                      (SELECT @rownum := 0) r) a
set data_links.sort_order = a.sort_order
where `Record ID` = record_id;
