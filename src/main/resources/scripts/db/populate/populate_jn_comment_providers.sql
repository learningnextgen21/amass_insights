delete from data_provider_comments where not exists(select 1 from data_provider dp, data_comments comments,
  mp_lookup_code lc where dp.record_id = data_provider_comments.data_providers_record_id and lc.id = data_provider_comments.comment_type
and data_provider_comments.comment_id = comments.id);

delete from data_provider_comments where data_providers_record_id is null and comment_record_id is null;

INSERT INTO data_provider_comments(comment_id, comment_record_id, data_providers_record_id,
                                  data_providers_id,
                                  comment_type)
   SELECT DISTINCT p1.id comment_id, p1.record_id, p2.record_id, p2.id data_providers_id, lc.id
        FROM staging_airtable.im_comment_provider p
             INNER JOIN mp_lookup_code lc
                ON     lc.lookupmodule = 'PROVIDER_TYPE'
                   AND lc.lookupcode = 'Participant Data Providers'
            INNER JOIN data_comments p1 ON p1.Record_ID = p.Comment_RecordID
            INNER JOIN data_provider p2 ON p2.Record_ID = p.Provider_RecordID
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_provider_comments pp
               WHERE     pp.comment_id = p1.id
                     AND pp.data_providers_id = p2.id
                     AND pp.comment_type = lc.id);

INSERT INTO data_provider_comments(comment_id, comment_record_id, data_providers_record_id,
                                  data_providers_id,
                                  comment_type)
   SELECT DISTINCT p1.id comment_id, p1.record_id, p2.record_id, p2.id data_providers_id, lc.id
        FROM staging_airtable.im_comment_relevent_provider p
             INNER JOIN mp_lookup_code lc
                ON     lc.lookupmodule = 'PROVIDER_TYPE'
                   AND lc.lookupcode = 'Relevant Data Providers'
            INNER JOIN data_comments p1 ON p1.Record_ID = p.Comment_RecordID
            INNER JOIN data_provider p2 ON p2.Record_ID = p.Provider_RecordID
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_provider_comments pp
               WHERE     pp.comment_id = p1.id
                     AND pp.data_providers_id = p2.id
                     AND pp.comment_type = lc.id);



