delete from data_provider_source where not exists(select 1 from data_provider dp, data_source ds where dp.id = data_provider_source.data_providers_id and ds.id = data_provider_source.sources_id);
INSERT INTO data_provider_source(data_providers_id, sources_id)
   SELECT DISTINCT p.ID, d.ID
     FROM staging_airtable.im_source_providers ps
          INNER JOIN data_provider p ON ps.Provider_RecordID = p.record_id
          INNER JOIN data_source d ON d.Record_ID = ps.Source_RecordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_source jsp
               WHERE jsp.data_providers_id = p.ID AND jsp.sources_id = d.ID);

create table temp_data_source_provider_count as select ds.id, count(*) count from datamarketplace.data_provider_source dps inner join datamarketplace.data_source ds ON dps.sources_id = ds.id
group by ds.id;

update data_source, temp_data_source_provider_count
set provider_count = temp_data_source_provider_count.count
where temp_data_source_provider_count.id = data_source.id;

drop table if exists temp_data_source_provider_count;
