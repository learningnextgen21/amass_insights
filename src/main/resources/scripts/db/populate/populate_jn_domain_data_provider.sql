DELETE from data_provider_domain
where not exists(select 1 from data_domains dp, data_provider lc where dp.id = data_provider_domain.domain_id and lc.id = data_provider_domain.provider_id);

	INSERT INTO data_provider_domain(provider_id, domain_id)
	   SELECT DISTINCT df.id, dp.id
		 FROM staging_airtable.im_domain_data_provider pf
			  INNER JOIN data_domains dp ON dp.record_id = pf.domains_recordID
			  INNER JOIN data_provider df ON df.record_id = pf.data_provider_recordID
		WHERE NOT EXISTS
				 (SELECT 1
					FROM data_provider_domain jpf
				   WHERE     jpf.provider_id = df.id
						 AND jpf.domain_id = dp.id);