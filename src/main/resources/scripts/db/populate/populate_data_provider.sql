INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'LICENSE_TYPE' lookupmodule,
                i.`Data License Type` lookupcode,
                i.`Data License Type` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Data License Type`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'LICENSE_TYPE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'VERIFICATION_STATUS' lookupmodule,
                i.`Profile Verification Status` lookupcode,
                i.`Profile Verification Status` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Profile Verification Status`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'VERIFICATION_STATUS'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'COVERAGE_RANGE' lookupmodule,
                i.`Public Equities Covered Range` lookupcode,
                i.`Public Equities Covered Range` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Public Equities Covered Range`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'COVERAGE_RANGE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'AGREEMENT_TYPE' lookupmodule,
                i.`Trial Agreement Type` lookupcode,
                i.`Trial Agreement Type` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Trial Agreement Type`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'AGREEMENT_TYPE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'DATA_SIZE_RANGE' lookupmodule,
                i.`Dataset Size` lookupcode,
                i.`Dataset Size` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Dataset Size`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'DATA_SIZE_RANGE'
               AND c.lookupcode = a.lookupcode);

 INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'ROWS_RANGE' lookupmodule,
                i.`Dataset Number of Rows` lookupcode,
                i.`Dataset Number of Rows` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Dataset Number of Rows`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'ROWS_RANGE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'YES_OR_NO_SOFT' lookupmodule,
                i.`Point-in-Time Accuracy` lookupcode,
                i.`Point-in-Time Accuracy` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Point-in-Time Accuracy`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'YES_OR_NO_SOFT'
               AND c.lookupcode = a.lookupcode);


INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'YES_OR_NO' lookupmodule,
                i.`SEC-Regulated` lookupcode,
                i.`SEC-Regulated` description
            FROM staging_airtable.stg_data_provider i WHERE i.`SEC-Regulated`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'YES_OR_NO'
               AND c.lookupcode = a.lookupcode);


INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'YES_NO_UNKNOWN' lookupmodule,
                i.`Duplicates Cleaned` lookupcode,
                i.`Duplicates Cleaned` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Duplicates Cleaned`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'YES_NO_UNKNOWN'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'YES_NO_UNKNOWN' lookupmodule,
                i.`Normalized` lookupcode,
                i.`Normalized` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Normalized`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'YES_NO_UNKNOWN'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'YES_NO_UNKNOWN' lookupmodule,
                i.`Includes Outliers` lookupcode,
                i.`Includes Outliers` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Includes Outliers`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'YES_NO_UNKNOWN'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'YES_NO_UNKNOWN' lookupmodule,
                i.`Data Gaps` lookupcode,
                i.`Data Gaps` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Data Gaps`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'YES_NO_UNKNOWN'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'CLIENT_RANGE' lookupmodule,
                i.`Investor Client Bucket` lookupcode,
                i.`Investor Client Bucket` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Investor Client Bucket`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'CLIENT_RANGE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'MONTHLY_PRICE_RANGE' lookupmodule,
                i.`Monthly Price Range` lookupcode,
                i.`Monthly Price Range` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Monthly Price Range`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'MONTHLY_PRICE_RANGE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'SUBSCRIPTION_MODEL' lookupmodule,
                i.`Subscription Model` lookupcode,
                i.`Subscription Model` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Subscription Model`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'SUBSCRIPTION_MODEL'
               AND c.lookupcode = a.lookupcode);

 INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'TIME_PERIOD' lookupmodule,
                i.`Subscription Time Period` lookupcode,
                i.`Subscription Time Period` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Subscription Time Period`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'TIME_PERIOD'
               AND c.lookupcode = a.lookupcode);


INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'DATA_SOURCES_RANGE' lookupmodule,
                i.`Number of Data Sources` lookupcode,
                i.`Number of Data Sources` description
            FROM staging_airtable.stg_data_provider i WHERE i.`Number of Data Sources`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'DATA_SOURCES_RANGE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (SELECT DISTINCT
              'ASSET_CLASS' lookupmodule,
              case when ifnull(trim(i.`main asset class`), '') = '' then 'DEFAULT_NULL'
              else trim(ifnull(i.`main asset class`, '')) end
                            lookupcode,
              case when ifnull(trim(i.`main asset class`), '') = '' then 'DEFAULT_NULL'
              else trim(ifnull(i.`main asset class`, '')) end description
          FROM staging_airtable.stg_data_provider i
          ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'ASSET_CLASS'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'OVERALL_SCORE' lookupmodule,
                i.`Overall Score` lookupcode,
                i.`Overall Score` description
            FROM staging_airtable.stg_data_provider i
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'OVERALL_SCORE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (SELECT DISTINCT
                'READINESS_SCORE' lookupmodule,
                i.`Readiness Score` lookupcode,
                i.`Readiness Score` description
            FROM staging_airtable.stg_data_provider i
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'READINESS_SCORE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (SELECT DISTINCT
              'VALUE_SCORE' lookupmodule,
              i.`value Score` lookupcode,
              i.`value Score` description
          FROM staging_airtable.stg_data_provider i
          ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'VALUE_SCORE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (SELECT DISTINCT
              'UNIQUENESS_SCORE' lookupmodule,
              i.`Uniqueness Score` lookupcode,
              i.`Uniqueness Score` description
          FROM staging_airtable.stg_data_provider i
          ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'UNIQUENESS_SCORE'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (SELECT DISTINCT
              'INVESTOR_WILLINGNESS_SCORE' lookupmodule,
              i.`Willingness to Provide to Investors Score` lookupcode,
              i.`Willingness to Provide to Investors Score` description
          FROM staging_airtable.stg_data_provider i
          ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'INVESTOR_WILLINGNESS_SCORE'
               AND c.lookupcode = a.lookupcode);



               INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (SELECT DISTINCT
              'IEI_STATUS' lookupmodule,
              i.`IEI Status` lookupcode,
              i.`IEI Status` description
          FROM staging_airtable.stg_data_provider i WHERE i.`IEI Status`!=''
          ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'IEI_STATUS'
               AND c.lookupcode = a.lookupcode);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (SELECT DISTINCT
              'MARKETPLACE_STATUS' lookupmodule,
              i.`IEI Status` lookupcode,
              i.`IEI Status` description
          FROM staging_airtable.stg_data_provider i WHERE i.`Marketplace Status`!=''
          ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'MARKETPLACE_STATUS'
               AND c.lookupcode = a.lookupcode);


               INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (SELECT DISTINCT
              'PRIORITY' lookupmodule,
              i.Priority lookupcode,
              i.Priority description
          FROM staging_airtable.stg_data_provider i WHERE i.Priority!=''
          ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'PRIORITY'
               AND c.lookupcode = a.lookupcode);




INSERT INTO data_provider (
    id,
    record_id,
    provider_name,
    website,
    short_description,
    long_description,
    notes,
    live,
    owner_organization_id,
    main_data_category_id,
    main_data_industry_id,
    created_date)
    select 1,
        'RECORD_NULL',
        'PROVIDER_NULL',
        'WEBSITE_NULL',
        'DESCRIPTION_NULL',
        'DESCRIPTION_NULL',
        'NOTES_NULL',
        0,
        1,
        1,
        1,
        current_date()
    from dual
    where not exists (select 1 from data_provider where id = 1);

INSERT INTO jhi_authority
(name, description, role_level)
    select dp.`User Permissions`, dp.`User Permissions`, 0 from staging_airtable.stg_data_provider dp where not exists(select 1 from datamarketplace.jhi_authority a where a.description = dp.`User Permissions`) and ifnull(`User Permissions`, '') <> '';

update data_provider, staging_airtable.stg_data_provider
    set provider_name = `Provider Name`,
    website = `Provider Website`,
    short_description = `Short Description`,
    long_description = `Long Description`,
    score_overall_id = (select id from mp_lookup_code where lookupmodule = 'OVERALL_SCORE' and description = `Overall Score`),
    score_readiness_id = (select id from mp_lookup_code where lookupmodule = 'READINESS_SCORE' and description = `Readiness Score`),
    score_value_id = (select id from mp_lookup_code where lookupmodule = 'VALUE_SCORE' and description = `value Score`),
    score_uniqueness_id = (select id from mp_lookup_code where lookupmodule = 'UNIQUENESS_SCORE' and description = `Uniqueness Score`),
    score_investors_willingness_id = (select id from mp_lookup_code where lookupmodule = 'INVESTOR_WILLINGNESS_SCORE' and description = `Willingness to Provide to Investors Score`),
	sec_regulated_id=(select id from mp_lookup_code where lookupmodule = 'YES_OR_NO' and description = `SEC-Regulated`),
          profile_verification_status_id = (select id from mp_lookup_code where lookupmodule = 'VERIFICATION_STATUS' and description = `Profile Verification Status`),
          public_equities_covered_count = case when ifnull(`Public Equities Covered Count`, '') <> '' then
                    CAST(`Public Equities Covered Count` as UNSIGNED) else null END,
     investor_clients_count=case when ifnull(`Investor Clients Count`, '') <> '' then
                    CAST(`Investor Clients Count` as UNSIGNED) else null END,
  trial_pricing_model_id=(select id from mp_lookup_code where lookupmodule = 'PRICING_MODEL' and description = `Trial Pricing Model`),
  trial_duration=`Trial Duration`,
  trial_agreement_type_id=(select id from mp_lookup_code where lookupmodule = 'AGREEMENT_TYPE' and description = `Trial Agreement Type`),
  trial_pricing_details=`Trial Pricing Details`,
  lag_time=`Lag Time`,
  competitive_differentiators=`Competitive Differentiators`,
  discussion_analytics=`Discussion - Analytics`,
  discussion_cleanliness=`Discussion - Cleanliness`,
  sample_or_panel_biases=`Sample or Panel Biases`,
  number_of_analyst_employees = case when ifnull(`Number of Analyst Employees`, '') <> '' then
                    CAST(`Number of Analyst Employees` as UNSIGNED) else null END,
  point_in_time_accuracy_id=(select id from mp_lookup_code where lookupmodule = 'YES_OR_NO_SOFT' and description = `Point-in-Time Accuracy`),
  data_sources_details=`Data Sources Details`,
  data_gaps_id=(select id from mp_lookup_code where lookupmodule = 'YES_NO_UNKNOWN' and description = `Data Gaps`),
  normalized_id=(select id from mp_lookup_code where lookupmodule = 'YES_NO_UNKNOWN' and description = `Normalized`),
  duplicates_cleaned_id=(select id from mp_lookup_code where lookupmodule = 'YES_NO_UNKNOWN' and description = `Duplicates Cleaned`),
  biases_id=(select id from mp_lookup_code where lookupmodule = 'BIASES' and description = `Biases`),
  pricing_details=`Pricing Details`,
  subscription_time_period_id=(select id from mp_lookup_code where lookupmodule = 'TIME_PERIOD' and description = `Subscription Time Period`),
  year_first_data_product_launched=case when ifnull(`Year First Data Product Launched`, '') <> '' then
                    CAST(`Year First Data Product Launched` as UNSIGNED) else null END,
  compliance_score_personal=`Compliance Score - Personal Information`,
  compliance_score_public=`Compliance Score - Material Non-Public Information`,
  monthly_price_range_id=(select id from mp_lookup_code where lookupmodule = 'MONTHLY_PRICE_RANGE' and description = `Monthly Price Range`),
  subscription_model_id=(select id from mp_lookup_code where lookupmodule = 'SUBSCRIPTION_MODEL' and description = `Subscription Model`),
  minimum_yearly_price=`Minimum Yearly Price`,
  maximum_yearly_price=`Maximum Yearly Price`,
  number_of_data_sources_id=(select id from mp_lookup_code where lookupmodule = 'DATA_SOURCES_RANGE' and description = `Number of Data Sources`),
  data_provider.privacy=stg_data_provider.Privacy,
    data_provider.notes = stg_data_provider.Notes,
    delivery_frequency_notes = `Delivery Frequency Notes`,
      live = 0,
      created_date = str_to_date(
          `Created Date`,
          '%m/%d/%Y'),
      updated_date = case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(
          `Last Updated Date`,
          '%m/%d/%Y') else null end,
      data_update_frequency = `Data Update Frequency`,
      data_update_frequency_notes = `Data Update Frequency Notes`,
      date_collection_began =case when ifnull(`Date Collection Began`, '') <> '' then str_to_date(
          `Date Collection Began`,
          '%m/%d/%Y') else null end,
      date_collection_range_explanation = `Date Collection Range Explanation`,
      collection_methods_explanation = `Collection Methods Explanation`,
      potential_drawbacks = `Potential Drawbacks`,
      unique_value_props = `Unique Value Props`,
      historical_date_range = `Historical Date Range`,
      key_data_fields = `Key Data Fields`,
      data_provider.summary = stg_data_provider.Summary,
      investor_clients = `# of Investor Clients`,
      historical_date_range_estimate = `Historical Date Range Estimate`,
      public_companies_covered = `Public Companies Covered`,
      product_details = `Product Details`,
      org_relationship_status = `orgs - relationship statuses`,
      data_provider.completeness = ifnull(replace(stg_data_provider.Completeness, '%', ''), 0),
        Sample_or_Panel_Size = `Sample or Panel Size`,
        Use_Cases_or_Questions_Addressed = `Use Cases or Questions Addressed`,
        data_license_type_id = (select id from mp_lookup_code where lookupmodule = 'LICENSE_TYPE' and description = `Data License Type`),
        Data_License_Type_Details =   `Data License Type Details`,
        Legal_and_Compliance_Issues =  `Legal & Compliance Issues`,
        Data_Roadmap = `Data Roadmap`,
        permission_name = case `User Permissions`
                   when 'Registered'	then 'ROLE_USER'
                   when 'Bronze'        then 'ROLE_BRONZE'
                   when 'Silver' 	    then 'ROLE_SILVER'
                   when 'Admin'         then 'ROLE_ADMIN'
                   when ''              then null
                   else `User Permissions`
                   end,
     `inactive` = case when `Inactive?` = 'checked' then 1 else 0 end,
      free_trial_available = case when `Free Trial Available` = 'checked' then 1 else 0 end,
      main_data_category_id = ifnull(
          (select
             dc.id
           from
             data_category dc
           where
             dc.record_id = `Main Data Category - Record ID`),
          1),
      owner_organization_id = ifnull(
          (select
             id
           from
             data_organization do
           where
             do.record_id  = `Owner Organization - Record ID`
           order by
             1
           limit
             1),
          1),
      main_data_industry_id = ifnull(
          (select
             di.id
           from
             data_industry di
           where
             di.record_id = `Main Data Industry - Record ID`),
          1),
      provider_type_id = ifnull(
          (select
             id
           from
             data_provider_type pt
           where
             pt.record_id = `Provider Type - Record ID`),
          1),
  iei_status_id=(select id from mp_lookup_code where lookupmodule = 'IEI_STATUS' and description = `IEI Status`),
  marketplace_status_id=(select id from mp_lookup_code where lookupmodule = 'MARKETPLACE_STATUS' and description = `Marketplace Status`),
  planned_research_date=case when ifnull(`Planned Research Date`, '') <> '' then str_to_date(
     `Planned Research Date`,
     '%m/%d/%Y') else null END,
  main_asset_class=`Main Asset Class`,
  dataset_size=`Dataset Size`,
  investor_client_bucket=`Investor Client Bucket`,
  public_equities_covered_range=`Public Equities Covered Range`,
  dataset_number_of_rows=`Dataset Number of Rows`,
  data_provider.priority=stg_data_provider.Priority
          where record_id = `Record ID` AND web_updated_date IS NULL;

insert into
		data_provider(
			ID,
			Record_ID,
			provider_name,
			Website,
			Short_Description,
			Long_Description,
			Provider_Type_ID,
			Main_Data_Industry_ID,
			Main_Data_Category_ID,
			Notes,
			Live,
			Created_Date
		)
	select
		1,
		'PROVIDER_NULL',
		'RECORD_NULL',
		'WEBSITE_NULL',
		'DESCRIPTION_NULL',
		'DESCRIPTION_NULL',
		1,
		1,
		1,
		'DEFAULT_NULL',
		1,
		current_date
	from dual where not exists (select 1 from data_provider where id = 1);

INSERT INTO
 data_provider(
   record_id,
   provider_name,
   website,
   short_description,
   long_description,
   notes,
   live,
   created_date,
   updated_date,
   data_update_frequency,
   data_update_frequency_notes,
   delivery_frequency_notes,
   date_collection_began,
   date_collection_range_explanation,
   collection_methods_explanation,
   potential_drawbacks,
   unique_value_props,
   historical_date_range,
   key_data_fields,
   summary,
   investor_clients,
   historical_date_range_estimate,
   public_companies_covered,
   product_details,
   permission_name,
   completeness,
   Sample_or_Panel_Size,
   Use_Cases_or_Questions_Addressed,
   data_license_type_id,
   Data_License_Type_Details,
   Legal_and_Compliance_Issues,
   Data_Roadmap,
   inactive,
   free_trial_available,
   main_data_category_id,
   owner_organization_id,
   main_data_industry_id,
   provider_type_id,
   score_overall_id,
   score_readiness_id,
   score_value_id,
   score_uniqueness_id,
   score_investors_willingness_id,
   sec_regulated_id,
  profile_verification_status_id,
  public_equities_covered_count,
  investor_clients_count,
  trial_pricing_model_id,
  trial_duration,
  trial_agreement_type_id,
  trial_pricing_details,
  lag_time,
  competitive_differentiators,
  discussion_analytics,
  discussion_cleanliness,
  sample_or_panel_biases,
  number_of_analyst_employees,
  point_in_time_accuracy_id,
  data_sources_details,
  data_gaps_id,
  includes_outliers_id,
  normalized_id,
  duplicates_cleaned_id,
  biases_id,
  pricing_details,
  subscription_time_period_id,
  year_first_data_product_launched,
  compliance_score_personal,
  compliance_score_public,
  monthly_price_range_id,
  subscription_model_id,
  minimum_yearly_price,
  maximum_yearly_price,
  number_of_data_sources_id,
  privacy,
  iei_status_id,
  marketplace_status_id,
  planned_research_date,
  main_asset_class,
  dataset_size,
  investor_client_bucket,
  public_equities_covered_range,
  dataset_number_of_rows,
  priority
  )
  SELECT
   `Record ID`
     AS record_id,
   `Provider Name`
     AS provider_name,
   `Provider Website`
     AS website,
   `Short Description`
     AS short_description,
   `Long Description`
     AS long_description,
   Notes
     AS notes,
   0
     AS live,
   str_to_date(
     `Created Date`,
     '%m/%d/%Y')
     AS created_date,
   case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(
     `Last Updated Date`,
     '%m/%d/%Y') else null end
     AS updated_date,
   `Data Update Frequency`
     AS data_update_frequency,
   `Data Update Frequency Notes`
     AS data_update_frequency_notes,
    `Delivery Frequency Notes`
     As delivery_frequency_notes,
     case when ifnull(`Date Collection Began`, '') <> '' then str_to_date(
     `Date Collection Began`,
     '%m/%d/%Y') else null end
     AS date_collection_began,
   `Date Collection Range Explanation`
     AS date_collection_range_explanation,
   `Collection Methods Explanation`
     AS collection_methods_explanation,
   `Potential Drawbacks`
     AS potential_drawbacks,
   `Unique Value Props`
     AS unique_value_props,
   `Historical Date Range`
     AS historical_date_range,
   `Key Data Fields`
     AS key_data_fields,
   Summary
     AS summary,
   `# of Investor Clients` as investor_clients,
   `Historical Date Range Estimate`
     AS historical_date_range_estimate,
   `Public Companies Covered`
     AS public_companies_covered,
   `Product Details`
     AS product_details,
     case `User Permissions`
				when 'Registered'	then 'ROLE_USER'
				when 'Bronze'       then 'ROLE_BRONZE'
				when 'Silver'	 	then 'ROLE_SILVER'
				when 'Admin'        then 'ROLE_ADMIN'
                when ''             then null
				else `User Permissions`
			end as permission_name,
    ifnull(replace(dp.Completeness, '%', ''), 0),
      `Sample or Panel Size`,
      `Use Cases or Questions Addressed`,
    (select id from mp_lookup_code where lookupmodule = 'LICENSE_TYPE' and description = `Data License Type`),
      `Data License Type Details`,
      `Legal & Compliance Issues`,
      `Data Roadmap`,
      case when `Inactive?` = 'checked' then 1 else 0 end,
      case when `Free Trial Available` = 'checked' then 1 else 0 end,
   ifnull(
     (select
       dc.id
      from
       data_category dc
      where
       dc.record_id = `Main Data Category - Record ID`),
     1)
     AS main_data_category_id,
   ifnull(
     (select
       id
      from
       data_organization do
      where
       do.record_id = `Owner Organization - Record ID`
      order by
       1
      limit
       1),
     1)
     AS owner_organization_id,
   ifnull(
     (select
       di.id
      from
       data_industry di
      where
       di.record_id = `Main Data Industry - Record ID`),
     1)
     AS main_data_industry_id,
   ifnull(
     (select
       id
      from
       data_provider_type pt
      where
       pt.record_id = `Provider Type - Record ID`),
     1)
     AS provider_type_id,
      (select id from mp_lookup_code where lookupmodule = 'OVERALL_SCORE' and description = `Overall Score`),
      (select id from mp_lookup_code where lookupmodule = 'READINESS_SCORE' and description = `Readiness Score`),
      (select id from mp_lookup_code where lookupmodule = 'VALUE_SCORE' and description = `value Score`),
      (select id from mp_lookup_code where lookupmodule = 'UNIQUENESS_SCORE' and description = `Uniqueness Score`),
      (select id from mp_lookup_code where lookupmodule = 'INVESTOR_WILLINGNESS_SCORE' and description = `Willingness to Provide to Investors Score`),
    (select id from mp_lookup_code where lookupmodule = 'YES_OR_NO' and description = `SEC-Regulated`),
(select id from mp_lookup_code where lookupmodule = 'VERIFICATION_STATUS' and description = `Profile Verification Status`),
case when ifnull(`Public Equities Covered Count`, '') <> '' then
                    CAST(`Public Equities Covered Count` as UNSIGNED) else null END
                     AS public_equities_covered_count,
case when ifnull(`Investor Clients Count`, '') <> '' then
                    CAST(`Investor Clients Count` as UNSIGNED) else null END
                     AS investor_clients_count,
 (select id from mp_lookup_code where lookupmodule = 'PRICING_MODEL' and description = `Trial Pricing Model`),
 `Trial Duration` AS trial_duration,
 (select id from mp_lookup_code where lookupmodule = 'AGREEMENT_TYPE' and description = `Trial Agreement Type`),
 `Trial Pricing Details` AS trial_pricing_details,
 `Lag Time` AS lag_time,
 `Competitive Differentiators` AS competitive_differentiators,
 `Discussion - Analytics` AS discussion_analytics,
 `Discussion - Cleanliness` AS discussion_cleanliness,
 `Sample or Panel Biases` AS sample_or_panel_biases,
 case when ifnull(`Number of Analyst Employees`, '') <> '' then
                    CAST(`Number of Analyst Employees` as UNSIGNED) else null END
                     AS number_of_analyst_employees,
  (select id from mp_lookup_code where lookupmodule = 'YES_OR_NO_SOFT' and description = `Point-in-Time Accuracy`),
  `Data Sources Details` AS data_sources_details,
  (select id from mp_lookup_code where lookupmodule = 'YES_NO_UNKNOWN' and description = `Data Gaps`),
  (select id from mp_lookup_code where lookupmodule = 'YES_NO_UNKNOWN' and description = `Includes Outliers`),
    (select id from mp_lookup_code where lookupmodule = 'YES_NO_UNKNOWN' and description = `Normalized`),
  (select id from mp_lookup_code where lookupmodule = 'YES_NO_UNKNOWN' and description = `Duplicates Cleaned`),
  (select id from mp_lookup_code where lookupmodule = 'BIASES' and description = `Biases`),
  `Pricing Details` AS pricing_details,
  (select id from mp_lookup_code where lookupmodule = 'TIME_PERIOD' and description = `Subscription Time Period`),
  case when ifnull(`Year First Data Product Launched`, '') <> '' then
                    CAST(`Year First Data Product Launched` as UNSIGNED) else null END
                     AS year_first_data_product_launched,
  `Compliance Score - Personal Information` AS compliance_score_personal,
  `Compliance Score - Material Non-Public Information` AS compliance_score_public,
  (select id from mp_lookup_code where lookupmodule = 'MONTHLY_PRICE_RANGE' and description = `Monthly Price Range`),
  (select id from mp_lookup_code where lookupmodule = 'SUBSCRIPTION_MODEL' and description = `Subscription Model`),
  `Minimum Yearly Price` AS minimum_yearly_price,
  `Maximum Yearly Price` AS maximum_yearly_price,
  (select id from mp_lookup_code where lookupmodule = 'DATA_SOURCES_RANGE' and description = `Number of Data Sources`),
  `Privacy` AS privacy,
    (select id from mp_lookup_code where lookupmodule = 'IEI_STATUS' and description = `IEI Status`),
    (select id from mp_lookup_code where lookupmodule = 'MARKETPLACE_STATUS' and description = `Marketplace Status`),
    case when ifnull(`Planned Research Date`, '') <> '' then str_to_date(
     `Planned Research Date`,
     '%m/%d/%Y') else null end
     AS planned_research_date,
    `Main Asset Class` AS main_asset_class,
    `Dataset Size` AS dataset_size,
    `Investor Client Bucket` AS investor_client_bucket,
    `Public Equities Covered Range` AS public_equities_covered_range,
    `Dataset Number of Rows` AS dataset_number_of_rows,
    dp.Priority AS priority
  FROM
   staging_airtable.stg_data_provider dp
  where
   not exists
     (select
       1
      from
       data_provider
      where
       `Record ID` = data_provider.record_id)
and ifnull(`Record ID`, '') <> '';

update data_provider, mp_lookup_code, staging_airtable.stg_data_provider
set main_asset_class = mp_lookup_code.description
where staging_airtable.stg_data_provider.`record id` = data_provider.record_id
      and mp_lookup_code.lookupcode = case when ifnull(trim(staging_airtable.stg_data_provider.`main asset class`), '') = '' then 'DEFAULT_NULL'
                                      else trim(staging_airtable.stg_data_provider.`main asset class`) end
      and mp_lookup_code.lookupmodule = '3'
      and ifnull(main_asset_class, 0) <> mp_lookup_code.description;

update data_provider, (
                  select @rownum := @rownum + 1 AS sort_order, a.*
                  FROM (SELECT dp.id
                        FROM data_provider dp
                        ORDER BY dp.provider_name) a,
                      (SELECT @rownum := 0) r) a
set data_provider.sort_order = a.sort_order
where a.id = data_provider.id;

update data_provider set completeness = 0 where completeness is null;

update data_provider set data_update_frequency = null where data_update_frequency = '';

update data_provider set permission_name = 'ROLE_ADMIN' where ifnull(permission_name, '') = '';

create table provider_count_temp as select count(*) provider_count from staging_airtable.stg_data_provider;

SET @@FOREIGN_KEY_CHECKS = 0;

SET @@FOREIGN_KEY_CHECKS = 1;
drop table if exists provider_count_temp;

create table temp_data_provider_score_count as
    select mlc.id, count(*) count from data_provider dp
        inner join mp_lookup_code mlc on dp.score_overall_id = mlc.id
    group by mlc.id;

update mp_lookup_code, temp_data_provider_score_count
set record_count = temp_data_provider_score_count.count
where temp_data_provider_score_count.id = mp_lookup_code.id;

drop table if exists temp_data_provider_score_count;

create table temp_data_provider_score_count as
    select mlc.id, count(*) count from data_provider dp
        inner join mp_lookup_code mlc on dp.score_readiness_id = mlc.id
    group by mlc.id;

update mp_lookup_code, temp_data_provider_score_count
set record_count = temp_data_provider_score_count.count
where temp_data_provider_score_count.id = mp_lookup_code.id;

drop table if exists temp_data_provider_score_count;

create table temp_data_provider_score_count as
    select mlc.id, count(*) count from data_provider dp
        inner join mp_lookup_code mlc on dp.score_value_id = mlc.id
    group by mlc.id;

update mp_lookup_code, temp_data_provider_score_count
set record_count = temp_data_provider_score_count.count
where temp_data_provider_score_count.id = mp_lookup_code.id;

drop table if exists temp_data_provider_score_count;

create table temp_data_provider_score_count as
    select mlc.id, count(*) count from data_provider dp
        inner join mp_lookup_code mlc on dp.score_uniqueness_id = mlc.id
    group by mlc.id;

update mp_lookup_code, temp_data_provider_score_count
set record_count = temp_data_provider_score_count.count
where temp_data_provider_score_count.id = mp_lookup_code.id;

drop table if exists temp_data_provider_score_count;

create table temp_data_provider_score_count as
    select mlc.id, count(*) count from data_provider dp
        inner join mp_lookup_code mlc on dp.score_investors_willingness_id = mlc.id
    group by mlc.id;

update mp_lookup_code, temp_data_provider_score_count
set record_count = temp_data_provider_score_count.count
where temp_data_provider_score_count.id = mp_lookup_code.id;

drop table if exists temp_data_provider_score_count;

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (SELECT DISTINCT
              'DATA_UPDATE_FREQUENCY' lookupmodule,
              ifnull(trim(DATA_UPDATE_FREQUENCY), '') lookupcode,
              ifnull(trim(DATA_UPDATE_FREQUENCY), '') description
          FROM data_provider i
          ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'DATA_UPDATE_FREQUENCY'
               AND c.lookupcode = a.lookupcode);

               select DATA_UPDATE_FREQUENCY, count(*) from data_provider
					 group by DATA_UPDATE_FREQUENCY;



					 update data_provider, mp_lookup_code
					  set data_update_frequency = mp_lookup_code.description
					  where lookupcode = data_update_frequency;



					 create table temp_data_provider_update_frequency_count as
    select mlc.id, count(*) count from data_provider dp
        inner join mp_lookup_code mlc on dp.data_update_frequency = mlc.description
    group by mlc.id;

update mp_lookup_code, temp_data_provider_update_frequency_count
set record_count = temp_data_provider_update_frequency_count.count
where temp_data_provider_update_frequency_count.id = mp_lookup_code.id;

drop table if exists temp_data_provider_update_frequency_count;


create table temp_im_provider_marketplace_status as select mlc.id, count(*) count from data_provider dpdf inner join mp_lookup_code mlc on dpdf.marketplace_status_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_im_provider_marketplace_status
set record_count = temp_im_provider_marketplace_status.count
where temp_im_provider_marketplace_status.id = mp_lookup_code.id;

drop table if exists temp_im_provider_marketplace_status;

create table temp_im_provider_iei_status as select mlc.id, count(*) count from data_provider dpdf inner join mp_lookup_code mlc on dpdf.iei_status_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_im_provider_iei_status
set record_count = temp_im_provider_iei_status.count
where temp_im_provider_iei_status.id = mp_lookup_code.id;

drop table if exists temp_im_provider_iei_status;


   UPDATE
    data_provider
SET
website = CASE website WHEN '' THEN NULL ELSE website END,
short_description = CASE short_description WHEN '' THEN NULL ELSE short_description END,
long_description = CASE long_description WHEN '' THEN NULL ELSE long_description END,
notes = CASE notes WHEN '' THEN NULL ELSE notes END,
data_update_frequency = CASE data_update_frequency WHEN '' THEN NULL ELSE data_update_frequency END,
data_update_frequency_notes = CASE data_update_frequency_notes WHEN '' THEN NULL ELSE data_update_frequency_notes END,
date_collection_began = CASE date_collection_began WHEN '' THEN NULL ELSE date_collection_began END,
date_collection_range_explanation = CASE date_collection_range_explanation WHEN '' THEN NULL ELSE date_collection_range_explanation END,
collection_methods_explanation = CASE collection_methods_explanation WHEN '' THEN NULL ELSE collection_methods_explanation END,
potential_drawbacks = CASE potential_drawbacks WHEN '' THEN NULL ELSE potential_drawbacks END,
unique_value_props = CASE unique_value_props WHEN '' THEN NULL ELSE unique_value_props END,
key_data_fields = CASE key_data_fields WHEN '' THEN NULL ELSE key_data_fields END,
summary = CASE summary WHEN '' THEN NULL ELSE summary END,
investor_clients = CASE investor_clients WHEN '' THEN NULL ELSE investor_clients END,
public_companies_covered = CASE public_companies_covered WHEN '' THEN NULL ELSE public_companies_covered END,
product_details = CASE product_details WHEN '' THEN NULL ELSE product_details END,
main_data_category_id = CASE main_data_category_id WHEN '' THEN NULL ELSE main_data_category_id END,
owner_organization_id = CASE owner_organization_id WHEN '' THEN NULL ELSE owner_organization_id END,
main_data_industry_id=CASE main_data_industry_id WHEN '' THEN NULL ELSE main_data_industry_id END,
provider_type_id=CASE provider_type_id WHEN '' THEN NULL ELSE provider_type_id END,
feature=CASE feature WHEN '' THEN NULL ELSE feature END,
industry=CASE industry WHEN '' THEN NULL ELSE industry END,
category=CASE category WHEN '' THEN NULL ELSE category END,
tag=CASE tag WHEN '' THEN NULL ELSE tag END,
source=CASE source WHEN '' THEN NULL ELSE source END,
asset_class=CASE asset_class WHEN '' THEN NULL ELSE asset_class END,
security_type=CASE security_type WHEN '' THEN NULL ELSE security_type END,
delivery_format=CASE delivery_format WHEN '' THEN NULL ELSE delivery_format END,
relevant_sector=CASE relevant_sector WHEN '' THEN NULL ELSE relevant_sector END,
delivery_method=CASE delivery_method WHEN '' THEN NULL ELSE delivery_method END,
permission_name=CASE permission_name WHEN '' THEN NULL ELSE permission_name END,
org_relationship_status=CASE org_relationship_status WHEN '' THEN NULL ELSE org_relationship_status END,
Sample_or_Panel_Size=CASE Sample_or_Panel_Size WHEN '' THEN NULL ELSE Sample_or_Panel_Size END,
Data_License_Type_Details=CASE Data_License_Type_Details WHEN '' THEN NULL ELSE Data_License_Type_Details END,
Legal_and_Compliance_Issues=CASE Legal_and_Compliance_Issues WHEN '' THEN NULL ELSE Legal_and_Compliance_Issues END,
Data_Roadmap=CASE Data_Roadmap WHEN '' THEN NULL ELSE Data_Roadmap END,
score_overall_id=CASE score_overall_id WHEN '' THEN NULL ELSE score_overall_id END,
score_readiness_id=CASE score_readiness_id WHEN '' THEN NULL ELSE score_readiness_id END,
score_value_id=CASE score_value_id WHEN '' THEN NULL ELSE score_value_id END,
score_uniqueness_id=CASE score_uniqueness_id WHEN '' THEN NULL ELSE score_uniqueness_id END,
score_investors_willingness_id=CASE score_investors_willingness_id WHEN '' THEN NULL ELSE score_investors_willingness_id END,
delivery_frequency_notes=CASE delivery_frequency_notes WHEN '' THEN NULL ELSE delivery_frequency_notes END,
creator_user_id=CASE creator_user_id WHEN '' THEN NULL ELSE creator_user_id END,
editor_user_id=CASE editor_user_id WHEN '' THEN NULL ELSE editor_user_id END,
sec_regulated_id=CASE sec_regulated_id WHEN '' THEN NULL ELSE sec_regulated_id END,
public_equities_covered_count=CASE public_equities_covered_count WHEN '' THEN NULL ELSE public_equities_covered_count END,
investor_clients_count=CASE investor_clients_count WHEN '' THEN NULL ELSE investor_clients_count END,
trial_duration=CASE trial_duration WHEN '' THEN NULL ELSE trial_duration END,
trial_agreement_type_id=CASE trial_agreement_type_id WHEN '' THEN NULL ELSE trial_agreement_type_id END,
trial_pricing_details=CASE trial_pricing_details WHEN '' THEN NULL ELSE trial_pricing_details END,
lag_time=CASE lag_time WHEN '' THEN NULL ELSE lag_time END,
competitive_differentiators=CASE competitive_differentiators WHEN '' THEN NULL ELSE competitive_differentiators END,
discussion_analytics=CASE discussion_analytics WHEN '' THEN NULL ELSE discussion_analytics END,
discussion_cleanliness=CASE discussion_cleanliness WHEN '' THEN NULL ELSE discussion_cleanliness END,
sample_or_panel_biases=CASE sample_or_panel_biases WHEN '' THEN NULL ELSE sample_or_panel_biases END,
number_of_analyst_employees=CASE number_of_analyst_employees WHEN '' THEN NULL ELSE number_of_analyst_employees END,
point_in_time_accuracy_id=CASE point_in_time_accuracy_id WHEN '' THEN NULL ELSE point_in_time_accuracy_id END,
data_sources_details=CASE data_sources_details WHEN '' THEN NULL ELSE data_sources_details END,
data_gaps_id=CASE data_gaps_id WHEN '' THEN NULL ELSE data_gaps_id END,
includes_outliers_id=CASE includes_outliers_id WHEN '' THEN NULL ELSE includes_outliers_id END,
normalized_id=CASE normalized_id WHEN '' THEN NULL ELSE normalized_id END,
duplicates_cleaned_id=CASE duplicates_cleaned_id WHEN '' THEN NULL ELSE duplicates_cleaned_id END,
biases_id=CASE biases_id WHEN '' THEN NULL ELSE normalized_id END,
pricing_details=CASE pricing_details WHEN '' THEN NULL ELSE pricing_details END,
subscription_time_period_id=CASE subscription_time_period_id WHEN '' THEN NULL ELSE subscription_time_period_id END,
year_first_data_product_launched=CASE year_first_data_product_launched WHEN '' THEN NULL ELSE year_first_data_product_launched END,
monthly_price_range_id=CASE monthly_price_range_id WHEN '' THEN NULL ELSE monthly_price_range_id END,
subscription_model_id=CASE subscription_model_id WHEN '' THEN NULL ELSE subscription_model_id END,
minimum_yearly_price=CASE minimum_yearly_price WHEN '' THEN NULL ELSE minimum_yearly_price END,
maximum_yearly_price=CASE maximum_yearly_price WHEN '' THEN NULL ELSE maximum_yearly_price END,
maximum_yearly_price=CASE maximum_yearly_price WHEN '' THEN NULL ELSE maximum_yearly_price END,
privacy=CASE privacy WHEN '' THEN NULL ELSE privacy END,
number_of_data_sources_id=CASE number_of_data_sources_id WHEN '' THEN NULL ELSE number_of_data_sources_id END,
trial_pricing_model_id=CASE trial_pricing_model_id WHEN '' THEN NULL ELSE trial_pricing_model_id END,
iei_status_id=CASE iei_status_id WHEN '' THEN NULL ELSE iei_status_id END,
research_methods=CASE research_methods WHEN '' THEN NULL ELSE research_methods END,
out_of_sample_data=CASE out_of_sample_data WHEN '' THEN NULL ELSE out_of_sample_data END,
data_license_type_id=CASE data_license_type_id WHEN '' THEN NULL ELSE data_license_type_id END,
main_asset_class=CASE main_asset_class WHEN '' THEN NULL ELSE main_asset_class END,
Use_Cases_or_Questions_Addressed=CASE Use_Cases_or_Questions_Addressed WHEN '' THEN NULL ELSE Use_Cases_or_Questions_Addressed END,
priority=CASE priority WHEN '' THEN NULL ELSE priority END,
dataset_size=CASE dataset_size WHEN '' THEN NULL ELSE dataset_size END,
investor_client_bucket=CASE investor_client_bucket WHEN '' THEN NULL ELSE investor_client_bucket END,
dataset_number_of_rows=CASE dataset_number_of_rows WHEN '' THEN NULL ELSE dataset_number_of_rows END,
marketplace_status_id=CASE marketplace_status_id WHEN '' THEN NULL ELSE marketplace_status_id END
