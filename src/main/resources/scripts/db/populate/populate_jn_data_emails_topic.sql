DELETE from data_emails_topic
where not exists(select 1 from data_emails dp, data_topic lc where dp.id = data_emails_topic.email_id and lc.id = data_emails_topic.topic_id);


INSERT INTO data_emails_topic(topic_id, email_id)
   SELECT DISTINCT df.id, dp.id
     FROM staging_airtable.im_emails_topics pf
          INNER JOIN data_emails dp ON dp.record_id = pf.emails_recordID
          INNER JOIN data_topic df ON df.record_id = pf.topics_recordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_emails_topic jpf
               WHERE     jpf.topic_id = df.id
                     AND jpf.email_id = dp.id);