DELETE from data_investor_types
where not exists(select 1 from data_inv_managers dp, mp_lookup_code lc where dp.id = data_investor_types.investor_managers_id and lc.id = data_investor_types.type_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'TYPES' lookupmodule,
                    i.type lookupcode,
                    i.type description
               FROM staging_airtable.im_investor_types i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'TYPES'
                     AND c.lookupcode = a.lookupcode);
                     
INSERT INTO data_investor_types(type_id,
                                             investor_managers_id)
   SELECT lc.id, dp.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_types rac
             ON rac.investment_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'TYPES'
                AND lc.lookupcode = rac.type
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_types pa
               WHERE     pa.type_id = lc.id
                     AND pa.investor_managers_id = dp.id);                     
