DELETE from data_provider_delivery_method where not exists(select 1 from data_provider dp, mp_lookup_code lc where dp.id = data_provider_delivery_method.data_providers_id and lc.id = data_provider_delivery_method.delivery_methods_id);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'DELIVERY_METHOD' lookupmodule,
                    i.method lookupcode,
                    i.method description
               FROM staging_airtable.im_provider_delivery_method i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'DELIVERY_METHOD'
                     AND c.lookupcode = lookupcode);

INSERT INTO data_provider_delivery_method(DELIVERY_methods_id,
                                          data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_delivery_method rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'DELIVERY_METHOD'
                AND lc.lookupcode = rac.method
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_delivery_method pa
               WHERE     pa.DELIVERY_methods_id = lc.id
                     AND pa.data_Providers_ID = dp.id);
create table temp_delivery_method_count as select mlc.id, count(*) count from data_provider_delivery_method dpdm inner join mp_lookup_code mlc on dpdm.delivery_methods_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_delivery_method_count
set record_count = temp_delivery_method_count.count
where temp_delivery_method_count.id = mp_lookup_code.id;

drop table if exists temp_delivery_method_count;
