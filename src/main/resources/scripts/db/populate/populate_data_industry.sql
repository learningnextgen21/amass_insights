-- truncate table data_industry;
update datamarketplace.data_industry, staging_airtable.stg_data_industry
		set Data_Industry = `Data Industry`,
			data_industry.Explanation = stg_data_industry.Explanation,
			Inactive = 0,
			Created_Date = str_to_date(`date created`, '%m/%d/%Y %h:%i %p'),
			data_industry.hashtag=stg_data_industry.Hashtag
					where record_id = `Record ID`;
insert
	into
		data_industry(
			ID,
			Record_ID,
			Data_Industry,
			Explanation,
			Inactive,
			Created_Date
		)
	select
		1,
		'RECORD_NULL',
		'DATA_INDUSTRY_NULL',
		null,
		0,
		current_timestamp from dual
	where not exists(select 1 from data_industry where id = 1);

insert
	into
		data_industry(
			Record_ID,
			Data_Industry,
			Explanation,
			inactive,
			created_date,
			hashtag
		)
		select
			`Record ID`,
			`Data Industry`,
			Explanation,
			0 inactive,
			str_to_date(`date created`, '%m/%d/%Y %h:%i %p') created_date,
			`Hashtag`
		from
			staging_airtable.stg_data_industry sdi where ifnull(`Record ID`, '') <> '' and not exists(select 1 from data_industry di where di.record_id = sdi.`Record ID`);

update data_industry, (
                  select @rownum := @rownum + 1 AS sort_order, a.*
                  FROM (SELECT sdi.`Record ID`, sdi.`Data Industry`
                        FROM staging_airtable.stg_data_industry sdi
                        ORDER BY sdi.`Data Industry`) a,
                      (SELECT @rownum := 0) r) a
set data_industry.sort_order = a.sort_order
where `Record ID` = record_id;
