SET SESSION group_concat_max_len = 999999999;
UPDATE data_provider set industry = (select cast(concat('[', group_concat('{"id":"', di.id, '", "desc":"', di.data_industry, '"}'), ']') as json) industry
from data_industry di inner join data_provider_industry dpi on di.id = dpi.industries_id where data_provider.id = dpi.data_providers_id);
update data_provider set category = (select cast(concat('[', group_concat('{"id":"', dc.id, '", "desc":"', dc.datacategory, '"}'), ']') as json) category
from data_category dc inner join data_provider_category dpc on dc.id = dpc.categories_id where dpc.data_providers_id = data_provider.id);
UPDATE data_provider SET tag = (select cast(concat('[', group_concat('{"id":"', dt.id, '", "desc":"', dt.provider_tag, '"}'), ']') as json) tag
from data_provider_tag dt inner join data_provider_provider_tag dpt on dt.id = dpt.provider_tags_id where dpt.data_providers_id = data_provider.id);
UPDATE data_provider SET source = (select cast(concat('[', group_concat('{"id":"', ds.id, '", "desc":"', ds.data_source, '"}'), ']') as json) source
from data_source ds inner join data_provider_source dps on ds.id = dps.sources_id where dps.data_providers_id = data_provider.id);
UPDATE data_provider SET asset_class = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) asset_class
from mp_lookup_code l inner join data_provider_asset_class ac on l.id = ac.asset_classes_id where ac.data_providers_id = data_provider.id);
UPDATE data_provider SET security_type = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) security_type
from mp_lookup_code l inner join data_provider_security_type ac on l.id = ac.security_types_id where ac.data_providers_id = data_provider.id);
UPDATE data_provider SET delivery_format = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) delivery_format
from mp_lookup_code l inner join data_provider_delivery_format ac on l.id = ac.delivery_formats_id where ac.data_providers_id = data_provider.id);
UPDATE data_provider SET delivery_method = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) delivery_method
from mp_lookup_code l inner join data_provider_delivery_method ac on l.id = ac.delivery_methods_id where ac.data_providers_id = data_provider.id);
UPDATE data_provider SET pricing_model = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) pricing_model
from mp_lookup_code l inner join data_provider_pricing_model ac on l.id = ac.pricing_models_id where ac.data_providers_id = data_provider.id);
