INSERT INTO data_organization_comments(comment_id, comment_record_id, organization_record_id,
                                  organization_id,
                                  comment_type)
   SELECT DISTINCT p1.id comment_id, p1.record_id, p2.record_id, p2.id organization_id, lc.id
        FROM staging_airtable.im_comment_organization p
             INNER JOIN mp_lookup_code lc
                ON     lc.lookupmodule = 'ORGANIZATION_TYPE'
                   AND lc.lookupcode = 'Participant Organizations'
            INNER JOIN data_comments p1 ON p1.Record_ID = p.Comment_RecordID
            INNER JOIN data_organization p2 ON p2.Record_ID = p.Organization_RecordID
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_organization_comments pp
               WHERE     pp.comment_id = p1.id
                     AND pp.organization_id = p2.id
                     AND pp.comment_type = lc.id);

INSERT INTO data_organization_comments(comment_id, comment_record_id, organization_record_id,
                                  organization_id,
                                  comment_type)
   SELECT DISTINCT p1.id comment_id, p1.record_id, p2.record_id, p2.id organization_id, lc.id
        FROM staging_airtable.im_comment_relevent_organization p
             INNER JOIN mp_lookup_code lc
                ON     lc.lookupmodule = 'ORGANIZATION_TYPE'
                   AND lc.lookupcode = 'Relevant Organizations'
            INNER JOIN data_comments p1 ON p1.Record_ID = p.Comment_RecordID
            INNER JOIN data_organization p2 ON p2.Record_ID = p.Organization_RecordID
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_organization_comments pp
               WHERE     pp.comment_id = p1.id
                     AND pp.organization_id = p2.id
                     AND pp.comment_type = lc.id);

