DELETE from data_investor_investing_timeframe
where not exists(select 1 from data_inv_managers dp, mp_lookup_code lc where dp.id = data_investor_investing_timeframe.investor_managers_id and lc.id = data_investor_investing_timeframe.investing_timeframe_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'INVESTING_TIME_FRAME' lookupmodule,
                    i.investing_timeframe lookupcode,
                    i.investing_timeframe description
               FROM staging_airtable.im_investor_investing_timeframe i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'INVESTING_TIME_FRAME'
                     AND c.lookupcode = a.lookupcode);
INSERT INTO data_investor_investing_timeframe(investing_timeframe_id,
                                             investor_managers_id)
   SELECT lc.id, dp.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_investing_timeframe rac
             ON rac.investment_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'INVESTING_TIME_FRAME'
                AND lc.lookupcode = rac.investing_timeframe
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_investing_timeframe pa
               WHERE     pa.investing_timeframe_id = lc.id
                     AND pa.investor_managers_id = dp.id);
