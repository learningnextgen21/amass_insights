DELETE from data_investor_investment_stage
where not exists(select 1 from data_inv_managers dp, mp_lookup_code lc where dp.id = data_investor_investment_stage.investor_managers_id and lc.id = data_investor_investment_stage.investment_stage_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'INVESTMENT_STAGE' lookupmodule,
                    i.investment_stage lookupcode,
                    i.investment_stage description
               FROM staging_airtable.im_investor_investment_stages i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'INVESTMENT_STAGE'
                     AND c.lookupcode = a.lookupcode);
                 
INSERT INTO data_investor_investment_stage(investment_stage_id,
                                             investor_managers_id)
   SELECT lc.id, dp.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_investment_stages rac
             ON rac.investment_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'INVESTMENT_STAGE'
                AND lc.lookupcode = rac.investment_stage
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_investment_stage pa
               WHERE     pa.investment_stage_id = lc.id
                     AND pa.investor_managers_id = dp.id);                     
                     