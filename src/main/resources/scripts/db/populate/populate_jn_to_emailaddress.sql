DELETE from data_to_email_addresses
where not exists(select 1 from data_emails dp, data_email_address lc where dp.id = data_to_email_addresses.email_id and lc.id = data_to_email_addresses.to_email_address_id);



INSERT INTO data_to_email_addresses(to_email_address_id, email_id)
   SELECT DISTINCT df.id, dp.id
     FROM staging_airtable.im_to_email_addresses pf
          INNER JOIN data_emails dp ON dp.record_id = pf.emails_recordID
          INNER JOIN data_email_address df ON df.record_id = pf.to_email_addresses_recordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_to_email_addresses jpf
               WHERE     jpf.to_email_address_id = df.id
                     AND jpf.email_id = dp.id);