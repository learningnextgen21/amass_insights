-- truncate table data_provider_tag;
update datamarketplace.data_provider_tag dpt, staging_airtable.stg_data_provider_tag sdt
		set Provider_Tag = name,
			dpt.Explanation = sdt.Explanation,
			dpt.inactive = FALSE,
			dpt.created_date = str_to_date(`date created`, '%m/%d/%Y %h:%i %p'),
			dpt.privacy = sdt.Privacy,
			dpt.type = sdt.type,
      dpt.by_user_id = case when ifnull(sdt.`By User`, '') <> '' then
                    CAST(`By User` as UNSIGNED) else null END,
      dpt.receive_notifications = sdt.`Receive Notifications`
where dpt.record_id = sdt.`Record ID`;
insert
	into
		data_provider_tag(
			record_id,
			Provider_Tag,
			Explanation,
			inactive,
			created_date,
			privacy,
			by_user_id,
			receive_notifications,
			type
		) select
			`record id`,
			Name,
			Explanation,
			Inactive,
			case when ifnull(`Date Created`, '') <> '' then str_to_date(
                    `Date Created`,
                    '%m/%d/%Y %h:%i%p') else null end
                    AS created_date,
                    Privacy AS privacy,
            case when ifnull(`By User`, '') <> '' then
                    CAST(`By User` as UNSIGNED) else null END
                     AS by_user_id,
                    `Receive Notifications` AS receive_notifications,
                    type
		from
			staging_airtable.stg_data_provider_tag dpt where not exists(select 1 from data_provider_tag pt where dpt.`Record ID` = pt.record_id);

update data_provider_tag, (
                          select @rownum := @rownum + 1 AS sort_order, a.*
                          FROM (SELECT sdpt.`Record ID`, sdpt.Name
                                FROM staging_airtable.stg_data_provider_tag sdpt
                                ORDER BY sdpt.Name) a,
                              (SELECT @rownum := 0) r) a
set data_provider_tag.sort_order = a.sort_order
where `Record ID` = record_id;

update datamarketplace.data_provider_tag dc SET dc.by_user_id = NULL WHERE dc.by_user_id=0;

DELETE FROM data_provider_tag WHERE by_user_id is not null and by_user_id not IN (SELECT id FROM jhi_user);
