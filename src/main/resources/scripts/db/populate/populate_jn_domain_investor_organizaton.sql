DELETE from data_investor_domain
where not exists(select 1 from data_domains dp, data_inv_managers lc where dp.id = data_investor_domain.domain_id and lc.id = data_investor_domain.investor_id);

INSERT INTO data_investor_domain(investor_id, domain_id)
   SELECT DISTINCT df.id, dp.id
     FROM staging_airtable.im_domain_investment_manager pf
          INNER JOIN data_domains dp ON dp.record_id = pf.domains_recordID
          INNER JOIN data_inv_managers df ON df.record_id = pf.investment_manager_recordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_domain jpf
               WHERE     jpf.investor_id = df.id
                     AND jpf.domain_id = dp.id);