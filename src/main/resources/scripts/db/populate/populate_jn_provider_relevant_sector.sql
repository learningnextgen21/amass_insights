delete from data_provider_relevant_sector where not exists(select 1 from data_provider dp, mp_lookup_code lc where dp.id = data_provider_relevant_sector.data_providers_id and lc.id = data_provider_relevant_sector.relevant_sectors_id);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'RLVNT_SECTOR' lookupmodule,
                    i.sector lookupcode,
                    i.sector description
               FROM staging_airtable.im_relevant_sectors i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'RLVNT_SECTOR'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_provider_relevant_sector(relevant_sectors_id, data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_relevant_sectors rit
             ON rit.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'RLVNT_SECTOR'
                AND lc.lookupcode = rit.sector
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_relevant_sector pa
               WHERE     pa.relevant_sectors_id = lc.id
                     AND pa.data_Providers_ID = dp.id);


   create table temp_relevant_sector as select mlc.id, count(*) count from data_provider_relevant_sector dpdf inner join mp_lookup_code mlc on dpdf.relevant_sectors_id = mlc.id
   group by mlc.id;

   update mp_lookup_code, temp_relevant_sector
   set record_count = temp_relevant_sector.count
   where temp_relevant_sector.id = mp_lookup_code.id;

   drop table if exists temp_relevant_sector;
