update data_resource_purpose_type, staging_airtable.stg_resource_purpose_type
set data_resource_purpose_type.purpose_type = `Purpose Type`,
    data_resource_purpose_type.explanation = stg_resource_purpose_type.Explanation,
	created_date = str_to_date(`Date Created`, '%m/%d/%Y %h:%i %p')
		where record_id = `Record ID`;

INSERT INTO data_resource_purpose_type
(
	record_id,
    purpose_type,
    explanation,
	`created_date`)
	SELECT
		`record id`,
		`Purpose Type`,
        Explanation,
		str_to_date(`Date Created`, '%m/%d/%Y %h:%i %p') created_date
	FROM `staging_airtable`.`stg_resource_purpose_type` sat WHERE not exists (select 1 from data_resource_purpose_type dt where dt.record_id = sat.`Record ID`) ORDER BY `Purpose Type`;

update data_resource_purpose_type, (
                  select @rownum := @rownum + 1 AS sort_order, a.*
                  FROM (SELECT sds.`Record ID`, sds.`Purpose Type`
                        FROM staging_airtable.stg_resource_purpose_type sds
                        ORDER BY sds.`Purpose Type`) a,
                      (SELECT @rownum := 0) r) a
set data_resource_purpose_type.sort_order = a.sort_order
where `Record ID` = record_id;
