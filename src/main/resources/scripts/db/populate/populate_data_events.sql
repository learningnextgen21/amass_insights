update datamarketplace.data_event de, staging_airtable.stg_data_events sde
    set de.event_edition_name = sde.`Event Edition Name`,
                              de.link=sde.Link,
                              de.description=sde.Description,
                              de.details=sde.Details,
                              de.agenda=sde.Agenda,
                              de.length_in_days=sde.`Length (in Days)`,
                              de.price=sde.Price,
                              de.venue=sde.Venue,
                              de.address=sde.Address,
                              de.city=sde.City,
                              de.state=sde.State,
                              de.country=sde.Country,
                              de.zip_code=sde.`Zip Code`,
                              de.authored_articles=sde.`Authored Articles Count`,
                              de.relevent_articles=sde.`Relevant Articles Count`,
                              de.start_time=case when ifnull(`Start Time (EST)`, '') <> '' then str_to_date(`Start Time (EST)`,
                    '%m/%d/%Y %h:%i%p') else null end,
                              de.end_time= case when ifnull(`End Time (EST)`, '') <> '' then str_to_date(
                                      `End Time (EST)`,
                                      '%m/%d/%Y %h:%i%p') else null end,
              de.created_time=case when ifnull(`Created Time`, '') <> '' then str_to_date(
                    `Created Time`,
                     '%m/%d/%Y %h:%i%p') else null end,
              de.last_updated=case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(
                               `Last Updated Date`,
                               '%m/%d/%Y') else null end,
                              de.user_permission = case `User Permissions`
                   when 'Registered'	then 'ROLE_USER'
                   when 'Bronze'        then 'ROLE_BRONZE'
                   when 'Silver' 	    then 'ROLE_SILVER'
                   when 'Admin'         then 'ROLE_ADMIN'
                   when ''              then null
                   else `User Permissions`
                   end,
                              de.completeness=sde.Completeness
        where record_id = `Record ID`;
insert
	into
		datamarketplace.data_event(
			              event_edition_name,
                          record_id,
                          link,
                          description,
                          details,
                          agenda,
                          length_in_days,
                          price,
                          venue,
                          address,
                          city,
                          state,
                          country,
                          zip_code,
                          authored_articles,
                          relevent_articles,
                          start_time,
                          end_time,
                          created_time,
                          last_updated,
                          user_permission,
                          completeness
                          )
		SELECT
			`Event Edition Name` AS event_edition_name,
            `Record ID` AS record_id,
             Link AS link,
             Description AS description,
             Details AS details,
             Agenda AS agenda,
             `Length (in Days)` AS length_in_days,
              Price AS price,
              Venue AS venue,
              Address AS address,
              City AS city,
              State AS state,
              Country AS country,
              `Zip Code` AS zip_code,
              `Authored Articles Count` AS authored_articles,
              `Relevant Articles Count` AS relevent_articles,
              case when ifnull(`Start Time (EST)`, '') <> '' then str_to_date(
                    `Start Time (EST)`,
                    '%m/%d/%Y %h:%i%p') else null end
                    AS start_time,
              case when ifnull(`End Time (EST)`, '') <> '' then str_to_date(
                                      `End Time (EST)`,
                                      '%m/%d/%Y %h:%i%p') else null end
                                      AS end_time,
              case when ifnull(`Created Time`, '') <> '' then str_to_date(
                    `Created Time`,
                     '%m/%d/%Y %h:%i%p') else null end
                     AS created_time,
              case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(
                               `Last Updated Date`,
                               '%m/%d/%Y') else null end AS last_updated,
     case `User Permissions`
				when 'Registered'	then 'ROLE_USER'
				when 'Bronze'       then 'ROLE_BRONZE'
				when 'Silver'	 	then 'ROLE_SILVER'
				when 'Admin'        then 'ROLE_ADMIN'
                when ''             then null
				else `User Permissions`
			end as user_permission,
			ifnull(replace(dp.Completeness, '%', ''), 0)
			FROM
               staging_airtable.stg_data_events dp
              where
               not exists
                 (select
                   1
                  from
                   data_event
                  where
                   `Record ID` = data_event.record_id)
            and ifnull(`Record ID`, '') <> '';
