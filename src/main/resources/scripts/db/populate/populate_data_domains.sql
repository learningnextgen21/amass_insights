INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'GENERIC_DOMAIN' lookupmodule,
                i.`Generic Email Domain` lookupcode,
                i.`Generic Email Domain` description
            FROM staging_airtable.stg_data_domains i WHERE i.`Generic Email Domain`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'GENERIC_DOMAIN'
               AND c.lookupcode = a.lookupcode);
               

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
    SELECT a.*, @rownum := @rownum + 1 AS sort_order
    FROM (  SELECT DISTINCT
                'DOMAIN_TYPE' lookupmodule,
                i.`Type` lookupcode,
                i.`Type` description
            FROM staging_airtable.stg_data_domains i WHERE i.`Type`!=''
            ORDER BY lookupcode) a,
        (SELECT @rownum := 0) r
    WHERE NOT EXISTS
    (SELECT 1
     FROM mp_lookup_code c
     WHERE     c.lookupmodule = 'DOMAIN_TYPE'
               AND c.lookupcode = a.lookupcode);               

update datamarketplace.data_domains de, staging_airtable.stg_data_domains sde
    set de.domain = sde.`Domain`,
                              de.type_id=(select id from mp_lookup_code where lookupmodule = 'DOMAIN_TYPE' and description = `Type`),
                              de.record_id=sde.`Record ID`,
              de.created_date=case when ifnull(`Created Date`, '') <> '' then str_to_date(
                    `Created Date`,
                     '%m/%d/%Y %h:%i%p') else null end,
                      de.last_updated_date=case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(
                               `Last Updated Date`,
                               '%m/%d/%Y') else null end,
                              de.generic_email_domain_id=(select id from mp_lookup_code where lookupmodule = 'GENERIC_DOMAIN' and description = `Generic Email Domain`)
        where record_id = `Record ID`;
 

INSERT INTO
 data_domains(
   domain,
   type_id,
   record_id,
   created_date,
   last_updated_date,
   generic_email_domain_id
   )
  SELECT
   `Domain`,
(select id from mp_lookup_code where lookupmodule = 'DOMAIN_TYPE' and description = `Type`),
 `Record ID`,
     case when ifnull(`Created Date`, '') <> '' then str_to_date(
                    `Created Date`,
                     '%m/%d/%Y %h:%i%p') else null end
                     AS created_date,   
    case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(
                               `Last Updated Date`,
                               '%m/%d/%Y') else null end AS last_updated_date,
     (select id from mp_lookup_code where lookupmodule = 'GENERIC_DOMAIN' and description = `Generic Email Domain`)    
  FROM
   staging_airtable.stg_data_domains a where not exists (select 1 from data_domains da where da.record_id = a.`Record ID`);        