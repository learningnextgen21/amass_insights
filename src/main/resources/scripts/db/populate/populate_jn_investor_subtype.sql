DELETE from data_investor_subtype
where not exists(select 1 from data_inv_managers dp, mp_lookup_code lc where dp.id = data_investor_subtype.investor_managers_id and lc.id = data_investor_subtype.subtype_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'INVESTMENT_MANAGER_SUBTYPE' lookupmodule,
                    i.subtype lookupcode,
                    i.subtype description
               FROM staging_airtable.im_investor_subtypes i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'INVESTMENT_MANAGER_SUBTYPE'
                     AND c.lookupcode = a.lookupcode);
                                          
                     
                 
INSERT INTO data_investor_subtype(subtype_id,
                                             investor_managers_id)
   SELECT lc.id, dp.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_subtypes rac
             ON rac.investment_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'INVESTMENT_MANAGER_SUBTYPE'
                AND lc.lookupcode = rac.subtype
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_subtype pa
               WHERE     pa.subtype_id = lc.id
                     AND pa.investor_managers_id = dp.id);                     
                     