delete from data_provider_partner where not exists(select 1 from data_provider dp, data_provider partner,
  mp_lookup_code lc where dp.id = data_provider_partner.data_providers_id and lc.id = data_provider_partner.partner_type
and data_provider_partner.partners_id = partner.id);


insert into mp_lookup_code (lookupmodule, lookupcode) select 'PARTNER_TYPE'
,'PROVIDER_DATA_PARTNER' from dual where not exists(select 1 from mp_lookup_code where lookupmodule = 'PARTNER_TYPE'
and lookupcode = 'PROVIDER_DATA_PARTNER');

insert into mp_lookup_code (lookupmodule, lookupcode) select 'PARTNER_TYPE'
                                                          ,'DISTRIBUTION_DATA_PARTNER' from dual where not exists(select 1 from mp_lookup_code where lookupmodule = 'PARTNER_TYPE'
                                                                                                                                                 and lookupcode = 'DISTRIBUTION_DATA_PARTNER');

insert into mp_lookup_code (lookupmodule, lookupcode) select 'PARTNER_TYPE'
                                                          ,'CONSUMER_DATA_PARTNER' from dual where not exists(select 1 from mp_lookup_code where lookupmodule = 'PARTNER_TYPE'
                                                                                                                                                 and lookupcode = 'CONSUMER_DATA_PARTNER');

insert into mp_lookup_code (lookupmodule, lookupcode) select 'PARTNER_TYPE'
                                                          ,'COMPETITORS' from dual where not exists(select 1 from mp_lookup_code where lookupmodule = 'PARTNER_TYPE'
                                                                                                                                                 and lookupcode = 'COMPETITORS');
insert into mp_lookup_code (lookupmodule, lookupcode) select 'PARTNER_TYPE'
                                                          ,'COMPLEMENTARY_DATA_PARTNER' from dual where not exists(select 1 from mp_lookup_code where lookupmodule = 'PARTNER_TYPE'
                                                                                                                                                 and lookupcode = 'COMPLEMENTARY_DATA_PARTNER');

INSERT INTO data_provider_partner(partners_id,
                                  data_providers_id,
                                  Partner_Type)
   SELECT DISTINCT p2.id partners_id, p1.id data_providers_id, lc.id
     FROM staging_airtable.im_provider_partner p
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'PARTNER_TYPE'
                AND lc.lookupcode = 'PROVIDER_DATA_PARTNER'
          INNER JOIN data_provider p1 ON p1.Record_ID = p.Provider_RecordID
          INNER JOIN data_provider p2 ON p2.record_id = p.Partner
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_provider_partner pp
               WHERE     pp.partners_id = p1.id
                     AND pp.data_providers_id = p2.id
                     AND pp.Partner_Type = lc.id);

INSERT INTO data_provider_partner(partners_id,
                                  data_providers_id,
                                  Partner_Type)
   SELECT DISTINCT p2.id partners_id,p1.id data_providers_id, lc.id
     FROM staging_airtable.im_distribution_partner p
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'PARTNER_TYPE'
                AND lc.lookupcode = 'DISTRIBUTION_DATA_PARTNER'
          INNER JOIN data_provider p1 ON p1.Record_ID = p.Provider_RecordID
          INNER JOIN data_provider p2 ON p2.record_id = p.Partner
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_provider_partner pp
               WHERE     pp.partners_id = p1.id
                     AND pp.data_providers_id = p2.id
                     AND pp.Partner_Type = lc.id);

INSERT INTO data_provider_partner(partners_id,
                                  data_providers_id,
                                  Partner_Type)
   SELECT DISTINCT p2.id partners_id, p1.id data_providers_id, lc.id
     FROM staging_airtable.im_consumer_partner p
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'PARTNER_TYPE'
                AND lc.lookupcode = 'CONSUMER_DATA_PARTNER'
         INNER JOIN data_provider p1 ON p1.Record_ID = p.Provider_RecordID
         INNER JOIN data_provider p2 ON p2.record_id = p.Partner
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_provider_partner pp
               WHERE     pp.partners_id = p1.id
                     AND pp.data_providers_id = p2.id
                     AND pp.Partner_Type = lc.id);


INSERT INTO data_provider_partner(partners_id,
                                  data_providers_id,
                                  Partner_Type)
   SELECT DISTINCT p2.id partners_id, p1.id data_providers_id, lc.id
     FROM staging_airtable.im_provider_competitors p
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'PARTNER_TYPE'
                AND lc.lookupcode = 'COMPETITORS'
          INNER JOIN data_provider p1 ON p1.Record_ID = p.Provider_RecordID
          INNER JOIN data_provider p2 ON p2.record_id = p.Competitors
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_provider_partner pp
               WHERE     pp.partners_id = p1.id
                     AND pp.data_providers_id = p2.id
                     AND pp.Partner_Type = lc.id);


INSERT INTO data_provider_partner(partners_id,
                                  data_providers_id,
                                  Partner_Type)
   SELECT DISTINCT p2.id partners_id,p1.id data_providers_id, lc.id
     FROM staging_airtable.im_provider_complementary p
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'PARTNER_TYPE'
                AND lc.lookupcode = 'COMPLEMENTARY_DATA_PARTNER'
          INNER JOIN data_provider p1 ON p1.Record_ID = p.Provider_RecordID
          INNER JOIN data_provider p2 ON p2.record_id = p.Complementary
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_provider_partner pp
               WHERE     pp.partners_id = p1.id
                     AND pp.data_providers_id = p2.id
                     AND pp.Partner_Type = lc.id);
