delete from data_provider_articles where not  exists(select 1 from data_article a, data_provider dp where a.id = data_provider_articles.articles_id
and dp.record_id = data_provider_articles.data_providers_record_id);

insert into data_provider_articles(Articles_ID, data_providers_record_id,data_providers_id)
   select a.id, dp.record_id,dp.id
     from data_article a
          inner join staging_airtable.im_provider_article pa
             on pa.Article_RecordID = a.Record_ID
          inner join data_provider dp on dp.record_id = pa.provider
    where not exists
             (select 1
                from data_provider_articles pa
               where pa.Articles_ID = a.id and pa.data_providers_record_id = dp.record_id);

delete from data_provider_articles
      where exists
               (select 1
                  from data_article a
                 where     a.author_data_provider_record_id =
                           data_provider_articles.data_providers_record_id
                       and a.id = data_provider_articles.articles_id);
