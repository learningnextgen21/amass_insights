INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'EQUITY_STYLE' lookupmodule,
                    i.EquitiesStyle lookupcode,
                    i.EquitiesStyle description
               FROM staging_airtable.im_provider_equities_style i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'EQUITY_STYLE'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_provider_equities_style(data_provider_equities_style_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_equities_style rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'EQUITY_STYLE'
                AND lc.lookupcode = rac.EquitiesStyle
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_equities_style pa
               WHERE     pa.data_provider_equities_style_id = lc.id
                     AND pa.data_Providers_id = dp.id);
