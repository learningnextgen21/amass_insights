INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'EMAIL_ADDRESS_TYPE' lookupmodule,
                    i.Type lookupcode,
                    i.Type description
               FROM staging_airtable.stg_data_email_address i WHERE i.Type!=''
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'EMAIL_ADDRESS_TYPE'
                     AND c.lookupcode = a.lookupcode);

insert
	into
		datamarketplace.data_email_address(
                          type_id,
			              primary_mail,
                          active,
                          data_provider,
                          last_updated_date,
                          created_date,
                          email_address,
                          record_id,
                          user,
                          subscribed_to_alt_data_newsletter,
                          exclude_from_newsletter,
                          date_confirmed_subscription,
                          date_confirmed_unsubscription
                          )
		SELECT
      (select id from mp_lookup_code where lookupmodule = 'EMAIL_ADDRESS_TYPE' and description = dp.Type),
      case when dp.`Primary` = 'checked' then 1 else 0 end,
      case when dp.Active = 'Yes' then 1 else 0 end,
      ifnull(
     (select
       dp1.id
      from
       data_provider dp1
      where
       dp1.record_id = dp.`Data Provider - Record ID`),
     1)
     AS main_data_industry_id,
      case when ifnull(dp.`Last Updated Date`, '') <> '' then str_to_date(
                               dp.`Last Updated Date`,
                               '%m/%d/%Y') else null end AS last_updated_date,
      case when ifnull(dp.`Created Date`, '') <> '' then str_to_date(
                    dp.`Created Date`,
                     '%m/%d/%Y %h:%i%p') else null end
                     AS created_date,
      dp.`Email Address` AS email_address,
      dp.`Record ID` AS record_id,
      case when ifnull(dp.User, '') <> '' then
                    CAST(dp.User as UNSIGNED) else null END
                     AS user,
      case when dp.`Subscribed to Alt Data Newsletter` = 'checked' then 1 else 0 end,
      case when dp.`Exclude from Newsletters` = 'checked' then 1 else 0 END,
      case when ifnull(dp.`Date Confirmed Subscription to Alt Data Newsletter`, '') <> '' then str_to_date(
                    dp.`Date Confirmed Subscription to Alt Data Newsletter`,
                    '%m/%d/%Y %h:%i%p') else null end
                    AS date_confirmed_subscription,
      case when ifnull(dp.`Date Confirmed Unsubscription to Alt Data Newsletter`, '') <> '' then str_to_date(
                    dp.`Date Confirmed Unsubscription to Alt Data Newsletter`,
                    '%m/%d/%Y %h:%i%p') else null end
                    AS date_confirmed_unsubscription
			FROM
               staging_airtable.stg_data_email_address dp
              where
               not exists
                 (select
                   1
                  from
                   data_email_address
                   where
                   `Email Address` = data_email_address.email_address)
            and ifnull(`Email Address`, '') <> '';



update data_email_address , staging_airtable.stg_data_email_address as dp
set type_id=(select id from mp_lookup_code where lookupmodule = 'EMAIL_ADDRESS_TYPE' and description = dp.Type),
data_email_address.primary_mail=case when dp.`Primary` = 'checked' then 1 else 0 end,
data_email_address.active=case when dp.Active = 'Yes' then 1 else 0 end,
data_provider=ifnull(
     (select
       dp1.id
      from
       data_provider dp1
      where
       dp1.record_id = dp.`Data Provider - Record ID`),
1),
last_updated_date=case when ifnull(dp.`Last Updated Date`, '') <> '' then str_to_date(
                               dp.`Last Updated Date`,
                               '%m/%d/%Y') else null end,
 created_date=case when ifnull(dp.`Created Date`, '') <> '' then str_to_date(
                    dp.`Created Date`,
                     '%m/%d/%Y %h:%i%p') else null end,
email_address=dp.`Email Address` ,
record_id=dp.`Record ID`,
data_email_address.user=case when ifnull(dp.User, '') <> '' then
                    CAST(dp.User as UNSIGNED) else null END,
 subscribed_to_alt_data_newsletter=case when dp.`Subscribed to Alt Data Newsletter` = 'checked' then 1 else 0 end,
 exclude_from_newsletter=case when dp.`Exclude from Newsletters` = 'checked' then 1 else 0 END,
 date_confirmed_subscription=case when ifnull(dp.`Date Confirmed Subscription to Alt Data Newsletter`, '') <> '' then str_to_date(
                    dp.`Date Confirmed Subscription to Alt Data Newsletter`,
                    '%m/%d/%Y %h:%i%p') else null end,
date_confirmed_unsubscription=case when ifnull(dp.`Date Confirmed Unsubscription to Alt Data Newsletter`, '') <> '' then str_to_date(
                    dp.`Date Confirmed Unsubscription to Alt Data Newsletter`,
                    '%m/%d/%Y %h:%i%p') else null end
 where data_email_address.email_address = `Email Address` AND web_updated_date IS NULL;   
