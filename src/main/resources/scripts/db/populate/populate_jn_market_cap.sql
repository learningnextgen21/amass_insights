INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'EQUITY_MARKET_CAP' lookupmodule,
                    i.MarketCap lookupcode,
                    i.MarketCap description
               FROM staging_airtable.im_provider_market_cap i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'EQUITY_MARKET_CAP'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_provider_market_cap(data_provider_market_cap_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_market_cap rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'EQUITY_MARKET_CAP'
                AND lc.lookupcode = rac.MarketCap
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_market_cap pa
               WHERE     pa.data_provider_market_cap_id = lc.id
                     AND pa.data_Providers_id = dp.id);
