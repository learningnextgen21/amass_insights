-- TRUNCATE TABLE data_organization;
update datamarketplace.data_organization, staging_airtable.stg_data_organization
    set data_organization.name = stg_data_organization.`Organization Name`,
      data_organization.website = stg_data_organization.Website,
      data_organization.description = stg_data_organization.Description,
      data_organization.City = stg_data_organization.City,
      data_organization.State = stg_data_organization.State,
      data_organization.Country = stg_data_organization.Country,
      data_organization.Zip_Code = stg_data_organization.`Zip Code`,
      data_organization.Year_Founded = CASE WHEN `Year Founded` = '' THEN NULL ELSE `Year Founded` END,
      data_organization.Headcount = stg_data_organization.Headcount,
      data_organization.Notes = stg_data_organization.Notes,
      data_organization.financial_position = `Financial Position`,
      data_organization.client_base = `Client Base`,
      data_organization.inactive = case when `Inactive?` = 'checked' then 1 else 0 end,
      created_date = str_to_date(`created time`, '%m/%d/%Y %h:%i %p'),
  data_organization.phone_number=`Phone Number`,
  data_organization.year_exit=CASE WHEN `Exit Year` = '' THEN NULL ELSE `Exit Year` END,
  data_organization.headcount_number=`Headcount Number`,
  data_organization.headcount_bucket_id=(select id from mp_lookup_code where lookupmodule = 'HEADCOUNT_BUCKET' and description = `Headcount Bucket`),
  data_organization.marketplace_status_id=(select id from mp_lookup_code where lookupmodule = 'MARKETPLACE_STATUS' and description =`Marketplace Status`),
  data_organization.iei_status_id=(select id from mp_lookup_code where lookupmodule = 'IEI_STATUS' and description = `IEI Status`),
  data_organization.last_updated_date=case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(
     `Last Updated Date`,
     '%m/%d/%Y') else null end
where record_id = `Record ID`;

INSERT INTO data_organization(ID,
                              Record_ID,
                              Website,
                              Description,
                              Parent_Organization_ID,
                              City,
                              State,
                              Country,
                              Zip_Code,
                              Year_Founded,
                              Headcount,
                              Notes,
                              name,
                              phone_number,
                              year_exit,
                              headcount_bucket_id,
                              headcount_number,
                                created_date)
     select 1,
             'RECORD_NULL',
             'DEFAULT_NULL',
             'DEFAULT_NULL',
             NULL,
             'CITY_NULL',
             'STATE_NULL',
             'COUNTRY_NULL',
             'ZIPCODE_NULL',
             2000,
             NULL,
             'NOTES_NULL',
             'NAME_NULL',
             'DEFAULT_NULL',
             'DEFAULT_NULL',
             'DEFAULT_NULL',
             'DEFAULT_NULL',
             CURRENT_TIMESTAMP from dual where not exists(select 1 from data_organization where id = 1);

INSERT INTO data_organization(Record_ID,
                              name,
                              Website,
                              Description,
                              Parent_Organization_ID,
                              City,
                              State,
                              Country,
                              Zip_Code,
                              Year_Founded,
                              Headcount,
                              Notes,
                              financial_position,
                              client_base,
                              inactive,
                              created_date,
                              phone_number,
                              year_exit,
                              headcount_number,
                              headcount_bucket_id,
                              marketplace_status_id,
                              iei_status_id,
                              last_updated_date
                              )
   SELECT `Record ID`,
          `Organization Name`,
          -- `Data Providers Owned`,
          -- `Tools Owned`,
          Website,
          Description,
          1,
          City,
          State,
          Country,
          `Zip Code`,
          CASE WHEN `Year Founded` = '' THEN NULL ELSE `Year Founded` END,
          Headcount,
          Notes,
          `Financial Position`,
          `Client Base`,
          case when `Inactive?` = 'checked' then 1 else 0 end,
          str_to_date(`created time`, '%m/%d/%Y %h:%i %p') created_date,
          `Phone Number`,
          CASE WHEN `Exit Year` = '' THEN NULL ELSE `Exit Year` END,
          `Headcount Number`,
          (select id from mp_lookup_code where lookupmodule = 'HEADCOUNT_BUCKET' and description = `Headcount Bucket`),
          (select id from mp_lookup_code where lookupmodule = 'MARKETPLACE_STATUS' and description = `Marketplace Status`),
    (select id from mp_lookup_code where lookupmodule = 'IEI_STATUS' and description = `IEI Status`),
  case when ifnull(`do`.`Last Updated Date`, '') <> '' then str_to_date(
     `Last Updated Date`,
     '%m/%d/%Y') else null end
     FROM staging_airtable.stg_data_organization do where ifnull(`Record ID`, '') <> ''
and not exists(select 1 from data_organization o where o.record_id = do.`Record ID`);

delete from jn_orgmapping;

insert into jn_orgmapping
   SELECT po.ID AS ParentOrganization,
          po.name AS `Parent Organization`,
          o.id AS OrganizationID,
          o.name
     FROM data_organization o
          INNER JOIN staging_airtable.stg_data_organization s
             ON     o.record_id = s.`record id`
                AND ifnull(s.`Parent Organization - Record ID`, '') <> ''
          INNER JOIN data_organization po
             ON po.record_id = s.`Parent Organization - Record ID`
    WHERE po.id <> o.id;

UPDATE data_organization inner join staging_airtable.stg_data_organization
    on record_id = `Record ID`
   SET Parent_Organization_ID = NULL;

UPDATE data_organization
       INNER JOIN jn_orgmapping
          ON jn_orgmapping.OrganizationID = data_organization.ID
   SET Parent_Organization_ID = jn_orgmapping.ParentOrganization;
   
  UPDATE
    data_organization
SET
record_id = CASE record_id WHEN '' THEN NULL ELSE record_id END,
name = CASE name WHEN '' THEN NULL ELSE name END,
city = CASE city WHEN '' THEN NULL ELSE city END,
state = CASE state WHEN '' THEN NULL ELSE state END,
country = CASE country WHEN '' THEN NULL ELSE country END,
zip_code = CASE zip_code WHEN '' THEN NULL ELSE zip_code END,
year_founded = CASE year_founded WHEN '' THEN NULL ELSE year_founded END,
Headcount = CASE Headcount WHEN '' THEN NULL ELSE Headcount END,
live = CASE live WHEN '' THEN NULL ELSE live END,
year_exit = CASE year_exit WHEN '' THEN NULL ELSE year_exit END,
headcount_bucket_id = CASE headcount_bucket_id WHEN '' THEN NULL ELSE headcount_bucket_id END,
headcount_number = CASE headcount_number WHEN '' THEN NULL ELSE headcount_number END,
creator_user_id = CASE creator_user_id WHEN '' THEN NULL ELSE creator_user_id END,
editor_user_id = CASE editor_user_id WHEN '' THEN NULL ELSE editor_user_id END,
marketplace_status_id = CASE marketplace_status_id WHEN '' THEN NULL ELSE marketplace_status_id END,
research_methods_completed_id = CASE research_methods_completed_id WHEN '' THEN NULL ELSE research_methods_completed_id END,
iei_status_id=CASE iei_status_id WHEN '' THEN NULL ELSE iei_status_id END   