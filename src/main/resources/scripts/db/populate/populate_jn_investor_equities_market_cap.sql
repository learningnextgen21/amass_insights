DELETE from data_investor_equities_market_cap
where not exists(select 1 from data_inv_managers dp, mp_lookup_code lc where dp.id = data_investor_equities_market_cap.investor_managers_id and lc.id = data_investor_equities_market_cap.equities_market_cap_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'EQUITY_MARKET_CAP' lookupmodule,
                    i.equities_market_cap lookupcode,
                    i.equities_market_cap description
               FROM staging_airtable.im_investor_equities_market_cap i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'EQUITY_MARKET_CAP'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_investor_equities_market_cap(equities_market_cap_id,
                                             investor_managers_id)
   SELECT lc.id, dp.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_equities_market_cap rac
             ON rac.investment_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'EQUITY_MARKET_CAP'
                AND lc.lookupcode = rac.equities_market_cap
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_equities_market_cap pa
               WHERE     pa.equities_market_cap_id = lc.id
                     AND pa.investor_managers_id = dp.id);
