INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'PAYMENT_METHOD' lookupmodule,
                    i.Payment lookupcode,
                    i.Payment description
               FROM staging_airtable.im_provider_payment_method i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'PAYMENT_METHOD'
                     AND c.lookupcode = a.lookupcode);



INSERT INTO data_provider_payment_method(data_provider_payment_method_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_payment_method rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'PAYMENT_METHOD'
                AND lc.lookupcode = rac.Payment
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_payment_method pa
               WHERE     pa.data_provider_payment_method_id = lc.id
                     AND pa.data_Providers_id = dp.id);
