TRUNCATE TABLE data_provider_industry;
delete from data_provider_industry where not exists(select 1 from data_provider p, data_industry di where p.id = data_provider_industry.data_providers_id
  and di.id = data_provider_industry.industries_id
);
INSERT INTO data_provider_industry(data_Providers_ID, Industries_ID)
   SELECT DISTINCT p.id, di.id
     FROM staging_airtable.im_industry_associated_providers ip
          INNER JOIN data_provider p ON ip.Provider_RecordID = p.record_id
          INNER JOIN data_industry di ON di.Record_ID = ip.Industry_RecordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_industry jap
               WHERE     jap.data_Providers_ID = p.id
                     AND jap.Industries_ID = di.id);

create table temp_main_data_industry_provider_count as select dp.main_data_industry_id, count(*) count from data_provider dp
group by dp.main_data_industry_id;

create table temp_relevant_data_industry_provider_count as select di.id relevant_industry_id, count(*) count from datamarketplace.data_provider_industry pi inner join data_industry di ON pi.industries_id = di.id
group by di.id;

update data_industry, temp_main_data_industry_provider_count
set main_data_industry_provider_count = temp_main_data_industry_provider_count.count
where temp_main_data_industry_provider_count.main_data_industry_id = data_industry.id;

update data_industry, temp_relevant_data_industry_provider_count
set relevant_data_industry_provider_count = temp_relevant_data_industry_provider_count.count
where temp_relevant_data_industry_provider_count.relevant_industry_id = data_industry.id;

drop table if exists temp_main_data_industry_provider_count;
drop table if exists temp_relevant_data_industry_provider_count;
