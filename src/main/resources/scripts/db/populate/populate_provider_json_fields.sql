UPDATE data_provider SET feature = (select cast(concat('[', group_concat('{"id":"', df.id, '", "desc":"', df.data_feature, '"}'), ']') as json) feature
from data_feature df inner join data_provider_feature dpf on df.id = dpf.features_id where data_provider.id = dpf.data_providers_id);
update data_provider set feature = cast('[]' as json) where feature is null;

-- Update Industries
UPDATE data_provider set industry = (select cast(concat('[', group_concat('{"id":"', di.id, '", "desc":"', di.data_industry, '"}'), ']') as json) industry
from data_industry di inner join data_provider_industry dpi on di.id = dpi.industries_id where data_provider.id = dpi.data_providers_id);

-- Update Provider Category
update data_provider set category = (select cast(concat('[', group_concat('{"id":"', dc.id, '", "desc":"', dc.datacategory, '"}'), ']') as json) category
from data_category dc inner join data_provider_category dpc on dc.id = dpc.categories_id where dpc.data_providers_id = data_provider.id);

-- Update Provider Tag
UPDATE data_provider SET tag = (select cast(concat('[', group_concat('{"id":"', dt.id, '", "desc":"', dt.provider_tag, '"}'), ']') as json) tag
from data_provider_tag dt inner join data_provider_provider_tag dpt on dt.id = dpt.provider_tags_id where dpt.data_providers_id = data_provider.id);

-- Update Provider Source
UPDATE data_provider SET source = (select cast(concat('[', group_concat('{"id":"', ds.id, '", "desc":"', ds.data_source, '"}'), ']') as json) source
from data_source ds inner join data_provider_source dps on ds.id = dps.sources_id where dps.data_providers_id = data_provider.id);

-- Update Provider Asset Classes
UPDATE data_provider SET asset_class = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) asset_class
from mp_lookup_code l inner join data_provider_asset_class ac on l.id = ac.asset_classes_id where ac.data_providers_id = data_provider.id);

-- Update Provider Security Type
UPDATE data_provider SET security_type = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) security_type
from mp_lookup_code l inner join data_provider_security_type ac on l.id = ac.security_types_id where ac.data_providers_id = data_provider.id);

-- Update Provider Delivery Format
UPDATE data_provider SET delivery_format = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) delivery_format
from mp_lookup_code l inner join data_provider_delivery_format ac on l.id = ac.delivery_formats_id where ac.data_providers_id = data_provider.id);

-- Update Provider Delivery Method
UPDATE data_provider SET delivery_method = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) delivery_method
from mp_lookup_code l inner join data_provider_delivery_method ac on l.id = ac.delivery_methods_id where ac.data_providers_id = data_provider.id);

-- Update Relevant Section
UPDATE data_provider SET relevant_sector = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) relevant_sector
from mp_lookup_code l inner join data_provider_relevant_sector ac on l.id = ac.relevant_sectors_id where ac.data_providers_id = data_provider.id);

UPDATE data_links SET link_category = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) link_category
from mp_lookup_code l inner join data_link_category lc on l.id = link_category_id where lc.link_id = data_links.id);

UPDATE data_event set industry = (select cast(concat('[', group_concat('{"id":"', di.id, '", "desc":"', di.data_industry, '"}'), ']') as json) industry
from data_industry di inner join data_event_industry dpi on di.id = dpi.industries_id where data_event.id = dpi.event_id);

UPDATE data_event set topic = (select cast(concat('[', group_concat('{"id":"', di.id, '", "desc":"', di.topic_name, '"}'), ']') as json) topic
from data_topic di inner join data_events_topic dpi on di.id = dpi.topics_id where data_event.id = dpi.data_events_id);

UPDATE data_event set category = (select cast(concat('[', group_concat('{"id":"', di.id, '", "desc":"', di.datacategory, '"}'), ']') as json) category
from data_category di inner join data_events_category dpi on di.id = dpi.event_category_id where data_event.id = dpi.event_id);

UPDATE data_event SET event_type = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) event_type
from mp_lookup_code l inner join data_event_type lc on l.id = lc.event_type_id where lc.data_event_id = data_event.id);

UPDATE data_event SET pricing_model = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) pricing_model
from mp_lookup_code l inner join data_event_pricing_model ac on l.id = ac.pricing_models_id where ac.data_events_id = data_event.id);

