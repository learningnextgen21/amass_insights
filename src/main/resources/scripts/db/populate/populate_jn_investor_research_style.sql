DELETE from data_investor_research_style
where not exists(select 1 from data_inv_managers dp, mp_lookup_code lc where dp.id = data_investor_research_style.investor_managers_id and lc.id = data_investor_research_style.research_style_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'INVESTMENT_MANAGER_RESEARCH_STYLE' lookupmodule,
                    i.research_style lookupcode,
                    i.research_style description
               FROM staging_airtable.im_investor_research_styles i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'INVESTMENT_MANAGER_RESEARCH_STYLE'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_investor_research_style(research_style_id,
                                             investor_managers_id)
   SELECT lc.id, dp.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_research_styles rac
             ON rac.investment_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'INVESTMENT_MANAGER_RESEARCH_STYLE'
                AND lc.lookupcode = rac.research_style
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_research_style pa
               WHERE     pa.research_style_id = lc.id
                     AND pa.investor_managers_id = dp.id);
