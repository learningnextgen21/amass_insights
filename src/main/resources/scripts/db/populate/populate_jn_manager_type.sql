INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'INVESTMENT_MANAGER_TYPE' lookupmodule,
                    i.ManagerType lookupcode,
                    i.ManagerType description
               FROM staging_airtable.im_provider_manager_type i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'INVESTMENT_MANAGER_TYPE'
                     AND c.lookupcode = a.lookupcode);
INSERT INTO data_provider_manager_type(data_provider_manager_type_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_manager_type rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'INVESTMENT_MANAGER_TYPE'
                AND lc.lookupcode = rac.ManagerType
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_manager_type pa
               WHERE     pa.data_provider_manager_type_id = lc.id
                     AND pa.data_Providers_id = dp.id);
