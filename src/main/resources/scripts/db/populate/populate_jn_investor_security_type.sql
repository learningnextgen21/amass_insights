DELETE from data_investor_security_type
where not exists(select 1 from data_inv_managers dp, mp_lookup_code lc where dp.id = data_investor_security_type.investor_managers_id and lc.id = data_investor_security_type.security_type_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'RLVNT_SECURITY_TYPE' lookupmodule,
                    i.security_type lookupcode,
                    i.security_type description
               FROM staging_airtable.im_investor_security_types i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'RLVNT_SECURITY_TYPE'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_investor_security_type(security_type_id, investor_managers_id)
   SELECT lc.id, dp.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_security_types rit
             ON rit.investment_recordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'RLVNT_SECURITY_TYPE'
                AND lc.lookupcode = rit.security_type
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_investor_security_type pa
               WHERE     pa.security_type_id = lc.id
                     AND pa.investor_managers_id = dp.id);