-- truncate table data_feature;
update datamarketplace.data_feature df, staging_airtable.stg_data_feature sf
		SET Data_Feature = `Data Features`,
			df.Explanation = sf.Explanation,
			df.inactive = 0,
			df.created_date = str_to_date(sf.`date created`, '%m/%d/%Y %h:%i %p')
					where df.record_id = sf.`Record ID`;
insert
	into
		data_feature(
			Record_ID,
			Data_Feature,
			Explanation,
			inactive,
			created_date
		) select
			`Record ID`,
			`Data Features`,
			Explanation,
			0 inactive,
			str_to_date(`date created`, '%m/%d/%Y %h:%i %p') created_date
		from
			staging_airtable.stg_data_feature sdf where not exists(select 1 from data_feature df where df.record_id = sdf.`Record ID`);

update data_feature, (
                  select @rownum := @rownum + 1 AS sort_order, a.*
                  FROM (SELECT sdf.`Record ID`, sdf.`Data Features`
                        FROM staging_airtable.stg_data_feature sdf
                        ORDER BY sdf.`Data Features`) a,
                      (SELECT @rownum := 0) r) a
set data_feature.sort_order = a.sort_order
where `Record ID` = record_id;
