INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'LANGUAGE' lookupmodule,
                    i.Languages lookupcode,
                    i.Languages description
               FROM staging_airtable.im_provider_language i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'LANGUAGE'
                     AND c.lookupcode = a.lookupcode);


INSERT INTO data_provider_languages(data_provider_languages_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_language rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'LANGUAGE'
                AND lc.lookupcode = rac.Languages
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_languages pa
               WHERE     pa.data_provider_languages_id = lc.id
                     AND pa.data_Providers_id = dp.id);
