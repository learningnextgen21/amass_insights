INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'ORGANIZATION_TAG' lookupmodule,
                    i.OrganizationType lookupcode,
                    i.OrganizationType description
               FROM staging_airtable.im_provider_target_organization i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'ORGANIZATION_TAG'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_provider_target_organization(data_provider_target_organization_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_target_organization rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'ORGANIZATION_TAG'
                AND lc.lookupcode = rac.OrganizationType
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_target_organization pa
               WHERE     pa.data_provider_target_organization_id = lc.id
                     AND pa.data_Providers_id = dp.id);
