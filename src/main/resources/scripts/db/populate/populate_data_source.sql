-- truncate table data_source;
update datamarketplace.data_source, staging_airtable.stg_data_source
set data_source = Source,
	data_source.explanation = stg_data_source.Explanation,
	inactive = 0,
	created_date = str_to_date(`date created`, '%m/%d/%Y %h:%i %p')
		where record_id = `Record ID`;

INSERT INTO data_source
(
	record_id,
	`Data_Source`,
	`Explanation`,
	inactive,
	created_date)
	SELECT
		`record id`,
		`Source`,
		`Explanation`,
		0 inactive,
		str_to_date(`date created`, '%m/%d/%Y %h:%i %p') created_date
	FROM `staging_airtable`.`stg_data_source` sds WHERE not exists (select 1 from data_source ds where ds.record_id = sds.`Record ID`);

update data_source, (
                  select @rownum := @rownum + 1 AS sort_order, a.*
                  FROM (SELECT sds.`Record ID`, sds.`Source`
                        FROM staging_airtable.stg_data_source sds
                        ORDER BY sds.`Source`) a,
                      (SELECT @rownum := 0) r) a
set data_source.sort_order = a.sort_order
where `Record ID` = record_id;
