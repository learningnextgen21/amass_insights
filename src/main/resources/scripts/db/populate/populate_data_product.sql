-- truncate table data_product;

INSERT INTO
 data_product(
   ProductName
  ,Website
  ,ShortDescription
  ,LongDescription
  ,SupplierID
  ,CreatorID
  ,DistributorID
  ,MainDataIndustryID
  ,MainDataCategoryID
  ,CollectionMethods
  ,ProductType
  ,PricingModel
  ,PricingTime
  ,Pricing
  ,NumLicenses
  ,FreeTrial
  ,FreeTrialLength
  ,DataType
  ,DeliveryMethod
  ,UpdateFrequency
  -- ,UpdateFrequencyDetails
  ,DateCollectionBegun
  ,DateRangeDetails
  ,SampleSize
  -- ,SampleSizeDetails
  ,KeyFields
  ,GeographicalFocus
  ,RelevantAssetClasses
  --  ,InvestorType
  -- ,Biases
  ,UseCases
  ,Notes
  -- ,ApprovalStatus
  ,Inactive
  ,Live
  ,CreatedDate
  ,record_id)
  SELECT
   `Product Name`
  ,Link
     website
  ,`Short Description`
  ,`Long Description`
  ,ifnull(
     (SELECT
       o.OrganizationID
      FROM
       data_organization o
      WHERE
       Supplier = o.name
      LIMIT
       1)
    ,1)
     SupplierID
  ,ifnull(
     (SELECT
       o.OrganizationID
      FROM
       data_organization o
      WHERE
       Creator = o.name
      LIMIT
       1)
    ,1)
     CreatorID
  ,ifnull(
     (SELECT
       o.OrganizationID
      FROM
       data_organization o
      WHERE
       Distributor = o.name
      LIMIT
       1)
    ,1)
     DistributorID
  ,1
     MainDataIndustryID
  ,1
     MainDataCategoryID
  -- ,`Data Features`
  -- ,`Data Sources`
  ,ifnull(
     `Collection Methods Explanation`
    ,'')
  ,`Product Type`
  ,ifnull(
     `Pricing Model`
    ,'')
  ,ifnull(
     `Pricing - Time Period`
    ,1)
     PricingTime
  -- ,`Pricing - Units`
  ,Pricing
  ,ifnull(
     (case when `# of Licenses` = '' then null else `# of Licenses` end)
    ,0)
  ,ifnull(
     CASE WHEN `Free Trial@f0` = 'checked' THEN 1 ELSE 0 END
    ,0)
  ,ifnull(
     `Free Trial Length`
    ,'')
  ,`Data Type`
  ,`Delivery Method`
  ,`Update Frequency`
  ,case when `Date collection Begun` = '' then null else str_to_date(
     `Date Collection Begun` 
    ,'%m/%d/%Y') end
  ,ifnull(
     `Date Range Explanation`
    ,'')
  ,`Sample Size`
  ,ifnull(
     `Key Fields`
    ,'')
  ,ifnull(
     `Geographies Covered`
    ,'')
  ,`Asset Classes`
  -- ,`Number of Fields`
  --  ,`# Records`
  -- ,`Records Explanation`
  -- ,`Size (in gb)`

  --  ,Sectors
  -- ,`Market Cap`
  -- ,`Regions Covered`

  -- ,`Limitations/Drawbacks`
  ,ifnull(
     `Use Cases`
    ,'')
  ,ifnull(
     Notes
    ,'')
  ,0
  ,1
  ,case when `date created` = '' then null else str_to_date(`date created`, '%m/%d/%Y %h:%i%p') end
  ,`Record ID`
  FROM
   staging_airtable.stg_data_product;

SET @@FOREIGN_KEY_CHECKS = 1;
