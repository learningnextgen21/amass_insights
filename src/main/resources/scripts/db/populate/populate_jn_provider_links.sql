--TRUNCATE TABLE data_provider_feature;
DELETE from data_provider_links where not exists(select 1 from data_provider dp, mp_lookup_code lc where dp.id = data_provider_links.data_providers_id and lc.id = data_provider_links.links_id);
INSERT INTO data_provider_links(links_id, data_Providers_ID)
   SELECT DISTINCT df.id, dp.id
     FROM staging_airtable.im_provider_links pf
          INNER JOIN data_provider dp ON dp.Record_ID = pf.Provider_RecordID
          INNER JOIN data_links df ON df.record_id = pf.Link_RecordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_links jpf
               WHERE     jpf.links_id = df.id
                     AND jpf.data_Providers_ID = dp.id);

create table temp_links_provider_count as select df.id, count(*) count from data_provider_links dpf inner join datamarketplace.data_links df ON dpf.links_id = df.id
group by df.id;

update data_links, temp_links_provider_count
set provider_count = temp_links_provider_count.count
where temp_links_provider_count.id = data_links.id;

drop table if exists temp_links_provider_count;
