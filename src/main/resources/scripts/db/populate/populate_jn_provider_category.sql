delete from data_provider_category where not exists(select 1 from data_provider dp, data_category dc where dp.id = data_provider_category.data_providers_id and dc.id = data_provider_category.categories_id);
INSERT INTO data_provider_category(data_providers_ID, Categories_ID)
   SELECT dp.id, dc.id
     FROM staging_airtable.im_provider_category pc
          INNER JOIN data_provider dp ON pc.Provider_RecordID = dp.Record_ID
          INNER JOIN data_category dc ON pc.category = dc.record_id
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_category jpc
               WHERE     jpc.Categories_ID = dc.id
                     AND jpc.data_providers_ID = dp.id);

create table temp_main_data_category_provider_count as select dp.main_data_category_id, count(*) count from data_provider dp
group by dp.main_data_category_id;

create table temp_relevant_data_category_provider_count as select c.id relevant_category_id, count(*) count from data_provider_category pc inner join data_category c on pc.categories_id = c.id
group by c.id;

update data_category, temp_main_data_category_provider_count
set main_data_category_provider_count = temp_main_data_category_provider_count.count
where temp_main_data_category_provider_count.main_data_category_id = data_category.id;

update data_category, temp_relevant_data_category_provider_count
set relevant_data_category_provider_count = temp_relevant_data_category_provider_count.count
where temp_relevant_data_category_provider_count.relevant_category_id = data_category.id;

drop table if exists temp_main_data_category_provider_count;
drop table if exists temp_relevant_data_category_provider_count;

update data_category set
    new_main_provider_id = (select id from data_provider where main_data_category_id = data_category.id order by created_date desc limit 1);

update data_category set
    new_main_provider_date = (select dp.created_date from data_provider dp where main_data_category_id = data_category.id order by created_date desc limit 1);

update data_category set
    new_relevant_provider_id = (select dp.id from data_provider dp
inner join data_provider_category category ON dp.id = category.data_providers_id
    where category.categories_id = data_category.id order by dp.created_date desc limit 1);

update data_category set
    new_relevant_provider_date = (select dp.created_date from data_provider dp
        inner join data_provider_category category ON dp.id = category.data_providers_id
    where category.categories_id = data_category.id order by dp.created_date desc limit 1);

update data_category set new_main_provider_id = 0 where new_main_provider_id is null;

update data_category set new_relevant_provider_id = 0 where new_relevant_provider_id is null;
