DELETE from data_provider_geographical_focus where not exists(select 1 from data_provider dp, mp_lookup_code lc where dp.id = data_provider_geographical_focus.data_providers_id and lc.id = data_provider_geographical_focus.geographical_foci_id);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'GEOGRAPHICAL_FOCUS' lookupmodule,
                    i.geographical_focus lookupcode,
                    i.geographical_focus description
               FROM staging_airtable.im_geographical_focus i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'GEOGRAPHICAL_FOCUS'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_provider_geographical_focus(geographical_foci_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_geographical_focus rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'GEOGRAPHICAL_FOCUS'
                AND lc.lookupcode = rac.geographical_focus
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_geographical_focus pa
               WHERE     pa.geographical_foci_id = lc.id
                     AND pa.data_Providers_ID = dp.id);

create table temp_geographical_focus_provider_count as select mlc.id, count(*) count from data_provider_geographical_focus dpgf inner join mp_lookup_code mlc on dpgf.geographical_foci_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_geographical_focus_provider_count
set record_count = temp_geographical_focus_provider_count.count
where temp_geographical_focus_provider_count.id = mp_lookup_code.id;

drop table if exists temp_geographical_focus_provider_count;
