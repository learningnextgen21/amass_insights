truncate table data_comment_purpose_type;
delete from data_comment_purpose_type where not exists(select 1 from
  data_comments da, data_resource_purpose_type drp
where da.id = data_comment_purpose_type.comment_id
      and drp.id = data_comment_purpose_type.purpose_type_id
);
INSERT INTO data_comment_purpose_type(comment_id, purpose_type_id)
   SELECT DISTINCT  da.id, dt.id
     FROM staging_airtable.im_comment_purpose_type at
          INNER JOIN data_resource_purpose_type dt ON dt.record_id = at.Purpose_Type_RecordID
         inner join data_comments da on da.record_id = at.Comment_RecordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_comment_purpose_type dat
               WHERE     dat.comment_id = da.id
                     AND dat.purpose_type_id = dt.id);
