TRUNCATE TABLE data_organization_research_methods;
DELETE from data_organization_research_methods where not exists(select 1 from data_organization dp, mp_lookup_code lc where dp.id = data_organization_research_methods.organization_id and lc.id = data_organization_research_methods.research_methods_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'RESEARCH_METHOD' lookupmodule,
                    i.research_methods lookupcode,
                    i.research_methods description
               FROM staging_airtable.im_organization_research_methods i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'RESEARCH_METHOD'
                     AND c.lookupcode = lookupcode);

INSERT INTO data_organization_research_methods(research_methods_id,
                                          organization_id)
   SELECT lc.id, dp.id
     FROM data_organization dp
          INNER JOIN staging_airtable.im_organization_research_methods rac
             ON rac.Organization_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'RESEARCH_METHOD'
                AND lc.lookupcode = rac.research_methods
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_organization_research_methods pa
               WHERE     pa.research_methods_id = lc.id
                     AND pa.organization_id = dp.id);


create table temp_data_organization_research_methods as select mlc.id, count(*) count from data_organization_research_methods dpdf inner join mp_lookup_code mlc on dpdf.research_methods_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_data_organization_research_methods
set record_count = temp_data_organization_research_methods.count
where temp_data_organization_research_methods.id = mp_lookup_code.id;

drop table if exists temp_data_organization_research_methods;
