truncate table data_event_industry;
delete from data_event_industry where not exists(select 1 from
  data_event da, data_industry dt
where da.id = data_event_industry.event_id
      and dt.id = data_event_industry.industries_id
);
INSERT INTO data_event_industry(event_id, industries_id)
   SELECT DISTINCT p.id, di.id
     FROM staging_airtable.im_event_industries ip
          INNER JOIN data_event p ON ip.Industries_RecordID = p.record_id
          INNER JOIN data_industry di ON ip.industries = di.record_id
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_event_industry jap
               WHERE     jap.event_id = p.id
                     AND jap.industries_id = di.id);

create table temp_data_event_industry_count as select drp.id, count(*) count from datamarketplace.data_event_industry dap inner join datamarketplace.data_industry drp ON dap.industries_id = drp.id
group by drp.id;
update data_industry, temp_data_event_industry_count
set event_industry_count = temp_data_event_industry_count.count
where temp_data_event_industry_count.id = data_industry.id;

drop table if exists temp_data_event_industry_count;

update data_industry set event_industry_count = 0 where event_industry_count is null;
