-- truncate table data_provider_type;
update datamarketplace.data_provider_type, staging_airtable.stg_data_provider_type
		set Provider_Type = `Provider Type`,
			data_provider_type.Explanation = stg_data_provider_type.Explanation,
			inactive = 0,
			created_date = str_to_date(`date created`, '%m/%d/%Y %h:%i %p')
					where record_id = `Record ID`;
insert
	into
		data_provider_type(
			Record_ID,
			Provider_Type,
			Explanation,
			inactive,
			created_date
		) select
			`Record ID`,
			`Provider Type`,
			Explanation,
			0 inactive,
			str_to_date(`date created`, '%m/%d/%Y %h:%i %p') created_date
		from
			staging_airtable.stg_data_provider_type dpt where not exists(select 1 from data_provider_type pt where pt.record_id = dpt.`Record ID`);
