INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'BIASES' lookupmodule,
                    i.Biases lookupcode,
                    i.Biases description
               FROM staging_airtable.im_provider_biases i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'BIASES'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_biases(data_biases_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_biases rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'BIASES'
                AND lc.lookupcode = rac.Biases
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_biases pa
               WHERE     pa.data_biases_id = lc.id
                     AND pa.data_Providers_id = dp.id);

