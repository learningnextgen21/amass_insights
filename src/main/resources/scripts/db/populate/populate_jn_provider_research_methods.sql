TRUNCATE TABLE data_provider_research_methods;
DELETE from data_provider_research_methods where not exists(select 1 from data_provider dp, mp_lookup_code lc where dp.id = data_provider_research_methods.data_providers_id and lc.id = data_provider_research_methods.research_methods_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'RESEARCH_METHOD' lookupmodule,
                    i.research_methods lookupcode,
                    i.research_methods description
               FROM staging_airtable.im_research_methods_completed i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'RESEARCH_METHOD'
                     AND c.lookupcode = lookupcode);

INSERT INTO data_provider_research_methods(research_methods_id,
                                          data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_research_methods_completed rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'RESEARCH_METHOD'
                AND lc.lookupcode = rac.research_methods
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_research_methods pa
               WHERE     pa.research_methods_id = lc.id
                     AND pa.data_Providers_ID = dp.id);

create table temp_im_research_methods_count as select mlc.id, count(*) count from data_provider_research_methods dpdf inner join mp_lookup_code mlc on dpdf.research_methods_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_im_research_methods_count
set record_count = temp_im_research_methods_count.count
where temp_im_research_methods_count.id = mp_lookup_code.id;

drop table if exists temp_im_research_methods_count;
