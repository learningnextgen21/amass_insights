truncate table data_events_topic;
delete from data_events_topic where not exists(select 1 from
  data_event da, data_topic dt
where da.id = data_events_topic.data_events_id
      and dt.id = data_events_topic.topics_id
);

INSERT INTO data_events_topic(data_events_id, topics_id)
   SELECT DISTINCT  da.id, dt.id
     FROM staging_airtable.im_event_topic at
          INNER JOIN data_topic dt ON dt.record_id = at.topic
         inner join data_event da on da.record_id = at.Event_RecordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_events_topic dat
               WHERE     dat.data_events_id = da.id
                     AND dat.topics_id = dt.id);
create table temp_data_event_topic_count as select drp.id, count(*) count from datamarketplace.data_events_topic dap inner join datamarketplace.data_topic drp ON dap.topics_id = drp.id
group by drp.id;
update data_topic, temp_data_event_topic_count
set event_count = temp_data_event_topic_count.count
where temp_data_event_topic_count.id = data_topic.id;

drop table if exists temp_data_event_topic_count;

update data_topic set event_count = 0 where event_count is null;
