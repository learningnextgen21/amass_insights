DELETE from data_emails_purposes
where not exists(select 1 from data_emails dp, data_resource_purpose lc where dp.id = data_emails_purposes.email_id and lc.id = data_emails_purposes.purpose_id);

INSERT INTO data_emails_purposes(purpose_id, email_id)
   SELECT DISTINCT df.id, dp.id
     FROM staging_airtable.im_emails_purposes pf
          INNER JOIN data_emails dp ON dp.record_id = pf.emails_recordID
          INNER JOIN data_resource_purpose df ON df.record_id = pf.purposes_recordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_emails_purposes jpf
               WHERE     jpf.purpose_id = df.id
                     AND jpf.email_id = dp.id);