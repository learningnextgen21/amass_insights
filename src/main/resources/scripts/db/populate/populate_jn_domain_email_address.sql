DELETE from data_domains_email_addresses
where not exists(select 1 from data_domains dp, data_email_address lc where dp.id = data_domains_email_addresses.domain_id and lc.id = data_domains_email_addresses.email_address_id);
    
INSERT INTO data_domains_email_addresses(email_address_id, domain_id)
   SELECT DISTINCT df.id, dp.id
     FROM staging_airtable.im_domain_email_address pf
          INNER JOIN data_domains dp ON dp.record_id = pf.domains_recordID
          INNER JOIN data_email_address df ON df.record_id = pf.email_address_recordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_domains_email_addresses jpf
               WHERE     jpf.email_address_id = df.id
                     AND jpf.domain_id = dp.id);