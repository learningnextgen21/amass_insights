DELETE from data_provider_delivery_frequency where not exists(select 1 from data_provider dp, mp_lookup_code lc where dp.id = data_provider_delivery_frequency.data_providers_id and lc.id = data_provider_delivery_frequency.delivery_frequencies_id);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'DELIVERY_FREQUENCY' lookupmodule,
                    i.frequency lookupcode,
                    i.frequency description
               FROM staging_airtable.im_provider_delivery_frequency i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'DELIVERY_FREQUENCY'
                     AND c.lookupcode = lookupcode);

INSERT INTO data_provider_delivery_frequency(DELIVERY_frequencies_id,                                            data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_delivery_frequency rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'DELIVERY_FREQUENCY'
                AND lc.lookupcode = rac.frequency
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_delivery_frequency pa
               WHERE     pa.DELIVERY_frequencies_id = lc.id
                     AND pa.data_Providers_ID = dp.id);

create table temp_delivery_frequency_count as select mlc.id, count(*) count from data_provider_delivery_frequency dpdf inner join mp_lookup_code mlc on dpdf.delivery_frequencies_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_delivery_frequency_count
set record_count = temp_delivery_frequency_count.count
where temp_delivery_frequency_count.id = mp_lookup_code.id;

drop table if exists temp_delivery_frequency_count;
