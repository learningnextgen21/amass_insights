insert into mp_documents_metadata (
  File_ID,
  File_Path,
  File_Extension,
  Date_Created,
  Date_Modified,
  File_Weblink,
  File_Size,
  Last_Modified_By,
  File_Owners
)

    select   File_ID,
      File_Path,
      File_Extension,
      Date_Created,
      Date_Modified,
      File_Weblink,
      File_Size,
      Last_Modified_By,
      File_Owners from staging_airtable.gdrive_documents_metadata sdm
where not exists(select 1 from mp_documents_metadata dm where dm.File_ID = sdm.File_ID);
update mp_documents_metadata set File_Path = replace(File_Path, '/My Drive/', '') where File_Path like '/My Drive/%';
update mp_documents_metadata, data_provider
set provider_record_id = data_provider.record_id
where provider_name = substr(File_Path from length('/Partner Research/Partners/')+1 for locate('/', substr(File_Path from length('/Partner Research/Partners/')+1))-1) and File_Path like '/Partner Research/Partners/%'
      and provider_record_id is null;

update mp_documents_metadata, data_provider
set provider_record_id = data_provider.record_id
where provider_name = substr(File_Path from length('/Data Provider Partners/')+1 for locate('/', substr(File_Path from length('/Data Provider Partners/')+1))-1) and File_Path like '/Data Provider Partners/%'
      and provider_record_id is null;

update mp_documents_metadata set file_name = substring_index(File_Path, '/', -1);



