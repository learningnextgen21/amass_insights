truncate table data_comment_purpose;
delete from data_comment_purpose where not exists(select 1 from
  data_comments da, data_resource_purpose drp
where da.id = data_comment_purpose.comment_id
      and drp.id = data_comment_purpose.purpose_id
);

INSERT INTO data_comment_purpose(comment_id, purpose_id)
   SELECT DISTINCT  da.id, dt.id
     FROM staging_airtable.im_comment_purpose at
          INNER JOIN data_resource_purpose dt ON dt.record_id = at.Purpose_RecordID
         inner join data_comments da on da.record_id = at.Comment_RecordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_comment_purpose dat
               WHERE     dat.comment_id = da.id
                     AND dat.purpose_id = dt.id);
