INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'DATA_PRODUCT_TYPE' lookupmodule,
                    i.DataProductTypes lookupcode,
                    i.DataProductTypes description
               FROM staging_airtable.im_data_product_types i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'DATA_PRODUCT_TYPE'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_provider_data_product_types(data_product_type_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_data_product_types rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'DATA_PRODUCT_TYPE'
                AND lc.lookupcode = rac.DataProductTypes
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_data_product_types pa
               WHERE     pa.data_product_type_id = lc.id
                     AND pa.data_Providers_id = dp.id);
