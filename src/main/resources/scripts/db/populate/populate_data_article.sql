update data_article, staging_airtable.stg_article
set
  articleuniquekey = `Article Unique Key`
  ,data_article.Title = staging_airtable.stg_article.Title
  ,data_article.URL = staging_airtable.stg_article.URL
  ,Published_Date = str_to_date(`Published Date Formula`, '%m/%d/%Y')
  ,data_article.Content = staging_airtable.stg_article.Content
  ,data_article.Content_HTML = staging_airtable.stg_article.`Content HTML`
  ,data_article.Summary = staging_airtable.stg_article.Summary
  ,data_article.Author = staging_airtable.stg_article.Author
  ,data_article.Publisher = 'PUBLISHER_NULL'
  ,data_article.Summary_HTML = staging_airtable.stg_article.`Summary HTML`
  ,data_article.Relevancy = 0
  ,data_article.include_in_newsletter = case when stg_article.`Include in Newsletter` = 'checked' then true else false end
  ,data_article.stream = staging_airtable.stg_article.Stream
  ,data_article.visual_url = case when `Visual URL` = '' then null when ifnull(`Visual URL`,'none') <> 'none' then `Visual URL` else null end
  -- ,data_article.Purpose = (select purpose from staging_airtable.stg_resource_purpose p where p.`Record ID` =  staging_airtable.stg_article.`Record ID`)
  -- ,data_article.Purpose_Type = (select pt.`Purpose Type` from staging_airtable.stg_resource_purpose_type pt where pt.`Record ID` =  staging_airtable.stg_article.`Record ID`)
    ,data_article.author_data_provider_record_id =
    case when ifnull(`Author Data Provider - Record ID`, 'PROVIDER_NULL') = '' then 'PROVIDER_NULL' else `Author Data Provider - Record ID` end
  ,data_article.Author_Data_Provider_ID = ifnull(
    (SELECT
       dp.id
     FROM
       data_provider dp
     WHERE
       dp.record_id = `Author Data Provider - Record ID` limit 1)
    ,1)
  -- ,Author_Data_Tool_ID
  ,data_article.Author_Organization_ID = ifnull(
    (SELECT
       o.ID
     FROM
       data_organization o
     WHERE
       o.record_id = `Author Organization - Record ID`)
    ,1)
  ,data_article.created_date = str_to_date(`Created Date`, '%m/%d/%Y')
  ,data_article.updated_date = case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(`Last Updated Date`, '%m/%d/%Y') else null end
  ,data_article.paywall = case when staging_airtable.stg_article.paywall = 'checked' then 1 else 0 end
  ,data_article.broken_link = case when `Broken Link`= 'checked' then 1 else 0 end
where data_article.record_id = staging_airtable.stg_article.`Record ID`;

INSERT INTO
 data_article(
   articleuniquekey
  ,Record_ID
  ,Title
  ,URL
  ,Published_Date
  ,Content
  ,Content_HTML
  ,Summary
  ,Author
  ,Publisher
  ,Summary_HTML
  ,Relevancy
  ,stream
  -- ,Purpose
  -- ,Purpose_Type
  ,Author_Data_Provider_ID
  ,author_data_provider_record_id
  ,Visual_URL
  -- ,Author_Data_Tool_ID
  ,Author_Organization_ID
  ,created_date
  ,updated_date
  ,paywall
  ,broken_link
  ,include_in_newsletter)
  SELECT
   `Article Unique Key`
  ,`Record ID`
  ,Title
  ,URL
  ,str_to_date(`Published Date Formula`, '%m/%d/%Y')
  ,Content
  ,`Content HTML`
  ,Summary
  ,Author
  ,'PUBLISHER_NULL'
  ,`Summary HTML`
  , 0 Relevancy
  ,Stream
    -- ,`Linked Organizations`,
    -- `Linked Data Providers`,
    -- `Linked Data Tools`,
    --  (select purpose from staging_airtable.stg_resource_purpose p where p.`Record ID` =  a.`Record ID`) Purpose
  -- , (select pt.`Purpose Type` from staging_airtable.stg_resource_purpose_type pt where pt.`Record ID` =  a.`Record ID`) `Purpose Type` -- Topic,
  -- `Blog Link`,
  -- `Article Keywords`,
  ,ifnull(
     (SELECT
       dp.id
      FROM
       data_provider dp
      WHERE
       dp.record_id = a.`Author Data Provider - Record ID` limit 1)
    ,1)
     AS AuthorDataProviderID
  , case when ifnull(`Author Data Provider - Record ID`, 'PROVIDER_NULL') = '' then 'PROVIDER_NULL' else `Author Data Provider - Record ID` end
  , case when `Visual URL` = '' then null when ifnull(`Visual URL`,'none') <> 'none' then `Visual URL` else null end as visual_url
  -- ,1 AS AuthorDataToolID
  ,ifnull(
     (SELECT
       o.ID
      FROM
       data_organization o
      WHERE
       o.record_id = `Author Organization - Record ID`)
    ,1)
     AS AuthorOrganizationID,
  str_to_date(`Created Date`, '%m/%d/%Y') created_date,
  case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(`Last Updated Date`, '%m/%d/%Y') else null end last_updated,
  case when paywall = 'checked' then 1 else 0 end paywall,
  case when `Broken Link`= 'checked' then 1 else 0 end broken_link,
  case when `Include in Newsletter` = 'checked' then true else false end
  FROM
   staging_airtable.stg_article a where not exists (select 1 from data_article da where da.record_id = a.`Record ID`);

create table article_count_temp as select count(*) article_count from staging_airtable.stg_article;

delete from data_article where exists(select 1 from article_count_temp where article_count > 0)
                               and record_id not in (select a.`Record ID` from staging_airtable.stg_article a);

drop table if exists article_count_temp;

update data_article set author_data_provider_record_id = 'PROVIDER_NULL'
where author_data_provider_record_id = '';
