TRUNCATE TABLE data_organization_inv_managers;

INSERT INTO data_organization_inv_managers(inv_managers_id, organization_inv_managers_id)
   SELECT dp.id,lc.id
     FROM data_inv_managers dp
          INNER JOIN staging_airtable.im_investor_organization rac
             ON rac.Investment_RecordID = dp.record_id
          INNER JOIN data_organization lc
             ON 
                lc.record_id = rac.Owner_Organization_RecordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_organization_inv_managers pa
               WHERE     pa.organization_inv_managers_id = lc.id
                     AND pa.inv_managers_id = dp.id);