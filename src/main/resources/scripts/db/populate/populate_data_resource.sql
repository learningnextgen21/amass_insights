update data_resource, staging_airtable.stg_data_resources
    set record_id = `Record ID`,
    data_resource.name = stg_data_resources.Name,
    data_resource.description = stg_data_resources.Description,
    file_id = ifnull(
     (select
       id
      from
       mp_documents_metadata dm
      where
       dm.record_id = `File - Record ID`
      order by
       1
      limit
       1),
     NULL),
  link_id = ifnull(
     (select
       id
      from
       data_links dl
      where
       dl.record_id = `Link - Record ID`
      order by
       1
      limit
       1),
     NULL),
    author_organization_id =ifnull(
     (select
       id
      from
       data_organization do
      where
       do.record_id = `Author Organization - Record ID`
      order by
       1
      limit
       1),
     NULL),
    author_data_provider_id =ifnull(
     (select
       id
      from
       data_provider dp1
      where
       dp1.record_id = `Author Data Provider - Record ID`
      order by
       1
      limit
       1),
     NULL),
    data_resource.notes = stg_data_resources.Notes,
    priority_id =(select id from mp_lookup_code where lookupmodule = 'PRIORITY' and description = `Priority`),
    user_permission =case `User Permissions`
                   when 'Registered'	then 'ROLE_USER'
                   when 'Bronze'        then 'ROLE_BRONZE'
                   when 'Silver' 	    then 'ROLE_SILVER'
                   when 'Admin'         then 'ROLE_ADMIN'
                   when ''              then 'ROLE_ADMIN'
                   else `User Permissions`
                   end,
    marketplace_status_id =(select id from mp_lookup_code where lookupmodule = 'MARKETPLACE_STATUS' and description =`Marketplace Status`),
	 iei_status_id=(select id from mp_lookup_code where lookupmodule = 'IEI_STATUS' and description = `IEI Status`),
   last_updated_date = case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(`Last Updated Date`,'%m/%d/%Y') else null end,
   created_time_formula = case when ifnull(`Created Time Formula`, '') <> '' then str_to_date(`Created Time Formula`,'%m/%d/%Y %h:%i%p') else null end
          where record_id = `Record ID` AND web_updated_date IS NULL;

 INSERT INTO
 data_resource(
   record_id,
   name,
   description,
   file_id,
   link_id,
   author_organization_id,
   author_data_provider_id,
   notes,
   priority_id,
   user_permission,
   marketplace_status_id,
   iei_status_id,
   last_updated_date,
   created_time_formula
  )
  SELECT
   `Record ID`
     AS record_id,
   `Name`
    AS name,
 `Description`
AS description,
    ifnull(
     (select
       id
      from
       mp_documents_metadata dm
      where
       dm.record_id = `File - Record ID`
      order by
       1
      limit
       1),
     NULL)
     AS file_id,
    ifnull(
     (select
       id
      from
       data_links dl
      where
       dl.record_id = `Link - Record ID`
      order by
       1
      limit
       1),
     NULL)
     AS file_id,
    ifnull(
     (select
       id
      from
       data_organization do
      where
       do.record_id = `Author Organization - Record ID`
      order by
       1
      limit
       1),
     NULL)
     AS author_organization_id,
    ifnull(
     (select
       id
      from
       data_provider dp1
      where
       dp1.record_id = `Author Data Provider - Record ID`
      order by
       1
      limit
       1),
     NULL)
     AS author_data_provider_id,
`Notes`
    AS notes,
    (select id from mp_lookup_code where lookupmodule = 'PRIORITY' and description = `Priority`) AS priority_id,
    case `User Permissions`
				when 'Registered'	then 'ROLE_USER'
				when 'Bronze'       then 'ROLE_BRONZE'
				when 'Silver'	 	then 'ROLE_SILVER'
				when 'Admin'        then 'ROLE_ADMIN'
                when ''             then 'ROLE_ADMIN'
				else `User Permissions`
			end as user_permission,
    (select id from mp_lookup_code where lookupmodule = 'MARKETPLACE_STATUS' and description = `Marketplace Status`) AS marketplace_status_id,
    (select id from mp_lookup_code where lookupmodule = 'IEI_STATUS' and description = `IEI Status`) AS iei_status_id,
    case when ifnull(`Last Updated Date`, '') <> '' then str_to_date(
     `Last Updated Date`,
     '%m/%d/%Y') else null end
     AS last_updated_date,
    case when ifnull(`Created Time Formula`, '') <> '' then str_to_date(
                    `Created Time Formula`,
                    '%m/%d/%Y %h:%i%p') else null end
                    AS created_time_formula
  FROM
   staging_airtable.stg_data_resources dp
  where
   not exists
     (select
       1
      from
       data_resource
      where
       `Record ID` = data_resource.record_id)
and ifnull(`Record ID`, '') <> '';
