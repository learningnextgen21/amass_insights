INSERT INTO data_provider_events(event_id, event_record_id, data_providers_record_id,
                                  data_providers_id,
                                  event_type)
   SELECT DISTINCT p1.id event_id, p1.record_id, p2.record_id, p2.id data_providers_id, lc.id
        FROM staging_airtable.im_event_data_provider_organization p
             INNER JOIN mp_lookup_code lc
                ON     lc.lookupmodule = 'EVENT_PROVIDER_TYPE'
                   AND lc.lookupcode = 'Data Provider Organizer'
            INNER JOIN data_event p1 ON p1.Record_ID = p.Data_Provider_Organization_RecordID
            INNER JOIN data_provider p2 ON p2.Record_ID = p.data_provider_organization
    WHERE p1.id <> p2.id AND NOT EXISTS
             (SELECT 1
                FROM data_provider_events pp
               WHERE     pp.event_id = p1.id
                     AND pp.data_providers_id = p2.id
                     AND pp.event_type = lc.id);


