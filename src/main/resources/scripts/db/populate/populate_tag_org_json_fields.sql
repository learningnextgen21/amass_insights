SET SESSION group_concat_max_len = 999999999;
UPDATE data_provider SET tag = (select cast(concat('[', group_concat('{"id":"', dt.id, '", "desc":"', dt.provider_tag, '"}'), ']') as json) tag
from data_provider_tag dt inner join data_provider_provider_tag dpt on dt.id = dpt.provider_tags_id where dpt.data_providers_id = data_provider.id);
UPDATE data_provider_tag SET tag_providers = (select cast(concat('[', group_concat('{"id":"', l.id, '"}'), ']') as json) tag_providers
from data_provider l inner join data_provider_provider_tag ac on l.id = ac.data_providers_id where ac.provider_tags_id = data_provider_tag.id);
