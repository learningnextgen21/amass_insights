truncate table data_event_pricing_model;
delete from data_event_pricing_model where not exists(select 1 from data_event dp, mp_lookup_code lc where dp.id = data_event_pricing_model.data_events_id and lc.id = data_event_pricing_model.pricing_models_id);
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'EVENT_PRICING_MODEL' lookupmodule,
                    i.pricing_model lookupcode,
                    i.pricing_model description
               FROM staging_airtable.im_event_pricing_model i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'EVENT_PRICING_MODEL'
                     AND c.lookupcode = lookupcode);

INSERT INTO data_event_pricing_model(pricing_models_id, data_events_id)
   SELECT lc.id, dp.id
     FROM data_event dp
          INNER JOIN staging_airtable.im_event_pricing_model rac
             ON rac.Event_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'EVENT_PRICING_MODEL'
                AND lc.lookupcode = rac.pricing_model
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_event_pricing_model pa
               WHERE     pa.pricing_models_id = lc.id
                     AND pa.data_events_id = dp.id);

create table temp_pricing_model_event_count as select mlc.id, count(*) count from data_event_pricing_model dppm inner join mp_lookup_code mlc on dppm.pricing_models_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_pricing_model_event_count
set record_count = temp_pricing_model_event_count.count
where temp_pricing_model_event_count.id = mp_lookup_code.id;

drop table if exists temp_pricing_model_event_count;
