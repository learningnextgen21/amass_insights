INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'INVESTMENT_MANAGER_STRATEGY' lookupmodule,
                    i.ManagerStrategies lookupcode,
                    i.ManagerStrategies description
               FROM staging_airtable.im_provider_investment_strategies i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'INVESTMENT_MANAGER_STRATEGY'
                     AND c.lookupcode = a.lookupcode);

INSERT INTO data_provider_investment_strategies(data_provider_investment_strategies_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_investment_strategies rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'INVESTMENT_MANAGER_STRATEGY'
                AND lc.lookupcode = rac.ManagerStrategies
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_investment_strategies pa
               WHERE     pa.data_provider_investment_strategies_id = lc.id
                     AND pa.data_Providers_id = dp.id);
