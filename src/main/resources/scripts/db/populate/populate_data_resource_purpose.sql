update data_resource_purpose, staging_airtable.stg_resource_purpose
set data_resource_purpose.purpose = staging_airtable.stg_resource_purpose.Purpose,
    data_resource_purpose.explanation = staging_airtable.stg_resource_purpose.Explanation,
	created_date = str_to_date(`Date Created`, '%m/%d/%Y %h:%i %p')
		where record_id = `Record ID`;

INSERT INTO data_resource_purpose
(
	record_id,
    purpose,
    explanation,
	created_date)
	SELECT
		`record id`,
        purpose,
        explanation,
		str_to_date(`Date Created`, '%m/%d/%Y %h:%i %p') created_date
	FROM `staging_airtable`.`stg_resource_purpose` sat WHERE not exists (select 1 from data_resource_purpose dt where dt.record_id = sat.`Record ID`) ORDER BY Purpose;

update data_resource_purpose, (
                  select @rownum := @rownum + 1 AS sort_order, a.*
                  FROM (SELECT sds.`Record ID`, sds.Purpose
                        FROM staging_airtable.stg_resource_purpose sds
                        ORDER BY sds.Purpose) a,
                      (SELECT @rownum := 0) r) a
set data_resource_purpose.sort_order = a.sort_order
where `Record ID` = record_id;
