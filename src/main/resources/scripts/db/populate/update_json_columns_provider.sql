SET SESSION group_concat_max_len = 999999999;

UPDATE data_provider SET research_methods = (select cast(concat('[', group_concat('{"id":"', l.id, '", "desc":"', l.description, '"}'), ']') as json) research_methods
from mp_lookup_code l inner join data_provider_research_methods ac on l.id = ac.research_methods_id where ac.data_providers_id = data_provider.id);
UPDATE data_provider_tag SET tag_providers = (select cast(concat('[', group_concat('{"id":"', l.id, '"}'), ']') as json) tag_providers
from data_provider l inner join data_provider_provider_tag ac on l.id = ac.data_providers_id where ac.provider_tags_id = data_provider_tag.id);
UPDATE data_provider SET feature = (select cast(concat('[', group_concat('{"id":"', df.id, '", "desc":"', df.data_feature, '"}'), ']') as json) feature
from data_feature df inner join data_provider_feature dpf on df.id = dpf.features_id where data_provider.id = dpf.data_providers_id);
UPDATE data_provider SET tag = (select cast(concat('[', group_concat('{"id":"', dt.id, '", "desc":"', dt.provider_tag, '"}'), ']') as json) tag
from data_provider_tag dt inner join data_provider_provider_tag dpt on dt.id = dpt.provider_tags_id where dpt.data_providers_id = data_provider.id);
update data_provider, (
                  select @rownum := @rownum + 1 AS sort_order, a.*
                  FROM (SELECT dp.id
                        FROM data_provider dp
                        ORDER BY dp.provider_name) a,
                      (SELECT @rownum := 0) r) a
set data_provider.sort_order = a.sort_order
where a.id = data_provider.id;
