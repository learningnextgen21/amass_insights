truncate table data_article_topic;
delete from data_article_topic where not exists(select 1 from
  data_article da, data_topic dt
where da.id = data_article_topic.articles_id
      and dt.id = data_article_topic.topics_id
);
INSERT INTO data_article_topic(articles_id, topics_id)
   SELECT DISTINCT  da.id, dt.id
     FROM staging_airtable.im_article_topic at
          INNER JOIN data_topic dt ON dt.record_id = at.topic
         inner join data_article da on da.record_id = at.Article_RecordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_article_topic dat
               WHERE     dat.articles_id = da.id
                     AND dat.topics_id = dt.id);

create table temp_data_article_topic_count as select drp.id, count(*) count from datamarketplace.data_article_topic dap inner join datamarketplace.data_topic drp ON dap.topics_id = drp.id
group by drp.id;

update data_topic, temp_data_article_topic_count
set article_count = temp_data_article_topic_count.count
where temp_data_article_topic_count.id = data_topic.id;

drop table if exists temp_data_article_topic_count;

update data_topic set article_count = 0 where article_count is null;
