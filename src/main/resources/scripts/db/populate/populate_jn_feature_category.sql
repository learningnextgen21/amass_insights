TRUNCATE TABLE data_feature_category;
delete from data_feature_category where not exists(select 1 from
  data_category dc, data_feature df
where dc.id = data_feature_category.categories_id
      and df.id = data_feature_category.data_features_id
);
INSERT INTO data_feature_category(Categories_ID, Data_Features_ID)
   SELECT dc.ID, df.ID
     FROM staging_airtable.im_feature_category cf
          INNER JOIN data_category dc ON dc.datacategory = cf.category
          INNER JOIN data_feature df ON df.Record_ID = cf.Feature_RecordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_feature_category jcf
               WHERE     jcf.Categories_ID = dc.id
                     AND jcf.Data_Features_ID = df.id);
