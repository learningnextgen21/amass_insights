truncate table data_event_type;
INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'EVENT_TYPE' lookupmodule,
                    i.event lookupcode,
                    i.event description
               FROM staging_airtable.im_event_type i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'EVENT_TYPE'
                     AND c.lookupcode = lookupcode);

INSERT INTO data_event_type(event_type_id,data_event_id)
   SELECT lc.id, dp.id
     FROM data_event dp
          INNER JOIN staging_airtable.im_event_type rac
             ON rac.Event_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'EVENT_TYPE'
                AND lc.lookupcode = rac.event
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_event_type pa
               WHERE     pa.event_type_id = lc.id
                     AND pa.data_event_id = dp.id);

create table temp_event_type_count as select mlc.id, count(*) count from data_event_type dpdf inner join mp_lookup_code mlc on dpdf.event_type_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_event_type_count
set record_count = temp_event_type_count.count
where temp_event_type_count.id = mp_lookup_code.id;

drop table if exists temp_event_type_count;
