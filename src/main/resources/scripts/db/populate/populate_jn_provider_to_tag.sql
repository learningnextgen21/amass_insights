delete from data_provider_provider_tag where not exists(select 1 from data_provider dp, data_provider_tag dpt where dp.id = data_provider_provider_tag.data_providers_id and dpt.id = data_provider_provider_tag.provider_tags_id);
INSERT INTO data_provider_provider_tag(data_providers_id, provider_tags_id)
   SELECT p.ID, pt.ID
     FROM staging_airtable.im_provider_to_tags ptt
          INNER JOIN data_provider p ON ptt.Provider_RecordID = p.record_id
          INNER JOIN data_provider_tag pt ON pt.Record_ID = ptt.Tag_RecordID
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_provider_tag ptt
               WHERE     ptt.data_providers_id = p.ID
                     AND ptt.provider_tags_id = pt.ID);
