update data_topic dt, staging_airtable.stg_article_topic stg
set dt.topic_name = stg.Name,
	dt.created_date = str_to_date(stg.`created time`, '%m/%d/%Y %h:%i %p'),
	dt.hashtag=stg.Hashtag
		where dt.record_id =stg.`Record ID`;

INSERT INTO data_topic
(
	record_id,
	`topic_name`,
	`created_date`,
	`hashtag`)
	SELECT
		`record id`,
		`name`,
		str_to_date(`Created Time`, '%m/%d/%Y %h:%i %p') created_date,
		`Hashtag`
	FROM `staging_airtable`.`stg_article_topic` sat WHERE not exists (select 1 from data_topic dt where dt.record_id = sat.`Record ID`) ORDER BY name;

update data_topic, (
                  select @rownum := @rownum + 1 AS sort_order, a.*
                  FROM (SELECT sds.`Record ID`, sds.`name`
                        FROM staging_airtable.stg_article_topic sds
                        ORDER BY sds.`name`) a,
                      (SELECT @rownum := 0) r) a
set data_topic.sort_order = a.sort_order
where `Record ID` = record_id;
