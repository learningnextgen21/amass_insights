TRUNCATE TABLE data_provider_geographical_focus;
DELETE from data_link_category where not exists(select 1 from data_links dl, mp_lookup_code lc where dl.id = data_link_category.link_id and lc.id = data_link_category.link_category_id);

INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'LINK_CATEGORY' lookupmodule,
                    i.category lookupcode,
                    i.category description
               FROM staging_airtable.im_link_category i where i.category <>''
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'LINK_CATEGORY'
                     AND c.lookupcode = lookupcode);

INSERT INTO data_link_category(link_category_id,
                                             link_id)
   SELECT lc.id, dl.id
     FROM data_links dl
          INNER JOIN staging_airtable.im_link_category ilc
             ON ilc.Link_RecordID = dl.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'LINK_CATEGORY'
                AND lc.lookupcode = ilc.category
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_link_category la
               WHERE     la.link_category_id = lc.id
                     AND la.link_id = dl.id);

create table temp_link_category as select mlc.id, count(*) count from data_link_category dlc inner join mp_lookup_code mlc on dlc.link_category_id = mlc.id
group by mlc.id;

update mp_lookup_code, temp_link_category
set record_count = temp_link_category.count
where temp_link_category.id = mp_lookup_code.id;

drop table if exists temp_link_category;
