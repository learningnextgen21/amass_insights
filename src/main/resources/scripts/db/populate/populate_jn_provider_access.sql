INSERT INTO mp_lookup_code(lookupmodule,
                           lookupcode,
                           description,
                           sortorder)
   SELECT a.*, @rownum := @rownum + 1 AS sort_order
     FROM (  SELECT DISTINCT
                    'PROVIDER_ACCESS' lookupmodule,
                    i.AccessOffered lookupcode,
                    i.AccessOffered description
               FROM staging_airtable.im_provider_access i
           ORDER BY lookupcode) a,
          (SELECT @rownum := 0) r
    WHERE NOT EXISTS
             (SELECT 1
                FROM mp_lookup_code c
               WHERE     c.lookupmodule = 'PROVIDER_ACCESS'
                     AND c.lookupcode = a.lookupcode);


   INSERT INTO data_provider_access(data_provider_access_id,
                                             data_Providers_id)
   SELECT lc.id, dp.id
     FROM data_provider dp
          INNER JOIN staging_airtable.im_provider_access rac
             ON rac.Provider_RecordID = dp.record_id
          INNER JOIN mp_lookup_code lc
             ON     lc.lookupmodule = 'PROVIDER_ACCESS'
                AND lc.lookupcode = rac.AccessOffered
    WHERE NOT EXISTS
             (SELECT 1
                FROM data_provider_access pa
               WHERE     pa.data_provider_access_id = lc.id
                     AND pa.data_Providers_id = dp.id);
