update datamarketplace.data_comments dc, staging_airtable.stg_data_comment sdc
		SET dc.title = sdc.Title,
			dc.comment = sdc.Comment,
			dc.privacy = sdc.Privacy,
			dc.by_user_id = sdc.`From User`,
			dc.to_user_id = case when ifnull(sdc.`To User`, '') <> '' then
                    CAST(sdc.`To User` as UNSIGNED) else null END,
			dc.receive_notifications= sdc.`Receive Notifications`,
			dc.anonymity = sdc.Anonymity,
			dc.inactive = sdc.Inactive,
			dc.created_date = case when ifnull(sdc.`Created Date`, '') <> '' then str_to_date(
                      sdc.`Created Date`,
                      '%m/%d/%Y %h:%i%p') else null end,
            dc.updated_date = case when ifnull(sdc.`Updated Date`, '') <> '' then str_to_date(
                      `Updated Date`,
                      '%m/%d/%Y %h:%i%p') else null end
                      where dc.record_id = sdc.`Record ID`;

insert
	into
		datamarketplace.data_comments(
			              title,
                          record_id,
                          comment,
                          privacy,
                          by_user_id,
                          to_user_id,
                          receive_notifications,
                          anonymity,
                          inactive,
                          created_date,
                          updated_date
                          )
		SELECT
			`Title` AS title,
      `Record ID` AS record_id,
            `Comment` AS comment,
             Privacy AS privacy,
             `From User` AS by_user_id,
             case when ifnull(`To User`, '') <> '' then
                    CAST(`To User` as UNSIGNED) else null END
                     AS to_user_id,
             `Receive Notifications` AS receive_notifications,
             Anonymity AS anonymity,
              Inactive AS inactive,
              case when ifnull(`Created Date`, '') <> '' then str_to_date(
                    `Created Date`,
                    '%m/%d/%Y %h:%i%p') else null end
                    AS created_date,
              case when ifnull(`Updated Date`, '') <> '' then str_to_date(
                                      `Updated Date`,
                                      '%m/%d/%Y %h:%i%p') else null end
                                      AS updated_date
      FROM
               staging_airtable.stg_data_comment sdc
               where
               not exists
                 (select
                   1
                  from
                   data_comments
                  where
                   `Record ID` = data_comments.record_id)
            and ifnull(`Record ID`, '') <> '';
update datamarketplace.data_comments dc SET dc.to_user_id = NULL WHERE dc.to_user_id=0;
