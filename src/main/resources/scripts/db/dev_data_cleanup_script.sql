set foreign_key_checks = 0;

delete from data_provider
      where id > 400
            and data_provider.record_id not in ('recXgfYQ3J9P2dJQ5', 'recKNP08sL01Zjron','recVzZ3TOUdYPi7jZ');

delete from data_organization
      where     id not in
                   (select ifnull(owner_organization_id, 0)
                      from data_provider)
            and id <> 1;

drop table if exists orgs_to_delete;
create table orgs_to_delete
as
   select id
     from data_organization
    where     parent_organization_id not in
                 (select id from data_organization)
          and id <> 1;

delete from data_organization
      where id in (select id from orgs_to_delete) and id <> 1;

drop table if exists orgs_to_delete;

delete from data_provider
      where     owner_organization_id not in
                   (select id from data_organization)
            and id <> 1;

delete from mp_dev.data_article
      where     author_organization_id not in
                   (select id from data_organization)
            and id <> 1;
delete from data_article where author_data_provider_record_id not in (select data_provider.record_id from data_provider);

delete from data_provider_articles
      where data_providers_record_id not in (select record_id from data_provider);

delete from data_article
      where     id not in (select articles_id from data_provider_articles)
            and id <> 1;

delete from data_provider_asset_class
      where     data_providers_id not in (select id from data_provider);

delete from data_provider_category
      where data_providers_id not in (select id from data_provider);

delete from data_provider_feature
      where data_providers_id not in (select id from data_provider);

delete from data_provider_feature
where features_id not in (select id from data_feature);

/*delete from data_provider_pricing_model
      where data_providers_id not in (select id from data_provider);*/

delete from data_provider_delivery_format
      where data_providers_id not in (select id from data_provider);

delete from data_provider_delivery_method
      where data_providers_id not in (select id from data_provider);

delete from data_provider_industry
      where data_providers_id not in (select id from data_provider);

delete from data_provider_investor_type
      where data_providers_id not in (select id from data_provider);

delete from data_industry
      where     id not in (select industries_id from data_provider_industry)
      and id not in (select main_data_industry_id from data_provider) and id <> 1;

delete from data_provider_partner
      where data_providers_id not in (select id from data_provider);

delete from data_provider_partner
      where partners_id not in (select id from data_provider);

delete from data_provider_geographical_focus
      where data_providers_id not in (select id from data_provider);

delete from data_provider_source
      where data_providers_id not in (select id from data_provider);

delete from data_source
      where     id not in (select sources_id from data_provider_source)
            and id <> 1;

delete from data_provider_provider_tag
      where data_providers_id not in (select id from data_provider);

delete from data_provider_feature
      where data_providers_id not in (select id from data_provider);

delete from data_provider_security_type
      where data_providers_id not in (select id from data_provider);

delete from data_feature
      where id not in (select data_features_id from data_feature_category);

delete from data_feature
      where     id not in (select features_id from data_provider_feature)
            and id <> 1;

delete from data_feature_category
      where data_features_id not in (select id from data_feature);

delete from data_provider_industry
      where data_providers_id not in (select id from data_industry);

delete from data_provider_logo where provider_record_id not in (select record_id from data_provider);

delete From mp_user_provider where provider_id not in (select id from data_provider);

truncate table mp_email;

set foreign_key_checks = 1;
