#!/bin/sh
MYSQLCMD="mysql"
MYSQLDUMPCMD="mysqldump"
echo `date` Starting test database preparation script
`$MYSQLCMD -e"drop database if exists mp_dev; create database mp_dev;"`
`$MYSQLDUMPCMD --routines --no-create-db --default-character-set=utf8 datamarketplace | $MYSQLCMD mp_dev`
`$MYSQLCMD mp_dev -e"source /Users/rahul/Development/AI/amass_marketplace/src/main/resources/scripts/db/dev_data_cleanup_script.sql;show warnings;"`
`$MYSQLCMD mp_dev -e"source /Users/rahul/Development/AI/amass_marketplace/src/main/resources/scripts/db/dev_data_cleanup_script.sql;show warnings;"`
`$MYSQLDUMPCMD --routines --no-create-db --default-character-set=utf8 mp_dev | gzip > mp_dev.sql.gz`
aws s3 cp mp_dev.sql.gz s3://ai-noahdata-shared --acl public-read --profil=amass
rm mp_dev.sql.gz
echo `date` Finished preparing dev_db datbase.
