#!/bin/sh
ctrlfile=populate_scripts.ctl
echo `date` Starting data population script
while IFS= read -r file
do
	comment=${file:0:1}
	if [ "$comment" = "#" ]
        then
		printf "`date` \033[0;33mSkipping $file \n\033[0m"
        else
		echo `date` Executing $file
		`mysql datamarketplace -e"SET @@FOREIGN_KEY_CHECKS = 0;source populate/$file;SET @@FOREIGN_KEY_CHECKS = 1;show warnings;"`
		echo `date` Finished executing $file
	fi
done < $ctrlfile
echo `date` Finished population scripts.
